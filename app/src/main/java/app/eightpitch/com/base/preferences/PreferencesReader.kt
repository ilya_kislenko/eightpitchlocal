package app.eightpitch.com.base.preferences

import android.content.Context
import app.eightpitch.com.data.dto.biometric.UserBiometricInfo
import app.eightpitch.com.data.dto.user.User
import com.google.gson.Gson

class PreferencesReader(appContext: Context) : PreferencesProvider(appContext) {

    internal fun getMACAddress() = getStringByKey(MAC_ADDRESS_KEY)

    internal fun getCachedAccessToken() = getStringByKey(ACCESS_TOKEN_KEY)

    internal fun getCachedRefreshToken() = getStringByKey(REFRESH_TOKEN_KEY)

    @User.CREATOR.RoleType
    internal fun getUserRole(): String = getStringByKey(USER_ROLE)

    internal fun getCurrentUserExternalId() = getStringByKey(USER_EXTERNAL_ID)

    internal fun getUserBiometricInfo(): UserBiometricInfo {
        val externalId = getCurrentUserExternalId()
        val userInfoJson = getStringByKey(externalId)
        return if (userInfoJson.isEmpty())
            UserBiometricInfo()
        else
            try {
                Gson().fromJson(userInfoJson, UserBiometricInfo::class.java)
            } catch (throwable: Throwable) {
                UserBiometricInfo()
            }
    }

    internal fun isTokenExpired(): Boolean {
        val expirationTime = getLongByKey(TOKEN_EXPIRATION_KEY)
        return System.currentTimeMillis() > expirationTime
    }

    internal fun isLoggedIn() = getCachedAccessToken().isNotEmpty()
    internal fun shouldShowQuickLoginDialog() = sharedPreferences.getBoolean(SHOW_QUICK_LOGIN_DIALOG_KEY, true)

    // ---------------------------------------------------------------------------------

    private fun getStringByKey(key: String, defaultValue: String = ""): String =
        sharedPreferences.getString(key, defaultValue) ?: defaultValue

    private fun getLongByKey(key: String, defaultValue: Long = -1) = sharedPreferences.getLong(key, defaultValue)
}