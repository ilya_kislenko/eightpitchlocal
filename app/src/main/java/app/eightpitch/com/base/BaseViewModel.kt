package app.eightpitch.com.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.coroutines.coroutineScope

abstract class BaseViewModel(private val threadsSeparator: ThreadsSeparator) : ViewModel(){

    private val isLoading = SingleActionLiveData<Boolean>()
    fun getLoadingState() : LiveData<Boolean> = isLoading

    fun setIsLoading(isLoading:Boolean){
        this.isLoading.postValue(isLoading)
    }

    suspend fun <T> asyncLoading(func: suspend () -> T): Result<T> = coroutineScope {

        isLoading.postValue(true)

        val result = threadsSeparator.startRequest { func() }

        isLoading.postValue(false)
        
        result
    }
    
    /**
     * This is a placeholder to clean up view model references, which shouldn't be kept
     * while current fragment is hidden (user input) because action move forward was called and fragment is still alive
     */
    open fun cleanUp() {}

    /**
     * This is a placeholder to clean up view model references finally, which shouldn't live
     * more than a fragment (Like user selection). Will be called when user move away from the fragment
     * and it will be destroyed finally
     */
    open fun cleanUpFinally() {}
}