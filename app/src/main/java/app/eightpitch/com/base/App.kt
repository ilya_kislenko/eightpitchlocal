package app.eightpitch.com.base

import android.app.Application
import app.eightpitch.com.BuildConfig
import app.eightpitch.com.R
import app.eightpitch.com.dagger.Injector
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        Injector.initAppComponent(applicationContext)

        if (BuildConfig.ENABLE_CRASHLYTICS) {
            // Obtain the FirebaseAnalytics instance to enable default analytics
            Firebase.analytics
        }
        //Required for some edge cases when we're creating a views using app context within a long living builders
        //see UIElementsBuilder for details
        applicationContext.setTheme(R.style.AppTheme)
    }
}