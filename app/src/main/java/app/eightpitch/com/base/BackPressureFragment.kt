package app.eightpitch.com.base

import android.os.Bundle
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import app.eightpitch.com.R
import app.eightpitch.com.extensions.*

open class BackPressureFragment : Fragment() {

    /**
     * We should keep an instance of created callback
     * for removing purpose, and remove it right after
     * view is destroyed and add it again when view is creating.
     * Because this callback will be removed only when fragment is completely destroyed, not view, by default.
     */
    private var onBackPressedCallback: OnBackPressedCallback? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleBackButton()
    }

    /**
     * We can't use  Navigation.findNavController(it).popBackStack() here,
     * because then, registered onBackPressedListeners won't be called
     */
    private fun handleBackButton() {

        if (isAliveAndAvailable())
            view?.let { rootView ->
                val backButton = rootView.findViewById<View>(R.id.toolbarBackButton)
                backButton?.setOnClickListener {
                    requireActivity().run {
                        hideKeyboard()
                        onBackPressed()
                    }
                }
            }
    }

    protected fun registerOnBackPressedListener(
        callback: () -> Unit
    ) {
        onBackPressedCallback = addOnBackPressedCallback(callback)
    }

    protected fun popBackStack(): Boolean {

        if (isAliveAndAvailable())
            view?.let {
                return Navigation.findNavController(it).popBackStack()
            }
        return false
    }

    protected fun popBackStack(@IdRes untilID: Int, isInclusive: Boolean = false): Boolean {

        if (isAliveAndAvailable())
            view?.let {
                return Navigation.findNavController(it).popBackStack(untilID, isInclusive)
            }
        return false
    }

    override fun onDestroyView() {
        onBackPressedCallback?.remove()
        super.onDestroyView()
    }
}