package app.eightpitch.com.base.rootbackhandlers

import android.annotation.SuppressLint
import android.app.Activity
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.viewpager2.widget.ViewPager2
import app.eightpitch.com.base.BaseActivity
import app.eightpitch.com.extensions.isAliveAndAvailable
import com.google.android.material.tabs.TabLayout

/**
 * An interface that designed for all `root` fragments
 * that are localed in general [Activity]'s [ViewPager2] and
 * each of them is bind with a [TabLayout] tab and has it's own navGraph.
 *
 * This interface gives an ability to handle back pressure inside a local
 * `root` fragment nav graph, not [Activity] one.
 */
interface RootNavGraphBackPressureHandler {

    /**
     * Method that should be called by [Activity]
     * and this gives an ability for a `root` fragment
     * to pop it's back stack if possible.
     *
     * @return `true` is fragment is pop out it's own back stack
     * `false` if there is no [Fragment]'s to pop out and there one
     * left.
     */
    fun handleBackPressure(): Boolean
}

/**
 * An interface for a `child` fragments that
 * are located in one of the `root` fragment.
 *
 * This interface gives an ability to handle back pressure inside a local
 * `child` fragment and pop out back stack not to previous fragment in the
 * stack, but anyone from `root`'s navGraph.
 */
interface ChildNavGraphBackPressureHandler {

    /**
     * Method that should be called by `root` fragment
     * and this gives an ability for a `child` fragment
     * to pop it's back stack if possible.
     *
     * @return `true` is fragment is pop out `root` back stack
     * on a several fragment's back `false` if it's not possible
     * and `child` fragment is only one left in `root` fragment stack.
     */
    fun handleBackPressure(): Boolean
}

/**
 * An abstraction for an [Activity] that will contain
 * a [ViewPager2] with [TabLayout] and `root` fragments
 * will be attached with each tab.
 *
 * This abstraction provides an ability to pass a back pressure
 * event to top most selected [Fragment] so it will be able
 * to handle back pressure properly.
 */
@SuppressLint("Registered")
abstract class MainTabbedNavGraphHandleAbleActivity : BaseActivity() {

    /**
     * Method where you should provide a top most `root` or not
     * fragment which is linked with selected tab in [TabLayout]
     * usually you can get it from a [ViewPager2]'s adapter
     */
    protected abstract fun getCurrentFragment(): Fragment?

    override fun onBackPressed() {

        val currentFragment = getCurrentFragment()
        if (currentFragment == null)
            super.onBackPressed()
        else {
            if (currentFragment is RootNavGraphBackPressureHandler) {
                if (!currentFragment.handleBackPressure()) {
                    //Case when root fragment have only one child left and cannot pop out it
                    super.onBackPressed()
                }
            } else
                super.onBackPressed()
        }
    }
}

/**
 * Base class for any `root` fragment that will be a `root` for a [Fragment]'s
 * hierarchy and has each own navGraph and be linked with a [TabLayout] tab
 * and be shown in [ViewPager2].
 *
 * This abstraction allows you to handle back pressure from [Activity] properly.
 */
abstract class RootNavGraphHandleAbleFragment : Fragment(), RootNavGraphBackPressureHandler {

    /**
     * Method where you should provide an id of [androidx.navigation.fragment.NavHostFragment]
     * related with this `root` fragment.
     */
    protected abstract fun getNavigationHostFragmentId(): Int

    override fun handleBackPressure(): Boolean {
        if (!isAliveAndAvailable())
            return false
        val navigationHostId = getNavigationHostFragmentId()
        val navHostFragment = childFragmentManager.findFragmentById(navigationHostId)
        val topMostFragment = navHostFragment?.childFragmentManager?.fragments?.getOrNull(0)
        return if (topMostFragment is ChildNavGraphBackPressureHandler) {
            val successfulTransaction = topMostFragment.handleBackPressure()
            if (!successfulTransaction) popBackStack() else successfulTransaction
        } else {
            popBackStack()
        }
    }

    /**
     * Method that will try to pop up the back stack by one step back.
     * Will return `true` if it was successfully `false` in the case if only one
     * fragment left in the graph.
     */
    private fun popBackStack(): Boolean {
        return if (isAliveAndAvailable()) {
            val navController =
                Navigation.findNavController(requireActivity(), getNavigationHostFragmentId())
            return navController.popBackStack()
        } else false
    }
}