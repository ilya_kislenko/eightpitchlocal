package app.eightpitch.com.base

import androidx.annotation.WorkerThread
import app.eightpitch.com.base.preferences.PreferencesReader
import app.eightpitch.com.base.preferences.PreferencesWriter
import app.eightpitch.com.extensions.builder
import app.eightpitch.com.utils.Logger
import app.eightpitch.com.utils.Logger.TAG_GENERIC_ERROR
import java.net.InetAddress
import java.net.NetworkInterface
import java.util.*

class MACAddressProvider(
    private val reader: PreferencesReader,
    private val writer: PreferencesWriter
) {

    internal fun provideAMACAddress(): String {
        val deviceMACAddress = reader.getMACAddress()
        return if (deviceMACAddress.isEmpty()) {
            val macAddress = getAMACAddress()
            if (macAddress.isNotEmpty())
                writer.saveMACAddress(macAddress)
            return macAddress
        } else
            deviceMACAddress
    }

    /**
     * This method should be called only when wifi connection is established, otherwise we can't guarantee that
     * MAC address will be retrieved.
     * NOTE: This method will return am empty string if couldn't get a real MAC address.
     */
    @WorkerThread
    private fun getAMACAddress(): String {
        try {
            val interfaceName = "wlan0"
            val interfaces = Collections.list(NetworkInterface.getNetworkInterfaces())
            interfaces.forEach {
                if (it.name.equals(interfaceName, true)) {

                    val macAddress = it.hardwareAddress ?: return ""

                    return StringBuilder().builder {
                        macAddress.forEach { byte ->
                            append(String.format("%02X:", byte))
                        }

                        if (this.isNotEmpty())
                            deleteCharAt(length - 1)

                        this
                    }
                }
            }

            return ""

        } catch (exception: Exception) {
            Logger.logError(
                TAG_GENERIC_ERROR,
                "Can't get a MAC address from this device",
                exception
            )
            return ""
        }
    }
}