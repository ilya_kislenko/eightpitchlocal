package app.eightpitch.com.base.baseviewmodels.codeverification.dto

import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.StringDef
import app.eightpitch.com.extensions.NO_TYPE
import app.eightpitch.com.extensions.readStringSafe

/**
 * Class contains all information for binding screens with code verification
 */
data class ValidationScreenInfoDTO(
    val header: String = "",
    val toolbarTitle: String = "",
    val subTitleMessage: String = "",
    val validationButtonName: String = "",
    val resendButtonName: String = "",
    @VerificationType val verificationType: String = NO_TYPE
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(header)
        parcel.writeString(toolbarTitle)
        parcel.writeString(subTitleMessage)
        parcel.writeString(validationButtonName)
        parcel.writeString(resendButtonName)
        parcel.writeString(verificationType)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<ValidationScreenInfoDTO> {
        override fun createFromParcel(parcel: Parcel): ValidationScreenInfoDTO {
            return ValidationScreenInfoDTO(parcel)
        }

        override fun newArray(size: Int): Array<ValidationScreenInfoDTO?> {
            return arrayOfNulls(size)
        }

        const val PHONE = "PHONE"
        const val EMAIL = "EMAIL"
        const val LOGIN = "LOGIN"
        const val LOGIN_WITH_NEXT_GA = "LOGIN_WITH_NEXT_GA"
        const val LOGIN2 = "LOGIN2"
        const val INVEST = "INVEST"
        const val GOOGLE = "GOOGLE"
        const val DELETE_ACCOUNT = "DELETE_ACCOUNT"
        const val CHANGE_EMAIL = "CHANGE_EMAIL"
        const val TWO_AUTH_VALIDATION_SMS_ENABLE = "TWO_AUTH_VALIDATION_SMS_ENABLE"
        const val TWO_AUTH_VALIDATION_SMS_DISABLE = "TWO_AUTH_VALIDATION_SMS_DISABLE"
        const val TWO_AUTH_VALIDATION_GA_ENABLE = "TWO_AUTH_VALIDATION_GA_ENABLE"
        const val TWO_AUTH_VALIDATION_GA_DISABLE = "TWO_AUTH_VALIDATION_GA_DISABLE"
        const val TWO_AUTH_CODE_VERIFICATION = "TWO_AUTH_CODE_VERIFICATION"
        const val RESTORE_PASSWORD = "RESTORE_PASSWORD"
        const val CHANGE_PHONE_NUMBER = "CHANGE_PHONE_NUMBER"

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(NO_TYPE,
            PHONE, EMAIL, LOGIN, LOGIN2,
            INVEST, GOOGLE, DELETE_ACCOUNT,
            RESTORE_PASSWORD, CHANGE_EMAIL,
            TWO_AUTH_VALIDATION_SMS_ENABLE,
            TWO_AUTH_VALIDATION_SMS_DISABLE,
            TWO_AUTH_VALIDATION_GA_ENABLE,
            TWO_AUTH_VALIDATION_GA_DISABLE,
            TWO_AUTH_CODE_VERIFICATION, CHANGE_PHONE_NUMBER)
        annotation class VerificationType
    }
}
