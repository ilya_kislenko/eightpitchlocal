package app.eightpitch.com.base

import android.content.Context
import app.eightpitch.com.R
import app.eightpitch.com.data.dto.api.ErrorBodyMessage
import app.eightpitch.com.data.dto.sms.ConfirmInvestmentResponse
import app.eightpitch.com.utils.Empty
import app.eightpitch.com.utils.Logger
import com.google.gson.Gson
import retrofit2.Response
import java.io.Reader

open class BaseModel(protected val appContext: Context) {

    private val emptyBodyMessage = appContext.getString(R.string.empty_body_message)
    private val serverIsUnavailableMessage = appContext.getString(R.string.server_unavailable_error)

    protected fun <T> checkRetrofitResponseBodyToError(
        response: Response<T>,
        clazz: Class<T>? = null
    ): T {

        val responseCode = response.code()

        return if (responseCode == 200) {

            val responseBody = response.body()

            if (null == responseBody && null != clazz && Empty::class.java.isAssignableFrom(clazz))
                return Empty.instance as T
            else
                responseBody ?: throw EmptyResponseBodyException(emptyBodyMessage)
        } else if (responseCode in 201 until 400) {
            Gson().fromJson(response.errorBody()?.charStream(), clazz) ?: throw EmptyResponseBodyException(
                emptyBodyMessage)
        } else
            throw getMessageForResponseCode(responseCode, getErrorBody(response.errorBody()?.charStream()))
    }

    protected fun checkRetrofitVoidResponseBodyToError(response: Response<Void>): Empty {

        val responseCode = response.code()

        return if (responseCode == 200) {

            val responseBody = response.body()

            if (null == responseBody)
                Empty.instance
            else
                throw EmptyResponseBodyException(emptyBodyMessage)
        } else if (responseCode in 201 until 400) {
            Gson().fromJson(response.errorBody()?.charStream(), null) ?: throw EmptyResponseBodyException(
                emptyBodyMessage)
        } else
            throw getMessageForResponseCode(responseCode, getErrorBody(response.errorBody()?.charStream()))
    }

    protected fun <T> checkOkHttpResponseBodyToError(response: okhttp3.Response, clazz: Class<T>): T {

        val responseCode = response.code()
        val responseBody = response.body()

        return if (responseCode == 200) {

            val realBody = responseBody?.string()

            if (null == realBody && clazz.isAssignableFrom(Empty::class.java))
                return Empty.instance as T
            else
                Gson().fromJson(realBody, clazz) ?: throw EmptyResponseBodyException(emptyBodyMessage)

        } else
            throw getMessageForResponseCode(responseCode, getErrorBody(responseBody?.charStream()))
    }

    private fun getErrorBody(errorBody: Reader?): ErrorBodyMessage {

        var errorBodyMessage = ErrorBodyMessage()

        if (null != errorBody) {

            try {
                errorBodyMessage = Gson().fromJson(errorBody, ErrorBodyMessage::class.java)
            } catch (jsonException: Exception) {
                Logger.logError(BaseModel::class.java.name, "Can't read error response body")
            }
        }

        return errorBodyMessage
    }

    private fun getMessageForResponseCode(
        httpCode: Int,
        errorBodyMessage: ErrorBodyMessage
    ): Exception = when (httpCode) {
        in 500..505 -> ServerException(serverIsUnavailableMessage)
        else -> ServerException(getMessageForErrorBody(errorBodyMessage))
    }

    private fun getMessageForErrorBody(errorBody: ErrorBodyMessage): String {

        val exceptionCode = errorBody.exceptionCode
        if (1300 == exceptionCode) {
            val message = errorBody.message
            return if (message.isNotEmpty()) message else appContext.getString(R.string.something_go_wrong_exception_text)
        }

        return appContext.getString(R.string.something_go_wrong_exception_text)
    }

    class ServerException(message: String) : Exception(message)
    class EmptyResponseBodyException(message: String) : Exception(message)
    class InvalidCredentialsException(message: String) : Exception(message)
    /**
     * Exception that investment confirmation finished with one of the result statuses from [ConfirmInvestmentResponse]
     */
    open class InvestmentConfirmationException(message: String) : Exception(message)

    /**
     * Exception that indicates the [ConfirmInvestmentResponse.CANCELED] status for investment.
     */
    class CancelledInvestmentException(message: String) : InvestmentConfirmationException(message)
    class AbsentTokensException(message: String, val interactionId: String, val arrayStatusTwoAuth: ArrayList<String> = arrayListOf()): Exception(message)
    class WaitingCodeException(message: String): Exception(message)
    class HasNotAttempts(message: String): Exception(message)
}