package app.eightpitch.com.base.preferences

import android.content.Context
import android.content.SharedPreferences

open class PreferencesProvider(appContext: Context) {

    private val PREFERENCES_FILE_NAME = "${appContext.packageName}.preferences"

    protected val sharedPreferences: SharedPreferences =
        appContext.getSharedPreferences(PREFERENCES_FILE_NAME, Context.MODE_PRIVATE)

    protected val USER_ROLE = "user_role"
    protected val MAC_ADDRESS_KEY = "mac_address_key"
    protected val USER_EXTERNAL_ID = "user_external_id"
    protected val ACCESS_TOKEN_KEY = "access_token_key"
    protected val REFRESH_TOKEN_KEY = "refresh_token_key"
    protected val TOKEN_EXPIRATION_KEY = "token_expiration_key"
    protected val SHOW_QUICK_LOGIN_DIALOG_KEY = "show_quick_login_dialog_key"
}