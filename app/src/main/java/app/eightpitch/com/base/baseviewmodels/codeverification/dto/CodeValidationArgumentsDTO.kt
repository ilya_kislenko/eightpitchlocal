package app.eightpitch.com.base.baseviewmodels.codeverification.dto

import android.os.Parcel
import android.os.Parcelable
import app.eightpitch.com.data.dto.signup.CommonInvestorRequestBody
import app.eightpitch.com.extensions.readStringSafe

/**
 * If you need new arguments for DefaultCodeValidation you can add it here
 */
data class CodeValidationArgumentsDTO(
    val projectId: String = "",
    val investmentId: String = "",
    val validationScreenInfo: ValidationScreenInfoDTO = ValidationScreenInfoDTO(),
    val partialUser: CommonInvestorRequestBody = CommonInvestorRequestBody()
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readParcelable(ValidationScreenInfoDTO::class.java.classLoader) ?: ValidationScreenInfoDTO(),
        parcel.readParcelable(CommonInvestorRequestBody::class.java.classLoader) ?: CommonInvestorRequestBody())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(projectId)
        parcel.writeString(investmentId)
        parcel.writeParcelable(validationScreenInfo, flags)
        parcel.writeParcelable(partialUser, flags)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<CodeValidationArgumentsDTO> {
        override fun createFromParcel(parcel: Parcel): CodeValidationArgumentsDTO {
            return CodeValidationArgumentsDTO(parcel)
        }

        override fun newArray(size: Int): Array<CodeValidationArgumentsDTO?> {
            return arrayOfNulls(size)
        }
    }
}