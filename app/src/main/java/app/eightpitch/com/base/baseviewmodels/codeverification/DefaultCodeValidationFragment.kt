package app.eightpitch.com.base.baseviewmodels.codeverification

import android.app.Activity
import android.content.*
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.base.BaseModel.HasNotAttempts
import app.eightpitch.com.base.BaseModel.CancelledInvestmentException
import app.eightpitch.com.base.BaseModel.InvestmentConfirmationException
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO.CREATOR.CHANGE_EMAIL
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO.CREATOR.CHANGE_PHONE_NUMBER
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO.CREATOR.DELETE_ACCOUNT
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO.CREATOR.INVEST
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO.CREATOR.LOGIN
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO.CREATOR.PHONE
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO.CREATOR.RESTORE_PASSWORD
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO.CREATOR.TWO_AUTH_CODE_VERIFICATION
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO.CREATOR.TWO_AUTH_VALIDATION_GA_DISABLE
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO.CREATOR.TWO_AUTH_VALIDATION_GA_ENABLE
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO.CREATOR.TWO_AUTH_VALIDATION_SMS_DISABLE
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO.CREATOR.TWO_AUTH_VALIDATION_SMS_ENABLE
import app.eightpitch.com.data.dto.signup.CommonInvestorRequestBody
import app.eightpitch.com.data.dto.twofactor.SmsTwoFactorResponse.Companion.INVALID_CODE
import app.eightpitch.com.data.dto.twofactor.SmsTwoFactorResponse.Companion.SUCCESS
import app.eightpitch.com.data.dto.twofactor.SmsTwoFactorResponse.Companion.TWO_FACTOR_AUTH_ALREADY_ENABLED
import app.eightpitch.com.data.dto.twofactor.TwoFactorVerifyCodeResponse.Companion.TWO_FACTOR_AUTH_VERIFICATION
import app.eightpitch.com.data.dto.user.User.CREATOR.INSTITUTIONAL_INVESTOR
import app.eightpitch.com.data.dto.user.User.CREATOR.PRIVATE_INVESTOR
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.authorized.AuthorizedActivity
import app.eightpitch.com.ui.payment.hardcappaymentselection.HardCapPaymentSelectionFragment
import app.eightpitch.com.ui.payment.softcappaymentselection.SoftCapPaymentSelectionFragment
import app.eightpitch.com.ui.registration.signup.institutionalinvestor.password.IIPasswordFragment
import app.eightpitch.com.ui.registration.signup.privateinvestor.password.PVIPasswordFragment
import app.eightpitch.com.ui.twofactor.choosetwoauthmethod.ChooseTwoAuthMethodFragment.Companion.NAVIGATE_FROM_KEY
import app.eightpitch.com.ui.twofactor.choosetwoauthmethod.ChooseTwoAuthMethodFragment.Companion.NAVIGATE_FROM_KYC
import app.eightpitch.com.ui.twofactor.choosetwoauthmethod.ChooseTwoAuthMethodFragment.Companion.NAVIGATE_FROM_SECURITY
import app.eightpitch.com.ui.twofactor.twofactorconfirm.KycTwoFactorSuccessfullyScreenFragment
import app.eightpitch.com.ui.webview.DefaultWebViewFragment
import app.eightpitch.com.utils.*
import app.eightpitch.com.utils.autoinsertsms.*
import com.google.android.gms.auth.api.phone.SmsRetriever
import kotlinx.android.synthetic.main.fragment_digit_validation_code.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import java.lang.ref.WeakReference

class DefaultCodeValidationFragment : BaseFragment<DefaultCodeValidationViewModel>() {

    companion object {

        internal const val PROJECT_ID = "PROJECT_ID"
        internal const val PARTIAL_USER = "PARTIAL_USER"
        internal const val INVESTMENT_ID = "INVESTMENT_ID"
        internal const val DELETING_REASON = "DELETING_REASON"
        internal const val VALIDATION_KEY_CODE = "VALIDATION_KEY_CODE"

        fun buildWithAnArguments(
            projectId: String = "",
            investmentId: String = "",
            deletingReason: String = "",
            twoAuthNavigateFrom: String = "",
            validationScreenInfo: ValidationScreenInfoDTO,
            partialUser: CommonInvestorRequestBody,
        ) = Bundle().apply {
            putString(PROJECT_ID, projectId)
            putString(INVESTMENT_ID, investmentId)
            putString(DELETING_REASON, deletingReason)
            putString(NAVIGATE_FROM_KEY, twoAuthNavigateFrom)
            putParcelable(VALIDATION_KEY_CODE, validationScreenInfo)
            putParcelable(PARTIAL_USER, partialUser)
        }
    }

    private val timer = Timer()
    private val smsVerificationReceiver = SMSBroadcastReceiver()

    private val projectId: String
        get() = arguments?.getString(PROJECT_ID) ?: ""

    private val investmentId: String
        get() = arguments?.getString(INVESTMENT_ID) ?: ""

    private val deletingReason: String
        get() = arguments?.getString(DELETING_REASON) ?: ""

    private val validationScreenInfo: ValidationScreenInfoDTO?
        get() = arguments?.getParcelable(VALIDATION_KEY_CODE)

    private val partialUser: CommonInvestorRequestBody
        get() = arguments?.getParcelable(PARTIAL_USER) ?: CommonInvestorRequestBody()

    private val navigateFrom: String
        get() = arguments?.getString(NAVIGATE_FROM_KEY) ?: ""

    override fun getLayoutID() = R.layout.fragment_digit_validation_code

    override fun getVMClass() = DefaultCodeValidationViewModel::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupReceiver()
    }

    private fun setupReceiver() {
        context?.registerSMSBroadcastReceiver(smsVerificationReceiver)
        smsVerificationReceiver.registerNewMessageListener(DefaultSMSInterceptor(WeakReference(this)))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()
        timer.timerListener = TimerListener()
        setupInputFieldsListener()
        context?.let { SmsRetriever.getClient(it).startSmsUserConsent(null) }

        viewModel.getLoginWithCodeResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) {
                if (it.verificationTypes.contains(TWO_FACTOR_AUTH_VERIFICATION)) {
                    setupGAVerification(it.interactionId)
                    val arguments = buildWithAnArguments(
                        validationScreenInfo = createValidationScreenInfo(),
                        partialUser = CommonInvestorRequestBody(
                            interactionId = it.interactionId
                        )
                    )
                    viewModel.cleanUpFinally()
                    navigate(R.id.action_digitCodeValidationFragment_to_emptyFragment, arguments)
                } else {
                    context?.let { appContext ->
                        IntentsController.startActivityWithANewStack(
                            appContext,
                            AuthorizedActivity::class.java
                        )
                    }
                }
            }
        })

        viewModel.getRegistrationVerifyCodeResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) {
                when (validationScreenInfo?.verificationType) {
                    DELETE_ACCOUNT -> viewModel.deleteAccount(deletingReason)
                    else -> navigateToNextScreen()
                }
            }
        })

        viewModel.getChangePhoneNumberResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) {
                navigate(R.id.action_defaultCodeValidationFragment_to_successPhoneNumberChanged)
            }
        })

        viewModel.getTwoFactorEnabledResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) { smsTwoFactorResponse ->
                when (smsTwoFactorResponse.status) {
                    SUCCESS, TWO_FACTOR_AUTH_ALREADY_ENABLED -> {
                        navigateToTwoFactorSuccess(KycTwoFactorSuccessfullyScreenFragment.STATUS_ENABLED)
                    }
                    INVALID_CODE -> {
                        showDialog(
                            title = getString(R.string.notification_text),
                            message = getString(R.string.invalid_sms_code_text)
                        )
                    }
                }
            }
        })

        viewModel.getTwoFactorDisableResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) { smsTwoFactorResponse ->
                when (smsTwoFactorResponse.status) {
                    SUCCESS -> {
                        navigateToTwoFactorSuccess(KycTwoFactorSuccessfullyScreenFragment.STATUS_DISABLED)
                    }
                    INVALID_CODE -> {
                        showDialog(
                            title = getString(R.string.notification_text),
                            message = getString(R.string.invalid_sms_code_text)
                        )
                    }
                }
            }
        })

        viewModel.getTwoFactorGAEnableResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) { smsTwoFactorResponse ->
                when (smsTwoFactorResponse.status) {
                    SUCCESS, TWO_FACTOR_AUTH_ALREADY_ENABLED -> {
                        navigateToTwoFactorSuccess(KycTwoFactorSuccessfullyScreenFragment.STATUS_ENABLED)
                    }
                    INVALID_CODE -> {
                        showDialog(
                            title = getString(R.string.notification_text),
                            message = getString(R.string.invalid_sms_code_text)
                        )
                    }
                }
            }
        })

        viewModel.getTwoFactorGADisableResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) { smsTwoFactorResponse ->
                when (smsTwoFactorResponse.status) {
                    SUCCESS, TWO_FACTOR_AUTH_ALREADY_ENABLED -> {
                        navigateToTwoFactorSuccess(KycTwoFactorSuccessfullyScreenFragment.STATUS_DISABLED)
                    }
                    INVALID_CODE -> {
                        showDialog(
                            title = getString(R.string.notification_text),
                            message = getString(R.string.invalid_sms_code_text)
                        )
                    }
                }
            }
        })

        viewModel.getTwoFactorLoginResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) {
                context?.let { appContext ->
                    IntentsController.startActivityWithANewStack(
                        appContext,
                        AuthorizedActivity::class.java
                    )
                }
            }
        })

        viewModel.getResendingCodeResult().observe(viewLifecycleOwner, { result ->
            handleResultWithError(result, HasNotAttempts::class.java, {
                showDialog(
                    message = it.message.orDefaultValue(""),
                    positiveButtonMessage = getString(R.string.ok_text),
                    positiveListener = { popBackStack() },
                    negativeListener = { popBackStack() },
                    cancelable = false
                )
            }, {
                restartTimer()
                focusableView.clear()
            })
        })

        viewModel.getResendNewEmailTokenResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) {
                restartTimer()
                focusableView.clear()
            }
        })

        viewModel.getNewEmailVerifyCodeResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) {
                navigate(R.id.action_defaultCodeValidationFragment_to_changeEmailSuccessFragment)
            }
        })

        //Handling the different statuses of investment confirmation response
        viewModel.getConfirmResult().observe(viewLifecycleOwner, { result ->
            handleResultWithError(result, InvestmentConfirmationException::class.java, { error ->
                when (error) {
                    is CancelledInvestmentException -> {
                        showDialog(
                            message = error.message.orDefaultValue(""),
                            positiveButtonMessage = getString(R.string.ok_text),
                            positiveListener = { popBackStack(R.id.projectDetailsFragment) },
                            negativeListener = { popBackStack(R.id.projectDetailsFragment) },
                            neutralListener = { popBackStack(R.id.projectDetailsFragment) },
                            cancelable = false
                        )
                    }
                    is InvestmentConfirmationException -> {
                        showDialog(
                            positiveButtonMessage = getString(R.string.ok_text),
                            message = error.message.orDefaultValue("")
                        )
                    }
                }
            }) {
                validationScreenInfo?.let { info ->
                    if (info.verificationType == INVEST) {
                        viewModel.getProjectInvestmentInfo(projectId, investmentId)
                    }
                }
            }
        })

        viewModel.getProjectInvestmentInfoResult().observe(viewLifecycleOwner, Observer { result ->
            handleResult(result) {
                val (projectResponse, investmentInfo) = it
                investmentInfo.setProjectName(projectResponse.projectPage.companyName)
                val softCap = projectResponse.tokenParametersDocument.softCap
                val softCapProject = softCap != null && softCap != 0L
                if (softCapProject) {
                    navigate(
                        R.id.action_digitCodeValidationFragment_to_softCapPaymentSelectionFragment,
                        SoftCapPaymentSelectionFragment.buildWithAnArguments(
                            investmentId, projectId, investmentInfo)
                    )
                } else {
                    navigate(
                        R.id.action_digitCodeValidationFragment_to_hardCapPaymentSelectionFragment,
                        HardCapPaymentSelectionFragment.buildWithAnArguments(
                            investmentId, projectId, investmentInfo)
                    )
                }
            }
        })

        viewModel.getConfirmVerifyForDeleteAccountResult().observe(viewLifecycleOwner, Observer {
            handleResult(it) {
                navigate(R.id.action_defaultCodeValidationFragment_to_deleteAccountOneLevelConfirmFragment)
            }
        })

        viewModel.getSendEmailLinkResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) {
                showDialog(
                    title = getString(R.string.check_your_inbox_text),
                    message = getString(R.string.please_check_your_email_inbox_to_find_a_link_text),
                    positiveButtonMessage = getString(R.string.ok_text),
                    positiveListener = { popBackStack(R.id.loginFragment) },
                    negativeListener = { popBackStack(R.id.loginFragment) },
                    cancelable = false
                )
            }
        })
        focusableView.setOnFocusChangeListener { _, hasFocus ->
            context?.let {
                if (!hasFocus) {
                    activity?.hideKeyboard()
                }
            }
        }

        handleArguments()
    }

    private fun navigateToTwoFactorSuccess(twoFactorStatus: String) {
        val id = when (navigateFrom) {
            NAVIGATE_FROM_KYC -> {
                R.id.action_defaultCodeValidationFragment_to_kycTwoFactorSuccessfullyScreenFragment
            }
            NAVIGATE_FROM_SECURITY -> {
                R.id.action_defaultCodeValidationFragment_to_kycTwoFactorSuccessfullyScreenFragment
            }
            else -> {
                R.id.action_defaultCodeValidationFragment_to_kycTwoFactorSuccessfullyScreenFragment
            }
        }
        navigate(id,
            KycTwoFactorSuccessfullyScreenFragment.buildWithAnArguments(navigateFrom, twoFactorStatus))
    }

    private fun handleArguments() {
        validationScreenInfo?.let {
            viewModel.setupBinding(it)
            val interactionId = partialUser.interactionId
            val phoneNumber = partialUser.phoneNumber
            val email = partialUser.email

            focusableView.clear()

            when (it.verificationType) {
                PHONE -> setupPhoneFlow(phoneNumber, interactionId) //Registration flow
                LOGIN -> setupLoginFlow(phoneNumber, interactionId)
                INVEST -> setupInvestFlow(interactionId)
                DELETE_ACCOUNT -> setupDeleteAccountFlow(phoneNumber, interactionId)
                CHANGE_EMAIL -> setupChangeEmailFlow(email)
                TWO_AUTH_VALIDATION_SMS_ENABLE -> setupValidationTwoAuthSMSEnable(interactionId)
                TWO_AUTH_VALIDATION_SMS_DISABLE -> setupValidationTwoAuthSMSDisable(interactionId)
                TWO_AUTH_VALIDATION_GA_ENABLE -> setupGAEnable()
                TWO_AUTH_VALIDATION_GA_DISABLE -> setupGADisable()
                TWO_AUTH_CODE_VERIFICATION -> setupGAVerification(interactionId)
                RESTORE_PASSWORD -> setupRestorePasswordFlow(interactionId)
                CHANGE_PHONE_NUMBER -> setupChangePhoneNumberFlow(phoneNumber, interactionId)
                else -> {
                }
            }
        }
    }

    private fun setupChangePhoneNumberFlow(
        phoneNumber: String,
        interactionId: String,
    ) {
        validateButton.setOnClickListener {
            viewModel.changePhoneNumber(phoneNumber, interactionId)
        }
        resendSMSTextView.setOnClickListener {
            viewModel.resendSMSCodeByPhone(interactionId)
        }
    }

    private fun setupRestorePasswordFlow(interactionId: String) {
        resendSMSTextView.setOnClickListener {
            viewModel.resendSMSCodeByPhone(interactionId)
        }
        validateButton.setOnClickListener {
            viewModel.sendEmailLink(interactionId = interactionId)
        }
    }

    private fun setupPhoneFlow(
        phoneNumber: String,
        interactionId: String,
    ) {
        viewModel.visibilityToolbarButton.set(View.VISIBLE)
        additionalToolbarRightButton.setOnClickListener {
            val arguments = DefaultWebViewFragment.buildWithAnArguments(Constants.FAQ_URL)
            navigate(R.id.action_digitCodeValidationFragment_to_defaultWebViewFragment, arguments)
        }
        validateButton.setOnClickListener {
            viewModel.registrationWithVerifyCode(phoneNumber, interactionId)
        }
        resendSMSTextView.setOnClickListener {
            viewModel.resendLimitSMSCodeByPhone(interactionId)
        }
    }

    private fun setupLoginFlow(
        phoneNumber: String,
        interactionId: String,
    ) {

        resendSMSTextView.setOnClickListener {
            viewModel.resendLimitSMSCodeByPhone(interactionId)
        }
        validateButton.setOnClickListener {
            viewModel.loginWithVerificationCode(
                phoneNumber = phoneNumber, interactionId = interactionId
            )
        }
    }

    private fun setupInvestFlow(
        interactionId: String,
    ) {

        resendSMSTextView.setOnClickListener {
            viewModel.resendSMSCodeByPhone(interactionId)
        }

        validateButton.setOnClickListener {
            viewModel.confirmInvestment(
                investmentId, projectId, interactionId
            )
        }
    }

    private fun setupDeleteAccountFlow(
        phoneNumber: String,
        interactionId: String,
    ) {

        resendSMSTextView.setOnClickListener {
            viewModel.resendSMSCodeByPhone(interactionId)
        }

        validateButton.setOnClickListener {
            viewModel.registrationWithVerifyCode(phoneNumber, interactionId)
        }
    }

    private fun setupValidationTwoAuthSMSEnable(interactionId: String) {
        setupBackButtonForTwoAuth()

        resendSMSTextView.setOnClickListener {
            viewModel.resendSMSCodeByPhone(interactionId)
        }
        validateButton.setOnClickListener {
            viewModel.enableTwoFactorAuth(interactionId)
        }
    }

    private fun setupValidationTwoAuthSMSDisable(interactionId: String) {
        setupBackButtonForTwoAuth()

        resendSMSTextView.setOnClickListener {
            viewModel.resendSMSCodeByPhone(interactionId)
        }
        validateButton.setOnClickListener {
            viewModel.disableTwoFactorAuth(interactionId)
        }
    }

    private fun setupGAEnable() {
        setupBackButtonForTwoAuth()

        resendSMSTextView.visibility = View.GONE
        messageTextView.visibility = View.GONE
        validateButton.setOnClickListener {
            viewModel.enableGATwoFactorAuth()
        }
    }

    private fun setupGADisable() {
        setupBackButtonForTwoAuth()

        resendSMSTextView.visibility = View.GONE
        messageTextView.visibility = View.GONE
        validateButton.setOnClickListener {
            viewModel.disableATwoFactorAuth()
        }
    }

    private fun setupGAVerification(interactionId: String) {
        setupBackButtonForTwoAuth()
        timer.stopTimer()
        messageTextView.visibility = View.GONE
        resendSMSTextView.visibility = View.GONE
        validateButton.setOnClickListener {
            viewModel.twoFactorLogin(interactionId)
        }
    }

    private fun setupChangeEmailFlow(email: String) {

        resendSMSTextView.setOnClickListener {
            viewModel.resendNewEmailVerifyToken(email)
        }

        validateButton.setOnClickListener {
            viewModel.checkNewEmailWithVerifyCode()
        }
    }

    private fun navigateToNextScreen() {
        when (partialUser.group) {
            PRIVATE_INVESTOR -> {
                val arguments = PVIPasswordFragment.buildWithAnArguments(partialUser)
                navigate(R.id.action_digitCodeValidationFragment_to_PVIPasswordFragment, arguments)
            }
            INSTITUTIONAL_INVESTOR -> {
                val arguments = IIPasswordFragment.buildWithAnArguments(partialUser)
                navigate(R.id.action_digitCodeValidationFragment_to_IIPasswordFragment, arguments)
            }
            NO_TYPE -> {
                //TODO navigate from anyone
            }
        }
    }

    private fun setupBackButtonForTwoAuth() {
        val backFragmentId = when (navigateFrom) {
            NAVIGATE_FROM_KYC -> {
                R.id.chooseTwoAuthMethodFragment
            }
            NAVIGATE_FROM_SECURITY -> {
                R.id.chooseTwoAuthMethodFragment
            }
            else -> {
                R.id.loginFragment
            }
        }

        toolbarBackButton.apply {
            setImageResource(R.drawable.icon_cross)
            setOnClickListener { popBackStack(backFragmentId) }
        }

        registerOnBackPressedListener { popBackStack(backFragmentId) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_SMS_CONSENT_REQUEST ->
                if (resultCode == Activity.RESULT_OK && data != null) {
                    val message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE).orDefaultValue("")
                    context?.copyToClipboard(message.findActivationCode())
                }
        }
    }

    override fun onResume() {
        super.onResume()
        showKeyboard()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        timer.timerListener = null
    }

    override fun onDestroy() {
        timer.stopTimer()
        context?.unregisterSMSBroadcastReceiver(smsVerificationReceiver)
        activity?.hideKeyboard()
        super.onDestroy()
    }

    private inner class OnClickListener : View.OnClickListener {
        override fun onClick(v: View?) {
            showKeyboard()
        }
    }

    private fun showKeyboard() {
        focusableView.focus()
    }

    private inner class TimerListener : Timer.TimerListener {

        override fun countProcessing(value: Int) {
            if (isAliveAndAvailable()) {
                updateControlsState()
                viewModel.setupSMSMessage(value)
            }
        }

        override fun countFinished() {
            updateControlsState(true)
        }
    }

    private fun startTimer() {
        val status = validationScreenInfo?.verificationType.orDefaultValue("")
        val twoFAStatuses = listOf(TWO_AUTH_CODE_VERIFICATION,
            TWO_AUTH_VALIDATION_GA_ENABLE,
            TWO_AUTH_VALIDATION_GA_DISABLE)
        val possibleToStartTimer = !twoFAStatuses.contains(status)
        if (possibleToStartTimer)
            timer.startTimer(30)
    }

    private fun restartTimer() {
        startTimer()
    }

    private fun updateControlsState(toEnabled: Boolean = false) {
        if (isAliveAndAvailable()) {
            resendSMSTextView.isEnabled = toEnabled
            messageTextView.changeVisibility(!toEnabled)
        }
    }

    private fun setupInputFieldsListener(listener: OnClickListener = OnClickListener()) {
        firstSMSInputField.setOnClickListener(listener)
        secondSMSInputField.setOnClickListener(listener)
        thirdSMSInputField.setOnClickListener(listener)
        fourthSMSInputField.setOnClickListener(listener)
        fifthSMSInputField.setOnClickListener(listener)
        sixthSMSInputField.setOnClickListener(listener)
    }

    private fun createValidationScreenInfo() = ValidationScreenInfoDTO(
        verificationType = TWO_AUTH_CODE_VERIFICATION,
        validationButtonName = getString(R.string.confirm_text),
        header = getString(R.string.enter_code_from_ga),
        toolbarTitle = getString(R.string.log_in_title_text),
        subTitleMessage = getString(R.string.enter_code_from_ga_description)
    )
}