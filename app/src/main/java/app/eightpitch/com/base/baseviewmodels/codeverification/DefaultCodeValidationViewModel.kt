package app.eightpitch.com.base.baseviewmodels.codeverification

import android.content.Context
import android.view.View
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.auth.LoginResponseBody
import app.eightpitch.com.data.dto.investment.ProjectInvestmentInfoResponse
import app.eightpitch.com.data.dto.projectdetails.GetProjectResponse
import app.eightpitch.com.data.dto.sms.EmailPhoneValidationResponse
import app.eightpitch.com.data.dto.sms.PhoneVerificationTokenRequestBody
import app.eightpitch.com.data.dto.twofactor.SmsTwoFactorResponse
import app.eightpitch.com.data.dto.twofactor.TwoFactorVerifyCodeResponse
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.extensions.wrapWithAnEmptyResult
import app.eightpitch.com.models.*
import app.eightpitch.com.utils.Empty
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.coroutines.launch
import kotlin.properties.Delegates

/**
 * It works with input code verification
 */
open class DefaultCodeValidationViewModel(
    private val appContext: Context,
    private val smsModel: SMSModel,
    private val registrationModel: RegistrationModel,
    private val authModel: AuthModel,
    private val investmentModel: InvestmentModel,
    private val projectModel: ProjectModel,
    private val userModel: UserModel,
    private val supportModel: SupportModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    companion object {
        internal const val MAX_CODE_LENGTH = 6
    }

    val buttonAvailability = ObservableBoolean()
    var visibilityToolbarButton = ObservableInt(View.GONE)

    val userSMSMessage = ObservableField("")

    /**
     * Variables for binding all view on screens with code verification (not related to input)
     */
    val header = ObservableField("")
    val toolbarTitle = ObservableField("")
    val subTitleMessage = ObservableField("")
    val validateButtonName = ObservableField("")
    val resendButtonName = ObservableField(appContext.getString(R.string.resend_confirmation_sms_text))

    val firstObservableField = ObservableField<String>()
    val secondObservableField = ObservableField<String>()
    val thirdObservableField = ObservableField<String>()
    val fourthObservableField = ObservableField<String>()
    val fifthObservableField = ObservableField<String>()
    val sixthObservableField = ObservableField<String>()

    var smsInput by Delegates.observable("") { _, _, newValue ->
        inputChanged(newValue)
    }

    private val observableFields = arrayListOf(
        firstObservableField, secondObservableField,
        thirdObservableField, fourthObservableField,
        fifthObservableField, sixthObservableField
    )

    private val registrationVerifyCodeResult = SingleActionLiveData<Result<Empty>>()
    fun getRegistrationVerifyCodeResult(): LiveData<Result<Empty>> = registrationVerifyCodeResult

    internal fun registrationWithVerifyCode(
        phoneNumber: String,
        interactionId: String
    ) {
        viewModelScope.launch {
            val result = asyncLoading {
                registrationModel.registrationWithVerifyCode(phoneNumber, interactionId, smsInput)
            }
            registrationVerifyCodeResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    private val changePhoneNumberResult = SingleActionLiveData<Result<Empty>>()
    fun getChangePhoneNumberResult(): LiveData<Result<Empty>> = changePhoneNumberResult

    internal fun changePhoneNumber(
        phoneNumber: String,
        interactionId: String
    ) {
        viewModelScope.launch {
            val result = asyncLoading {
                smsModel.changePhoneNumber(interactionId, phoneNumber, smsInput)
            }
            changePhoneNumberResult.postAction(result)
        }
    }

    private val sendEmailLinkResult = SingleActionLiveData<Result<Empty>>()
    fun getSendEmailLinkResult(): LiveData<Result<Empty>> = sendEmailLinkResult

    internal fun sendEmailLink(interactionId: String) {
        viewModelScope.launch {
            val result = asyncLoading {
                smsModel.sendEmailLink(interactionId = interactionId, code = smsInput)
            }
            sendEmailLinkResult.postAction(result)
        }
    }

    private val loginWithCodeResult = SingleActionLiveData<Result<LoginResponseBody>>()
    fun getLoginWithCodeResult(): LiveData<Result<LoginResponseBody>> = loginWithCodeResult

    internal fun loginWithVerificationCode(phoneNumber: String, interactionId: String) {
        viewModelScope.launch {
            val result = asyncLoading {
                authModel.loginWithSMSCode(smsInput, phoneNumber, interactionId)
            }
            loginWithCodeResult.postAction(result)
        }
    }

    private val resendingCodeResult = SingleActionLiveData<Result<Empty>>()
    fun getResendingCodeResult(): LiveData<Result<Empty>> = resendingCodeResult

    internal fun resendSMSCodeByPhone(
        interactionId: String
    ) {
        viewModelScope.launch {
            val result = asyncLoading {
                smsModel.resendSMSCodeByPhone(interactionId)
            }
            resendingCodeResult.postAction(result)
        }
    }

    internal fun resendLimitSMSCodeByPhone(
        interactionId: String
    ) {
        viewModelScope.launch {
            val result = asyncLoading {
                smsModel.resendLimitSMSCodeByPhone(interactionId)
            }
            resendingCodeResult.postAction(result)
        }
    }

    private val confirmResult = SingleActionLiveData<Result<GetProjectResponse>>()
    internal fun getConfirmResult(): LiveData<Result<GetProjectResponse>> = confirmResult

    internal fun confirmInvestment(
        investmentId: String,
        projectId: String,
        interactionId: String
    ) {
        viewModelScope.launch {
            val result = asyncLoading {
                investmentModel.confirmInvestment(
                    investmentId,
                    projectId,
                    PhoneVerificationTokenRequestBody(
                        interactionId = interactionId,
                        code = smsInput
                    )
                )
                projectModel.getProjectInfo(projectId)
            }
            confirmResult.postAction(result)
        }
    }

    private fun inputChanged(input: String) {

        if (input.isEmpty()) {
            observableFields.forEach { it.set("") }
        } else {

            for (i in 0 until MAX_CODE_LENGTH) {

                val observableField = observableFields[i]

                if (input.length <= i) {
                    observableField.set("")
                } else
                    observableField.set(input[i].toString())
            }
        }
        buttonAvailability.set(input.length == MAX_CODE_LENGTH)
    }

    /**
     * Filling screen from bundle of calling fragment
     */
    internal fun setupBinding(validationCodeDTO: ValidationScreenInfoDTO) {
        validationCodeDTO.apply {
            this@DefaultCodeValidationViewModel.also {
                it.header.set(header)
                it.toolbarTitle.set(toolbarTitle)
                it.subTitleMessage.set(subTitleMessage)
                it.validateButtonName.set(validationButtonName)
                if (resendButtonName.isNotEmpty()) {
                    it.resendButtonName.set(resendButtonName)
                }
            }
        }
    }

    private val projectInvestmentInfoResult =
        SingleActionLiveData<Result<Pair<GetProjectResponse, ProjectInvestmentInfoResponse>>>()

    internal fun getProjectInvestmentInfoResult(): LiveData<Result<Pair<GetProjectResponse, ProjectInvestmentInfoResponse>>> =
        projectInvestmentInfoResult

    internal fun getProjectInvestmentInfo(
        projectId: String,
        investmentId: String
    ) {
        viewModelScope.launch {
            val result = asyncLoading {
                val project = projectModel.getProjectInfo(projectId)
                val investmentInfo = investmentModel.getProjectInvestmentInfo(projectId)
                val investmentAmount = investmentModel.getInvestmentAmount(investmentId)
                project to investmentInfo.copy(assetBasedFees = investmentAmount.fee)
            }
            projectInvestmentInfoResult.postAction(result)
        }
    }

    internal fun setupSMSMessage(waitingTimeInSeconds: Int) {
        userSMSMessage.set(appContext.getString(R.string.asking_for_code_message_pattern, waitingTimeInSeconds))
    }

    private val confirmVerifyForDeleteAccount = SingleActionLiveData<Result<Empty>>()
    internal fun getConfirmVerifyForDeleteAccountResult(): LiveData<Result<Empty>> = confirmVerifyForDeleteAccount

    internal fun deleteAccount(
        deletingReason: String
    ) {
        viewModelScope.launch {
            val result = asyncLoading {
                userModel.deleteAccount(deletingReason)
            }
            confirmVerifyForDeleteAccount.postAction(result)
        }
    }

    private val newEmailVerifyCodeResult = SingleActionLiveData<Result<EmailPhoneValidationResponse>>()
    fun getNewEmailVerifyCodeResult(): LiveData<Result<EmailPhoneValidationResponse>> =
        newEmailVerifyCodeResult

    internal fun checkNewEmailWithVerifyCode() {
        viewModelScope.launch {

            val result = asyncLoading {
                supportModel.verifyEmailCode(smsInput)
            }
            newEmailVerifyCodeResult.postAction(result)
        }
    }

    private val resendNewEmailTokenResult = SingleActionLiveData<Result<Empty>>()
    fun getResendNewEmailTokenResult(): LiveData<Result<Empty>> = resendNewEmailTokenResult

    internal fun resendNewEmailVerifyToken(email: String) {
        viewModelScope.launch {
            val result = asyncLoading {
                smsModel.requireCodeByNewEmail(email)
            }
            resendNewEmailTokenResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    private val twoFactorEnabledResult = SingleActionLiveData<Result<SmsTwoFactorResponse>>()
    fun getTwoFactorEnabledResult(): LiveData<Result<SmsTwoFactorResponse>> = twoFactorEnabledResult

    internal fun enableTwoFactorAuth(interactionId: String) {
        viewModelScope.launch {
            val result = asyncLoading {
                val user = userModel.getUserData()
                authModel.smsTwoFactorEnable(user.role, user.externalId, interactionId, smsInput)
            }
            twoFactorEnabledResult.postAction(result)
        }
    }

    private val twoFactorDisabledResult = SingleActionLiveData<Result<SmsTwoFactorResponse>>()
    fun getTwoFactorDisableResult(): LiveData<Result<SmsTwoFactorResponse>> = twoFactorDisabledResult

    internal fun disableTwoFactorAuth(interactionId: String) {
        viewModelScope.launch {
            val result = asyncLoading {
                val user = userModel.getUserData()
                authModel.smsTwoFactorDisable(user.role, user.externalId, interactionId, smsInput)
            }
            twoFactorDisabledResult.postAction(result)
        }
    }

    private val twoFactorVerifyCodeResult = SingleActionLiveData<Result<TwoFactorVerifyCodeResponse>>()
    fun getTwoFactorLoginResult(): LiveData<Result<TwoFactorVerifyCodeResponse>> = twoFactorVerifyCodeResult

    internal fun twoFactorLogin(interactionId: String) {
        viewModelScope.launch {
            val result = asyncLoading {
                authModel.twoFactorVerifyCode(code = smsInput, interactionId = interactionId)
            }
            twoFactorVerifyCodeResult.postAction(result)
        }
    }

    private val twoFactorGAEnableResult = SingleActionLiveData<Result<SmsTwoFactorResponse>>()
    fun getTwoFactorGAEnableResult(): LiveData<Result<SmsTwoFactorResponse>> = twoFactorGAEnableResult

    internal fun enableGATwoFactorAuth() {
        viewModelScope.launch {
            val result = asyncLoading {
                authModel.twoFactorAuthEnable(twoFactorAuthCode = smsInput)
            }
            twoFactorGAEnableResult.postAction(result)
        }
    }

    private val twoFactorGADisableResult = SingleActionLiveData<Result<SmsTwoFactorResponse>>()
    fun getTwoFactorGADisableResult(): LiveData<Result<SmsTwoFactorResponse>> = twoFactorGADisableResult

    internal fun disableATwoFactorAuth() {
        viewModelScope.launch {
            val result = asyncLoading {
                authModel.twoFactorAuthDisable(twoFactorAuthCode = smsInput)
            }
            twoFactorGADisableResult.postAction(result)
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        smsInput = ""
        header.set("")
        toolbarTitle.set("")
        subTitleMessage.set("")
        validateButtonName.set("")
        buttonAvailability.set(false)
        visibilityToolbarButton.set(View.GONE)
        observableFields.forEach { it.set("") }
        resendButtonName.set(appContext.getString(R.string.resend_confirmation_sms_text))
    }
}
