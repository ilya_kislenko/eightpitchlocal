package app.eightpitch.com.base

interface TitleProvider {
    fun getTitle() : String?
}