package app.eightpitch.com.base

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.*
import app.eightpitch.com.R
import app.eightpitch.com.dagger.Injector
import app.eightpitch.com.extensions.hideKeyboard
import javax.inject.Inject

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory:  ViewModelProvider.Factory

    private val fragmentBackStackChangedListener = FragmentManager.OnBackStackChangedListener {
        onFragmentBackStackChanged()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Injector.buildModelsComponent().injectIntoActivity(this)
        super.onCreate(savedInstanceState)
        supportFragmentManager.addOnBackStackChangedListener(fragmentBackStackChangedListener)
    }

    private fun onFragmentBackStackChanged() {

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment)

        val title = when (val lastFragment = navHostFragment?.childFragmentManager?.fragments?.last()) {
            is TitleProvider -> lastFragment.getTitle()
            else -> null
        }

        title?.let {
            setTitle(it)
        }

        hideKeyboard()
    }

    override fun setTitle(title: CharSequence?) {
        super.setTitle(title)
        findViewById<AppCompatTextView>(R.id.headerTitle).text = title
    }
}