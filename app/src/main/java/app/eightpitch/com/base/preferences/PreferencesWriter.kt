package app.eightpitch.com.base.preferences

import android.content.*
import app.eightpitch.com.data.dto.biometric.UserBiometricInfo
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.data.dto.user.User.CREATOR.RoleType
import com.google.gson.Gson
import java.util.concurrent.TimeUnit

class PreferencesWriter(appContext: Context) : PreferencesProvider(appContext) {

    private val editor = sharedPreferences.edit()

    fun saveMACAddress(macAddress: String) = editor.performWithCommit {
        putString(MAC_ADDRESS_KEY, macAddress)
    }

    fun cacheAccessToken(accessToken: String) = editor.performWithCommit {
        putString(ACCESS_TOKEN_KEY, accessToken)
    }

    fun cacheRefreshToken(refreshToken: String) = editor.performWithCommit {
        putString(REFRESH_TOKEN_KEY, refreshToken)
    }

    fun cacheTokenExpirationTime(expirationTimeMinutes: Long) = editor.performWithCommit {
        putLong(TOKEN_EXPIRATION_KEY, System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(expirationTimeMinutes))
    }

    fun cacheUserRole(@RoleType userRole: String) = editor.performWithCommit {
        putString(USER_ROLE, userRole)
    }

    fun cacheExternalId(externalId: String) = editor.performWithCommit {
        putString(USER_EXTERNAL_ID, externalId)
    }

    fun cacheQuickLoginDialogState(shouldShow: Boolean) = editor.performWithCommit {
        putBoolean(SHOW_QUICK_LOGIN_DIALOG_KEY, shouldShow)
    }

    fun cacheUserBiometricInfo(externalId: String, user: UserBiometricInfo) = editor.performWithCommit {
        if (user.hasPinCode) {
            val json = Gson().toJson(user)
            putString(externalId, json)
        }
    }

    fun clearBiometricInfo(externalId: String) = editor.performWithCommit {
        remove(externalId)
        clearIsDialogShowed()
    }

    private fun clearIsDialogShowed() = editor.performWithCommit {
        remove(SHOW_QUICK_LOGIN_DIALOG_KEY)
    }

    fun clearLoginCredentials() {
        clearIsDialogShowed()
        cacheExternalId("")
        cacheAccessToken("")
        cacheRefreshToken("")
        cacheTokenExpirationTime(0)
    }

    private inline fun <T : SharedPreferences.Editor> T.performWithCommit(putFunc: T.() -> Unit) {
        putFunc()
        commit()
    }
}