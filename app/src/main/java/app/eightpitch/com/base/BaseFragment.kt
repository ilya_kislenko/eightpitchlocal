package app.eightpitch.com.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.annotation.IdRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import app.eightpitch.com.BR
import app.eightpitch.com.R
import app.eightpitch.com.extensions.*

abstract class BaseFragment<VM : BaseViewModel> : BackPressureFragment(), TitleProvider {

    protected lateinit var viewModel: VM
    protected lateinit var binding: ViewDataBinding

    protected abstract fun getLayoutID(): Int
    protected abstract fun getVMClass(): Class<VM>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, getLayoutID(), container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = injectViewModel(getVMClass())
        binding.setVariable(BR.viewModel, viewModel)
    }

    protected fun hasViewModel() = ::viewModel.isInitialized

    override fun onDestroyView() {
        if (::viewModel.isInitialized)
            viewModel.cleanUp()
        super.onDestroyView()
    }

    override fun onDestroy() {
        if (::viewModel.isInitialized)
            viewModel.cleanUpFinally()
        super.onDestroy()
    }

    protected fun bindLoadingIndicator() {

        viewModel.getLoadingState().observe(viewLifecycleOwner, Observer { isLoading ->

            activity?.runOnUiThread {

                if (isLoading)
                    blockUI()
                else
                    unblockUI()
            }
        })
    }

    protected open fun blockUI() {
        view?.findViewById<FrameLayout>(R.id.curtainView)?.show()
    }

    protected open fun unblockUI() {
        view?.findViewById<FrameLayout>(R.id.curtainView)?.hide()
    }

    protected fun navigate(@IdRes actionID: Int) {

        if (isAliveAndAvailable())
            view?.let {
                Navigation.findNavController(it).navigateSafe(actionID)
                activity?.hideKeyboard()
            }
    }

    protected fun navigate(@IdRes actionID: Int, bundle: Bundle) {

        if (isAliveAndAvailable())
            view?.let {
                Navigation.findNavController(it).navigateSafe(actionID, bundle)
                activity?.hideKeyboard()
            }
    }

    /**
     * You can inherit this method to be notified when swipe to refresh action was called
     */
    open protected fun onRefreshCalled() {}

    /**
     * Return the "target" for this fragment of specified type. By default target is activity that owns
     * current fragment but also could be any fragment.
     *
     * @param clazz requested callback interface
     * @return requested callback or null if no callback of requested type is found
     */
    protected fun <T> getTarget(clazz: Class<T>): T? {

        val targets = arrayOf(targetFragment, parentFragment, activity)

        targets.forEach {
            if (null != it && clazz.isInstance(it)) {
                return clazz.cast(it)
            }
        }
        return null
    }

    override fun getTitle(): String? = null
}