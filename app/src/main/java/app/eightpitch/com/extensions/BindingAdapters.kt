package app.eightpitch.com.extensions

import android.annotation.SuppressLint
import android.graphics.drawable.GradientDrawable
import android.view.View
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatRadioButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.SwitchCompat
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.databinding.*
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.R
import app.eightpitch.com.ui.recycleradapters.BindableAdapter
import app.eightpitch.com.ui.recycleradapters.BindableFilterAdapter
import app.eightpitch.com.utils.Logger
import app.eightpitch.com.utils.Logger.TAG_GENERIC_ERROR
import app.eightpitch.com.views.CenteredInputField
import app.eightpitch.com.views.DefaultToggleButton
import app.eightpitch.com.views.HintSpinnerView
import app.eightpitch.com.views.InputField
import app.eightpitch.com.views.ToggleTextView
import app.eightpitch.com.views.interfaces.ShortFieldValidator
import com.google.android.material.tabs.TabLayout
import java.math.BigDecimal

@BindingAdapter("textWatcher")
fun InputField.setTextWatcher(fieldValidator: ShortFieldValidator) {
    this.fieldValidator = fieldValidator
}

@BindingAdapter("errorHolder")
fun InputField.errorHolder(errorHolderValue: ObservableField<String>) {

    errorHolderValue.onChanged { value ->

        if (value.isNotEmpty())
            showError(value)
    }
}

@BindingAdapter("errorHolder")
fun HintSpinnerView.errorHolder(errorHolderValue: ObservableField<String>) {

    errorHolderValue.onChanged { value ->

        if (value.isNotEmpty())
            setError(value)
    }
}

@BindingAdapter("textWatcher")
fun CenteredInputField.setTextWatcher(fieldValidator: ShortFieldValidator) {
    this.fieldValidator = fieldValidator
}

@BindingAdapter("errorHolder")
fun CenteredInputField.errorHolder(errorHolderValue: ObservableField<String>) {

    errorHolderValue.onChanged { value ->
        if (value.isNotEmpty())
            showError(value)
    }
}

@BindingAdapter("isActive")
fun ToggleTextView.isActive(isActive: ObservableField<Boolean>) {
    this.isActivated(isActive.get() ?: false)
}

@BindingAdapter("changeBackgroundBy")
fun <T> ConstraintLayout.changeBackgroundBy(items: ObservableArrayList<T>){
    if(items.isEmpty())
        setBackgroundResource(R.drawable.custom_check_disable_background)
    else
        setBackgroundResource(R.drawable.gray_bottom_corner_background)
}

@BindingAdapter("selectTabBy")
fun TabLayout.selectTabBy(position: ObservableField<Int>) {

    position.get()?.let {
        if (this.tabCount <= it)
            selectTabWith(it)
    }
}

@BindingAdapter("available")
fun InputField.available(available: ObservableBoolean) {
    val availability = available.get()
    setInputAvailability(availability)
    if (availability) showClearButton() else hideClearButton()
}

@BindingAdapter("available")
fun AppCompatRadioButton.setAvailability(available: Boolean) {
    isChecked = available
    isEnabled = available
}

@BindingAdapter("isChecked")
fun DefaultToggleButton.isChecked(available: ObservableBoolean) {
    val availability = available.get()
    changeSelectionState(availability)
}

@BindingAdapter("drawableLeftResource")
fun AppCompatTextView.setDrawableLeftResource(resourceRef: ObservableInt) {
    try {
        val drawable = context.getDrawable(resourceRef.get())
        setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null)
    } catch (didNotFindAResource: Exception) {
        Logger.logError(
            javaClass::class.java.simpleName,
            "Can't parse ${resourceRef.get()} reference as Drawable resource"
        )
    }
}

@BindingAdapter("onCheckedChangeListener")
fun SwitchCompat.onCheckedChangeListener(block: (Boolean) -> Unit) {
    setOnCheckedChangeListener { _, isChecked ->
        block(isChecked)
    }
}

@BindingAdapter("errorHolder")
fun AppCompatTextView.errorHolder(errorHolderValue: ObservableField<String>) {

    errorHolderValue.onChanged { value ->

        if (value.isNotEmpty()) {
            show()
            text = value
        }
    }
}

@BindingAdapter("visibleIfEmpty")
fun <T> View.visibleIfEmpty(items: ObservableArrayList<T>) {
    visibility = if (items.isEmpty()) View.VISIBLE else View.GONE
}

@BindingAdapter("visibleIfHasItems")
fun <T> View.visibleIfHasItems(items: ObservableArrayList<T>) {
    visibility = if (items.isEmpty()) View.GONE else View.VISIBLE
}

@BindingAdapter("changeVisibility")
fun View.changeVisibility(toVisible: ObservableBoolean) {
    isVisible = toVisible.get()
}

@BindingAdapter("changeVisibility")
fun View.changeVisibility(toVisible: ObservableField<Boolean>) {
    toVisible.get()?.let {
        isVisible = it
    }
}

@BindingAdapter("textUpdatesFrom")
fun TextView.textUpdatesFrom(toText: ObservableField<String>) {
    text = toText.get().orDefaultValue(text)
}

@BindingAdapter("hideIfPositive")
fun <T> View.hideIfPositive(positive: ObservableBoolean) {
    visibility = if (positive.get()) View.GONE else View.VISIBLE
}

@BindingAdapter("enabled")
fun View.enabled(toEnabled: ObservableBoolean) {
    isEnabled = toEnabled.get()
}

@BindingAdapter("changeVisibilityIfLessThanOne")
fun View.changeVisibilityIfLessThanOne(itemsAmount: Int) {
    visibility = if (itemsAmount < 1) View.GONE else View.VISIBLE
}

@BindingAdapter("changeAvailability")
fun <T> View.changeAvailability(items: ObservableArrayList<T>) {
    isEnabled = !items.isEmpty()
}

@BindingAdapter("visibility")
fun <T> View.visibility(toVisible: ObservableBoolean) {
    isVisible = toVisible.get()
}

@BindingAdapter("data")
fun <DataType : Any> RecyclerView.data(data: ObservableArrayList<DataType>) {
    (adapter as? BindableAdapter<DataType>)?.setData(data)
}

@BindingAdapter("filterData")
fun <DataType : Any> RecyclerView.filterData(data: ObservableArrayList<DataType>) {
    (adapter as? BindableFilterAdapter<DataType>)?.setFilterData(data)
}


@BindingAdapter("positive")
fun DefaultToggleButton.changeState(state: ObservableBoolean) {
    changeSelectionState(state.get())
}

@BindingAdapter("euroValue")
fun AppCompatTextView.setEuroValue(value: ObservableField<BigDecimal>) {
    val name = value.get()?.formatToCurrencyView() + " " + context.getString(R.string.eur_text)
    text = name
}

/**
 * It's a name of virtual tokens not currency
 */
@BindingAdapter(value = ["tokenValue", "tokenName"], requireAll = true)
fun AppCompatTextView.setTokenValue(value: ObservableField<BigDecimal>, tokenName: String) {
    val name = value.get()?.formatToCurrencyView() + " " + tokenName
    text = name
}

@SuppressLint("ResourceType")
@BindingAdapter(value = ["isActive", "activeColor", "disableColor"], requireAll = true)
fun AppCompatImageView.changeColor(
    isActive: ObservableField<Boolean>,
    @ColorRes activeColor: Int,
    @ColorRes disableColor: Int,
) {
    try {
        isActive.get()?.let { active ->
            (drawable as? GradientDrawable)?.apply {
                mutate()
                color = ContextCompat.getColorStateList(
                    context,
                    if (active) activeColor else disableColor
                )
            }
        }
    } catch (e: Exception) {
        Logger.logError(TAG_GENERIC_ERROR, e.message.orDefaultValue("Error cannot change color"))
    }
}

@BindingAdapter("switched")
fun SwitchCompat.isSwitched(switch: ObservableBoolean) {
    isChecked = switch.get()
}

@BindingAdapter("goneIfEmpty")
fun View.goneIfEmpty(string: ObservableField<String>) {
    visibility = if (string.get().orDefaultValue("").isEmpty()) View.GONE else View.VISIBLE
}