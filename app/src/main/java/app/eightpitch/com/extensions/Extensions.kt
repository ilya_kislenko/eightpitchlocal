package app.eightpitch.com.extensions

import android.content.*
import android.content.Context.WINDOW_SERVICE
import android.graphics.Point
import android.graphics.Rect
import android.os.Build
import android.text.*
import android.text.style.ClickableSpan
import android.view.View
import android.view.ViewTreeObserver
import android.view.WindowManager
import androidx.annotation.RequiresApi
import androidx.annotation.StringRes
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.NavBackStackEntry
import androidx.viewpager2.widget.ViewPager2
import app.eightpitch.com.R
import app.eightpitch.com.data.dto.dynamicviews.TextLinkItem
import app.eightpitch.com.data.dto.dynamicviews.UIElementArguments
import app.eightpitch.com.data.dto.dynamicviews.language.I18nData
import app.eightpitch.com.data.dto.projectdetails.ProjectsFile
import app.eightpitch.com.data.dto.projectdetails.ProjectsFile.CREATOR.FileType
import app.eightpitch.com.data.dto.user.LanguageRequestBody
import app.eightpitch.com.data.dto.user.LanguageRequestBody.Companion.EN
import app.eightpitch.com.base.preferences.PreferencesReader
import app.eightpitch.com.base.preferences.PreferencesWriter
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.utils.Constants.DD_MMMM_YYYY
import app.eightpitch.com.utils.DateUtils.formatByPattern
import app.eightpitch.com.utils.Empty
import app.eightpitch.com.utils.Logger
import app.eightpitch.com.utils.Logger.TAG_GENERIC_ERROR
import app.eightpitch.com.utils.RegexPatterns
import app.eightpitch.com.utils.biometrics.BiometricsUtil
import com.google.android.gms.auth.api.phone.SmsRetriever.SEND_PERMISSION
import com.google.android.gms.auth.api.phone.SmsRetriever.SMS_RETRIEVED_ACTION
import com.google.android.material.tabs.TabLayout
import java.lang.ref.WeakReference
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern
import kotlin.Exception
import kotlin.reflect.KClass

const val OTHER = "OTHER"

//Should be used as default value
const val NO_TYPE = ""

fun Array<String>.matchPartially(text: String): Boolean {

    forEach {
        if (text.contains(it))
            return true
    }

    return false
}

fun StringBuilder.builder(block: StringBuilder.() -> StringBuilder): String {
    block()
    return toString()
}

fun String.matchPattern(pattern: String): Boolean {
    return Pattern.compile(pattern).matcher(this).find()
}

fun Char.matchPattern(pattern: String): Boolean {
    return Pattern.compile(pattern).matcher(this.toString()).find()
}

fun String.toHtml(): Spanned = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
    Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY)
} else {
    Html.fromHtml(this)
}

/**
 * This function should be removed as soon as we will have a
 * different country codes in the UI layer
 */
fun String.appendPrefixToPhone(prefix: String) = "$prefix$this"

/**
 * @param string string for validation
 * @param fieldName @StringRes string name
 * @param prefixPattern pattern for match on first character
 * @param validPattern pattern for match on valid string
 */
fun String.validateByDefaultPattern(
    context: Context,
    @StringRes fieldName: Int,
    validPattern: String,
    prefixPattern: String = RegexPatterns.PATTERN_PREFIX_CORRECT
): String {
    if (isEmpty())
        return getErrorEmptyString(context)

    val firstLetterCondition = prefixPattern.isNotEmpty() && !first().matchPattern(prefixPattern)
    val validCondition = validPattern.isNotEmpty() && !matchPattern(validPattern)
    return when {
        firstLetterCondition -> getErrorInvalidFirstChar(context, fieldName)
        validCondition -> getErrorInvalidString(context, fieldName)
        else -> ""
    }
}

fun getErrorEmptyString(appContext: Context, @StringRes errorTextRes: Int) =
    appContext.getString(R.string.vld1_error_text, appContext.getString(errorTextRes))

fun getErrorEmptyString(appContext: Context) =
    appContext.getString(R.string.vld2_error_text)

fun getErrorInvalidString(appContext: Context, @StringRes errorTextRes: Int) =
    appContext.getString(R.string.not_valid_name, appContext.getString(errorTextRes))

fun getErrorInvalidFirstChar(appContext: Context, @StringRes errorTextRes: Int) =
    appContext.getString(R.string.not_valid_first_letter, appContext.getString(errorTextRes))

/**
 * Could be useful when the result of the response should be
 * kept inside ViewModel and affect of a ViewModel state somehow,
 * but for an endpoint subscriber of LiveData (Fragment) only important to know
 * whether the result was successful or not, and handle exceptions if they were
 */
fun <T> Result<T>.wrapWithAnEmptyResult(): Result<Empty> {
    return Result(this.status, if (null != this.result) Empty.instance else null, this.exception)
}

fun <Type, Wrap> Result<Type>.wrapWithAnotherResult(result: Wrap): Result<Wrap> {
    return Result(this.status, if (null != this.result) result else null, this.exception)
}

fun <T : Any> T?.orDefaultValue(defaultValue: T) = this ?: defaultValue

/**
 * Method that will format a [Long] presentation of the currency
 * to a user acceptable view, like: 100000 -> 100.000
 *
 * @return formatted [String] or an empty [String] if parsing isn't
 * possible
 */
fun <T : Number> T.formatToCurrencyView(): String {
    return try {
        val tokenFormatter = DecimalFormat().apply {
            this.decimalFormatSymbols = DecimalFormatSymbols().apply {
                decimalSeparator = ','
                groupingSeparator = '.'
            }
            groupingSize = 3
        }

        tokenFormatter.format(this)
    } catch (exception: ArithmeticException) {
        Logger.logError(
            TAG_GENERIC_ERROR,
            "Cannot parse $this by a currency format, reason: ",
            exception
        )
        ""
    }
}


/**
 * Method that will parse the String to a BigDecimal
 * but safe, if parsing isn't possible method will return a
 * BigDecimal(0) and log an exception
 */
fun String.parseToBigDecimalSafe(): BigDecimal = try {
    toBigDecimal()
} catch (exception: NumberFormatException) {
    Logger.logError(TAG_GENERIC_ERROR, "Cannot cast $this to BigDecimal, reason: ", exception)
    BigDecimal(0)
}


fun <T> List<T>.toArrayList() = toCollection(arrayListOf())

fun BigDecimal.divideSafe(divider: BigDecimal, roundingMode: RoundingMode = RoundingMode.DOWN): BigDecimal = try {
    divide(divider, roundingMode)
} catch (exception: Exception) {
    Logger.logError(TAG_GENERIC_ERROR, "Cannot divide $this to $divider, reason: ", exception)
    this
}

fun Number.toStringSafe() = try {
    toString()
} catch (exception: NumberFormatException) {
    Logger.logError(TAG_GENERIC_ERROR, "Cannot cast $this to string, reason: ", exception)
    "0"
}

fun String.toLongSafe() = try {
    toLong()
} catch (exception: Exception) {
    Logger.logError(TAG_GENERIC_ERROR, "Cannot parse $this to long, reason: ", exception)
    0L
}

fun <T : Number> BigDecimal.multiplySafe(value: T, scale: Int = 2): BigDecimal = try {
    multiply(value.toStringSafe().toBigDecimal()).setScale(scale, RoundingMode.HALF_UP)
} catch (exception: Exception) {
    Logger.logError(TAG_GENERIC_ERROR, "Cannot multiply $this to $value, reason: ", exception)
    this
}

/**
 * Method that will return a [String]
 * with the presentation of how days further/before
 * today the project was created
 */

fun Long.getDaysDuration(context: Context): String {

    val millionSeconds = this - System.currentTimeMillis()
    val dayCount: Int = TimeUnit.MILLISECONDS.toDays(millionSeconds).toInt()

    if (dayCount <= 10) {
        return when {
            dayCount < 0 -> context.getString(R.string.ended_text)
            dayCount == 0 -> context.getString(R.string.ends_today_text)
            dayCount == 1 -> String.format(
                Locale.getDefault(),
                context.getString(R.string.day_left),
                dayCount
            )
            else -> String.format(
                Locale.getDefault(),
                context.getString(R.string.days_left),
                dayCount
            )
        }
    }

    return context.getString(R.string.ends_on_pattern, this.formatByPattern(DD_MMMM_YYYY))
}


fun <T : Number> T.asCurrency(): String {
    return if (this.toLong() < 999) {
        this.toString()
    } else {
        this.formatToCurrencyView()
    }
}

fun Context.registerSMSBroadcastReceiver(
    broadcastReceiver: BroadcastReceiver,
    broadcastPermission: String = SEND_PERMISSION,
    intentFilter: IntentFilter = IntentFilter(SMS_RETRIEVED_ACTION)
) {
    try {
        registerReceiver(broadcastReceiver, intentFilter, broadcastPermission, null)
    } catch (throwable: Throwable) {
        //Do nothing
    }
}

fun Context.unregisterSMSBroadcastReceiver(
    broadcastReceiver: BroadcastReceiver
) {
    try {
        unregisterReceiver(broadcastReceiver)
    } catch (throwable: Throwable) {
        //Do nothing
    }
}

/**
 * Method that is finding a code from Intent data, that was sent by SMS
 */
fun String.findActivationCode(): String {
    var activationCode = ""
    val pattern = Pattern.compile("\\b\\d{6}\\b")
    val matcher = pattern.matcher(this)
    if (matcher.find()) {
        activationCode = (matcher.group(0) ?: "")
    }
    return activationCode
}

fun I18nData.getLocalizedText(id: String): String {
    val locale = Locale.getDefault()
    val language = locale.getUserLanguage()
    val currentTranslation = i18nMaps[language]?.get(id)
    return if (!currentTranslation.isNullOrEmpty()) currentTranslation
    else i18nMaps[defaultLanguage]?.get(id).orDefaultValue("")
}

fun List<TextLinkItem>.createSpannableString(block: (String) -> Unit): SpannableString {
    return SpannableString(this.joinToString(" ") { it.text }).apply {
        var indexStart = 0
        var indexEnd = 0
        this@createSpannableString.forEach {
            indexStart = indexOf(it.text, indexStart)
            indexEnd = indexStart + it.text.length
            if (it.linkUrl.isNotEmpty()) {
                try {
                    val clickableSpan = object : ClickableSpan() {
                        override fun onClick(widget: View) {
                            block.invoke(it.linkUrl)
                        }
                    }
                    setSpan(clickableSpan, indexStart, indexEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                } catch (exception: IndexOutOfBoundsException) {
                    Logger.logError(TAG_GENERIC_ERROR, "Incorrect indexes", exception)
                }
            }
        }
    }
}

fun Context.getSortedCountries() = resources.getStringArray(R.array.countries).toMutableList().apply { sort() }

/**
 * Return the "target" for this fragment of specified type. By default target is activity that owns
 * current fragment but also could be any fragment.
 *
 * @param clazz requested callback interface
 * @return requested callback or null if no callback of requested type is found
 */
fun <T> Fragment.getTarget(clazz: Class<T>): T? {

    val targets = arrayOf(targetFragment, parentFragment, activity)

    targets.forEach {
        if (null != it && clazz.isInstance(it)) {
            return clazz.cast(it)
        }
    }
    return null
}

fun List<View>.filterByTag(id: String) = filter { view ->
    view.tag?.let {
        (it as? UIElementArguments)?.id == id
    }.orDefaultValue(false)
}

fun List<ProjectsFile>.findImageUrlByTag(@FileType fileType: String) = find {
    it.fileType == fileType
}?.projectsFile?.uniqueFileName.orDefaultValue("")

fun BigDecimal.setScaleSafe(scale: Int = 2, roundingMode: RoundingMode = RoundingMode.DOWN): BigDecimal =
    try {
        setScale(scale, roundingMode)
    } catch (e: Exception) {
        this
    }

/**
 * Needs to check if system locale doesn't contain within user available languages,
 * then the method will return an [EN], otherwise system locale.
 */
fun Locale.getUserLanguage(): String {
    val systemLanguage = language.toLowerCase(this)
    return if (LanguageRequestBody.availableLanguages.contains(systemLanguage)) systemLanguage
    else EN
}

fun List<ProjectsFile>.filterDraftDocuments() =
    filter { it.fileType == ProjectsFile.CONTRACT_DOCUMENTS_DRAFT }.toArrayList()

fun Deque<NavBackStackEntry>.foundFragmentByName(kClass: KClass<out Fragment>): Boolean {
    val iterator = descendingIterator()
    while (iterator.hasNext()) {
        val navEntry = iterator.next()
        if (navEntry.destination.label == kClass.simpleName)
            return true
    }
    return false
}

fun ViewPager2.selectNextItem() {
    currentItem = if (currentItem + 1 == adapter?.itemCount) 0 else currentItem + 1
}

fun PreferencesReader.isQuickLoginActive() = getUserBiometricInfo().hasPinCode

/**
 * If pinCode is empty -> update state of fingerPrint or faceId
 */
fun PreferencesWriter.updateQuickLoginPinCode(
    preferencesReader: PreferencesReader,
    pinCode: String = "",
    isBiometricSetup: Boolean = false
) {
    val (externalId, userInfo) = preferencesReader.run {
        getCurrentUserExternalId() to getUserBiometricInfo()
    }
    val newUserInfo = userInfo.copy(
        pinCode = if (userInfo.hasPinCode) userInfo.pinCode else pinCode,
        isBiometricActive = isBiometricSetup)

    cacheUserBiometricInfo(externalId, newUserInfo)
    cacheQuickLoginDialogState(false)
}

@RequiresApi(Build.VERSION_CODES.P)
fun Fragment.startBiometrics(
    onSuccess: () -> Unit,
    onFailure: () -> Unit = {},
    onBiometricDismiss: () -> Unit = {},
) {
    if (!isAliveAndAvailable())
        return

    context?.let {
        val reference = WeakReference(activity)
        BiometricsUtil(reference).run {
            startBiometrics()
            successAuthCallback = { onSuccess() }
            failureAuthCallback = { onFailure() }
            onBiometricDismissByUser = { onBiometricDismiss() }
        }
    }
}

/**
 * Return false if collection is Empty or a previous element is not equal a parameter
 * @param element
 */
fun <T> List<T>.isPreviousEqual(element: T): Boolean {
    if (this.isEmpty())
        return false
    return this[size - 1] == element
}

fun View.addTabLayoutVisibilityManager(tabLayout: TabLayout): ViewTreeObserver.OnGlobalLayoutListener {
    val globalLayoutListener = ViewTreeObserver.OnGlobalLayoutListener {
        val sizesRect = Rect()
        getWindowVisibleDisplayFrame(sizesRect)
        val screenHeight = context.getScreenSize().second
        val keypadHeight = screenHeight - sizesRect.bottom
        tabLayout.isVisible = keypadHeight <= screenHeight * 0.2f
    }
    viewTreeObserver.addOnGlobalLayoutListener(globalLayoutListener)
    return globalLayoutListener
}

/**
 * Method that is returning a screen size from [WindowManager].
 * @return Pair<Int, Int> where first Int it's width and second it's height.
 */
fun Context.getScreenSize(): Pair<Int, Int> {
    val windowManager = getSystemService(WINDOW_SERVICE) as? WindowManager
    val point = Point()
    windowManager?.defaultDisplay?.getSize(point)
    return point.x to point.y
}

fun BigDecimal.roundDownToMultipleOf(number: BigDecimal): BigDecimal {
    return try {
        this.minus(this.rem(number))
    } catch (throwable: Throwable) {
        this
    }
}