package app.eightpitch.com.extensions

import android.animation.ObjectAnimator
import android.app.Activity
import android.app.DatePickerDialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Typeface
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Parcel
import android.util.TypedValue
import android.util.TypedValue.COMPLEX_UNIT_DIP
import android.util.TypedValue.COMPLEX_UNIT_PX
import android.view.LayoutInflater
import android.view.View
import android.view.View.TEXT_ALIGNMENT_TEXT_START
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.view.animation.LinearInterpolator
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.toColorInt
import androidx.core.view.isVisible
import androidx.core.view.postDelayed
import androidx.databinding.Observable
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseActivity
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.data.transmit.ResultStatus
import app.eightpitch.com.ui.authorized.AuthorizedActivity
import app.eightpitch.com.ui.dialogs.GenericDialogFragment
import app.eightpitch.com.ui.registration.RegistrationActivity
import app.eightpitch.com.utils.DateUtils.toLocalDateTime
import app.eightpitch.com.utils.IntentsController
import app.eightpitch.com.utils.Logger
import app.eightpitch.com.utils.Logger.TAG_GENERIC_ERROR
import app.eightpitch.com.utils.models.RefreshTokenModel
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target.SIZE_ORIGINAL
import com.bumptech.glide.signature.ObjectKey
import com.google.android.material.tabs.TabLayout
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*

//region Permissions request codes
const val REQUEST_CAMERA_AND_MICROPHONE = 1
const val REQUEST_WRITING_EXTERNAL_STORAGE = 2
const val REQUEST_SMS_CONSENT_REQUEST = 3
const val REQUEST_START_BIOMETRIC_ENROLL = 5
const val REQUEST_SETTINGS_FINGERPRINT = 6
const val REQUEST_BIOMETRIC_SUCCESS = 10
const val BIOMETRIC_SET_UP_START = 11
const val PIN_CODE_ENABLE_KEY = 12
const val PIN_CODE_DISABLE_KEY = 13
const val UPLOAD_FILES = 14
//endregion

fun <VM : ViewModel> Fragment.injectViewModel(clazz: Class<VM>): VM {

    val activity = requireActivity()
    val factory = (activity as BaseActivity).viewModelFactory
    return ViewModelProvider(activity, factory)[clazz]
}

fun TabLayout.selectTabWith(position: Int) {
    getTabAt(position)?.select()
}

fun Activity.hideKeyboard() {
    val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(window?.decorView?.windowToken, 0)
}

fun Activity.showKeyboard() {
    val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0)
}

fun Fragment.addOnBackPressedCallback(callback: () -> Unit): OnBackPressedCallback? {

    val backPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            callback.invoke()
        }
    }

    return if (isAliveAndAvailable()) {
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            backPressedCallback
        )
        backPressedCallback
    } else
        null
}

fun Fragment.popBackStack(): Boolean {

    if (isAliveAndAvailable())
        view?.let {
            return Navigation.findNavController(it).popBackStack()
        }
    return false
}

fun Fragment.popBackStack(@IdRes untilID: Int): Boolean {

    if (isAliveAndAvailable())
        view?.let {
            return Navigation.findNavController(it).popBackStack(untilID, false)
        }
    return false
}

fun Fragment.isAliveAndAvailable() = isAdded && !isRemoving && null != context && null != view

fun Fragment.finishActivity() {
    if (isAliveAndAvailable())
        activity?.finish()
}

/**
 * Handling back pressure depending on root activity.
 * If recent activity is root, method will start new activity with a new stack
 * otherwise just finish the recent activity.
 */
fun Fragment.moveBackward(toActivityClass: Class<out Activity> = AuthorizedActivity::class.java) {
    if (isAliveAndAvailable()) {
        activity?.let {
            if (it.isTaskRoot)
                IntentsController.startActivityWithANewStack(it, toActivityClass)
            else
                finishActivity()
        }
    }
}

fun Fragment.navigate(@IdRes actionID: Int) {

    if (isAliveAndAvailable())
        view?.let {
            Navigation.findNavController(it).navigateSafe(actionID)
            activity?.hideKeyboard()
        }
}

fun Fragment.navigate(@IdRes actionID: Int, bundle: Bundle) {

    if (isAliveAndAvailable())
        view?.let {
            Navigation.findNavController(it).navigateSafe(actionID, bundle)
            activity?.hideKeyboard()
        }
}

fun Fragment.showDialog(
    title: String = context?.getString(R.string.notification_text) ?: "",
    message: String = "",
    positiveButtonMessage: String = "",
    positiveListener: (() -> Unit)? = null,
    negativeButtonMessage: String = "",
    negativeListener: (() -> Unit)? = null,
    neutralButtonMessage: String = "",
    neutralListener: (() -> Unit)? = null,
    cancelable: Boolean = true,
) {

    val dialogTag = AlertDialog::class.java.name
    val result = childFragmentManager.findFragmentByTag(dialogTag)

    if (null == result && isAliveAndAvailable()) {

        val dialogFragment = GenericDialogFragment.createAnInstance(
            title, message,
            positiveButtonMessage, positiveListener,
            negativeButtonMessage, negativeListener,
            neutralButtonMessage, neutralListener, cancelable
        )
        dialogFragment.showNow(childFragmentManager, dialogTag)
    }
}

fun AppCompatActivity.showDialog(
    title: String = getString(R.string.notification_text),
    message: String = "",
    positiveButtonMessage: String = "",
    positiveListener: (() -> Unit)? = null,
    negativeButtonMessage: String = "",
    negativeListener: (() -> Unit)? = null,
    neutralButtonMessage: String = "",
    neutralListener: (() -> Unit)? = null,
    cancelable: Boolean = true,
) {

    val dialogTag = AlertDialog::class.java.name
    val result = supportFragmentManager.findFragmentByTag(dialogTag)

    if (null == result && !isFinishing) {

        val dialogFragment = GenericDialogFragment.createAnInstance(
            title, message,
            positiveButtonMessage, positiveListener,
            negativeButtonMessage, negativeListener,
            neutralButtonMessage, neutralListener, cancelable
        )

        dialogFragment.showNow(supportFragmentManager, dialogTag)
    }
}

fun Fragment.runDelayedInMainThread(delay: Long, block: () -> Unit) {
    Handler(Looper.getMainLooper()).postDelayed({
        block()
    }, delay)
}

fun AppCompatActivity.runDelayedInMainThread(delay: Long, block: () -> Unit) {
    Handler(Looper.getMainLooper()).postDelayed({
        block()
    }, delay)
}

fun View.runInMainThread(action: () -> Unit) {
    Handler(Looper.getMainLooper()).post { action.invoke() }
}

fun View.changeVisibility(toVisible: Boolean) {
    isVisible = toVisible
}

inline fun <T> Fragment.handleResult(
    result: Result<T>,
    completeIfSuccess: (T) -> Unit = {},
) {

    val (status, requestResult, exception) = result

    if (status == ResultStatus.SUCCESS)
        requestResult?.let {
            completeIfSuccess.invoke(it)
        }
    else {
        exception?.let {
            showDialog(
                title = getString(R.string.notification_text),
                message = it.message.orEmpty()
            )
        }
        if (exception is RefreshTokenModel.AuthorizationFailedException)
            runDelayedInMainThread(1500) {
                context?.let { context ->
                    IntentsController.startActivityWithANewStack(
                        context,
                        RegistrationActivity::class.java
                    )
                }
            }
    }
}

inline fun <T> Fragment.handleResultWithError(
    result: Result<T>,
    doNotShowDialogFor: Class<out Exception>? = null,
    completeIfError: (Exception) -> Unit,
    completeIfSuccess: (T) -> Unit,
) {
    val (status, requestResult, exception) = result

    if (status == ResultStatus.SUCCESS)
        requestResult?.let {
            completeIfSuccess.invoke(it)
        }
    else {

        exception?.let {
            it.message?.let { message ->
                (doNotShowDialogFor as? Class<*>)?.let { classEx ->
                    if (!classEx.isAssignableFrom(it.javaClass))
                        showDialog(message = message)
                }
            }
        }

        if (exception is RefreshTokenModel.AuthorizationFailedException)
            runDelayedInMainThread(1500) {
                context?.let { context ->
                    IntentsController.startActivityWithANewStack(
                        context,
                        RegistrationActivity::class.java
                    )
                }
            }
        else {
            exception?.let { completeIfError.invoke(it) }
        }
    }
}

inline fun <T> AppCompatActivity.handleResult(
    result: Result<T>,
    completeIfSuccess: (T) -> Unit,
) {

    val (status, requestResult, exception) = result

    if (status == ResultStatus.SUCCESS)
        requestResult?.let {
            completeIfSuccess.invoke(it)
        }
    else {
        exception?.let {
            showDialog(
                title = getString(R.string.notification_text),
                message = it.message.orEmpty()
            )
        }
        if (exception is RefreshTokenModel.AuthorizationFailedException)
            runDelayedInMainThread(1500) {
                this.let { context ->
                    IntentsController.startActivityWithANewStack(
                        context,
                        RegistrationActivity::class.java
                    )
                }
            }
    }
}


/**
 * We can't use  Navigation.findNavController(it).popBackStack() here,
 * because then, registered onBackPressedListeners won't be called
 */
fun Fragment.handleBackButton() {

    if (isAliveAndAvailable())
        view?.let { rootView ->
            val backButton = rootView.findViewById<View>(R.id.toolbarBackButton)
            backButton?.setOnClickListener {
                requireActivity().onBackPressed()
            }
        }
}

fun NavController.navigateSafe(@IdRes actionID: Int) {
    try {
        navigate(actionID)
    } catch (navigationException: Exception) {
        Logger.logError(TAG_GENERIC_ERROR, "Cannot navigate right now", navigationException)
    }
}

fun NavController.navigateSafe(
    @IdRes actionID: Int,
    bundle: Bundle,
) {
    try {
        navigate(actionID, bundle)
    } catch (navigationException: Exception) {
        Logger.logError(TAG_GENERIC_ERROR, "Cannot navigate right now", navigationException)
    }
}

inline fun <T> ObservableField<T>.onChanged(
    crossinline lambda: (T) -> Unit,
) {

    val callback = object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
            this@onChanged.get()?.let { lambda(it) }
        }
    }

    addOnPropertyChangedCallback(callback)
}

fun TextView.clear() {
    text = ""
}

fun View.hideHoldingAPlace() {
    visibility = View.INVISIBLE
}

fun View.show() {
    visibility = View.VISIBLE
}

fun View.hide() {
    visibility = View.GONE
}

fun EditText.focus() {

    post {
        if (requestFocus()) {
            val inputMethodManager =
                context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
        }
    }
}

fun Parcel.readStringSafe(): String {
    return this.readString() ?: ""
}

fun <T> Parcel.readListSafe(list: List<T>, classLoader: ClassLoader): List<T> {
    this.readList(list, classLoader)
    return list
}

fun Parcel.readBigDecimal(): BigDecimal {
    return (this.readSerializable() as? BigDecimal) ?: BigDecimal.ZERO
}

/**
 * Method to ask to check available permissions
 * for the app
 *
 * @param permissions -> list permissions to be checked
 * @param performIfAllIsGranted -> action to perform is all permissions
 * is granted
 * @param performIfSomeDenied -> action to perform is one or more
 * permission were denied
 */
fun Context.checkPermissions(
    permissions: Array<String>,
    performIfAllIsGranted: () -> Unit,
    performIfSomeDenied: (Array<String>) -> Unit = {},
) {
    val deniedPermissionIds =
        permissions.filter { (ContextCompat.checkSelfPermission(this, it) == PackageManager.PERMISSION_DENIED) }

    if (deniedPermissionIds.isEmpty())
        performIfAllIsGranted()
    else
        performIfSomeDenied(deniedPermissionIds.toTypedArray())
}

fun ObservableBoolean.revertValue() {
    set(!get())
}

fun View.setDisablingClickListener(delay: Long = 500, perform: (View) -> Unit) {
    setOnClickListener {
        isEnabled = false
        perform(it)
        postDelayed(delay) {
            isEnabled = true
        }
    }
}

/**
 * This method take two booleans and revert value within
 */
fun Pair<ObservableBoolean, ObservableBoolean>.revertValues() {
    first.revertValue()
    second.revertValue()
}

fun Fragment.showDatePickerDialog(
    selectedDateInMillis: Long = System.currentTimeMillis(),
    onDatePicked: (LocalDate) -> Unit,
) {
    if (isAliveAndAvailable()) {
        context?.let {
            val localDateTime = selectedDateInMillis.toLocalDateTime()

            //We do need to increment a month because we are using a java 8 api where months is starting from 1
            val datePickerDialog = DatePickerDialog(it, { _, year, month, dayOfMonth ->
                onDatePicked(LocalDate.of(year, month + 1, dayOfMonth))
            }, localDateTime.year, localDateTime.monthValue - 1, localDateTime.dayOfMonth)

            datePickerDialog.show()
        }
    }
}

fun Context.loadImage(
    url: String?,
    roundImage: Boolean = false,
    withOriginalSize: Boolean = false,
    transformation: Array<BitmapTransformation>? = null,
    targetImageView: AppCompatImageView,
    @DrawableRes placeholderRef: Int = R.drawable.tools_project_detail_bg,
) {

    if (url.isNullOrEmpty()) {
        targetImageView.setImageResource(placeholderRef)
    }

    val requestOptions = RequestOptions()
        .priority(Priority.HIGH)
        .error(placeholderRef)
        .format(DecodeFormat.PREFER_RGB_565)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .signature(ObjectKey(SimpleDateFormat("yyyy-MM-dd'T'HH", Locale.getDefault()).format(Date())))

    transformation?.let {
        requestOptions.transform(*transformation)
    }

    val glideBuilder = Glide.with(this)
        .setDefaultRequestOptions(requestOptions)
        .load(url)

    if (roundImage)
        glideBuilder.apply(RequestOptions.circleCropTransform())
    if (withOriginalSize)
        glideBuilder.override(SIZE_ORIGINAL, SIZE_ORIGINAL)

    glideBuilder.into(targetImageView)
}

fun Context.loadImagePlaceholderNot(
    url: String?,
    roundImage: Boolean = false,
    withOriginalSize: Boolean = false,
    targetImageView: AppCompatImageView,
) {

    if (url.isNullOrEmpty()) {
        return
    }

    val requestOptions = RequestOptions()
        .priority(Priority.HIGH)
        .format(DecodeFormat.PREFER_RGB_565)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .signature(ObjectKey(SimpleDateFormat("yyyy-MM-dd'T'HH", Locale.getDefault()).format(Date())))

    val glideBuilder = Glide.with(this)
        .setDefaultRequestOptions(requestOptions)
        .load(url)

    if (roundImage)
        glideBuilder.apply(RequestOptions.circleCropTransform())
    if (withOriginalSize)
        glideBuilder.override(SIZE_ORIGINAL, SIZE_ORIGINAL)

    glideBuilder.into(targetImageView)
}

fun <VH : RecyclerView.ViewHolder> RecyclerView.Adapter<VH>.inflateView(
    parent: ViewGroup,
    @LayoutRes layoutRef: Int,
): View = LayoutInflater.from(parent.context).inflate(layoutRef, parent, false)


fun ViewPager2.addOnPageSelectedCallback(block: (Int) -> Unit) {
    registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            super.onPageSelected(position)
            block(position)
        }
    })
}

/**
 * The 'onChangedSafe' extension lambda is added to ObservableFields in order to simplify usage of
 * Observable.OnPropertyChangedCallback. The 'onChangedSafe' extension creates and adds
 * OnPropertyChangedCallback to ObservableField. Due to in lot of cases we will use
 * this simplification from Fragments/Activities means that subscribed property could
 * placed in a ViewModel which lives much longer than a screen, and we need to unsubscribe
 * screen from listening the changes of the property, so we add an lifecycle observer to
 * handle subscription, it's safe because all lifecycle subscribers not lives longer than
 * a screen.
 */
inline fun ObservableBoolean.onChangedSafe(
    lifecycleOwner: LifecycleOwner,
    crossinline lambda: (Boolean) -> Unit,
) {

    val callback = object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
            lambda(this@onChangedSafe.get())
        }
    }

    lifecycleOwner.lifecycle.addObserver(object : LifecycleObserver {

        @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
        private fun addPropertyChangedCallback() {
            this@onChangedSafe.addOnPropertyChangedCallback(callback)
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        private fun removePropertyChangedCallback() {
            this@onChangedSafe.removeOnPropertyChangedCallback(callback)
        }
    })
}

fun View.animateProperty(propertyName: String, toValue: Int, animationDuration: Long) {
    try {
        ObjectAnimator.ofInt(this, propertyName, toValue).apply {
            duration = animationDuration
            interpolator = LinearInterpolator()
            start()
        }
    } catch (exception: Exception) {
        Logger.logError(TAG_GENERIC_ERROR, "Cannot animate property right now, reason: ", exception)
    }
}

/**
 * Method that is copying the [text] to clipboard.
 */
fun Context.copyToClipboard(text: String) {
    if (text.isEmpty())
        return

    val clipboard =
        this.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    clipboard.setPrimaryClip(ClipData.newPlainText("text", text))

    Toast.makeText(
        this,
        this.getString(R.string.copied_text),
        Toast.LENGTH_SHORT
    ).show()
}

//TODO add top padding
fun ViewGroup.addViews(vararg views: View) {
    views.forEach {
        addView(it)
    }
}

//TODO may be return params
fun View.setupDefaultLayoutParams(width: Int = MATCH_PARENT, height: Int = WRAP_CONTENT) {
    layoutParams = LinearLayout.LayoutParams(width, height)
}

fun View.setupDefaultMarginLayoutParams(width: Int = MATCH_PARENT, height: Int = WRAP_CONTENT) {
    layoutParams = LinearLayout.LayoutParams(width, height).apply {
        setDefaultMargins(context)
    }
}

fun LinearLayout.LayoutParams.setDefaultMargins(context: Context) = setMargins(
    context.resources.getDimensionPixelOffset(R.dimen.default_left_right_padding),
    context.resources.getDimensionPixelOffset(R.dimen.default_top_bottom_padding),
    context.resources.getDimensionPixelOffset(R.dimen.default_left_right_padding),
    0
)

fun ConstraintLayout.LayoutParams.setDefaultMargins(context: Context) = setMargins(
    context.resources.getDimensionPixelOffset(R.dimen.default_left_right_padding),
    context.resources.getDimensionPixelOffset(R.dimen.default_top_bottom_padding),
    context.resources.getDimensionPixelOffset(R.dimen.default_left_right_padding),
    0
)

fun Context.parseColor(color: String): Int {
    return try {
        color.toColorInt()
    } catch (e: IllegalArgumentException) {
        R.color.lightRed
    }
}

fun AppCompatTextView.setDrawableLeft(@DrawableRes drawable: Int) {
    setCompoundDrawablesWithIntrinsicBounds(drawable, 0, 0, 0)
}

fun ViewGroup.addViewSafe(view: View) = try {
    addView(view)
} catch (e: Exception) {
    Logger.logError(TAG_GENERIC_ERROR, "This view is already added or something else")
}

fun Context.createGradientDrawable(
    shape: Int,
    @ColorRes color: Int,
) = GradientDrawable().apply {
    this.shape = shape
    this.color = ContextCompat.getColorStateList(this@createGradientDrawable, color)
}

fun AppCompatTextView.createHeaderView(
    headingSize: Float = 28f,
    gravity: Int = TEXT_ALIGNMENT_TEXT_START
) {
    setupDefaultLayoutParams()
    setTextAppearance(R.style.HeadingH2)
    textAlignment = gravity
    setDipTextSize(headingSize)
}

fun AppCompatTextView.setupDefaultText() {
    setTextColor(ContextCompat.getColor(context, R.color.white80))
    textSize = resources.getDimension(R.dimen.textSize10)
    typeface = Typeface.create(ResourcesCompat.getFont(context, R.font.roboto_light), Typeface.NORMAL)
}

fun AppCompatTextView.setDipTextSize(dimenResource: Int) =
    setTextSize(COMPLEX_UNIT_PX, resources.getDimension(dimenResource))

fun AppCompatTextView.setDipTextSize(size: Float) =
    setTextSize(COMPLEX_UNIT_DIP, size)

fun Resources.getDpBy(pixels: Float) = TypedValue.applyDimension(COMPLEX_UNIT_DIP, pixels, displayMetrics)

fun Fragment.moveToStartDestination(navHostId: Int) {
    (childFragmentManager.findFragmentById(navHostId) as? NavHostFragment)?.let {
        it.popBackStack(it.navController.graph.startDestination)
    }
}
