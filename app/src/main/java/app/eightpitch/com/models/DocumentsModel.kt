package app.eightpitch.com.models

import android.content.Context
import androidx.annotation.WorkerThread
import app.eightpitch.com.base.BaseModel
import app.eightpitch.com.data.api.APIService
import app.eightpitch.com.utils.Empty

@WorkerThread
class DocumentsModel(
    appContext: Context,
    private val apiService: APIService
) : BaseModel(appContext) {

    fun sendDocumentsByEmail(
        projectId: String,
        investmentId: String
    ): Empty {
        val rawResponse = apiService.sendDocumentsByEmail(projectId, investmentId).execute()
        return checkRetrofitResponseBodyToError(rawResponse, Empty::class.java)
    }
}