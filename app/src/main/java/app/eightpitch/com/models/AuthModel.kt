package app.eightpitch.com.models

import android.content.Context
import androidx.annotation.WorkerThread
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseModel
import app.eightpitch.com.base.MACAddressProvider
import app.eightpitch.com.base.preferences.PreferencesReader
import app.eightpitch.com.base.preferences.PreferencesWriter
import app.eightpitch.com.data.api.APIService
import app.eightpitch.com.data.dto.auth.LoginRequestBody
import app.eightpitch.com.data.dto.auth.LoginResponseBody
import app.eightpitch.com.data.dto.auth.LoginResponseBody.Companion.EXPIRED
import app.eightpitch.com.data.dto.auth.LoginResponseBody.Companion.INACTIVE
import app.eightpitch.com.data.dto.auth.LoginResponseBody.Companion.INVALID
import app.eightpitch.com.data.dto.auth.LoginResponseBody.Companion.SUCCESS
import app.eightpitch.com.data.dto.auth.LoginResponseBody.Companion.UNSUCCESS
import app.eightpitch.com.data.dto.auth.LoginResponseBody.Companion.VALID
import app.eightpitch.com.data.dto.auth.TokenResponse
import app.eightpitch.com.data.dto.sms.PhoneVerificationTokenRequestBody
import app.eightpitch.com.data.dto.twofactor.*
import app.eightpitch.com.data.dto.user.User.CREATOR.RoleType
import app.eightpitch.com.utils.BasicAuthorization
import app.eightpitch.com.utils.Empty

class AuthModel(
    appContext: Context,
    private val basicAuthorization: BasicAuthorization,
    private val userModel: UserModel,
    private val apiService: APIService,
    private val macAddressProvider: MACAddressProvider,
    private val preferencesWriter: PreferencesWriter,
    private val preferencesReader: PreferencesReader
) : BaseModel(appContext) {

    @WorkerThread
    fun login(
        password: String,
        phoneNumber: String,
        twoFactorAuthCode: String = ""
    ): LoginResponseBody {

        val rawLoginResponse = apiService.login(
            basicAuthorization.headerEncodedCredentials,
            LoginRequestBody(
                password = password,
                phoneNumber = phoneNumber,
                deviceId = macAddressProvider.provideAMACAddress(),
                twoFactorAuthCode = twoFactorAuthCode
            )
        ).execute()

        val loginResponse = checkRetrofitResponseBodyToError(rawLoginResponse)
        val tokenResponse = loginResponse.tokenResponse

        if (loginResponse.remainingAttempts == 0)
            throw java.lang.IllegalStateException(appContext.getString(R.string.number_of_attempts_exceeded_text))

        when (loginResponse.result) {
            SUCCESS -> {
                cacheTokens(tokenResponse)
                retrieveUserRole()
                return loginResponse
            }
            UNSUCCESS -> {
                throw InvalidCredentialsException(appContext.getString(R.string.invalid_credentials))
            }
            INACTIVE -> {
                throw IllegalStateException(appContext.getString(R.string.your_account_has_been_suspended_text))
            }
            else -> {
                throw AbsentTokensException(
                    appContext.getString(R.string.please_perform_sms_confirmation_text),
                    loginResponse.interactionId,
                    loginResponse.verificationTypes
                )
            }
        }
    }

    @WorkerThread
    fun loginWithSMSCode(
        smsCode: String,
        phoneNumber: String,
        interactionId: String
    ): LoginResponseBody {

        val rawResponse = apiService.loginWithVerifyCode(
            phoneNumber = phoneNumber,
            phoneVerificationTokenRequestBody = PhoneVerificationTokenRequestBody(
                interactionId = interactionId,
                code = smsCode
            )
        ).execute()

        val loginResponse = checkRetrofitResponseBodyToError(rawResponse)

        when (loginResponse.validationResult) {
            VALID -> {
                cacheTokens(loginResponse.tokenResponse)
                retrieveUserRole()
                return loginResponse
            }
            INVALID -> {
                throw IllegalStateException(appContext.getString(R.string.invalid_sms_code_text))
            }
            EXPIRED -> {
                throw IllegalStateException(appContext.getString(R.string.sms_code_expired_text))
            }
            else -> {
                throw IllegalStateException(appContext.getString(R.string.something_go_wrong_exception_text))
            }
        }
    }

    private fun cacheTokens(tokenResponse: TokenResponse) {
        preferencesWriter.apply {
            cacheAccessToken(tokenResponse.accessToken)
            cacheRefreshToken(tokenResponse.refreshToken)
            cacheTokenExpirationTime(1)
        }
    }

    private fun retrieveUserRole() {
        try {
            val userData = userModel.getUserData()
            @RoleType val userRole = userData.role
            val externalId = userData.externalId
            preferencesWriter.run {
                cacheUserRole(userRole)
                cacheExternalId(externalId)
            }
        } catch (throwable: Throwable) {
            preferencesWriter.clearLoginCredentials()
            throw IllegalStateException("Cannot retrieve a user type")
        }
    }

    @WorkerThread
    fun logout(): Empty {
        val rawLogoutResponse = apiService.logout().execute()
        val result = checkRetrofitResponseBodyToError(rawLogoutResponse, Empty::class.java)
        preferencesWriter.clearLoginCredentials()
        return result
    }

    fun smsTwoFactorDisable(
        userRole: String,
        userExternalId: String,
        interactionId: String,
        token: String
    ): SmsTwoFactorResponse {

        val rawSmsTwoFactorDisable = apiService.disableSmsTwoFactorAuth(
            authHeader = "Bearer ${preferencesReader.getCachedAccessToken()}",
            group = userRole,
            userExternalId = userExternalId,
            body = SmsTwoFactorRequestBody(
                interactionId = interactionId,
                token = token
            )
        ).execute()

        return checkRetrofitResponseBodyToError(rawSmsTwoFactorDisable)
    }

    fun smsTwoFactorEnable(
        userRole: String,
        userExternalId: String,
        interactionId: String,
        token: String
    ): SmsTwoFactorResponse {

        val rawSmsTwoFactorDisable = apiService.enableSmsTwoFactorAuth(
            authHeader = "Bearer ${preferencesReader.getCachedAccessToken()}",
            body = SmsTwoFactorRequestBody(
                interactionId = interactionId,
                token = token
            )
        ).execute()

        return checkRetrofitResponseBodyToError(rawSmsTwoFactorDisable)
    }

    fun twoFactorAuthDisable(twoFactorAuthCode: String): SmsTwoFactorResponse {
        val rawTwoFactorAuthDisable = apiService.disableTwoFactorAuth(
            body = TwoFactorAuthRequestBody(twoFactorAuthCode)
        ).execute()

        return checkRetrofitResponseBodyToError(rawTwoFactorAuthDisable)
    }

    fun twoFactorAuthEnable(
        twoFactorAuthCode: String
    ): SmsTwoFactorResponse {
        val rawTwoFactorAuthEnable = apiService.enableTwoFactorAuth(
            body = TwoFactorAuthEnableRequestBody(
                twoFactorAuthCode = twoFactorAuthCode
            )
        ).execute()

        return checkRetrofitResponseBodyToError(rawTwoFactorAuthEnable)
    }

    fun twoFactorGetQrCode(): TwoFactorQrCodeResponse {
        val rawTwoFactorGetQrCode = apiService.getQrCodeForTwoFactorAuth().execute()
        return checkRetrofitResponseBodyToError(rawTwoFactorGetQrCode)
    }

    fun twoFactorVerifyCode(code: String, interactionId: String): TwoFactorVerifyCodeResponse {
        val rawTwoFactorVerifyCode = apiService.verifyCodeForTwoFactorAuth(
            body = TwoFactorVerifyCodeRequestBody(
                code = code,
                interactionId = interactionId
            )
        ).execute()

        val loginResponse = checkRetrofitResponseBodyToError(rawTwoFactorVerifyCode)
        val tokenResponse = loginResponse.tokenResponse

        val accessToken = tokenResponse?.accessToken ?: ""
        if (!loginResponse.valid) {
            throw InvalidCredentialsException(appContext.getString(R.string.invalid_sms_code_text))
        } else if (accessToken.isNotEmpty()) {
            cacheTokens(
                TokenResponse(
                    loginResponse.tokenResponse?.accessToken,
                    loginResponse.tokenResponse?.refreshToken
                )
            )
            retrieveUserRole()
        } else {
            throw AbsentTokensException(
                appContext.getString(R.string.please_perform_sms_confirmation_text),
                loginResponse.interactionId,
                loginResponse.verificationTypes
            )
        }

        return loginResponse
    }

}