package app.eightpitch.com.models

import android.content.Context
import androidx.annotation.WorkerThread
import app.eightpitch.com.base.BaseModel
import app.eightpitch.com.data.api.APIService
import app.eightpitch.com.data.dto.notifications.Notification
import app.eightpitch.com.utils.Empty

@WorkerThread
class NotificationsModel(
    applicationContext: Context,
    private val apiService: APIService
) : BaseModel(applicationContext) {

    internal fun retrieveNotifications(): List<Notification> {
        val notificationsResult = apiService.getNotifications(0, 999).execute()
        val notificationsResponseBody = checkRetrofitResponseBodyToError(notificationsResult)
        return notificationsResponseBody.notifications
    }

    internal fun markNotificationRead(notificationId: Int): Empty {
        val markingResult = apiService.markNotificationAsRead(notificationId).execute()
        return checkRetrofitResponseBodyToError(markingResult, Empty::class.java)
    }
}