package app.eightpitch.com.models

import android.content.Context
import androidx.annotation.WorkerThread
import app.eightpitch.com.base.BaseModel
import app.eightpitch.com.data.api.APIService
import app.eightpitch.com.data.dto.investment.InvestorGraph
import app.eightpitch.com.data.dto.project.GetProjectPageResponse
import app.eightpitch.com.data.dto.project.InitiatorProjects
import app.eightpitch.com.data.dto.project.InvestorsProject
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.ACTIVE
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.COMING_SOON
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.FINISHED
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.MANUAL_RELEASE
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.NOT_STARTED
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.REFUNDED
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.TOKENS_TRANSFERRED
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.WAIT_BLOCKCHAIN
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.WAIT_RELEASE
import app.eightpitch.com.data.dto.projectdetails.GetProjectResponse

@WorkerThread
class ProjectModel(
    appContext: Context,
    private val apiService: APIService
) : BaseModel(appContext) {

    companion object {
        const val ASC = "asc"
        const val DESC = "desc"
        const val CREATED = "created"
    }

    /**
     * @return projects in which the investor has invested
     */
    fun getInvestorProjects(): List<InvestorsProject> {
        val rawUserDataResponse = apiService.getInvestorProjects().execute()
        return checkRetrofitResponseBodyToError(rawUserDataResponse)
    }

    /**
     * @param page - Loaded page number
     * @param size - List size per page
     * @param sort - Sort type of page list
     * @return All the projects.
     */
    fun getProjectInfo(
        page: Int = 0,
        size: Int = 9999,
        sort: String = DESC
    ): GetProjectPageResponse {
        val rawUserDataResponse = apiService.getProjects(page, size, sort).execute()
        return checkRetrofitResponseBodyToError(rawUserDataResponse)
    }

    /**
     * @param page - Loaded page number
     * @param size - List size per page
     * @param sort - Sort type of page list
     * @note We set all statuses because backend returns projects by three default statuses
     * @return All the initiator's projects.
     */
    fun getInitiatorProjects(
        page: Int = 0,
        size: Int = 9999,
        sort: String = ASC,
        statuses: List<String> = listOf(
            NOT_STARTED, COMING_SOON, ACTIVE, MANUAL_RELEASE, FINISHED,
            WAIT_RELEASE, REFUNDED, TOKENS_TRANSFERRED, WAIT_BLOCKCHAIN
        )
    ): InitiatorProjects {
        val rawUserDataResponse = apiService.getInitiatorProjects(page, size, sort, statuses).execute()
        return checkRetrofitResponseBodyToError(rawUserDataResponse)
    }

    /**
     * @return brief information about which project and how much the investor has invested
     */
    fun getInvestorGraph(): InvestorGraph {
        val rawUserDataResponse = apiService.getInvestorsGraph().execute()
        return checkRetrofitResponseBodyToError(rawUserDataResponse)
    }

    /**
     * Returns project details
     */
    fun getProjectInfo(projectId: String): GetProjectResponse {
        val rawUserDataResponse = apiService.getProjectInfo(projectId).execute()
        return checkRetrofitResponseBodyToError(rawUserDataResponse)
    }
}