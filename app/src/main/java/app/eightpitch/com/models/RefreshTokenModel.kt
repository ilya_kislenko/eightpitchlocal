package app.eightpitch.com.utils.models

import android.content.Context
import androidx.annotation.WorkerThread
import app.eightpitch.com.BuildConfig
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseModel
import app.eightpitch.com.base.preferences.PreferencesWriter
import app.eightpitch.com.data.dto.auth.TokenResponse
import app.eightpitch.com.utils.Logger
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import org.json.JSONObject
import javax.inject.Inject

/**
 * I intentionally don't inject an api service here, because, if this model need an api
 * service means that it need an retrofit instance, and i intent to this model only
 * from OkHtttp authenticator which is creating during building a retrofit instance, so,
 * if i inject a retrofit here, i won't able to use this model from authenticator cause
 * of cyclic dependency.
 *
 * Example:
 *
 * Retrofit -> OkHttpClient -> Authenticator(RefreshTokenModel)
 * RefreshTokenModel(Retrofit -> api service)
 *
 * Dagger won't be able to create a model because first he would try to build a retrofit,
 * but he can't build a retrofit because model requires retrofit
 *
 * So, i can't use retrofit in Authenticator, i'll use OkHttpClient
 */

class RefreshTokenModel @Inject constructor(
    appContext: Context,
    private val preferencesWriter: PreferencesWriter
) : BaseModel(appContext) {

    @WorkerThread
    @Synchronized
    fun refreshToken(refreshToken: String): TokenResponse {

        if(refreshToken.isEmpty()){
            Logger.logError(RefreshTokenModel::class.java.name,
                "Cannot perform access token refresh, because refresh token is empty")
            performAuthorizationFailure()
        }

        val json = JSONObject().put("refreshToken", refreshToken)
        val body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), json.toString())

        val request = Request.Builder()
            .url(BuildConfig.ENDPOINT + "user-service/refresh-token")
            .post(body)
            .build()

        val response = OkHttpClient().newCall(request).execute()

        val responseCode = response.code()

        if (responseCode in 400 until 500) {
            Logger.logError(RefreshTokenModel::class.java.name,
                "Refresh token failed with: http code = $responseCode, message = ${response.message()}, body = ${response.body()
                    ?.string()}")
            performAuthorizationFailure()
        }

        return checkOkHttpResponseBodyToError(response, TokenResponse::class.java)
    }

    private fun performAuthorizationFailure(){
        preferencesWriter.clearLoginCredentials()
        throw AuthorizationFailedException(appContext.getString(R.string.token_expired_message))
    }

    /**
     * The exception for describing a situation when refresh token
     * process has finished with an error and that error was invoked by backend side,
     * and we need to redirect user to a login page.
     */
    class AuthorizationFailedException(message: String) : Exception(message)
}