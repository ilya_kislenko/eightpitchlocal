package app.eightpitch.com.models

import android.content.Context
import android.net.Uri
import android.provider.OpenableColumns
import androidx.annotation.WorkerThread
import app.eightpitch.com.base.BaseModel
import app.eightpitch.com.data.api.APIService
import app.eightpitch.com.data.dto.files.*
import app.eightpitch.com.utils.Empty
import okhttp3.*
import java.io.File
import java.io.FileOutputStream
import javax.inject.Inject
import kotlin.Exception

@WorkerThread
class FilesModel @Inject constructor(
    applicationContext: Context,
    private val apiService: APIService,
) : BaseModel(applicationContext) {

    internal fun uploadFile(uri: Uri, metaData: FileMetaData): FileCreationResponse {
        val file = getFileByUri(uri, metaData.displayName)
        val rawResponse =
            apiService.uploadFile(createAMultipartFile(file, "file", MultipartBody.FORM)).execute()
        return checkRetrofitResponseBodyToError(rawResponse)
    }

    internal fun deleteFile(id: String): Empty {
        val rawResponse = apiService.deleteFile(id).execute()
        return checkRetrofitResponseBodyToError(rawResponse, Empty::class.java)
    }

    internal fun getFileMetaDataByUri(uri: Uri): FileMetaData {

        val cursor = appContext.contentResolver?.query(uri, null, null, null, null, null)

        cursor?.use {

            if (it.moveToFirst()) {

                val displayName: String = it.getString(it.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                val sizeIndex: Int = it.getColumnIndex(OpenableColumns.SIZE)
                val size = if (!it.isNull(sizeIndex)) it.getString(sizeIndex).toInt() else 0

                //TODO add translation
                if (size > 5e+6)
                    throw OversizedFileException("Over size")
                else
                    return FileMetaData(displayName, size, uri, System.currentTimeMillis())
            }
        }

        throw AttachmentException("cannot parse")
    }

    private fun getFileByUri(uri: Uri, fileName: String): File {

        val inputStream = appContext.contentResolver?.openInputStream(uri)

        inputStream?.let {

            it.use { iStream ->

                val rootPath = File(appContext.cacheDir, fileName)
                val outputStream = FileOutputStream(rootPath)

                outputStream.use {
                    val buffer = ByteArray(4 * 1024)
                    var read = iStream.read(buffer)

                    while (read != -1) {
                        outputStream.write(buffer, 0, read)
                        read = iStream.read(buffer)
                    }

                    outputStream.flush()
                    return rootPath
                }
            }
        }

        throw AttachmentException("cannot parse")
    }

    private fun createAMultipartFile(file: File, multipartName: String, mediaType: MediaType): MultipartBody.Part {

        val requestFile = RequestBody.create(mediaType, file)
        return MultipartBody.Part.createFormData(multipartName, file.name, requestFile)
    }

    class OversizedFileException(message: String) : Exception(message)
    class AttachmentException(message: String) : Exception(message)
}