package app.eightpitch.com.models

import android.content.Context
import androidx.annotation.WorkerThread
import app.eightpitch.com.base.BaseModel
import app.eightpitch.com.data.api.APIService
import app.eightpitch.com.data.dto.filter.AmountRange
import app.eightpitch.com.data.dto.filter.MinimumInvestmentAmountResponse
import app.eightpitch.com.data.dto.project.GetProjectPageResponse
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.ACTIVE
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.COMING_SOON
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.FINISHED
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.MANUAL_RELEASE
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.NOT_STARTED
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.REFUNDED
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.TOKENS_TRANSFERRED
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.WAIT_BLOCKCHAIN
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.WAIT_RELEASE

/**
 * This class contains methods for getting information for a filter,
 * also contains a request for getting projects based on filter data.
 */
@WorkerThread
class FilterModel(
    appContext: Context,
    private val apiService: APIService
) : BaseModel(appContext) {

    fun getTypeOfSecurity(
        statuses: List<String> = listOf(
            NOT_STARTED,
            COMING_SOON,
            ACTIVE,
            MANUAL_RELEASE,
            FINISHED,
            WAIT_RELEASE,
            REFUNDED,
            TOKENS_TRANSFERRED,
            WAIT_BLOCKCHAIN
        )
    ): List<String> {
        val rawUserDataResponse = apiService.getTypeOfSecurity(statuses).execute()
        return checkRetrofitResponseBodyToError(rawUserDataResponse)
    }

    fun getMinimalInvestment(
        statuses: List<String> = listOf(
            COMING_SOON,
            ACTIVE,
            MANUAL_RELEASE,
            FINISHED,
            WAIT_RELEASE,
            REFUNDED,
            TOKENS_TRANSFERRED,
            WAIT_BLOCKCHAIN
        )
    ): MinimumInvestmentAmountResponse {
        val rawUserDataResponse = apiService.getMinimalInvestment(statuses).execute()
        return checkRetrofitResponseBodyToError(rawUserDataResponse)
    }

    fun getFilteredProject(
        page: Int = 0,
        size: Int = 9999,
        sort: String = ProjectModel.DESC,
        companyName: String = "",
        minimumAmounts: List<AmountRange> = arrayListOf(),
        projectStatuses: List<String> = arrayListOf(),
        typesOfSecurity: List<String> = arrayListOf()
    ) : GetProjectPageResponse {

        val defaultStatuses = listOf(
            COMING_SOON,
            ACTIVE,
            MANUAL_RELEASE,
            FINISHED,
            WAIT_RELEASE,
            REFUNDED,
            TOKENS_TRANSFERRED,
            WAIT_BLOCKCHAIN
        )

        val rawUserDataResponse = apiService.getFilteredProjects(
            page = page,
            size = size,
            sort = sort,
            companyName = companyName,
            minimumAmounts = minimumAmounts.map { it.name },
            statuses = if(projectStatuses.isEmpty()) defaultStatuses else projectStatuses,
            typesOfSecurity = typesOfSecurity
        ).execute()
        return checkRetrofitResponseBodyToError(rawUserDataResponse)
    }
}