package app.eightpitch.com.models

import android.content.Context
import androidx.annotation.WorkerThread
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseModel
import app.eightpitch.com.data.api.APIService
import app.eightpitch.com.data.dto.DataValidResponse
import app.eightpitch.com.data.dto.sms.EmailPhoneValidationResponse
import app.eightpitch.com.data.dto.sms.EmailPhoneValidationResponse.Companion.EXPIRED
import app.eightpitch.com.data.dto.sms.EmailPhoneValidationResponse.Companion.INVALID
import app.eightpitch.com.data.dto.sms.EmailPhoneValidationResponse.Companion.VALID
import app.eightpitch.com.data.dto.sms.EmailVerificationTokenRequestBody

@WorkerThread
class SupportModel(
    applicationContext: Context,
    private val apiService: APIService
) : BaseModel(applicationContext) {

    /**
     * This method should be called from a [WorkerThread] inside a
     * safe try/catch wrapped block, to handle an [Exception] on
     * the UI layer
     */

    fun checkIfEmailIsFree(email: String): DataValidResponse {
        val rawResponse = apiService.checkIfEmailIsFree(email = email).execute()
        val emailValidResponse = checkRetrofitResponseBodyToError(rawResponse, DataValidResponse::class.java)

        if (emailValidResponse.isDataValid) {
            return emailValidResponse
        } else
            throw IllegalStateException(appContext.getString(R.string.email_is_not_free_text))
    }

    fun checkIfPhoneIsFree(phoneNumber: String): DataValidResponse {
        val rawResponse = apiService.checkIfPhoneIsFree(phoneNumber = phoneNumber).execute()
        val phoneValidResponse = checkRetrofitResponseBodyToError(rawResponse, DataValidResponse::class.java)

        if (phoneValidResponse.isDataValid) {
            return phoneValidResponse
        } else
            throw IllegalStateException(appContext.getString(R.string.phone_is_not_free_text))
    }

    fun verifyEmailCode(
        emailCode: String
    ): EmailPhoneValidationResponse {

        val rawResponse = apiService.verifyEmailCode(
            EmailVerificationTokenRequestBody(emailCode)
        ).execute()

        val verifyEmailResponse = checkRetrofitResponseBodyToError(rawResponse)

        when (verifyEmailResponse.result) {
            VALID -> {
                return verifyEmailResponse
            }
            INVALID -> {
                throw IllegalStateException(appContext.getString(R.string.invalid_email_code_text))
            }
            EXPIRED -> {
                throw IllegalStateException(appContext.getString(R.string.sms_code_expired_text))
            }
            else -> {
                throw IllegalStateException(appContext.getString(R.string.something_go_wrong_exception_text))
            }
        }
    }
}