package app.eightpitch.com.models

import android.content.Context
import androidx.annotation.WorkerThread
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseModel
import app.eightpitch.com.base.preferences.PreferencesReader
import app.eightpitch.com.base.preferences.PreferencesWriter
import app.eightpitch.com.data.api.APIService
import app.eightpitch.com.data.dto.changingpassword.ChangePasswordRequestBody
import app.eightpitch.com.data.dto.changingpassword.VerifyOldPasswordRequestBody
import app.eightpitch.com.data.dto.changingpassword.VerifyOldPasswordResponseBody
import app.eightpitch.com.data.dto.changingpassword.VerifyOldPasswordResponseBody.Companion.SUCCESS
import app.eightpitch.com.data.dto.profilesettings.SubscribeToEmailRequestBody
import app.eightpitch.com.data.dto.questionnaire.InvestorQualificationRequestBody
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody
import app.eightpitch.com.data.dto.user.DeleteAccountRequest
import app.eightpitch.com.data.dto.user.LinkSliderItemResponseBody
import app.eightpitch.com.data.dto.user.TOP_INITIATOR_SLIDER
import app.eightpitch.com.data.dto.user.TOP_INVESTOR_SLIDER
import app.eightpitch.com.data.dto.user.TaxInformationBody
import app.eightpitch.com.data.dto.user.UpdateUserPersonalDataRequestBody
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.data.dto.user.User.CREATOR.PRIVATE_INVESTOR
import app.eightpitch.com.data.dto.webid.ActionIdResponseBody
import app.eightpitch.com.data.dto.webid.WebIdUserRequestBody
import app.eightpitch.com.utils.Empty
import java.lang.IllegalStateException

@WorkerThread
class UserModel(
    appContext: Context,
    private val apiService: APIService,
    private val preferencesReader: PreferencesReader,
    private val preferencesWriter: PreferencesWriter,
) : BaseModel(appContext) {

    fun getUserData(): User {
        val rawUserDataResponse = apiService.getUserData().execute()
        return checkRetrofitResponseBodyToError(rawUserDataResponse)
    }

    fun updateUserProfileData(body: UpdateUserPersonalDataRequestBody): Empty {
        val rawResponse = apiService.updateUserProfileData(body).execute()
        return checkRetrofitVoidResponseBodyToError(rawResponse)
    }

    fun deleteAccount(reason: String): Empty {

        val rawUserDataResponse = apiService.deleteAccount(
            "Bearer ${preferencesReader.getCachedAccessToken()}",
            DeleteAccountRequest(reason)
        ).execute()
        val result = checkRetrofitVoidResponseBodyToError(rawUserDataResponse)
        preferencesWriter.clearLoginCredentials()
        return result
    }

    fun getActionId(userRequestBody: WebIdUserRequestBody): ActionIdResponseBody {
        val rawResponse = apiService.getActionId(userRequestBody).execute()
        return checkRetrofitResponseBodyToError(rawResponse)
    }

    fun reportFinishedCall(): Empty {
        val rawResponse = apiService.reportFinishedCall().execute()
        return checkRetrofitResponseBodyToError(rawResponse, Empty::class.java)
    }

    fun sendQuestionnaire(questionnaireRequestBody: QuestionnaireRequestBody): Empty {
        val (userRole, userExternalId) = preferencesReader.run {
            getUserRole() to getCurrentUserExternalId()
        }
        val rawResponse =
            apiService.sendQuestionnaire(userRole, userExternalId, questionnaireRequestBody)
                .execute()
        return checkRetrofitResponseBodyToError(rawResponse, Empty::class.java)
    }

    fun sendQualificationForm(investorQualificationRequestBody: InvestorQualificationRequestBody): Empty {
        val (userRole, userExternalId) = preferencesReader.run {
            getUserRole() to getCurrentUserExternalId()
        }
        val rawResponse =
            apiService.sendQualificationForm(userRole, userExternalId, investorQualificationRequestBody).execute()
        return checkRetrofitResponseBodyToError(rawResponse, Empty::class.java)
    }


    fun updateEmailSubscription(enableSubscription: Boolean): Empty {
        val rawResponse = apiService.setSubscribeToEmail(SubscribeToEmailRequestBody(enableSubscription)).execute()
        return checkRetrofitVoidResponseBodyToError(rawResponse)
    }

    fun verifyOldPassword(body: VerifyOldPasswordRequestBody): VerifyOldPasswordResponseBody {
        val rawResponse = apiService.verifyOldPassword(body).execute()
        val response = checkRetrofitResponseBodyToError(rawResponse)
        if (response.status == SUCCESS)
            return response
        else
            throw IllegalStateException(appContext.getString(R.string.incorrect_password_text))
    }

    fun changePassword(body: ChangePasswordRequestBody): Empty {
        val rawResponse = apiService.changePassword(body).execute()
        return checkRetrofitResponseBodyToError(rawResponse, Empty::class.java)
    }

    fun getDashboardSliderInfo(): List<LinkSliderItemResponseBody> {
        fun getGroup() = when (preferencesReader.getUserRole()) {
            PRIVATE_INVESTOR -> TOP_INVESTOR_SLIDER
            else -> TOP_INITIATOR_SLIDER
        }

        val rawResponse = apiService.getDashboardSliderInfo(getGroup()).execute()
        return checkRetrofitResponseBodyToError(rawResponse)
    }

    fun updateTaxInformation(body: TaxInformationBody): Empty {
        val rawResponse = apiService.updateTaxInformation(body).execute()
        return checkRetrofitResponseBodyToError(rawResponse, Empty::class.java)
    }
}