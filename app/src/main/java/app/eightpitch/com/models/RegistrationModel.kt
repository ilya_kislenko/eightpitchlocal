package app.eightpitch.com.models

import android.content.Context
import androidx.annotation.WorkerThread
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseModel
import app.eightpitch.com.base.preferences.*
import app.eightpitch.com.data.api.APIService
import app.eightpitch.com.data.dto.auth.TokenResponse
import app.eightpitch.com.data.dto.signup.*
import app.eightpitch.com.data.dto.sms.EmailPhoneValidationResponse
import app.eightpitch.com.data.dto.sms.EmailPhoneValidationResponse.Companion.EXPIRED
import app.eightpitch.com.data.dto.sms.EmailPhoneValidationResponse.Companion.INVALID
import app.eightpitch.com.data.dto.sms.EmailPhoneValidationResponse.Companion.VALID
import app.eightpitch.com.data.dto.signup.RegistrationResponseBody.Companion.SUCCESS
import app.eightpitch.com.data.dto.signup.RegistrationResponseBody.Companion.USER_ALREADY_EXIST
import app.eightpitch.com.data.dto.sms.PhoneVerificationTokenRequestBody
import app.eightpitch.com.data.dto.user.LanguageRequestBody
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.extensions.getUserLanguage
import app.eightpitch.com.utils.Empty
import retrofit2.Response
import java.util.*

class RegistrationModel(
    appContext: Context,
    private val userModel: UserModel,
    private val apiService: APIService,
    private val preferencesWriter: PreferencesWriter,
    private val preferencesReader: PreferencesReader
) : BaseModel(appContext) {

    @WorkerThread
    fun performRegistration(
        partialUser: CommonInvestorRequestBody
    ): Empty {

        partialUser.apply { deviceId = preferencesReader.getMACAddress() }
        val rawRegistrationResponse = performUserRegistration(partialUser)
        val response = checkRetrofitResponseBodyToError(rawRegistrationResponse)

        when (response.status) {
            SUCCESS -> {
                cacheTokens(response.tokenResponse)
                retrieveUserRole()
                return Empty.instance
            }
            USER_ALREADY_EXIST -> {
                throw IllegalStateException(appContext.getString(R.string.user_already_exists_message_text))
            }
            else -> {
                throw IllegalStateException(appContext.getString(R.string.registration_is_failed))
            }
        }
    }

    private fun retrieveUserRole() {
        try {
            val userData = userModel.getUserData()
            @User.CREATOR.RoleType val userRole = userData.role
            val externalId = userData.externalId
            preferencesWriter.run {
                cacheUserRole(userRole)
                cacheExternalId(externalId)
            }
        } catch (throwable: Throwable) {
            preferencesWriter.clearLoginCredentials()
            throw IllegalStateException("Cannot retrieve a user type")
        }
    }

    @WorkerThread
    fun setUserLanguage(systemLocale: Locale): Empty {
        val requestBody = LanguageRequestBody(systemLocale.getUserLanguage())
        val rawLanguageResponse = apiService.setLanguage(requestBody).execute()
        return checkRetrofitResponseBodyToError(rawLanguageResponse, Empty::class.java)
    }

    private fun performUserRegistration(partialUser: CommonInvestorRequestBody): Response<RegistrationResponseBody> {
        val request = if (partialUser is InstitutionalInvestorRequestBody)
            apiService.createUser(partialUser)
        else {
            apiService.createUser(partialUser)
        }
        return request.execute()
    }

    @WorkerThread
    fun registrationWithVerifyCode(
        phoneNumber: String,
        interactionId: String,
        smsCode: String
    ): EmailPhoneValidationResponse {

        val rawResponse = apiService.registrationWithVerifyCode(
            phoneNumber = phoneNumber,
            phoneVerificationTokenRequestBody = PhoneVerificationTokenRequestBody(
                interactionId = interactionId,
                code = smsCode
            )
        ).execute()

        val codeValidResponse = checkRetrofitResponseBodyToError(rawResponse)

        when (codeValidResponse.result) {
            VALID -> {
                return codeValidResponse
            }
            INVALID -> {
                throw IllegalStateException(appContext.getString(R.string.invalid_sms_code_text))
            }
            EXPIRED -> {
                throw IllegalStateException(appContext.getString(R.string.sms_code_expired_text))
            }
            else -> {
                throw IllegalStateException(appContext.getString(R.string.something_go_wrong_exception_text))
            }
        }
    }

    private fun cacheTokens(tokenResponse: TokenResponse) {
        preferencesWriter.apply {
            cacheAccessToken(tokenResponse.accessToken)
            cacheRefreshToken(tokenResponse.refreshToken)
            cacheTokenExpirationTime(1)
        }
    }
}