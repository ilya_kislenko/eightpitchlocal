package app.eightpitch.com.models

import android.content.Context
import androidx.annotation.WorkerThread
import app.eightpitch.com.base.BaseModel
import app.eightpitch.com.data.api.APIService
import app.eightpitch.com.data.dto.payment.*
import app.eightpitch.com.data.dto.profilepayments.GetProfilePaymentsResponse
import app.eightpitch.com.data.dto.profiletransfers.GetProfileTransfersResponse
import app.eightpitch.com.models.ProjectModel.Companion.ASC
import app.eightpitch.com.utils.Constants.ONLINE_ABORT_URL
import app.eightpitch.com.utils.Constants.ONLINE_CONFIRM_URL
import app.eightpitch.com.utils.Empty

@WorkerThread
class PaymentModel(
    appContext: Context,
    private val apiService: APIService
) : BaseModel(appContext) {

    fun getClassicHardCapPaymentInfoBy(investmentId: String): BankPaymentsDataResponse {
        val rawUserDataResponse = apiService.getClassicHardCapPaymentInfoBy(investmentId).execute()
        return checkRetrofitResponseBodyToError(rawUserDataResponse)
    }

    fun getClassicSoftCapPaymentInfoBy(investmentId: String): BankPaymentsDataResponse {
        val rawUserDataResponse = apiService.getClassicSoftCapPaymentInfoBy(investmentId).execute()
        return checkRetrofitResponseBodyToError(rawUserDataResponse)
    }

    fun confirmBankTransfer(investmentId: String): Empty {
        val rawResponse = apiService.confirmBankTransfer(investmentId).execute()
        return checkRetrofitVoidResponseBodyToError(rawResponse)
    }

    fun confirmDebitPayment(
        investmentId: String,
        saveSequpayDataRequest: SaveSequpayDataRequest
    ): SaveSequpayDataResponse {
        val rawResponse =
            apiService.confirmDebitPayment(investmentId, saveSequpayDataRequest).execute()
        return checkRetrofitResponseBodyToError(rawResponse)
    }

    /**
     * Submit online payment named `Fint`
     */
    fun submitFintPayment(
        investmentId: String
    ): TransactionInfoResponse {
        val fintecsystemsRequest = FintecsystemsRequest(RedirectParams(
            abortUrl = ONLINE_ABORT_URL,
            successUrl = ONLINE_CONFIRM_URL
        ))
        val rawResponse =
            apiService.submitFint(investmentId, fintecsystemsRequest).execute()
        return checkRetrofitResponseBodyToError(rawResponse)
    }

    fun submitKlarnaPayment(
        investmentId: String
    ): TransactionInfoResponse {
        val redirectParams = RedirectParams(
            abortUrl = ONLINE_ABORT_URL,
            successUrl = ONLINE_CONFIRM_URL
        )
        val rawResponse =
            apiService.submitKlarna(investmentId, redirectParams).execute()
        return checkRetrofitResponseBodyToError(rawResponse)
    }

    /**
     * @return All completed payments
     * 
     * @param page - Loaded page number
     * @param size - List size per page
     * @param sort - Sort type of page list
     */
    fun getProfilePayments(
        page: Int = 0,
        size: Int = 9999,
        sort: String = ASC
    ): GetProfilePaymentsResponse {
        val rawUserDataResponse = apiService.getProfilePayments(page, size, sort).execute()
        return checkRetrofitResponseBodyToError(rawUserDataResponse)
    }

    fun getProfileTransfers(
        page: Int = 0,
        size: Int = 9999
    ): GetProfileTransfersResponse {
        val rawUserDataResponse = apiService.getProfileTransfers(page, size).execute()
        return checkRetrofitResponseBodyToError(rawUserDataResponse)
    }
}
