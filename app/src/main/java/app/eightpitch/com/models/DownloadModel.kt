package app.eightpitch.com.models

import android.app.DownloadManager
import android.app.DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED
import android.content.Context
import android.content.Context.DOWNLOAD_SERVICE
import android.net.Uri
import android.os.Build
import android.os.Environment.DIRECTORY_DOCUMENTS
import android.os.Environment.DIRECTORY_DOWNLOADS
import android.widget.Toast
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseModel
import app.eightpitch.com.base.preferences.PreferencesReader


class DownloadModel(
    appContext: Context,
    private val preferencesReader: PreferencesReader
) : BaseModel(appContext) {

    internal fun downloadDocuments(url: String, originalName: String) {

        val downloadDirectory = if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
            DIRECTORY_DOCUMENTS
        } else {
            DIRECTORY_DOWNLOADS
        }

        val request = DownloadManager.Request(Uri.parse(url))
            .setTitle(originalName)
            .setDescription(appContext.getString(R.string.downloading_text))
            .setNotificationVisibility(VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
            .setDestinationInExternalPublicDir(downloadDirectory, originalName)
            .addRequestHeader("Authorization", "Bearer ${preferencesReader.getCachedAccessToken()}")

        val downloadManager =
            appContext.getSystemService(DOWNLOAD_SERVICE) as DownloadManager
        downloadManager.enqueue(request)

        Toast.makeText(
            appContext,
            appContext.getString(R.string.download_completed_text),
            Toast.LENGTH_SHORT
        ).show()
    }
}