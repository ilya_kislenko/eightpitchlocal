package app.eightpitch.com.utils.models

import android.content.Context
import androidx.annotation.WorkerThread
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseModel
import app.eightpitch.com.data.dto.vimeo.VimeoConfigurationResponse
import app.eightpitch.com.extensions.orDefaultValue
import okhttp3.OkHttpClient
import okhttp3.Request
import java.lang.IllegalStateException
import javax.inject.Inject


/**
 * Model that is responsible for handling of
 * Vimeo Videos converting/de-converting
 */
class VimeoModel @Inject constructor(
    appContext: Context
) : BaseModel(appContext) {

    companion object {

        const val BASE_VIMEO_URL = "http://player.vimeo.com/video/"
    }

    /**
     * This method will retrieve a real vimeo url
     *  according to the temporary url received from a backend
     *
     *  @param videoQuality -> one of the quality values, like: 240p, 360p, 720p, etc..
     *  @param vimeoVideoId -> backend id of the video
     *
     *  @return video video url
     */
    @WorkerThread
    fun getVimeoLinkOfVideo(
        videoQuality: Int,
        vimeoVideoId: String?
    ): String {

        val genericVimeoErrorText = appContext.getString(R.string.vimeo_generic_error)

        if (vimeoVideoId.isNullOrEmpty()) {
            throw IllegalStateException(genericVimeoErrorText)
        }

        val request = Request.Builder()
            .url("$BASE_VIMEO_URL$vimeoVideoId/config")
            .get()
            .build()

        val response = OkHttpClient().newCall(request).execute()
        val videoConfig =
            checkOkHttpResponseBodyToError(response, VimeoConfigurationResponse::class.java)

        val progressive = videoConfig.request.files.progressive
        if (progressive.isEmpty()) {
            throw IllegalStateException(genericVimeoErrorText)
        } else {
            val videoLinks = progressive.sortedBy { it.height }
            val link =
                videoLinks.find { it.height == videoQuality }?.url.orDefaultValue(videoLinks.last().url)
            return if (link.isEmpty()) {
                throw IllegalStateException(genericVimeoErrorText)
            } else link
        }
    }
}