package app.eightpitch.com.models

import android.content.Context
import androidx.annotation.WorkerThread
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseModel
import app.eightpitch.com.data.api.APIService
import app.eightpitch.com.data.dto.auth.LoginResponseBody
import app.eightpitch.com.data.dto.changephonenumber.UpdatePhoneRequestBody
import app.eightpitch.com.data.dto.restorepassword.PasswordRecoverySendInitialSmsCodeResponse
import app.eightpitch.com.data.dto.restorepassword.PasswordRecoverySendInitialSmsCodeResponse.Companion.SUCCESS
import app.eightpitch.com.data.dto.sms.EmailPhoneValidationResponse.Companion.EXPIRED
import app.eightpitch.com.data.dto.sms.EmailPhoneValidationResponse.Companion.INVALID
import app.eightpitch.com.data.dto.sms.EmailPhoneValidationResponse.Companion.VALID
import app.eightpitch.com.data.dto.sms.InteractionIdRequestBody
import app.eightpitch.com.data.dto.sms.PhoneVerificationTokenRequestBody
import app.eightpitch.com.data.dto.sms.RequireSMSResponse
import app.eightpitch.com.data.dto.sms.SendTokenLimitResponse
import app.eightpitch.com.data.dto.sms.UpdateEmailRequestBody
import app.eightpitch.com.utils.BasicAuthorization
import app.eightpitch.com.utils.Empty
import java.lang.IllegalStateException

@WorkerThread
class SMSModel(
    appContext: Context,
    private val basicAuthorization: BasicAuthorization,
    private val apiService: APIService
) : BaseModel(appContext) {

    fun requireSMSCodeByPhoneNumber(
        phoneNumber: String
    ): RequireSMSResponse {
        val response = apiService.requireAnSMSCodeByPhoneNumber(
            basicAuthorization.headerEncodedCredentials, phoneNumber).execute()

        return checkRetrofitResponseBodyToError(response)
    }

    fun resendSMSCodeByPhone(
        interactionId: String
    ): Empty {
        val response =
            apiService.resendSMSCodeByPhone(basicAuthorization.headerEncodedCredentials,
                InteractionIdRequestBody(interactionId))
                .execute()
        return checkRetrofitResponseBodyToError(response, Empty::class.java)
    }

    fun requireLimitSMSCodeByPhoneNumber(
        phoneNumber: String
    ): SendTokenLimitResponse {
        val response = apiService.requireAnLimitSMSCodeByPhoneNumber(
            basicAuthorization.headerEncodedCredentials, phoneNumber).execute()

        val smsResponse = checkRetrofitResponseBodyToError(response)

        if (smsResponse.remainingAttempts == 0)
            throw IllegalStateException(appContext.getString(R.string.number_of_attempts_exceeded_text))
        else
            return smsResponse
    }

    fun resendLimitSMSCodeByPhone(
        interactionId: String
    ): Empty {
        val response =
            apiService.resendAnLimitSMSCodeByPhone(basicAuthorization.headerEncodedCredentials,
                InteractionIdRequestBody(interactionId))
                .execute()

        val smsResponse = checkRetrofitResponseBodyToError(response)

        if (smsResponse.remainingAttempts == 0)
            throw HasNotAttempts(appContext.getString(R.string.number_of_attempts_exceeded_text))
        else
            return Empty.instance
    }

    fun requireCodeByEmail(): Empty {
        val response = apiService.sendVerifyCodeToEmail().execute()
        return checkRetrofitResponseBodyToError(response, Empty::class.java)
    }

    fun requireCodeByNewEmail(email: String): Empty {
        val response = apiService.sendVerifyCodeToNewEmail(UpdateEmailRequestBody(email)).execute()
        return checkRetrofitVoidResponseBodyToError(response)
    }

    fun resendCodeByEmail(): Empty {
        val response = apiService.resendVerifyCodeToEmail().execute()
        return checkRetrofitResponseBodyToError(response, Empty::class.java)
    }

    fun requestInvestmentConfirmation(
        investmentId: String,
        projectId: String
    ): SendTokenLimitResponse {
        val rawResponse =
            apiService.requestInvestmentConfirmation(investmentId, projectId).execute()
        return checkRetrofitResponseBodyToError(rawResponse)
    }

    fun sendSmsCodeForPasswordRecovery(email: String): PasswordRecoverySendInitialSmsCodeResponse {
        val rawResponse =
            apiService.sendInitialSmsCode(UpdateEmailRequestBody(email)).execute()
        val response = checkRetrofitResponseBodyToError(rawResponse)
        if (response.status == SUCCESS)
            return response
        else
            throw IllegalStateException(appContext.getString(R.string.not_valid_email))
    }

    fun sendEmailLink(
        interactionId: String,
        code: String
    ): Empty {
        val rawResponse = apiService.sendEmailLink(
            PhoneVerificationTokenRequestBody(interactionId = interactionId, code = code)
        ).execute()
        val response = checkRetrofitResponseBodyToError(rawResponse)
        when (response.result) {
            VALID -> {
                return Empty.instance
            }
            INVALID -> {
                throw IllegalStateException(appContext.getString(R.string.invalid_sms_code_text))
            }
            EXPIRED -> {
                throw IllegalStateException(appContext.getString(R.string.sms_code_expired_text))
            }
            else -> {
                throw IllegalStateException(appContext.getString(R.string.something_go_wrong_exception_text))
            }
        }
    }

    fun changePhoneNumber(
        interactionId: String,
        phone: String,
        code: String
    ): Empty {
        val rawResponse = apiService.changePhoneNumber(
            UpdatePhoneRequestBody(interactionId, phone, code)
        ).execute()
        val response = checkRetrofitResponseBodyToError(rawResponse)
        when (response.result) {
            VALID -> {
                return Empty.instance
            }
            INVALID -> {
                throw IllegalStateException(appContext.getString(R.string.invalid_sms_code_text))
            }
            EXPIRED -> {
                throw IllegalStateException(appContext.getString(R.string.sms_code_expired_text))
            }
            else -> {
                throw IllegalStateException(appContext.getString(R.string.something_go_wrong_exception_text))
            }
        }
    }
}