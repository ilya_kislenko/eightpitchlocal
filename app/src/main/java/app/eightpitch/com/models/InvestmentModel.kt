package app.eightpitch.com.models

import android.content.Context
import androidx.annotation.WorkerThread
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseModel
import app.eightpitch.com.data.api.APIService
import app.eightpitch.com.data.dto.investment.BookInvestmentResponse
import app.eightpitch.com.data.dto.investment.BookInvestmentResponse.Companion.IN_PROGRESS
import app.eightpitch.com.data.dto.investment.CurrencyInputRequestBody
import app.eightpitch.com.data.dto.investment.InvestmentAmountResponseBody
import app.eightpitch.com.data.dto.investment.IsInvestmentAllowedResponse.Companion.TRUE
import app.eightpitch.com.data.dto.investment.ProjectInvestmentInfoResponse
import app.eightpitch.com.data.dto.sms.ConfirmInvestmentResponse
import app.eightpitch.com.data.dto.sms.ConfirmInvestmentResponse.Companion.CANCELED
import app.eightpitch.com.data.dto.sms.ConfirmInvestmentResponse.Companion.EXPIRED_CODE
import app.eightpitch.com.data.dto.sms.ConfirmInvestmentResponse.Companion.SUCCESS
import app.eightpitch.com.data.dto.sms.ConfirmInvestmentResponse.Companion.WAITING
import app.eightpitch.com.data.dto.sms.PhoneVerificationTokenRequestBody
import app.eightpitch.com.utils.Empty
import java.lang.IllegalStateException

@WorkerThread
class InvestmentModel(
    applicationContext: Context,
    private val apiService: APIService
) : BaseModel(applicationContext) {

    fun confirmInvestment(
        investmentId: String,
        projectId: String,
        phoneVerificationTokenRequestBody: PhoneVerificationTokenRequestBody
    ): ConfirmInvestmentResponse {
        val rawResponse =
            apiService.confirmInvestment(investmentId, projectId, phoneVerificationTokenRequestBody)
                .execute()
        val confirmInvestmentResponse = checkRetrofitResponseBodyToError(rawResponse)

        return when (confirmInvestmentResponse.status) {
            SUCCESS -> confirmInvestmentResponse
            WAITING -> throw InvestmentConfirmationException(appContext.getString(R.string.wait_your_status_please_text))
            CANCELED -> throw CancelledInvestmentException(appContext.getString(R.string.your_reservation_was_canceled_text))
            EXPIRED_CODE -> throw InvestmentConfirmationException(appContext.getString(R.string.code_expired_message_text))
            else -> throw InvestmentConfirmationException(appContext.getString(R.string.invalid_sms_code_text))
        }
    }

    fun getProjectInvestmentInfo(
        projectId: String
    ): ProjectInvestmentInfoResponse {
        val rawResponse = apiService.getProjectInvestmentInfo(projectId).execute()
        return checkRetrofitResponseBodyToError(rawResponse)
    }

    fun bookInvestment(
        projectId: String,
        currencyInputRequestBody: CurrencyInputRequestBody
    ): BookInvestmentResponse {
        val rawResponse =
            apiService.bookInvestment(projectId, currencyInputRequestBody).execute()
        val response = checkRetrofitResponseBodyToError(rawResponse)
        if (response.status == IN_PROGRESS)
            return response
        else
            throw IllegalStateException(appContext.getString(R.string.investment_booking_error))
    }

    fun getInvestmentAmount(investmentId: String): InvestmentAmountResponseBody {
        val rawResponse = apiService.getInvestmentAmount(investmentId).execute()
        return checkRetrofitResponseBodyToError(rawResponse)
    }

    fun cancelProjectInvestment(projectId: String, investmentId: String): Empty {
        val rawResponse = apiService.cancelProjectInvestment(projectId, investmentId).execute()
        return checkRetrofitResponseBodyToError(rawResponse, Empty::class.java)
    }

    fun isInvestmentAllowed(projectId: String): String {
        val rawResponse = apiService.isInvestmentAllowed(projectId).execute()
        val response = checkRetrofitResponseBodyToError(rawResponse)
        if (response.status == TRUE)
            return response.whitelistEndDate
        else
            throw IllegalStateException(appContext.getString(R.string.is_allowed_investment_error))
    }
}