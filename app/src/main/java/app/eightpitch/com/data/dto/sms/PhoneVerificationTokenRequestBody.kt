package app.eightpitch.com.data.dto.sms

import com.google.gson.annotations.SerializedName

data class PhoneVerificationTokenRequestBody(
    val interactionId: String,
    @SerializedName("token")
    val code: String
)