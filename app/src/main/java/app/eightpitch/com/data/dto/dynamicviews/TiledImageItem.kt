package app.eightpitch.com.data.dto.dynamicviews

import android.os.Parcel
import android.os.Parcelable
import app.eightpitch.com.extensions.orDefaultValue
import app.eightpitch.com.extensions.readStringSafe
import com.google.gson.annotations.SerializedName

data class TiledImageItem(
    @SerializedName("imageId")
    private val _imageId: String? = "",
    @SerializedName("imageFileName")
    private val _imageFileName: String? = "",
    @SerializedName("header")
    private val _header: String? = "",
    @SerializedName("text")
    private val _text: String? = ""
) : Parcelable {

    val imageId: String
        get() = _imageId.orDefaultValue("")
    val imageFileName: String
        get() = _imageFileName.orDefaultValue("")
    val header: String
        get() = _header.orDefaultValue("")
    val text: String
        get() = _text.orDefaultValue("")

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(imageId)
        parcel.writeString(imageFileName)
        parcel.writeString(header)
        parcel.writeString(text)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<TiledImageItem> {
        override fun createFromParcel(parcel: Parcel): TiledImageItem {
            return TiledImageItem(parcel)
        }

        override fun newArray(size: Int): Array<TiledImageItem?> {
            return arrayOfNulls(size)
        }
    }
}