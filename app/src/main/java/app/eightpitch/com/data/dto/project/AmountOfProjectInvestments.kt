package app.eightpitch.com.data.dto.project

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * The class contains fields with the current amount of invested funds,
 * the amount of investments required to launch the project
 * and the amount of investments for the entire project.
 * Data in the amount of funds and in their percentage.
 *
 * @param currentFundingSum - the amount of accumulated investments in percent
 * @param currentFundingSumInTokens - the amount of collected investments in tokens
 * @param hardCap - the amount of investment for the start of the project in percent
 * @param hardCapInTokens - the amount of investments for launching the project in tokens
 * @param softCap - total investment required in percentage
 * @param softCapInTokens - total amount of required investments in tokens
 */
class AmountOfProjectInvestments(
    @SerializedName("currentFundingSum")
    var currentFundingSum: Int = 0,
    @SerializedName("currentFundingSumInTokens")
    var currentFundingSumInTokens: Long = 0,
    @SerializedName("hardCap")
    var hardCap: Int = 0,
    @SerializedName("hardCapInTokens")
    var hardCapInTokens: Long = 0,
    @SerializedName("softCap")
    var softCap: Int? = null,
    @SerializedName("softCapInTokens")
    var softCapInTokens: Long = 0
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readLong(),
        parcel.readInt(),
        parcel.readLong(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readLong())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(currentFundingSum)
        parcel.writeLong(currentFundingSumInTokens)
        parcel.writeInt(hardCap)
        parcel.writeLong(hardCapInTokens)
        parcel.writeValue(softCap)
        parcel.writeLong(softCapInTokens)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<AmountOfProjectInvestments> {
        override fun createFromParcel(parcel: Parcel): AmountOfProjectInvestments {
            return AmountOfProjectInvestments(parcel)
        }

        override fun newArray(size: Int): Array<AmountOfProjectInvestments?> {
            return arrayOfNulls(size)
        }
    }
}