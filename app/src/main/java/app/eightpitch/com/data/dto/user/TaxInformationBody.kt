package app.eightpitch.com.data.dto.user

import android.os.*
import app.eightpitch.com.extensions.orDefaultValue
import com.google.gson.annotations.SerializedName
import java.util.*

data class TaxInformationBody(
    @SerializedName("churchTaxAttribute")
    private val _churchTaxAttribute: String? = null,
    @SerializedName("churchTaxLiability")
    private val _churchTaxLiability: Boolean? = null,
    @SerializedName("isTaxResidentInGermany")
    private val _isTaxResidentInGermany: Boolean? = null,
    @SerializedName("responsibleTaxOffice")
    private val _responsibleTaxOffice: String? = null,
    @SerializedName("taxNumber")
    private val _taxNumber: String? = null,
) : Parcelable {

    val churchTaxAttribute
        get() = _churchTaxAttribute.orDefaultValue("")
    val churchTaxLiability
        get() = _churchTaxLiability.orDefaultValue(false)
    val isTaxResidentInGermany
        get() = _isTaxResidentInGermany.orDefaultValue(false)
    val responsibleTaxOffice
        get() = _responsibleTaxOffice.orDefaultValue("")
    val taxNumber
        get() = _taxNumber.orDefaultValue("")

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readString(),
        parcel.readString())

    enum class TaxChurch(val displayValue: String) {

        AK("alt-katholisch"),
        AP("apostolisch"),
        AG("evangelisch-augsburgisch"),
        BA("baptistisch"),
        BD("buddhistisch"),
        CA("calvinistisch"),
        CE("anglikanisch \"Church of England\""),
        EV("evangelisch"),
        FCCS("\"First Church of Christ, Scientist\""),
        GF("Quäker"),
        GK("griechisch-katholisch"),
        LT("evangelisch-lutherisch"),
        UN("evangelisch-uniert"),
        FR("franzözisch-reformiert"),
        GO("griechisch-orthodox"),
        HT("Heilige der letzten Tage"),
        IS("islamisch"),
        JD("jüdisch"),
        KA("katholisch"),
        ME("mennonitisch"),
        MT("methodisctisch"),
        ML("moslemisch"),
        NA("neuapostolisch"),
        OB("ohne Bekenntis"),
        OX("orthodox"),
        PR("presbyterianisch"),
        RF("evagelisch-reformiert"),
        RK("römisch-katholisch"),
        RO("russisch-orthodox"),
        SO("sonstige"),
        WR("wallonisch-reformiert"),
        ZJ("Zeugen Jehovas");

        override fun toString(): String {
            return when (this) {
                CE -> name
                else -> name.toLowerCase(Locale.getDefault())
            }
        }

        companion object {

            fun toDisplayValue(value: String): String {
                return values().find { it.name == value }?.displayValue ?: AK.displayValue
            }

            fun fromDisplayValue(displayValue: String): TaxChurch {
                return values().find { it.displayValue == displayValue } ?: AK
            }

            fun getTaxChurches(): List<String> {
                val arrayList = arrayListOf<String>()
                values().forEach {
                    arrayList.add(it.displayValue)
                }
                return arrayList.toList()
            }
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(_churchTaxAttribute)
        parcel.writeValue(_churchTaxLiability)
        parcel.writeValue(_isTaxResidentInGermany)
        parcel.writeString(_responsibleTaxOffice)
        parcel.writeString(_taxNumber)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<TaxInformationBody> {
        override fun createFromParcel(parcel: Parcel): TaxInformationBody {
            return TaxInformationBody(parcel)
        }

        override fun newArray(size: Int): Array<TaxInformationBody?> {
            return arrayOfNulls(size)
        }
    }
}