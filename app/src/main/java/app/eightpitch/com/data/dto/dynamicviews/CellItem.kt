package app.eightpitch.com.data.dto.dynamicviews

import android.os.Parcel
import android.os.Parcelable
import android.widget.TableLayout
import app.eightpitch.com.extensions.orDefaultValue
import app.eightpitch.com.extensions.readStringSafe
import app.eightpitch.com.ui.dynamicdetails.DynamicProjectDetailsChildFragment
import com.google.gson.annotations.SerializedName

/**
 * It's an object that represents a single cell in [TableLayout]
 * on a [DynamicProjectDetailsChildFragment].
 */
data class CellItem(
    @SerializedName("text")
    private val _text: String? = ""
) : Parcelable {

    val text: String
        get() = _text.orDefaultValue("")

    constructor(parcel: Parcel) : this(parcel.readStringSafe())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(text)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<CellItem> {
        override fun createFromParcel(parcel: Parcel): CellItem {
            return CellItem(parcel)
        }

        override fun newArray(size: Int): Array<CellItem?> {
            return arrayOfNulls(size)
        }
    }
}
