package app.eightpitch.com.data.dto.twofactor

import com.google.gson.annotations.SerializedName

data class TwoFactorAuthRequestBody(
    @SerializedName("twoFactorAuthCode")
    val twoFactorAuthCode: String
)