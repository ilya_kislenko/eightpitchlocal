package app.eightpitch.com.data.dto.dynamicviews

import android.os.Parcel
import android.os.Parcelable
import app.eightpitch.com.extensions.orDefaultValue
import app.eightpitch.com.extensions.readStringSafe
import com.google.gson.annotations.SerializedName

data class TextLinkItem(
    @SerializedName("linkUrl")
    private val _linkUrl: String? = "",
    @SerializedName("position")
    private val _position: Int? = 0,
    @SerializedName("text")
    private val _text: String? = "",
    @SerializedName("label")
    private val _label: String? = ""
) : Parcelable {

    val linkUrl: String
        get() = _linkUrl.orDefaultValue("")
    val position: Int
        get() = _position.orDefaultValue(0)
    val text: String
        get() = _text.orDefaultValue("")
    val label: String
        get() = _label.orDefaultValue("")

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readInt(),
        parcel.readStringSafe(),
        parcel.readStringSafe())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(linkUrl)
        parcel.writeInt(position)
        parcel.writeString(text)
        parcel.writeString(label)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<TextLinkItem> {
        override fun createFromParcel(parcel: Parcel): TextLinkItem {
            return TextLinkItem(parcel)
        }

        override fun newArray(size: Int): Array<TextLinkItem?> {
            return arrayOfNulls(size)
        }
    }
}