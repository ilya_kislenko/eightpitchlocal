package app.eightpitch.com.data.dto.projectdetails

import com.google.gson.annotations.SerializedName


class ProjectInitiator(
    @SerializedName("email")
    var email: String = "",
    @SerializedName("firstName")
    var firstName: String = "",
    @SerializedName("lastName")
    var lastName: String = "",
    @SerializedName("phone")
    var phone: String = "",
    @SerializedName("prefix")
    var prefix: String = ""
)