package app.eightpitch.com.data.dto.filter

import android.content.Context
import app.eightpitch.com.R

enum class AmountRange(val displayValue: Int) {
    RANGE_0_1000_EUR(R.string.filter_amount_range_0_1000),
    RANGE_1001_5000_EUR(R.string.filter_amount_range_1000_5000),
    RANGE_5001_10000_EUR(R.string.filter_amount_range_5000_10000),
    RANGE_10001_25000_EUR(R.string.filter_amount_range_10000_25000),
    RANGE_25001_50000_EUR(R.string.filter_amount_range_25000_50000),
    RANGE_50001_100000_EUR(R.string.filter_amount_range_50000_100000),
    RANGE_MORE_THAN_100000_EUR(R.string.filter_amount_range_more_than_100000);

    override fun toString(): String {
        return this.name
    }

    companion object {

        fun toDisplayValue(context: Context, value: String): String {
            return values().find { it.name == value }?.displayValue?.let { context.getString(it) }
                ?: context.getString(R.string.filter_amount_range_0_1000)
        }

        fun fromDisplayValue(context: Context?, displayValue: String): AmountRange {
            return values().find { context?.getString(it.displayValue) == displayValue }
                ?: RANGE_MORE_THAN_100000_EUR
        }
    }
}