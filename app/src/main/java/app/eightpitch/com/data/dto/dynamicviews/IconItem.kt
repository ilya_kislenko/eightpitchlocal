package app.eightpitch.com.data.dto.dynamicviews

import android.os.Parcel
import android.os.Parcelable
import android.widget.LinearLayout
import app.eightpitch.com.extensions.orDefaultValue
import app.eightpitch.com.extensions.readStringSafe
import app.eightpitch.com.ui.dynamicdetails.DynamicProjectDetailsChildFragment
import com.google.gson.annotations.SerializedName

/**
 * It's an object that represents a single icon in [LinearLayout]
 * on a [DynamicProjectDetailsChildFragment].
 */
data class IconItem(
    @SerializedName("imageId")
    private val _imageId: String? = "",
    @SerializedName("imageFileName")
    private val _imageFileName: String? = "",
    @SerializedName("link")
    private val _link: String? = ""
) : Parcelable {

    val imageId: String
        get() = _imageId.orDefaultValue("")
    val imageFileName: String
        get() = _imageFileName.orDefaultValue("")
    val link: String
        get() = _link.orDefaultValue("")

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(imageId)
        parcel.writeString(imageFileName)
        parcel.writeString(link)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<IconItem> {
        override fun createFromParcel(parcel: Parcel): IconItem {
            return IconItem(parcel)
        }

        override fun newArray(size: Int): Array<IconItem?> {
            return arrayOfNulls(size)
        }
    }
}
