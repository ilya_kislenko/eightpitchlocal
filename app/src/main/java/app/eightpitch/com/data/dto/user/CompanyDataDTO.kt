package app.eightpitch.com.data.dto.user

import android.os.*
import app.eightpitch.com.extensions.readStringSafe
import com.google.gson.annotations.SerializedName

data class CompanyDataDTO(
    @SerializedName("legalForm")
    val companyType: String = "OTHER",
    @SerializedName("name")
    val companyName: String = ""
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(companyType)
        parcel.writeString(companyName)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<CompanyDataDTO> {
        override fun createFromParcel(parcel: Parcel): CompanyDataDTO {
            return CompanyDataDTO(parcel)
        }

        override fun newArray(size: Int): Array<CompanyDataDTO?> {
            return arrayOfNulls(size)
        }
    }

    enum class CompanyType(val displayValue: String) {
        AG("AG"),
        EG("eG"),
        EINGETRAGENER_KAUFMANN("Eingetragener Kaufmann (e.K.)"),
        FREIBERUFLER_SELBSTSTÄNDIG("Freiberufler / Selbstständig"),
        GBR_BGBGESELLSCHAFT("GbR / BGBGesellschaft"),
        GMBH("GmbH"),
        GMBH_CO_KG("GmbH &amp; Co. KG"),
        KG("KG"),
        KGAA("KGaA"),
        OHG("OHG"),
        OHG_CO_KG("OHG &amp; Co. KG"),
        UG("UG (haftungsbeschränkt)"),
        LIMITED("Limited (UK)"),
        EINGETRAGENER_VEREIN("Eingetragener Verein"),
        STIFTUNG("tiftung"),
        OTHER("Other");

        override fun toString(): String {
            return this.name
        }

        companion object {
            fun fromDisplayValue(displayValue: String): CompanyType {
                return values().find { it.displayValue == displayValue } ?: AG
            }
        }
    }
}