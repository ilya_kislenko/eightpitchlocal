package app.eightpitch.com.data.dto.projectdetails

import com.google.gson.annotations.SerializedName


data class CustomAdditionalField(
    @SerializedName("fieldName")
    var fieldName: String = "",
    @SerializedName("fieldText")
    var fieldText: String = "",
    @SerializedName("id")
    var id: String = ""
)