package app.eightpitch.com.data.dto.dynamicviews

import android.os.Parcel
import android.os.Parcelable
import app.eightpitch.com.extensions.orDefaultValue
import app.eightpitch.com.extensions.readStringSafe
import com.google.gson.annotations.SerializedName

data class ImageLink(
    @SerializedName("imageUrl")
    private val _imageId: String? = "",
    @SerializedName("imageFileName")
    private val _imageFileName: String? = "",
    @SerializedName("link")
    private val _link: String? = "",
    @SerializedName("description")
    private val _description: String? = ""
) : Parcelable {

    val imageUrl: String
        get() = _imageId.orDefaultValue("")
    val imageFileName: String
        get() = _imageFileName.orDefaultValue("")
    val link: String
        get() = _link.orDefaultValue("")
    val description: String
        get() = _description.orDefaultValue("")

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(imageUrl)
        parcel.writeString(imageFileName)
        parcel.writeString(link)
        parcel.writeString(description)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<ImageLink> {
        override fun createFromParcel(parcel: Parcel): ImageLink {
            return ImageLink(parcel)
        }

        override fun newArray(size: Int): Array<ImageLink?> {
            return arrayOfNulls(size)
        }
    }
}