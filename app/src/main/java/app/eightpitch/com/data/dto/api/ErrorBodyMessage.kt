package app.eightpitch.com.data.dto.api

data class ErrorBodyMessage(val message: String = "", val exceptionCode: Int = 1012)