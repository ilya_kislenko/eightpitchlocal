package app.eightpitch.com.data.dto.twofactor

import com.google.gson.annotations.SerializedName

class TwoFactorQrCodeResponse (
    @SerializedName("keyId")
    val keyId: String,
    @SerializedName("secret")
    val secret: String
)