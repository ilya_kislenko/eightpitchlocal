package app.eightpitch.com.data.dto.projectdetails

import androidx.annotation.StringDef
import app.eightpitch.com.extensions.NO_TYPE
import com.google.gson.annotations.SerializedName

data class ProjectPage(
    @SerializedName("")
    var additionalFields: AdditionalFields = AdditionalFields(),
    @SerializedName("blockConstructor")
    var blockConstructor: String = "",
    @SerializedName("companyName")
    var companyName: String = "",
    @SerializedName("externalId")
    var externalId: String = "",
    @SerializedName("firstInvestmentArguments")
    var firstInvestmentArguments: String = "",
    @SerializedName("firstQuoteTitle")
    var firstQuoteTitle: String = "",
    @SerializedName("id")
    var id: String = "",
    @SerializedName("projectPageFiles")
    var projectPageFiles: List<ProjectsFile> = arrayListOf(),
    @SerializedName("projectPageRejections")
    var projectPageRejections: List<ProjectPageRejection> = arrayListOf(),
    @SerializedName("projectPageStatus")
    @FProjectPageStatus
    var projectPageStatus: String = NO_TYPE,
    @SerializedName("promoVideo")
    var promoVideo: String = "",
    @SerializedName("quoteText")
    var quoteText: String = "",
    @SerializedName("secondInvestmentArguments")
    var secondInvestmentArguments: String = "",
    @SerializedName("secondQuoteTitle")
    var secondQuoteTitle: String = "",
    @SerializedName("slogan")
    var slogan: String = "",
    @SerializedName("thirdInvestmentArguments")
    var thirdInvestmentArguments: String = "",
    @SerializedName("url")
    var url: String = ""
){
    companion object{

        const val DRAFT = "DRAFT"
        const val WAITING = "WAITING"
        const val REJECTED = "REJECTED"
        const val APPROVED = "APPROVED"
        const val PUBLISHED = "PUBLISHED"

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(NO_TYPE, DRAFT, WAITING, REJECTED, APPROVED, PUBLISHED)
        annotation class FProjectPageStatus
    }
}