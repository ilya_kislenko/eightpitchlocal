package app.eightpitch.com.data.dto.sms

import com.google.gson.annotations.SerializedName

data class PhoneInteractionIdRequestBody(
    @SerializedName("interactionId")
    val interactionId: String
)