package app.eightpitch.com.data.dto.dynamicviews

import android.os.*
import app.eightpitch.com.extensions.*

/**
 * Parsed information from Json for BarChart and LineChart representation
 * @see com.github.mikephil.charting.charts.BarChart
 * @see com.github.mikephil.charting.charts.LineChart
 */
data class ChartData(
    val label: String = "",
    val rgbColor: String = "rgba(245, 76, 76, 0.5)",
    val hashColor: String = "#F54C4C",
    val xValuesList: List<Float> = listOf(),
    val yValuesList: List<Float> = listOf()
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readListSafe(mutableListOf<Float>(), ChartData::class.java.classLoader!!),
        parcel.readListSafe(mutableListOf<Float>(), ChartData::class.java.classLoader!!))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(label)
        parcel.writeString(rgbColor)
        parcel.writeString(hashColor)
        parcel.writeList(xValuesList)
        parcel.writeList(yValuesList)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<ChartData> {
        override fun createFromParcel(parcel: Parcel): ChartData {
            return ChartData(parcel)
        }

        override fun newArray(size: Int): Array<ChartData?> {
            return arrayOfNulls(size)
        }
    }
}