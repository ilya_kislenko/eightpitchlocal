package app.eightpitch.com.data.dto.project

import android.os.Parcel
import android.os.Parcelable

data class InvestorsCount(
    val totalCount: Int = 0
) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readInt())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(totalCount)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<InvestorsCount> {
        override fun createFromParcel(parcel: Parcel): InvestorsCount {
            return InvestorsCount(parcel)
        }

        override fun newArray(size: Int): Array<InvestorsCount?> {
            return arrayOfNulls(size)
        }
    }
}