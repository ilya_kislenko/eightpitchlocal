package app.eightpitch.com.data.dto.projectdetails

import androidx.annotation.StringDef
import app.eightpitch.com.extensions.NO_TYPE
import com.google.gson.annotations.SerializedName
import java.math.BigDecimal
import java.math.BigDecimal.ZERO

class TokenParametersDocumentDividends(
    @SerializedName("day")
    var day: Int = 0,
    @SerializedName("dividendAmountPerToken")
    var dividendAmountPerToken: BigDecimal = ZERO,
    @SerializedName("dividendsType")
    @DividendsType
    var dividendsType: String = NO_TYPE,
    @SerializedName("month")
    var month: Int = 0
){
    companion object{

        const val FIXED_DATE_YEARLY = "FIXED_DATE_YEARLY"
        const val IRREGULAR_DATE = "IRREGULAR_DATE"

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(NO_TYPE, FIXED_DATE_YEARLY, IRREGULAR_DATE)
        annotation class DividendsType
    }
}