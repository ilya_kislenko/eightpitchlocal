package app.eightpitch.com.data.dto.payment

import com.google.gson.annotations.SerializedName

data class SaveSequpayDataRequest(
    @SerializedName("bankAccount")
    val bankAccount: BankAccount
)