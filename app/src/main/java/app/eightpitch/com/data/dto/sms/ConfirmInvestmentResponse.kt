package app.eightpitch.com.data.dto.sms

import androidx.annotation.StringDef
import app.eightpitch.com.extensions.NO_TYPE

data class ConfirmInvestmentResponse(
    @InvestmentResult val status: String = NO_TYPE
) {

    companion object {

        const val SUCCESS = "SUCCESS"
        const val WAITING = "WAITING"
        const val INVALID_CODE = "INVALID_CODE"
        const val CANCELED = "CANCELED"
        const val EXPIRED_CODE = "EXPIRED_CODE"

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(SUCCESS, WAITING, INVALID_CODE, CANCELED, EXPIRED_CODE, NO_TYPE)
        annotation class InvestmentResult
    }
}