package app.eightpitch.com.data.dto.twofactor

import androidx.annotation.StringDef
import app.eightpitch.com.extensions.NO_TYPE
import com.google.gson.annotations.SerializedName

data class TwoFactorVerifyCodeResponse(
    @SerializedName("interactionId")
    val interactionId: String,
    @SerializedName("result")
    @TwoFactorVerifyCodeStatusType
    val result: String,
    @SerializedName("tokenResponse")
    val tokenResponse: TwoFactorTokenResponse? = TwoFactorTokenResponse(),
    @SerializedName("valid")
    val valid: Boolean,
    @SerializedName("verificationTypes")
    val verificationTypes: ArrayList<String>
){
    companion object{

        //verification types
        const val PHONE_VERIFICATION = "PHONE_VERIFICATION"
        const val TWO_FACTOR_AUTH_VERIFICATION = "TWO_FACTOR_AUTH_VERIFICATION"
        //end region verification types

        const val SUCCESS = "SUCCESS"
        const val UNSUCCESS = "UNSUCCESS"
        const val INACTIVE = "INACTIVE"

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(NO_TYPE, SUCCESS, UNSUCCESS, INACTIVE)
        annotation class TwoFactorVerifyCodeStatusType
    }
}