package app.eightpitch.com.data.dto.user

import android.content.Context
import android.os.*
import androidx.annotation.StringDef
import app.eightpitch.com.R
import app.eightpitch.com.data.dto.questionnaire.InvestorQualificationRequestBody
import app.eightpitch.com.data.dto.user.User.PrefixType.MR
import app.eightpitch.com.extensions.*
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

/**
 * User could have a Private investor or Institutional investor role, so, for now we are receiving
 * a bunch of fields for this two types in one object
 */
data class User(
    @SerializedName("actionId")
    private val _actionId: String? = null,
    @SerializedName("accountOwner")
    private val _accountOwner: String? = null,
    @SerializedName("address")
    private val _userAddress: UserAddress? = null,
    @SerializedName("companyName")
    private val _companyName: String? = null,
    @SerializedName("companyType")
    private val _companyType: String? = null,
    @SerializedName("commercialRegisterNumber")
    private val _commercialRegisterNumber: String? = null,
    @SerializedName("dateOfBirth")
    private val _dateOfBirth: String? = null,
    @SerializedName("email")
    private val _email: String? = null,
    @SerializedName("emailConfirmed")
    private val _isEmailConfirmed: Boolean? = null,
    @SerializedName("externalId")
    private val _externalId: String? = null,
    @SerializedName("firstName")
    private val _firstName: String? = null,
    @SerializedName("group")
    @RoleType private val _role: String? = null,
    @SerializedName("language")
    @LanguageRequestBody.Companion.UserLanguage private val _language: String? = null,
    @SerializedName("lastName")
    private val _lastName: String? = null,
    @SerializedName("nationality")
    private val _nationality: String? = null,
    @SerializedName("newNotifications")
    private val _isNotificationsEnabled: Boolean? = null,
    @SerializedName("phone")
    private val _phoneNumber: String? = null,
    @SerializedName("phoneNumberConfirmed")
    private val _isPhoneNumberConfirmed: Boolean? = null,
    @SerializedName("investmentExperienceQuestionnaire")
    val investmentExperienceQuestionnaire: InvestmentExperienceQuestionnaire? = null,
    @SerializedName("investorQualificationForm")
    val investorQualificationForm: InvestorQualificationRequestBody? = null,
    @SerializedName("prefix")
    private val _prefix: String? = null,
    @SerializedName("sex")
    @SexType private val _sex: String? = null,
    @SerializedName("status")
    @StatusType private val _status: String? = null,
    @SerializedName("subscribeToEmails")
    private val _isSubscribedToEmails: Boolean? = null,
    @SerializedName("taxInformation")
    private val _taxInformation: TaxInformationBody? = null,
    @SerializedName("twoFactorAuthenticationEnabled")
    private val _isTwoFactorAuthenticationEnabled: Boolean? = null,
    @SerializedName("twoFactorAuthenticationType")
    @TwoFactorAuthenticationType
    private val _twoFactorAuthenticationType: String? = null,
    @SerializedName("webidUpgradeProcessBlock")
    private val _isWebIdUpgradeProcessBlocked: Boolean? = null,
    @SerializedName("placeOfBirth")
    private val _placeOfBirth: String? = null,
    @SerializedName("deleteAccountProcess")
    private val _deleteAccountProcess: Boolean? = null,
    @SerializedName("usTaxLiability")
    private val _usTaxLiability: Boolean? = null,
    @SerializedName("iban")
    private val _iban: String? = null,
    @SerializedName("politicallyExposedPerson")
    private val _politicallyExposedPerson: Boolean? = null,
) : Parcelable {

    val firstName
        get() = _firstName ?: ""
    val lastName
        get() = _lastName ?: ""
    val language
        get() = _language.orDefaultValue(Locale.getDefault().getUserLanguage())
    val dateOfBirth
        get() = _dateOfBirth ?: ""
    val actionId
        get() = _actionId ?: ""
    val accountOwner
        get() = _accountOwner ?: ""
    val userAddress
        get() = _userAddress ?: UserAddress()
    val companyName
        get() = _companyName ?: ""
    val companyType
        get() = _companyType ?: CompanyDataDTO.CompanyType.OTHER.name
    val commercialRegisterNumber
        get() = _commercialRegisterNumber ?: ""
    val email
        get() = _email ?: ""
    val isEmailConfirmed
        get() = _isEmailConfirmed ?: false
    val externalId
        get() = _externalId ?: ""
    val role
        get() = _role ?: NO_TYPE
    val nationality
        get() = _nationality ?: ""
    val isNotificationsEnabled
        get() = _isNotificationsEnabled ?: false
    val phoneNumber
        get() = _phoneNumber ?: ""
    val isPhoneNumberConfirmed
        get() = _isPhoneNumberConfirmed ?: false
    val prefix
        get() = _prefix ?: MR.name
    val sex
        get() = _sex ?: NO_TYPE
    val status
        get() = _status ?: NO_TYPE
    val isSubscribedToEmails
        get() = _isSubscribedToEmails ?: false
    val taxInformation: TaxInformationBody?
        get() = _taxInformation
    val isShowTaxUpdating
        get() = _taxInformation == null
    val isTwoFactorAuthenticationEnabled
        get() = _isTwoFactorAuthenticationEnabled ?: false
    val twoFactorAuthenticationType
        get() = _twoFactorAuthenticationType ?: ""
    val isWebIdUpgradeProcessBlocked
        get() = _isWebIdUpgradeProcessBlocked ?: false
    val placeOfBirth
        get() = _placeOfBirth ?: ""
    val deleteAccountProcess
        get() = _deleteAccountProcess ?: false
    val usTaxLiability
        get() = _usTaxLiability ?: false
    val iban
        get() = _iban ?: ""
    val politicallyExposedPerson
        get() = _politicallyExposedPerson ?: false
    val isInvestor
        get() = (role == PRIVATE_INVESTOR || role == INSTITUTIONAL_INVESTOR)
    val isInitiator
        get() = role == PROJECT_INITIATOR

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readParcelable(UserAddress::class.java.classLoader),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readString(),
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readParcelable(InvestmentExperienceQuestionnaire::class.java.classLoader),
        parcel.readParcelable(InvestorQualificationRequestBody::class.java.classLoader),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readParcelable(TaxInformationBody::class.java.classLoader),
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readString(),
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readString(),
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readString(),
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean)

    internal fun getWelcomeAppeal(context: Context?) =
        "${context?.let { PrefixType.toDisplayValue(it, prefix) }} $lastName".orDefaultValue("")

    /**
     * This is a specific case when the user have completed an account upgrade flow, but still didn't get
     * an approval from admin, so technically he is between lvl 1 and lvl 2, but account upgrade flow isn't available
     * anymore
     */
    internal fun isWaitingForApproval() =
        (isInvestor && status != LEVEL_2 && investmentExperienceQuestionnaire != null)

    /**
     * We can upgrade account only if the user's status is not a [LEVEL_2],
     * he is an investor, and not completed questionnaire flow.
     *
     * If the user is not an investor, return the false
     * If the user of the 2nd level and the investor return the false
     * If the user is waiting for the administrator's approval to upgrade the account, we return the false
     */
    internal fun canUpgradeAccount() = when {
        !isInvestor || status == LEVEL_2 || isWaitingForApproval() -> false
        else -> true
    }

    enum class PrefixType(val displayValue: Int) {
        MR(R.string.mr_prefix_display_value),
        MS(R.string.ms_prefix_display_value),
        MR_DR(R.string.mr_dr_prefix_display_value),
        MS_DR(R.string.ms_dr_prefix_display_value),
        MR_PROF_DR(R.string.mr_prof_dr_prefix_display_value),
        MS_PROF_DR(R.string.mrs_prof_dr_prefix_display_value);

        override fun toString(): String {
            return this.name
        }

        companion object {

            fun toDisplayValue(context: Context, value: String): String {
                return values().find { it.name == value }?.displayValue?.let { context.getString(it) }
                       ?: context.getString(R.string.mr_prefix_display_value)
            }

            fun fromDisplayValue(context: Context?, displayValue: String): PrefixType {
                return values().find { context?.getString(it.displayValue) == displayValue } ?: MR
            }
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(_actionId)
        parcel.writeString(_accountOwner)
        parcel.writeParcelable(_userAddress, flags)
        parcel.writeString(_companyName)
        parcel.writeString(_companyType)
        parcel.writeString(_commercialRegisterNumber)
        parcel.writeString(_dateOfBirth)
        parcel.writeString(_email)
        parcel.writeValue(_isEmailConfirmed)
        parcel.writeString(_externalId)
        parcel.writeString(_firstName)
        parcel.writeString(_role)
        parcel.writeString(_language)
        parcel.writeString(_lastName)
        parcel.writeString(_nationality)
        parcel.writeValue(_isNotificationsEnabled)
        parcel.writeString(_phoneNumber)
        parcel.writeValue(_isPhoneNumberConfirmed)
        parcel.writeParcelable(investmentExperienceQuestionnaire, flags)
        parcel.writeString(_prefix)
        parcel.writeString(_sex)
        parcel.writeString(_status)
        parcel.writeValue(_isSubscribedToEmails)
        parcel.writeParcelable(_taxInformation, flags)
        parcel.writeValue(_isTwoFactorAuthenticationEnabled)
        parcel.writeString(_twoFactorAuthenticationType)
        parcel.writeValue(_isWebIdUpgradeProcessBlocked)
        parcel.writeString(_placeOfBirth)
        parcel.writeValue(_deleteAccountProcess)
        parcel.writeValue(_usTaxLiability)
        parcel.writeString(_iban)
        parcel.writeValue(_politicallyExposedPerson)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<User> {

        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }

        const val UNVERIFIED = "UNVERIFIED"
        const val LEVEL_1 = "LEVEL_1"
        const val KYC_WEB_ID = "KYC_WEB_ID"
        const val KYC_FINISH_CALL = "KYC_FINISH_CALL"
        const val KYC_INTERNAL = "KYC_INTERNAL"
        const val WAITING_BLOCKCHAIN_CONFIRMATION = "WAITING_BLOCKCHAIN_CONFIRMATION"
        const val LEVEL_2 = "LEVEL_2"
        const val INACTIVE = "INACTIVE"
        const val ADMIN = "ADMIN"

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(
            NO_TYPE, UNVERIFIED, LEVEL_1, KYC_WEB_ID, KYC_FINISH_CALL,
            KYC_INTERNAL, WAITING_BLOCKCHAIN_CONFIRMATION, LEVEL_2, INACTIVE, ADMIN
        )
        annotation class StatusType

        const val MALE = "MALE"
        const val FEMALE = "FEMALE"

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(NO_TYPE, MALE, FEMALE, OTHER)
        annotation class SexType

        const val INSTITUTIONAL_INVESTOR = "INSTITUTIONAL_INVESTOR"
        const val PRIVATE_INVESTOR = "PRIVATE_INVESTOR"
        const val PROJECT_INITIATOR = "PROJECT_INITIATOR"

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(NO_TYPE, INSTITUTIONAL_INVESTOR, PRIVATE_INVESTOR, PROJECT_INITIATOR)
        annotation class RoleType

        const val TOTP = "TOTP"
        const val SMS = "SMS"

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(NO_TYPE, TOTP, SMS)
        annotation class TwoFactorAuthenticationType
    }
}