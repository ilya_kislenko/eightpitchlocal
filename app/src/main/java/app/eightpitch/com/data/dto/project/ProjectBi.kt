package app.eightpitch.com.data.dto.project

import android.os.Parcel
import android.os.Parcelable
import app.eightpitch.com.extensions.readBigDecimal
import java.math.BigDecimal

data class ProjectBi(
    val averageInvestment: BigDecimal = BigDecimal.ZERO,
    val capitalInvested: BigDecimal = BigDecimal.ZERO,
    val investors: InvestorsCount = InvestorsCount(),
    val maxInvestment: BigDecimal = BigDecimal.ZERO,
    val minInvestment: BigDecimal = BigDecimal.ZERO
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readBigDecimal(),
        parcel.readBigDecimal(),
        parcel.readParcelable(InvestorsCount::class.java.classLoader) ?: InvestorsCount(),
        parcel.readBigDecimal(),
        parcel.readBigDecimal())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeSerializable(averageInvestment)
        parcel.writeSerializable(capitalInvested)
        parcel.writeParcelable(investors, flags)
        parcel.writeSerializable(maxInvestment)
        parcel.writeSerializable(minInvestment)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<ProjectBi> {
        override fun createFromParcel(parcel: Parcel): ProjectBi {
            return ProjectBi(parcel)
        }

        override fun newArray(size: Int): Array<ProjectBi?> {
            return arrayOfNulls(size)
        }
    }
}