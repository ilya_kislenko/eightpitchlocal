package app.eightpitch.com.data.dto.webid

import android.os.Parcel
import android.os.Parcelable
import app.eightpitch.com.data.dto.user.User.PrefixType.MR
import app.eightpitch.com.data.dto.user.UserAddress
import app.eightpitch.com.extensions.readStringSafe
import app.eightpitch.com.utils.Constants.DD_MM_YYYY_HH_MM_SS_ZONE
import app.eightpitch.com.utils.DateUtils.parseByFormat
import app.eightpitch.com.utils.DateUtils.parseByFormatWithoutMMSS
import com.google.gson.annotations.SerializedName

data class WebIdUserRequestBody(
    val address: UserAddress = UserAddress(),
    @SerializedName("dateOfBirth")
    val _dateOfBirth: String = "",
    val firstName: String = "",
    @SerializedName("internalInfo")
    val webIdPersonalInfo: WebIdPersonalInfo = WebIdPersonalInfo(),
    val lastName: String = "",
    val prefix: String = MR.name
) : Parcelable {

    val dateOfBirth: Long
        get() = _dateOfBirth.parseByFormatWithoutMMSS()

    val fullName = "$firstName $lastName"

    constructor(parcel: Parcel) : this(
        parcel.readParcelable(UserAddress::class.java.classLoader) ?: UserAddress(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readParcelable(WebIdPersonalInfo::class.java.classLoader) ?: WebIdPersonalInfo(),
        parcel.readStringSafe(),
        parcel.readStringSafe()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(address, flags)
        parcel.writeString(_dateOfBirth)
        parcel.writeString(firstName)
        parcel.writeParcelable(webIdPersonalInfo, flags)
        parcel.writeString(lastName)
        parcel.writeString(prefix)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<WebIdUserRequestBody> {
        override fun createFromParcel(parcel: Parcel): WebIdUserRequestBody {
            return WebIdUserRequestBody(parcel)
        }

        override fun newArray(size: Int): Array<WebIdUserRequestBody?> {
            return arrayOfNulls(size)
        }
    }
}