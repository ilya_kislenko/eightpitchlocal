package app.eightpitch.com.data.dto.auth

import com.google.gson.annotations.SerializedName

data class LoginRequestBody(
    val deviceId: String,
    val password: String,
    @SerializedName("phone")
    val phoneNumber: String,
    val twoFactorAuthCode: String
)