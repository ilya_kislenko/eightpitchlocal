package app.eightpitch.com.data.dto.projectdetails

import androidx.annotation.StringDef
import app.eightpitch.com.extensions.NO_TYPE
import app.eightpitch.com.utils.Constants.DEFAULT_DATA_PATTERN
import app.eightpitch.com.utils.DateUtils.parseByFormat
import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class TokenParametersDocument(
    @SerializedName("assetBasedFees")
    var assetBasedFees: BigDecimal = BigDecimal.ZERO,
    @SerializedName("assetId")
    var assetId: String = "",
    @SerializedName("breakable")
    var breakable: Int = -1,
    @SerializedName("currentSupply")
    var currentSupply: Int = -1,
    @SerializedName("distributorId")
    var distributorId: String = "",
    @SerializedName("dsoProjectFinishDate")
    var dsoProjectFinishDate: String = "",
    @SerializedName("dsoProjectStartDate")
    var dsoProjectStartDate: String = "",
    @SerializedName("hardCap")
    var hardCap: Long = 0,
    @SerializedName("id")
    var id: Long = -1,
    @SerializedName("investmentStepSize")
    var investmentStepSize: Long = 0,
    @SerializedName("isin")
    var isin: String = "",
    @SerializedName("issuerId")
    var issuerId: String = "",
    @SerializedName("minimumInvestmentAmount")
    var minimumInvestmentAmount: Long = 0,
    @SerializedName("multisignRelease")
    var multisignRelease: Boolean = false,
    @SerializedName("nominalValue")
    var nominalValue: Int = 0,
    @SerializedName("productType")
    var productType: String = "",
    @SerializedName("projectId")
    var projectId: Long = -1,
    @SerializedName("shortCut")
    var shortCut: String = "",
    @SerializedName("singleInvestorOwnershipLimit")
    var singleInvestorOwnershipLimit: BigDecimal = BigDecimal.ZERO,
    @SerializedName("softCap")
    var softCap: Long? = null,
    @SerializedName("tokenId")
    var tokenId: String = "",
    @SerializedName("tokenParametersDocumentAnnualMeetings")
    var tokenParametersDocumentAnnualMeetings: TokenParametersDocumentAnnualMeetings = TokenParametersDocumentAnnualMeetings(),
    @SerializedName("tokenParametersDocumentChangeability")
    var tokenParametersDocumentChangeability: TokenParametersDocumentChangeability = TokenParametersDocumentChangeability(),
    @SerializedName("tokenParametersDocumentDividends")
    var tokenParametersDocumentDividends: TokenParametersDocumentDividends = TokenParametersDocumentDividends(),
    @SerializedName("tokenParametersDocumentRejections")
    var tokenParametersDocumentRejections: List<TokenParametersDocumentRejection> =
        arrayListOf(),
    @SerializedName("tokenParametersDocumentStatus")
    @ParametersDocumentStatus
    var tokenParametersDocumentStatus: String = NO_TYPE,
    @SerializedName("tokenParametersDocumentTradeability")
    var tokenParametersDocumentTradeability: TokenParametersDocumentTradeability = TokenParametersDocumentTradeability(),
    @SerializedName("tokenParametersDocumentTransferability")
    var tokenParametersDocumentTransferability: TokenParametersDocumentTransferability = TokenParametersDocumentTransferability(),
    @SerializedName("totalNumberOfTokens")
    var totalNumberOfTokens: Long = 0,
    @SerializedName("voting")
    var voting: Boolean = false
) {

    val projectFinishDate
        get() = dsoProjectFinishDate.parseByFormat(pattern = DEFAULT_DATA_PATTERN)

    val projectStartDate
        get() = dsoProjectStartDate.parseByFormat(pattern = DEFAULT_DATA_PATTERN)

    companion object {

        const val DRAFT = "DRAFT"
        const val WAITING = "WAITING"
        const val REJECTED = "APPROVED"
        const val REQUESTED_TOKEN_CREATION = "REQUESTED_TOKEN_CREATION"
        const val TOKEN_CREATION_IN_PROGRESS = "TOKEN_CREATION_IN_PROGRESS"
        const val FIXED_DATE_YEARLY = "FIXED_DATE_YEARLY"
        const val IRREGULAR_DATE = "IRREGULAR_DATE"
        const val TOKEN_CREATED = "TOKEN_CREATED"

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(NO_TYPE, DRAFT, WAITING, REJECTED, REQUESTED_TOKEN_CREATION, TOKEN_CREATION_IN_PROGRESS,
            FIXED_DATE_YEARLY, IRREGULAR_DATE, TOKEN_CREATED)
        annotation class ParametersDocumentStatus
    }
}