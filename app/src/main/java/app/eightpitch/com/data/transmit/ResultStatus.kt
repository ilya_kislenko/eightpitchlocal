package app.eightpitch.com.data.transmit

enum class ResultStatus {
    SUCCESS, FAILED
}