package app.eightpitch.com.data.dto.projectdetails

import app.eightpitch.com.extensions.NO_TYPE
import com.google.gson.annotations.SerializedName

class TokenParametersDocumentTransferability(
    @SerializedName("requestDistributorApproval")
    @TokenParametersDocumentTradeability.Companion.ApprovalStatus
    var requestDistributorApproval: String = NO_TYPE,
    @SerializedName("requestDssxApproval")
    @TokenParametersDocumentTradeability.Companion.ApprovalStatus
    var requestDssxApproval: String = NO_TYPE,
    @SerializedName("requestIssuerApproval")
    @TokenParametersDocumentTradeability.Companion.ApprovalStatus
    var requestIssuerApproval: String = NO_TYPE,
    @SerializedName("tokenTransferableFromDate")
    var tokenTransferableFromDate: String = "",
    @SerializedName("tokenTransferableToDate")
    var tokenTransferableToDate: String = ""
)