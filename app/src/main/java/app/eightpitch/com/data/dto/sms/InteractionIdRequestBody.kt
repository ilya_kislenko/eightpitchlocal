package app.eightpitch.com.data.dto.sms

data class InteractionIdRequestBody(
    val interactionId: String
)