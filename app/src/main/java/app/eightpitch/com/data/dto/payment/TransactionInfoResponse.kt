package app.eightpitch.com.data.dto.payment

import androidx.annotation.StringDef
import app.eightpitch.com.extensions.NO_TYPE
import app.eightpitch.com.extensions.orDefaultValue
import com.google.gson.annotations.SerializedName

data class TransactionInfoResponse(
    @SerializedName("paymentUrl")
    private val _paymentUrl: String?,
    @SerializedName("responseStatus")
    @TransactionStatus private val _responseStatus: String?
) {
    val paymentUrl: String
        get() = _paymentUrl.orDefaultValue("")
    @TransactionStatus val responseStatus: String
        get() = _responseStatus.orDefaultValue(NO_TYPE)


    companion object {

        const val OK = "OK"
        const val PAYMENT_GATEWAY_ERROR = "PAYMENT_GATEWAY_ERROR"

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(NO_TYPE, OK, PAYMENT_GATEWAY_ERROR)
        annotation class TransactionStatus
    }
}