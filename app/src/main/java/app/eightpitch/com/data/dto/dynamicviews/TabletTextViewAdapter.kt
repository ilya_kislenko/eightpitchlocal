package app.eightpitch.com.data.dto.dynamicviews

import android.view.*
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.R
import app.eightpitch.com.extensions.inflateView

class TabletTextViewAdapter(private val descriptions: List<Pair<String, String>>) :
    RecyclerView.Adapter<TabletTextViewAdapter.TextHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TextHolder =
        TextHolder(inflateView(parent, R.layout.item_tablet_text_view))

    override fun getItemCount(): Int = descriptions.size

    override fun onBindViewHolder(holder: TextHolder, position: Int) {
        val description = descriptions[position]
        holder.apply {
            val first = description.first
            firstTextView.text = first

            val second = description.second
            secondTextView.text = second
        }
    }

    class TextHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val firstTextView: AppCompatTextView = itemView.findViewById(R.id.firstTextView)
        val secondTextView: AppCompatTextView = itemView.findViewById(R.id.secondTextView)
    }
}