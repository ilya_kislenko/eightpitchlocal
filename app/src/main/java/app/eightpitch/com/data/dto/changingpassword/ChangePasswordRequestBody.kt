package app.eightpitch.com.data.dto.changingpassword

data class ChangePasswordRequestBody(
    val confirmPassword: String = "",
    val interactionId: String = "",
    val password: String = "",
    val userExternalId: String = ""
)