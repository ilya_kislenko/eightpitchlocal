package app.eightpitch.com.data.dto.investment

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

/**
 * The class contains a list of brief information about where the investor has invested
 * and the total amount of investment.
 */
class InvestorGraph(
    @SerializedName("investments")
    var investments: List<GraphsInvestment> = arrayListOf(),
    @SerializedName("totalMonetaryAmount")
    var totalMonetaryAmount: BigDecimal = BigDecimal.ZERO
)