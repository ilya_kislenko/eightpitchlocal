package app.eightpitch.com.data.dto.auth

import app.eightpitch.com.extensions.orDefaultValue
import com.google.gson.annotations.SerializedName

data class TokenResponse(
    @SerializedName("accessToken")
    private val _accessToken: String? = "",
    @SerializedName("refreshToken")
    private val _refreshToken: String? = ""
) {
    val accessToken: String
        get() = _accessToken.orDefaultValue("")
    val refreshToken: String
        get() = _refreshToken.orDefaultValue("")
}