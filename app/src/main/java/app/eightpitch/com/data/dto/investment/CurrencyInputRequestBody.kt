package app.eightpitch.com.data.dto.investment

import java.math.BigDecimal

data class CurrencyInputRequestBody(
    val amount: BigDecimal = BigDecimal.ZERO,
    val riskAwareness: Boolean = true
)