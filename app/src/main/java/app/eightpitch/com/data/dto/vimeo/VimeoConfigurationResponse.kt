package app.eightpitch.com.data.dto.vimeo

import com.google.gson.annotations.SerializedName

/**
 * The class describes the response of the vimeo server when requesting video configurations
 */
data class VimeoConfigurationResponse(
    @SerializedName("request")
    val request: VimeoRequest = VimeoRequest(VimeoFiles(arrayListOf()))
)