package app.eightpitch.com.data.dto.dynamicviews.mainfacts

import android.os.*
import androidx.annotation.*
import app.eightpitch.com.R
import app.eightpitch.com.extensions.readStringSafe
import app.eightpitch.com.utils.Constants.DEFAULT_DATA_PATTERN
import app.eightpitch.com.utils.DateUtils.formatByPattern
import app.eightpitch.com.utils.DateUtils.parseByFormat

class FinancialInformationItem(
    var country: String = "",
    var financingPurpose: String = "",
    var goal: String = "",
    var guarantee: String = "",
    var investmentSeries: String = "",
    var isin: String = "",
    var nominalValue: String = "",
    var typeOfSecurity: String = "",
    var dsoProjectFinishDate: String = "",
    var dsoProjectStartDate: String = "",
    var softCap: String = "",
    var minimumInvestmentAmount: String = ""
) : Parcelable {

    private val projectFinishDate
        get() = dsoProjectFinishDate.parseByFormat(pattern = DEFAULT_DATA_PATTERN).formatByPattern()
    private val projectStartDate
        get() = dsoProjectStartDate.parseByFormat(pattern = DEFAULT_DATA_PATTERN).formatByPattern()

    val descriptionToLabel: List<MainFact>
        get() = createFacts()

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(country)
        parcel.writeString(financingPurpose)
        parcel.writeString(goal)
        parcel.writeString(guarantee)
        parcel.writeString(investmentSeries)
        parcel.writeString(isin)
        parcel.writeString(nominalValue)
        parcel.writeString(typeOfSecurity)
        parcel.writeString(dsoProjectFinishDate)
        parcel.writeString(dsoProjectStartDate)
        parcel.writeString(softCap)
        parcel.writeString(minimumInvestmentAmount)
        parcel.writeString(nominalValue)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<FinancialInformationItem> {
        override fun createFromParcel(parcel: Parcel): FinancialInformationItem {
            return FinancialInformationItem(parcel)
        }

        override fun newArray(size: Int): Array<FinancialInformationItem?> {
            return arrayOfNulls(size)
        }

        const val FINANCIAL_INSTRUMENT = R.string.financial_instrument_text
        const val MINIMUM_INVESTMENT_AMOUNT = R.string.minimum_investment_amount_text
        const val SALES_LAUNCH = R.string.sales_launch_text
        const val END_OF_TERM = R.string.end_of_term_text
        const val PURPOSE_OF_FINANCING = R.string.purpose_of_financing_text
        const val FINANCING_ROUND = R.string.financing_round_text
        const val COUNTRY = R.string.country_text
        const val GUARANTEE = R.string.guarantee_text
        const val ISSUING_VOLUME = R.string.issuing_volume_text
        const val SOFT_CAP = R.string.soft_cap_text
        const val ISIN = R.string.isin_text
        const val NOMINAL_VALUE = R.string.nominal_value_of_the_token_text
    }

    private fun createFacts(): List<MainFact> {
        return arrayListOf<MainFact>().apply {
            addIfNotEmpty(isin, ISIN, R.drawable.icon_isin_field)
            addIfNotEmpty(country, COUNTRY, R.drawable.icon_country_field)
            addIfNotEmpty(financingPurpose, PURPOSE_OF_FINANCING, R.drawable.icon_financing_purpose_field)
            addIfNotEmpty(goal, ISSUING_VOLUME, R.drawable.icon_financing_purpose_field)
            addIfNotEmpty(guarantee, GUARANTEE, R.drawable.icon_goal_field)
            addIfNotEmpty(investmentSeries, FINANCING_ROUND, R.drawable.icon_investment_series)
            addIfNotEmpty(typeOfSecurity, FINANCIAL_INSTRUMENT, R.drawable.icon_type_security)
            addIfNotEmpty(minimumInvestmentAmount, MINIMUM_INVESTMENT_AMOUNT, R.drawable.icon_bitcoin_in_circle)
            addIfNotEmpty(projectStartDate, SALES_LAUNCH, R.drawable.icon_issue_surcharge_field)
            addIfNotEmpty(projectFinishDate, END_OF_TERM, R.drawable.icon_conversation_right)
            addIfNotEmpty(softCap, SOFT_CAP, R.drawable.icon_type_of_repayment_field)
            addIfNotEmpty(nominalValue, NOMINAL_VALUE, R.drawable.icon_bitcoin_in_circle)
        }.toList()
    }

    private fun ArrayList<MainFact>.addIfNotEmpty(
        text: String,
        @StringRes label: Int,
        @DrawableRes icon: Int
    ) {
        if (text.isNotEmpty())
            add(MainFact(
                label, text, icon
            ))
    }
}