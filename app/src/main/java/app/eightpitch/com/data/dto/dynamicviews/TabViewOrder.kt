package app.eightpitch.com.data.dto.dynamicviews

import android.os.*
import app.eightpitch.com.extensions.*

/**
 * Views order for every project dynamic constructor tab
 */
data class TabViewOrder(
    val tabName: String = "",
    val _orderId: RootViewsContainer? = RootViewsContainer()
) : Parcelable {

    val orderId
        get() = _orderId.orDefaultValue(RootViewsContainer())

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readParcelable(RootViewsContainer::class.java.classLoader) ?: RootViewsContainer())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(tabName)
        parcel.writeParcelable(orderId, flags)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<TabViewOrder> {
        override fun createFromParcel(parcel: Parcel): TabViewOrder {
            return TabViewOrder(parcel)
        }

        override fun newArray(size: Int): Array<TabViewOrder?> {
            return arrayOfNulls(size)
        }
    }
}