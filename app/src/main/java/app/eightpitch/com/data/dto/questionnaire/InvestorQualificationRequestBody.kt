package app.eightpitch.com.data.dto.questionnaire

import android.content.Context
import android.os.*
import androidx.annotation.StringDef
import app.eightpitch.com.R
import app.eightpitch.com.extensions.*

class InvestorQualificationRequestBody(
    val description: String? = null,
    val fileIds: List<String>? = null,
    @InvestorClass val investorClass: String = NO_TYPE,
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readListSafe(listOf(), InvestorQualificationRequestBody::class.java.classLoader!!),
        parcel.readStringSafe())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(description)
        parcel.writeStringList(fileIds)
        parcel.writeString(investorClass)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<InvestorQualificationRequestBody> {
        override fun createFromParcel(parcel: Parcel): InvestorQualificationRequestBody {
            return InvestorQualificationRequestBody(parcel)
        }

        override fun newArray(size: Int): Array<InvestorQualificationRequestBody?> {
            return arrayOfNulls(size)
        }

        const val UNQUALIFIED = "UNQUALIFIED"
        const val CLASS_1 = "CLASS_1"
        const val CLASS_2 = "CLASS_2"
        const val CLASS_3 = "CLASS_3"
        const val CLASS_4 = "CLASS_4"
        const val CLASS_5 = "CLASS_5"

        @Retention(AnnotationRetention.RUNTIME)
        @StringDef(NO_TYPE, UNQUALIFIED, CLASS_1, CLASS_2, CLASS_3, CLASS_4, CLASS_5)
        annotation class InvestorClass
    }

    enum class InvestorQualification(val displayValue: Int) {
        UNQUALIFIED(R.string.unqualified_display_value),
        CLASS_1(R.string.class_one_display_value),
        CLASS_2(R.string.class_two_display_value),
        CLASS_3(R.string.class_three_display_value),
        CLASS_4(R.string.class_four_display_value),
        CLASS_5(R.string.class_five_display_value);

        override fun toString(): String {
            return this.name
        }

        companion object {

            fun isUnQualified(context: Context, qualification: String) = fromDisplayValue(context, qualification) == UNQUALIFIED

            fun toDisplayValue(context: Context, value: String): String {
                return values().find { it.name == value }?.displayValue?.let { context.getString(it) }
                       ?: context.getString(R.string.mr_prefix_display_value)
            }

            fun fromDisplayValue(context: Context?, displayValue: String): InvestorQualification {
                return values().find { context?.getString(it.displayValue) == displayValue } ?: UNQUALIFIED
            }
        }
    }
}