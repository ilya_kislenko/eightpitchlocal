package app.eightpitch.com.data.dto.dynamicviews

import android.os.*
import android.view.Gravity
import androidx.annotation.StringDef
import app.eightpitch.com.extensions.*
import java.math.BigDecimal
import java.math.BigDecimal.ZERO
import app.eightpitch.com.data.dto.dynamicviews.ImageAndTextUIElement.CREATOR.BOTTOM
import app.eightpitch.com.data.dto.dynamicviews.ImageAndTextUIElement.CREATOR.CENTER
import app.eightpitch.com.data.dto.dynamicviews.ImageAndTextUIElement.CREATOR.LEFT
import app.eightpitch.com.data.dto.dynamicviews.ImageAndTextUIElement.CREATOR.RIGHT
import app.eightpitch.com.data.dto.dynamicviews.TextUIElement.CREATOR.SIZE_H1
import app.eightpitch.com.data.dto.dynamicviews.TextUIElement.CREATOR.SIZE_H2
import app.eightpitch.com.data.dto.dynamicviews.TextUIElement.CREATOR.SIZE_H3
import app.eightpitch.com.data.dto.user.LanguageRequestBody.Companion.DE
import app.eightpitch.com.data.dto.user.LanguageRequestBody.Companion.UserLanguage
import com.google.gson.annotations.SerializedName

/**
 * Represents a cells for a particular table row
 */
typealias RowItem = List<CellItem>

/**
 * Type that represents a backend data of a UI element
 */
sealed class UIElement(
    @SerializedName("id")
    var _id: String = ""
) : Parcelable {

    val id
        get() = _id.orDefaultValue("")
}

open class TextUIElement(
    @SerializedName("text")
    private val _text: String? = "",
    @SerializedName("color")
    private val _color: String? = "black",
    @SerializedName("fontSize")
    private val _fontSize: BigDecimal? = ZERO, // in px
    @SerializedName("justify")
    @ImageAndTextUIElement.CREATOR.Justify private val _justify: String? = NO_TYPE,
    @SerializedName("headingSize")
    @HeadingSize private val _headingSize: String? = SIZE_H1,
    @SerializedName("bold")
    private val _bold: Boolean? = false,
    @SerializedName("italic")
    private val _italic: Boolean? = false,
    @SerializedName("underline")
    private val _underline: Boolean? = false,
    @SerializedName("header")
    private val _header: String? = "",
    @SerializedName("preHeader")
    private val _preHeader: String? = ""
) : UIElement() {


    val text: String
        get() = _text.orDefaultValue("")
    val color: String
        get() = _color.orDefaultValue("")
    val fontSize: BigDecimal
        get() = _fontSize.orDefaultValue(ZERO)
    val gravity: Int
        get() = justifyToGravity(_justify.orDefaultValue(""))
    val headingSize: Float
        get() = headingSizeToFloat(_headingSize.orDefaultValue(SIZE_H1))
    val headingSizeString: String
        get() = _headingSize.orDefaultValue(SIZE_H1)
    val bold: Boolean
        get() = _bold.orDefaultValue(false)
    val italic: Boolean
        get() = _italic.orDefaultValue(false)
    val underline: Boolean
        get() = _underline.orDefaultValue(false)
    val header: String
        get() = _header.orDefaultValue("")
    val preHeader: String
        get() = _preHeader.orDefaultValue("")

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        BigDecimal(parcel.readDouble()),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readStringSafe(),
        parcel.readStringSafe()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(text)
        parcel.writeString(color)
        parcel.writeSerializable(fontSize)
        parcel.writeString(_justify)
        parcel.writeString(_headingSize)
        parcel.writeByte(if (bold) 1 else 0)
        parcel.writeByte(if (italic) 1 else 0)
        parcel.writeByte(if (underline) 1 else 0)
        parcel.writeString(header)
        parcel.writeString(preHeader)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<TextUIElement> {
        override fun createFromParcel(parcel: Parcel): TextUIElement {
            return TextUIElement(parcel)
        }

        override fun newArray(size: Int): Array<TextUIElement?> {
            return arrayOfNulls(size)
        }

        const val SIZE_H1 = "h1"
        const val SIZE_H2 = "h2"
        const val SIZE_H3 = "h3"

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(SIZE_H1, SIZE_H2, SIZE_H3, NO_TYPE)
        annotation class HeadingSize
    }
}

data class DividerUIElement(
    @SerializedName("height")
    private val _height: BigDecimal? = ZERO
) : UIElement() {

    val height: BigDecimal
        get() = _height.orDefaultValue(ZERO)

    constructor(parcel: Parcel) : this(
        parcel.readBigDecimal()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(height.toDouble())
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<DividerUIElement> {
        override fun createFromParcel(parcel: Parcel): DividerUIElement {
            return DividerUIElement(parcel)
        }

        override fun newArray(size: Int): Array<DividerUIElement?> {
            return arrayOfNulls(size)
        }
    }
}

/**
 * Not translated
 * Show element if matches userLanguage this
 * @param _language
 */
data class FAQUIElement(
    @SerializedName("header")
    private val _header: String? = null,
    @SerializedName("items")
    private val _items: List<FaqItem>? = null,
    @SerializedName("language")
    @UserLanguage private val _language: String? = null,
) : UIElement() {

    val header: String
        get() = _header.orDefaultValue("")
    val items: List<FaqItem>
        get() = _items.orDefaultValue(listOf())
    val language: String
        get() = _language.orDefaultValue(DE)

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readListSafe(mutableListOf<FaqItem>(), FaqItem::class.java.classLoader!!)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(header)
        parcel.writeTypedList(items)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<FAQUIElement> {
        override fun createFromParcel(parcel: Parcel): FAQUIElement {
            return FAQUIElement(parcel)
        }

        override fun newArray(size: Int): Array<FAQUIElement?> {
            return arrayOfNulls(size)
        }
    }
}

data class IconsUIElement(
    @SerializedName("perLine")
    private val _perLine: BigDecimal? = ZERO,
    @SerializedName("items")
    private val _items: List<IconItem>? = listOf()
) : UIElement() {

    val perLine: BigDecimal
        get() = _perLine.orDefaultValue(ZERO)
    val items: List<IconItem>
        get() = _items.orDefaultValue(listOf())

    constructor(parcel: Parcel) : this(
        BigDecimal(parcel.readDouble()),
        parcel.readListSafe(mutableListOf<IconItem>(), IconItem::class.java.classLoader!!)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(perLine.toDouble())
        parcel.writeTypedList(items)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<IconsUIElement> {
        override fun createFromParcel(parcel: Parcel): IconsUIElement {
            return IconsUIElement(parcel)
        }

        override fun newArray(size: Int): Array<IconsUIElement?> {
            return arrayOfNulls(size)
        }
    }
}

data class ImageAndTextUIElement(
    @SerializedName("imageId")
    private val _imageId: String? = "",
    @SerializedName("imageFileName")
    private val _imageFileName: String? = "",
    @SerializedName("imageDescription")
    private val _imageDescription: String? = "",
    @SerializedName("textPosition")
    @Justify val _textPosition: String? = NO_TYPE,
    @SerializedName("header")
    private val _header: String? = "",
    @SerializedName("text")
    private val _text: String? = ""
) : UIElement() {

    val imageId
        get() = _imageId.orDefaultValue("")
    val imageFileName
        get() = _imageFileName.orDefaultValue("")
    val imageDescription
        get() = _imageDescription.orDefaultValue("")
    val header
        get() = _header.orDefaultValue("")
    val text
        get() = _text.orDefaultValue("")

    internal val gravity
        get() = justifyToGravity(_textPosition.orDefaultValue(""))

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(imageId)
        parcel.writeString(imageFileName)
        parcel.writeString(imageDescription)
        parcel.writeString(_textPosition)
        parcel.writeString(header)
        parcel.writeString(text)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<ImageAndTextUIElement> {

        const val LEFT = "left"
        const val BOTTOM = "bottom"
        const val RIGHT = "right"
        const val CENTER = "center"

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(NO_TYPE, LEFT, BOTTOM, RIGHT, CENTER)
        annotation class Justify

        override fun createFromParcel(parcel: Parcel): ImageAndTextUIElement {
            return ImageAndTextUIElement(parcel)
        }

        override fun newArray(size: Int): Array<ImageAndTextUIElement?> {
            return arrayOfNulls(size)
        }
    }
}

data class ListUIElement(
    val header: String = "",
    val items: List<ListItem> = listOf()
) : UIElement() {

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readListSafe(mutableListOf<ListItem>(), ListItem::class.java.classLoader!!)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(header)
        parcel.writeTypedList(items)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<ListUIElement> {
        override fun createFromParcel(parcel: Parcel): ListUIElement {
            return ListUIElement(parcel)
        }

        override fun newArray(size: Int): Array<ListUIElement?> {
            return arrayOfNulls(size)
        }
    }
}

data class TableUIElement(
    @SerializedName("headerColor")
    private val _headerColor: String? = "red",
    @SerializedName("rowsNumber")
    private val _rowsNumber: BigDecimal? = ZERO,
    @SerializedName("colsNumber")
    private val _colsNumber: BigDecimal? = ZERO,
    @SerializedName("rows")
    private val _rows: List<RowItem>? = listOf()
) : UIElement() {

    val headerColor
        get() = _headerColor.orDefaultValue("red")
    val rowsNumber
        get() = _rowsNumber.orDefaultValue(ZERO)
    val colsNumber
        get() = _colsNumber.orDefaultValue(ZERO)
    val rows
        get() = _rows.orDefaultValue(listOf())

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        BigDecimal(parcel.readDouble()),
        BigDecimal(parcel.readDouble()),
        readTwoDimensionalCells(parcel)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(headerColor)
        parcel.writeDouble(rowsNumber.toDouble())
        parcel.writeDouble(colsNumber.toDouble())
        parcel.writeInt(rows.size)
        for (element in rows) {
            parcel.writeTypedList(element)
        }
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<TableUIElement> {

        private fun readTwoDimensionalCells(parcel: Parcel): List<RowItem> {
            val rowsSize = parcel.readInt()
            val result: MutableList<RowItem> = mutableListOf()
            repeat(rowsSize) {
                result.add(
                    parcel.readListSafe(
                        mutableListOf(),
                        CellItem::class.java.classLoader!!
                    )
                )
            }
            return result
        }

        override fun createFromParcel(parcel: Parcel): TableUIElement {
            return TableUIElement(parcel)
        }

        override fun newArray(size: Int): Array<TableUIElement?> {
            return arrayOfNulls(size)
        }
    }
}

data class ImageUIElement(
    val fullWidth: Boolean = false,
    val imageId: String = "",
    val imageFileName: String = "",
    val useText: Boolean = false
) : TextUIElement(), Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readByte() != 0.toByte(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readByte() != 0.toByte()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        super.writeToParcel(parcel, flags)
        parcel.writeByte(if (fullWidth) 1 else 0)
        parcel.writeString(imageId)
        parcel.writeString(imageFileName)
        parcel.writeByte(if (useText) 1 else 0)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<ImageUIElement> {
        override fun createFromParcel(parcel: Parcel): ImageUIElement {
            return ImageUIElement(parcel)
        }

        override fun newArray(size: Int): Array<ImageUIElement?> {
            return arrayOfNulls(size)
        }
    }
}

data class VimeoUIElement(
    val vimeoId: String = "",
    @ImageAndTextUIElement.CREATOR.Justify val justify: String = NO_TYPE,
    val useText: Boolean = false,
    val header: String = "",
    val text: String = ""
) : UIElement() {

    internal val gravity
        get() = justifyToGravity(justify)

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readByte() != 0.toByte(),
        parcel.readStringSafe(),
        parcel.readStringSafe()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(vimeoId)
        parcel.writeString(justify)
        parcel.writeByte(if (useText) 1 else 0)
        parcel.writeString(header)
        parcel.writeString(text)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<VimeoUIElement> {
        override fun createFromParcel(parcel: Parcel): VimeoUIElement {
            return VimeoUIElement(parcel)
        }

        override fun newArray(size: Int): Array<VimeoUIElement?> {
            return arrayOfNulls(size)
        }
    }
}

data class DualColumnImagesUIElement(
    val leftImageId: String = "",
    val leftImageFileName: String = "",
    val leftText: String = "",
    val rightImageId: String = "",
    val rightImageFileName: String = "",
    val rightText: String = ""
) : UIElement() {

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(leftImageId)
        parcel.writeString(leftImageFileName)
        parcel.writeString(leftText)
        parcel.writeString(rightImageId)
        parcel.writeString(rightImageFileName)
        parcel.writeString(rightText)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<DualColumnImagesUIElement> {
        override fun createFromParcel(parcel: Parcel): DualColumnImagesUIElement {
            return DualColumnImagesUIElement(parcel)
        }

        override fun newArray(size: Int): Array<DualColumnImagesUIElement?> {
            return arrayOfNulls(size)
        }
    }
}

data class FactsUIElement(
    val perLine: Int = 0,
    val items: List<FactItem> = listOf()
) : UIElement() {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readListSafe(mutableListOf<FactItem>(), FactsUIElement::class.java.classLoader!!))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(perLine)
        parcel.writeTypedList(items)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<FactsUIElement> {
        override fun createFromParcel(parcel: Parcel): FactsUIElement {
            return FactsUIElement(parcel)
        }

        override fun newArray(size: Int): Array<FactsUIElement?> {
            return arrayOfNulls(size)
        }
    }
}

data class TiledImagesUIElement(
    val perLine: Int = 0,
    val bold: Boolean = false,
    val italic: Boolean = false,
    val underline: Boolean = false,
    val items: List<TiledImageItem> = listOf()
) : UIElement() {

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readListSafe(mutableListOf<TiledImageItem>(), TiledImagesUIElement::class.java.classLoader!!))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(perLine)
        parcel.writeByte(if (bold) 1 else 0)
        parcel.writeByte(if (italic) 1 else 0)
        parcel.writeByte(if (underline) 1 else 0)
        parcel.writeTypedList(items)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<TiledImagesUIElement> {
        override fun createFromParcel(parcel: Parcel): TiledImagesUIElement {
            return TiledImagesUIElement(parcel)
        }

        override fun newArray(size: Int): Array<TiledImagesUIElement?> {
            return arrayOfNulls(size)
        }
    }

}

data class ButtonUIElement(
    val btnColor: String = "#F54C4C",
    val textColor: String = "",
    val text: String = "",
    val link: String = ""
) : UIElement() {

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(btnColor)
        parcel.writeString(textColor)
        parcel.writeString(text)
        parcel.writeString(link)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<ButtonUIElement> {
        override fun createFromParcel(parcel: Parcel): ButtonUIElement {
            return ButtonUIElement(parcel)
        }

        override fun newArray(size: Int): Array<ButtonUIElement?> {
            return arrayOfNulls(size)
        }
    }
}

data class DualColumnTextUIElement(
    @SerializedName("leftText")
    private val _leftText: String = "",
    @SerializedName("rightText")
    private val _rightText: String = ""
) : UIElement() {

    val leftText: String
        get() = _leftText.orDefaultValue("")
    val rightText: String
        get() = _rightText.orDefaultValue("")

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(leftText)
        parcel.writeString(rightText)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<DualColumnTextUIElement> {
        override fun createFromParcel(parcel: Parcel): DualColumnTextUIElement {
            return DualColumnTextUIElement(parcel)
        }

        override fun newArray(size: Int): Array<DualColumnTextUIElement?> {
            return arrayOfNulls(size)
        }
    }
}

data class DisclaimerUiElement(
    @SerializedName("iconUrl")
    private val _iconUrl: String = "",
    @SerializedName("iconFileName")
    private val _iconFileName: String = "",
    @SerializedName("text")
    private val _text: String = "",
    @SerializedName("background")
    private val _background: String = "#FF2E3844"
) : UIElement() {

    val iconUrl: String
        get() = _iconUrl.orDefaultValue("")
    val iconFileName: String
        get() = _iconFileName.orDefaultValue("")
    val text: String
        get() = _text.orDefaultValue("")
    val background: String
        get() = _background.orDefaultValue("")

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(iconUrl)
        parcel.writeString(iconFileName)
        parcel.writeString(text)
        parcel.writeString(background)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<DisclaimerUiElement> {
        override fun createFromParcel(parcel: Parcel): DisclaimerUiElement {
            return DisclaimerUiElement(parcel)
        }

        override fun newArray(size: Int): Array<DisclaimerUiElement?> {
            return arrayOfNulls(size)
        }
    }
}

data class HeaderUIElement(
    @SerializedName("text")
    private val _header: String? = "",
    @SerializedName("headingSize")
    @TextUIElement.CREATOR.HeadingSize private val _headingSize: String? = SIZE_H2,
    @SerializedName("preHeader")
    private val _preHeader: String? = ""
) : UIElement() {

    val header: String
        get() = _header.orDefaultValue("")
    val headingSize: Float
        get() = headingSizeToFloat(_headingSize.orDefaultValue(""))
    val preHeader: String
        get() = _preHeader.orDefaultValue("")

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(header)
        parcel.writeString(_headingSize)
        parcel.writeString(preHeader)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<HeaderUIElement> {
        override fun createFromParcel(parcel: Parcel): HeaderUIElement {
            return HeaderUIElement(parcel)
        }

        override fun newArray(size: Int): Array<HeaderUIElement?> {
            return arrayOfNulls(size)
        }
    }
}

data class LinkedTextUIElement(
    @SerializedName("text")
    private val _text: String = "",
    @SerializedName("color")
    private val _color: String = "",
    @SerializedName("fontSize")
    private val _fontSize: BigDecimal = ZERO,
    @SerializedName("justify")
    private val _justify: String = "",
    @SerializedName("bold")
    private val _bold: Boolean = false,
    @SerializedName("italic")
    private val _italic: Boolean = false,
    @SerializedName("underline")
    private val _underline: Boolean = false,
    @SerializedName("fontFamily")
    private val _fontFamily: String = "",
    @SerializedName("language")
    @UserLanguage val _language: String = NO_TYPE,
    @SerializedName("wordsLink")
    private val _wordsLink: List<TextLinkItem> = listOf()
) : UIElement() {

    val text: String
        get() = _text.orDefaultValue("")
    val color: String
        get() = _color.orDefaultValue("")
    val fontSize: BigDecimal
        get() = _fontSize.orDefaultValue(ZERO)
    val justify: String
        get() = _justify.orDefaultValue("")
    val bold: Boolean
        get() = _bold.orDefaultValue(false)
    val italic: Boolean
        get() = _italic.orDefaultValue(false)
    val underline: Boolean
        get() = _underline.orDefaultValue(false)
    val fontFamily: String
        get() = _fontFamily.orDefaultValue("")
    val language: String
        get() = _language.orDefaultValue("")
    val wordsLink: List<TextLinkItem>
        get() = _wordsLink.orDefaultValue(listOf())

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readBigDecimal(),
        parcel.readStringSafe(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readListSafe(mutableListOf<TextLinkItem>(), LinkedTextUIElement::class.java.classLoader!!))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(text)
        parcel.writeString(color)
        parcel.writeSerializable(fontSize)
        parcel.writeString(justify)
        parcel.writeByte(if (bold) 1 else 0)
        parcel.writeByte(if (italic) 1 else 0)
        parcel.writeByte(if (underline) 1 else 0)
        parcel.writeString(fontFamily)
        parcel.writeString(language)
        parcel.writeList(wordsLink)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<LinkedTextUIElement> {
        override fun createFromParcel(parcel: Parcel): LinkedTextUIElement {
            return LinkedTextUIElement(parcel)
        }

        override fun newArray(size: Int): Array<LinkedTextUIElement?> {
            return arrayOfNulls(size)
        }
    }
}

data class DepartmentUIElement(
    @SerializedName("header")
    private val _header: String = "",
    @SerializedName("items")
    private val _items: List<DepartmentItem> = listOf()
) : UIElement() {

    val header: String
        get() = _header.orDefaultValue("")
    val items: List<DepartmentItem>
        get() = _items.orDefaultValue(listOf())

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readListSafe(mutableListOf<DepartmentItem>(), DepartmentUIElement::class.java.classLoader!!))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(header)
        parcel.writeTypedList(items)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<DepartmentUIElement> {
        override fun createFromParcel(parcel: Parcel): DepartmentUIElement {
            return DepartmentUIElement(parcel)
        }

        override fun newArray(size: Int): Array<DepartmentUIElement?> {
            return arrayOfNulls(size)
        }
    }
}

data class ImageLinkUIElement(
    @SerializedName("perLine")
    private val _perLine: Int? = 0,
    @SerializedName("items")
    private val _items: List<ImageLink>? = listOf(),
    @SerializedName("isLinkAdded")
    private val _isLinkAdded: Boolean? = false,
    @SerializedName("isDescriptionAdded")
    private val _isDescriptionAdded: Boolean? = false
) : UIElement() {

    val perLine: Int
        get() = _perLine.orDefaultValue(0)
    val items: List<ImageLink>
        get() = _items.orDefaultValue(listOf())
    val isLinkAdded: Boolean
        get() = _isLinkAdded.orDefaultValue(false)
    val isDescriptionAdded: Boolean
        get() = _isDescriptionAdded.orDefaultValue(false)

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readListSafe(mutableListOf<ImageLink>(), ImageLinkUIElement::class.java.classLoader!!),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(perLine)
        parcel.writeTypedList(items)
        parcel.writeByte(if (isLinkAdded) 1 else 0)
        parcel.writeByte(if (isDescriptionAdded) 1 else 0)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<ImageLinkUIElement> {
        override fun createFromParcel(parcel: Parcel): ImageLinkUIElement {
            return ImageLinkUIElement(parcel)
        }

        override fun newArray(size: Int): Array<ImageLinkUIElement?> {
            return arrayOfNulls(size)
        }
    }
}

data class Group(
    @SerializedName("idList")
    private val _idList: List<String>? = listOf()
) : UIElement() {

    val idList: List<String>
        get() = _idList.orDefaultValue(listOf())

    constructor(parcel: Parcel) : this(parcel.readListSafe(listOf<String>(), Group::class.java.classLoader!!))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeStringList(idList)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<Group> {
        override fun createFromParcel(parcel: Parcel): Group {
            return Group(parcel)
        }

        override fun newArray(size: Int): Array<Group?> {
            return arrayOfNulls(size)
        }
    }
}

data class ChartItems(
    @SerializedName("perLine")
    private val _perLine: Int? = 0,
    @SerializedName("items")
    private val _items: List<ChartUIElement>? = listOf()
) : UIElement() {

    val perLine: Int
        get() = _perLine.orDefaultValue(0)

    val items: List<ChartUIElement>
        get() = _items.orDefaultValue(listOf())

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readListSafe(listOf<ChartUIElement>(), ChartUIElement::class.java.classLoader!!))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(perLine)
        parcel.writeList(items)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<ChartItems> {
        override fun createFromParcel(parcel: Parcel): ChartItems {
            return ChartItems(parcel)
        }

        override fun newArray(size: Int): Array<ChartItems?> {
            return arrayOfNulls(size)
        }
    }
}

data class ChartUIElement(
    @SerializedName("title")
    private val _title: String? = "",
    @SerializedName("text")
    private val _text: String? = "",
    @SerializedName("chartType")
    @ChartType val _chartType: String? = NO_TYPE,
    @SerializedName("data")
    private val _data: List<List<String>> = listOf()
) : Parcelable {

    val title: String
        get() = _title.orDefaultValue("")
    val text: String
        get() = _text.orDefaultValue("")
    @ChartType val chartType: String
        get() = _chartType.orDefaultValue("")
    val data: List<List<String>>
        get() = _data.orDefaultValue(listOf())
//            .run {
//            if (this.isNullOrEmpty())
//                return listOf()
//            val arrayList = ArrayList<List<String>>()
//            for (i in 1 until this.size) {
//                val stringArray = arrayListOf<String>()
//                val chartData = this[i]
//                if (chartData.isNullOrEmpty()) {
//                    arrayList.add(stringArray)
//                    continue
//                }
//                for (value in chartData) {
//                    stringArray.add(value.orDefaultValue(""))
//                }
//                arrayList.add(stringArray)
//            }
//            arrayList
//        }

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readListSafe(listOf<List<String>>(), ChartUIElement::class.java.classLoader!!))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeString(text)
        parcel.writeString(chartType)
        parcel.writeList(data)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<ChartUIElement> {
        override fun createFromParcel(parcel: Parcel): ChartUIElement {
            return ChartUIElement(parcel)
        }

        override fun newArray(size: Int): Array<ChartUIElement?> {
            return arrayOfNulls(size)
        }

        const val BAR = "bar"
        const val LINE = "line"
        const val DOUGHNUT = "doughnut"
        const val HALF_DOUGHNUT = "half-doughnut"

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(BAR, LINE, DOUGHNUT, HALF_DOUGHNUT, NO_TYPE)
        annotation class ChartType
    }
}

/**
 * Method that transforms a backend representation of a `justify` parameter
 * to android common `gravity`
 */
private fun justifyToGravity(justify: String) = when (justify) {
    RIGHT -> Gravity.END
    LEFT -> Gravity.START
    CENTER -> Gravity.CENTER
    BOTTOM -> Gravity.BOTTOM
    else -> Gravity.CENTER
}

private fun headingSizeToFloat(headingSize: String) = when (headingSize) {
    SIZE_H1 -> 28F
    SIZE_H2 -> 22F
    SIZE_H3 -> 20F
    else -> 22F
}

/**
 * Class that is designed to determine an arguments for an
 * particular [UIElement] and it's supposed to be put into a
 * related view tag during dynamic view creation in [UIElementsBuilder]
 */
sealed class UIElementArguments(open val id: String)

data class DefaultUIElementArguments(override val id: String) : UIElementArguments(id)
data class ImageUIElementArguments(
    override val id: String,
    val imageId: String,
    val withOriginalSize: Boolean = true
) : UIElementArguments(id)
data class VimeoUIElementArguments(override val id: String, val vimeoId: String) : UIElementArguments(id)
data class IconsUIElementArguments(override val id: String, val iconLink: String, val imageId: String) :
    UIElementArguments(id)

data class ClickableUIElementArguments(override val id: String, val url: String) : UIElementArguments(id)
data class SpannableUIElementArguments(override val id: String, val words: List<TextLinkItem>) : UIElementArguments(id)
data class GroupUIElementArguments(override val id: String, val idList: List<String>) : UIElementArguments(id)
