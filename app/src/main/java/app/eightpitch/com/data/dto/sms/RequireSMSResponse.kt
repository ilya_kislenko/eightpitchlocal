package app.eightpitch.com.data.dto.sms

import app.eightpitch.com.extensions.orDefaultValue
import com.google.gson.annotations.SerializedName

data class RequireSMSResponse(
    @SerializedName("interactionId")
    private val _interactionId: String?
) {
    val interactionId: String
        get() = _interactionId.orDefaultValue("")
}