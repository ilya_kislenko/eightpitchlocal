package app.eightpitch.com.data.dto.vimeo

import com.google.gson.annotations.SerializedName

/**
 * The class contains a file with video configurations
 */
class VimeoRequest(
    @SerializedName("files")
    val files: VimeoFiles = VimeoFiles(arrayListOf())
)