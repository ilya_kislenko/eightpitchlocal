package app.eightpitch.com.data.dto.profilepayments

import app.eightpitch.com.utils.Constants.DEFAULT_DATA_PATTERN
import app.eightpitch.com.utils.DateUtils.parseByFormat
import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class ProfilePayment(
    @SerializedName("amount")
    var amount: BigDecimal = BigDecimal(-1),
    @SerializedName("companyAddress")
    var companyAddress: String = "",
    @SerializedName("companyName")
    var companyName: String = "",
    @SerializedName("isin")
    var isin: String = "",
    @SerializedName("monetaryAmount")
    var monetaryAmount: BigDecimal = BigDecimal(-1),
    @SerializedName("paymentSystem")
    var paymentSystem: String = "",
    @SerializedName("shortCut")
    var shortCut: String = "",
    @SerializedName("transactionDate")
    var transactionDate: String = "",
    @SerializedName("transactionId")
    var transactionId: String = ""
) {

    val date
        get() = transactionDate.parseByFormat(pattern = DEFAULT_DATA_PATTERN)
}