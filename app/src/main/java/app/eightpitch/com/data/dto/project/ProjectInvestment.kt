package app.eightpitch.com.data.dto.project

import androidx.annotation.StringDef
import app.eightpitch.com.extensions.NO_TYPE
import app.eightpitch.com.utils.Constants.DEFAULT_DATA_PATTERN
import app.eightpitch.com.utils.DateUtils.parseByFormat
import com.google.gson.annotations.SerializedName

/**
 * The object contains information and status about investment in a project for any project
 */
class ProjectInvestment(
    @SerializedName("amount")
    var amount: Int = 0,
    @SerializedName("createDateTime")
    var createDateTime: String = "",
    @SerializedName("status")
    @InvestmentStatus var status: String = NO_TYPE
) {

    val date
        get() = createDateTime.parseByFormat(pattern = DEFAULT_DATA_PATTERN)

    companion object {
        const val NEW = "NEW"
        const val BOOKED = "BOOKED"
        const val CONFIRMED = "CONFIRMED"
        const val PENDING = "PENDING"
        const val PAID = "PAID"
        const val CANCELED_IN_PROGRESS = "CANCELED_IN_PROGRESS"
        const val CANCELED = "CANCELED"
        const val RECEIVED = "RECEIVED"
        const val REFUNDED_IN_PROGRESS = "REFUNDED_IN_PROGRESS"
        const val REFUNDED = "REFUNDED"
        const val REFUND_FAILED = "REFUND_FAILED"

        @StringDef(NO_TYPE, NEW, BOOKED, CONFIRMED, PENDING, PAID, CANCELED_IN_PROGRESS,
            CANCELED, RECEIVED, REFUNDED_IN_PROGRESS, REFUNDED, REFUND_FAILED)
        annotation class InvestmentStatus
    }

}