package app.eightpitch.com.data.dto.notifications

import android.content.Context
import android.text.Spanned
import app.eightpitch.com.BuildConfig
import app.eightpitch.com.R
import app.eightpitch.com.data.dto.notifications.Notification.Companion.CANCEL_NOT_CONFIRMED_INVESTMENT
import app.eightpitch.com.data.dto.notifications.Notification.Companion.DELETE_USER_REQUEST
import app.eightpitch.com.data.dto.notifications.Notification.Companion.FILLED_FORM_2
import app.eightpitch.com.data.dto.notifications.Notification.Companion.FORM_2_DEADLINE_PASSED
import app.eightpitch.com.data.dto.notifications.Notification.Companion.INVESTMENT_CANCELLED
import app.eightpitch.com.data.dto.notifications.Notification.Companion.INVESTMENT_RECEIPT
import app.eightpitch.com.data.dto.notifications.Notification.Companion.LEVEL_2_KYC_INTERNAL
import app.eightpitch.com.data.dto.notifications.Notification.Companion.LEVEL_2_REJECT
import app.eightpitch.com.data.dto.notifications.Notification.Companion.LEVEL_2_UPGRADE
import app.eightpitch.com.data.dto.notifications.Notification.Companion.NEW_PROJECT_REQUEST
import app.eightpitch.com.data.dto.notifications.Notification.Companion.NotificationType
import app.eightpitch.com.data.dto.notifications.Notification.Companion.PAYMENT_CANCELLED
import app.eightpitch.com.data.dto.notifications.Notification.Companion.PAYMENT_IS_MISSING_10_BUSINESS_DAYS_AFTER_INVESTOR_AGREED_TO_PAY
import app.eightpitch.com.data.dto.notifications.Notification.Companion.PAYMENT_IS_MISSING_AFTER_3_BUSINESS_DAYS
import app.eightpitch.com.data.dto.notifications.Notification.Companion.PAYMENT_IS_MISSING_AFTER_7_BUSINESS_DAYS
import app.eightpitch.com.data.dto.notifications.Notification.Companion.PERSONAL_INFO_CHANGED
import app.eightpitch.com.data.dto.notifications.Notification.Companion.PROJECT_FINISHED_SUCCESSFULLY
import app.eightpitch.com.data.dto.notifications.Notification.Companion.PROJECT_FINISHED_SUCCESSFULLY_ADMIN
import app.eightpitch.com.data.dto.notifications.Notification.Companion.PROJECT_FINISHED_SUCCESSFULLY_MANUAL_RELEASE
import app.eightpitch.com.data.dto.notifications.Notification.Companion.PROJECT_FINISHED_UNSUCCESSFULLY
import app.eightpitch.com.data.dto.notifications.Notification.Companion.PROJECT_FINISHED_UNSUCCESSFULLY_ADMIN
import app.eightpitch.com.data.dto.notifications.Notification.Companion.PROJECT_PAGE_DRAFT_APPROVED
import app.eightpitch.com.data.dto.notifications.Notification.Companion.PROJECT_PAGE_DRAFT_REJECTED
import app.eightpitch.com.data.dto.notifications.Notification.Companion.PROJECT_REQUEST_ACCEPTED
import app.eightpitch.com.data.dto.notifications.Notification.Companion.PROJECT_REQUEST_DECLINED
import app.eightpitch.com.data.dto.notifications.Notification.Companion.PROJECT_REQUEST_IN_PROGRESS
import app.eightpitch.com.data.dto.notifications.Notification.Companion.PROJECT_REQUEST_LEFT_OUT
import app.eightpitch.com.data.dto.notifications.Notification.Companion.PROJECT_REQUEST_REQUIRE_ADDITIONAL_INFORMATION
import app.eightpitch.com.data.dto.notifications.Notification.Companion.PROJECT_STARTED
import app.eightpitch.com.data.dto.notifications.Notification.Companion.PROJECT_STARTED_ADMIN
import app.eightpitch.com.data.dto.notifications.Notification.Companion.REVIEW_PROJECT_PAGE
import app.eightpitch.com.data.dto.notifications.Notification.Companion.REVIEW_TOKEN_PARAMETERS_DOCUMENT
import app.eightpitch.com.data.dto.notifications.Notification.Companion.TOKEN_CREATED
import app.eightpitch.com.data.dto.notifications.Notification.Companion.TOKEN_CREATED_ADMIN
import app.eightpitch.com.data.dto.notifications.Notification.Companion.TOKEN_PARAMETERS_DOCUMENT_APPROVED
import app.eightpitch.com.data.dto.notifications.Notification.Companion.TOKEN_PARAMETERS_DOCUMENT_REJECTED
import app.eightpitch.com.data.dto.notifications.Notification.Companion.TOKEN_TRANSFER
import app.eightpitch.com.data.dto.notifications.Notification.Companion.TRANSFER_RECEIVED
import app.eightpitch.com.data.dto.notifications.Notification.Companion.TRANSFER_SENT
import app.eightpitch.com.data.dto.notifications.Notification.Companion.UPDATE_USER_REQUEST
import app.eightpitch.com.extensions.toHtml

/**
 * Class that supposed to map [Notification.notificationType] with
 * [Notification.message] which is vararg and return a result message
 * for UI layer
 */
class NotificationsMapper(private val appContext: Context) {

    private val PROJECT_ADDITIONAL_INFO_URL =
        "${BuildConfig.WEB_ENDPOINT}dashboard/project-additional-info/"

    /**
     * Method that supposed to map [Notification.notificationType] with
     * [Notification.message] which is vararg and return a result message
     * for UI layer
     */
    fun mapNotificationMessageBy(
        notificationMessage: String,
        @NotificationType notificationType: String
    ): Spanned {
        val varargs = notificationMessage.split(",")
        val message = when (notificationType) {
            LEVEL_2_UPGRADE -> appContext.getString(R.string.investor_account_upgraded_to_level_2)
            LEVEL_2_REJECT -> appContext.getString(R.string.investor_account_upgrade_declined)
            PROJECT_REQUEST_IN_PROGRESS -> appContext.getString(R.string.status_of_project_request_changed)
            PROJECT_REQUEST_ACCEPTED -> appContext.getString(R.string.project_request_accepted)
            PROJECT_REQUEST_DECLINED -> appContext.getString(R.string.project_request_declined)
            PROJECT_REQUEST_REQUIRE_ADDITIONAL_INFORMATION -> appContext.getString(
                R.string.require_additional_info_for_project_request,
                "$PROJECT_ADDITIONAL_INFO_URL${varargs.getOrElse(0) { "" }}"
            )
            REVIEW_TOKEN_PARAMETERS_DOCUMENT -> appContext.getString(R.string.review_token_parameters_document)
            TOKEN_CREATED -> appContext.getString(
                R.string.request_for_token_creation,
                varargs.getOrElse(0) { "" },
                varargs.getOrElse(1) { "" },
                "$PROJECT_ADDITIONAL_INFO_URL${varargs.getOrElse(2) { "" }}"
            )
            TOKEN_CREATED_ADMIN -> appContext.getString(
                R.string.request_for_token_creation_admin,
                varargs.getOrElse(0) { "" },
                varargs.getOrElse(1) { "" },
                "$PROJECT_ADDITIONAL_INFO_URL${varargs.getOrElse(2) { "" }}"
            )
            REVIEW_PROJECT_PAGE -> appContext.getString(
                R.string.review_project_page,
                "$PROJECT_ADDITIONAL_INFO_URL${varargs.getOrElse(0) { "" }}"
            )
            PAYMENT_IS_MISSING_AFTER_3_BUSINESS_DAYS -> appContext.getString(
                R.string.payment_is_missing_after_3_business_days,
                varargs.getOrElse(0) { "" })
            PAYMENT_IS_MISSING_AFTER_7_BUSINESS_DAYS -> appContext.getString(
                R.string.payment_is_missing_after_7_business_days,
                varargs.getOrElse(0) { "" })
            FORM_2_DEADLINE_PASSED -> appContext.getString(R.string.deadline_for_additional_info_passed)
            PROJECT_STARTED -> appContext.getString(
                R.string.project_started,
                varargs.getOrElse(0) { "" },
                "$PROJECT_ADDITIONAL_INFO_URL${varargs.getOrElse(1) { "" }}"
            )
            NEW_PROJECT_REQUEST -> appContext.getString(
                R.string.new_project_request,
                "$PROJECT_ADDITIONAL_INFO_URL${varargs.getOrElse(0) { "" }}"
            )
            FILLED_FORM_2 -> appContext.getString(
                R.string.filled_form_2,
                varargs.getOrElse(0) { "" },
                "$PROJECT_ADDITIONAL_INFO_URL${varargs.getOrElse(1) { "" }}"
            )
            TOKEN_PARAMETERS_DOCUMENT_REJECTED -> appContext.getString(
                R.string.token_parameters_document_rejected,
                varargs.getOrElse(0) { "" },
                "$PROJECT_ADDITIONAL_INFO_URL${varargs.getOrElse(1) { "" }}"
            )
            TOKEN_PARAMETERS_DOCUMENT_APPROVED -> appContext.getString(
                R.string.token_parameters_document_approved,
                varargs.getOrElse(0) { "" },
                "$PROJECT_ADDITIONAL_INFO_URL${varargs.getOrElse(1) { "" }}"
            )
            PROJECT_PAGE_DRAFT_REJECTED -> appContext.getString(
                R.string.project_page_draft_rejected,
                varargs.getOrElse(0) { "" },
                "$PROJECT_ADDITIONAL_INFO_URL${varargs.getOrElse(1) { "" }}"
            )
            PROJECT_PAGE_DRAFT_APPROVED -> appContext.getString(
                R.string.project_page_draft_approved,
                varargs.getOrElse(0) { "" },
                "$PROJECT_ADDITIONAL_INFO_URL${varargs.getOrElse(1) { "" }}"
            )
            PROJECT_STARTED_ADMIN -> appContext.getString(
                R.string.project_started_admin,
                varargs.getOrElse(0) { "" },
                "$PROJECT_ADDITIONAL_INFO_URL${varargs.getOrElse(1) { "" }}"
            )
            PROJECT_FINISHED_SUCCESSFULLY -> appContext.getString(R.string.project_finished_successfully)
            PROJECT_FINISHED_SUCCESSFULLY_ADMIN -> appContext.getString(
                R.string.project_finished_successfully_admin,
                varargs.getOrElse(0) { "" },
                "$PROJECT_ADDITIONAL_INFO_URL${varargs.getOrElse(1) { "" }}"
            )
            PROJECT_FINISHED_SUCCESSFULLY_MANUAL_RELEASE -> appContext.getString(
                R.string.project_finished_successfully_admin_manual_release,
                "$PROJECT_ADDITIONAL_INFO_URL${varargs.getOrElse(0) { "" }}"
            )
            PROJECT_FINISHED_UNSUCCESSFULLY -> appContext.getString(R.string.project_finished_unsuccessfully)
            PROJECT_FINISHED_UNSUCCESSFULLY_ADMIN -> appContext.getString(
                R.string.project_finished_unsuccessfully_admin,
                varargs.getOrElse(0) { "" },
                "$PROJECT_ADDITIONAL_INFO_URL${varargs.getOrElse(1) { "" }}"
            )
            PAYMENT_IS_MISSING_10_BUSINESS_DAYS_AFTER_INVESTOR_AGREED_TO_PAY -> appContext.getString(
                R.string.payment_is_missing_after_10_business_days,
                varargs.getOrElse(0) { "" },
                varargs.getOrElse(1) { "" },
                "$PROJECT_ADDITIONAL_INFO_URL${varargs.getOrElse(2) { "" }}"
            )
            LEVEL_2_KYC_INTERNAL -> appContext.getString(R.string.level_2_kyc_internal)
            PROJECT_REQUEST_LEFT_OUT -> appContext.getString(
                R.string.project_request_left_out,
                varargs.getOrElse(0) { "" },
                varargs.getOrElse(1) { "" },
                "$PROJECT_ADDITIONAL_INFO_URL${varargs.getOrElse(2) { "" }}"
            )
            INVESTMENT_RECEIPT -> appContext.getString(R.string.investment_receipt)
            UPDATE_USER_REQUEST -> appContext.getString(R.string.update_user_request)
            DELETE_USER_REQUEST -> appContext.getString(R.string.request_to_delete_account_by_investor_level_2,
                varargs.getOrElse(0) { "" },
                varargs.getOrElse(1) { "" },
                varargs.getOrElse(2) { "" },
                varargs.getOrElse(3) { "" })
            CANCEL_NOT_CONFIRMED_INVESTMENT -> appContext.getString(R.string.cancel_not_confirmed_investment)
            PAYMENT_CANCELLED -> appContext.getString(R.string.payment_canceled)
            PERSONAL_INFO_CHANGED -> appContext.getString(R.string.personal_info_changed)
            TOKEN_TRANSFER -> appContext.getString(R.string.token_transfer)
            TRANSFER_RECEIVED -> appContext.getString(R.string.transfer_received)
            TRANSFER_SENT -> appContext.getString(R.string.transfer_sent)
            INVESTMENT_CANCELLED -> appContext.getString(R.string.investment_canceled)
            else -> appContext.getString(R.string.unexpected_notification_status_text)
        }
        return message.toHtml()
    }
}
