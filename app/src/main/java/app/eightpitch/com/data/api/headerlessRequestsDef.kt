package app.eightpitch.com.data.api

/**
 * Common entity that declares a type of a api request
 * without header and it's validation behaviour
 *
 * @param requestPath it's a path or a parts of the path that should be validated
 */
sealed class HeaderlessRequest<out T : Any>(
    protected val requestPath: T
) {
    abstract fun matchToExternalURL(externalURL: String): Boolean
}

/**
 * Entity that represents an api request that should be should be compared by
 * equals exactly
 */
class ExactRequest(requestPath: String) : HeaderlessRequest<String>(requestPath) {

    override fun matchToExternalURL(externalURL: String): Boolean = requestPath == externalURL
}

/**
 * Entity that represents an api request that should be should be compared by
 * equals partially by parts
 */
class PartialRequest(requestPaths: Array<String>) :
    HeaderlessRequest<Array<String>>(requestPaths) {

    override fun matchToExternalURL(externalURL: String): Boolean {
        val result = requestPath.map { requestPart -> externalURL.contains(requestPart) }
        return result.find { externalUrlContainsPart -> !externalUrlContainsPart } == null
    }
}