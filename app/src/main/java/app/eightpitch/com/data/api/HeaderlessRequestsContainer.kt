package app.eightpitch.com.data.api

import app.eightpitch.com.BuildConfig

object HeaderlessRequestsContainer {

    private val login = ExactRequest("${BuildConfig.ENDPOINT}user-service/mobile/login")
    private val createUser = ExactRequest("${BuildConfig.ENDPOINT}user-service/users")
    private val sendQuestionnaire = ExactRequest("${BuildConfig.ENDPOINT}user-service/investors/send-questionnaire")
    private val verifyPhoneCode = PartialRequest(arrayOf("phones", "verify_token"))
    private val emailVerification = PartialRequest(arrayOf("emails", "free"))
    private val phoneVerification = PartialRequest(arrayOf("phones", "free"))
    private val verifySMSTokenLogin = PartialRequest(arrayOf("login/phones/", "verify-token"))
    private val sendCodeByPhone = PartialRequest(arrayOf("mobile/phones", "send-code"))
    private val resendCodeByPhone = PartialRequest(arrayOf("mobile/phones", "resend-code"))
    private val sendLimitCodeByPhone = PartialRequest(arrayOf("mobile/phones", "send-limit-code"))
    private val resendLimitCodeByPhone = PartialRequest(arrayOf("mobile/phones", "resend-limit-code"))
    private val verifySMSTokenRegistration =
        PartialRequest(arrayOf("registration/phones/", "verify-token"))
    private val getActionId = PartialRequest(arrayOf("webid", "form-complete-code"))
    private val downloadDocuments = PartialRequest(arrayOf(" documents/invoices", "manual-transfers"))
    private val sendDocumentsByEmail = PartialRequest(arrayOf("email", "send-project-documents"))
    private val verify2FA = PartialRequest(arrayOf("2fa", "verify-code"))
    private val sendInitialSmsCode = PartialRequest(arrayOf("password-recovery", "send-initial-sms-code"))
    private val sendEmailLink = PartialRequest(arrayOf("password-recovery", "end-email-link"))

    val headerlessRequests = arrayOf<HeaderlessRequest<Any>>(
        login, createUser, verifyPhoneCode, emailVerification, phoneVerification,
        verifySMSTokenLogin, sendCodeByPhone, resendCodeByPhone, verifySMSTokenRegistration, getActionId,
        sendQuestionnaire, downloadDocuments, sendDocumentsByEmail, verify2FA, sendInitialSmsCode, sendEmailLink,
        sendLimitCodeByPhone, resendLimitCodeByPhone
    )

    /**
     * Method will check if [externalURL] is
     * one of the [HeaderlessRequest]'s
     */
    fun matchToExternalURL(externalURL: String): Boolean {
        headerlessRequests.forEach {
            if (it.matchToExternalURL(externalURL))
                return true
        }
        return false
    }
}