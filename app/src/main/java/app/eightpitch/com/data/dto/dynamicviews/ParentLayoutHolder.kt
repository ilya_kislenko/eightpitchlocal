package app.eightpitch.com.data.dto.dynamicviews

import android.view.ViewGroup

/**
 * An interface which holds a reference to a parent ViewGroup,
 * and able to provide it for any operations on a UI layer.
 * @see ImageLinkAdapter
 * @see TiledImageAdapter
 */
interface ParentLayoutHolder {
    fun getParentLayout(): ViewGroup?
}