package app.eightpitch.com.data.dto.changephonenumber

data class UpdatePhoneRequestBody(
    val interactionId: String = "",
    val phone: String = "",
    val token: String = ""
)