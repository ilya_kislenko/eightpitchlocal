package app.eightpitch.com.data.dto.sms

import app.eightpitch.com.extensions.orDefaultValue
import com.google.gson.annotations.SerializedName

data class SendTokenLimitResponse(
    @SerializedName("interactionId")
    private val _interactionId: String?,
    @SerializedName("remainingAttempts")
    private val _remainingAttempts: Int?
) {
    val interactionId: String
        get() = _interactionId.orDefaultValue("")
    val remainingAttempts: Int
        get() = _remainingAttempts.orDefaultValue(1)
}