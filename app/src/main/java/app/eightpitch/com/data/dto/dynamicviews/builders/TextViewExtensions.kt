package app.eightpitch.com.data.dto.dynamicviews.builders

import android.graphics.*
import android.graphics.Typeface.*
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.res.ResourcesCompat
import app.eightpitch.com.R
import app.eightpitch.com.data.dto.dynamicviews.*
import app.eightpitch.com.extensions.*

fun AppCompatTextView.setupTextView(element: TextUIElement) = apply {
    setupDefaultMarginLayoutParams()
    tag = DefaultUIElementArguments(element.id)
    typeface = create(
        ResourcesCompat.getFont(context, R.font.roboto_light),
        when {
            element.bold && element.italic -> BOLD_ITALIC
            element.bold -> BOLD
            element.italic -> ITALIC
            else -> NORMAL
        }
    )
    letterSpacing = -0.02f
    gravity = element.gravity
    setDipTextSize(element.fontSize.toFloat())
    setTextColor(Color.parseColor(element.color))
    paintFlags = if (element.underline) Paint.UNDERLINE_TEXT_FLAG else paintFlags
}