package app.eightpitch.com.data.dto.user

import android.os.Parcel
import android.os.Parcelable
import app.eightpitch.com.extensions.readStringSafe
import com.google.gson.annotations.SerializedName

data class UserAddress(
    @SerializedName("city")
    val city: String = "",
    @SerializedName("country")
    val country: String = "",
    @SerializedName("street")
    val street: String = "",
    @SerializedName("streetNo")
    val streetNumber: String = "",
    @SerializedName("zip")
    val zipCode: String = ""
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(city)
        parcel.writeString(country)
        parcel.writeString(street)
        parcel.writeString(streetNumber)
        parcel.writeString(zipCode)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<UserAddress> {
        override fun createFromParcel(parcel: Parcel): UserAddress {
            return UserAddress(parcel)
        }

        override fun newArray(size: Int): Array<UserAddress?> {
            return arrayOfNulls(size)
        }
    }
}