package app.eightpitch.com.data.dto.dynamicviews.builders

import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import app.eightpitch.com.data.dto.dynamicviews.ChartData
import app.eightpitch.com.data.dto.dynamicviews.ChartUIElement
import app.eightpitch.com.data.dto.dynamicviews.DoughnutData
import app.eightpitch.com.extensions.*
import app.eightpitch.com.utils.Logger
import com.github.mikephil.charting.charts.*
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet

//BarChart region
fun Context.buildBarChart(chartUIElement: ChartUIElement): View {
    return BarChart(this).apply {
        layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 700)
        animateY(2000)
        setDrawGridBackground(false)
        if (chartUIElement.data.isNotEmpty())
            setBarData(this, readBarData(chartUIElement.data))
    }
}

private fun readBarData(list: List<List<String>>): List<ChartData> {
    val chartDataList = arrayListOf<ChartData>()

    val xValuesList = arrayListOf<Float>()
    val yValuesList = arrayListOf<ArrayList<Float>>()
    loop@ for (i in 1 until list.size) {
        val yList = arrayListOf<Float>()
        mark@ for ((index, value) in list[i].withIndex()) {
            when (index) {
                0 -> xValuesList.add(value.parseToBigDecimalSafe().toFloat())
                1, 2 -> continue@mark
                else -> {
                    if (value.isNullOrEmpty())
                        continue@loop
                    yList.add(value.parseToBigDecimalSafe().toFloat())
                }
            }
        }
        yValuesList.add(yList)
    }

    val labelList = arrayListOf<String>()
    val hashColorList = arrayListOf<String>()

    loop@ for (i in 1 until list.size) {
        mark@ for ((index, value) in list[i].withIndex()) {
            when (index) {
                0 -> if (list[i][1].isNullOrEmpty()) break@loop
                1 -> labelList.add(value)
                2 -> hashColorList.add(value)
                else -> break@mark
            }
        }
    }

    for (i in labelList.indices) {
        val chartData = ChartData(
            label = labelList[i],
            hashColor = hashColorList[i],
            xValuesList = xValuesList,
            yValuesList = yValuesList[i]
        )
        chartDataList.add(chartData)
    }
    return chartDataList
}

private fun setBarData(barChart: BarChart, chartDataList: List<ChartData>) {
    val dataSets = arrayListOf<IBarDataSet>()
    chartDataList.forEach { chartData ->
        val values = arrayListOf<BarEntry>()
        chartData.run {
            for ((index, x) in xValuesList.withIndex()) {
                values.add(BarEntry(x, yValuesList[index]))
            }
            val barDataSet = BarDataSet(values, label).apply {
                color = Color.parseColor(hashColor)
            }
            dataSets.add(barDataSet)
        }
    }
    val barData = BarData(dataSets)
    barChart.apply {
        description = Description().apply { text = "" }
        data = barData
        data.barWidth = 12F
        data.notifyDataChanged()
        notifyDataSetChanged()
        val xValuesList = chartDataList[0].xValuesList
        xAxis.also {
            it.axisMinimum = xValuesList[0] - 20f
            it.axisMaximum = xValuesList[xValuesList.size - 1] + 20f
        }
        groupBars(xValuesList)
        invalidate()
    }
}

private fun BarChart.groupBars(xValuesList: List<Float>) {
    try {
        groupBars(xValuesList[0] - 20f, 20f, 0f)
    } catch (e: Exception) {
        Logger.logError(Logger.TAG_GENERIC_ERROR, e.message.orDefaultValue("Group bars error"))
    }
}
//endregion

//LineChart region
fun Context.buildLineChart(chartUIElement: ChartUIElement): View {
    return LineChart(this).apply {
        layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 700)
        setLineChartData(this, readLineData(chartUIElement.data))
    }
}

private fun readLineData(list: List<List<String>>): List<ChartData> {
    val chartDataList = arrayListOf<ChartData>()

    val xValuesList = arrayListOf<Float>()
    val yValuesList = arrayListOf<ArrayList<Float>>()
    loop@ for (i in 1 until list.size) {
        val yList = arrayListOf<Float>()
        mark@ for ((index, value) in list[i].withIndex()) {
            when (index) {
                0 -> xValuesList.add(value.parseToBigDecimalSafe().toFloat())
                1, 2, 3 -> continue@mark
                else -> {
                    if (value.isNullOrEmpty())
                        continue@loop
                    yList.add(value.parseToBigDecimalSafe().toFloat())
                }
            }
        }
        yValuesList.add(yList)
    }

    val labelList = arrayListOf<String>()
    val rgbColorList = arrayListOf<String>()
    val hashColorList = arrayListOf<String>()

    loop@ for (i in 1 until list.size) {
        mark@ for ((index, value) in list[i].withIndex()) {
            when (index) {
                0 -> if (list[i][1].isNullOrEmpty()) break@loop
                1 -> labelList.add(value)
                2 -> rgbColorList.add(value)
                3 -> hashColorList.add(value)
                else -> break@mark
            }
        }
    }

    for (i in labelList.indices) {
        val chartData = ChartData(
            label = labelList[i],
            rgbColor = rgbColorList[i],
            hashColor = hashColorList[i],
            xValuesList = xValuesList,
            yValuesList = yValuesList[i]
        )
        chartDataList.add(chartData)
    }
    return chartDataList
}

private fun setLineChartData(lineChart: LineChart, chartDataList: List<ChartData>) {
    val dataSets = arrayListOf<ILineDataSet>()
    chartDataList.forEach { chartData ->
        val values = arrayListOf<Entry>()
        chartData.run {
            for ((index, x) in xValuesList.withIndex()) {
                values.add(Entry(x, yValuesList[index]))
            }
            val lineDataSet = LineDataSet(values, label).apply {
                color = Color.parseColor(hashColor)
                circleHoleColor = Color.parseColor(hashColor)
            }
            dataSets.add(lineDataSet)
        }
    }
    lineChart.apply {
        data = LineData(dataSets)
        description = Description().apply { text = "" }
        invalidate()
    }
}
//endregion

//DoughnutChart region
fun Context.buildDoughnutChart(chartUIElement: ChartUIElement): View {
    return PieChart(this).apply {
        layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 700)
        setPieChartData(this, readPieData(chartUIElement.data))
    }
}

private fun readPieData(list: List<List<String>>): List<DoughnutData> {
    val chartDataList = arrayListOf<DoughnutData>()

    val labelList = arrayListOf<String>()
    val progressList = arrayListOf<Float>()
    val firstColorList = arrayListOf<String>()
    val secondColorList = arrayListOf<String>()
    loop@ for (i in 1 until list.size) {
        mark@ for ((index, value) in list[i].withIndex()) {
            when (index) {
                0 -> labelList.add(value)
                1 -> progressList.add(value.parseToBigDecimalSafe().toFloat())
                2 -> firstColorList.add(value)
                3 -> secondColorList.add(value)
            }
        }
    }

    for (i in labelList.indices) {
        val chartData = DoughnutData(
            label = labelList[i],
            progress = progressList[i],
            firstHashColor = firstColorList[i],
            secondHashColor = secondColorList[i]
        )
        chartDataList.add(chartData)
    }
    return chartDataList
}

private fun setPieChartData(pieChart: PieChart, chartDataList: List<DoughnutData>) {
    val values = arrayListOf<PieEntry>()
    val colorList = arrayListOf<Int>()
    val textColorList = arrayListOf<Int>()
    chartDataList.forEach { chartData ->
        chartData.run {
            colorList.add(Color.parseColor(firstHashColor))
            textColorList.add(Color.parseColor(secondHashColor))
            values.add(PieEntry(progress, label))
        }
    }
    val pieDataSet = PieDataSet(values, "").apply {
        colors = colorList
        setValueTextColors(textColorList)
    }
    val pieData = PieData(pieDataSet)


    pieChart.apply {
        data = pieData
        description = Description().apply { text = "" }
        invalidate()
    }
}
//endregion