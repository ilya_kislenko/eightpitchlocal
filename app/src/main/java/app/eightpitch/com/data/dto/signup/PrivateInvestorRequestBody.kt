package app.eightpitch.com.data.dto.signup

import android.os.Parcel
import android.os.Parcelable
import app.eightpitch.com.data.dto.user.User.CREATOR.PRIVATE_INVESTOR
import app.eightpitch.com.extensions.NO_TYPE
import app.eightpitch.com.extensions.readStringSafe

class PrivateInvestorRequestBody(
    email: String = "",
    deviceId: String = "",
    password: String = "",
    lastName: String = "",
    firstName: String = "",
    phoneNumber: String = "",
    interactionId: String = "",
    isSubscribedToEmail: Boolean = false,
    prefix: String = NO_TYPE
) : Parcelable, CommonInvestorRequestBody(
    email, deviceId, password, lastName, firstName, phoneNumber,
    interactionId, "", isSubscribedToEmail, PRIVATE_INVESTOR, prefix
) {

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readByte() != 0.toByte(),
        parcel.readStringSafe())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        super.writeToParcel(parcel, flags)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<PrivateInvestorRequestBody> {
        override fun createFromParcel(parcel: Parcel): PrivateInvestorRequestBody {
            return PrivateInvestorRequestBody(parcel)
        }

        override fun newArray(size: Int): Array<PrivateInvestorRequestBody?> {
            return arrayOfNulls(size)
        }
    }
}