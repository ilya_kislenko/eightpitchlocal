package app.eightpitch.com.data.dto.dynamicviews

import android.os.*
import androidx.annotation.StringDef
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.projects.mappers.UIElementsConverter
import com.google.gson.annotations.SerializedName

/**
 * Class that represents a `Project Details` UI elements,
 * it's a backend presentation.
 *
 * @param payload it's a json that will be specific for particular tab's [type].
 * You could see the parsing in [UIElementsConverter].
 */
data class ProjectPageBlock(
    @SerializedName("type")
    @BlockTypes val _type: String? = NO_TYPE,
    @SerializedName("payload")
    val _payload: String? = "", // json serialized data, block-specific
    @SerializedName("children")
    val _children: List<String>? = listOf() //id list
) : IdField(), Parcelable {

    @BlockTypes val type: String
        get() = _type.orDefaultValue(NO_TYPE)
    val payload: String
        get() = _payload.orDefaultValue("")
    val children: List<String>
        get() = _children.orDefaultValue(listOf())

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readListSafe(listOf(), ProjectPageBlock::class.java.classLoader!!))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        super.writeToParcel(parcel, flags)
        parcel.writeString(type)
        parcel.writeString(payload)
        parcel.writeStringList(children)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<ProjectPageBlock> {
        override fun createFromParcel(parcel: Parcel): ProjectPageBlock {
            return ProjectPageBlock(parcel)
        }

        override fun newArray(size: Int): Array<ProjectPageBlock?> {
            return arrayOfNulls(size)
        }

        const val GROUP = "group"
        const val TEXT = "text"
        const val IMAGE = "image"
        const val TEXT_AND_IMAGE = "textAndImage"
        const val FAQ = "faq"
        const val LIST = "list"
        const val TABLE = "table"
        const val ICONS = "icons"
        const val VIDEO = "video"
        const val DIVIDER = "divider"
        const val DUAL_COLUMN_IMAGES = "dual-column-images"
        const val FACTS = "facts"
        const val TILED_IMAGES = "tiles-images"
        const val IMAGE_LINK = "imageLink"
        const val DUAL_COLUMN_TEXT = "dual-column-text"
        const val BUTTON_BLOCK = "button_block"
        const val DISCLAIMER = "disclaimer"
        const val LINKED_TEXT = "linked_text"
        const val CHART = "chart"
        const val HEADER = "header"
        const val DEPARTMENT = "department"
        const val MENTION = "mention"


        @Retention(AnnotationRetention.SOURCE)
        @StringDef(
            NO_TYPE, GROUP, TEXT, IMAGE, TEXT_AND_IMAGE,
            FAQ, LIST, TABLE, ICONS, VIDEO, DIVIDER, CHART,
            DUAL_COLUMN_IMAGES, FACTS, TILED_IMAGES, IMAGE_LINK,
            DUAL_COLUMN_TEXT, BUTTON_BLOCK, DISCLAIMER, LINKED_TEXT,
            HEADER, DEPARTMENT, MENTION
        )
        annotation class BlockTypes
    }
}