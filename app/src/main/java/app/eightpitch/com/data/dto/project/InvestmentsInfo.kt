package app.eightpitch.com.data.dto.project

import app.eightpitch.com.extensions.NO_TYPE
import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

/**
 * Contains information about investments in the project.
 *
 * List with information about the investment,
 * the status of the latest investments, and the total amount of investments in tokens
 *
 * @param investments - list with information about the investment
 * @param lastInvestmentStatus - status of the latest investments
 * @param totalAmountOfTokens - total amount of investments in tokens
 */
class InvestmentsInfo(
    @SerializedName("investments")
    var investments: List<ProjectInvestment> = arrayListOf(),
    @SerializedName("lastInvestmentStatus")
    @ProjectInvestment.Companion.InvestmentStatus var lastInvestmentStatus: String = NO_TYPE,
    @SerializedName("totalAmountOfTokens")
    var totalAmountOfTokens: BigDecimal = BigDecimal.ZERO
)