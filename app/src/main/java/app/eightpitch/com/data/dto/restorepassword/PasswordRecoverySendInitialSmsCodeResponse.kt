package app.eightpitch.com.data.dto.restorepassword

import androidx.annotation.StringDef
import app.eightpitch.com.extensions.orDefaultValue
import com.google.gson.annotations.SerializedName

class PasswordRecoverySendInitialSmsCodeResponse(
    @SerializedName("interactionId")
    private val _interactionId: String?,
    @SerializedName("status")
    private val _status: String?
) {
    val interactionId: String
        get() = _interactionId.orDefaultValue("")

    @EmailSendingStatus val status: String
        get() = _status.orDefaultValue("")

    companion object {

        const val SUCCESS = "SUCCESS"
        const val UNSUCCESS = "UNSUCCESS"

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(SUCCESS, UNSUCCESS)
        annotation class EmailSendingStatus
    }
}