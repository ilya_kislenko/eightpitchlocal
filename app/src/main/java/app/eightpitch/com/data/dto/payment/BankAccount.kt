package app.eightpitch.com.data.dto.payment

import com.google.gson.annotations.SerializedName

data class BankAccount(
    @SerializedName("bic")
    val bic: String,
    @SerializedName("iban")
    val IBAN: String,
    @SerializedName("owner")
    val owner: String
)