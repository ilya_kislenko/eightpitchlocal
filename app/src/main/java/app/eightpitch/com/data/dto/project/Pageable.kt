package app.eightpitch.com.data.dto.project

import com.google.gson.annotations.SerializedName

/**
 * The class contains information about the pagination of the request.
 *
 * @param page - Loaded page
 * @param size  - size of the list of objects on the page
 * @param sort - information about sorting
 */
class Pageable(
    @SerializedName("page")
    var page: Int = 0,
    @SerializedName("size")
    var size: Int = 0,
    @SerializedName("sort")
    var sort: Sort = Sort()
)