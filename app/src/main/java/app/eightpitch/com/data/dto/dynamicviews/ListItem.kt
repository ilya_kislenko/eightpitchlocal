package app.eightpitch.com.data.dto.dynamicviews

import android.os.Parcel
import android.os.Parcelable
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.extensions.orDefaultValue
import app.eightpitch.com.extensions.readStringSafe
import app.eightpitch.com.ui.dynamicdetails.DynamicProjectDetailsChildFragment
import com.google.gson.annotations.SerializedName

/**
 * It's an object that represents a single [RecyclerView]'s item
 * on a [DynamicProjectDetailsChildFragment].
 */
data class ListItem(
    @SerializedName("text")
    val _text: String? = ""
) : Parcelable {

    val text: String
        get() = _text.orDefaultValue("")

    constructor(parcel: Parcel) : this(parcel.readStringSafe())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(text)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<ListItem> {
        override fun createFromParcel(parcel: Parcel): ListItem {
            return ListItem(parcel)
        }

        override fun newArray(size: Int): Array<ListItem?> {
            return arrayOfNulls(size)
        }
    }
}
