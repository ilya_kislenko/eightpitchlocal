package app.eightpitch.com.data.dto.changingpassword

import androidx.annotation.StringDef
import app.eightpitch.com.extensions.NO_TYPE

class VerifyOldPasswordResponseBody(
    val interactionId: String = "",
    @PasswordStatus val status: String = NO_TYPE
) {

    companion object {

        const val SUCCESS = "SUCCESS"
        const val INVALID_PASSWORD = "INVALID_PASSWORD"

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(NO_TYPE, SUCCESS, INVALID_PASSWORD)
        annotation class PasswordStatus
    }
}