package app.eightpitch.com.data.dto.notifications

class NotificationsResponseBody(
    val notifications: List<Notification>,
    val totalNotifications: Int
)