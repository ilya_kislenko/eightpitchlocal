package app.eightpitch.com.data.dto.projectdetails

import com.google.gson.annotations.SerializedName


data class PaymentConfiguration(
    @SerializedName("accountHolderName")
    var accountHolderName: String = "",
    @SerializedName("bic")
    var bic: String = "",
    @SerializedName("fintecsystemApiKey")
    var fintecsystemApiKey: String = "",
    @SerializedName("iban")
    var iban: String = "",
    @SerializedName("id")
    var id: Long = -1,
    @SerializedName("klarnaApiKey")
    var klarnaApiKey: String = "",
    @SerializedName("klarnaCustomerNumber")
    var klarnaCustomerNumber: Int = -1,
    @SerializedName("klarnaProjectId")
    var klarnaProjectId: Int = -1
)