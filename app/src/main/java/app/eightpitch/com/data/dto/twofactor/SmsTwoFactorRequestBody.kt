package app.eightpitch.com.data.dto.twofactor

import com.google.gson.annotations.SerializedName

data class SmsTwoFactorRequestBody(
    @SerializedName("interactionId")
    val interactionId: String,
    @SerializedName("token")
    val token: String
)