package app.eightpitch.com.data.dto.dynamicviews.language

import android.os.*
import app.eightpitch.com.data.dto.user.LanguageRequestBody
import app.eightpitch.com.extensions.*
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*
import kotlin.collections.HashMap

typealias TranslationMap = HashMap<String, String>
typealias I18nMaps = HashMap<String, TranslationMap>

/**
 * @param defaultLanguage key for getting translated string in the second param
 * @param i18nMaps map, when key - language constant, value - translated string
 */
data class I18nData(
    @SerializedName("defaultLanguage")
    @LanguageRequestBody.Companion.UserLanguage val _defaultLanguage: String? = NO_TYPE,
    @SerializedName("i18nMaps")
    val _i18nMaps: I18nMaps? = I18nMaps()
) : Parcelable, Serializable {

    val defaultLanguage
        get() = _defaultLanguage.orDefaultValue(Locale.getDefault().getUserLanguage())

    val i18nMaps
        get() = _i18nMaps.orDefaultValue(I18nMaps())

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readI18nMaps())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(defaultLanguage)
        parcel.writeI18nMaps(i18nMaps)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<I18nData> {
        override fun createFromParcel(parcel: Parcel): I18nData {
            return I18nData(parcel)
        }

        override fun newArray(size: Int): Array<I18nData?> {
            return arrayOfNulls(size)
        }
    }
}

private fun Parcel.readI18nMaps(): I18nMaps {
    val size = readInt()
    val i18nMaps = I18nMaps()
    for (i in 0 until size) {
        i18nMaps[readStringSafe()] = readTranslationMap()
    }

    return i18nMaps
}

private fun Parcel.readTranslationMap(): TranslationMap {
    val size = readInt()
    val translationMap = TranslationMap()
    for (i in 0 until size) {
        translationMap[readStringSafe()] = readStringSafe()
    }
    return translationMap
}

private fun Parcel.writeI18nMaps(i18nMaps: I18nMaps) {
    writeInt(i18nMaps.size)
    for (entry in i18nMaps.entries) {
        writeString(entry.key)
        writeInt(entry.value.size)
        writeTranslationMap(entry.value)
    }
}

private fun Parcel.writeTranslationMap(translationMap: TranslationMap) {
    for (entry in translationMap.entries) {
        writeString(entry.key)
        writeString(entry.value)
    }
}