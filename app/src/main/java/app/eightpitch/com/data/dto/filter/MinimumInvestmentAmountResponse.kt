package app.eightpitch.com.data.dto.filter

import app.eightpitch.com.data.dto.filter.AmountRange.*
import com.google.gson.annotations.SerializedName


data class MinimumInvestmentAmountResponse(
    @SerializedName("RANGE_MORE_THAN_100000_EUR")
    private val rangeMoreThan100000Eur: Boolean = false,
    @SerializedName("RANGE_5001_10000_EUR")
    private val range500110000Eur: Boolean = false,
    @SerializedName("RANGE_25001_50000_EUR")
    private val range2500150000Eur: Boolean = false,
    @SerializedName("RANGE_50001_100000_EUR")
    private val range50001100000Eur: Boolean = false,
    @SerializedName("RANGE_0_1000_EUR")
    private val range01000Eur: Boolean = false,
    @SerializedName("RANGE_1001_5000_EUR")
    private val range10015000Eur: Boolean = false,
    @SerializedName("RANGE_10001_25000_EUR")
    private val range1000125000Eur: Boolean = false
) {


    fun getActualRange(): List<AmountRange>{
        val resultList = arrayListOf<AmountRange>()
        if(range01000Eur)
            resultList.add(RANGE_0_1000_EUR)
        if(range10015000Eur)
            resultList.add(RANGE_1001_5000_EUR)
        if(range500110000Eur)
            resultList.add(RANGE_5001_10000_EUR)
        if(range1000125000Eur)
            resultList.add(RANGE_10001_25000_EUR)
        if(range2500150000Eur)
            resultList.add(RANGE_25001_50000_EUR)
        if(range50001100000Eur)
            resultList.add(RANGE_50001_100000_EUR)
        if(rangeMoreThan100000Eur)
            resultList.add(RANGE_MORE_THAN_100000_EUR)

        return resultList
    }
}