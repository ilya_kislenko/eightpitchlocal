package app.eightpitch.com.data.dto.user

import androidx.annotation.StringDef
import app.eightpitch.com.extensions.NO_TYPE

data class LanguageRequestBody(
    @UserLanguage val language: String = EN
) {

    companion object {

        const val EN = "en"
        const val DE = "de"

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(EN, DE, NO_TYPE)
        annotation class UserLanguage

        val availableLanguages = listOf(EN, DE)
    }
}

