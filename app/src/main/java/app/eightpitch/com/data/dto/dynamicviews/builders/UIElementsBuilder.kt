package app.eightpitch.com.data.dto.dynamicviews.builders

import android.content.Context
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.Typeface
import android.graphics.Typeface.NORMAL
import android.graphics.Typeface.create
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.GradientDrawable.OVAL
import android.graphics.drawable.GradientDrawable.RECTANGLE
import android.view.Gravity
import android.view.Gravity.CENTER
import android.view.Gravity.START
import android.view.Gravity.TOP
import android.view.LayoutInflater
import android.view.View
import android.view.View.OVER_SCROLL_NEVER
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.view.ViewGroup.MarginLayoutParams
import android.view.ViewGroup.TEXT_ALIGNMENT_CENTER
import android.widget.HorizontalScrollView
import android.widget.ImageView.ScaleType.CENTER_CROP
import android.widget.ImageView.ScaleType.MATRIX
import android.widget.LinearLayout
import android.widget.LinearLayout.HORIZONTAL
import android.widget.LinearLayout.LayoutParams
import android.widget.LinearLayout.VERTICAL
import android.widget.RelativeLayout
import android.widget.TableLayout
import android.widget.TableRow
import androidx.appcompat.view.ContextThemeWrapper
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.content.res.ResourcesCompat.*
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import app.eightpitch.com.R
import app.eightpitch.com.data.dto.dynamicviews.ChartUIElement.CREATOR.BAR
import app.eightpitch.com.data.dto.dynamicviews.ChartUIElement.CREATOR.DOUGHNUT
import app.eightpitch.com.data.dto.dynamicviews.ChartUIElement.CREATOR.HALF_DOUGHNUT
import app.eightpitch.com.data.dto.dynamicviews.ChartUIElement.CREATOR.LINE
import app.eightpitch.com.data.dto.dynamicviews.*
import app.eightpitch.com.data.dto.dynamicviews.language.I18nData
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.dynamicdetails.*
import app.eightpitch.com.utils.SafeLinearLayoutManager
import app.eightpitch.com.views.RoundedCornersButton
import kotlinx.android.synthetic.main.item_fact.view.*
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

/**
 * Class that is responsible for a building a concrete [View] by a
 * type of [UIElement].
 */
class UIElementsBuilder
@Inject constructor(
    private val appContext: Context
) {

    companion object {
        const val SANS_SERIF_FONT_FAMILY = "sans-serif"
    }

    fun buildTextView(textUIElement: TextUIElement, i18nData: I18nData): View {
        val container = LinearLayout(appContext).apply {
            orientation = VERTICAL
            tag = DefaultUIElementArguments(textUIElement.id)
        }
        container.addView(buildHeader(
            HeaderUIElement(
                _header = textUIElement.header,
                _preHeader = textUIElement.preHeader,
                _headingSize = textUIElement.headingSizeString
            ), i18nData)
        )
        container.addView(AppCompatTextView(appContext).setupTextView(textUIElement).apply {
            text = i18nData.getLocalizedText(textUIElement.text)
        })
        return container
    }

    fun buildDividerView(dividerUIElement: DividerUIElement): View {
        return View(appContext).apply {
            val height = resources.getDpBy(dividerUIElement.height.toFloat()).toInt()
            layoutParams = LayoutParams(MATCH_PARENT, height)
            tag = DefaultUIElementArguments(dividerUIElement.id)
        }
    }

    fun buildFAQView(faqUiElement: FAQUIElement): View {
        val container = LinearLayout(appContext)

        if (Locale.getDefault().getUserLanguage() != faqUiElement.language)
            return container

        container.apply {
            setupDefaultMarginLayoutParams()
            orientation = VERTICAL
            tag = DefaultUIElementArguments(faqUiElement.id)
        }

        val header = AppCompatTextView(appContext).apply {
            createHeaderView()
            text = faqUiElement.header
        }

        container.run {
            addView(header)
            addView(RecyclerView(appContext).apply {
                layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT).also {
                    it.setMargins(0, resources.getDpBy(27f).toInt(), 0, 0)
                }
                overScrollMode = OVER_SCROLL_NEVER
                layoutManager = SafeLinearLayoutManager(context, RecyclerView.VERTICAL, false)
                adapter = FAQAdapter(faqUiElement.items)
            })
        }

        return container
    }

    fun buildIconsView(iconsUIElement: IconsUIElement): View {
        val array = ArrayList<View>()
        iconsUIElement.items.forEach { item ->
            val iconView = AppCompatImageView(appContext).apply {
                tag = IconsUIElementArguments("", item.link, item.imageId)
                val size = resources.getDimensionPixelOffset(R.dimen.iconSize)
                layoutParams = LayoutParams(size, size).also {
                    it.setMargins(0, 0, resources.getDimensionPixelOffset(R.dimen.default_left_right_padding), 0)
                }
                scaleType = CENTER_CROP
            }
            array.add(iconView)
        }

        val container = LinearLayout(appContext).apply {
            setupDefaultMarginLayoutParams()
            gravity = CENTER
            orientation = HORIZONTAL
            tag = DefaultUIElementArguments(iconsUIElement.id)
        }
        array.forEach { container.addView(it) }
        return container
    }

    fun buildImageView(imageUIElement: ImageUIElement, i18nData: I18nData): View {
        val container = RelativeLayout(appContext).apply {
            layoutParams = RelativeLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT).also {
                it.setMargins(0, resources.getDimensionPixelOffset(R.dimen.margin_16), 0, 0)
            }
            tag = DefaultUIElementArguments(imageUIElement.id)
        }
        container.addView(AppCompatImageView(appContext).apply {
            tag = ImageUIElementArguments("", imageUIElement.imageId, false)
            layoutParams = RelativeLayout.LayoutParams(if (imageUIElement.fullWidth) MATCH_PARENT
            else WRAP_CONTENT, WRAP_CONTENT)
        })

        if (imageUIElement.useText) {
            container.addView(AppCompatTextView(appContext).apply {
                setupTextView(imageUIElement).also {
                    layoutParams = RelativeLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
                        addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
                        setPadding(
                            resources.getDimensionPixelOffset(R.dimen.default_left_right_padding),
                            0,
                            resources.getDimensionPixelOffset(R.dimen.default_left_right_padding),
                            resources.getDimensionPixelOffset(R.dimen.default_top_bottom_padding)
                        )
                    }
                    it.text = i18nData.getLocalizedText(imageUIElement.text)
                }
            })
        }

        return container
    }

    fun buildImageAndTextView(imageAndTextUIElement: ImageAndTextUIElement, i18nData: I18nData): View {
        val container = LinearLayout(appContext).apply {
            setupDefaultMarginLayoutParams()
            orientation = VERTICAL
            tag = DefaultUIElementArguments(imageAndTextUIElement.id)
        }
        val image = AppCompatImageView(appContext).apply {
            setupDefaultLayoutParams()
            tag = ImageUIElementArguments(imageAndTextUIElement.id, imageAndTextUIElement.imageId, false)
        }
        val description = AppCompatTextView(appContext).apply {
            layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT).also {
                it.setMargins(0, resources.getDimensionPixelOffset(R.dimen.margin_20), 0, 0)
            }
            text = i18nData.getLocalizedText(imageAndTextUIElement.imageDescription)
            setTextAppearance(R.style.Body12RobotoLight)
        }
        val header = AppCompatTextView(appContext).apply {
            createHeaderView()
            text = i18nData.getLocalizedText(imageAndTextUIElement.header)
            layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT).also {
                it.setMargins(0, resources.getDimensionPixelOffset(R.dimen.margin_20), 0, 0)
            }
            gravity = CENTER
            textAlignment = gravity
        }
        val text = AppCompatTextView(appContext).apply {
            layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT).also {
                it.setMargins(0, resources.getDimensionPixelOffset(R.dimen.margin_20), 0, 0)
            }
            text = i18nData.getLocalizedText(imageAndTextUIElement.text)
            setTextAppearance(R.style.Body14RobotoLight)
        }
        container.addViews(image, description, header, text)
        return container
    }

    fun buildListView(listUIElement: ListUIElement, i18nData: I18nData): View {
        val views = ArrayList<View>()
        val headerView = AppCompatTextView(appContext).apply {
            createHeaderView()
            text = i18nData.getLocalizedText(listUIElement.header)
            gravity = TEXT_ALIGNMENT_CENTER
        }
        views.add(headerView)
        listUIElement.items.forEach { item ->
            val textView = AppCompatTextView(appContext).apply {
                layoutParams = MarginLayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
                    setMargins(0, resources.getDimensionPixelOffset(R.dimen.margin_8), 0, 0)
                    setPadding(resources.getDimensionPixelOffset(R.dimen.margin_8), 0, 0, 0)
                }
                setCompoundDrawablesWithIntrinsicBounds(GradientDrawable().also {
                    val size = resources.getDimensionPixelOffset(R.dimen.margin_8)
                    it.setSize(size, size)
                    it.shape = OVAL
                    it.setColor(ContextCompat.getColor(context, R.color.darkBlue))
                }, null, null, null)
                compoundDrawablePadding = resources.getDimensionPixelOffset(R.dimen.margin_8)
                text = i18nData.getLocalizedText(item.text)
            }
            views.add(textView)
        }
        val container = LinearLayout(appContext).apply {
            setupDefaultMarginLayoutParams()
            orientation = VERTICAL
            tag = DefaultUIElementArguments(listUIElement.id)
        }
        views.forEach { container.addView(it) }

        return container
    }

    fun buildTableView(tableUIElement: TableUIElement, i18nData: I18nData): View {

        val tableView = TableLayout(appContext).apply {

            layoutParams = TableLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
                setPadding(0, 0, 4, 4)
                setBackgroundColor(ContextCompat.getColor(appContext, R.color.gray20))
            }
            tag = DefaultUIElementArguments(tableUIElement.id)
            tableUIElement.run {

                val columns = colsNumber.toInt()
                val rowsCount = rowsNumber.toInt()

                for (i in 0 until rowsCount) {

                    val row = TableRow(context).apply {
                        layoutParams = TableRow.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
                        setColumnStretchable(i, true)
                    }

                    for (j in 0 until columns) {
                        val cell = when {
                            i != 0 -> createTableCell(i)
                            else -> createTableHeaderCell(j == 0, headerColor)
                        }
                        row.addView(cell.apply {
                            layoutParams = TableRow.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
                            text = i18nData.getLocalizedText(rows[i][j].text)
                            gravity = START
                            setPadding(30, 30, 30, 30)
                        })
                    }

                    addView(row, i)
                }
            }
        }

        return HorizontalScrollView(appContext).apply {
            setupDefaultMarginLayoutParams()
            isFillViewport = true
            isHorizontalScrollBarEnabled = false
            tag = DefaultUIElementArguments(tableUIElement.id)
            addView(tableView)
        }
    }

    //TODO strokes
    private fun createTableHeaderCell(isFirstItem: Boolean, headerColor: String) = AppCompatTextView(appContext).apply {
        setTextAppearance(R.style.Body14RobotoRegular)
        setTextColor(ContextCompat.getColor(appContext, R.color.white))
        val headerBackground = GradientDrawable().also {
            it.shape = RECTANGLE
            it.setColor(Color.parseColor(headerColor))
        }
        background = headerBackground
    }

    private fun createTableCell(rowCounter: Int) = AppCompatTextView(appContext).apply {
        setTextAppearance(R.style.Body12RobotoLight)
        val isEven = rowCounter % 2 == 0
        background = ContextCompat.getDrawable(appContext, if (isEven) R.drawable.even_stroked_cell_background
        else R.drawable.odd_stroked_cell_background)
    }

    fun buildVimeoView(vimeoUIElement: VimeoUIElement, i18nData: I18nData): View {
        val container = LinearLayout(appContext).apply {
            setupDefaultMarginLayoutParams()
            orientation = VERTICAL
            tag = DefaultUIElementArguments(vimeoUIElement.id)
        }

        container.addView(AppCompatImageView(appContext).apply {
            tag = VimeoUIElementArguments("", vimeoUIElement.vimeoId)
            setupDefaultLayoutParams()
            background = ContextCompat.getDrawable(appContext, R.drawable.vimeo_layout)
        })

        container.addView(AppCompatTextView(appContext).apply {
            createHeaderView()
            text = i18nData.getLocalizedText(vimeoUIElement.header)
        })

        container.addView(AppCompatTextView(appContext).apply {
            setupDefaultLayoutParams()
            text = i18nData.getLocalizedText(vimeoUIElement.text)
            textSize = 14F
            typeface = create(SANS_SERIF_FONT_FAMILY, NORMAL)
        })

        return container
    }

    fun buildDualColumnImages(dualColumnImagesUIElement: DualColumnImagesUIElement, i18nData: I18nData): View {
        val container = LinearLayout(appContext).apply {
            setupDefaultMarginLayoutParams()
            orientation = VERTICAL
            tag = DefaultUIElementArguments(dualColumnImagesUIElement.id)
        }

        fun LayoutParams.setupMargins() {
            setMargins(0, appContext.resources.getDimension(R.dimen.margin_16).toInt(), 0, 0)
        }

        val firstImage = AppCompatImageView(appContext).apply {
            layoutParams = LayoutParams(MATCH_PARENT, 360).apply {
                setupMargins()
            }
            scaleType = CENTER_CROP
            tag = ImageUIElementArguments("", dualColumnImagesUIElement.leftImageId)
        }
        val firstText = AppCompatTextView(appContext).apply {
            layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
                setupMargins()
            }
            text = i18nData.getLocalizedText(dualColumnImagesUIElement.leftText)
            textSize = 14F
            typeface = create(SANS_SERIF_FONT_FAMILY, NORMAL)
            gravity = TOP
        }
        val secondImage = AppCompatImageView(appContext).apply {
            layoutParams = LayoutParams(MATCH_PARENT, 360).apply {
                setupMargins()
            }
            scaleType = CENTER_CROP
            tag = ImageUIElementArguments("", dualColumnImagesUIElement.rightImageId)
        }
        val secondText = AppCompatTextView(appContext).apply {
            layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
                setupMargins()
            }
            text = i18nData.getLocalizedText(dualColumnImagesUIElement.rightText)
            textSize = 14F
            typeface = create(SANS_SERIF_FONT_FAMILY, NORMAL)
            gravity = TOP
        }
        container.addViews(firstImage, firstText, secondImage, secondText)
        return container
    }

    fun buildDualColumnText(dualColumnTextUIElement: DualColumnTextUIElement, i18nData: I18nData): View {
        val container = LinearLayout(appContext).apply {
            setupDefaultMarginLayoutParams()
            orientation = VERTICAL
            tag = DefaultUIElementArguments(dualColumnTextUIElement.id)
        }

        fun LayoutParams.setupMargins() {
            setMargins(
                0,
                appContext.resources.getDimension(R.dimen.margin_16).toInt(),
                0,
                0
            )
        }

        val firstText = AppCompatTextView(appContext).apply {
            layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
                setupMargins()
            }
            setupDefaultText()
            text = i18nData.getLocalizedText(dualColumnTextUIElement.leftText)
        }
        val secondText = AppCompatTextView(appContext).apply {
            layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
                setupMargins()
            }
            setupDefaultText()
            text = i18nData.getLocalizedText(dualColumnTextUIElement.rightText)
        }
        container.addViews(firstText, secondText)
        return container
    }

    fun buildFacts(factsUIElement: FactsUIElement, i18nData: I18nData): View {
        val container = LinearLayout(appContext).apply {
            setupDefaultLayoutParams()
            orientation = VERTICAL
            tag = DefaultUIElementArguments(factsUIElement.id)
        }

        val views = arrayListOf<View>()
        val layoutInflater = LayoutInflater.from(appContext)

        factsUIElement.items.forEachIndexed { index, factItem ->
            views.add(layoutInflater.inflate(R.layout.item_fact, null).apply {
                imageView.tag = ImageUIElementArguments("", factItem.imageId)
                descriptionTextView.text = i18nData.getLocalizedText(factItem.text)
                setupDefaultMarginLayoutParams(MATCH_PARENT, 250)
            })
        }

        views.forEach {
            container.addView(it)
        }

        return container
    }

    fun buildTiledImage(tiledImagesUIElement: TiledImagesUIElement, i18nData: I18nData): View {
        val container = LinearLayout(appContext).apply {
            setupDefaultMarginLayoutParams()
            orientation = VERTICAL
            tag = DefaultUIElementArguments(tiledImagesUIElement.id)
        }
        val tabLayout = appContext.createTabLayout(tiledImagesUIElement.items.size)
        val viewPagerImage = ViewPager2(appContext).apply {
            layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT)
            adapter = TiledImageAdapter(tiledImagesUIElement.items.map {
                it.copy(
                    _header = i18nData.getLocalizedText(it.header),
                    _text = i18nData.getLocalizedText(it.text)
                )
            }, Triple(tiledImagesUIElement.bold, tiledImagesUIElement.italic, tiledImagesUIElement.underline))
        }
        container.apply {
            addView(viewPagerImage)
            addView(tabLayout)
        }
        appContext.bindViewPagerWithTabLayout(tabLayout, viewPagerImage)
        return container
    }

    fun buildButton(buttonUIElement: ButtonUIElement, i18nData: I18nData): View {
        val container = LinearLayout(appContext).apply {
            setupDefaultMarginLayoutParams()
            orientation = VERTICAL
            tag = DefaultUIElementArguments(buttonUIElement.id)
        }
        container.addView(RoundedCornersButton(ContextThemeWrapper(appContext, R.style.CommonRedButtonStyle)).apply {
            buttonUIElement.also {
                text = i18nData.getLocalizedText(it.text)
                setColor(appContext.parseColor(it.btnColor))
                setTextColor(appContext.parseColor(it.textColor))
                tag = ClickableUIElementArguments("", buttonUIElement.link)
            }
        })
        return container
    }

    fun buildDisclaimer(disclaimerUiElement: DisclaimerUiElement, i18nData: I18nData): View {
        val container = RelativeLayout(appContext).apply {
            layoutParams = RelativeLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT).also {
                it.setMargins(
                    0,
                    context.resources.getDimension(R.dimen.default_top_bottom_padding).toInt(),
                    0,
                    0
                )
            }
            tag = DefaultUIElementArguments(disclaimerUiElement.id)
            val padding = context.resources.getDimensionPixelOffset(R.dimen.margin_16)
            setPadding(padding, padding, padding, padding)
            setBackgroundColor(Color.parseColor(disclaimerUiElement.background))
        }

        val icon = AppCompatImageView(appContext).apply {
            val size = resources.getDimensionPixelOffset(R.dimen.iconDisclaimerSize)
            layoutParams = RelativeLayout.LayoutParams(size, size).apply {
                setMargins(0, 0, resources.getDimensionPixelOffset(R.dimen.margin_12), 0)
            }
            id = View.generateViewId()
            scaleType = CENTER_CROP
            tag = ImageUIElementArguments("", disclaimerUiElement.iconUrl, false)
        }

        val description = AppCompatTextView(appContext).apply {
            layoutParams = RelativeLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
            id = View.generateViewId()
            text = i18nData.getLocalizedText(disclaimerUiElement.text)
            setTextColor(ContextCompat.getColor(context, R.color.white80))
            setDipTextSize(R.dimen.textSize10)
            typeface = create(getFont(context, R.font.roboto_light), NORMAL)
        }

        container.addViews(
            icon.apply { (layoutParams as? RelativeLayout.LayoutParams)?.addRule(RelativeLayout.ALIGN_PARENT_TOP) },
            description.apply {
                (layoutParams as? RelativeLayout.LayoutParams)?.addRule(RelativeLayout.RIGHT_OF, icon.id)
            }
        )

        return container
    }

    fun buildLinkedText(linkedTextUIElement: LinkedTextUIElement): View {
        val container = LinearLayout(appContext)

        if (Locale.getDefault().getUserLanguage() != linkedTextUIElement.language)
            return container

        container.apply {
            setupDefaultMarginLayoutParams()
            orientation = VERTICAL
            tag = DefaultUIElementArguments(linkedTextUIElement.id)
        }

        container.addView(AppCompatTextView(appContext).apply {
            linkedTextUIElement.also {
                setupTextView(TextUIElement(
                    _color = it.color,
                    _fontSize = it.fontSize,
                    _justify = it.justify,
                    _bold = it.bold,
                    _italic = it.italic,
                    _underline = it.underline
                ))
                text = linkedTextUIElement.text
                tag = SpannableUIElementArguments("", it.wordsLink)
            }
        })
        return container
    }

    fun buildHeader(headerUIElement: HeaderUIElement, i18nData: I18nData): View {
        val container = LinearLayout(appContext).apply {
            setupDefaultMarginLayoutParams()
            orientation = VERTICAL
            tag = DefaultUIElementArguments(headerUIElement.id)
        }
        val preHeader = AppCompatTextView(appContext).apply {
            setupDefaultLayoutParams()
            setTextAppearance(R.style.SubTitle)
            compoundDrawablePadding = 10
            text = i18nData.getLocalizedText(headerUIElement.preHeader)
            setTextColor(ContextCompat.getColor(appContext, R.color.darkBlue))
            typeface = create(getFont(context, R.font.barlow_regular), NORMAL)
            setDipTextSize(R.dimen.preHeaderSize)
            setCompoundDrawablesWithIntrinsicBounds(GradientDrawable().also {
                it.setSize(40, 6)
                it.shape = RECTANGLE
                it.cornerRadius = resources.getDimension(R.dimen.cornerRadius)
                it.setColor(ContextCompat.getColor(context, R.color.lightRed))
            }, null, null, null)
            isAllCaps = true
        }

        if (preHeader.text.isNotEmpty())
            container.addView(preHeader)

        container.addView(AppCompatTextView(appContext).apply {
            createHeaderView(headerUIElement.headingSize)
            text = i18nData.getLocalizedText(headerUIElement.header)
        })
        return container
    }

    fun buildDepartment(departmentUIElement: DepartmentUIElement): View {
        return RecyclerView(appContext).apply {
            setupDefaultMarginLayoutParams()
            overScrollMode = OVER_SCROLL_NEVER
            layoutManager = SafeLinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            tag = DefaultUIElementArguments(departmentUIElement.id)
            adapter = DepartmentAdapter(departmentUIElement.items)
        }
    }

    fun buildImageLink(imageLinkUIElement: ImageLinkUIElement, i18nData: I18nData): View {
        if (imageLinkUIElement.items.isEmpty())
            return View(appContext)

        val container = LinearLayout(appContext).apply {
            setupDefaultMarginLayoutParams()
            orientation = VERTICAL
            tag = DefaultUIElementArguments(imageLinkUIElement.id)
        }
        val tabLayout = appContext.createTabLayout(imageLinkUIElement.items.size)
        val viewPagerImage = appContext.createViewPager(imageLinkUIElement.items)
        container.apply {
            addView(viewPagerImage)
            addView(tabLayout)
        }
        appContext.bindViewPagerWithTabLayout(tabLayout, viewPagerImage)

        if (!imageLinkUIElement.isDescriptionAdded)
            return container
        val viewPagerText = appContext.createViewPagerText(imageLinkUIElement.items.map {
            i18nData.getLocalizedText(it.description)
        })
        container.addView(viewPagerText)
        appContext.bindViewPagerWithTabLayout(tabLayout, viewPagerText)
        return container
    }

    fun buildGroupUIElementView(group: Group): View {
        return LinearLayout(appContext).apply {
            setupDefaultLayoutParams()
            orientation = VERTICAL
            tag = GroupUIElementArguments(group.id, group.idList)
        }
    }

    fun buildCharts(chartItems: ChartItems, i18nData: I18nData): View {
        val container = LinearLayout(appContext).apply {
            setupDefaultMarginLayoutParams()
            tag = DefaultUIElementArguments(chartItems.id)
            orientation = VERTICAL
        }

        chartItems.items.forEach {
            container.addView(AppCompatTextView(appContext).apply {
                layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
                    setMargins(0,
                        0,
                        0,
                        context.resources.getDimensionPixelOffset(R.dimen.margin_10))
                }
                setTextAppearance(R.style.HeadingH2)
                gravity = CENTER
                text = i18nData.getLocalizedText(it.title)
            })
            container.addView(when (it.chartType) {
                BAR -> appContext.buildBarChart(it)
                LINE -> appContext.buildLineChart(it)
                DOUGHNUT, HALF_DOUGHNUT -> appContext.buildDoughnutChart(it)
                else -> appContext.buildBarChart(it)
            })
            container.addView(AppCompatTextView(appContext).apply {
                layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
                    setMargins(0, context.resources.getDimensionPixelOffset(R.dimen.margin_10), 0, 0)
                }
                text = i18nData.getLocalizedText(it.text)
                setTextAppearance(R.style.Body14RobotoLight)
            })
        }

        return container
    }
}
