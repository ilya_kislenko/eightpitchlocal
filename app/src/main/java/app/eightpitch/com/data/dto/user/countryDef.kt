package app.eightpitch.com.data.dto.user

import android.content.Context
import androidx.annotation.StringRes
import app.eightpitch.com.R

enum class Country(@StringRes val displayValue: Int) {

    AT(R.string.austria_country_display_value),
    BE(R.string.belgium_country_display_value),
    BG(R.string.bulgaria_country_display_value),
    HR(R.string.croatia_country_display_value),
    CY(R.string.cyprus_country_display_value),
    CZ(R.string.czech_republic_country_display_value),
    DK(R.string.denmark_country_display_value),
    EE(R.string.estonia_country_display_value),
    FI(R.string.finland_country_display_value),
    FR(R.string.france_country_display_value),
    DE(R.string.germany_country_display_value),
    GR(R.string.greece_country_display_value),
    HU(R.string.hungary_country_display_value),
    IS(R.string.iceland_country_display_value),
    IE(R.string.ireland_country_display_value),
    IT(R.string.italy_country_display_value),
    LV(R.string.latvia_country_display_value),
    LI(R.string.liechtenstein_country_display_value),
    LT(R.string.lithuania_country_display_value),
    LU(R.string.luxembourg_country_display_value),
    MT(R.string.malta_country_display_value),
    NL(R.string.netherlands_country_display_value),
    NO(R.string.norway_country_display_value),
    PL(R.string.poland_country_display_value),
    PT(R.string.portugal_country_display_value),
    RO(R.string.romania_country_display_value),
    SK(R.string.slovakia_country_display_value),
    SI(R.string.slovenia_country_display_value),
    ES(R.string.spain_country_display_value),
    SE(R.string.sweden_country_display_value),
    CH(R.string.switzerland_country_display_value);

    override fun toString(): String {
        return this.name
    }

    companion object {

        fun toDisplayValue(context: Context, value: String): String {
            return values().find { it.name.equals(value, true) }?.displayValue?.let { context.getString(it) } ?: ""
        }

        fun fromDisplayValue(context: Context, displayValue: String): Country {
            return values().find { context.getString(it.displayValue) == displayValue } ?: AT
        }
    }
}

enum class CountryCode(val displayValue: String, val phoneCode: String) {
    DE("\uD83C\uDDE9\uD83C\uDDEA +49", "49"),
    AT("\uD83C\uDDE6\uD83C\uDDF9 +43", "43"),
    BE("\uD83C\uDDE7\uD83C\uDDEA +32", "32"),
    BG("\uD83C\uDDE7\uD83C\uDDEC +359", "359"),
    HR("\uD83C\uDDED\uD83C\uDDF7 +385", "385"),
    CY("\uD83C\uDDE8\uD83C\uDDFE +357", "357"),
    CZ("\uD83C\uDDE8\uD83C\uDDFF +420", "420"),
    DK("\uD83C\uDDE9\uD83C\uDDF0 +45", "45"),
    EE("\uD83C\uDDEA\uD83C\uDDEA +372", "372"),
    FI("\uD83C\uDDEB\uD83C\uDDEE +358", "358"),
    FR("\uD83C\uDDEB\uD83C\uDDF7 +33", "33"),
    GR("\uD83C\uDDEC\uD83C\uDDF7 +30", "30"),
    HU("\uD83C\uDDED\uD83C\uDDFA +36", "36"),
    IS("\uD83C\uDDEE\uD83C\uDDF8 +354", "354"),
    IE("\uD83C\uDDEE\uD83C\uDDEA +353", "353"),
    IT("\uD83C\uDDEE\uD83C\uDDF9 +39", "39"),
    LV("\uD83C\uDDF1\uD83C\uDDFB +371", "371"),
    LI("\uD83C\uDDF1\uD83C\uDDEE +423", "423"),
    LT("\uD83C\uDDF1\uD83C\uDDF9 +370", "370"),
    LU("\uD83C\uDDF1\uD83C\uDDFA +352", "352"),
    MT("\uD83C\uDDF2\uD83C\uDDF9 +356", "356"),
    NL("\uD83C\uDDF3\uD83C\uDDF1 +31", "31"),
    NO("\uD83C\uDDF3\uD83C\uDDF4 +47", "47"),
    PL("\uD83C\uDDF5\uD83C\uDDF1 +48", "48"),
    PT("\uD83C\uDDF5\uD83C\uDDF9 +351", "351"),
    RO("\uD83C\uDDF7\uD83C\uDDF4 +40", "40"),
    SK("\uD83C\uDDF8\uD83C\uDDF0 +421", "421"),
    SI("\uD83C\uDDF8\uD83C\uDDEE +386", "386"),
    ES("\uD83C\uDDEA\uD83C\uDDF8 +34", "34"),
    SE("\uD83C\uDDF8\uD83C\uDDEA +46", "46"),
    CH("\uD83C\uDDE8\uD83C\uDDED +41", "41");

    override fun toString(): String {
        return this.name
    }

    companion object {

        fun displayValueToPhoneCode(value: String): String {
            return values().find { it.displayValue == value }?.phoneCode ?: DE.phoneCode
        }

        fun getPhoneCodes(): List<String> {
            val arrayList = arrayListOf<String>()
            values().forEach {
                arrayList.add(it.displayValue)
            }
            return arrayList.toList()
        }
    }
}