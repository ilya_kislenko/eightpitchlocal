package app.eightpitch.com.data.dto.profiletransfers

import android.os.*
import app.eightpitch.com.extensions.*
import app.eightpitch.com.utils.Constants.DD_DOT_MM_DOT_YYYY_HH_MM_SS
import app.eightpitch.com.utils.Constants.DEFAULT_DATA_PATTERN
import app.eightpitch.com.utils.DateUtils.formatByPattern
import app.eightpitch.com.utils.DateUtils.parseByFormat
import com.google.gson.annotations.SerializedName
import java.math.BigDecimal
import java.math.BigDecimal.ZERO

data class TransferDetails(
    @SerializedName("amount")
    val _amount: BigDecimal? = null,
    @SerializedName("companyAddress")
    val _companyAddress: String? = null,
    @SerializedName("companyName")
    val _companyName: String? = null,
    @SerializedName("fromUser")
    val _fromUser: TransferUserInfo? = null,
    @SerializedName("isin")
    val _isin: String? = null,
    @SerializedName("toUser")
    val _toUser: TransferUserInfo? = null,
    @SerializedName("shortCut")
    val _shortCut: String? = null,
    @SerializedName("transferDate")
    val _transferDate: String? = null,
) : Parcelable {

    val amount
        get() = _amount.orDefaultValue(ZERO)
    val companyAddress
        get() = _companyAddress.orDefaultValue("")
    val companyName
        get() = _companyName.orDefaultValue("")
    val fromUser
        get() = _fromUser.orDefaultValue(TransferUserInfo())
    val isin
        get() = _isin.orDefaultValue("")
    val toUser
        get() = _toUser.orDefaultValue(TransferUserInfo())
    val shortCut
        get() = _shortCut.orDefaultValue("")
    val transferDate
        get() = _transferDate.orDefaultValue("")

    val date
        get() = transferDate.parseByFormat(pattern = DEFAULT_DATA_PATTERN)
    val formattedDate
        get() = date.formatByPattern(pattern = DD_DOT_MM_DOT_YYYY_HH_MM_SS)

    val toUserFullName
        get() = toUser.run { "$prefixFull $firstName $lastName" }
    val fromUserFullName
        get() = fromUser.run { "$prefixFull $firstName $lastName" }

    constructor(parcel: Parcel) : this(
        parcel.readBigDecimal(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readParcelable(TransferUserInfo::class.java.classLoader) ?: TransferUserInfo(),
        parcel.readStringSafe(),
        parcel.readParcelable(TransferUserInfo::class.java.classLoader) ?: TransferUserInfo(),
        parcel.readStringSafe(),
        parcel.readStringSafe()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(companyAddress)
        parcel.writeString(companyName)
        parcel.writeParcelable(fromUser, flags)
        parcel.writeString(isin)
        parcel.writeParcelable(toUser, flags)
        parcel.writeString(shortCut)
        parcel.writeString(transferDate)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<TransferDetails> {
        override fun createFromParcel(parcel: Parcel): TransferDetails {
            return TransferDetails(parcel)
        }

        override fun newArray(size: Int): Array<TransferDetails?> {
            return arrayOfNulls(size)
        }
    }
}