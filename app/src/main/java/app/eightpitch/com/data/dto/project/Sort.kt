package app.eightpitch.com.data.dto.project

import com.google.gson.annotations.SerializedName

/**
 * Contains information about sorting the resulting list on the page
 *
 * @param empty - empty list
 * @param sorted - sorted list
 * @param unsorted - unsorted list
 */
class Sort(
    @SerializedName("empty")
    var empty: Boolean = true,
    @SerializedName("sorted")
    var sorted: Boolean = false,
    @SerializedName("unsorted")
    var unsorted: Boolean = true
)