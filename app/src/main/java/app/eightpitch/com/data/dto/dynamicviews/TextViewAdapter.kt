package app.eightpitch.com.data.dto.dynamicviews

import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.R
import app.eightpitch.com.extensions.inflateView

class TextViewAdapter(private val descriptions: List<String>) : RecyclerView.Adapter<TextViewAdapter.TextHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TextHolder =
        TextHolder(inflateView(parent, R.layout.item_text_view))

    override fun getItemCount(): Int = descriptions.size

    override fun onBindViewHolder(holder: TextHolder, position: Int) {
        val description = descriptions[position]
        holder.apply {
            textView.text = description
        }
    }

    class TextHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textView: AppCompatTextView = itemView.findViewById(R.id.textView)
    }
}