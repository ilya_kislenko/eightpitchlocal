package app.eightpitch.com.data.dto.projectdetails

import androidx.annotation.StringDef
import app.eightpitch.com.extensions.NO_TYPE
import com.google.gson.annotations.SerializedName

class TokenParametersDocumentInterest(
    @SerializedName("endOfTermDate")
    var endOfTermDate: String = "",
    @SerializedName("interestAmount")
    var interestAmount: Int = -1,
    @SerializedName("tokenParametersDocumentInterestType")
    @DocumentInterestType
    var tokenParametersDocumentInterestType: String = NO_TYPE
){
    companion object{

        const val PER_MONTH = "PER_MONTH"
        const val PER_QUARTER = "PER_QUARTER"
        const val PER_YEAR = "PER_YEAR"
        const val END_OF_TERM = "END_OF_TERM"

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(NO_TYPE, PER_MONTH, PER_QUARTER, PER_YEAR, END_OF_TERM)
        annotation class DocumentInterestType
    }
}