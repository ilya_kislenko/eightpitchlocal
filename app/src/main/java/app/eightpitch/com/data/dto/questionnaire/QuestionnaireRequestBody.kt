package app.eightpitch.com.data.dto.questionnaire

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.StringDef
import app.eightpitch.com.R

data class QuestionnaireRequestBody(
    @Answer val experience: String? = null,
    @Answer val experienceDuration: String? = null,
    @Answer val investingAmount: String? = null,
    @Answer val investingFrequency: String? = null,
    val knowledge: List<String>? = null,
    @Answer val professionalExperienceSource: String? = null,
    @Answer val virtualCurrenciesUsed: String? = null
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.createStringArrayList(),
        parcel.readString(),
        parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(experience)
        parcel.writeString(experienceDuration)
        parcel.writeString(investingAmount)
        parcel.writeString(investingFrequency)
        parcel.writeStringList(knowledge)
        parcel.writeString(professionalExperienceSource)
        parcel.writeString(virtualCurrenciesUsed)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<QuestionnaireRequestBody> {
        override fun createFromParcel(parcel: Parcel): QuestionnaireRequestBody {
            return QuestionnaireRequestBody(parcel)
        }

        override fun newArray(size: Int): Array<QuestionnaireRequestBody?> {
            return arrayOfNulls(size)
        }

        const val NOT_PROVIDED = "NOT_PROVIDED"

        const val TRUE = "TRUE"
        const val NULL = "NULL"
        const val LESS_THAN_1 = "LESS_THAN_1"
        const val FROM_1_TO_3 = "FROM_1_TO_3"
        const val MORE_THAN_3 = "MORE_THAN_3"
        const val LESS_THAN_5K = "LESS_THAN_5K"
        const val FROM_5K_TO_25K = "FROM_5K_TO_25K"
        const val MORE_THAN_25K = "MORE_THAN_25K"
        const val LESS_THAN_5 = "LESS_THAN_5"
        const val MORE_THAN_5 = "MORE_THAN_5"
        const val PROFESSION = "PROFESSION"
        const val STUDIES = "STUDIES"
        const val PROFESSION_STUDIES = "PROFESSION_STUDIES"
        const val LESS_THAN_50 = "LESS_THAN_50"
        const val MORE_THAN_50 = "MORE_THAN_50"

        @Retention(AnnotationRetention.RUNTIME)
        @StringDef(TRUE, NULL, NOT_PROVIDED, LESS_THAN_1, FROM_1_TO_3, MORE_THAN_3,
            LESS_THAN_5K, FROM_5K_TO_25K, MORE_THAN_25K, LESS_THAN_5, MORE_THAN_5,
            PROFESSION, STUDIES, PROFESSION_STUDIES, LESS_THAN_50, MORE_THAN_50)
        annotation class Answer
    }

    enum class Knowledge(val displayValue: Int) {
        TOKENIZED_SECURITIES(R.string.tokenized_securities),
        STOCKS(R.string.stocks),
        HEDGE_FUNDS(R.string.hedge_funds),
        WARRANTS(R.string.warrants),
        BONDS(R.string.bonds),
        CLOSED_END_MUTUAL_FUNDS(R.string.closed_end_mutual_funds),
        INVESTMENT_CERTIFICATES(R.string.investment_certificates);

        override fun toString(): String {
            return this.name
        }

        companion object {

            fun toDisplayValue(context: Context, value: String): String {
                return Knowledge.values().find { it.name == value }?.displayValue?.let { context.getString(it) }
                        ?: context.getString(R.string.tokenized_securities)
            }

            fun fromDisplayValue(context: Context?, displayValue: String): Knowledge {
                return Knowledge.values().find { context?.getString(it.displayValue) == displayValue } ?: Knowledge.TOKENIZED_SECURITIES
            }
        }
    }
}
