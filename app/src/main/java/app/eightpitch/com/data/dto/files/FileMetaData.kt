package app.eightpitch.com.data.dto.files

import android.net.Uri
import android.net.Uri.EMPTY
import android.os.*
import app.eightpitch.com.extensions.*

data class FileMetaData(
    val displayName: String = "",
    val fileSize: Int = -1,
    val fileUri: Uri = EMPTY,
    val attachmentTime: Long = -1,
) :
    Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readInt(),
        parcel.readParcelable<Uri>(Uri::class.java.classLoader!!) ?: EMPTY,
        parcel.readLong()
    )

    override fun writeToParcel(parcel: Parcel?, flags: Int) {
        parcel?.apply {
            writeString(displayName)
            writeInt(fileSize)
            writeParcelable(fileUri, flags)
            writeLong(attachmentTime)
        }
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<FileMetaData> {
        override fun createFromParcel(parcel: Parcel): FileMetaData {
            return FileMetaData(parcel)
        }

        override fun newArray(size: Int): Array<FileMetaData?> {
            return arrayOfNulls(size)
        }
    }

    internal fun fileSizeToText() = when (fileSize) {
        in 0 until 1000 -> "$fileSize bytes"
        in 1000..1_000_000 -> "${formatFileSize(fileSize / 1_000.0F)} kB"
        else -> "${formatFileSize(fileSize / 1_000_000.0F)} MB"
    }

    private fun formatFileSize(size: Float) = String.format("%.2f", size)
}