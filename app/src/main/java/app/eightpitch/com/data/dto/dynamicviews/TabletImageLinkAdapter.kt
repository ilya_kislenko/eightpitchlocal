package app.eightpitch.com.data.dto.dynamicviews

import android.view.*
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.view.isVisible
import androidx.lifecycle.*
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.R
import app.eightpitch.com.extensions.inflateView

class TabletImageLinkAdapter(
    private val items: List<Pair<ImageLink, ImageLink>>
) :
    RecyclerView.Adapter<TabletImageLinkAdapter.TiledImageHolder>(), ParentLayoutHolder {

    private val result = MutableLiveData<ViewGroup>()
    internal fun getResult(): LiveData<ViewGroup> = result

    private var parentViewGroup: ViewGroup? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TiledImageHolder {
        val view = inflateView(parent, R.layout.item_tablet_image_link)
        parentViewGroup = view as? ViewGroup
        return TiledImageHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: TiledImageHolder, position: Int) {
        val item = items[position]
        holder.apply {
            val first = item.first
            firstBackgroundImage.tag = ImageUIElementArguments("", first.imageUrl)
            firstDescriptionText.text = first.description
            if (first.link.isEmpty())
                firstLinkView.isVisible = false
            else {
                firstLinkView.run {
                    tag = ClickableUIElementArguments("", first.link)
                    isVisible = true
                }
            }

            val second = item.second
            secondBackgroundImage.tag = ImageUIElementArguments("", second.imageUrl)
            secondDescriptionText.text = second.description
            if (second.link.isEmpty())
                secondLinkView.isVisible = false
            else {
                secondLinkView.run {
                    tag = ClickableUIElementArguments("", second.link)
                    isVisible = true
                }
            }

            parentViewGroup?.let {
                result.value = it
            }
        }
    }

    class TiledImageHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val firstBackgroundImage: AppCompatImageView = itemView.findViewById(R.id.firstBackgroundImage)
        val firstLinkView: AppCompatImageView = itemView.findViewById(R.id.firstLinkView)
        val firstDescriptionText: AppCompatTextView = itemView.findViewById(R.id.firstDescriptionText)
        val secondBackgroundImage: AppCompatImageView = itemView.findViewById(R.id.secondBackgroundImage)
        val secondLinkView: AppCompatImageView = itemView.findViewById(R.id.secondLinkView)
        val secondDescriptionText: AppCompatTextView = itemView.findViewById(R.id.secondDescriptionText)
    }

    override fun getParentLayout(): ViewGroup? = parentViewGroup
}