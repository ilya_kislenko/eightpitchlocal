package app.eightpitch.com.data.dto.signup

import android.os.Parcel
import android.os.Parcelable
import app.eightpitch.com.data.dto.user.User.CREATOR.RoleType
import app.eightpitch.com.extensions.NO_TYPE
import app.eightpitch.com.extensions.readStringSafe
import com.google.gson.annotations.SerializedName

open class CommonInvestorRequestBody(
    var email: String = "",
    var deviceId: String = "",
    var password: String = "",
    var lastName: String = "",
    var firstName: String = "",
    @SerializedName("phone")
    var phoneNumber: String = "",
    var interactionId: String = "",
    var commercialRegisterNumber: String = "",
    @SerializedName("subscribeToEmails")
    var isSubscribedToEmail: Boolean = false,
    @RoleType var group: String = NO_TYPE,
    var prefix: String = NO_TYPE
) : Parcelable {

    open fun copy(
        email: String = this.email,
        deviceId: String = this.deviceId,
        password: String = this.password,
        lastName: String = this.lastName,
        firstName: String = this.firstName,
        phoneNumber: String = this.phoneNumber,
        interactionId: String = this.interactionId,
        commercialRegisterNumber: String = this.commercialRegisterNumber,
        isSubscribedToEmail: Boolean = this.isSubscribedToEmail,
        @RoleType group: String = this.group,
        prefix: String = this.prefix
    ) = CommonInvestorRequestBody(
        email, deviceId, password, lastName, firstName, phoneNumber,
        interactionId, commercialRegisterNumber, isSubscribedToEmail, group, prefix
    )

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readByte() != 0.toByte(),
        parcel.readStringSafe(),
        parcel.readStringSafe())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(email)
        parcel.writeString(deviceId)
        parcel.writeString(password)
        parcel.writeString(lastName)
        parcel.writeString(firstName)
        parcel.writeString(phoneNumber)
        parcel.writeString(interactionId)
        parcel.writeString(commercialRegisterNumber)
        parcel.writeByte(if (isSubscribedToEmail) 1 else 0)
        parcel.writeString(group)
        parcel.writeString(prefix)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<CommonInvestorRequestBody> {
        override fun createFromParcel(parcel: Parcel): CommonInvestorRequestBody {
            return CommonInvestorRequestBody(parcel)
        }

        override fun newArray(size: Int): Array<CommonInvestorRequestBody?> {
            return arrayOfNulls(size)
        }
    }
}