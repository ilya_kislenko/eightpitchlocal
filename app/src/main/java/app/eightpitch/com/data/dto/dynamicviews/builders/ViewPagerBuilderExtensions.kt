package app.eightpitch.com.data.dto.dynamicviews.builders

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.viewpager2.widget.ViewPager2
import app.eightpitch.com.R
import app.eightpitch.com.data.dto.dynamicviews.ImageLink
import app.eightpitch.com.data.dto.dynamicviews.ImageLinkAdapter
import app.eightpitch.com.data.dto.dynamicviews.TextViewAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

fun Context.createTabLayout(itemsSize: Int): TabLayout {
    val tabLayout = TabLayout(this).apply {
        layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 30).apply {
            setMargins(
                0,
                context.resources.getDimension(R.dimen.margin_32).toInt(),
                0,
                context.resources.getDimension(R.dimen.margin_32).toInt()
            )
        }
        setSelectedTabIndicator(null)
        tabGravity = TabLayout.GRAVITY_CENTER
        tabMode = TabLayout.MODE_SCROLLABLE
        setSelectedTabIndicatorColor(ContextCompat.getColor(context, R.color.lightRed))
        tabRippleColor = ResourcesCompat.getColorStateList(resources, R.color.lightRed, theme)
    }
    repeat(itemsSize) {
        tabLayout.apply { addTab(newTab()) }
    }
    return tabLayout
}

fun Context.createViewPager(items: List<ImageLink>): ViewPager2 {
    return ViewPager2(this).apply {
        layoutParams =
            LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        adapter = ImageLinkAdapter(items)
    }
}

fun Context.createViewPagerText(descriptions: List<String>): ViewPager2 {
    return ViewPager2(this).apply {
        layoutParams =
            LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        adapter = TextViewAdapter(descriptions)
    }
}

fun Context.bindViewPagerWithTabLayout(
    tabLayout: TabLayout,
    viewPager: ViewPager2
) {
    TabLayoutMediator(tabLayout, viewPager) { tab, position ->
        tab.customView = View(this).also {
            it.layoutParams = LinearLayout.LayoutParams(
                resources.getDimension(R.dimen.margin_54).toInt(),
                resources.getDimension(R.dimen.margin_2).toInt()
            )
            it.background = ContextCompat.getDrawable(this, R.drawable.tab_constructor_selector)
        }
    }.attach()
    viewPager.setCurrentItem(0, true)
}