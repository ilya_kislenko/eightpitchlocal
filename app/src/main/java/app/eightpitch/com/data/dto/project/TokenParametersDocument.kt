package app.eightpitch.com.data.dto.project

import android.os.*
import app.eightpitch.com.extensions.*
import app.eightpitch.com.utils.Constants.DEFAULT_DATA_PATTERN
import app.eightpitch.com.utils.DateUtils.parseByFormat
import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

/**
 * Information about the beginning and end of the project,
 * the amount of required investments, the minimum investment rate
 *
 * @param dsoProjectFinishDate - Project end date
 * @param dsoProjectStartDate - Project start date
 * @param hardCap - Percentage of investment for starting a project
 * @param minimumInvestmentAmount - The minimum number of tokens to invest
 * @param nominalValue - The amount to invest in the project in token
 */
class TokenParametersDocument(
    @SerializedName("dsoProjectFinishDate")
    var dsoProjectFinishDate: String = "",
    @SerializedName("dsoProjectStartDate")
    var dsoProjectStartDate: String = "",
    @SerializedName("hardCap")
    var hardCap: Int = 0,
    @SerializedName("minimumInvestmentAmount")
    var minimumInvestmentAmount: BigDecimal = BigDecimal.ZERO,
    @SerializedName("nominalValue")
    var nominalValue: BigDecimal = BigDecimal.ZERO
) : Parcelable {

    val projectFinishDate
        get() = dsoProjectFinishDate.parseByFormat(pattern = DEFAULT_DATA_PATTERN)

    val projectStartDate
        get() = dsoProjectStartDate.parseByFormat(pattern = DEFAULT_DATA_PATTERN)

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readInt(),
        parcel.readBigDecimal(),
        parcel.readBigDecimal())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(dsoProjectFinishDate)
        parcel.writeString(dsoProjectStartDate)
        parcel.writeInt(hardCap)
        parcel.writeSerializable(minimumInvestmentAmount)
        parcel.writeSerializable(nominalValue)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<TokenParametersDocument> {
        override fun createFromParcel(parcel: Parcel): TokenParametersDocument {
            return TokenParametersDocument(parcel)
        }

        override fun newArray(size: Int): Array<TokenParametersDocument?> {
            return arrayOfNulls(size)
        }
    }
}