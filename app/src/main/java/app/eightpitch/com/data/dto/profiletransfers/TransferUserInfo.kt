package app.eightpitch.com.data.dto.profiletransfers

import android.os.*
import app.eightpitch.com.extensions.*
import com.google.gson.annotations.SerializedName

data class TransferUserInfo(
    @SerializedName("firstName")
    private val _firstName: String? = null,
    @SerializedName("lastName")
    private val _lastName: String? = null,
    @SerializedName("prefixFull")
    private val _prefixFull: String? = null,
) : Parcelable {

    val firstName
        get() = _firstName.orDefaultValue("")
    val lastName
        get() = _lastName.orDefaultValue("")
    val prefixFull
        get() = _prefixFull.orDefaultValue("")

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(firstName)
        parcel.writeString(lastName)
        parcel.writeString(prefixFull)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<TransferUserInfo> {
        override fun createFromParcel(parcel: Parcel): TransferUserInfo {
            return TransferUserInfo(parcel)
        }

        override fun newArray(size: Int): Array<TransferUserInfo?> {
            return arrayOfNulls(size)
        }
    }
}