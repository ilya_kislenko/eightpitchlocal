package app.eightpitch.com.data.dto.user

import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.StringDef
import app.eightpitch.com.extensions.NO_TYPE
import app.eightpitch.com.extensions.readStringSafe
import com.google.gson.annotations.SerializedName

/**
 * Information for creating dashboard slider
 */
data class LinkSliderItemResponseBody(
    @SerializedName("backgroundImageFileId")
    val backgroundImageFileId: String = "",
    @SerializedName("buttonText")
    val buttonText: String = "",
    @SerializedName("group")
    @DashboardGroup val group: String = NO_TYPE,
    @SerializedName("header")
    val header: String = "",
    @SerializedName("link")
    val link: String = "",
    @SerializedName("title")
    val title: String = ""
): Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(backgroundImageFileId)
        parcel.writeString(buttonText)
        parcel.writeString(group)
        parcel.writeString(header)
        parcel.writeString(link)
        parcel.writeString(title)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<LinkSliderItemResponseBody> {
        override fun createFromParcel(parcel: Parcel): LinkSliderItemResponseBody {
            return LinkSliderItemResponseBody(parcel)
        }

        override fun newArray(size: Int): Array<LinkSliderItemResponseBody?> {
            return arrayOfNulls(size)
        }
    }

}

const val TOP_HOME_SLIDER = "topHomeSlider"
const val TOP_INITIATOR_SLIDER = "topInitiatorSlider"
const val TOP_INVESTOR_SLIDER = "topInvestorSlider"

@Retention(AnnotationRetention.SOURCE)
@StringDef(NO_TYPE, TOP_HOME_SLIDER, TOP_INITIATOR_SLIDER, TOP_INVESTOR_SLIDER)
annotation class DashboardGroup