package app.eightpitch.com.data.dto.sms

import androidx.annotation.StringDef
import app.eightpitch.com.extensions.NO_TYPE
import app.eightpitch.com.extensions.orDefaultValue
import com.google.gson.annotations.SerializedName

class EmailPhoneValidationResponse(
    @SerializedName("result")
    private val _result: String?
) {
    @SmsVerificationStatus val result: String
        get() = _result.orDefaultValue(NO_TYPE)

    companion object {

        const val EXPIRED = "EXPIRED"
        const val INVALID = "INVALID"
        const val VALID = "VALID"

        @StringDef(EXPIRED, INVALID, VALID, NO_TYPE)
        annotation class SmsVerificationStatus
    }
}