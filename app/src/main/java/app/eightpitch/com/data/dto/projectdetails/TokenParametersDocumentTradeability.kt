package app.eightpitch.com.data.dto.projectdetails

import androidx.annotation.StringDef
import app.eightpitch.com.extensions.NO_TYPE
import com.google.gson.annotations.SerializedName


data class TokenParametersDocumentTradeability(
    @SerializedName("requestDistributorApproval")
    @ApprovalStatus
    var requestDistributorApproval: String = NO_TYPE,
    @SerializedName("requestDssxApproval")
    @ApprovalStatus
    var requestDssxApproval: String = NO_TYPE,
    @SerializedName("requestIssuerApproval")
    @ApprovalStatus
    var requestIssuerApproval: String = NO_TYPE,
    @SerializedName("tokenTradeableFromDate")
    var tokenTradeableFromDate: String = "",
    @SerializedName("tokenTradeableToDate")
    var tokenTradeableToDate: String = ""
){
    companion object{

        const val ONCE = "ONCE"
        const val EVERY_OPERATION = "EVERY_OPERATION"

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(
            NO_TYPE, ONCE, EVERY_OPERATION)
        annotation class ApprovalStatus
    }
}