package app.eightpitch.com.data.dto.webid

import android.os.Parcel
import android.os.Parcelable
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.extensions.readStringSafe
import com.google.gson.annotations.SerializedName

data class WebIdPersonalInfo(
    val accountOwner: String = "",
    @SerializedName("iban")
    val IBAN: String = "",
    val nationality: String = "",
    val placeOfBirth: String = "",
    @SerializedName("politicallyExposedPerson")
    val isPoliticallyExposedPerson: Boolean = false,
    @SerializedName("usTaxLiability")
    val isUsTaxLiability: Boolean = false
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(accountOwner)
        parcel.writeString(IBAN)
        parcel.writeString(nationality)
        parcel.writeString(placeOfBirth)
        parcel.writeByte(if (isPoliticallyExposedPerson) 1 else 0)
        parcel.writeByte(if (isUsTaxLiability) 1 else 0)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<WebIdPersonalInfo> {
        override fun createFromParcel(parcel: Parcel): WebIdPersonalInfo {
            return WebIdPersonalInfo(parcel)
        }

        override fun newArray(size: Int): Array<WebIdPersonalInfo?> {
            return arrayOfNulls(size)
        }
    }
}