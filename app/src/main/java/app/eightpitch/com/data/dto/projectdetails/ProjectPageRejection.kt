package app.eightpitch.com.data.dto.projectdetails

import com.google.gson.annotations.SerializedName


data class ProjectPageRejection(
    @SerializedName("comment")
    var comment: String = "",
    @SerializedName("createDate")
    var createDate: String = "",
    @SerializedName("id")
    var id: Long = -1
)