package app.eightpitch.com.data.dto.project

import com.google.gson.annotations.SerializedName

/**
 * Backend representation of initiator's projects
 */
data class InitiatorProjects(
    @SerializedName("content")
    val projects: List<InitiatorProjectDetails> = listOf(),
    val first: Boolean = false,
    val hasContent: Boolean = false,
    val hasNext: Boolean = false,
    val hasPrevious: Boolean = false,
    val last: Boolean = false,
    //nextPage
    val number: Int = 0,
    val numberOfElements: Int = 0,
    //pageable
    //previousPageable
    val size: Int = 0,
    //sort
    val totalElements: Int = 0,
    val totalPages:Int = 0
)