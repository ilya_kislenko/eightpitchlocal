package app.eightpitch.com.data.dto.dynamicviews

import android.os.*
import app.eightpitch.com.extensions.*
import com.google.gson.annotations.SerializedName

/**
 * List of view ids for changing order of views
 */
class RootViewsContainer(
    @SerializedName("nameTranslationCode")
    private val _nameTranslationCode: String? = "",
    @SerializedName("layoutRootIdList")
    private val _layoutRootIdList: List<String>? = listOf()
) : Parcelable {

    val layoutRootIdList: List<String>
        get() = _layoutRootIdList.orDefaultValue(listOf())

    val nameTranslationCode: String
        get() = _nameTranslationCode.orDefaultValue("")

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readListSafe(mutableListOf(), RootViewsContainer::class.java.classLoader!!)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(nameTranslationCode)
        parcel.writeStringList(layoutRootIdList)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<RootViewsContainer> {
        override fun createFromParcel(parcel: Parcel): RootViewsContainer {
            return RootViewsContainer(parcel)
        }

        override fun newArray(size: Int): Array<RootViewsContainer?> {
            return arrayOfNulls(size)
        }
    }
}