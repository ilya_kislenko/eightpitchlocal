package app.eightpitch.com.data.dto.signup

import androidx.annotation.StringDef
import app.eightpitch.com.data.dto.auth.TokenResponse
import app.eightpitch.com.extensions.NO_TYPE
import app.eightpitch.com.extensions.orDefaultValue
import com.google.gson.annotations.SerializedName

data class RegistrationResponseBody(
    @SerializedName("id")
    private val _id: String?,
    @SerializedName("status")
    @RegistrationStatus private val _status: String?,
    @SerializedName("tokenResponse")
    private val _tokenResponse: TokenResponse?
) {
    val id: String
        get() = _id.orDefaultValue("")
    @RegistrationStatus val status: String
        get() = _status.orDefaultValue("")
    val tokenResponse: TokenResponse
        get() = _tokenResponse.orDefaultValue(TokenResponse())

    companion object {

        const val SUCCESS = "SUCCESS"
        const val USER_ALREADY_EXIST = "USER_ALREADY_EXIST"

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(SUCCESS, USER_ALREADY_EXIST, NO_TYPE)
        annotation class RegistrationStatus
    }
}