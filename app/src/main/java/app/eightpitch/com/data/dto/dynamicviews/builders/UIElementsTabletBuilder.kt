package app.eightpitch.com.data.dto.dynamicviews.builders

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.Typeface.NORMAL
import android.graphics.drawable.GradientDrawable
import android.view.*
import android.view.View.TEXT_ALIGNMENT_CENTER
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.HorizontalScrollView
import android.widget.ImageView.ScaleType.CENTER_CROP
import android.widget.*
import androidx.appcompat.view.ContextThemeWrapper
import androidx.appcompat.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import app.eightpitch.com.R
import app.eightpitch.com.data.dto.dynamicviews.*
import app.eightpitch.com.data.dto.dynamicviews.language.I18nData
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.dynamicdetails.DepartmentAdapter
import app.eightpitch.com.ui.dynamicdetails.FAQAdapter
import app.eightpitch.com.utils.SafeLinearLayoutManager
import app.eightpitch.com.views.RoundedCornersButton
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.item_fact.view.*
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class UIElementsTabletBuilder @Inject constructor(private val appContext: Context) {

    fun buildTextView(textUIElement: TextUIElement, i18nData: I18nData): View {
        val container = LinearLayout(appContext).apply {
            orientation = LinearLayout.VERTICAL
            tag = DefaultUIElementArguments(textUIElement.id)
        }

        container.addView(buildHeader(
            HeaderUIElement(
                _header = textUIElement.header,
                _preHeader = textUIElement.preHeader,
                _headingSize = textUIElement.headingSizeString
            ), i18nData)
        )
        container.addView(AppCompatTextView(appContext).setupTextView(textUIElement).apply {
            text = i18nData.getLocalizedText(textUIElement.text)
        })
        return container
    }

    fun buildDividerView(dividerUIElement: DividerUIElement): View {
        return View(appContext).apply {
            layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, dividerUIElement.height.toInt())
            tag = DefaultUIElementArguments(dividerUIElement.id)
        }
    }

    fun buildFAQView(faqUiElement: FAQUIElement): View {
        val container = LinearLayout(appContext)

        if (Locale.getDefault().getUserLanguage() != faqUiElement.language)
            return container

        container.apply {
            setupDefaultMarginLayoutParams()
            orientation = LinearLayout.VERTICAL
            tag = DefaultUIElementArguments(faqUiElement.id)
        }

        val header = AppCompatTextView(appContext).apply {
            createHeaderView(gravity = TEXT_ALIGNMENT_CENTER)
            text = faqUiElement.header
        }

        container.run {
            addView(header)
            addView(RecyclerView(appContext).apply {
                layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT).also {
                    it.setMargins(0, resources.getDpBy(27f).toInt(), 0, 0)
                }
                overScrollMode = View.OVER_SCROLL_NEVER
                layoutManager = SafeLinearLayoutManager(context, RecyclerView.VERTICAL, false)
                adapter = FAQAdapter(faqUiElement.items)
            })
        }

        return container
    }

    fun buildIconsView(iconsUIElement: IconsUIElement): View {
        val array = ArrayList<View>()
        iconsUIElement.items.forEach { item ->
            val iconView = AppCompatImageView(appContext).apply {
                tag = IconsUIElementArguments("", item.link, item.imageId)
                val size = resources.getDimensionPixelOffset(R.dimen.iconSize)
                layoutParams = LinearLayout.LayoutParams(size, size).also {
                    it.setMargins(0, 0, resources.getDimensionPixelOffset(R.dimen.default_left_right_padding), 0)
                }
                scaleType = CENTER_CROP
            }
            array.add(iconView)
        }

        val container = LinearLayout(appContext).apply {
            setupDefaultMarginLayoutParams()
            gravity = Gravity.CENTER
            orientation = LinearLayout.HORIZONTAL
            tag = DefaultUIElementArguments(iconsUIElement.id)
        }
        array.forEach { container.addView(it) }
        return container
    }

    fun buildImageView(imageUIElement: ImageUIElement, i18nData: I18nData): View {
        val container = RelativeLayout(appContext).apply {
            layoutParams = RelativeLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT).also {
                it.setMargins(0, resources.getDimensionPixelOffset(R.dimen.margin_16), 0, 0)
            }
            tag = DefaultUIElementArguments(imageUIElement.id)
        }
        container.addView(AppCompatImageView(appContext).apply {
            tag = ImageUIElementArguments("", imageUIElement.imageId, false)
            layoutParams = RelativeLayout.LayoutParams(if (imageUIElement.fullWidth) MATCH_PARENT
            else WRAP_CONTENT, WRAP_CONTENT)
        })

        if (imageUIElement.useText) {
            container.addView(AppCompatTextView(appContext).apply {
                setupTextView(imageUIElement).also {
                    layoutParams = RelativeLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
                        addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
                        setPadding(
                            resources.getDimensionPixelOffset(R.dimen.default_left_right_padding),
                            0,
                            resources.getDimensionPixelOffset(R.dimen.default_left_right_padding),
                            resources.getDimensionPixelOffset(R.dimen.default_top_bottom_padding)
                        )
                    }
                    it.text = i18nData.getLocalizedText(imageUIElement.text)
                }
            })
        }

        return container
    }

    fun buildImageAndTextView(imageAndTextUIElement: ImageAndTextUIElement, i18nData: I18nData): View {
        val container = ConstraintLayout(appContext).apply {
            layoutParams = ConstraintLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT).also {
                it.setDefaultMargins(appContext)
            }
            tag = DefaultUIElementArguments(imageAndTextUIElement.id)
        }

        val header = AppCompatTextView(appContext).apply {
            createHeaderView()
            text = i18nData.getLocalizedText(imageAndTextUIElement.header)
            gravity = Gravity.CENTER
        }
        val image = AppCompatImageView(appContext).apply {
            setupDefaultMarginLayoutParams()
            scaleType = CENTER_CROP
            tag = ImageUIElementArguments(imageAndTextUIElement.id, imageAndTextUIElement.imageId, false)
        }
        val description = AppCompatTextView(appContext).apply {
            text = i18nData.getLocalizedText(imageAndTextUIElement.imageDescription)
            textSize = resources.getDimensionPixelOffset(R.dimen.textSize12).toFloat()
        }
        val text = AppCompatTextView(appContext).apply {
            text = i18nData.getLocalizedText(imageAndTextUIElement.text)
            textSize = resources.getDimensionPixelOffset(R.dimen.textSize14).toFloat()
        }

        val (viewsLeft, viewsRight) = if (imageAndTextUIElement.gravity == Gravity.END) {
            arrayOf<View>().plus(header).plus(text) to arrayOf<View>().plus(image).plus(description)
        } else {
            arrayOf<View>().plus(image).plus(description) to arrayOf<View>().plus(header).plus(text)
        }

        container.apply {
            buildVerticalDividedLayout(
                appContext,
                viewsLeft = viewsLeft,
                viewsRight = viewsRight
            )
        }
        return container
    }

    fun buildListView(listUIElement: ListUIElement, i18nData: I18nData): View {
        val views = ArrayList<View>()
        val headerView = AppCompatTextView(appContext).apply {
            createHeaderView()
            text = i18nData.getLocalizedText(listUIElement.header)
            gravity = ViewGroup.TEXT_ALIGNMENT_CENTER
        }
        views.add(headerView)
        listUIElement.items.forEach { item ->
            val textView = AppCompatTextView(appContext).apply {
                layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
                    setMargins(0, resources.getDimension(R.dimen.margin_16).toInt(), 0, 0)
                    setPadding(resources.getDimension(R.dimen.margin_8).toInt(), 0, 0, 0)
                }
                setCompoundDrawablesWithIntrinsicBounds(GradientDrawable().also {
                    val size = resources.getDimension(R.dimen.margin_8).toInt()
                    it.setSize(size, size)
                    it.shape = GradientDrawable.OVAL
                    it.setColor(ContextCompat.getColor(context, R.color.darkBlue))
                }, null, null, null)
                compoundDrawablePadding = resources.getDimension(R.dimen.margin_8).toInt()
                text = i18nData.getLocalizedText(item.text)
            }
            views.add(textView)
        }
        val container = LinearLayout(appContext).apply {
            setupDefaultMarginLayoutParams()
            orientation = LinearLayout.VERTICAL
            tag = DefaultUIElementArguments(listUIElement.id)
        }
        views.forEach { container.addView(it) }

        return container
    }

    fun buildImageLink(imageLinkUIElement: ImageLinkUIElement, i18nData: I18nData): View {
        if (imageLinkUIElement.items.isEmpty())
            return View(appContext)

        val container = LinearLayout(appContext).apply {
            setupDefaultMarginLayoutParams()
            orientation = LinearLayout.VERTICAL
            tag = DefaultUIElementArguments(imageLinkUIElement.id)
        }
        val tabLayout = appContext.createTabLayout(imageLinkUIElement.items.size)
        val viewPagerImage = ViewPager2(appContext).apply {
            layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
            val pairs = imageLinkUIElement.items.run {
                val listPair = arrayListOf<Pair<ImageLink, ImageLink>>()
                for (i in 0 until size step 2) {
                    val second = if (i + 1 == size) ImageLink() else get(i + 1).copy(

                    )
                    listPair.add(get(i).copy(_description = i18nData.getLocalizedText(get(i).description))
                            to second.copy(_description = i18nData.getLocalizedText(second.description)))
                }
                listPair
            }
            adapter = TabletImageLinkAdapter(pairs)
        }
        container.addView(viewPagerImage)

        if (imageLinkUIElement.items.size > 2) {
            container.addView(tabLayout)
            appContext.bindViewPagerWithTabLayout(tabLayout.apply { tabMode = TabLayout.MODE_FIXED }, viewPagerImage)
        }

        return container
    }

    fun buildHeader(headerUIElement: HeaderUIElement, i18nData: I18nData): View {
        val container = LinearLayout(appContext).apply {
            setupDefaultMarginLayoutParams()
            orientation = LinearLayout.VERTICAL
            tag = DefaultUIElementArguments(headerUIElement.id)
        }
        val preHeader = AppCompatTextView(appContext).apply {
            setupDefaultLayoutParams()
            setTextAppearance(R.style.SubTitle)
            compoundDrawablePadding = 10
            text = i18nData.getLocalizedText(headerUIElement.preHeader)
            setTextColor(ContextCompat.getColor(appContext, R.color.darkBlue))
            typeface = Typeface.create(ResourcesCompat.getFont(context, R.font.barlow_regular), NORMAL)
            setDipTextSize(R.dimen.preHeaderSize)
            setCompoundDrawablesWithIntrinsicBounds(GradientDrawable().also {
                it.setSize(40, 6)
                it.shape = GradientDrawable.RECTANGLE
                it.cornerRadius = resources.getDimension(R.dimen.cornerRadius)
                it.setColor(ContextCompat.getColor(context, R.color.lightRed))
            }, null, null, null)
            isAllCaps = true
        }

        if (preHeader.text.isNotEmpty())
            container.addView(preHeader)

        container.addView(AppCompatTextView(appContext).apply {
            createHeaderView(headerUIElement.headingSize)
            text = i18nData.getLocalizedText(headerUIElement.header)
        })
        return container
    }

    fun buildTableView(tableUIElement: TableUIElement, i18nData: I18nData): View {

        val tableView = TableLayout(appContext).apply {

            layoutParams = TableLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
                setPadding(0, 0, 4, 4)
                setBackgroundColor(ContextCompat.getColor(appContext, R.color.gray20))
            }
            tableUIElement.run {

                val columns = colsNumber.toInt()
                val rowsCount = rowsNumber.toInt()

                for (i in 0 until rowsCount) {

                    val row = TableRow(context).apply {
                        layoutParams = TableRow.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
                        setColumnStretchable(i, true)
                    }

                    for (j in 0 until columns) {
                        val cell = when {
                            i != 0 -> appContext.createTableCell(i)
                            else -> appContext.createTableHeaderCell(j == 0, headerColor)
                        }
                        row.addView(cell.apply {
                            layoutParams = TableRow.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
                            text = i18nData.getLocalizedText(rows[i][j].text)
                            gravity = Gravity.START
                            setPadding(30, 30, 30, 30)
                        })
                    }

                    addView(row, i)
                }
            }
        }

        return HorizontalScrollView(appContext).apply {
            setupDefaultMarginLayoutParams()
            isFillViewport = true
            isHorizontalScrollBarEnabled = false
            tag = DefaultUIElementArguments(tableUIElement.id)
            addView(tableView)
        }
    }

    //Check
    fun buildVimeoView(vimeoUIElement: VimeoUIElement, i18nData: I18nData): View {

        val videoView = AppCompatImageView(appContext).apply {
            tag = VimeoUIElementArguments("", vimeoUIElement.vimeoId)
            setupDefaultLayoutParams()
            background = ContextCompat.getDrawable(appContext, R.drawable.vimeo_layout)
        }

        val header = AppCompatTextView(appContext).apply {
            createHeaderView()
            text = i18nData.getLocalizedText(vimeoUIElement.header)
        }

        val text = AppCompatTextView(appContext).apply {
            setupDefaultLayoutParams()
            text = i18nData.getLocalizedText(vimeoUIElement.text)
            textSize = 14F
            typeface = Typeface.create(UIElementsBuilder.SANS_SERIF_FONT_FAMILY, NORMAL)
        }

        return when (vimeoUIElement.gravity) {
            Gravity.END -> {
                val container = ConstraintLayout(appContext).apply {
                    layoutParams = ConstraintLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT).also {
                        it.setDefaultMargins(appContext)
                    }
                    tag = DefaultUIElementArguments(vimeoUIElement.id)
                }
                container.buildVerticalDividedLayout(appContext, arrayOf(header, text), arrayOf(videoView))
                container
            }
            Gravity.START -> {
                val container = ConstraintLayout(appContext).apply {
                    layoutParams = ConstraintLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT).also {
                        it.setDefaultMargins(appContext)
                    }
                    tag = DefaultUIElementArguments(vimeoUIElement.id)
                }
                container.buildVerticalDividedLayout(appContext, arrayOf(videoView), arrayOf(header, text))
                container
            }
            else -> {
                val container = LinearLayout(appContext).apply {
                    setupDefaultMarginLayoutParams()
                    orientation = LinearLayout.VERTICAL
                    tag = DefaultUIElementArguments(vimeoUIElement.id)
                }
                container.addViews(videoView, header, text)
                container
            }
        }
    }

    fun buildCharts(chartItems: ChartItems, i18nData: I18nData): View {
        val container = LinearLayout(appContext).apply {
            setupDefaultMarginLayoutParams()
            tag = DefaultUIElementArguments(chartItems.id)
            orientation = LinearLayout.VERTICAL
        }

        chartItems.items.forEach {
            container.addView(AppCompatTextView(appContext).apply {
                layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
                    setMargins(0,
                        0,
                        0,
                        context.resources.getDimension(R.dimen.margin_10).toInt())
                }
                setTextAppearance(R.style.HeadingH2)
                gravity = Gravity.CENTER
                text = i18nData.getLocalizedText(it.title)
            })
            container.addView(when (it.chartType) {
                ChartUIElement.BAR -> appContext.buildBarChart(it)
                ChartUIElement.LINE -> appContext.buildLineChart(it)
                ChartUIElement.DOUGHNUT, ChartUIElement.HALF_DOUGHNUT -> appContext.buildDoughnutChart(it)
                else -> appContext.buildBarChart(it)
            })
            container.addView(AppCompatTextView(appContext).apply {
                layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
                    setMargins(0, context.resources.getDimensionPixelOffset(R.dimen.margin_10), 0, 0)
                }
                text = i18nData.getLocalizedText(it.text)
                setTextAppearance(R.style.Body14RobotoLight)
            })
        }

        return container
    }

    fun buildDualColumnImages(dualColumnImagesUIElement: DualColumnImagesUIElement, i18nData: I18nData): View {
        val container = ConstraintLayout(appContext).apply {
            layoutParams = ConstraintLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT).also {
                it.setDefaultMargins(appContext)
            }
            tag = DefaultUIElementArguments(dualColumnImagesUIElement.id)
        }

        fun LinearLayout.LayoutParams.setupMargins() {
            setMargins(0, appContext.resources.getDimension(R.dimen.margin_16).toInt(), 0, 0)
        }

        val firstImage = AppCompatImageView(appContext).apply {
            layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, 360).apply {
                setupMargins()
            }
            scaleType = CENTER_CROP
            tag = ImageUIElementArguments("", dualColumnImagesUIElement.leftImageId)
        }
        val firstText = AppCompatTextView(appContext).apply {
            layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
                setupMargins()
            }
            text = i18nData.getLocalizedText(dualColumnImagesUIElement.leftText)
            textSize = 14F
            typeface = Typeface.create(UIElementsBuilder.SANS_SERIF_FONT_FAMILY, NORMAL)
            gravity = Gravity.TOP
        }
        val secondImage = AppCompatImageView(appContext).apply {
            layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, 360).apply {
                setupMargins()
            }
            scaleType = CENTER_CROP
            tag = ImageUIElementArguments("", dualColumnImagesUIElement.rightImageId)
        }
        val secondText = AppCompatTextView(appContext).apply {
            layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
                setupMargins()
            }
            text = i18nData.getLocalizedText(dualColumnImagesUIElement.rightText)
            textSize = 14F
            typeface = Typeface.create(UIElementsBuilder.SANS_SERIF_FONT_FAMILY, NORMAL)
            gravity = Gravity.TOP
        }

        container.apply {
            buildVerticalDividedLayout(
                appContext,
                viewsLeft = arrayOf(firstImage, firstText),
                viewsRight = arrayOf(secondImage, secondText)
            )
        }
        return container
    }

    fun buildFacts(factsUIElement: FactsUIElement, i18nData: I18nData): View {
        val container = LinearLayout(appContext).apply {
            setupDefaultMarginLayoutParams()
            orientation = LinearLayout.VERTICAL
            tag = DefaultUIElementArguments(factsUIElement.id)
        }

        val views = arrayListOf<View>()
        val layoutInflater = LayoutInflater.from(appContext)

        factsUIElement.items.forEachIndexed { index, factItem ->
            views.add(layoutInflater.inflate(R.layout.item_fact, null).apply {
                imageView.tag = ImageUIElementArguments("", factItem.imageId)
                descriptionTextView.text = i18nData.getLocalizedText(factItem.text)
                layoutParams = LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT, 1f)
            })
        }

        for (i in factsUIElement.items.indices step factsUIElement.perLine) {
            val linear = LinearLayout(appContext).apply {
                setupDefaultLayoutParams()
            }
            val s =
                if ((i + factsUIElement.perLine) < factsUIElement.items.size) factsUIElement.perLine else factsUIElement.items.size - i
            for (index in i until i + s) {
                linear.addView(views[index].apply {
                    layoutParams = LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT, 1f)
                })
            }
            container.addView(linear)
        }

        return container
    }

    fun buildDualColumnText(dualColumnTextUIElement: DualColumnTextUIElement, i18nData: I18nData): View {
        val container = ConstraintLayout(appContext).apply {
            layoutParams = ConstraintLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT).also {
                it.setDefaultMargins(appContext)
            }
            tag = DefaultUIElementArguments(dualColumnTextUIElement.id)
        }

        fun LinearLayout.LayoutParams.setupMargins() {
            setMargins(
                0,
                appContext.resources.getDimension(R.dimen.margin_16).toInt(),
                0,
                0
            )
        }

        val firstText = AppCompatTextView(appContext).apply {
            layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
                setupMargins()
            }
            setupDefaultText()
            text = i18nData.getLocalizedText(dualColumnTextUIElement.leftText)
        }
        val secondText = AppCompatTextView(appContext).apply {
            layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
                setupMargins()
            }
            setupDefaultText()
            text = i18nData.getLocalizedText(dualColumnTextUIElement.rightText)
        }

        container.apply {
            buildVerticalDividedLayout(
                appContext,
                viewsLeft = arrayOf(firstText),
                viewsRight = arrayOf(secondText)
            )
        }

        return container
    }

    fun buildTiledImage(tiledImagesUIElement: TiledImagesUIElement, i18nData: I18nData): View {
        val container = LinearLayout(appContext).apply {
            setupDefaultMarginLayoutParams()
            orientation = LinearLayout.VERTICAL
            tag = DefaultUIElementArguments(tiledImagesUIElement.id)
        }
        val tabLayout = appContext.createTabLayout(tiledImagesUIElement.items.size)
        val viewPagerImage = ViewPager2(appContext).apply {
            layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
            val pairs = tiledImagesUIElement.items.map {
                it.copy(
                    _header = i18nData.getLocalizedText(it.header),
                    _text = i18nData.getLocalizedText(it.text)
                )
            }.run {
                val listPair = arrayListOf<Pair<TiledImageItem, TiledImageItem>>()
                for (i in 0 until size step 2) {
                    val second = if (i + 1 == size) TiledImageItem() else get(i + 1)
                    listPair.add(get(i) to second)
                }
                listPair
            }
            adapter = TabletTiledImageAdapter(pairs, tiledImagesUIElement.run { Triple(bold, italic, underline) })
        }
        container.addView(viewPagerImage)
        if (tiledImagesUIElement.items.size > 2) {
            container.addView(tabLayout)
            appContext.bindViewPagerWithTabLayout(tabLayout.apply { tabMode = TabLayout.MODE_FIXED }, viewPagerImage)
        }

        appContext.bindViewPagerWithTabLayout(tabLayout, viewPagerImage)
        return container
    }

    fun buildButton(buttonUIElement: ButtonUIElement, i18nData: I18nData): View {
        val container = LinearLayout(appContext).apply {
            setupDefaultMarginLayoutParams()
            orientation = LinearLayout.VERTICAL
            tag = DefaultUIElementArguments(buttonUIElement.id)
        }
        container.addView(RoundedCornersButton(ContextThemeWrapper(appContext,
            R.style.CommonRedButtonStyle)).apply {
            buttonUIElement.also {
                layoutParams =
                    LinearLayout.LayoutParams(resources.getDimensionPixelOffset(R.dimen.tabletButtonWidth),
                        WRAP_CONTENT).also { params ->
                        params.gravity = Gravity.CENTER_HORIZONTAL
                    }
                text = i18nData.getLocalizedText(it.text)
                setColor(appContext.parseColor(it.btnColor))
                setTextColor(appContext.parseColor(it.textColor))
                tag = ClickableUIElementArguments("", buttonUIElement.link)
            }
        })
        return container
    }

    fun buildDisclaimer(disclaimerUiElement: DisclaimerUiElement, i18nData: I18nData): View {
        val container = RelativeLayout(appContext).apply {
            layoutParams = RelativeLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT).also {
                it.setMargins(
                    0,
                    context.resources.getDimension(R.dimen.default_top_bottom_padding).toInt(),
                    0,
                    0
                )
            }
            tag = DefaultUIElementArguments(disclaimerUiElement.id)
            val padding = context.resources.getDimensionPixelOffset(R.dimen.margin_16)
            setPadding(padding, padding, padding, padding)
            setBackgroundColor(Color.parseColor(disclaimerUiElement.background))
        }

        val icon = AppCompatImageView(appContext).apply {
            val size = resources.getDimensionPixelOffset(R.dimen.iconDisclaimerSize)
            layoutParams = RelativeLayout.LayoutParams(size, size).apply {
                setMargins(0, 0, resources.getDimensionPixelOffset(R.dimen.margin_12), 0)
            }
            id = View.generateViewId()
            scaleType = CENTER_CROP
            tag = ImageUIElementArguments("", disclaimerUiElement.iconUrl, false)
        }

        val description = AppCompatTextView(appContext).apply {
            layoutParams = RelativeLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
            id = View.generateViewId()
            text = i18nData.getLocalizedText(disclaimerUiElement.text)
            setTextColor(ContextCompat.getColor(context, R.color.white80))
            setDipTextSize(R.dimen.textSize10)
            typeface = Typeface.create(ResourcesCompat.getFont(context, R.font.roboto_light), NORMAL)
        }

        container.addViews(
            icon.apply { (layoutParams as? RelativeLayout.LayoutParams)?.addRule(RelativeLayout.ALIGN_PARENT_TOP) },
            description.apply {
                (layoutParams as? RelativeLayout.LayoutParams)?.addRule(RelativeLayout.RIGHT_OF, icon.id)
            }
        )

        return container
    }

    fun buildLinkedText(linkedTextUIElement: LinkedTextUIElement): View {
        val container = LinearLayout(appContext)

        if (Locale.getDefault().getUserLanguage() != linkedTextUIElement.language)
            return container

        container.apply {
            layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT).also { params ->
                params.setMargins(48, 0, 48, 0)
            }
            orientation = LinearLayout.VERTICAL
            tag = DefaultUIElementArguments(linkedTextUIElement.id)
        }

        container.addView(AppCompatTextView(appContext).apply {
            linkedTextUIElement.also {
                setupTextView(TextUIElement(
                    _color = it.color,
                    _fontSize = it.fontSize,
                    _justify = it.justify,
                    _bold = it.bold,
                    _italic = it.italic,
                    _underline = it.underline
                ))
                text = linkedTextUIElement.text
                tag = SpannableUIElementArguments("", it.wordsLink)
            }
        })
        return container
    }

    fun buildDepartment(departmentUIElement: DepartmentUIElement): View {
        return RecyclerView(appContext).apply {
            setupDefaultMarginLayoutParams()
            overScrollMode = View.OVER_SCROLL_NEVER
            layoutManager = SafeLinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            tag = DefaultUIElementArguments(departmentUIElement.id)
            adapter = DepartmentAdapter(departmentUIElement.items)
        }
    }


    fun buildGroupUIElementView(group: Group): View {
        return LinearLayout(appContext).apply {
            setupDefaultLayoutParams()
            orientation = LinearLayout.VERTICAL
            tag = GroupUIElementArguments(group.id, group.idList)
        }
    }
}