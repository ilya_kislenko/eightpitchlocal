package app.eightpitch.com.data.dto.dynamicviews

import android.graphics.Paint
import android.graphics.Typeface
import android.graphics.Typeface.*
import android.view.*
import androidx.appcompat.widget.*
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.*
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.R
import app.eightpitch.com.extensions.inflateView

typealias TextAppearanceSet = Triple<Boolean, Boolean, Boolean>

class TiledImageAdapter(
    private val items: List<TiledImageItem>,
    private val textAppearanceSet: TextAppearanceSet
) :
    RecyclerView.Adapter<TiledImageAdapter.TiledImageHolder>(), ParentLayoutHolder {

    private val result = MutableLiveData<ViewGroup>()
    internal fun getResult(): LiveData<ViewGroup> = result

    private var constraint: ViewGroup? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TiledImageHolder {
        val view = inflateView(parent, R.layout.item_tiled_image)
        constraint = view as ViewGroup
        return TiledImageHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: TiledImageHolder, position: Int) {
        val item = items[position]
        holder.apply {
            backgroundImage.tag = ImageUIElementArguments("", item.imageId)
            description.also {
                it.text = item.text
                val fontFamily = ResourcesCompat.getFont(it.context, R.font.roboto_regular)
                val (isBold, isItalic, isUnderline) = textAppearanceSet
                it.typeface = Typeface.create(
                    fontFamily,
                    when {
                        isBold && isItalic-> BOLD_ITALIC
                        isBold -> BOLD
                        isItalic -> ITALIC
                        else -> NORMAL
                    }
                )
                it.paintFlags = if (isUnderline) Paint.UNDERLINE_TEXT_FLAG else it.paintFlags
            }
            header.text = item.header
            constraint?.let { result.value = it }
        }
    }

    class TiledImageHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val backgroundImage: AppCompatImageView = itemView.findViewById(R.id.backgroundImage)
        val description: AppCompatTextView = itemView.findViewById(R.id.descriptionTextView)
        val header: AppCompatTextView = itemView.findViewById(R.id.headerTextView)
    }

    override fun getParentLayout(): ViewGroup? = constraint
}