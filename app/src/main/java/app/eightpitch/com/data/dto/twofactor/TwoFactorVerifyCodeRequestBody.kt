package app.eightpitch.com.data.dto.twofactor

import com.google.gson.annotations.SerializedName

data class TwoFactorVerifyCodeRequestBody(
    @SerializedName("code")
    val code: String,
    @SerializedName("interactionId")
    val interactionId: String
)