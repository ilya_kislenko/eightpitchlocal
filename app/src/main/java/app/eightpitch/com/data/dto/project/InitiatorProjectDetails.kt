package app.eightpitch.com.data.dto.project

import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.ACTIVE
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.COMING_SOON
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.FINISHED
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.MANUAL_RELEASE
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.NOT_STARTED
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.REFUNDED
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.TOKENS_TRANSFERRED
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.WAIT_BLOCKCHAIN
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.WAIT_RELEASE
import app.eightpitch.com.data.dto.projectdetails.ProjectPage
import app.eightpitch.com.data.dto.projectdetails.ProjectsFile
import app.eightpitch.com.extensions.NO_TYPE

/**
 * Description of initiator's project
 */
class InitiatorProjectDetails(
    val companyName: String = "",
    val graph: AmountOfProjectInvestments = AmountOfProjectInvestments(),
    val id: String = "",
    val projectBi: ProjectBi = ProjectBi(),
    val projectDescription: String = "",
    val projectFiles: List<ProjectsFile> = listOf(),
    val projectPage: ProjectPage = ProjectPage(),
    @InvestorsProject.Companion.ProjectStatus val projectStatus: String = NO_TYPE,
    val tokenParametersDocument: TokenParametersDocument = TokenParametersDocument()
) {

    companion object {

        val activeStatuses = arrayListOf(
            NOT_STARTED, COMING_SOON, ACTIVE,
            MANUAL_RELEASE, FINISHED, WAIT_RELEASE
        )

        val finishedStatuses = arrayListOf(
            REFUNDED, TOKENS_TRANSFERRED, WAIT_BLOCKCHAIN
        )
    }
}