package app.eightpitch.com.data.dto.investment

import androidx.annotation.StringDef
import app.eightpitch.com.extensions.NO_TYPE
import app.eightpitch.com.utils.Constants.DEFAULT_DATA_PATTERN
import app.eightpitch.com.utils.DateUtils.parseByFormat

data class IsInvestmentAllowedResponse(
    @AllowStatus val status: String = NO_TYPE,
    val whitelistEndDate: String = ""
) {

    val data
    get() = whitelistEndDate.parseByFormat(pattern = DEFAULT_DATA_PATTERN)

    companion object {

        const val TRUE = "TRUE"
        const val FALSE = "FALSE"
        const val COMING_SOON = "COMING_SOON"

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(NO_TYPE, TRUE, FALSE, COMING_SOON)
        annotation class AllowStatus
    }
}