package app.eightpitch.com.data.dto.vimeo

import com.google.gson.annotations.SerializedName

/**
 * The class describes the video configuration.
 * @param height - Video height
 * @param url - Direct link to vimeo video
 */
data class VimeoProgressive(
    @SerializedName("height")
    val height: Int = 0,
    @SerializedName("url")
    val url: String = ""
)