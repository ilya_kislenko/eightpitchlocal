package app.eightpitch.com.data.dto.payment

import com.google.gson.annotations.SerializedName

/**
 * Represents an info about payment (classic/debit)
 */
data class BankPaymentsDataResponse(
    @SerializedName("bic")
    val bic: String = "",
    @SerializedName("iban")
    val IBAN: String = "",
    @SerializedName("transactionInfo")
    val transactionInfo: SoftCapClassicBankTransactionInfo = SoftCapClassicBankTransactionInfo()
)