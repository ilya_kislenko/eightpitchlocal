package app.eightpitch.com.data.dto.vimeo

import com.google.gson.annotations.SerializedName

/**
 * The class stores a list with vimeo video configurations
 */
class VimeoFiles(
    @SerializedName("progressive")
    val progressive: List<VimeoProgressive> = arrayListOf()
)