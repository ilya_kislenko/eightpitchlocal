package app.eightpitch.com.data.dto.investment

import android.os.*
import app.eightpitch.com.data.dto.project.ProjectInvestment.Companion.InvestmentStatus
import app.eightpitch.com.extensions.NO_TYPE
import app.eightpitch.com.extensions.readStringSafe
import java.math.BigDecimal

data class UnprocessedInvestment(
    val amount: BigDecimal = BigDecimal(0),
    val investmentId: String = "",
    @InvestmentStatus val investmentStatus: String = NO_TYPE
) : Parcelable {

    constructor(parcel: Parcel) : this(
        investmentId = parcel.readStringSafe(),
        investmentStatus = parcel.readStringSafe())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeSerializable(amount)
        parcel.writeString(investmentId)
        parcel.writeString(investmentStatus)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<UnprocessedInvestment> {
        override fun createFromParcel(parcel: Parcel): UnprocessedInvestment {
            return UnprocessedInvestment(parcel)
        }

        override fun newArray(size: Int): Array<UnprocessedInvestment?> {
            return arrayOfNulls(size)
        }
    }
}