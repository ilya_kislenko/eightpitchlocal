package app.eightpitch.com.data.dto.projectdetails

import android.os.*
import app.eightpitch.com.extensions.readStringSafe
import com.google.gson.annotations.SerializedName

class FinancialInformation(
    @SerializedName("companyAddress")
    var companyAddress: String = "",
    @SerializedName("country")
    var country: String = "",
    @SerializedName("financingPurpose")
    var financingPurpose: String = "",
    @SerializedName("goal")
    var goal: Long = 0,
    @SerializedName("guarantee")
    var guarantee: String = "",
    @SerializedName("id")
    var id: String = "",
    @SerializedName("investmentSeries")
    var investmentSeries: String = "",
    @SerializedName("isin")
    var isin: String = "",
    @SerializedName("nominalValue")
    var nominalValue: String = "",
    @SerializedName("threshold")
    var threshold: Long = 0,
    @SerializedName("typeOfSecurity")
    var typeOfSecurity: String = ""
): Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readLong(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readLong(),
        parcel.readStringSafe()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(companyAddress)
        parcel.writeString(country)
        parcel.writeString(financingPurpose)
        parcel.writeLong(goal)
        parcel.writeString(guarantee)
        parcel.writeString(id)
        parcel.writeString(investmentSeries)
        parcel.writeString(isin)
        parcel.writeString(nominalValue)
        parcel.writeLong(threshold)
        parcel.writeString(typeOfSecurity)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<FinancialInformation> {
        override fun createFromParcel(parcel: Parcel): FinancialInformation {
            return FinancialInformation(parcel)
        }

        override fun newArray(size: Int): Array<FinancialInformation?> {
            return arrayOfNulls(size)
        }
    }
}