package app.eightpitch.com.data.dto.project

import androidx.annotation.StringDef
import app.eightpitch.com.extensions.NO_TYPE
import com.google.gson.annotations.SerializedName

/**
 * The class describes the model of the project in which the investor has already invested
 */
class InvestorsProject(
    @SerializedName("investmentsInfo")
    var investmentsInfo: InvestmentsInfo = InvestmentsInfo(),
    @SerializedName("projectInfo")
    var projectInfo: ProjectDetails = ProjectDetails(),
    @SerializedName("projectStatus")
    @ProjectStatus var projectStatus: String = NO_TYPE
) {

    companion object {
        const val NOT_STARTED = "NOT_STARTED"
        const val COMING_SOON = "COMING_SOON"
        const val ACTIVE = "ACTIVE"
        const val FINISHED = "FINISHED"
        const val WAIT_BLOCKCHAIN = "WAIT_BLOCKCHAIN"
        const val REFUNDED = "REFUNDED"
        const val MANUAL_RELEASE = "MANUAL_RELEASE"
        const val WAIT_RELEASE = "WAIT_RELEASE"
        const val TOKENS_TRANSFERRED = "TOKENS_TRANSFERRED"

        @StringDef(
            NO_TYPE, NOT_STARTED, COMING_SOON, ACTIVE, FINISHED, WAIT_BLOCKCHAIN,
            REFUNDED, MANUAL_RELEASE, WAIT_RELEASE, TOKENS_TRANSFERRED
        )
        annotation class ProjectStatus

        val finishedStatuses = arrayListOf(
            TOKENS_TRANSFERRED,
            WAIT_BLOCKCHAIN,
            WAIT_RELEASE,
            REFUNDED,
            MANUAL_RELEASE
        )
    }
}