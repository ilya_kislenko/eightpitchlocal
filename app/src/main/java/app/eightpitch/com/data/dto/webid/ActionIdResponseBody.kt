package app.eightpitch.com.data.dto.webid

data class ActionIdResponseBody(
    val actionId: String = ""
)