package app.eightpitch.com.data.dto.twofactor

import androidx.annotation.StringDef
import app.eightpitch.com.extensions.NO_TYPE
import com.google.gson.annotations.SerializedName

class SmsTwoFactorResponse (
    @SerializedName("status")
    @SmsTwoFactorStatusType
    val status: String
){
    companion object{

        const val SUCCESS = "SUCCESS"
        const val INVALID_PASSWORD = "INVALID_PASSWORD"
        const val INVALID_CODE = "INVALID_CODE"
        const val TWO_FACTOR_AUTH_ALREADY_ENABLED = "TWO_FACTOR_AUTH_ALREADY_ENABLED"

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(NO_TYPE, SUCCESS, INVALID_PASSWORD, INVALID_CODE, TWO_FACTOR_AUTH_ALREADY_ENABLED)
        annotation class SmsTwoFactorStatusType
    }
}