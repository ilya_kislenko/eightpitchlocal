package app.eightpitch.com.data.dto.projectdetails

import androidx.annotation.StringDef
import app.eightpitch.com.extensions.NO_TYPE
import com.google.gson.annotations.SerializedName
import java.math.BigDecimal
import java.math.BigDecimal.ZERO

class TokenParametersDocumentChangeability(
    @SerializedName("allowChangeFromDate")
    var allowChangeFromDate: String = "",
    @SerializedName("allowChangeToDate")
    var allowChangeToDate: String = "",
    @SerializedName("conversionRatio")
    var conversionRatio: BigDecimal = ZERO,
    @SerializedName("mandatoryChangeOnDate")
    var mandatoryChangeOnDate: String = "",
    @SerializedName("mandatoryChangeTradePrice")
    var mandatoryChangeTradePrice: BigDecimal = ZERO,
    @SerializedName("optionalChangeTradePrice")
    var optionalChangeTradePrice: BigDecimal = ZERO,
    @SerializedName("tokenId")
    var tokenId: String = "",
    @SerializedName("tokenParametersDocumentChangeabilityType")
    @DocumentChangeabilityType
    var tokenParametersDocumentChangeabilityType: String = NO_TYPE
){

    companion object{

        const val MANDATORY_CHANGE_ON_DATE = "MANDATORY_CHANGE_ON_DATE"
        const val MANDATORY_CHANGE_ON_TRADE_PRICE = "MANDATORY_CHANGE_ON_TRADE_PRICE"
        const val OPTIONAL_CHANGE_ON_TRADE_PRICE_WITHOUT_APPROVAL = "OPTIONAL_CHANGE_ON_TRADE_PRICE_WITHOUT_APPROVAL"
        const val OPTIONAL_CHANGE_ON_TRADE_PRICE_ONCE_APPROVAL = "OPTIONAL_CHANGE_ON_TRADE_PRICE_ONCE_APPROVAL"
        const val OPTIONAL_CHANGE_ON_TRADE_PRICE_EVERY_OPERATION_APPROVAL = "OPTIONAL_CHANGE_ON_TRADE_PRICE_EVERY_OPERATION_APPROVAL"
        const val ALLOW_CHANGE_FROM_DATE_TO_DATE_WITHOUT_APPROVAL = "ALLOW_CHANGE_FROM_DATE_TO_DATE_WITHOUT_APPROVAL"
        const val ALLOW_CHANGE_FROM_DATE_TO_DATE_ONCE_APPROVAL = "ALLOW_CHANGE_FROM_DATE_TO_DATE_ONCE_APPROVAL"
        const val ALLOW_CHANGE_FROM_DATE_TO_DATE_EVERY_OPERATION_APPROVAL = "ALLOW_CHANGE_FROM_DATE_TO_DATE_EVERY_OPERATION_APPROVAL"

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(NO_TYPE, MANDATORY_CHANGE_ON_DATE, MANDATORY_CHANGE_ON_TRADE_PRICE, OPTIONAL_CHANGE_ON_TRADE_PRICE_WITHOUT_APPROVAL,
            OPTIONAL_CHANGE_ON_TRADE_PRICE_ONCE_APPROVAL, OPTIONAL_CHANGE_ON_TRADE_PRICE_EVERY_OPERATION_APPROVAL,
            ALLOW_CHANGE_FROM_DATE_TO_DATE_WITHOUT_APPROVAL, ALLOW_CHANGE_FROM_DATE_TO_DATE_ONCE_APPROVAL,
            ALLOW_CHANGE_FROM_DATE_TO_DATE_EVERY_OPERATION_APPROVAL)
        annotation class DocumentChangeabilityType
    }
}