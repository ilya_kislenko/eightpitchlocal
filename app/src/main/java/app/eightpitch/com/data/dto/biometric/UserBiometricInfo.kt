package app.eightpitch.com.data.dto.biometric

/**
 * Contains data for QuickLogin
 */
data class UserBiometricInfo(
    val pinCode: String = "",
    val isBiometricActive: Boolean = false
) {

    internal val hasPinCode
        get() = pinCode.isNotEmpty()
}