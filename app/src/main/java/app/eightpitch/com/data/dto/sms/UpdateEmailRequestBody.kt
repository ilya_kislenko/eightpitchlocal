package app.eightpitch.com.data.dto.sms

import com.google.gson.annotations.SerializedName

class UpdateEmailRequestBody(
    @SerializedName("email")
    val email: String
)