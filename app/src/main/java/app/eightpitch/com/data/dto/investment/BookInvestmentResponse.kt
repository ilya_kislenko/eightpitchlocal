package app.eightpitch.com.data.dto.investment

import androidx.annotation.StringDef
import app.eightpitch.com.extensions.NO_TYPE
import app.eightpitch.com.extensions.orDefaultValue
import com.google.gson.annotations.SerializedName

data class BookInvestmentResponse(
    @SerializedName("availableTokens")
    private val _availableTokens: Int = 0,
    @SerializedName("confirmedInvestmentId")
    private val _confirmedInvestmentId: String = "",
    @SerializedName("investmentId")
    private val _investmentId: String = "",
    @SerializedName("status")
    @BookInvestmentStatus val _status: String?
) {

    val availableTokens: Int
        get() = _availableTokens.orDefaultValue(0)

    val confirmedInvestmentId: String
        get() = _confirmedInvestmentId.orDefaultValue("")

    val investmentId: String
        get() = _investmentId.orDefaultValue("")

    val status: String
        get() = _status.orDefaultValue(NO_TYPE)

    companion object {

        const val IN_PROGRESS = "IN_PROGRESS"
        const val REACHED_SINGLE_INVESTOR_OWNERSHIP_LIMIT = "REACHED_SINGLE_INVESTOR_OWNERSHIP_LIMIT"
        const val AMOUNT_IS_LESS_THAN_MINIMAL_INVESTMENT_AMOUNT = "AMOUNT_IS_LESS_THAN_MINIMAL_INVESTMENT_AMOUNT"
        const val AMOUNT_IS_MORE_THAN_AVAILABLE_TOKENS = "AMOUNT_IS_MORE_THAN_AVAILABLE_TOKENS "

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(NO_TYPE, IN_PROGRESS, REACHED_SINGLE_INVESTOR_OWNERSHIP_LIMIT,
            AMOUNT_IS_LESS_THAN_MINIMAL_INVESTMENT_AMOUNT, AMOUNT_IS_MORE_THAN_AVAILABLE_TOKENS)
        annotation class BookInvestmentStatus
    }
}