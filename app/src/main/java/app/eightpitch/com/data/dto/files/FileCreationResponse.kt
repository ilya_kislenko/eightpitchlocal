package app.eightpitch.com.data.dto.files

import app.eightpitch.com.extensions.orDefaultValue
import com.google.gson.annotations.SerializedName

class FileCreationResponse(
    @SerializedName("id")
    val _filedId: String = "",
) {

    val fileId
        get() = _filedId.orDefaultValue("")
}