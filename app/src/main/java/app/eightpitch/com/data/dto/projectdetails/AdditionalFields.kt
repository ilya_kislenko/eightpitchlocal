package app.eightpitch.com.data.dto.projectdetails

import android.os.*
import androidx.annotation.*
import app.eightpitch.com.R
import app.eightpitch.com.data.dto.dynamicviews.mainfacts.MainFact
import app.eightpitch.com.extensions.readStringSafe
import com.google.gson.annotations.SerializedName

/**
 * Represents a bunch of a properties to build MainFacts tab.
 * @see MainFact
 */
data class AdditionalFields(
    @SerializedName("agio")
    var agio: String = "",
    @SerializedName("conversionRight")
    var conversionRight: String = "",
    @SerializedName("dividend")
    var dividend: String = "",
    @SerializedName("futureShareNominalValue")
    var futureShareNominalValue: String = "",
    @SerializedName("highOfDividend")
    var highOfDividend: String = "",
    @SerializedName("id")
    var id: String = "",
    @SerializedName("initialSalesCharge")
    var initialSalesCharge: String = "",
    @SerializedName("interest")
    var interest: String = "",
    @SerializedName("interestInterval")
    var interestInterval: String = "",
    @SerializedName("issuedShares")
    var issuedShares: String = "",
    @SerializedName("typeOfRepayment")
    var typeOfRepayment: String = "",
    @SerializedName("wkin")
    var wkin: String = ""
) : Parcelable {

    val descriptionToLabel: List<MainFact>
        get() = createFacts()

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(agio)
        parcel.writeString(conversionRight)
        parcel.writeString(dividend)
        parcel.writeString(futureShareNominalValue)
        parcel.writeString(highOfDividend)
        parcel.writeString(id)
        parcel.writeString(initialSalesCharge)
        parcel.writeString(interest)
        parcel.writeString(interestInterval)
        parcel.writeString(issuedShares)
        parcel.writeString(typeOfRepayment)
        parcel.writeString(wkin)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<AdditionalFields> {
        override fun createFromParcel(parcel: Parcel): AdditionalFields {
            return AdditionalFields(parcel)
        }

        override fun newArray(size: Int): Array<AdditionalFields?> {
            return arrayOfNulls(size)
        }

        const val WKN = R.string.wkn_text
        const val INTEREST_INTERVAL = R.string.interest_interval_text
        const val DIVIDEND_CLAIMS = R.string.dividend_claims_text
        const val DIVIDEND_AMOUNT = R.string.dividend_amount_text
        const val BROKERAGE_COMMISSION = R.string.brokerage_commission_text
        const val TYPE_OF_REPAYMENT = R.string.type_of_repayment_text
        const val ISSUE_SURCHARGE = R.string.issue_surcharge_text
        const val CONVERSION_RIGHT = R.string.conversion_right_text
        const val ISSUED_SHARES = R.string.issued_shares_text
        const val FUTURE_SHARE_NOMINAL_VALUE = R.string.future_share_nominal_value_text
        const val INTEREST_RATE = R.string.interest_rate_text
    }

    private fun createFacts(): List<MainFact> {
        return arrayListOf<MainFact>().apply {
            addIfNotEmpty(agio, ISSUE_SURCHARGE, R.drawable.icon_issue_surcharge_field)
            addIfNotEmpty(conversionRight, CONVERSION_RIGHT, R.drawable.icon_conversation_right)
            addIfNotEmpty(dividend, DIVIDEND_CLAIMS, R.drawable.icon_divide_end_field)
            addIfNotEmpty(futureShareNominalValue, FUTURE_SHARE_NOMINAL_VALUE, R.drawable.icon_future_share_value_field)
            addIfNotEmpty(highOfDividend, DIVIDEND_AMOUNT, R.drawable.icon_hight_of_dividend_field)
            addIfNotEmpty(initialSalesCharge, BROKERAGE_COMMISSION, R.drawable.icon_sales_charge_field)
            addIfNotEmpty(interest, INTEREST_RATE, R.drawable.icon_interest_rate)
            addIfNotEmpty(interestInterval, INTEREST_INTERVAL, R.drawable.icon_interest_interval_field)
            addIfNotEmpty(issuedShares, ISSUED_SHARES, R.drawable.icon_issued_shares_field)
            addIfNotEmpty(typeOfRepayment, TYPE_OF_REPAYMENT, R.drawable.icon_type_of_repayment_field)
            addIfNotEmpty(wkin, WKN, R.drawable.icon_wkin_field)
        }.toList()
    }

    private fun ArrayList<MainFact>.addIfNotEmpty(
        description: String,
        @StringRes label: Int,
        @DrawableRes icon: Int
    ) {
        if (description.isNotEmpty())
            add(MainFact(
                label, description, icon
            ))
    }
}