package app.eightpitch.com.data.dto.profilesettings

import com.google.gson.annotations.SerializedName

data class SubscribeToEmailRequestBody(
    @SerializedName("subscribeToEmails")
    val subscribeToEmails: Boolean
)