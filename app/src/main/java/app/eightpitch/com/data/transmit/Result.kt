package app.eightpitch.com.data.transmit

import java.lang.Exception

/**
 * To handle this result representation object properly on a Fragments level, please use following functions:
 * Fragment.handleResult || Fragment.handleResultWithError, because they are supporting a redirection to a login page
 * when the refresh token process was finished with an error.
 */
data class Result<out T>(val status: ResultStatus, val result: T? = null, val exception: Exception? = null)