package app.eightpitch.com.data.dto.dynamicviews.builders

import android.content.Context
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.MATCH_CONSTRAINT
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.WRAP_CONTENT
import androidx.constraintlayout.widget.ConstraintSet
import androidx.constraintlayout.widget.ConstraintSet.*
import androidx.constraintlayout.widget.Guideline
import app.eightpitch.com.R

fun ConstraintLayout.buildVerticalDividedLayout(
    appContext: Context,
    viewsLeft: Array<View>,
    viewsRight: Array<View>
) {
    val guideline = Guideline(appContext).apply {
        id = View.generateViewId()
        layoutParams = ConstraintLayout.LayoutParams(ConstraintSet.WRAP_CONTENT, ConstraintSet.WRAP_CONTENT).apply {
            guidePercent = 0.5F
            orientation = ConstraintLayout.LayoutParams.VERTICAL
        }
    }
    this.addView(guideline)

    viewsLeft.forEachIndexed { index, view ->
        addView(view.apply {
            if (view.layoutParams == null)
                view.layoutParams = ConstraintLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
            if (view.id == View.NO_ID)
                this.id = View.generateViewId()
        })
        val (constrainWidth, constrainHeight) = view.layoutParams.run { width to height }
        val previousViewId = if (index == 0) PARENT_ID else viewsLeft[index - 1].id
        ConstraintSet().apply {
            clone(this@buildVerticalDividedLayout)
            connect(view.id, END, guideline.id, START, resources.getDimensionPixelSize(R.dimen.margin_8))
            connect(view.id, START, PARENT_ID, START, resources.getDimensionPixelSize(R.dimen.margin_16))
            connect(view.id,
                TOP,
                previousViewId,
                if (index == 0) TOP else BOTTOM,
                resources.getDimensionPixelSize(R.dimen.margin_12))
            constrainWidth(view.id, MATCH_CONSTRAINT)
            constrainHeight(view.id, constrainHeight)
            applyTo(this@buildVerticalDividedLayout)
        }
    }

    viewsRight.forEachIndexed { index, view ->
        addView(view.apply {
            if (view.layoutParams == null)
                view.layoutParams = ConstraintLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
            if (view.id == View.NO_ID)
                this.id = View.generateViewId()
        })
        val previousViewId = if (index == 0) PARENT_ID else viewsRight[index - 1].id
        val (constrainWidth,constrainHeight) = view.layoutParams.run { width to height }
        ConstraintSet().apply {
            clone(this@buildVerticalDividedLayout)
            connect(view.id, END, PARENT_ID, END, resources.getDimensionPixelSize(R.dimen.margin_16))
            connect(view.id, START, guideline.id, END, resources.getDimensionPixelSize(R.dimen.margin_8))
            connect(view.id,
                TOP,
                previousViewId,
                if (index == 0) TOP else BOTTOM,
                resources.getDimensionPixelSize(R.dimen.margin_12))
            constrainWidth(view.id, MATCH_CONSTRAINT)
            constrainHeight(view.id, constrainHeight)
            applyTo(this@buildVerticalDividedLayout)
        }
    }
}

fun ConstraintLayout.buildVerticalDividedLayout(
    appContext: Context,
    viewLeft: View?,
    viewRight: View?
) {
    val guideline = Guideline(appContext).apply {
        id = View.generateViewId()
        layoutParams = ConstraintLayout.LayoutParams(ConstraintSet.WRAP_CONTENT, ConstraintSet.WRAP_CONTENT).apply {
            guidePercent = 0.5F
            orientation = ConstraintLayout.LayoutParams.VERTICAL
        }
    }
    this.addView(guideline)

    if (viewLeft != null) {
        addView(viewLeft.apply {
            layoutParams = ConstraintLayout.LayoutParams(MATCH_CONSTRAINT,
                ConstraintSet.WRAP_CONTENT)
            if (viewLeft.id == View.NO_ID)
                viewLeft.id = View.generateViewId()
        })
        ConstraintSet().apply {
            clone(this@buildVerticalDividedLayout)
            connect(viewLeft.id, END, guideline.id, START, resources.getDimensionPixelSize(R.dimen.margin_8))
            connect(viewLeft.id, START, PARENT_ID, START, resources.getDimensionPixelSize(R.dimen.margin_16))
            connect(viewLeft.id, TOP, PARENT_ID, TOP, resources.getDimensionPixelSize(R.dimen.margin_12))
            constrainWidth(viewLeft.id, MATCH_CONSTRAINT)
            constrainHeight(viewLeft.id, WRAP_CONTENT)
            applyTo(this@buildVerticalDividedLayout)
        }
    }

    if (viewRight != null) {
        addView(viewRight.apply {
            layoutParams = ConstraintLayout.LayoutParams(MATCH_CONSTRAINT,
                ConstraintSet.WRAP_CONTENT)
            if (viewRight.id == View.NO_ID)
                viewRight.id = View.generateViewId()
        })
        ConstraintSet().apply {
            clone(this@buildVerticalDividedLayout)
            connect(viewRight.id, END, PARENT_ID, END, resources.getDimensionPixelSize(R.dimen.margin_8))
            connect(viewRight.id, START, guideline.id, START, resources.getDimensionPixelSize(R.dimen.margin_16))
            connect(viewRight.id, TOP, PARENT_ID, TOP, resources.getDimensionPixelSize(R.dimen.margin_12))
            constrainWidth(viewRight.id, MATCH_CONSTRAINT)
            constrainHeight(viewRight.id, WRAP_CONTENT)
            applyTo(this@buildVerticalDividedLayout)
        }
    }
}

