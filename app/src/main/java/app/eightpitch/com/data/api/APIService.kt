package app.eightpitch.com.data.api

import app.eightpitch.com.data.dto.DataValidResponse
import app.eightpitch.com.data.dto.auth.LoginRequestBody
import app.eightpitch.com.data.dto.auth.LoginResponseBody
import app.eightpitch.com.data.dto.changephonenumber.UpdatePhoneRequestBody
import app.eightpitch.com.data.dto.changingpassword.ChangePasswordRequestBody
import app.eightpitch.com.data.dto.changingpassword.VerifyOldPasswordRequestBody
import app.eightpitch.com.data.dto.changingpassword.VerifyOldPasswordResponseBody
import app.eightpitch.com.data.dto.files.FileCreationResponse
import app.eightpitch.com.data.dto.filter.MinimumInvestmentAmountResponse
import app.eightpitch.com.data.dto.investment.*
import app.eightpitch.com.data.dto.notifications.NotificationsResponseBody
import app.eightpitch.com.data.dto.payment.*
import app.eightpitch.com.data.dto.profilepayments.GetProfilePaymentsResponse
import app.eightpitch.com.data.dto.profilesettings.SubscribeToEmailRequestBody
import app.eightpitch.com.data.dto.profiletransfers.GetProfileTransfersResponse
import app.eightpitch.com.data.dto.project.GetProjectPageResponse
import app.eightpitch.com.data.dto.project.InitiatorProjects
import app.eightpitch.com.data.dto.project.InvestorsProject
import app.eightpitch.com.data.dto.projectdetails.GetProjectResponse
import app.eightpitch.com.data.dto.questionnaire.InvestorQualificationRequestBody
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody
import app.eightpitch.com.data.dto.restorepassword.PasswordRecoverySendInitialSmsCodeResponse
import app.eightpitch.com.data.dto.signup.CommonInvestorRequestBody
import app.eightpitch.com.data.dto.signup.InstitutionalInvestorRequestBody
import app.eightpitch.com.data.dto.signup.RegistrationResponseBody
import app.eightpitch.com.data.dto.sms.*
import app.eightpitch.com.data.dto.twofactor.*
import app.eightpitch.com.data.dto.user.DeleteAccountRequest
import app.eightpitch.com.data.dto.user.LanguageRequestBody
import app.eightpitch.com.data.dto.user.LinkSliderItemResponseBody
import app.eightpitch.com.data.dto.user.TaxInformationBody
import app.eightpitch.com.data.dto.user.UpdateUserPersonalDataRequestBody
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.data.dto.user.User.CREATOR.RoleType
import app.eightpitch.com.data.dto.webid.ActionIdResponseBody
import app.eightpitch.com.data.dto.webid.WebIdUserRequestBody
import app.eightpitch.com.utils.Empty
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

/**
 * Interface for Retrofit usage that
 * supposed to contain an REST api request
 * declarations
 */
interface APIService {
    //region Login
    @POST("user-service/mobile/login")
    fun login(
        @Header("Authorization") authHeader: String,
        @Body loginRequestBody: LoginRequestBody,
    ): Call<LoginResponseBody>

    @POST("user-service/login/phones/{phone}/verify-token")
    fun loginWithVerifyCode(
        @Path("phone") phoneNumber: String,
        @Body phoneVerificationTokenRequestBody: PhoneVerificationTokenRequestBody,
    ): Call<LoginResponseBody>

    @POST("user-service/logout")
    fun logout(): Call<Empty>
    //endregion

    //region UserModel
    @POST("user-service/users")
    fun createUser(@Body partialUser: CommonInvestorRequestBody): Call<RegistrationResponseBody>

    @POST("user-service/users")
    fun createUser(@Body partialUser: InstitutionalInvestorRequestBody): Call<RegistrationResponseBody>

    @GET("user-service/users/me")
    fun getUserData(): Call<User>

    @PUT("user-service/users/language")
    fun setLanguage(@Body body: LanguageRequestBody): Call<Empty>

    @PUT("user-service/users/personal")
    fun updateUserProfileData(@Body body: UpdateUserPersonalDataRequestBody): Call<Void>

    @HTTP(method = "DELETE", path = "user-service/users", hasBody = true)
    fun deleteAccount(@Header("Authorization") authHeader: String, @Body body: DeleteAccountRequest): Call<Void>

    @PUT("user-service/users/settings")
    fun setSubscribeToEmail(@Body body: SubscribeToEmailRequestBody): Call<Void>
    //endregion

    //region SupportModel
    @GET("user-service/emails/{email}/free")
    fun checkIfEmailIsFree(@Path("email") email: String): Call<DataValidResponse>

    @GET("user-service/phones/{phone}/free")
    fun checkIfPhoneIsFree(@Path("phone") phoneNumber: String): Call<DataValidResponse>
    //endregion

    //region SMSModel
    @POST("user-service/mobile/phones/{phone}/send-code-without-recaptcha")
    fun requireAnSMSCodeByPhoneNumber(
        @Header("Authorization") authHeader: String,
        @Path("phone") phone: String,
    ): Call<RequireSMSResponse>

    @POST("user-service/mobile/phones/resend-code-without-recaptcha")
    fun resendSMSCodeByPhone(
        @Header("Authorization") authHeader: String,
        @Body body: InteractionIdRequestBody,
    ): Call<Empty>

    @POST("user-service/mobile/phones/{phone}/send-limit-code")
    fun requireAnLimitSMSCodeByPhoneNumber(
        @Header("Authorization") authHeader: String,
        @Path("phone") phone: String,
    ): Call<SendTokenLimitResponse>

    @POST("user-service/mobile/phones/resend-limit-code")
    fun resendAnLimitSMSCodeByPhone(
        @Header("Authorization") authHeader: String,
        @Body body: InteractionIdRequestBody,
    ): Call<SendTokenLimitResponse>

    @POST("user-service/registration/phones/{phone}/verify-token")
    fun registrationWithVerifyCode(
        @Path("phone") phoneNumber: String,
        @Body phoneVerificationTokenRequestBody: PhoneVerificationTokenRequestBody,
    ): Call<EmailPhoneValidationResponse>

    @POST("user-service/email/send-code")
    fun sendVerifyCodeToEmail(): Call<Empty>

    @POST("user-service/email/resend-code")
    fun resendVerifyCodeToEmail(): Call<Empty>

    @PUT("user-service/email/update")
    fun sendVerifyCodeToNewEmail(@Body body: UpdateEmailRequestBody): Call<Void>

    @POST("user-service/email/verify-token")
    fun verifyEmailCode(@Body emailVerificationTokenRequestBody: EmailVerificationTokenRequestBody): Call<EmailPhoneValidationResponse>
    //endregion

    //webId
    @POST("user-service/webid/form-complete")
    fun getActionId(@Body userRequestBody: WebIdUserRequestBody): Call<ActionIdResponseBody>

    @PUT("user-service/webid/finish-call")
    fun reportFinishedCall(): Call<Empty>
    //endregion

    //Questionnaire
    @POST("user-service/investors/send-questionnaire")
    fun sendQuestionnaire(
        @Query("group") @RoleType role: String,
        @Query("userExternalId") userExternalId: String,
        @Body questionnaireRequestBody: QuestionnaireRequestBody,
    ): Call<Empty>

    @POST("user-service/investors/send-qualification-form")
    fun sendQualificationForm(
        @Query("group") @User.CREATOR.RoleType role: String,
        @Query("userExternalId") userExternalId: String,
        @Body investorQualificationRequestBody: InvestorQualificationRequestBody,
    ): Call<Empty>
    //endregion

    //region Investment flow
    @GET("project-service/investments/{investmentId}/amount")
    fun getInvestmentAmount(@Path("investmentId") investmentId: String): Call<InvestmentAmountResponseBody>

    @POST("project-service/investments/{projectId}/send-project-documents-draft/{investmentId}")
    fun sendDocumentsByEmail(
        @Path("projectId") projectId: String,
        @Path("investmentId") investmentId: String,
    ): Call<Empty>

    @POST("project-service/investments/{projectId}/send-confirmation-code-without-recaptcha/{investmentId}")
    fun requestInvestmentConfirmation(
        @Path("investmentId") investmentId: String,
        @Path("projectId") projectId: String,
    ): Call<SendTokenLimitResponse>

    @PUT("project-service/investments/{projectId}/confirm-investment/{investmentId}")
    fun confirmInvestment(
        @Path("investmentId") investmentId: String,
        @Path("projectId") projectId: String,
        @Body phoneVerificationTokenRequestBody: PhoneVerificationTokenRequestBody,
    ): Call<ConfirmInvestmentResponse>

    @GET("project-service/investments/{projectId}/is-allowed")
    fun isInvestmentAllowed(@Path("projectId") projectId: String): Call<IsInvestmentAllowedResponse>
    //endregion

    //Tax-information
    @POST("user-service/tax-information")
    fun updateTaxInformation(@Body taxInformation: TaxInformationBody): Call<Empty>
    //endregion

    @GET("project-service/project-overview/security-type")
    fun getTypeOfSecurity(
        @Query("projectStatuses") statuses: List<String>
    ): Call<List<String>>


    @GET("project-service/project-overview/minimal-investment")
    fun getMinimalInvestment(
        @Query("projectStatuses") statuses: List<String>
    ): Call<MinimumInvestmentAmountResponse>

    /**
     *Returns all projects
     */
    @GET("project-service/project-overview/paged")
    fun getFilteredProjects(
        @Query("page") page: Int,
        @Query("size") size: Int,
        @Query("sort") sort: String,
        @Query("companyName") companyName: String,
        @Query("minimumInvestmentAmounts") minimumAmounts: List<String>,
        @Query("projectStatuses") statuses: List<String>,
        @Query("typeOfSecurity") typesOfSecurity: List<String>,
    ): Call<GetProjectPageResponse>

    /**
     * Returns projects where the investor has an investments
     * already.
     */
    @GET("project-service/investors/projects")
    fun getInvestorProjects(): Call<List<InvestorsProject>>

    /**
     *Returns all projects
     */
    @GET("project-service/projects/paged")
    fun getProjects(
        @Query("page") page: Int,
        @Query("size") size: Int,
        @Query("sort") sort: String,
    ): Call<GetProjectPageResponse>

    /**
     * Returns projects which were created by this particular initiator.
     */
    @GET("project-service/initiators/projects")
    fun getInitiatorProjects(
        @Query("page") page: Int,
        @Query("size") size: Int,
        @Query("sort") sort: String,
        @Query("status") statuses: List<String>,
    ): Call<InitiatorProjects>

    /**
     * Returns brief information about which project and how much the investor has invested
     */
    @GET("project-service/investors/graph")
    fun getInvestorsGraph(): Call<InvestorGraph>

    /**
     * Returns project details
     */
    @GET("project-service/projects/{projectId}")
    fun getProjectInfo(@Path("projectId") projectId: String): Call<GetProjectResponse>

    @GET("project-service/investments/{projectId}")
    fun getProjectInvestmentInfo(@Path("projectId") projectId: String): Call<ProjectInvestmentInfoResponse>

    @POST("project-service/investments/{projectId}/book")
    fun bookInvestment(
        @Path("projectId") projectId: String,
        @Body currencyInputRequestBody: CurrencyInputRequestBody,
    ): Call<BookInvestmentResponse>

    @PUT("project-service/investments/{projectId}/cancel/{investmentId}")
    fun cancelProjectInvestment(
        @Path("projectId") projectId: String,
        @Path("investmentId") investmentId: String,
    ): Call<Empty>

    @GET("project-service/files/{id}")
    fun downloadFileBy(@Path("id") fileId: String): Call<String>
    //endregion

    //Files
    @Multipart
    @POST("project-service/files/upload")
    fun uploadFile(@Part file: MultipartBody.Part): Call<FileCreationResponse>

    @HTTP(method = "DELETE", path = "project-service/files/{id}")
    fun deleteFile(@Path("id") id: String): Call<Empty>
    //endregion
    /**
     * Gets an info about classing bank payment with hard cap
     */
    @GET("project-service/classic-bank-transfer/payments/manual-bank-transfer-info/{investmentId}")
    fun getClassicHardCapPaymentInfoBy(@Path("investmentId") id: String): Call<BankPaymentsDataResponse>

    /**
     * Gets an info about classing bank payment with soft cap
     */
    @GET("project-service/classic-bank-transfer/payments/secupay-bank-transfer-info/{investmentId}")
    fun getClassicSoftCapPaymentInfoBy(@Path("investmentId") id: String): Call<BankPaymentsDataResponse>

    /**
     * Confirm bank transfer
     */
    @PUT("project-service/classic-bank-transfer/payments/confirm-bank-transfer/{investmentId}")
    fun confirmBankTransfer(@Path("investmentId") id: String): Call<Void>

    /**
     * Confirm `secupay`-related data
     */
    @POST("project-service/secupay/payments/submit/{investmentId}")
    fun confirmDebitPayment(
        @Path("investmentId") id: String,
        @Body body: SaveSequpayDataRequest,
    ): Call<SaveSequpayDataResponse>


    //region Notifications
    @GET("user-service/users/notifications")
    fun getNotifications(
        @Query("pageNo") pageNumber: Int,
        @Query("pageSize") size: Int,
    ): Call<NotificationsResponseBody>

    @PUT("user-service/users/notifications/{notificationId}")
    fun markNotificationAsRead(@Path("notificationId") notificationId: Int): Call<Empty>
    //endregion

    /**
     * Submit online payment named `Fint`
     */
    @POST("project-service/fintecsystems/payments/submit/{investmentId}")
    fun submitFint(
        @Path("investmentId") investmentId: String,
        @Body fintecsystemsRequest: FintecsystemsRequest,
    ): Call<TransactionInfoResponse>

    /**
     * Submit online payment named `Klarna`
     */
    @POST("project-service/klarna/payments/submit/{investmentId}")
    fun submitKlarna(
        @Path("investmentId") investmentId: String,
        @Body redirectParams: RedirectParams,
    ): Call<TransactionInfoResponse>

    /**
     * Returns investors payments
     */
    @GET("project-service/investors/payments")
    fun getProfilePayments(
        @Query("page") page: Int,
        @Query("size") size: Int,
        @Query("sort") sort: String,
    ): Call<GetProfilePaymentsResponse>

    @GET("project-service/investors/transfers")
    fun getProfileTransfers(
        @Query("page") page: Int,
        @Query("size") size: Int,
    ): Call<GetProfileTransfersResponse>

    //region twoFactorModels
    @POST("user-service/sms-2fa/disable")
    fun disableSmsTwoFactorAuth(
        @Header("Authorization") authHeader: String,
        @Query("group") group: String,
        @Query("userExternalId") userExternalId: String,
        @Body body: SmsTwoFactorRequestBody,
    ): Call<SmsTwoFactorResponse>

    @POST("user-service/sms-2fa/enable")
    fun enableSmsTwoFactorAuth(
        @Header("Authorization") authHeader: String,
        @Body body: SmsTwoFactorRequestBody,
    ): Call<SmsTwoFactorResponse>

    @POST("user-service/2fa/disable")
    fun disableTwoFactorAuth(@Body body: TwoFactorAuthRequestBody): Call<SmsTwoFactorResponse>

    @POST("user-service/2fa/enable-with-access-token")
    fun enableTwoFactorAuth(@Body body: TwoFactorAuthEnableRequestBody): Call<SmsTwoFactorResponse>

    @GET("user-service/2fa/qrcode-data")
    fun getQrCodeForTwoFactorAuth(): Call<TwoFactorQrCodeResponse>

    @POST("user-service/2fa/verify-code")
    fun verifyCodeForTwoFactorAuth(
        @Body body: TwoFactorVerifyCodeRequestBody,
    ): Call<TwoFactorVerifyCodeResponse>
    //region twoFactorModels

    //Changing password
    @PUT("user-service/change-password/change-password")
    fun changePassword(@Body requestBody: ChangePasswordRequestBody): Call<Empty>

    @POST("user-service/change-password/verify-old-password")
    fun verifyOldPassword(@Body requestBody: VerifyOldPasswordRequestBody): Call<VerifyOldPasswordResponseBody>
    //endregion

    //Restore password
    @POST("user-service/password-recovery/send-initial-sms-code")
    fun sendInitialSmsCode(@Body emailBody: UpdateEmailRequestBody): Call<PasswordRecoverySendInitialSmsCodeResponse>

    @POST("user-service/password-recovery/send-email-link")
    fun sendEmailLink(@Body verificationBody: PhoneVerificationTokenRequestBody): Call<EmailPhoneValidationResponse>
    //endregion

    //Change phome number
    @PUT("user-service/users/phone")
    fun changePhoneNumber(@Body updatePhoneRequestBody: UpdatePhoneRequestBody): Call<EmailPhoneValidationResponse>

    //Dashboard slider
    @GET("project-service/link-slider")
    fun getDashboardSliderInfo(@Query("group") group: String): Call<List<LinkSliderItemResponseBody>>
}