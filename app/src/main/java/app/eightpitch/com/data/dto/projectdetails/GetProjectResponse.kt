package app.eightpitch.com.data.dto.projectdetails

import app.eightpitch.com.data.dto.project.AmountOfProjectInvestments
import app.eightpitch.com.data.dto.project.InvestorsProject
import app.eightpitch.com.extensions.NO_TYPE
import com.google.gson.annotations.SerializedName

/**
 * This object contains all information about the projects.
 */
data class GetProjectResponse(
    @SerializedName("additionalFields")
    var additionalFields: AdditionalFields = AdditionalFields(),
    @SerializedName("companyName")
    var companyName: String = "",
    @SerializedName("customAdditionalFields")
    var customAdditionalFields: List<CustomAdditionalField> = arrayListOf(),
    @SerializedName("domain")
    var domain: String = "",
    @SerializedName("financialInformation")
    var financialInformation: FinancialInformation = FinancialInformation(),
    @SerializedName("geoipRestrictions")
    var geoipRestrictions: List<String> = arrayListOf(),
    @SerializedName("graph")
    var graph: AmountOfProjectInvestments = AmountOfProjectInvestments(softCap = null),
    @SerializedName("id")
    var id: String = "",
    @SerializedName("legalForm")
    var legalForm: String = "",
    @SerializedName("paymentConfiguration")
    var paymentConfiguration: PaymentConfiguration = PaymentConfiguration(),
    @SerializedName("possibilityAccept")
    var possibilityAccept: Boolean = false,
    @SerializedName("projectDescription")
    var projectDescription: String = "",
    @SerializedName("projectExternalId")
    var projectExternalId: String = "",
    @SerializedName("projectFiles")
    var projectFiles: List<ProjectsFile> = arrayListOf(),
    @SerializedName("projectInitiator")
    var projectInitiator: ProjectInitiator = ProjectInitiator(),
    @SerializedName("projectInitiatorExternalId")
    var projectInitiatorExternalId: String = "",
    @SerializedName("projectPage")
    var projectPage: ProjectPage = ProjectPage(),
    @SerializedName("projectStatus")
    @InvestorsProject.Companion.ProjectStatus
    var projectStatus: String = NO_TYPE,
    @SerializedName("tokenId")
    var tokenId: String = "",
    @SerializedName("tokenParametersDocument")
    var tokenParametersDocument: TokenParametersDocument = TokenParametersDocument(),
    @SerializedName("whitelistedUserExternalIds")
    var whitelistedUserExternalIds: List<String> = arrayListOf()
)