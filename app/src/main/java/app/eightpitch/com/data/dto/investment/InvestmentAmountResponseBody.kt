package app.eightpitch.com.data.dto.investment

import android.os.*
import app.eightpitch.com.extensions.readBigDecimal
import java.math.BigDecimal

data class InvestmentAmountResponseBody(
    val amount: Int = 0,
    val fee: BigDecimal = BigDecimal.ZERO
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readBigDecimal()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(amount)
        parcel.writeSerializable(fee)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<InvestmentAmountResponseBody> {
        override fun createFromParcel(parcel: Parcel): InvestmentAmountResponseBody {
            return InvestmentAmountResponseBody(parcel)
        }

        override fun newArray(size: Int): Array<InvestmentAmountResponseBody?> {
            return arrayOfNulls(size)
        }
    }
}