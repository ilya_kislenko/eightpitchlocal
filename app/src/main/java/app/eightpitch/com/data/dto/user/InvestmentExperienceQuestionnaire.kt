package app.eightpitch.com.data.dto.user

import android.os.*
import app.eightpitch.com.extensions.readStringSafe
import com.google.gson.annotations.SerializedName

data class InvestmentExperienceQuestionnaire(
    @SerializedName("experience")
    val experience: String = "",
    @SerializedName("experienceDuration")
    val experienceDuration: String = "",
    @SerializedName("investingAmount")
    val investingAmount: String = "",
    @SerializedName("investingFrequency")
    val investingFrequency: String = "",
    @SerializedName("knowledge")
    val knowledge: ArrayList<String>  = arrayListOf(),
    @SerializedName("professionalExperienceSource")
    val professionalExperienceSource: String = "",
    @SerializedName("virtualCurrenciesUsed")
    val virtualCurrenciesUsed: String = ""
): Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readArrayList(InvestmentExperienceQuestionnaire::class.java.classLoader!!) as? ArrayList<String> ?: arrayListOf(),
        parcel.readStringSafe(),
        parcel.readStringSafe()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(experience)
        parcel.writeString(experienceDuration)
        parcel.writeString(investingAmount)
        parcel.writeString(investingFrequency)
        parcel.writeString(professionalExperienceSource)
        parcel.writeString(virtualCurrenciesUsed)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<InvestmentExperienceQuestionnaire> {
        override fun createFromParcel(parcel: Parcel): InvestmentExperienceQuestionnaire {
            return InvestmentExperienceQuestionnaire(parcel)
        }

        override fun newArray(size: Int): Array<InvestmentExperienceQuestionnaire?> {
            return arrayOfNulls(size)
        }
    }
}