package app.eightpitch.com.data.dto.user

import com.google.gson.annotations.SerializedName

data class DeleteAccountRequest(
    @SerializedName("reason")
    val reason: String
)