package app.eightpitch.com.data.dto.twofactor

import app.eightpitch.com.extensions.orDefaultValue
import com.google.gson.annotations.SerializedName

data class TwoFactorTokenResponse(
    @SerializedName("accessToken")
    val _accessToken: String? = "",
    @SerializedName("currentTime")
    val _currentTime: String? = "",
    @SerializedName("refreshToken")
    val _refreshToken: String? = ""
){
    val accessToken: String
        get() = _accessToken.orDefaultValue("")
    val currentTime: String
        get() = _currentTime.orDefaultValue("")
    val refreshToken: String
        get() = _refreshToken.orDefaultValue("")
}