package app.eightpitch.com.data.dto.dynamicviews.mainfacts

/**
 * Class for main facts representation, it's kind of an additional information for the project
 * @see app.eightpitch.com.data.dto.projectdetails.AdditionalFields
 * @see FinancialInformationItem
 */
data class MainFact(
    val label: Int = 0,
    val description: String = "",
    val icon: Int = 0
)