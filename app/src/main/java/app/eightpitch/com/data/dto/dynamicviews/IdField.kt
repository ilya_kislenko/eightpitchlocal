package app.eightpitch.com.data.dto.dynamicviews

import android.os.*
import app.eightpitch.com.extensions.*
import com.google.gson.annotations.SerializedName

open class IdField(
    @SerializedName("id")
    val _id: String? = ""
) : Parcelable {

    val id
        get() = _id.orDefaultValue("")

    constructor(parcel: Parcel) : this(parcel.readStringSafe())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<IdField> {
        override fun createFromParcel(parcel: Parcel): IdField {
            return IdField(parcel)
        }

        override fun newArray(size: Int): Array<IdField?> {
            return arrayOfNulls(size)
        }
    }
}