package app.eightpitch.com.data.dto.dynamicviews

import android.os.Parcel
import android.os.Parcelable
import app.eightpitch.com.extensions.orDefaultValue
import app.eightpitch.com.extensions.readListSafe
import app.eightpitch.com.extensions.readStringSafe
import com.google.gson.annotations.SerializedName

/**
 * Class that represents a `Project Details` screen tab.
 * @param name - it's a tab name
 * @param blocks - it's a list of the backend entities that are related with the tab's UI
 */
data class ProjectPageTab(
    @SerializedName("id")
    private val _id: String? = "",
    @SerializedName("name")
    private val _name: String? = "",
    @SerializedName("blocks")
    private val _blocks: List<ProjectPageBlock>? = listOf(),
    @SerializedName("addSimpleBlockMenuData")
    private val _addSimpleBlockMenuData: List<String>? = listOf(),
    @SerializedName("payload")
    private val _payload: String? = ""
) : Parcelable {

    val id: String
        get() = _id.orDefaultValue("")
    val name: String
        get() = _name.orDefaultValue("")
    val blocks: List<ProjectPageBlock>
        get() = _blocks.orDefaultValue(listOf())
    val payload: String
        get() = _payload.orDefaultValue("")
    val addSimpleBlockMenuData: List<String>
        get() = _addSimpleBlockMenuData.orDefaultValue(listOf())

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readListSafe(listOf(), ProjectPageTab::class.java.classLoader!!),
        parcel.readListSafe(listOf(), ProjectPageTab::class.java.classLoader!!),
        parcel.readStringSafe())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeList(blocks)
        parcel.writeList(addSimpleBlockMenuData)
        parcel.writeString(payload)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<ProjectPageTab> {
        override fun createFromParcel(parcel: Parcel): ProjectPageTab {
            return ProjectPageTab(parcel)
        }

        override fun newArray(size: Int): Array<ProjectPageTab?> {
            return arrayOfNulls(size)
        }
    }
}