package app.eightpitch.com.data.dto.dynamicviews

import android.os.Parcel
import android.os.Parcelable
import app.eightpitch.com.extensions.readStringSafe

/**
 * Data for chart UI presenting of this type
 * @see com.github.mikephil.charting.charts.PieChart
 */
data class DoughnutData(
    val label: String = "",
    val progress: Float = 0f,
    val firstHashColor: String = "#F54C4C",
    val secondHashColor: String = "#F54C4C"
): Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readFloat(),
        parcel.readStringSafe(),
        parcel.readStringSafe()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(label)
        parcel.writeFloat(progress)
        parcel.writeString(firstHashColor)
        parcel.writeString(secondHashColor)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<DoughnutData> {
        override fun createFromParcel(parcel: Parcel): DoughnutData {
            return DoughnutData(parcel)
        }

        override fun newArray(size: Int): Array<DoughnutData?> {
            return arrayOfNulls(size)
        }
    }
}