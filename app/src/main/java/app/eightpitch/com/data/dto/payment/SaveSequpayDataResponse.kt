package app.eightpitch.com.data.dto.payment

import com.google.gson.annotations.SerializedName

data class SaveSequpayDataResponse(
    @SerializedName("success")
    val success: Boolean = false
)