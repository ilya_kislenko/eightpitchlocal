package app.eightpitch.com.data.dto.profiletransfers

import app.eightpitch.com.data.dto.project.Pageable
import app.eightpitch.com.data.dto.project.Sort
import com.google.gson.annotations.SerializedName

data class GetProfileTransfersResponse(
    @SerializedName("content")
    var content: List<TransferDetails> = arrayListOf(),
    @SerializedName("empty")
    var empty: Boolean = true,
    @SerializedName("first")
    var first: Boolean? = null,
    @SerializedName("hasContent")
    var hasContent: Boolean? = null,
    @SerializedName("hasNext")
    var hasNext: Boolean? = null,
    @SerializedName("hasPrevious")
    var hasPrevious: Boolean? = null,
    @SerializedName("last")
    var last: Boolean? = null,
    @SerializedName("nextPageable")
    var nextPageable: String? = null,
    @SerializedName("number")
    var number: Int? = null,
    @SerializedName("numberOfElements")
    var numberOfElements: Int? = null,
    @SerializedName("pageable")
    var pageable: Pageable? = null,
    @SerializedName("previousPageable")
    var previousPageable: String? = null,
    @SerializedName("size")
    var size: Int? = null,
    @SerializedName("sort")
    var sort: Sort? = null,
    @SerializedName("totalElements")
    var totalElements: Int? = null,
    @SerializedName("totalPages")
    var totalPages: Int? = null,
)