package app.eightpitch.com.data.dto.auth

import androidx.annotation.StringDef
import app.eightpitch.com.extensions.NO_TYPE
import app.eightpitch.com.extensions.orDefaultValue
import com.google.gson.annotations.SerializedName

data class LoginResponseBody(
    @SerializedName("interactionId")
    private val _interactionId: String?,
    @SerializedName("remainingAttempts")
    private val _remainingAttempts: Int?,
    @SerializedName("result")
    @LoginResult private val _result: String?,
    @SerializedName("tokenResponse")
    private val _tokenResponse: TokenResponse?,
    @SerializedName("valid")
    private val _isValid: Boolean?,
    @SerializedName("validationResult")
    @ValidationResult private val _validationResult: String?,
    @SerializedName("verificationTypes")
    @VerificationType val _verificationTypes: ArrayList<String>?
) {
    val interactionId: String
        get() = _interactionId.orDefaultValue("")
    val remainingAttempts: Int
        get() = _remainingAttempts.orDefaultValue(0)
    @LoginResult val result: String
        get() = _result.orDefaultValue("")
    val tokenResponse: TokenResponse
        get() = _tokenResponse.orDefaultValue(TokenResponse())
    val isValid: Boolean
        get() = _isValid.orDefaultValue(false)
    @ValidationResult val validationResult: String
        get() = _validationResult.orDefaultValue("")
    @VerificationType val verificationTypes: ArrayList<String>
        get() = if (_verificationTypes.isNullOrEmpty()) arrayListOf() else _verificationTypes

    companion object {

        private const val PHONE_VERIFICATION = "PHONE_VERIFICATION"
        private const val TWO_FACTOR_AUTH_VERIFICATION = "TWO_FACTOR_AUTH_VERIFICATION "

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(PHONE_VERIFICATION, TWO_FACTOR_AUTH_VERIFICATION)
        annotation class VerificationType

        const val SUCCESS = "SUCCESS"
        const val UNSUCCESS = "UNSUCCESS"
        const val INACTIVE = "INACTIVE"

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(SUCCESS, UNSUCCESS, INACTIVE, NO_TYPE)
        annotation class LoginResult

        const val EXPIRED = "EXPIRED"
        const val INVALID = "INVALID"
        const val VALID = "VALID"

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(EXPIRED, INVALID, VALID, NO_TYPE)
        annotation class ValidationResult
    }
}