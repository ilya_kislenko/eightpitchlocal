package app.eightpitch.com.data.dto.questionnaire

import android.view.*
import androidx.appcompat.widget.*
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.R
import app.eightpitch.com.data.dto.files.FileMetaData
import app.eightpitch.com.extensions.inflateView
import app.eightpitch.com.ui.recycleradapters.BindableRecyclerAdapter

class UploadedFilesAdapter :
    BindableRecyclerAdapter<FileMetaData, UploadedFilesAdapter.UploadedFilesViewHolder>() {

    lateinit var deleteFileCallback: (position: Int) -> Unit

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        UploadedFilesViewHolder(
            inflateView(
                parent,
                R.layout.item_uploaded_file
            )
        )

    override fun onBindViewHolder(holder: UploadedFilesViewHolder, position: Int) {
        val content = dataList[position]
        holder.apply {
            nameTextView.text = content.displayName
            fileSizeTextView.text = content.fileSizeToText()
            trashcanIcon.apply {
                tag = position
                setOnClickListener {
                    (tag as? Int)?.let {
                        if (::deleteFileCallback.isInitialized)
                            deleteFileCallback.invoke(it)
                        dataList.removeAt(it)
                    }
                }
            }
        }
    }

    class UploadedFilesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameTextView: AppCompatTextView = itemView.findViewById(R.id.nameTextView)
        val fileSizeTextView: AppCompatTextView = itemView.findViewById(R.id.fileSizeTextView)
        val trashcanIcon: AppCompatImageView = itemView.findViewById(R.id.trashcanIcon)
    }
}