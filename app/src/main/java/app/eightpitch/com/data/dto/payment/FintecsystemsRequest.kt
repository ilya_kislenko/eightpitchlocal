package app.eightpitch.com.data.dto.payment

data class FintecsystemsRequest(
    val redirectParams: RedirectParams = RedirectParams()
)