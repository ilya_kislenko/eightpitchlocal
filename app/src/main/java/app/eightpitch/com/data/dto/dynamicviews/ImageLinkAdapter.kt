package app.eightpitch.com.data.dto.dynamicviews

import android.view.*
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.view.isVisible
import androidx.lifecycle.*
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.R
import app.eightpitch.com.extensions.inflateView

class ImageLinkAdapter(private val items: List<ImageLink>)
    : RecyclerView.Adapter<ImageLinkAdapter.ImageHolder>(), ParentLayoutHolder {

    private val result = MutableLiveData<ViewGroup>()
    internal fun getResult(): LiveData<ViewGroup> = result

    private var parentViewGroup: ViewGroup? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageHolder {
        val view = inflateView(parent, R.layout.item_image_link)
        parentViewGroup = view as? ViewGroup
        return ImageHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ImageHolder, position: Int) {
        val item = items[position]
        holder.apply {
            backgroundImage.tag = ImageUIElementArguments("", item.imageUrl)
            if (item.link.isEmpty())
                imageLink.isVisible = false
            else {
                imageLink.run {
                    tag = ClickableUIElementArguments("", item.link)
                    isVisible = true
                }
            }
            parentViewGroup?.let {
                result.value = it
            }
        }
    }

    class ImageHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val backgroundImage: AppCompatImageView = itemView.findViewById(R.id.backgroundImage)
        val imageLink: AppCompatImageView = itemView.findViewById(R.id.linkView)
    }

    override fun getParentLayout(): ViewGroup? = parentViewGroup
}

