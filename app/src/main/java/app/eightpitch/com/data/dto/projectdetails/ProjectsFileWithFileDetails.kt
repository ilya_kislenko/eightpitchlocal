package app.eightpitch.com.data.dto.projectdetails

import android.os.Parcel
import android.os.Parcelable
import app.eightpitch.com.extensions.readStringSafe
import com.google.gson.annotations.SerializedName


data class ProjectsFileWithFileDetails(
    @SerializedName("customFileName")
    var customFileName: String = "",
    @SerializedName("id")
    var id: String = "",
    @SerializedName("originalFileName")
    var originalFileName: String = "",
    @SerializedName("uniqueFileName")
    var uniqueFileName: String = ""
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(customFileName)
        parcel.writeString(id)
        parcel.writeString(originalFileName)
        parcel.writeString(uniqueFileName)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<ProjectsFileWithFileDetails> {
        override fun createFromParcel(parcel: Parcel): ProjectsFileWithFileDetails {
            return ProjectsFileWithFileDetails(parcel)
        }

        override fun newArray(size: Int): Array<ProjectsFileWithFileDetails?> {
            return arrayOfNulls(size)
        }
    }
}