package app.eightpitch.com.data.dto.changingpassword

class VerifyOldPasswordRequestBody(
    val password: String = "",
    val userExternalId: String = ""
)