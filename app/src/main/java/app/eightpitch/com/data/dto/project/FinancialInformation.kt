package app.eightpitch.com.data.dto.project

import com.google.gson.annotations.SerializedName

/**
 * The class contains information about the type of security of the project,
 * the investment series of the project and the total amount of investments for the project.
 */
class FinancialInformation(
    @SerializedName("investmentSeries")
    var investmentSeries: String = "",
    @SerializedName("nominalValue")
    var nominalValue: String = "0",
    @SerializedName("typeOfSecurity")
    var typeOfSecurity: String = ""
)