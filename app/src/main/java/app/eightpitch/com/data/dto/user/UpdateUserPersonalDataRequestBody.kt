package app.eightpitch.com.data.dto.user

import com.google.gson.annotations.SerializedName

data class UpdateUserPersonalDataRequestBody(
    @SerializedName("accountOwner")
    var accountOwner: String? = null,
    @SerializedName("address")
    var address: UserAddress? = null,
    @SerializedName("companyName")
    var companyName: String? = null,
    @SerializedName("companyType")
    var companyType: String? = null,
    @SerializedName("commercialRegisterNumber")
    var commercialRegisterNumber: String? = null,
    @SerializedName("dateOfBirth")
    var dateOfBirth: String? = null,
    @SerializedName("firstName")
    var firstName: String? = null,
    @SerializedName("iban")
    var iban: String? = null,
    @SerializedName("lastName")
    var lastName: String? = null,
    @SerializedName("nationality")
    var nationality: String? = null,
    @SerializedName("placeOfBirth")
    var placeOfBirth: String? = null,
    @SerializedName("politicallyExposedPerson")
    var politicallyExposedPerson: Boolean? = null,
    @SerializedName("title")
    var prefix: String? = null,
    @SerializedName("usTaxLiability")
    var usTaxLiability: Boolean? = null,
    @SerializedName("taxInformation")
     val taxInformation: TaxInformationBody? = null,
)