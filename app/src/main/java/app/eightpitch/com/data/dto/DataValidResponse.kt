package app.eightpitch.com.data.dto

import app.eightpitch.com.extensions.orDefaultValue
import com.google.gson.annotations.SerializedName

data class DataValidResponse(
    @SerializedName("valid")
    private val _isDataValid: Boolean?
) {
    val isDataValid: Boolean
        get() = _isDataValid.orDefaultValue(false)
}