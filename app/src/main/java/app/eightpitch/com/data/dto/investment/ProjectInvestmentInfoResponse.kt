package app.eightpitch.com.data.dto.investment

import android.os.*
import app.eightpitch.com.extensions.*
import app.eightpitch.com.utils.Constants.DEFAULT_DATA_PATTERN
import app.eightpitch.com.utils.DateUtils.parseByFormat
import java.math.BigDecimal
import java.math.BigDecimal.ZERO

data class ProjectInvestmentInfoResponse(
    val companyName: String = "",
    val companyAddress: String = "",
    val isin: String = "",
    val shortCut: String = "",
    val typeOfSecurity: String = "",
    val investmentSeries: String = "",
    val financingPurpose: String = "",
    val startTime: String = "",
    val endTime: String = "",
    val nominalValue: BigDecimal = BigDecimal(1),
    val investmentStepSize: Long = 1,
    val assetBasedFees: BigDecimal = ZERO,
    val investorClassLimit: BigDecimal? = null,
    val minimumInvestmentAmount: Long = 1L,
    val maximumInvestmentAmount: BigDecimal = BigDecimal(20),
    val unprocessedInvestment: UnprocessedInvestment = UnprocessedInvestment(),
) : Parcelable {

    val startDate: Long
        get() = startTime.parseByFormat(pattern = DEFAULT_DATA_PATTERN)

    val endDate: Long
        get() = endTime.parseByFormat(pattern = DEFAULT_DATA_PATTERN)

    /**
     * This field isn't related with the backend fields,
     * we're appending projectName only in the case of
     * simplification for transmitting this value during the
     * investment/payments flow
     */
    private var projectName: String? = null

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readBigDecimal(),
        parcel.readLong(),
        parcel.readBigDecimal(),
        parcel.readBigDecimal(),
        parcel.readLong(),
        parcel.readBigDecimal(),
        parcel.readParcelable(UnprocessedInvestment::class.java.classLoader) ?: UnprocessedInvestment()) {
        projectName = parcel.readString()
    }

    internal fun setProjectName(name: String?) {
        projectName = name.orDefaultValue("")
    }

    internal fun getProjectName(): String = projectName.orDefaultValue("")
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(companyName)
        parcel.writeString(companyAddress)
        parcel.writeString(isin)
        parcel.writeString(shortCut)
        parcel.writeString(typeOfSecurity)
        parcel.writeString(investmentSeries)
        parcel.writeString(financingPurpose)
        parcel.writeString(startTime)
        parcel.writeString(endTime)
        parcel.writeLong(investmentStepSize)
        parcel.writeLong(minimumInvestmentAmount)
        parcel.writeParcelable(unprocessedInvestment, flags)
        parcel.writeString(projectName)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<ProjectInvestmentInfoResponse> {
        override fun createFromParcel(parcel: Parcel): ProjectInvestmentInfoResponse {
            return ProjectInvestmentInfoResponse(parcel)
        }

        override fun newArray(size: Int): Array<ProjectInvestmentInfoResponse?> {
            return arrayOfNulls(size)
        }
    }

}