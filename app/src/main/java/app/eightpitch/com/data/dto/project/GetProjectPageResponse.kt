package app.eightpitch.com.data.dto.project

import com.google.gson.annotations.SerializedName

/**
 * The class describes the page model when
 * loading the paginated list of all projects
 */
class GetProjectPageResponse(
    @SerializedName("content")
    var content: List<ProjectDetails> = arrayListOf(),
    @SerializedName("empty")
    var empty: Boolean = true,
    @SerializedName("first")
    var first: Boolean = true,
    @SerializedName("last")
    var last: Boolean = false,
    @SerializedName("number")
    var number: Int = 0,
    @SerializedName("numberOfElements")
    var numberOfElements: Int = 0,
    @SerializedName("pageable")
    var pageable: Pageable? = null,
    @SerializedName("size")
    var size: Int = 0,
    @SerializedName("sort")
    var sort: Sort? = null,
    @SerializedName("totalElements")
    var totalElements: Int = 0,
    @SerializedName("totalPages")
    var totalPages: Int = 0
)