package app.eightpitch.com.data.dto.dynamicviews

import android.os.Parcel
import android.os.Parcelable
import app.eightpitch.com.data.dto.dynamicviews.language.I18nData
import app.eightpitch.com.extensions.readListSafe
import com.google.gson.annotations.SerializedName

typealias LocalFileIdToBackendFileId = HashMap<String, String>

/**
 * Class that represents a `Project Details` screen UI,
 * which contains a amount of tabs and additional tabs info, list of objects
 * which corresponds with a UI of the particular tab.
 */

data class ProjectDetailsTabsBlock(
    val tabs: List<ProjectPageTab> = listOf(),
    val i18nData: I18nData = I18nData(),
    @SerializedName("fileIdMap")
    val localIdToBackendIdMap: LocalFileIdToBackendFileId = hashMapOf() // localId -> fileId
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readListSafe(listOf(), ProjectDetailsTabsBlock::class.java.classLoader!!),
        parcel.readParcelable(I18nData::class.java.classLoader) ?: I18nData(),
        parcel.readHashMap(ProjectDetailsTabsBlock::class.java.classLoader!!) as? HashMap<String, String>
        ?: hashMapOf())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(tabs)
        parcel.writeParcelable(i18nData, flags)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<ProjectDetailsTabsBlock> {
        override fun createFromParcel(parcel: Parcel): ProjectDetailsTabsBlock {
            return ProjectDetailsTabsBlock(parcel)
        }

        override fun newArray(size: Int): Array<ProjectDetailsTabsBlock?> {
            return arrayOfNulls(size)
        }
    }
}