package app.eightpitch.com.data.dto.sms

import com.google.gson.annotations.SerializedName

data class EmailVerificationTokenRequestBody(
    @SerializedName("token")
    val code: String
)