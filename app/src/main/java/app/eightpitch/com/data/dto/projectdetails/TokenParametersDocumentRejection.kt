package app.eightpitch.com.data.dto.projectdetails

import com.google.gson.annotations.SerializedName

class TokenParametersDocumentRejection(
    @SerializedName("comment")
    var comment: String = "",
    @SerializedName("createDate")
    var createDate: String = "",
    @SerializedName("id")
    var id: Long = -1,
    @SerializedName("updateDate")
    var updateDate: String = ""
)