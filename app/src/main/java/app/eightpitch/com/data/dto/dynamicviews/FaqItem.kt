package app.eightpitch.com.data.dto.dynamicviews

import android.os.Parcel
import android.os.Parcelable
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.extensions.orDefaultValue
import app.eightpitch.com.extensions.readStringSafe
import app.eightpitch.com.ui.dynamicdetails.DynamicProjectDetailsChildFragment
import com.google.gson.annotations.SerializedName

/**
 * It's an object that represents a single [RecyclerView]'s item
 * on a [DynamicProjectDetailsChildFragment].
 */
data class FaqItem(
    @SerializedName("question")
    private val _question: String? = "",
    @SerializedName("answer")
    private val _answer: String? = ""
) : Parcelable {

    val question: String
        get() = _question.orDefaultValue("")
    val answer: String
        get() = _answer.orDefaultValue("")

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(question)
        parcel.writeString(answer)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<FaqItem> {
        override fun createFromParcel(parcel: Parcel): FaqItem {
            return FaqItem(parcel)
        }

        override fun newArray(size: Int): Array<FaqItem?> {
            return arrayOfNulls(size)
        }
    }
}