package app.eightpitch.com.data

import android.content.Context
import android.net.ConnectivityManager
import app.eightpitch.com.R
import app.eightpitch.com.data.transmit.*
import app.eightpitch.com.utils.Logger
import kotlinx.coroutines.*
import java.io.IOException
import kotlinx.coroutines.coroutineScope
import java.lang.IllegalStateException
import javax.inject.Inject

class ThreadsSeparator @Inject constructor(private val appContext: Context) {

    private fun hasNetworkConnection(context: Context): Boolean {

        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return connectivityManager.activeNetworkInfo != null
    }

    internal suspend fun <T> startRequest(
        request: suspend () -> T?
    ): Result<T> {

        return withContext(Dispatchers.IO) {

            try {

                if (hasNetworkConnection(appContext)) {
                    val result = request()
                    Result(
                        ResultStatus.SUCCESS,
                        result
                    )
                } else {
                    Result(
                        ResultStatus.FAILED,
                        exception = AbsentNetworkConnectionException(appContext.getString(R.string.no_internet_connection_message))
                    )
                }

            } catch (exception: Exception) {
                Logger.logError(this::class.java.simpleName, "Started request was break down with: ", exception)
                Result<T>(
                    ResultStatus.FAILED,
                    exception = when (exception) {
                        is IOException -> AbsentNetworkConnectionException(appContext.getString(R.string.no_internet_connection_message))
                        else -> exception
                    }
                )
            }
        }
    }

    class AbsentNetworkConnectionException(message: String) : Exception(message)

    internal suspend fun <T> asyncIO(block: suspend () -> T) = coroutineScope {
        withContext(Dispatchers.IO) {
            block()
        }
    }

    internal suspend fun <T> asyncIOWithResult(block: suspend () -> T): Result<T> = coroutineScope {

        withContext(Dispatchers.IO) {
            try {
                Result(ResultStatus.SUCCESS, block())
            } catch (throwable: Throwable) {
                val message = "Async operation finished with exception"
                Logger.logError(ThreadsSeparator::class.java.simpleName, "$message = $throwable")
                Result<T>(
                    ResultStatus.FAILED,
                    exception = if (throwable is Exception) throwable else IllegalStateException(message)
                )
            }
        }
    }
}