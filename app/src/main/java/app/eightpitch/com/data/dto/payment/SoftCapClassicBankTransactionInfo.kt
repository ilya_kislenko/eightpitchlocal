package app.eightpitch.com.data.dto.payment

import androidx.annotation.StringDef
import app.eightpitch.com.extensions.NO_TYPE
import com.google.gson.annotations.SerializedName

data class SoftCapClassicBankTransactionInfo(
    @SerializedName("referenceNumber")
    val referenceNumber: String = "",
    @SerializedName("responseStatus")
    @ResponseStatus val responseStatus: String = NO_TYPE
) {

    companion object {

        const val RS_OK = "OK"
        const val PAYMENT_GATEWAY_ERROR = "PAYMENT_GATEWAY_ERROR "

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(NO_TYPE, RS_OK, PAYMENT_GATEWAY_ERROR)
        annotation class ResponseStatus
    }
}