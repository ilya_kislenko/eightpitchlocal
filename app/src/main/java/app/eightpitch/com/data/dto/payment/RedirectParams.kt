package app.eightpitch.com.data.dto.payment

import app.eightpitch.com.utils.Constants.ONLINE_ABORT_URL
import app.eightpitch.com.utils.Constants.ONLINE_CONFIRM_URL

data class RedirectParams(
    val abortUrl: String = ONLINE_ABORT_URL,
    val successUrl: String = ONLINE_CONFIRM_URL
)