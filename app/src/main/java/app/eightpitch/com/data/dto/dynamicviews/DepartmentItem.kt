package app.eightpitch.com.data.dto.dynamicviews

import android.os.Parcel
import android.os.Parcelable
import app.eightpitch.com.extensions.orDefaultValue
import app.eightpitch.com.extensions.readStringSafe
import com.google.gson.annotations.SerializedName

/**
 * One of project dynamic constructor objects. Consist of photo, some info, and linkedIn reference.
 * Looks like a card
 * @see androidx.cardview.widget.CardView
 */
data class DepartmentItem(
    @SerializedName("fullName")
    private val _fullName: String? = "",
    @SerializedName("position")
    private val _position: String? = "",
    @SerializedName("linkedinUrl")
    private val _linkedinUrl: String? = "",
    @SerializedName("imageUrl")
    private val _imageUrl: String? = "",
    @SerializedName("imageFileName")
    private val _imageFileName: String? = ""
) : Parcelable {

    val fullName: String
        get() = _fullName.orDefaultValue("")
    val position: String
        get() = _position.orDefaultValue("")
    val linkedinUrl: String
        get() = _linkedinUrl.orDefaultValue("")
    val imageUrl: String
        get() = _imageUrl.orDefaultValue("")
    val imageFileName: String
        get() = _imageFileName.orDefaultValue("")

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(fullName)
        parcel.writeString(position)
        parcel.writeString(linkedinUrl)
        parcel.writeString(imageUrl)
        parcel.writeString(imageFileName)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<DepartmentItem> {
        override fun createFromParcel(parcel: Parcel): DepartmentItem {
            return DepartmentItem(parcel)
        }

        override fun newArray(size: Int): Array<DepartmentItem?> {
            return arrayOfNulls(size)
        }
    }
}