package app.eightpitch.com.data.dto.dynamicviews

import android.graphics.*
import android.view.*
import androidx.appcompat.widget.*
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.*
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.R
import app.eightpitch.com.extensions.inflateView

class TabletTiledImageAdapter(
    private val items: List<Pair<TiledImageItem, TiledImageItem>>,
    private val textAppearanceSet: TextAppearanceSet
) :
    RecyclerView.Adapter<TabletTiledImageAdapter.TiledImageHolder>(), ParentLayoutHolder {

    private val result = MutableLiveData<ViewGroup>()
    internal fun getResult(): LiveData<ViewGroup> = result

    private var constraint: ViewGroup? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TiledImageHolder {
        val view = inflateView(parent, R.layout.item_tablet_tiled_image)
        constraint = view as ViewGroup
        return TiledImageHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: TiledImageHolder, position: Int) {
        val item = items[position]
        holder.apply {
            val first = item.first
            firstBackgroundImage.tag = ImageUIElementArguments("", first.imageId)
            firstDescriptionTextView.also {
                it.text = first.text
                val fontFamily = ResourcesCompat.getFont(it.context, R.font.roboto_regular)
                val (isBold, isItalic, isUnderline) = textAppearanceSet
                it.typeface = Typeface.create(
                    fontFamily,
                    when {
                        isBold && isItalic -> Typeface.BOLD_ITALIC
                        isBold -> Typeface.BOLD
                        isItalic -> Typeface.ITALIC
                        else -> Typeface.NORMAL
                    }
                )
                it.paintFlags = if (isUnderline) Paint.UNDERLINE_TEXT_FLAG else it.paintFlags
            }
            firstHeaderTextView.text = first.header
            constraint?.let { result.value = it }

            val second = item.second
            secondBackgroundImage.tag = ImageUIElementArguments("", second.imageId)
            secondDescriptionTextView.also {
                it.text = second.text
                val fontFamily = ResourcesCompat.getFont(it.context, R.font.roboto_regular)
                val (isBold, isItalic, isUnderline) = textAppearanceSet
                it.typeface = Typeface.create(
                    fontFamily,
                    when {
                        isBold && isItalic -> Typeface.BOLD_ITALIC
                        isBold -> Typeface.BOLD
                        isItalic -> Typeface.ITALIC
                        else -> Typeface.NORMAL
                    }
                )
                it.paintFlags = if (isUnderline) Paint.UNDERLINE_TEXT_FLAG else it.paintFlags
            }
            secondHeaderTextView.text = second.header
            constraint?.let { result.value = it }
        }
    }

    class TiledImageHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val firstBackgroundImage: AppCompatImageView = itemView.findViewById(R.id.firstBackgroundImage)
        val firstDescriptionTextView: AppCompatTextView = itemView.findViewById(R.id.firstDescriptionTextView)
        val firstHeaderTextView: AppCompatTextView = itemView.findViewById(R.id.firstHeaderTextView)
        val secondBackgroundImage: AppCompatImageView = itemView.findViewById(R.id.secondBackgroundImage)
        val secondDescriptionTextView: AppCompatTextView = itemView.findViewById(R.id.secondDescriptionTextView)
        val secondHeaderTextView: AppCompatTextView = itemView.findViewById(R.id.secondHeaderTextView)
    }

    override fun getParentLayout(): ViewGroup? = constraint
}