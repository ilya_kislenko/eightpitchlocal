package app.eightpitch.com.data.dto.project

import app.eightpitch.com.data.dto.projectdetails.ProjectsFile
import com.google.gson.annotations.SerializedName

/**
 * Detailed information about the project
 *
 * @param companyName - Name of the company
 * @param description - Company description
 * @param financialInformation - Information about the financial component of the project
 * @param amountOfProjectInvestments - Information on the amount of existing
 * and required investments
 * @param id - Project id
 * @param projectPageUrl - Link to the web page with the project
 * @param tokenParametersDocument - Information about the beginning and end of the project,
 * the amount of required investments, the minimum investment rate
 * @param videoUrl - id video on vimeo platform
 */
class ProjectDetails(
    @SerializedName("companyName")
    var companyName: String = "",
    @SerializedName("description")
    var description: String = "",
    @SerializedName("financialInformation")
    var financialInformation: FinancialInformation = FinancialInformation(),
    @SerializedName("graph")
    var amountOfProjectInvestments: AmountOfProjectInvestments = AmountOfProjectInvestments(softCap = null),
    @SerializedName("id")
    var id: Int = -1,
    @SerializedName("projectPageUrl")
    var projectPageUrl: String = "",
    @SerializedName("tokenParametersDocument")
    var tokenParametersDocument: TokenParametersDocument = TokenParametersDocument(),
    @SerializedName("videoUrl")
    var videoUrl: String = "",
    @SerializedName("projectPageFiles")
    val projectPageFiles: List<ProjectsFile> = listOf()
)