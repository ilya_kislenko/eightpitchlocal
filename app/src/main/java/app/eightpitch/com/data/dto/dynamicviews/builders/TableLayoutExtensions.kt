package app.eightpitch.com.data.dto.dynamicviews.builders

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.GradientDrawable.RECTANGLE
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import app.eightpitch.com.R

//TODO strokes
fun Context.createTableHeaderCell(isFirstItem: Boolean, headerColor: String) = AppCompatTextView(this).apply {
    setTextAppearance(R.style.Body14RobotoRegular)
    setTextColor(ContextCompat.getColor(this@createTableHeaderCell, R.color.white))
    val headerBackground = GradientDrawable().also {
        it.shape = RECTANGLE
        it.setColor(Color.parseColor(headerColor))
    }
    background = headerBackground
}

fun Context.createTableCell(rowCounter: Int) = AppCompatTextView(this).apply {
    setTextAppearance(R.style.Body12RobotoLight)
    val isEven = rowCounter % 2 == 0
    background = ContextCompat.getDrawable(this@createTableCell, if (isEven) R.drawable.even_stroked_cell_background
    else R.drawable.odd_stroked_cell_background)
}