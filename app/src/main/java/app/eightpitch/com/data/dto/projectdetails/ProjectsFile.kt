package app.eightpitch.com.data.dto.projectdetails

import android.os.*
import androidx.annotation.StringDef
import app.eightpitch.com.extensions.*
import com.google.gson.annotations.SerializedName

data class ProjectsFile(
    @SerializedName("file")
    var projectsFile: ProjectsFileWithFileDetails = ProjectsFileWithFileDetails(),
    @SerializedName("fileType")
    @FileType
    var fileType: String = NO_TYPE,
    @SerializedName("id")
    var id: Long = -1,
    @SerializedName("uniqueFileName")
    var uniqueFileName: String = ""
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readParcelable(ProjectsFileWithFileDetails::class.java.classLoader)
        ?: ProjectsFileWithFileDetails(),
        parcel.readStringSafe(),
        parcel.readLong(),
        parcel.readStringSafe()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(projectsFile, flags)
        parcel.writeString(fileType)
        parcel.writeLong(id)
        parcel.writeString(uniqueFileName)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<ProjectsFile> {

        override fun createFromParcel(parcel: Parcel): ProjectsFile {
            return ProjectsFile(parcel)
        }

        override fun newArray(size: Int): Array<ProjectsFile?> {
            return arrayOfNulls(size)
        }

        const val UPDATES = "UPDATES"
        const val CONTRACT_DOCUMENTS = "CONTRACT_DOCUMENTS"
        const val EIGHT_PITCH_PLATFORM_DOCUMENTS = "EIGHT_PITCH_PLATFORM_DOCUMENTS"
        const val BACKGROUND_IMAGE = "BACKGROUND_IMAGE"
        const val COMPANY_LOGO_B_W = "COMPANY_LOGO_B_W"
        const val COMPANY_LOGO_COLORED = "COMPANY_LOGO_COLORED"
        const val TEAM_PICTURE = "TEAM_PICTURE"
        const val PROJECT_THUMBNAIL = "PROJECT_THUMBNAIL"
        const val CONTRACT_DOCUMENTS_DRAFT = "CONTRACT_DOCUMENTS_DRAFT"

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(
            NO_TYPE, UPDATES, CONTRACT_DOCUMENTS, EIGHT_PITCH_PLATFORM_DOCUMENTS, CONTRACT_DOCUMENTS_DRAFT,
            BACKGROUND_IMAGE, COMPANY_LOGO_B_W, COMPANY_LOGO_COLORED, TEAM_PICTURE, PROJECT_THUMBNAIL
        )
        annotation class FileType
    }
}