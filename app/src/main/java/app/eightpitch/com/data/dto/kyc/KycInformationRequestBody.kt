package app.eightpitch.com.data.dto.kyc

import android.os.Parcel
import android.os.Parcelable
import app.eightpitch.com.extensions.readStringSafe

data class KycInformationRequestBody(
    val lastName: String = "",
    val firstName: String = "",
    val personType: String = "",
    val dateOfBirth: Long = System.currentTimeMillis(),
    val placeOfBirth:String = "",
    val nationality: String = "",
    val politicallyExposedPerson: Boolean = false,
    val uSTaxLiability: Boolean = false,
    val country:String = "",
    val city:String = "",
    val zipCode: String = "",
    val street:String = "",
    val streetNumber: String = "",
    val accountOwner: String = "",
    val theSameLikeMyFullName: Boolean = false,
    val iBan: String = "",
    val rank: String = ""
): Parcelable {

    val fullName
        get() = "$firstName $lastName"

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readLong(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readByte() != 0.toByte(),
        parcel.readStringSafe(),
        parcel.readStringSafe()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(lastName)
        parcel.writeString(firstName)
        parcel.writeString(personType)
        parcel.writeLong(dateOfBirth)
        parcel.writeString(placeOfBirth)
        parcel.writeString(nationality)
        parcel.writeByte(if (politicallyExposedPerson) 1 else 0)
        parcel.writeByte(if (uSTaxLiability) 1 else 0)
        parcel.writeString(country)
        parcel.writeString(city)
        parcel.writeString(zipCode)
        parcel.writeString(street)
        parcel.writeString(streetNumber)
        parcel.writeString(accountOwner)
        parcel.writeByte(if (theSameLikeMyFullName) 1 else 0)
        parcel.writeString(iBan)
        parcel.writeString(rank)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<KycInformationRequestBody> {
        override fun createFromParcel(parcel: Parcel): KycInformationRequestBody {
            return KycInformationRequestBody(parcel)
        }

        override fun newArray(size: Int): Array<KycInformationRequestBody?> {
            return arrayOfNulls(size)
        }
    }
}