package app.eightpitch.com.data.dto.signup

import android.os.Parcel
import android.os.Parcelable
import app.eightpitch.com.data.dto.user.CompanyDataDTO
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.data.dto.user.User.CREATOR.INSTITUTIONAL_INVESTOR
import app.eightpitch.com.data.dto.user.User.CREATOR.RoleType
import app.eightpitch.com.extensions.NO_TYPE
import app.eightpitch.com.extensions.readStringSafe
import com.google.gson.annotations.SerializedName

class InstitutionalInvestorRequestBody(
    @SerializedName("companyData")
    var companyDataDTO: CompanyDataDTO = CompanyDataDTO(),
    @Suppress
    email: String = "",
    deviceId: String = "",
    password: String = "",
    lastName: String = "",
    firstName: String = "",
    phoneNumber: String = "",
    interactionId: String = "",
    commercialRegisterNumber: String = "",
    isSubscribedToEmail: Boolean = false,
    prefix: String = NO_TYPE
) : Parcelable, CommonInvestorRequestBody(
    email, deviceId, password, lastName, firstName, phoneNumber,
    interactionId, commercialRegisterNumber, isSubscribedToEmail, INSTITUTIONAL_INVESTOR, prefix
) {

    override fun copy(
        email: String,
        deviceId: String,
        password: String,
        lastName: String,
        firstName: String,
        phoneNumber: String,
        interactionId: String,
        commercialRegisterNumber: String,
        isSubscribedToEmail: Boolean,
        @RoleType group: String,
        prefix: String): CommonInvestorRequestBody = InstitutionalInvestorRequestBody(
        companyDataDTO, email, deviceId, password, lastName, firstName,
        phoneNumber, interactionId, commercialRegisterNumber, isSubscribedToEmail, prefix
    )

    constructor(parcel: Parcel) : this(
        parcel.readParcelable(CompanyDataDTO::class.java.classLoader) ?: CompanyDataDTO(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readByte() != 0.toByte(),
        parcel.readStringSafe())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        super.writeToParcel(parcel, flags)
        parcel.writeParcelable(companyDataDTO, flags)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<InstitutionalInvestorRequestBody> {
        override fun createFromParcel(parcel: Parcel): InstitutionalInvestorRequestBody {
            return InstitutionalInvestorRequestBody(parcel)
        }

        override fun newArray(size: Int): Array<InstitutionalInvestorRequestBody?> {
            return arrayOfNulls(size)
        }
    }
}