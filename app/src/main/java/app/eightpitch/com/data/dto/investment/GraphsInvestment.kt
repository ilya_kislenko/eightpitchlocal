package app.eightpitch.com.data.dto.investment

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

/**
 * The object contains information and status about investments
 * in those projects in which the investor has already invested.
 */
data class GraphsInvestment(
    @SerializedName("companyName")
    var companyName: String = "",
    @SerializedName("monetaryAmount")
    var monetaryAmount: BigDecimal = BigDecimal.ZERO
)