package app.eightpitch.com.dagger.modules

import app.eightpitch.com.BuildConfig
import app.eightpitch.com.base.preferences.PreferencesReader
import app.eightpitch.com.base.preferences.PreferencesWriter
import app.eightpitch.com.dagger.scopes.ModelsScope
import app.eightpitch.com.data.api.APIService
import app.eightpitch.com.data.api.HeaderlessRequestsContainer
import app.eightpitch.com.utils.models.RefreshTokenModel
import dagger.Module
import dagger.Provides
import okhttp3.*
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Type
import java.util.concurrent.TimeUnit

@Module
class RetrofitModule {

    @ModelsScope
    @Provides
    fun provideRetrofit(
        httpClient: OkHttpClient,
        responseBodyConverterFactory: Converter.Factory
    ): Retrofit =
        Retrofit.Builder()
            .client(httpClient)
            .addConverterFactory(responseBodyConverterFactory)
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BuildConfig.ENDPOINT)
            .build()

    @ModelsScope
    @Provides
    fun provideHttpClient(
        authenticator: Authenticator,
        interceptor: Interceptor
    ): OkHttpClient =
        OkHttpClient.Builder()
            .authenticator(authenticator)
            .addNetworkInterceptor(interceptor)
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(12, TimeUnit.SECONDS)
            .writeTimeout(15, TimeUnit.SECONDS)
            .build()

    @ModelsScope
    @Provides
    fun provideInterceptor(
        reader: PreferencesReader
    ): Interceptor {

        return Interceptor { chain ->

            val token = reader.getCachedAccessToken()

            var request = chain.request()
            if (HeaderlessRequestsContainer.matchToExternalURL(request.url().toString()))
            else request = request.buildWithAnAuthorizationHeader(token)

            chain.proceed(request)
        }
    }

    @ModelsScope
    @Provides
    fun provideAuthenticator(
        preferencesWriter: PreferencesWriter,
        preferencesReader: PreferencesReader,
        refreshTokenModel: RefreshTokenModel
    ): Authenticator {

        return Authenticator { _, response ->

            synchronized(this) {

                val refreshTokenResult = refreshTokenModel.refreshToken(preferencesReader.getCachedRefreshToken())

                preferencesWriter.apply {
                    cacheAccessToken(refreshTokenResult.accessToken)
                    cacheRefreshToken(refreshTokenResult.refreshToken)
                    cacheTokenExpirationTime(1)
                }

                response.request().buildWithAnAuthorizationHeader(preferencesReader.getCachedAccessToken())
            }
        }
    }

    private fun Request.buildWithAnAuthorizationHeader(accessToken: String): Request {
        return this.newBuilder()
            .header("Authorization", "Bearer $accessToken")
            .build()
    }

    /**
     * Converter factory which allows us to declare Call<OurType>, in case of
     * an empty response body it's not more necessary to declare request like Call<Void>,
     * factory will return a null instead of exception, and it helps us just to check
     * that if request was successful an body is empty and we expected OurType and just
     * return an OurType instead of null into system. Without this factory retrofit
     * handles empty body successfully only if expected type was Void and returning a null,
     * but the problem is that we can't create an instance of Void type to return it into the system as
     * expected result.
     */
    @ModelsScope
    @Provides
    fun provideANullOrEmptyFactory(): Converter.Factory {

        return object : Converter.Factory() {
            fun converterFactory() = this
            override fun responseBodyConverter(type: Type, annotations: Array<out Annotation>, retrofit: Retrofit) =
                object : Converter<ResponseBody, Any?> {
                    val nextResponseBodyConverter =
                        retrofit.nextResponseBodyConverter<Any?>(converterFactory(), type, annotations)

                    override fun convert(value: ResponseBody) =
                        if (value.contentLength() != 0L) nextResponseBodyConverter.convert(value) else null
                }
        }
    }

    @ModelsScope
    @Provides
    fun provideAnApiService(retrofit: Retrofit): APIService {
        return retrofit.create(APIService::class.java)
    }
}