package app.eightpitch.com.dagger.modules

import android.content.Context
import app.eightpitch.com.base.MACAddressProvider
import app.eightpitch.com.base.preferences.PreferencesReader
import app.eightpitch.com.base.preferences.PreferencesWriter
import app.eightpitch.com.dagger.scopes.ModelsScope
import app.eightpitch.com.data.api.APIService
import app.eightpitch.com.models.*
import app.eightpitch.com.utils.BasicAuthorization
import app.eightpitch.com.utils.models.RefreshTokenModel
import app.eightpitch.com.utils.webid.WebIdController
import dagger.Module
import dagger.Provides

@Module
class ModelsModule {

    @ModelsScope
    @Provides
    fun provideRefreshTokenModel(
        appContext: Context,
        preferencesWriter: PreferencesWriter,
    ) =
        RefreshTokenModel(appContext, preferencesWriter)

    @ModelsScope
    @Provides
    fun provideAUserModel(
        appContext: Context,
        apiService: APIService,
        preferencesReader: PreferencesReader,
        preferencesWriter: PreferencesWriter,
    ) = UserModel(appContext, apiService, preferencesReader, preferencesWriter)

    @ModelsScope
    @Provides
    fun provideAProjectModel(appContext: Context, apiService: APIService) =
        ProjectModel(appContext, apiService)

    @ModelsScope
    @Provides
    fun provideFilterModel(appContext: Context, apiService: APIService) =
        FilterModel(appContext, apiService)

    @ModelsScope
    @Provides
    fun providePaymentModel(appContext: Context, apiService: APIService) =
        PaymentModel(appContext, apiService)

    @ModelsScope
    @Provides
    fun provideAuthModel(
        appContext: Context,
        basicAuthorization: BasicAuthorization,
        userModel: UserModel,
        apiService: APIService,
        macAddressProvider: MACAddressProvider,
        preferencesWriter: PreferencesWriter,
        preferencesReader: PreferencesReader,
    ) =
        AuthModel(appContext,
            basicAuthorization,
            userModel,
            apiService,
            macAddressProvider,
            preferencesWriter,
            preferencesReader)

    @ModelsScope
    @Provides
    fun provideSupportModel(appContext: Context, apiService: APIService) = SupportModel(appContext, apiService)

    @ModelsScope
    @Provides
    fun provideSMSModel(appContext: Context, basicAuthorization: BasicAuthorization, apiService: APIService) =
        SMSModel(appContext, basicAuthorization, apiService)

    @ModelsScope
    @Provides
    fun provideARegistrationModel(
        appContext: Context,
        userModel: UserModel,
        apiService: APIService,
        preferencesWriter: PreferencesWriter,
        preferencesReader: PreferencesReader,
    ) = RegistrationModel(appContext, userModel, apiService, preferencesWriter, preferencesReader)

    @ModelsScope
    @Provides
    fun provideWebIdController(appContext: Context) = WebIdController(appContext)

    @ModelsScope
    @Provides
    fun provideDownloadModel(appContext: Context, preferencesReader: PreferencesReader) =
        DownloadModel(appContext, preferencesReader)

    @ModelsScope
    @Provides
    fun provideDocumentsModel(appContext: Context, apiService: APIService) = DocumentsModel(appContext, apiService)

    @ModelsScope
    @Provides
    fun provideInvestmentModel(appContext: Context, apiService: APIService) = InvestmentModel(appContext, apiService)

    @ModelsScope
    @Provides
    fun provideNotificationsModel(appContext: Context, apiService: APIService) =
        NotificationsModel(appContext, apiService)

    @ModelsScope
    @Provides
    fun provideAFileModel(appContext: Context, apiService: APIService) = FilesModel(appContext, apiService)
}