package app.eightpitch.com.dagger

import android.content.Context
import app.eightpitch.com.dagger.components.*
import app.eightpitch.com.dagger.modules.*
import java.lang.IllegalStateException

object Injector {

    private lateinit var appComponent: AppComponent

    internal fun initAppComponent(applicationContext: Context) {

        if (!::appComponent.isInitialized)
            appComponent = DaggerAppComponent
                .builder()
                .applicationModule(ApplicationModule(applicationContext))
                .build()
    }

    internal fun getAppComponent() = appComponent

    internal fun buildModelsComponent(): ModelsComponent {

        if (!::appComponent.isInitialized)
            throw IllegalStateException("Can't build a models component until app components is initialized")

        return appComponent.buildModelsSubComponent(RetrofitModule(), ViewModelsModule(),
            UtilsModule(), ModelsModule()
        )
    }
}