package app.eightpitch.com.dagger.components

import app.eightpitch.com.ui.SplashActivity
import app.eightpitch.com.base.preferences.*
import app.eightpitch.com.dagger.modules.*
import app.eightpitch.com.dagger.scopes.AppScope
import dagger.Component


@AppScope
@Component(modules = [ApplicationModule::class])
interface AppComponent {

    fun injectIntoActivity(splashActivity: SplashActivity)

    fun buildModelsSubComponent(
        retrofitModule: RetrofitModule,
        viewModelsModule: ViewModelsModule,
        utilsModule: UtilsModule,
        modelsModule: ModelsModule
    ): ModelsComponent

    fun providePreferencesReader() : PreferencesReader
    fun providePreferencesWriter() : PreferencesWriter
}