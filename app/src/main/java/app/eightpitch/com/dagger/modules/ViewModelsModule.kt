package app.eightpitch.com.dagger.modules

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import app.eightpitch.com.base.baseviewmodels.codeverification.DefaultCodeValidationViewModel
import app.eightpitch.com.base.preferences.PreferencesReader
import app.eightpitch.com.base.preferences.PreferencesWriter
import app.eightpitch.com.dagger.scopes.ModelsScope
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.notifications.NotificationsMapper
import app.eightpitch.com.models.*
import app.eightpitch.com.ui.PreContractualViewModel
import app.eightpitch.com.ui.authorized.AuthorizedActivityViewModel
import app.eightpitch.com.ui.authorized.main.homepager.DashboardRootViewModel
import app.eightpitch.com.ui.authorized.main.test.TestFragmentViewModel
import app.eightpitch.com.ui.changeemail.ChangeEmailFragmentViewModel
import app.eightpitch.com.ui.dashboard.initiator.InitiatorStatisticViewModel
import app.eightpitch.com.ui.dashboard.initiator.InitiatorWelcomeViewModel
import app.eightpitch.com.ui.dashboard.investor.InvestorStatisticViewModel
import app.eightpitch.com.ui.dashboard.investor.InvestorWelcomeViewModel
import app.eightpitch.com.ui.dynamicdetails.DynamicProjectDetailsChildViewModel
import app.eightpitch.com.ui.dynamicdetails.DynamicProjectDetailsViewModel
import app.eightpitch.com.ui.editprofile.EditProfileFragmentViewModel
import app.eightpitch.com.ui.investment.CurrencyChooserViewModel
import app.eightpitch.com.ui.investment.InputBghViewModel
import app.eightpitch.com.ui.investment.InputEuroViewModel
import app.eightpitch.com.ui.investment.ViewPagerSharableViewModel
import app.eightpitch.com.ui.investment.choosedocuments.ProjectDocumentsViewModel
import app.eightpitch.com.ui.investment.directdownloading.DirectDownloadingViewModel
import app.eightpitch.com.ui.investment.germanytax.GermanTaxViewModel
import app.eightpitch.com.ui.investment.processing.InvestmentProcessingViewModel
import app.eightpitch.com.ui.investment.sendingemail.SendingByEmailViewModel
import app.eightpitch.com.ui.investment.unprocessed.UnfinishedInvestmentViewModel
import app.eightpitch.com.ui.kyc.KycActivityViewModel
import app.eightpitch.com.ui.kyc.country.KycCountryInfoFragmentViewModel
import app.eightpitch.com.ui.kyc.editsummary.KycEditSummaryInfoFragmentViewModel
import app.eightpitch.com.ui.kyc.editsummary.SharableViewModel
import app.eightpitch.com.ui.kyc.emailvalidation.EmailCodeValidationViewModel
import app.eightpitch.com.ui.kyc.financial.KycFinancialInfoFragmentViewModel
import app.eightpitch.com.ui.kyc.nationality.KycNationalityViewModel
import app.eightpitch.com.ui.kyc.personalinfo.KycPersonalInfoFragmentViewModel
import app.eightpitch.com.ui.kyc.street.KycStreetInfoFragmentViewModel
import app.eightpitch.com.ui.kyc.summaruinformation.KycSummaryInfoFragmentViewModel
import app.eightpitch.com.ui.kyc.yourbirth.UserBirthInfoFragmentViewModel
import app.eightpitch.com.ui.notifications.NotificationsFragmentViewModel
import app.eightpitch.com.ui.payment.compleatepayment.ClassicBankPaymentFragmentViewModel
import app.eightpitch.com.ui.payment.directdebitcompleate.DirectDebitPaymentFragmentViewModel
import app.eightpitch.com.ui.payment.hardcappaymentselection.HardCapPaymentSelectionViewModel
import app.eightpitch.com.ui.payment.softcappaymentselection.SoftCapPaymentSelectionViewModel
import app.eightpitch.com.ui.pincode.PinCodeViewModel
import app.eightpitch.com.ui.profile.ProfileFragmentViewModel
import app.eightpitch.com.ui.profilepayments.PaymentsListViewModel
import app.eightpitch.com.ui.profilepayments.ProfilePaymentsFragmentViewModel
import app.eightpitch.com.ui.profilepayments.TransfersListViewModel
import app.eightpitch.com.ui.profilepayments.tranferdetails.TransferDetailsViewModel
import app.eightpitch.com.ui.profilepaymentsdetails.ProfilePaymentDetailsFragmentViewModel
import app.eightpitch.com.ui.profilepersonal.ProfilePersonalFragmentViewModel
import app.eightpitch.com.ui.profilepersonal.changingphone.SpecifyPhoneNumberViewModel
import app.eightpitch.com.ui.profilesecurity.ProfileSecurityFragmentViewModel
import app.eightpitch.com.ui.profilesecurity.changingpassword.EnterOldPasswordViewModel
import app.eightpitch.com.ui.profilesecurity.changingpassword.SetNewPasswordViewModel
import app.eightpitch.com.ui.profilesecurity.quicklogin.ProfileQuickLoginViewModel
import app.eightpitch.com.ui.projectfilter.SharableFilterViewModel
import app.eightpitch.com.ui.projectfilter.filter.FilterProjectFragmentViewModel
import app.eightpitch.com.ui.projectfilter.filteramount.FilterChooseAmountRangeFragmentViewModel
import app.eightpitch.com.ui.projectfilter.filtersecurity.FilterChooseSecurityFragmentViewModel
import app.eightpitch.com.ui.projectfilter.filterstatus.FilterChooseStatusFragmentViewModel
import app.eightpitch.com.ui.projects.ProjectsFragmentViewModel
import app.eightpitch.com.ui.projects.mappers.UIElementsConverter
import app.eightpitch.com.ui.projectsdetail.ProjectDetailsViewModel
import app.eightpitch.com.ui.projectsearch.SearchProjectsFragmentViewModel
import app.eightpitch.com.ui.questionnaire.QuestionnaireViewModel
import app.eightpitch.com.ui.questionnaire.investorqualification.InvestorQualificationViewModel
import app.eightpitch.com.ui.questionnaire.knowledge.KnowledgeViewModel
import app.eightpitch.com.ui.reasonselector.DeletingReasonSelectorFragmentViewModel
import app.eightpitch.com.ui.registration.login.LoginViewModel
import app.eightpitch.com.ui.registration.restorepassword.EnterEmailAccountViewModel
import app.eightpitch.com.ui.registration.signup.institutionalinvestor.company.IICompanyInfoFragmentViewModel
import app.eightpitch.com.ui.registration.signup.institutionalinvestor.email.IIPersonalInfoEmailFragmentViewModel
import app.eightpitch.com.ui.registration.signup.institutionalinvestor.password.IIPasswordFragmentViewModel
import app.eightpitch.com.ui.registration.signup.institutionalinvestor.personalInfo.IIPersonalInfoFragmentViewModel
import app.eightpitch.com.ui.registration.signup.institutionalinvestor.phone.IISpecifyPhoneNumberViewModel
import app.eightpitch.com.ui.registration.signup.privateinvestor.email.PVIPersonalInfoEmailFragmentViewModel
import app.eightpitch.com.ui.registration.signup.privateinvestor.password.PVIPasswordFragmentViewModel
import app.eightpitch.com.ui.registration.signup.privateinvestor.personalinfo.PVIPersonalInfoFragmentViewModel
import app.eightpitch.com.ui.registration.signup.privateinvestor.phone.PVISpecifyPhoneNumberViewModel
import app.eightpitch.com.ui.settings.SettingsFragmentViewModel
import app.eightpitch.com.ui.twofactor.choosetwoauthmethod.ChooseTwoAuthMethodFragmentViewModel
import app.eightpitch.com.ui.videoplayer.activity.VideoPlayerActivityViewModel
import app.eightpitch.com.ui.webid.WebIdIdentifyViewModel
import app.eightpitch.com.utils.constructor.ProjectDetailsTabsContentParser
import app.eightpitch.com.utils.models.RefreshTokenModel
import app.eightpitch.com.utils.models.VimeoModel
import app.eightpitch.com.utils.webid.WebIdController
import dagger.MapKey
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import javax.inject.Inject
import javax.inject.Provider
import kotlin.reflect.KClass

@Module
class ViewModelsModule {

    /**
     * Main rule : Factory should be injected, but models aren't
     */
    @ModelsScope
    class BaseViewModelFactory @Inject constructor(private val viewModelsProvidersMap: MutableMap<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>) :
        ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {

            val viewModelProvider =
                viewModelsProvidersMap[modelClass]
                ?: throw IllegalArgumentException("ViewModel with class $modelClass not found in providers map")

            return viewModelProvider.get() as T
        }
    }

    /**
     * Key for signing each new binded view model, to put it into the view models map in BaseViewModelFactory
     * to prevent view models recreation by common injection process. BaseViewModelFactory should be injected into activity
     * and used as base factory for view models creation.
     */
    @Target(
        AnnotationTarget.FUNCTION,
        AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER
    )
    @kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
    @MapKey
    internal annotation class ViewModelKey(val value: KClass<out ViewModel>)

    @Provides
    @ModelsScope
    internal fun providesViewModelFactory(viewModelsProvidersMap: MutableMap<Class<out ViewModel>, Provider<ViewModel>>): ViewModelProvider.Factory =
        BaseViewModelFactory(viewModelsProvidersMap)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(AuthorizedActivityViewModel::class)
    internal fun provideAuthorizedActivityVM(
        preferencesReader: PreferencesReader,
        preferencesWriter: PreferencesWriter,
        notificationsModel: NotificationsModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        AuthorizedActivityViewModel(preferencesReader, preferencesWriter, notificationsModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(KycActivityViewModel::class)
    internal fun provideKycActivityViewModel(
        userModel: UserModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        KycActivityViewModel(userModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(VideoPlayerActivityViewModel::class)
    internal fun provideVideoPlayerActivityViewModel(
        context: Context,
        threadSeparator: ThreadsSeparator,
    ): ViewModel = VideoPlayerActivityViewModel(context, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    internal fun provideLoginViewModel(
        appContext: Context,
        authModel: AuthModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        LoginViewModel(appContext, authModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(PVIPersonalInfoFragmentViewModel::class)
    internal fun providePVIPersonalInfoFragmentViewModel(
        appContext: Context,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        PVIPersonalInfoFragmentViewModel(appContext, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(IIPersonalInfoFragmentViewModel::class)
    internal fun provideIIPersonalInfoFragmentViewModel(
        appContext: Context,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        IIPersonalInfoFragmentViewModel(appContext, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(IISpecifyPhoneNumberViewModel::class)
    internal fun provideIISpecifyPhoneNumberViewModel(
        appContext: Context,
        smsModel: SMSModel,
        supportModel: SupportModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        IISpecifyPhoneNumberViewModel(appContext, smsModel, supportModel, threadSeparator)


    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(IIPersonalInfoEmailFragmentViewModel::class)
    internal fun provideIIPersonalInfoEmailFragmentViewModel(
        appContext: Context,
        supportModel: SupportModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        IIPersonalInfoEmailFragmentViewModel(appContext, supportModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(IIPasswordFragmentViewModel::class)
    internal fun provideIIPasswordFragmentViewModel(
        appContext: Context,
        registrationModel: RegistrationModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        IIPasswordFragmentViewModel(appContext, registrationModel, threadSeparator)


    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(PVIPersonalInfoEmailFragmentViewModel::class)
    internal fun providePVIPersonalInfoEmailFragmentViewModel(
        appContext: Context,
        supportModel: SupportModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        PVIPersonalInfoEmailFragmentViewModel(appContext, supportModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(PVISpecifyPhoneNumberViewModel::class)
    internal fun providePVISpecifyPhoneNumberViewModel(
        appContext: Context,
        smsModel: SMSModel,
        supportModel: SupportModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        PVISpecifyPhoneNumberViewModel(
            appContext,
            smsModel,
            supportModel,
            threadSeparator
        )

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(DefaultCodeValidationViewModel::class)
    internal fun provideDefaultCodeValidationViewModel(
        appContext: Context,
        smsModel: SMSModel,
        registrationModel: RegistrationModel,
        authModel: AuthModel,
        investmentModel: InvestmentModel,
        projectModel: ProjectModel,
        userModel: UserModel,
        supportModel: SupportModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        DefaultCodeValidationViewModel(
            appContext,
            smsModel,
            registrationModel,
            authModel,
            investmentModel,
            projectModel,
            userModel,
            supportModel,
            threadSeparator
        )

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(PVIPasswordFragmentViewModel::class)
    internal fun providePVIPasswordFragmentViewModel(
        appContext: Context,
        registrationModel: RegistrationModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        PVIPasswordFragmentViewModel(appContext, registrationModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(TestFragmentViewModel::class)
    internal fun provideProfileViewModel(
        appContext: Context,
        userModel: UserModel,
        authModel: AuthModel,
        refreshTokenModel: RefreshTokenModel,
        preferencesReader: PreferencesReader,
        preferencesWriter: PreferencesWriter,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        TestFragmentViewModel(
            appContext,
            userModel,
            authModel,
            refreshTokenModel,
            preferencesReader,
            preferencesWriter,
            threadSeparator
        )

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(IICompanyInfoFragmentViewModel::class)
    internal fun provideIICompanyInfoFragmentViewModel(
        appContext: Context,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        IICompanyInfoFragmentViewModel(appContext, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(EmailCodeValidationViewModel::class)
    internal fun provideEmailCodeValidationFragmentViewModel(
        appContext: Context,
        smsModel: SMSModel,
        userModel: UserModel,
        supportModel: SupportModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        EmailCodeValidationViewModel(appContext, smsModel, userModel, supportModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(WebIdIdentifyViewModel::class)
    internal fun provideWebIdIdentifyViewModel(
        appContext: Context,
        webIdController: WebIdController,
        userModel: UserModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        WebIdIdentifyViewModel(appContext, webIdController, userModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(KycPersonalInfoFragmentViewModel::class)
    internal fun provideKycPersonalInfoFragmentViewModel(
        appContext: Context,
        userModel: UserModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        KycPersonalInfoFragmentViewModel(appContext, userModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(UserBirthInfoFragmentViewModel::class)
    internal fun provideUserBirthInfoFragmentViewModel(
        appContext: Context,
        userModel: UserModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        UserBirthInfoFragmentViewModel(appContext, userModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(KycNationalityViewModel::class)
    internal fun provideKycNationalityViewModel(
        appContext: Context,
        userModel: UserModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        KycNationalityViewModel(appContext, userModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(QuestionnaireViewModel::class)
    internal fun provideQuestionnaireViewModel(
        appContext: Context,
        userModel: UserModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        QuestionnaireViewModel(appContext, userModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(KnowledgeViewModel::class)
    internal fun provideKnowledgeViewModel(
        appContext: Context,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        KnowledgeViewModel(appContext, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(KycCountryInfoFragmentViewModel::class)
    internal fun provideKycCountryInfoFragmentViewModel(
        appContext: Context,
        userModel: UserModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        KycCountryInfoFragmentViewModel(appContext, userModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(KycStreetInfoFragmentViewModel::class)
    internal fun provideKycStreetInfoFragmentViewModel(
        appContext: Context,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        KycStreetInfoFragmentViewModel(appContext, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(KycFinancialInfoFragmentViewModel::class)
    internal fun provideKycFinantialInfoFragmentViewModel(
        appContext: Context,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        KycFinancialInfoFragmentViewModel(appContext, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(KycEditSummaryInfoFragmentViewModel::class)
    internal fun provideKycEditSummaryInfoFragmentViewModel(
        appContext: Context,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        KycEditSummaryInfoFragmentViewModel(appContext, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(KycSummaryInfoFragmentViewModel::class)
    internal fun provideKycSummaryInfoFragmentViewModel(
        appContext: Context,
        userModel: UserModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        KycSummaryInfoFragmentViewModel(appContext, userModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(SharableViewModel::class)
    internal fun provideSharableViewModel(): ViewModel =
        SharableViewModel(null)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(PreContractualViewModel::class)
    internal fun providePreContractualViewModel(
        appContext: Context,
        investmentModel: InvestmentModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        PreContractualViewModel(appContext, investmentModel, threadSeparator)


    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(ProjectsFragmentViewModel::class)
    internal fun provideProjectsFragmentViewModel(
        appContext: Context,
        projectModel: ProjectModel,
        filterModel: FilterModel,
        userModel: UserModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        ProjectsFragmentViewModel(appContext, projectModel, filterModel, userModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(ProjectDetailsViewModel::class)
    internal fun provideProjectDetailsFragmentViewModel(
        appContext: Context,
        projectModel: ProjectModel,
        investmentModel: InvestmentModel,
        userModel: UserModel,
        vimeoModel: VimeoModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        ProjectDetailsViewModel(
            appContext,
            projectModel,
            investmentModel,
            userModel,
            vimeoModel,
            threadSeparator
        )

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(CurrencyChooserViewModel::class)
    internal fun provideCurrencyChooserViewModel(
        appContext: Context,
        projectModel: ProjectModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        CurrencyChooserViewModel(appContext, projectModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(InputEuroViewModel::class)
    internal fun provideInputEuroViewModel(
        appContext: Context,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        InputEuroViewModel(appContext, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(InputBghViewModel::class)
    internal fun provideCInputBghViewModel(
        appContext: Context,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        InputBghViewModel(appContext, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(DirectDownloadingViewModel::class)
    internal fun provideDirectDownloadingViewModel(
        appContext: Context,
        projectModel: ProjectModel,
        downloadModel: DownloadModel,
        smsModel: SMSModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        DirectDownloadingViewModel(
            appContext,
            projectModel,
            downloadModel,
            smsModel,
            threadSeparator
        )

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(SendingByEmailViewModel::class)
    internal fun provideSendingByEmailViewModel(
        appContext: Context,
        smsModel: SMSModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        SendingByEmailViewModel(appContext, smsModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(ProjectDocumentsViewModel::class)
    internal fun provideProjectDocumentsViewModel(
        appContext: Context,
        documentsModel: DocumentsModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        ProjectDocumentsViewModel(appContext, documentsModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(UnfinishedInvestmentViewModel::class)
    internal fun provideUnFinishedInvestmentViewModel(
        appContext: Context,
        investmentModel: InvestmentModel,
        projectModel: ProjectModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        UnfinishedInvestmentViewModel(appContext, investmentModel, projectModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(ClassicBankPaymentFragmentViewModel::class)
    internal fun provideOnlineBankPaymentFragmentViewModel(
        appContext: Context,
        paymentModel: PaymentModel,
        downloadModel: DownloadModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        ClassicBankPaymentFragmentViewModel(appContext, paymentModel, downloadModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(DirectDebitPaymentFragmentViewModel::class)
    internal fun provideDirectDebitPaymentFragmentViewModel(
        appContext: Context,
        userModel: UserModel,
        paymentModel: PaymentModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        DirectDebitPaymentFragmentViewModel(appContext, userModel, paymentModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(NotificationsFragmentViewModel::class)
    internal fun provideNotificationsFragmentViewModel(
        appContext: Context,
        notificationsModel: NotificationsModel,
        notificationsMapper: NotificationsMapper,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        NotificationsFragmentViewModel(
            appContext,
            notificationsModel,
            notificationsMapper,
            threadSeparator
        )

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(ViewPagerSharableViewModel::class)
    internal fun provideViewPagerSharableViewModel(): ViewModel =
        ViewPagerSharableViewModel()

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(SharableFilterViewModel::class)
    internal fun provideSharableFilterViewModel(): ViewModel =
        SharableFilterViewModel()

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(HardCapPaymentSelectionViewModel::class)
    internal fun provideHardCapPaymentSelectionViewModel(
        appContext: Context,
        paymentModel: PaymentModel,
        investmentModel: InvestmentModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        HardCapPaymentSelectionViewModel(appContext, paymentModel, investmentModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(SoftCapPaymentSelectionViewModel::class)
    internal fun provideSoftCapPaymentSelectionViewModel(
        appContext: Context,
        paymentModel: PaymentModel,
        investmentModel: InvestmentModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        SoftCapPaymentSelectionViewModel(appContext, paymentModel, investmentModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(ProfileFragmentViewModel::class)
    internal fun provideProfileFragmentViewModel(
        appContext: Context,
        userModel: UserModel,
        authModel: AuthModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        ProfileFragmentViewModel(appContext, userModel, authModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(SettingsFragmentViewModel::class)
    internal fun provideSettingsFragmentViewModel(
        appContext: Context,
        userModel: UserModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        SettingsFragmentViewModel(appContext, userModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(DeletingReasonSelectorFragmentViewModel::class)
    internal fun provideDeletingReasonSelectorFragmentViewModel(
        appContext: Context,
        userModel: UserModel,
        smsModel: SMSModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        DeletingReasonSelectorFragmentViewModel(appContext, userModel, smsModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(ProfilePersonalFragmentViewModel::class)
    internal fun provideProfilePersonalFragmentViewModel(
        appContext: Context,
        userModel: UserModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        ProfilePersonalFragmentViewModel(appContext, userModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(EditProfileFragmentViewModel::class)
    internal fun provideEditProfileFragmentViewModel(
        appContext: Context,
        userModel: UserModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        EditProfileFragmentViewModel(appContext, userModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(ChangeEmailFragmentViewModel::class)
    internal fun provideChangeEmailFragmentViewModel(
        appContext: Context,
        userModel: UserModel,
        smsModel: SMSModel,
        supportModel: SupportModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        ChangeEmailFragmentViewModel(appContext, userModel, smsModel, supportModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(DynamicProjectDetailsViewModel::class)
    internal fun provideDynamicProjectDetailsViewModel(
        projectDetailsTabsContentParser: ProjectDetailsTabsContentParser,
        userModel: UserModel,
        projectModel: ProjectModel,
        investmentModel: InvestmentModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        DynamicProjectDetailsViewModel(projectDetailsTabsContentParser,
            userModel,
            projectModel,
            investmentModel,
            threadSeparator
        )

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(DynamicProjectDetailsChildViewModel::class)
    internal fun provideDynamicProjectDetailsChildViewModel(
        vimeoModel: VimeoModel,
        uiElementsConverter: UIElementsConverter,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        DynamicProjectDetailsChildViewModel(vimeoModel, uiElementsConverter, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(ProfilePaymentsFragmentViewModel::class)
    internal fun provideProfilePaymentsFragmentViewModel(
        context: Context,
        paymentModel: PaymentModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        ProfilePaymentsFragmentViewModel(context, paymentModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(ProfilePaymentDetailsFragmentViewModel::class)
    internal fun provideProfilePaymentDetailsFragmentViewModel(
        context: Context,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        ProfilePaymentDetailsFragmentViewModel(context, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(TransferDetailsViewModel::class)
    internal fun provideTransferDetailsViewModel(
        context: Context,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        TransferDetailsViewModel(context, threadSeparator)


    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(ProfileSecurityFragmentViewModel::class)
    internal fun provideProfileSecurityFragmentViewModel(
        context: Context,
        userModel: UserModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        ProfileSecurityFragmentViewModel(context, userModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(InvestorWelcomeViewModel::class)
    internal fun provideInvestorWelcomeViewModel(
        appContext: Context,
        projectModel: ProjectModel,
        userModel: UserModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        InvestorWelcomeViewModel(appContext, projectModel, userModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(InitiatorWelcomeViewModel::class)
    internal fun provideInitiatorWelcomeViewModel(
        appContext: Context,
        projectModel: ProjectModel,
        userModel: UserModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        InitiatorWelcomeViewModel(appContext, projectModel, userModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(InvestorStatisticViewModel::class)
    internal fun provideInvestorStatisticViewModel(
        appContext: Context,
        projectModel: ProjectModel,
        userModel: UserModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        InvestorStatisticViewModel(appContext, projectModel, userModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(DashboardRootViewModel::class)
    internal fun provideDashboardRootViewModel(
        userModel: UserModel,
        projectModel: ProjectModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        DashboardRootViewModel(userModel, projectModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(InitiatorStatisticViewModel::class)
    internal fun provideInitiatorStatisticViewModel(
        appContext: Context,
        projectModel: ProjectModel,
        userModel: UserModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        InitiatorStatisticViewModel(appContext, projectModel, userModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(ChooseTwoAuthMethodFragmentViewModel::class)
    internal fun provideChooseTwoAuthMethodFragmentViewModel(
        context: Context,
        userModel: UserModel,
        authModel: AuthModel,
        smsModel: SMSModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        ChooseTwoAuthMethodFragmentViewModel(context, userModel, authModel, smsModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(EnterOldPasswordViewModel::class)
    internal fun provideEnterOldPasswordViewModel(
        context: Context,
        userModel: UserModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        EnterOldPasswordViewModel(context, userModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(SetNewPasswordViewModel::class)
    internal fun provideSetNewPasswordViewModel(
        context: Context,
        userModel: UserModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        SetNewPasswordViewModel(context, userModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(EnterEmailAccountViewModel::class)
    internal fun provideEnterEmailAccountViewModel(
        context: Context,
        smsModel: SMSModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        EnterEmailAccountViewModel(context, smsModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(PinCodeViewModel::class)
    internal fun providePinCodeViewModel(
        context: Context,
        preferencesReader: PreferencesReader,
        preferencesWriter: PreferencesWriter,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        PinCodeViewModel(context, preferencesReader, preferencesWriter, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(ProfileQuickLoginViewModel::class)
    internal fun provideProfileQuickLoginViewModel(
        context: Context,
        preferencesReader: PreferencesReader,
        preferencesWriter: PreferencesWriter,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        ProfileQuickLoginViewModel(context, preferencesReader, preferencesWriter, threadSeparator)


    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(SpecifyPhoneNumberViewModel::class)
    internal fun provideSpecifyPhoneNumberViewModel(
        appContext: Context,
        smsModel: SMSModel,
        supportModel: SupportModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        SpecifyPhoneNumberViewModel(appContext, smsModel, supportModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(InvestmentProcessingViewModel::class)
    internal fun provideInvestmentProcessingViewModel(
        investmentModel: InvestmentModel,
        projectModel: ProjectModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        InvestmentProcessingViewModel(investmentModel, projectModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(SearchProjectsFragmentViewModel::class)
    internal fun provideSearchProjectsFragmentViewModel(
        appContext: Context,
        filterModel: FilterModel,
        threadSeparator: ThreadsSeparator
    ): ViewModel =
        SearchProjectsFragmentViewModel(appContext, filterModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(FilterChooseStatusFragmentViewModel::class)
    internal fun provideFilterChooseStatusFragmentViewModel(
        appContext: Context,
        threadSeparator: ThreadsSeparator
    ): ViewModel =
        FilterChooseStatusFragmentViewModel(appContext, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(FilterChooseSecurityFragmentViewModel::class)
    internal fun provideFilterChooseSecurityFragmentViewModel(
        appContext: Context,
        filterModel: FilterModel,
        threadSeparator: ThreadsSeparator
    ): ViewModel =
        FilterChooseSecurityFragmentViewModel(appContext, filterModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(FilterChooseAmountRangeFragmentViewModel::class)
    internal fun provideFilterChooseAmountRangeFragmentViewModel(
        appContext: Context,
        filterModel: FilterModel,
        threadSeparator: ThreadsSeparator
    ): ViewModel =
        FilterChooseAmountRangeFragmentViewModel(appContext, filterModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(FilterProjectFragmentViewModel::class)
    internal fun provideFilterProjectFragmentViewModel(
        appContext: Context,
        filterModel: FilterModel,
        threadSeparator: ThreadsSeparator
    ): ViewModel =
        FilterProjectFragmentViewModel(appContext, filterModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(PaymentsListViewModel::class)
    internal fun providePaymentsListViewModel(
        appContext: Context,
        paymentModel: PaymentModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        PaymentsListViewModel(appContext, paymentModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(TransfersListViewModel::class)
    internal fun provideTransfersListViewModel(
        appContext: Context,
        paymentModel: PaymentModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        TransfersListViewModel(appContext, paymentModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(InvestorQualificationViewModel::class)
    internal fun provideInvestorQualificationViewModel(
        appContext: Context,
        filesModel: FilesModel,
        userModel: UserModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        InvestorQualificationViewModel(appContext, filesModel, userModel, threadSeparator)

    @Provides
    @ModelsScope
    @IntoMap
    @ViewModelKey(GermanTaxViewModel::class)
    internal fun provideGermanTaxViewModel(
        appContext: Context,
        userModel: UserModel,
        threadSeparator: ThreadsSeparator,
    ): ViewModel =
        GermanTaxViewModel(appContext, userModel, threadSeparator)

    //TODO all view models should be defined here, to give dagger opportunity to control of viewModels creating and putting
    //TODO  into the viewModels map
}