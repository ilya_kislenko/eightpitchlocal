package app.eightpitch.com.dagger.modules

import android.content.Context
import android.os.*
import app.eightpitch.com.base.MACAddressProvider
import app.eightpitch.com.base.preferences.PreferencesReader
import app.eightpitch.com.base.preferences.PreferencesWriter
import app.eightpitch.com.dagger.scopes.ModelsScope
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.dynamicviews.builders.UIElementsBuilder
import app.eightpitch.com.data.dto.dynamicviews.builders.UIElementsTabletBuilder
import app.eightpitch.com.data.dto.notifications.NotificationsMapper
import app.eightpitch.com.ui.projects.mappers.IUIElementsConverter
import app.eightpitch.com.ui.projects.mappers.UIElementsConverter
import app.eightpitch.com.utils.BasicAuthorization
import app.eightpitch.com.utils.RootDetection
import app.eightpitch.com.utils.constructor.ProjectDetailsTabsContentParser
import com.google.gson.Gson
import dagger.*

@Module
class UtilsModule {

    @ModelsScope
    @Provides
    fun provideThreadSeparator(appContext: Context) = ThreadsSeparator(appContext)

    @ModelsScope
    @Provides
    fun provideGson() = Gson()

    @ModelsScope
    @Provides
    fun provideMacAddressProvider(
        preferencesReader: PreferencesReader,
        preferenceWriter: PreferencesWriter
    ) = MACAddressProvider(preferencesReader, preferenceWriter)

    @ModelsScope
    @Provides
    fun provideMainThreadHandler() = Handler(Looper.getMainLooper())

    @ModelsScope
    @Provides
    fun provideARootDetection() = RootDetection()

    @ModelsScope
    @Provides
    fun provideNotificationsMapper(appContext: Context) = NotificationsMapper(appContext)

    @ModelsScope
    @Provides
    fun provideUiElementsBuilder(appContext: Context) = UIElementsBuilder(appContext)

    @ModelsScope
    @Provides
    fun provideUiElementsConverter(
        uiElementsBuilder: UIElementsBuilder,
        uiElementsTabletBuilder: UIElementsTabletBuilder
    ): IUIElementsConverter =
        UIElementsConverter(uiElementsBuilder, uiElementsTabletBuilder)

    @ModelsScope
    @Provides
    fun provideProjectDetailsContentParser() = ProjectDetailsTabsContentParser()

    @ModelsScope
    @Provides
    fun provideBasicAuthorization() = BasicAuthorization()
}