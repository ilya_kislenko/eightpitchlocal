package app.eightpitch.com.dagger.modules

import android.content.Context
import app.eightpitch.com.base.preferences.*
import app.eightpitch.com.dagger.scopes.AppScope
import dagger.*

@Module
class ApplicationModule(private val appContext: Context) {

    @AppScope
    @Provides
    fun provideContext() = appContext

    @AppScope
    @Provides
    fun providePreferencesWriter(appContext: Context) = PreferencesWriter(appContext)

    @AppScope
    @Provides
    fun providePreferencesReader(appContext: Context) = PreferencesReader(appContext)

}