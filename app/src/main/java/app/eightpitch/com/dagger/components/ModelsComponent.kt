package app.eightpitch.com.dagger.components

import app.eightpitch.com.base.BaseActivity
import app.eightpitch.com.dagger.modules.ModelsModule
import app.eightpitch.com.dagger.modules.RetrofitModule
import app.eightpitch.com.dagger.modules.UtilsModule
import app.eightpitch.com.dagger.modules.ViewModelsModule
import app.eightpitch.com.dagger.scopes.ModelsScope
import app.eightpitch.com.utils.RootDetection
import dagger.Subcomponent

@ModelsScope
@Subcomponent(modules = [RetrofitModule::class, ViewModelsModule::class, UtilsModule::class, ModelsModule::class])
interface ModelsComponent {

    fun injectIntoActivity(activity: BaseActivity)

    fun provideRootDetection(): RootDetection
}