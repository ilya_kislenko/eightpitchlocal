package app.eightpitch.com.utils.autoinsertsms

import android.content.ActivityNotFoundException
import android.content.Intent
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import app.eightpitch.com.R
import app.eightpitch.com.extensions.REQUEST_SMS_CONSENT_REQUEST
import app.eightpitch.com.extensions.orDefaultValue
import app.eightpitch.com.utils.Logger
import app.eightpitch.com.utils.Logger.TAG_GENERIC_ERROR
import java.lang.ref.WeakReference

/**
 * An interface that supposed to be used to handle all SMS messages interception from
 * [SMSBroadcastReceiver].
 */
interface ISMSInterceptor {
    fun onTimeout()
    fun onIntercept(consentIntent: Intent)
}

class DefaultSMSInterceptor(anchor: WeakReference<Fragment?>) : ISMSInterceptor {

    private val relatedContext = anchor.get()

    override fun onTimeout() {
        //Do nothing
    }

    override fun onIntercept(consentIntent: Intent) {
        try {
            relatedContext?.startActivityForResult(consentIntent, REQUEST_SMS_CONSENT_REQUEST)
        } catch (exception: ActivityNotFoundException) {
            Logger.logError(TAG_GENERIC_ERROR,
                relatedContext?.getString(R.string.something_go_wrong_exception_text).orDefaultValue(""),
                exception)
        }
    }
}