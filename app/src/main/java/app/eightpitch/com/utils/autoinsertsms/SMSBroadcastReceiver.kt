package app.eightpitch.com.utils.autoinsertsms

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.gms.auth.api.phone.SmsRetriever.EXTRA_CONSENT_INTENT
import com.google.android.gms.auth.api.phone.SmsRetriever.EXTRA_STATUS
import com.google.android.gms.auth.api.phone.SmsRetriever.SMS_RETRIEVED_ACTION
import com.google.android.gms.common.api.CommonStatusCodes.SUCCESS
import com.google.android.gms.common.api.CommonStatusCodes.TIMEOUT
import com.google.android.gms.common.api.Status

/**
 * BroadcastReceiver is to handle SMS interception. This can be registered either
 * in the AndroidManifest or at runtime. Should filter Intents on
 * [SmsRetriever.SMS_RETRIEVED_ACTION].
 */
class SMSBroadcastReceiver : BroadcastReceiver() {

    private var iSMSInterceptor: ISMSInterceptor? = null

    fun registerNewMessageListener(ISMSInterceptor: ISMSInterceptor?) {
        this.iSMSInterceptor = ISMSInterceptor
    }

    override fun onReceive(context: Context, intent: Intent) {

        if (SMS_RETRIEVED_ACTION != intent.action)
            return

        val extras = intent.extras
        val smsRetrieverStatus = extras?.get(EXTRA_STATUS) as? Status
        when (smsRetrieverStatus?.statusCode) {
            SUCCESS -> {
                val consentIntent = extras.getParcelable<Intent>(EXTRA_CONSENT_INTENT)
                consentIntent?.let { iSMSInterceptor?.onIntercept(it) }
            }
            TIMEOUT -> {
                iSMSInterceptor?.onTimeout()
            }
        }
        iSMSInterceptor = null
    }
}