package app.eightpitch.com.utils.defaultstrokeviews

import androidx.databinding.*

/**
 * Object for the  default representation of a view with label, drawable start and value
 * Using with DataBinding
 */
data class StrokeViewItem(
    val label: ObservableField<String>,
    val icon: ObservableInt,
    var value: ObservableField<String> = ObservableField(""),
)