package app.eightpitch.com.utils

import androidx.annotation.*
import androidx.lifecycle.*

class SingleActionLiveData<T> : MutableLiveData<T>() {

    @MainThread
    override fun observe(owner: LifecycleOwner, observer: Observer<in T>) {

        super.observe(owner, Observer { data ->
            if (data == null) return@Observer
            observer.onChanged(data)
            value = null
        })
    }

    @MainThread
    fun sendAction(data: T) {
        value = data
    }

    /**
     * For the classes which can be using live data from unit tests
     */
    @WorkerThread
    fun postAction(data: T){
        postValue(data)
    }
}