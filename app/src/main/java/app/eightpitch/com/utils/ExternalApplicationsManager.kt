package app.eightpitch.com.utils

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager.NameNotFoundException
import android.net.Uri
import app.eightpitch.com.utils.Constants.GA_PACKAGE_NAME

object ExternalApplicationsManager {

    fun isGoogleAuthenticatorInstalled(context: Context): Boolean {
        val packageManager = context.packageManager
        return try {
            packageManager.getPackageInfo(GA_PACKAGE_NAME, 0)
            true
        } catch (exception: NameNotFoundException) {
            false
        }
    }

    //example uri - "otpauth://totp/blackgoatholding.com?secret=LWAFNBLNHKVGCPZL"
    fun addGoogleAuthenticatorAccount(context: Context, uri: String) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
        context.startActivity(intent);
    }
}