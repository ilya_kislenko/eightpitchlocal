package app.eightpitch.com.utils

import app.eightpitch.com.BuildConfig
import okhttp3.Credentials

class BasicAuthorization {

    val headerTitle
        get() = "Basic"

    val headerEncodedCredentials: String
        get() = Credentials.basic(BuildConfig.AUTH_USER_NAME, BuildConfig.AUTH_PASSWORD)
}