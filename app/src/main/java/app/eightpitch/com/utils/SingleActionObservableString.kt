package app.eightpitch.com.utils

import androidx.databinding.ObservableField

class SingleActionObservableString : ObservableField<String>() {

    override fun set(value: String) {
        super.set(value)
        super.set("")
    }
}