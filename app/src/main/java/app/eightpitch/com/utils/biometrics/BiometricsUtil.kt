package app.eightpitch.com.utils.biometrics

/*MIT License

Copyright (c) 2017 Michel Omar Aflak

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.*/

import android.app.KeyguardManager
import android.content.Context.KEYGUARD_SERVICE
import android.content.Intent
import android.hardware.biometrics.BiometricManager.Authenticators.BIOMETRIC_STRONG
import android.hardware.biometrics.BiometricPrompt
import android.os.Build.VERSION.SDK_INT
import android.os.Build.VERSION_CODES.M
import android.os.Build.VERSION_CODES.P
import android.os.Build.VERSION_CODES.R as RB
import android.os.Build.VERSION_CODES.Q
import android.os.CancellationSignal
import android.provider.Settings
import android.provider.Settings.ACTION_BIOMETRIC_ENROLL
import android.provider.Settings.ACTION_FINGERPRINT_ENROLL
import android.provider.Settings.EXTRA_BIOMETRIC_AUTHENTICATORS_ALLOWED
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.biometric.BiometricManager
import androidx.core.hardware.fingerprint.FingerprintManagerCompat
import androidx.fragment.app.FragmentActivity
import app.eightpitch.com.R
import app.eightpitch.com.extensions.REQUEST_SETTINGS_FINGERPRINT
import app.eightpitch.com.extensions.REQUEST_START_BIOMETRIC_ENROLL
import app.eightpitch.com.ui.dialogs.GenericDialogFragment
import app.eightpitch.com.utils.Logger
import app.eightpitch.com.utils.Logger.TAG_GENERIC_ERROR
import app.eightpitch.com.utils.Logger.TAG_INFO
import me.aflak.libraries.callback.FingerprintDialogCallback
import me.aflak.libraries.dialog.FingerprintDialog
import java.lang.ref.WeakReference

class BiometricsUtil(anchor: WeakReference<FragmentActivity?>) {

    lateinit var successAuthCallback: () -> Unit
    lateinit var failureAuthCallback: () -> Unit
    lateinit var onBiometricDismissByUser: () -> Unit

    private val activity = anchor.get()

    internal fun startBiometrics() {
        val sdkVersion = SDK_INT
        when {
            sdkVersion >= P -> checkBiometricSupport()
            sdkVersion >= M -> startDeprecatedAuth()
            else -> activity?.let {
                createDialog(message = it.getString(R.string.no_biometric_features_available_on_device_text))
                    .showNow(it.supportFragmentManager, BiometricsUtil::class.java.name)
            }
        }
    }

    @RequiresApi(P)
    private fun getBiometricAuthCallback() = object : BiometricPrompt.AuthenticationCallback() {
        override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult?) {
            super.onAuthenticationSucceeded(result)
            activity?.let { notifyUser(it.getString(R.string.success_text)) }
            if (::successAuthCallback.isInitialized) {
                successAuthCallback.invoke()
            }
        }

        override fun onAuthenticationError(errorCode: Int, errString: CharSequence?) {
            super.onAuthenticationError(errorCode, errString)
            Logger.logError(TAG_GENERIC_ERROR, errString.toString())
            activity?.let { notifyUser(errString.toString()) }
            if (::onBiometricDismissByUser.isInitialized) {
                onBiometricDismissByUser.invoke()
            }
        }
    }

    @RequiresApi(Q)
    private fun startAuth() {
        activity?.let {
            val promptInfoBuilder = BiometricPrompt.Builder(activity)
                .setTitle(activity.getString(R.string.finger_print_title_text))
                .setSubtitle(activity.getString(R.string.finger_print_subtitle_text))
                .setNegativeButton(activity.getString(R.string.cancel_text),
                    activity.mainExecutor,
                    { _, _ ->
                        notifyUser(it.getString(R.string.negative_text))
                        if (::failureAuthCallback.isInitialized) {
                            failureAuthCallback.invoke()
                        }
                    })

            if (SDK_INT >= Q)
                promptInfoBuilder.setConfirmationRequired(false)

            val promptInfo = promptInfoBuilder.build()
            promptInfo.authenticate(getCancellationSignal(), activity.mainExecutor, getBiometricAuthCallback())
        }
    }

    @RequiresApi(M)
    fun startDeprecatedAuth() {
        activity?.let {
            val fingerprintManager = FingerprintManagerCompat.from(activity)
            if (!fingerprintManager.isHardwareDetected) {
                createDialog(
                    message = it.getString(R.string.setup_your_fingerprint_in_settings_text),
                    positiveButtonMessage = it.getString(R.string.go_to_settings_text),
                    positiveBlock = { startSettings() }
                ).showNow(activity.supportFragmentManager,
                    BiometricsUtil::class.java.name)
                return
            }

            if (fingerprintManager.hasEnrolledFingerprints()) {
                FingerprintDialog.initialize(it)
                    .title(it.getString(R.string.finger_print_title_text))
                    .message(it.getString(R.string.finger_print_subtitle_text))
                    .callback(object : FingerprintDialogCallback {
                        override fun onAuthenticationSucceeded() {
                            notifyUser(it.getString(R.string.success_text))
                            if (::successAuthCallback.isInitialized) {
                                successAuthCallback.invoke()
                            }
                        }

                        override fun onAuthenticationCancel() {
                            notifyUser(it.getString(R.string.negative_text))
                            if (::failureAuthCallback.isInitialized) {
                                failureAuthCallback.invoke()
                            }
                        }
                    }).show()
            } else {
                startSettings()
            }
        }
    }

    private fun startSettings() {
        val settingsIntent = Intent(Settings.ACTION_SECURITY_SETTINGS)
        activity?.startActivityForResult(settingsIntent, REQUEST_SETTINGS_FINGERPRINT)
    }

    @RequiresApi(P)
    private fun checkBiometricSupport() {
        activity?.let {
            val biometricManager = BiometricManager.from(it)
            when (biometricManager.canAuthenticate()) {
                BiometricManager.BIOMETRIC_SUCCESS -> {
                    startAuth()
                    Logger.logInfo(TAG_INFO, "App can authenticate using biometrics.")
                }
                BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> {
                    val dialogFragment =
                        createDialog(message = it.getString(R.string.biometric_auth_not_enabled_on_device_text))
                    dialogFragment.showNow(activity.supportFragmentManager, BiometricsUtil::class.java.name)
                    Logger.logError(TAG_GENERIC_ERROR, "No biometric features available on this device.")
                }
                BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> {
                    val dialogFragment =
                        createDialog(it.getString(R.string.biometric_features_are_currently_unavailable_text))
                    dialogFragment.showNow(activity.supportFragmentManager, BiometricsUtil::class.java.name)
                    Logger.logError(TAG_GENERIC_ERROR, "Biometric features are currently unavailable.")
                }

                BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
                    val sdkVersion = SDK_INT
                    if (!isKeyguardSecure() && sdkVersion >= P && sdkVersion < RB) {
                        createDialog(
                            message = it.getString(R.string.put_any_of_the_ways_to_protect_text),
                            positiveButtonMessage = it.getString(R.string.go_to_settings_text),
                            positiveBlock = { startSettings() }
                        ).showNow(activity.supportFragmentManager,
                            BiometricsUtil::class.java.name)
                        return
                    }
                    val enrollIntent = if (sdkVersion in P until RB) {
                        Intent(ACTION_FINGERPRINT_ENROLL)
                    } else Intent(ACTION_BIOMETRIC_ENROLL).apply {
                        putExtra(EXTRA_BIOMETRIC_AUTHENTICATORS_ALLOWED, BIOMETRIC_STRONG)
                    }
                    it.startActivityForResult(enrollIntent,
                        REQUEST_START_BIOMETRIC_ENROLL) // Dont work after login onActivityResult
                }
                else -> {
                }
            }
        }
    }

    @JvmName("getCancellationSignal1")
    private fun getCancellationSignal() = CancellationSignal()

    private fun isKeyguardSecure(): Boolean {
        val keyguardManager = activity?.getSystemService(KEYGUARD_SERVICE) as? KeyguardManager
        return keyguardManager?.isKeyguardSecure ?: false
    }

    private fun createDialog(
        message: String,
        positiveButtonMessage: String = "",
        positiveBlock: (() -> Unit)? = null,

        ) = GenericDialogFragment.createAnInstance(
        message = message,
        positiveButtonMessage = positiveButtonMessage,
        positiveListener = positiveBlock
    )

    private fun notifyUser(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show()
    }
}