package app.eightpitch.com.utils.webid

import android.app.Activity
import android.content.Context
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import app.eightpitch.com.BuildConfig
import app.eightpitch.com.R
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.data.transmit.ResultStatus.FAILED
import app.eightpitch.com.utils.Constants.DD_MM_YYYY_HH_MM_SS
import app.eightpitch.com.utils.DateUtils.formatByPattern
import app.eightpitch.com.utils.Logger
import app.eightpitch.com.utils.Logger.TAG_GENERIC_ERROR
import app.eightpitch.com.utils.SingleActionLiveData
import de.webid_solutions.mobile_app.sdk.*
import de.webid_solutions.mobile_app.sdk.domain.*
import de.webid_solutions.mobile_app.sdk.domain.videocall.EVideoCallState.*
import de.webid_solutions.mobile_app.sdk.domain.videocall.HoldTheLineProgress
import de.webid_solutions.mobile_app.sdk.domain.videocall.IVideoCallStateListener
import de.webid_solutions.mobile_app.sdk.impl.WebIdMobileAppSdkFactory
import org.webrtc.SurfaceViewRenderer
import java.io.IOException
import java.net.URI
import javax.net.ssl.SSLPeerUnverifiedException
import kotlin.time.microseconds

class WebIdController(private val appContext: Context) {

    private var defaultExceptionMessage = appContext.getString(R.string.something_go_wrong_exception_text)

    private var sdkInstance: IWebIdMobileAppSdk? = null
    private var actionResult: VerifyActionIdResult? = null
    private var videoCall: IVideoCallStateListener? = null

    //Callbacks
    internal lateinit var userPositionInQueueCallback: (HoldTheLineProgress) -> Unit
    internal lateinit var callWithAgentStartedCallback: () -> Unit
    internal lateinit var callEndCallback: () -> Unit
    internal lateinit var webIdEdgeCasesHandler: (String) -> Unit
    //endregion

    //TODO remove for prod
    internal lateinit var showTANCallback: () -> Unit

    private val webIDErrorsContainer = SingleActionLiveData<Result<String>>()
    internal fun getWebIDErrorsContainer(): LiveData<Result<String>> = webIDErrorsContainer

    fun setupWebIdArguments() {
        try {
            val webIdSdkEnvironment = WebIdSdkEnvironment(URI(BuildConfig.WEBIDENVIRONMENT_URL), BuildConfig.SHA_KEY)
            val webIdMobileAppSdkFactory = WebIdMobileAppSdkFactory()
            sdkInstance = webIdMobileAppSdkFactory.create(
                webIdSdkEnvironment, BuildConfig.WEB_ID_USERNAME, BuildConfig.WEB_ID_API_KEY, appContext
            )
        } catch (e: Exception) {
            Logger.logError(
                TAG_GENERIC_ERROR, "Web id arguments failed,reason: ", e
            )
        }
    }

    /**
     * This method should be called asynchronously to handle an [Exception] on the UI layer
     */
    internal fun verifyActionId(actionId: String): VerifyActionIdResult? {
        try {
            actionResult = sdkInstance?.verifyActionId(actionId)
        } catch (exception: IllegalArgumentException) {
            logErrorAndThrow(
                appContext.getString(R.string.exception_during_action_id_verification_dots_text),
                exception,
                appContext.getString(R.string.incorrect_action_id_exception_text)
            )
        } catch (exception: AccessDeniedException) {
            logErrorAndThrow(
                appContext.getString(R.string.exception_during_action_id_verification_dots_text),
                exception,
                appContext.getString(R.string.wrong_api_credentials_exception_text)
            )
        } catch (exception: UserActionNotFoundException) {
            logErrorAndThrow(
                appContext.getString(R.string.exception_during_action_id_verification_dots_text),
                exception,
                appContext.getString(R.string.unknown_user_action_exception_text)
            )
        } catch (exception: SSLPeerUnverifiedException) {
            logErrorAndThrow(
                appContext.getString(R.string.exception_during_action_id_verification_dots_text),
                exception,
                appContext.getString(R.string.connection_with_pinned_certificate_failed_exception_text)
            )
        } catch (exception: IOException) {
            logErrorAndThrow(
                appContext.getString(R.string.exception_during_action_id_verification_dots_text),
                exception,
                appContext.getString(R.string.api_connection_error_exception_text)
            )
        } catch (exception: IllegalStateException) {
            logErrorAndThrow(
                appContext.getString(R.string.exception_during_action_id_verification_dots_text),
                exception,
                appContext.getString(R.string.user_action_found_but_state_denies_usage_exception_text)
            )
        } catch (exception: Exception) {
            logErrorAndThrow(
                appContext.getString(R.string.exception_during_action_id_verification_dots_text),
                exception
            )
        }

        return actionResult
    }

    internal fun createCall(
        activityContext: Activity,
        viewsPair: Pair<SurfaceViewRenderer, SurfaceViewRenderer>
    ): IVideoCallStateListener? {
        try {
            actionResult?.let {
                videoCall = sdkInstance?.createCall(
                    activityContext, it.userAction, it.userAction.userActionStatus.videoCallInfo
                )
            }
            videoCall?.let {
                val (localeVideoView, remoteVideoView) = viewsPair
                setupListeners(it, localeVideoView, remoteVideoView)
                it.startCall(localeVideoView, remoteVideoView)
            }
        } catch (exception: CallcenterIsClosedException) {

            val message = appContext.getString(R.string.exception_during_creating_call_dots_text)
            Logger.logError(TAG_GENERIC_ERROR, message, exception)
            val addInfo = actionResult?.userAction?.userActionStatus?.videoCallInfo?.nextCallcenterOpening
            webIDErrorsContainer.postAction(
                Result(
                    FAILED, exception = Exception(
                        appContext.getString(
                            R.string.call_center_is_closed_exception_text,
                            addInfo?.toInstant()?.toEpochMilli()?.formatByPattern(pattern = DD_MM_YYYY_HH_MM_SS)
                        )
                    )
                )
            )
        } catch (exception: VideoCallInfoExpiredException) {

            val message = appContext.getString(R.string.exception_during_creating_call_dots_text)
            Logger.logError(TAG_GENERIC_ERROR, message, exception)
            webIDErrorsContainer.postAction(
                Result(
                    FAILED, exception = Exception(
                        appContext.getString(R.string.video_call_inf_expired_exception_text)
                    )
                )
            )
        } catch (exception: AssertNotNullArgumentException) {
            webIDErrorsContainer.postAction(
                Result(
                    FAILED, exception = Exception(
                        appContext.getString(R.string.something_go_wrong_exception_text)
                    )
                )
            )
        }

        return videoCall
    }

    internal fun stopCall() {
        videoCall?.hangUp()
        videoCall = null
    }

    internal fun switchCamera() {
        videoCall?.switchCamera()
    }

    internal fun releaseSDK() {
        sdkInstance = null
    }

    /**
     * This method should be called asynchronously to handle an [Exception] on the UI layer
     */
    @WorkerThread
    internal fun getUserActionStatus(): GetUserActionStatusResult? {
        var userStatus: GetUserActionStatusResult? = null
        try {
            actionResult?.run {
                userStatus = sdkInstance?.getUserActionStatus(userAction)
            }
        } catch (exception: AccessDeniedException) {
            logErrorAndThrow(appContext.getString(R.string.exception_during_getting_user_action_status_dots_text),
                exception)
        } catch (exception: UserActionNotFoundException) {
            logErrorAndThrow(appContext.getString(R.string.exception_during_getting_user_action_status_dots_text),
                exception)
        } catch (exception: IOException) {
            logErrorAndThrow(appContext.getString(R.string.exception_during_getting_user_action_status_dots_text),
                exception)
        } catch (exception: IllegalStateException) {
            logErrorAndThrow(appContext.getString(R.string.exception_during_getting_user_action_status_dots_text),
                exception)
        }

        return userStatus
    }

    /**
     * This method should be called asynchronously to handle an [Exception] on the UI layer
     */
    @WorkerThread
    internal fun verifyTan(tan: String): VerifyTanResult? {
        var tanResult: VerifyTanResult? = null
        try {
            actionResult?.let {
                tanResult = sdkInstance?.verifyTan(it.userAction, tan)
            }
        } catch (exception: IllegalArgumentException) {
            logErrorAndThrow(
                appContext.getString(R.string.exception_during_tan_verification_dots_text),
                exception,
                appContext.getString(R.string.you_input_incorrect_format_tan_exception_text)
            )
        } catch (exception: AccessDeniedException) {
            logErrorAndThrow(
                appContext.getString(R.string.exception_during_tan_verification_dots_text),
                exception
            )
        } catch (exception: UserActionNotFoundException) {
            logErrorAndThrow(
                appContext.getString(R.string.exception_during_tan_verification_dots_text),
                exception
            )
        } catch (exception: IOException) {
            logErrorAndThrow(
                appContext.getString(R.string.exception_during_tan_verification_dots_text),
                exception
            )
        } catch (exception: TanIllegalStateException) {
            logErrorAndThrow(
                appContext.getString(R.string.exception_during_tan_verification_dots_text),
                exception,
                appContext.getString(R.string.user_action_state_denies_tan_verification_exception_text)
            )
        } catch (exception: TanIncorrectException) {
            logErrorAndThrow(
                appContext.getString(R.string.exception_during_tan_verification_dots_text),
                exception,
                appContext.getString(R.string.you_input_invalid_tan_exception_text)
            )
        }

        return tanResult
    }

    private fun setupListeners(
        videoCall: IVideoCallStateListener,
        localeVideoView: SurfaceViewRenderer,
        remoteVideoView: SurfaceViewRenderer
    ) {
        videoCall.let {
            it.addStateChangedListener { newVideoState, _ ->
                when (newVideoState) {
                    MEDIA_STREAM_LOCAL_CREATED -> {
                        localeVideoView.requestLayout()
                        if (::showTANCallback.isInitialized)
                            showTANCallback.invoke()
                    }
                    MEDIA_STREAM_REMOTE_CREATED -> remoteVideoView.requestLayout()
                    IN_CALL_WITH_AGENT -> if (::callWithAgentStartedCallback.isInitialized)
                        callWithAgentStartedCallback.invoke()
                    FAILED_HOLD_THE_LINE_TIMEOUT_REACHED -> {
                        stopCall()
                        val addInfo = actionResult?.userAction?.userActionStatus?.videoCallInfo?.nextCallcenterOpening
                        if (::webIdEdgeCasesHandler.isInitialized)
                            webIdEdgeCasesHandler.invoke(appContext.getString(R.string.call_center_is_closed_exception_text,
                                addInfo?.toInstant()?.toEpochMilli()?.formatByPattern(pattern = DD_MM_YYYY_HH_MM_SS)
                            ))
                    }
                    CALL_DISCONNECTED -> if (::callEndCallback.isInitialized)
                        callEndCallback.invoke()
                    else -> {
                    }
                }
            }
            it.addHoldTheLineProgressChangedListener { newHoldTheLineProgress, _ ->
                if (::userPositionInQueueCallback.isInitialized)
                    userPositionInQueueCallback.invoke(newHoldTheLineProgress)
            }
        }
    }

    private fun logErrorAndThrow(
        logMessage: String,
        exception: Exception,
        throwMessage: String = defaultExceptionMessage
    ) {
        Logger.logError(TAG_GENERIC_ERROR, logMessage, exception)
        throw Exception(throwMessage)
    }
}