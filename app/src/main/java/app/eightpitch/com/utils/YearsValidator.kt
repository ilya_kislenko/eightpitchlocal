package app.eightpitch.com.utils

import java.util.*

object YearsValidator {

    fun validateYearsOld(birthDateMillis: Long): Boolean {
        val calendarCurrentTime = Calendar.getInstance()
        val calendarBirthTime = Calendar.getInstance()

        calendarCurrentTime.time = Date(System.currentTimeMillis())
        calendarBirthTime.time = Date(birthDateMillis)
        calendarBirthTime.add(Calendar.YEAR, Constants.MINIMAL_USER_AGE)

        return calendarCurrentTime.before(calendarBirthTime)
    }
}