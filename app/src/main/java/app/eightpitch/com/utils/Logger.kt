package app.eightpitch.com.utils

import android.util.Log
import app.eightpitch.com.BuildConfig

/**
 * Util class for control logging messages between build types.
 * Mostly for disable the logs on the "release" build type.
 */
object Logger {

    val TAG_INFO = "INFO"
    val TAG_PAGGING = "PAGGING"
    val TAG_GENERIC_ERROR = "GENERIC_ERROR"

    fun logInfo(tag: String, message: String) {
        if (BuildConfig.BUILD_TYPE != "release")
            Log.i(tag, message)
    }

    fun logError(tag: String, message: String, error: Throwable? = Exception()) {
        if (BuildConfig.BUILD_TYPE != "release")
            Log.e(tag, message, error)
    }
}