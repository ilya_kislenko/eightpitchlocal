package app.eightpitch.com.utils

import android.os.Handler
import androidx.annotation.IntRange
import java.util.concurrent.TimeUnit

class Timer {

    private var timeVariable = -1
    private var emptyMessage = -1
    internal var timerListener: TimerListener? = null

    interface TimerListener {
        fun countProcessing(value: Int)
        fun countFinished()
    }

    fun startTimer(@IntRange(from = 0, to = Int.MAX_VALUE.toLong()) seconds: Int) {
        this.timeVariable = seconds
        timerHandler.run {
            removeCallbacks(timerRunnable)
            post(timerRunnable)
            //this variable need to check that we have a some runnable inside, and when we
            //want to continue existing counter
            sendEmptyMessage(emptyMessage)
        }
    }

    fun stopTimer() {
        timeVariable = 0
        timerHandler.removeCallbacks(timerRunnable)
        notifyListener()
    }

    fun pauseCounter() = timerHandler.removeCallbacks(timerRunnable)

    fun resumeCounter(timeVariable: Int): Boolean {
        if (timeVariable < 0)
            return false

        this.timeVariable = timeVariable
        val canResumeCounter = this.timeVariable != 0 && !timerHandler.hasMessages(emptyMessage)

        if (canResumeCounter)
            timerHandler.post(timerRunnable)

        return canResumeCounter;
    }

    private fun notifyListener() {
        timerListener?.countFinished()
    }

    private val timerHandler = Handler()
    private val timerRunnable: Runnable = object : Runnable {
        override fun run() {
            timerListener?.countProcessing(timeVariable)

            --timeVariable

            timerHandler.postDelayed(this, TimeUnit.SECONDS.toMillis(1))

            if (timeVariable == -1)
                stopTimer()
        }
    }
}