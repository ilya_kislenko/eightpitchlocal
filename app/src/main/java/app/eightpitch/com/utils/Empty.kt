package app.eightpitch.com.utils

/**
 * Entity for wrapping an empty request body and preventing passing a null into the system
 */
class Empty private constructor() {
    companion object {
        val instance
            get() = Empty()
    }
}