package app.eightpitch.com.utils

import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.R
import app.eightpitch.com.extensions.inflateView
import app.eightpitch.com.ui.recycleradapters.BindableRecyclerAdapter
import app.eightpitch.com.utils.TextViewsRecyclerAdapter.CommonRecyclerViewHolder
import java.util.*

class TextViewsRecyclerAdapter(private val isWhiteColor: Boolean = false) :
    BindableRecyclerAdapter<String, CommonRecyclerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CommonRecyclerViewHolder(
            inflateView(
                parent,
                R.layout.item_view_common_text
            )
        )

    override fun onBindViewHolder(holder: CommonRecyclerViewHolder, position: Int) {
        val content = dataList[position]
        holder.nameTextView.apply {
            text = content.toUpperCase(Locale.getDefault())
            setTextColor(ContextCompat.getColor(holder.itemView.context, if (isWhiteColor) R.color.white else R.color.middleGray))
        }
    }

    class CommonRecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameTextView: AppCompatTextView = itemView.findViewById(R.id.nameTextView)
    }
}