package app.eightpitch.com.utils

import android.content.Context
import androidx.recyclerview.widget.*
import java.lang.Exception

/**
 * This is a workaround to fix a common recycler view bug according to indexing a children positions
 */
class SafeLinearLayoutManager(context: Context, orientation: Int, reverseLayout: Boolean) :
    LinearLayoutManager(context, orientation, reverseLayout) {

    override fun onLayoutChildren(recycler: RecyclerView.Recycler?, state: RecyclerView.State?) {
        try {
            super.onLayoutChildren(recycler, state)
        } catch (ignore: Exception) {
            Logger.logInfo(SafeLinearLayoutManager::class.java.simpleName, "Layout manager caught an error, $ignore")
        }
    }
}