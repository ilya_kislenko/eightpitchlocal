package app.eightpitch.com.utils.animation

import android.graphics.drawable.Animatable
import android.graphics.drawable.Drawable
import androidx.vectordrawable.graphics.drawable.Animatable2Compat

/**
 * Callback that is working with the vector drawables,
 * and schedules an animation right after it finishes
 */
class AnimationCallback(private val animatableDrawable: Animatable) :
    Animatable2Compat.AnimationCallback() {

    override fun onAnimationEnd(drawable: Drawable?) {
        super.onAnimationEnd(drawable)
        animatableDrawable.start()
    }
}
