package app.eightpitch.com.utils

//TODO Add a java docs
object IBANValidator {

    private const val IBAN_MODULUS = 97
    private const val IBAN_MIN_SIZE = 15
    private const val IBAN_MAX_SIZE = 34
    private const val IBAN_MAX = 999999999

    //TODO add java docs with the links where we get this validation approach
    fun validate(IBAN: String): Boolean {

        val trimmed = IBAN.trim { it <= ' ' }.replace(" ", "")
        if (trimmed.length !in IBAN_MIN_SIZE..IBAN_MAX_SIZE)
            return false

        val traversalString = trimmed.substring(4) + trimmed.substring(0, 4)
        val filtered = traversalString.filter {
            val charValue = Character.getNumericValue(it)
            charValue in 0..35
        }
        if (filtered.length != traversalString.length)
            return false
        var total: Long = 0
        traversalString.map { Character.getNumericValue(it) }.forEach { charValue ->
            total = (if (charValue > 9) total * 100 else total * 10) + charValue
            if (total > IBAN_MAX) {
                total %= IBAN_MODULUS
            }
        }
        return total % IBAN_MODULUS == 1L
    }

}