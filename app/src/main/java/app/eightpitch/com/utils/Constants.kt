package app.eightpitch.com.utils

import app.eightpitch.com.BuildConfig

object Constants {

    const val FAQ_URL = "http://wpkuchen2.bgh.by/"

    //We don't use a BuildConfig.ENDPOINT as a prefix because this links are working only on qa environment
    const val HELP_INVESTORS_URL = "https://web.qa.bgh-dev.xyz/faq/investors"
    const val HELP_INITIATOR_URL = "https://web.qa.bgh-dev.xyz/faq/pi"
    const val LEGAL_URL = "https://web.qa.bgh-dev.xyz/static/terms"

    const val FRONT_APPLICATION_URL = "${BuildConfig.WEB_ENDPOINT}login"

    const val FILES_ENDPOINT = "${BuildConfig.ENDPOINT}project-service/files/"
    const val INVOICE_ENDPOINT = "${BuildConfig.ENDPOINT}project-service/documents/invoices/manual-transfers/"

    const val ONLINE_ABORT_URL = "payment/abort"
    const val ONLINE_CONFIRM_URL = "payment/confirm"

    const val MINIMAL_USER_AGE = 18

    const val RANK_DR = "DR"

    const val INVOICE_PDF_FILE_NAME = "invoice.pdf"

    const val GA_PACKAGE_NAME = "com.google.android.apps.authenticator2"

    //Date formats
    const val YYYY_MM_DD = "yyyy-MM-dd"
    const val MMMM_DD_YYYY = "MMMM dd, yyyy"
    const val DD_p_MM_p_YYYY = "dd.MM.yyyy"
    const val DD_MMMM_YYYY = "dd.MMMM yyyy"
    const val DD_SLASH_MM_SLASH_YYYY = "dd/MM/yyyy"
    const val DD_DOT_MM_DOT_YY_HH_DOT_MM = "dd.MM.yy HH:mm"
    const val DD_MM_YYYY_HH_MM_SS_ZONE = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    const val DD_MM_YYYY_HH_MM_SS = "yyyy-MM-dd HH:mm:ss"
    const val DD_DOT_MM_DOT_YYYY_HH_MM_SS = "dd.MM.yyyy HH:mm:ss"

    //Default backend pattern
    const val DEFAULT_DATA_PATTERN = "yyyy-MM-dd HH:mm:ssZ"

    const val IBAN_MIN_SIZE = 15
    const val IBAN_MAX_SIZE = 34
    const val IBAN_MODULUS = 97
    const val IBAN_MAX = 999999999

    const val DELETE_REDIRECT_DURATION = 10000L
}
