package app.eightpitch.com.utils

object RegexPatterns {
    const val PATTERN_PREFIX_CORRECT = "[a-zA-ZÀ-ÿ]"
    const val PATTERN_OWNER_PREFIX_CORRECT = "[a-zA-ZÀ-ÿ\\s]"
    const val PATTERN_VALID_CITY = "[\\'\\-a-zA-ZÀ-ÿ\\d]+\$"
    const val PATTERN_VALID_CODE = "[a-zA-ZÀ-ÿ0-9]"
    const val PATTERN_VALID_NAME = "^[A-Za-zÀ-ÿ- ]*[^0-9]\$"
    const val PATTERN_VALID_OWNER = "^[\\'\\-a-zA-ZÀ-ÿ\\d\\s]+\$"
    const val PATTERN_VALID_PASSWORD_SYMBOLS = "^(?=.*[A-Z])(?=.*[!@#\$&*])(?=.*[0-9]).{8,}\$"
    const val PATTERN_VALID_BIRTH_PLACES = "^[a-zA-ZÀ-ÿ0-9\\'\\-\\s]+\$"
}