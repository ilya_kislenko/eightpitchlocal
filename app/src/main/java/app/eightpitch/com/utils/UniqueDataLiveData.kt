package app.eightpitch.com.utils

import androidx.annotation.WorkerThread
import androidx.lifecycle.MutableLiveData

class UniqueDataLiveData<T> : MutableLiveData<T>() {

    /**
     * Checks current data with new
     * If equals - return, otherwise post new value
     */
    @WorkerThread
    fun postAction(data: T) {
        if (data == value) return
        postValue(data)
    }
}