package app.eightpitch.com.utils.constructor

import android.util.Base64
import androidx.annotation.WorkerThread
import app.eightpitch.com.data.dto.dynamicviews.*
import app.eightpitch.com.data.dto.dynamicviews.ProjectPageBlock.CREATOR.BUTTON_BLOCK
import app.eightpitch.com.data.dto.dynamicviews.ProjectPageBlock.CREATOR.CHART
import app.eightpitch.com.data.dto.dynamicviews.ProjectPageBlock.CREATOR.DEPARTMENT
import app.eightpitch.com.data.dto.dynamicviews.ProjectPageBlock.CREATOR.DISCLAIMER
import app.eightpitch.com.data.dto.dynamicviews.ProjectPageBlock.CREATOR.DIVIDER
import app.eightpitch.com.data.dto.dynamicviews.ProjectPageBlock.CREATOR.DUAL_COLUMN_IMAGES
import app.eightpitch.com.data.dto.dynamicviews.ProjectPageBlock.CREATOR.DUAL_COLUMN_TEXT
import app.eightpitch.com.data.dto.dynamicviews.ProjectPageBlock.CREATOR.FACTS
import app.eightpitch.com.data.dto.dynamicviews.ProjectPageBlock.CREATOR.FAQ
import app.eightpitch.com.data.dto.dynamicviews.ProjectPageBlock.CREATOR.GROUP
import app.eightpitch.com.data.dto.dynamicviews.ProjectPageBlock.CREATOR.HEADER
import app.eightpitch.com.data.dto.dynamicviews.ProjectPageBlock.CREATOR.ICONS
import app.eightpitch.com.data.dto.dynamicviews.ProjectPageBlock.CREATOR.IMAGE
import app.eightpitch.com.data.dto.dynamicviews.ProjectPageBlock.CREATOR.IMAGE_LINK
import app.eightpitch.com.data.dto.dynamicviews.ProjectPageBlock.CREATOR.LINKED_TEXT
import app.eightpitch.com.data.dto.dynamicviews.ProjectPageBlock.CREATOR.LIST
import app.eightpitch.com.data.dto.dynamicviews.ProjectPageBlock.CREATOR.TABLE
import app.eightpitch.com.data.dto.dynamicviews.ProjectPageBlock.CREATOR.TEXT
import app.eightpitch.com.data.dto.dynamicviews.ProjectPageBlock.CREATOR.TEXT_AND_IMAGE
import app.eightpitch.com.data.dto.dynamicviews.ProjectPageBlock.CREATOR.TILED_IMAGES
import app.eightpitch.com.data.dto.dynamicviews.ProjectPageBlock.CREATOR.VIDEO
import app.eightpitch.com.extensions.getLocalizedText
import com.google.gson.Gson
import java.lang.IllegalStateException

/**
 * Representation of String as a Project Details tab name,
 * and a UI Elements for the particular tab
 */
typealias ProjectDetailsTab = Pair<TabViewOrder, List<UIElement>>

/**
 * Class that is designed to parse all the details for a
 * project details dynamic tabs and UI elements for a particular
 * tab
 */
@WorkerThread
class ProjectDetailsTabsContentParser {

    /**
     * This method should be called asynchronously
     */
    internal fun parseProjectDetailsTabsBlock(projectDetailsJson: String): ProjectDetailsTabsBlock {
        val json = if (projectDetailsJson.first() == '{') projectDetailsJson else decodeResponse(projectDetailsJson)
        if (projectDetailsJson.isNotEmpty())
            return Gson().fromJson(json, ProjectDetailsTabsBlock::class.java)
        else
            throw IllegalStateException("Project details is empty")
    }

    private fun decodeResponse(projectDetailsJson: String) =
        try {
            Base64.decode(projectDetailsJson.toByteArray(), Base64.DEFAULT).toString(Charsets.ISO_8859_1)
        } catch (e: Exception) {
            projectDetailsJson
        }

    internal fun parseUIItems(objectConstructorProjectDetailsTabs: ProjectDetailsTabsBlock): ArrayList<ProjectDetailsTab> {
        val GSON = Gson()
        val items =
            objectConstructorProjectDetailsTabs.tabs.map {
                val i18nData = objectConstructorProjectDetailsTabs.i18nData
                val tabInfo = GSON.fromJson(it.payload, RootViewsContainer::class.java)
                TabViewOrder(i18nData.getLocalizedText(tabInfo.nameTranslationCode), tabInfo) to it.blocks
            }
        val projectDetailsTabs = ArrayList<ProjectDetailsTab>()
        items.forEach { tabs ->
            val (tabViewOrder, projectPageBlock) = tabs

            val uiElements = ArrayList<UIElement>()
            projectPageBlock.forEach { blocks ->
                //Json representation of a single UI element
                val payload = blocks.payload
                val uiElement = when (blocks.type) {
                    TEXT -> GSON.fromJson(payload, TextUIElement::class.java)
                    IMAGE -> GSON.fromJson(payload, ImageUIElement::class.java)
                    TEXT_AND_IMAGE -> GSON.fromJson(payload, ImageAndTextUIElement::class.java)
                    FAQ -> GSON.fromJson(payload, FAQUIElement::class.java)
                    LIST -> GSON.fromJson(payload, ListUIElement::class.java)
                    TABLE -> GSON.fromJson(payload, TableUIElement::class.java)
                    ICONS -> GSON.fromJson(payload, IconsUIElement::class.java)
                    VIDEO -> GSON.fromJson(payload, VimeoUIElement::class.java)
                    DIVIDER -> GSON.fromJson(payload, DividerUIElement::class.java)
                    CHART -> GSON.fromJson(payload, ChartItems::class.java)
                    FACTS -> GSON.fromJson(payload, FactsUIElement::class.java)
                    TILED_IMAGES -> GSON.fromJson(payload, TiledImagesUIElement::class.java)
                    IMAGE_LINK -> GSON.fromJson(payload, ImageLinkUIElement::class.java)
                    DUAL_COLUMN_IMAGES -> GSON.fromJson(payload, DualColumnImagesUIElement::class.java)
                    DUAL_COLUMN_TEXT -> GSON.fromJson(payload, DualColumnTextUIElement::class.java)
                    BUTTON_BLOCK -> GSON.fromJson(payload, ButtonUIElement::class.java)
                    DISCLAIMER -> GSON.fromJson(payload, DisclaimerUiElement::class.java)
                    LINKED_TEXT -> GSON.fromJson(payload, LinkedTextUIElement::class.java)
                    HEADER -> GSON.fromJson(payload, HeaderUIElement::class.java)
                    DEPARTMENT -> GSON.fromJson(payload, DepartmentUIElement::class.java)
                    GROUP -> Group(_idList = blocks.children)
                    else -> GSON.fromJson(blocks.payload, Group::class.java)
                }
                uiElement?.let {
                    it._id = blocks.id
                    uiElements.add(it)
                }
            }
            projectDetailsTabs.add(tabViewOrder to uiElements)
        }
        return projectDetailsTabs
    }
}