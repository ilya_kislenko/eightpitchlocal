package app.eightpitch.com.utils

import app.eightpitch.com.utils.Constants.DD_p_MM_p_YYYY
import app.eightpitch.com.utils.Logger.TAG_GENERIC_ERROR
import java.time.*
import java.time.format.DateTimeFormatter
import java.util.*

object DateUtils {

    private val zoneIdUTC = ZoneOffset.UTC
    private val zoneIdDefault = ZoneId.systemDefault().rules.getStandardOffset(Instant.now())

    private fun getTimezoneOffset(defaultOne: Boolean) = when {
        defaultOne -> zoneIdDefault
        else -> zoneIdUTC
    }

    /**
     * Method that will format the milliseconds
     * timestamp within the [Long] to a [String]
     * representation by specified pattern,
     * including device timezone or not.
     *
     * @param pattern - it's a [String] pattern to apply
     * @param timeZoneIncluding - determine whether the
     * formatting be in a device default timezone or a UTC
     * If `true` is used here, the default device timezone
     * will affect the result and will increase/decrease timestamp
     * according to a default device timezone.
     * @param locale - determines in what locale
     * the formatting will be, affects only a names of months/days, etc.
     *
     * @return formatted [String] or empty String if formatting is impossible
     */
    fun Long.formatByPattern(
        pattern: String = DD_p_MM_p_YYYY,
        timeZoneIncluding: Boolean = true,
        locale: Locale = Locale.getDefault()
    ): String {
        return try {
            val instant = Instant.ofEpochMilli(this)
            val formatter = DateTimeFormatter.ofPattern(pattern, locale)
            ZonedDateTime.ofInstant(instant, getTimezoneOffset(timeZoneIncluding)).format(formatter) ?: ""
        } catch (formattingException: Exception) {
            Logger.logError(
                TAG_GENERIC_ERROR,
                "Cannot format $this timestamp by $pattern, cause:",
                formattingException
            )
            ""
        }
    }

    /**
     * Method that will parse string representation of the time
     * by passed [pattern] and will return a timestamp in UTC-0
     *
     * @param pattern - it's a [String] pattern to apply
     * @param locale - determines in what locale the formatting
     * will be applied
     *
     * @return a timestamp in UTC-0 or -1 if parsing isn't possible
     */
    fun String.parseByFormat(
        locale: Locale = Locale.US,
        pattern: String = DD_p_MM_p_YYYY
    ): Long {
        return try {
            val formatter = DateTimeFormatter.ofPattern(pattern, locale)
            val localDate = LocalDateTime.parse(this, formatter)
            return localDate.toInstant(ZoneOffset.UTC).toEpochMilli()
        } catch (formattingException: Exception) {
            Logger.logError(
                TAG_GENERIC_ERROR,
                "Cannot format $this by $pattern, cause:",
                formattingException
            )
            -1
        }
    }

    fun String.parseByFormatWithoutMMSS(
        locale: Locale = Locale.US,
        pattern: String = DD_p_MM_p_YYYY
    ): Long {
        return try {
            val formatter = DateTimeFormatter.ofPattern(pattern, locale)
            val localDate = LocalDate.parse(this, formatter).atStartOfDay()
            return localDate.toInstant(ZoneOffset.UTC).toEpochMilli()
        } catch (formattingException: Exception) {
            Logger.logError(
                TAG_GENERIC_ERROR,
                "Cannot format $this by $pattern, cause:",
                formattingException
            )
            -1
        }
    }

    /**
     * Method that converts a [LocalDate]
     * to an [Instant] safe. Method will return
     * current time milliseconds if parsing isn't possible
     */
    fun LocalDate.toInstant(): Long {
        return try {
            atStartOfDay(zoneIdDefault).toInstant().toEpochMilli()
        } catch (parseException: Exception) {
            System.currentTimeMillis()
        }
    }

    /**
     * Method that will transform a [Long] to a [LocalDateTime]
     * appending a timezone offset or not
     */
    fun Long.toLocalDateTime(timeZoneIncluding: Boolean = true): LocalDateTime {
        return try {
            val instant = Instant.ofEpochMilli(this)
            LocalDateTime.ofInstant(instant, getTimezoneOffset(timeZoneIncluding))
        } catch (parseException: Exception) {
            Logger.logError(TAG_GENERIC_ERROR, "Cannot transform $this to a LocalDateTime, cause:", parseException)
            LocalDateTime.ofInstant(Instant.now(), zoneIdDefault)
        }
    }
}