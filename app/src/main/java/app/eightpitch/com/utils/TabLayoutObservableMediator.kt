package app.eightpitch.com.utils

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentManager.FragmentLifecycleCallbacks
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.google.android.material.tabs.TabLayoutMediator.TabConfigurationStrategy

class TabLayoutObservableMediator<T : Fragment>(
    private val unsubscribeType: Class<T>,
    fragmentManager: FragmentManager,
    tabLayout: TabLayout,
    viewPager: ViewPager2,
    tabConfigurationStrategy: TabConfigurationStrategy,
) {

    private var tabLayoutMediator: TabLayoutMediator? = null

    init {
        fragmentManager.registerFragmentLifecycleCallbacks(object : FragmentLifecycleCallbacks() {
            override fun onFragmentViewDestroyed(fm: FragmentManager, fragment: Fragment) {
                super.onFragmentViewDestroyed(fm, fragment)
                if (fragment.javaClass == unsubscribeType) {
                    tabLayoutMediator?.detach()
                    tabLayoutMediator = null
                    fragmentManager.unregisterFragmentLifecycleCallbacks(this)
                }
            }
        }, false)

        tabLayoutMediator = TabLayoutMediator(tabLayout, viewPager, tabConfigurationStrategy)
    }

    fun attach() {
        tabLayoutMediator?.attach()
    }
}