package app.eightpitch.com.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.fragment.app.Fragment

object IntentsController {

    fun startActivityWithANewStack(
        context: Context,
        toActivityClass: Class<out Activity>,
    ) {

        val intent = Intent(context, toActivityClass)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        context.startActivity(intent)
    }

    fun browseFiles(
        host: Fragment,
        resultCode: Int = 1,
        fileType: String = "*/*",
    ) {

        val intent = Intent(Intent.ACTION_GET_CONTENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = fileType
        }

        host.startActivityForResult(intent, resultCode)
    }

    fun redirectToPlayMarket(context: Context, packageName: String) {
        val url = try {
            //Check whether Google Play store is installed or not
            context.packageManager.getPackageInfo(packageName, 0)
            "market://details?id=$packageName";
        } catch (exception: Exception) {
            "https://play.google.com/store/apps/details?id=$packageName"
        }
        //Open the app page in Google Play store:
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        context.startActivity(intent)
    }
}