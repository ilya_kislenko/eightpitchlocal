package app.eightpitch.com.ui.twofactor.choosetwoauthmethod

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.base.baseviewmodels.codeverification.DefaultCodeValidationFragment
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO
import app.eightpitch.com.data.dto.signup.CommonInvestorRequestBody
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.extensions.showDialog
import app.eightpitch.com.ui.twofactor.gettwoauthcode.KycTwoFactorGetCodeFragment
import app.eightpitch.com.ui.twofactor.installapp.KycTwoFactorInstallAppFragment
import app.eightpitch.com.utils.ExternalApplicationsManager
import kotlinx.android.synthetic.main.fragment_choose_two_auth_method.*

class ChooseTwoAuthMethodFragment : BaseFragment<ChooseTwoAuthMethodFragmentViewModel>() {

    companion object {

        internal const val NAVIGATE_FROM_KEY = "NAVIGATE_FROM_KEY"
        internal const val NAVIGATE_FROM_KYC = "NAVIGATE_FROM_KYC"
        internal const val NAVIGATE_FROM_SECURITY = "NAVIGATE_FROM_SECURITY"

        fun buildWithAnArguments(
            navigateFrom: String
        ) = Bundle().apply {
            putString(NAVIGATE_FROM_KEY, navigateFrom)
        }
    }

    override fun getLayoutID() = R.layout.fragment_choose_two_auth_method

    override fun getVMClass() = ChooseTwoAuthMethodFragmentViewModel::class.java

    private val navigateFrom: String
        get() = arguments?.getString(NAVIGATE_FROM_KEY) ?: ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindLoadingIndicator()

        var navigateOpenAppId =
            R.id.action_chooseTwoAuthMethodFragment_to_kycTwoFactorInstallAppFragment
        var navigateGetCodeId =
            R.id.action_chooseTwoAuthMethodFragment_to_kycTwoFactorGetCodeFragment
        var navigateDefaultCodeId =
            R.id.action_chooseTwoAuthMethodFragment_to_defaultCodeValidationFragment

        when (navigateFrom) {
            NAVIGATE_FROM_KYC -> {
                navigateOpenAppId =
                    R.id.action_chooseTwoAuthMethodFragment_to_kycTwoFactorInstallAppFragment
                navigateGetCodeId =
                    R.id.action_chooseTwoAuthMethodFragment_to_kycTwoFactorGetCodeFragment
                navigateDefaultCodeId =
                    R.id.action_chooseTwoAuthMethodFragment_to_defaultCodeValidationFragment
                skipTextView.visibility = View.VISIBLE
                registerOnBackPressedListener { popBackStack(R.id.kycAddExtraSecurityFragment) }
            }
            NAVIGATE_FROM_SECURITY -> {
                navigateOpenAppId =
                    R.id.action_chooseTwoAuthMethodFragment_to_kycTwoFactorInstallAppFragment
                navigateGetCodeId =
                    R.id.action_chooseTwoAuthMethodFragment_to_kycTwoFactorGetCodeFragment
                navigateDefaultCodeId =
                    R.id.action_chooseTwoAuthMethodFragment_to_defaultCodeValidationFragment

                skipTextView.visibility = View.GONE
                registerOnBackPressedListener { popBackStack(R.id.profileSecurityFragment) }
            }
        }

        twoFactorSmsSwitch.setOnClickListener {
            if (viewModel.twoFactorAppSwitchEnabledField.get() == false) {
                viewModel.requestSMSCode()
            } else {
                twoFactorSmsSwitch.isChecked = false
                showDialog(message = getString(R.string.choose_two_auth_dialog_message))
            }
        }

        twoAuthAppFactorSwitch.setOnClickListener {
            if (viewModel.twoFactorSmsSwitchEnabledField.get() == false) {
                viewModel.getQrCode()
            } else {
                twoAuthAppFactorSwitch.isChecked = false
                showDialog(message = getString(R.string.choose_two_auth_dialog_message))
            }
        }

        skipTextView.setOnClickListener {
            navigate(R.id.action_chooseTwoAuthMethodFragment_to_kycPersonalInfoFragment)
        }

        viewModel.getQrCodeResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) { qrResponse ->
                val link = "otpauth://totp/${qrResponse.keyId}?secret=${qrResponse.secret}"
                context?.let {
                    if (viewModel.twoFactorAppSwitchEnabledField.get() == false) {
                        if (!ExternalApplicationsManager.isGoogleAuthenticatorInstalled(it)) {
                            navigate(
                                navigateOpenAppId,
                                KycTwoFactorInstallAppFragment.buildWithAnArguments(
                                    navigateFrom,
                                    link,
                                    KycTwoFactorGetCodeFragment.GA_ENABLE
                                )
                            )
                        } else {
                            navigate(
                                navigateGetCodeId,
                                KycTwoFactorGetCodeFragment.buildWithAnArguments(
                                    navigateFrom,
                                    link,
                                    KycTwoFactorGetCodeFragment.GA_ENABLE
                                )
                            )
                        }
                    } else {
                        val arguments =
                            DefaultCodeValidationFragment.buildWithAnArguments(
                                twoAuthNavigateFrom = navigateFrom,
                                validationScreenInfo = createValidationScreenInfo(),
                                partialUser = CommonInvestorRequestBody(
                                    interactionId = "",
                                    phoneNumber = ""
                                )
                            )
                        navigate(
                            navigateDefaultCodeId,
                            arguments
                        )
                    }
                }
            }
        })

        viewModel.getSMSResult().observe(viewLifecycleOwner, Observer { result ->
            handleResult(result) { pair ->
                val (interactionId, phoneNumber) = pair
                val arguments =
                    DefaultCodeValidationFragment.buildWithAnArguments(
                        twoAuthNavigateFrom = navigateFrom,
                        validationScreenInfo = createSMSValidationScreenInfo(),
                        partialUser = CommonInvestorRequestBody(
                            interactionId = interactionId,
                            phoneNumber = phoneNumber
                        )
                    )
                navigate(
                    navigateDefaultCodeId,
                    arguments
                )
            }
        })

        viewModel.getUserMeResult()
            .observe(viewLifecycleOwner, { result -> handleResult(result) {} })
        viewModel.getUser()
    }

    private fun createSMSValidationScreenInfo() = ValidationScreenInfoDTO(
        verificationType = if (viewModel.twoFactorSmsSwitchEnabledField.get() == false)
            ValidationScreenInfoDTO.TWO_AUTH_VALIDATION_SMS_ENABLE
        else
            ValidationScreenInfoDTO.TWO_AUTH_VALIDATION_SMS_DISABLE,
        validationButtonName = getString(R.string.confirm_text),
        header = getString(R.string.two_factor_authentication),
        toolbarTitle = getString(R.string.two_factor_authentication),
        subTitleMessage = getString(R.string.we_have_sent_sms_to_login_text)
    )

    private fun createValidationScreenInfo() = ValidationScreenInfoDTO(
        verificationType = ValidationScreenInfoDTO.TWO_AUTH_VALIDATION_GA_DISABLE,
        validationButtonName = getString(R.string.confirm_text),
        header = getString(R.string.enter_code_from_ga),
        toolbarTitle = getString(R.string.two_factor_authentication),
        subTitleMessage = getString(R.string.enter_code_from_ga_description)
    )
}
