package app.eightpitch.com.ui.dynamicdetails

import android.view.View
import androidx.lifecycle.*
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.dynamicviews.UIElement
import app.eightpitch.com.data.dto.dynamicviews.language.I18nData
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.ui.projects.mappers.UIElementsConverter
import app.eightpitch.com.utils.SingleActionLiveData
import app.eightpitch.com.utils.models.VimeoModel
import kotlinx.coroutines.launch
import javax.inject.Inject

class DynamicProjectDetailsChildViewModel
@Inject constructor(
    private val vimeoModel: VimeoModel,
    private val uiElementsConverter: UIElementsConverter,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    /**
     * Method that is converting a [UIElement]
     * to a corresponded [View]
     */
    internal fun convertUiElements(items: List<UIElement>, i18nData: I18nData): List<View> =
        uiElementsConverter.convertUIElements(items, i18nData)

    internal fun convertTabletUiElements(items: List<UIElement>, i18nData: I18nData): List<View> =
        uiElementsConverter.convertTabletUIElements(items, i18nData)

    private val videoUrlLinkResult = SingleActionLiveData<Result<String>>()
    internal fun getVideoUrlLinkResult(): LiveData<Result<String>> = videoUrlLinkResult

    internal fun retrieveVideoLink(projectPromoVideoLink: String) {

        viewModelScope.launch {

            val result = asyncLoading {
                vimeoModel.getVimeoLinkOfVideo(720, projectPromoVideoLink)
            }

            videoUrlLinkResult.postAction(result)
        }
    }
}