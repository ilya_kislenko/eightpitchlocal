package app.eightpitch.com.ui.kyc.country

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.webid.WebIdUserRequestBody
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.ui.kyc.street.KycStreetInfoFragment
import kotlinx.android.synthetic.main.fragment_kyc_country_info.*

class KycCountryInfoFragment : BaseFragment<KycCountryInfoFragmentViewModel>() {

    companion object {

        internal const val KYC_BODY = "KYC_BODY"

        fun buildWithAnArguments(
            webIdUserRequestBody: WebIdUserRequestBody
        ) = Bundle().apply {
            putParcelable(KYC_BODY, webIdUserRequestBody)
        }
    }

    override fun getLayoutID() = R.layout.fragment_kyc_country_info

    override fun getVMClass() = KycCountryInfoFragmentViewModel::class.java

    private val webIdUserRequestBody: WebIdUserRequestBody
        get() = arguments?.getParcelable(KYC_BODY) ?: WebIdUserRequestBody()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        setupCountrySpinner()

        buttonNext.setOnClickListener {
            if (viewModel.validateInput()) {
                val kycArguments = viewModel.addCountryInfoToKycBody(webIdUserRequestBody)
                navigate(
                    R.id.action_kycCountryInfoFragment_to_kycStreetInfoFragment,
                    KycStreetInfoFragment.buildWithAnArguments(kycArguments)
                )
            }
        }

        viewModel.getUserMeResult().observe(viewLifecycleOwner, Observer { result ->
            handleResult(result) { user ->
                countrySpinner.setSelection(viewModel.getUserCountryArrayIndex(user))
            }
        })

        viewModel.loadUserOrRestoreState()
    }

    private fun setupCountrySpinner() {
        countrySpinner.apply {
            val countryAdapter = CountrySpinnerAdapter(context)
            setAdapter(countryAdapter)
            setSelection(countryAdapter.getHintPosition())
            setSelectedListener({
                viewModel.setCountry(it)
            }, {})
        }
    }
}