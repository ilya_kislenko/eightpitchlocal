package app.eightpitch.com.ui.dashboard.initiator

import android.content.Context
import androidx.databinding.ObservableField
import androidx.lifecycle.*
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.data.transmit.ResultStatus.*
import app.eightpitch.com.extensions.*
import app.eightpitch.com.models.ProjectModel
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.coroutines.launch

class InitiatorWelcomeViewModel(
    private val appContext: Context,
    private val projectModel: ProjectModel,
    private val userModel: UserModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val toolbarTitle = appContext.getString(R.string.project_initiator_dashboard_text)
    val welcomeText = ObservableField("")

    /**
     * Result that is describing the state of user's projects, true if user has created some.
     */
    private val initiatorResult = SingleActionLiveData<Result<Boolean>>()
    internal fun getInitiatorResult(): LiveData<Result<Boolean>> = initiatorResult

    internal fun retrieveData() {
        viewModelScope.launch {
            val result = asyncLoading {
                val projects = projectModel.getInitiatorProjects().projects.isNotEmpty()
                val user = userModel.getUserData()
                projects to user
            }

            if (SUCCESS == result.status) {
                val user = result.result!!.second
                welcomeText.set(appContext.getString(R.string.welcome_pattern_text, user.getWelcomeAppeal(appContext)))
            }

            val haveProjects = result.result?.first.orDefaultValue(false)
            initiatorResult.postAction(result.wrapWithAnotherResult(haveProjects))
        }
    }
}