package app.eightpitch.com.ui.authorized

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.ViewTreeObserver
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseActivity
import app.eightpitch.com.base.rootbackhandlers.MainTabbedNavGraphHandleAbleActivity
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.authorized.main.MainNavigationAdapter
import app.eightpitch.com.ui.authorized.main.MainScreenTab
import app.eightpitch.com.ui.authorized.main.homepager.NavigationApplier
import app.eightpitch.com.ui.pincode.ChoiceBiometricDialog
import app.eightpitch.com.ui.pincode.ChoiceBiometricDialog.Companion.ACTIVATE_LATER
import app.eightpitch.com.ui.pincode.ChoiceBiometricDialog.Companion.USE_FINGER_PRINT
import app.eightpitch.com.ui.pincode.ChoiceBiometricDialog.Companion.USE_PIN_CODE
import app.eightpitch.com.ui.profilesecurity.quicklogin.QuickLoginSettingActivity
import app.eightpitch.com.ui.profilesecurity.quicklogin.QuickLoginSettingActivity.Companion.QUICK_LOGIN_NAVIGATION_ID
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.activity_authorized.*

class AuthorizedActivity : MainTabbedNavGraphHandleAbleActivity(), BottomTabsHolder {

    lateinit var viewModel: AuthorizedActivityViewModel
    private var globalListener: ViewTreeObserver.OnGlobalLayoutListener? = null

    private var quickLoginDialog: ChoiceBiometricDialog? = ChoiceBiometricDialog()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authorized)

        val factory = (this as BaseActivity).viewModelFactory
        viewModel = ViewModelProvider(this, factory)[AuthorizedActivityViewModel::class.java]

        viewModel.getNotificationsStatusResult().observe(this, { result ->
            handleResult(result) { isVisible ->
                unreadNotificationsIndicator.isVisible = isVisible
            }
        })

        mainNavigationViewPager.apply {
            isUserInputEnabled = false
            adapter = MainNavigationAdapter(this@AuthorizedActivity)
            addOnPageSelectedCallback { position -> viewModel.onPageChanged(position) }
        }

        pageTabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                //Nothing.
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                //Nothing.
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                val root = (getCurrentFragment() as? NavigationApplier) ?: return
                root.popUpToStartDestination()
            }

        })

        globalListener = authContainer?.addTabLayoutVisibilityManager(pageTabLayout)

        if (viewModel.shouldShowBiometricDialog())
            showQuickLoginDialog()

        initTabLayout()
        syncWithNotificationsStatus()
    }

    override fun onDestroy() {
        super.onDestroy()
        authContainer?.viewTreeObserver?.removeOnGlobalLayoutListener(globalListener)
    }

    private fun showQuickLoginDialog() {
        quickLoginDialog?.apply {
            onNavigationCodeSelected = { navigationCode ->
                when (navigationCode) {
                    USE_PIN_CODE -> openQuickLoginSettingActivity(
                        context,
                        R.id.createPinCodeFragment,
                        PIN_CODE_ENABLE_KEY
                    )
                    USE_FINGER_PRINT -> openQuickLoginSettingActivity(
                        context,
                        R.id.fingerPrintWithEnterPinCodeFragment,
                        BIOMETRIC_SET_UP_START
                    )
                    ACTIVATE_LATER -> {
                        viewModel.postponeQuickLogin()
                        dismiss()
                    }
                }
            }
        }?.show(supportFragmentManager, ChoiceBiometricDialog::class.java.name)
    }

    private fun openQuickLoginSettingActivity(context: Context?, navigationID: Int, requestCode: Int) {
        startActivityForResult(
            Intent(context, QuickLoginSettingActivity::class.java).apply {
                putExtra(QUICK_LOGIN_NAVIGATION_ID, navigationID)
            },
            requestCode
        )
    }

    private fun syncWithNotificationsStatus() {
        //This indicator solutions works only for mobile versions for now
        if (!resources.getBoolean(R.bool.isItTabletDevice))
            viewModel.getNotificationsStatus()
    }

    private fun initTabLayout() {
        val tabTitles = MainScreenTab.getBottomTabNames(applicationContext)
        val tabBackgrounds = MainScreenTab.getBottomTabSelectors()
        TabLayoutMediator(pageTabLayout, mainNavigationViewPager) { tab, position ->
            tab.run {
                text = tabTitles[position]
                setIcon(tabBackgrounds[position])
            }
        }.attach()
        mainNavigationViewPager.apply {
            setCurrentItem(0, true)
            offscreenPageLimit = 3
        }
    }

    override fun getCurrentFragment(): Fragment? {
        val navigationPagerAdapter = mainNavigationViewPager.adapter as? MainNavigationAdapter
        return navigationPagerAdapter?.getCurrentFragment(mainNavigationViewPager.currentItem)
    }

    //region BottomTabsHolder
    override fun switchTabTo(position: Int) {
        pageTabLayout.selectTabWith(position)
    }

    override fun switchTabWith(position: Int, navigationId: Int) {
        pageTabLayout.selectTabWith(position)
        val navigationPagerAdapter = mainNavigationViewPager.adapter as? MainNavigationAdapter
        val navigationHelper = navigationPagerAdapter?.getCurrentFragment(position) as NavigationApplier
        navigationHelper.applyNavigationState(navigationId)
    }

    override fun getRecentSelectedTab() = viewModel.selectedTab

    override fun syncBottomTabsIndicators() {
        syncWithNotificationsStatus()
    }
    //endregion

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && quickLoginDialog?.isVisible == true) {
            quickLoginDialog?.dismiss()
            quickLoginDialog = null
        }
    }
}