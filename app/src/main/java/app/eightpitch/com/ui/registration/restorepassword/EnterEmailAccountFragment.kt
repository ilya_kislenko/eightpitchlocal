package app.eightpitch.com.ui.registration.restorepassword

import android.os.Bundle
import android.view.View
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.base.baseviewmodels.codeverification.DefaultCodeValidationFragment
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO.CREATOR.RESTORE_PASSWORD
import app.eightpitch.com.data.dto.signup.CommonInvestorRequestBody
import app.eightpitch.com.extensions.handleResult
import kotlinx.android.synthetic.main.fragment_enter_email_to_restore.*

class EnterEmailAccountFragment : BaseFragment<EnterEmailAccountViewModel>() {

    override fun getLayoutID() = R.layout.fragment_enter_email_to_restore

    override fun getVMClass() = EnterEmailAccountViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindLoadingIndicator()

        buttonNext.setOnClickListener {
            viewModel.validateEmail()
        }

        viewModel.getSendInitialSmsCodeResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) { smsCodeResponse ->
                val arguments = DefaultCodeValidationFragment.buildWithAnArguments(
                    validationScreenInfo = createValidationScreenInfo(),
                    partialUser = CommonInvestorRequestBody(interactionId = smsCodeResponse.interactionId)
                )
                navigate(R.id.action_enterEmailAccountFragment_to_defaultCodeValidationFragment, arguments)
            }
        })
    }

    private fun createValidationScreenInfo() = ValidationScreenInfoDTO(
        verificationType = RESTORE_PASSWORD,
        validationButtonName = getString(R.string.confirm_text),
        header = getString(R.string.account_confirmation_text),
        toolbarTitle = getString(R.string.password_recovery_text),
        subTitleMessage = getString(R.string.recovery_password_message_text)
    )
}