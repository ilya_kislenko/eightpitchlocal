package app.eightpitch.com.ui.mock

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import app.eightpitch.com.R
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO
import app.eightpitch.com.data.dto.signup.CommonInvestorRequestBody
import app.eightpitch.com.extensions.navigate
import app.eightpitch.com.ui.twofactor.choosetwoauthmethod.ChooseTwoAuthMethodFragment

class EmptyFragment : Fragment() {

    companion object {

        private const val PROJECT_ID = "PROJECT_ID"
        private const val PARTIAL_USER = "PARTIAL_USER"
        private const val INVESTMENT_ID = "INVESTMENT_ID"
        private const val DELETING_REASON = "DELETING_REASON"
        private const val VALIDATION_KEY_CODE = "VALIDATION_KEY_CODE"

        fun buildWithAnArguments(
            projectId: String = "",
            investmentId: String = "",
            deletingReason: String = "",
            twoAuthNavigateFrom: String = "",
            validationScreenInfo: ValidationScreenInfoDTO,
            partialUser: CommonInvestorRequestBody
        ) = Bundle().apply {
            putString(PROJECT_ID, projectId)
            putString(INVESTMENT_ID, investmentId)
            putString(DELETING_REASON, deletingReason)
            putString(ChooseTwoAuthMethodFragment.NAVIGATE_FROM_KEY, twoAuthNavigateFrom)
            putParcelable(VALIDATION_KEY_CODE, validationScreenInfo)
            putParcelable(PARTIAL_USER, partialUser)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_empty, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navigate(R.id.action_emptyFragment_to_digitCodeValidationFragment, arguments ?: Bundle())
    }
}
