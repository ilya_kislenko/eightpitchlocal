package app.eightpitch.com.ui.kyc.street

import android.content.Context
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.webid.WebIdUserRequestBody
import app.eightpitch.com.extensions.*
import app.eightpitch.com.utils.RegexPatterns.PATTERN_VALID_CODE
import app.eightpitch.com.utils.RegexPatterns.PATTERN_VALID_NAME
import app.eightpitch.com.utils.SingleActionObservableString
import app.eightpitch.com.views.interfaces.ShortFieldValidator
import javax.inject.Inject

class KycStreetInfoFragmentViewModel @Inject constructor(
    private val appContext: Context,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val title = appContext.getString(R.string.kyc_street_title)

    private var street: String = ""
    val streetErrorHolder = SingleActionObservableString()

    val streetWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            street = sequence.toString()
        }
    }

    private var number: String = ""
    val numberErrorHolder = SingleActionObservableString()

    val numberWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            number = sequence.toString()
        }
    }

    internal fun addStreetInfoToKycBody(webIdUserRequestBody: WebIdUserRequestBody): WebIdUserRequestBody =
        webIdUserRequestBody.copy(
            address = webIdUserRequestBody.address.copy(
                street = street, streetNumber = number
            )
        )

    internal fun validateInput(): Boolean {

        val streetError =
            street.validateByDefaultPattern(appContext, R.string.street, PATTERN_VALID_NAME, "")
        if (streetError.isNotEmpty())
            streetErrorHolder.set(streetError)

        val streetNumberError =
            number.validateByDefaultPattern(appContext, R.string.number, PATTERN_VALID_CODE, "")
        if (streetNumberError.isNotEmpty())
            numberErrorHolder.set(streetNumberError)

        return streetError.isEmpty() && streetNumberError.isEmpty()
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        street = ""
        number = ""
        streetErrorHolder.set("")
        numberErrorHolder.set("")
    }
}