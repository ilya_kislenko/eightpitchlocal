package app.eightpitch.com.ui.kyc.yourbirth

import android.os.Bundle
import android.view.View
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.webid.WebIdUserRequestBody
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.kyc.nationality.KycNationalityFragment
import app.eightpitch.com.utils.Constants
import kotlinx.android.synthetic.main.fragment_user_birth_info.*
import java.util.*

class UserBirthInfoFragment : BaseFragment<UserBirthInfoFragmentViewModel>() {

    companion object {

        internal const val KYC_BODY = "KYC_BODY"

        fun buildWithAnArguments(
            webIdUserRequestBody: WebIdUserRequestBody
        ) = Bundle().apply {
            putParcelable(KYC_BODY, webIdUserRequestBody)
        }
    }

    override fun getLayoutID() = R.layout.fragment_user_birth_info

    override fun getVMClass() = UserBirthInfoFragmentViewModel::class.java

    private val webIdUserRequestBody: WebIdUserRequestBody
        get() = arguments?.getParcelable(KYC_BODY) ?: WebIdUserRequestBody()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        buttonNext.setOnClickListener {
            if (viewModel.validateInput())
                moveForward()
        }

        textInputLayoutBirthDate.setOnFieldClickListener {
            showDatePickerDialog(
                selectedDateInMillis = if (viewModel.birthDateMillis == 0L)
                    Calendar.getInstance().run {
                        time = Calendar.getInstance().apply {
                            add(Calendar.YEAR, -Constants.MINIMAL_USER_AGE)
                            add(Calendar.DAY_OF_MONTH, -1)
                        }.time
                        timeInMillis
                    } else viewModel.birthDateMillis
            ) { localDate ->
                textInputLayoutBirthDate.setText("")
                viewModel.setBirthDate(localDate)
            }
        }

        viewModel.getUserMeResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) {}
        })

        viewModel.loadUserOrRestoreState()
    }

    private fun moveForward() {
        val kycInfoArguments = viewModel.addBirthInfoToBody(webIdUserRequestBody)
        navigate(
            R.id.action_userBirthInfoFragment_to_kycNationalityFragment,
            KycNationalityFragment.buildWithAnArguments(kycInfoArguments)
        )
    }
}
