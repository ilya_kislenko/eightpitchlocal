package app.eightpitch.com.ui.profilepayments

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.ui.profilepayments.adapters.TransferAdapter
import app.eightpitch.com.ui.profilepayments.tranferdetails.TransferDetailsFragment
import app.eightpitch.com.utils.SafeLinearLayoutManager
import kotlinx.android.synthetic.main.fragment_transfers_list_layout.*

class TransfersListFragment : BaseFragment<TransfersListViewModel>() {

    override fun getLayoutID() = R.layout.fragment_transfers_list_layout

    override fun getVMClass() = TransfersListViewModel::class.java

    override fun onResume() {
        super.onResume()
        viewModel.getTransfers()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        setupPaymentRecycler(view.context)

        viewModel.getTransfersResult().observe(viewLifecycleOwner, {
            handleResult(it){}
        })
    }

    private fun setupPaymentRecycler(context: Context) {
        val horizontalDecoration = DividerItemDecoration(
            context,
            DividerItemDecoration.VERTICAL
        ).apply {
            val horizontalDivider =
                ContextCompat.getDrawable(context, R.drawable.horizontal_divider)
            horizontalDivider?.let { setDrawable(it) }
        }
        transfersRecycler.apply {
            layoutManager = SafeLinearLayoutManager(context, VERTICAL, false)
            adapter = TransferAdapter().apply {
                onTransferClicked = { transferItem ->
                    navigate(
                        R.id.action_profilePaymentsFragment_to_transferDetailsFragment,
                        TransferDetailsFragment.buildWithAnArguments(transferItem)
                    )
                }
            }
            addItemDecoration(horizontalDecoration)
        }
    }
}