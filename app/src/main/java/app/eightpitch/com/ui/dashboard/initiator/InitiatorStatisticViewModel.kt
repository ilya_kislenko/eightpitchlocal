package app.eightpitch.com.ui.dashboard.initiator

import android.content.Context
import androidx.databinding.*
import androidx.lifecycle.*
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.project.InitiatorProjectDetails
import app.eightpitch.com.data.dto.project.InitiatorProjectDetails.Companion.activeStatuses
import app.eightpitch.com.data.dto.project.InitiatorProjectDetails.Companion.finishedStatuses
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.ACTIVE
import app.eightpitch.com.data.dto.user.LinkSliderItemResponseBody
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.data.transmit.ResultStatus.SUCCESS
import app.eightpitch.com.extensions.wrapWithAnEmptyResult
import app.eightpitch.com.models.*
import app.eightpitch.com.ui.projects.mappers.mapToInitiatorProjectItem
import app.eightpitch.com.utils.*
import kotlinx.coroutines.launch

class InitiatorStatisticViewModel(
    private val appContext: Context,
    private val projectModel: ProjectModel,
    private val userModel: UserModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    private val ACTIVE_PROJECTS = 0
    private val FINISHED_PROJECTS = 1

    val toolbarTitle = appContext.getString(R.string.project_initiator_dashboard_text)

    internal var savedTabPosition: Int = 0
    val tabLayoutVisibility = ObservableBoolean()
    val welcomeText = ObservableField("")
    val sliderTabsVisibility = ObservableBoolean()

    /**
     * @see allProjects is need for finding differences by status
     */
    private val allProjects = ObservableArrayList<InitiatorProjectDetails>()
    val projectsByType = ObservableArrayList<InitiatorProjectItem>()

    private val initiatorResult = SingleActionLiveData<Result<Empty>>()
    internal fun getInitiatorResult(): LiveData<Result<Empty>> = initiatorResult

    internal fun retrieveData() {
        viewModelScope.launch {
            val result = asyncLoading {
                val projects = projectModel.getInitiatorProjects().projects
                val user = userModel.getUserData()
                getDashboardSliderInfo()
                projects to user
            }

            if (SUCCESS == result.status) {
                val (projects, user) = result.result!!
                putAllProjects(projects)
                welcomeText.set(appContext.getString(R.string.welcome_pattern_text, user.getWelcomeAppeal(appContext)))
            }

            initiatorResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    private val sliderResult = UniqueDataLiveData<Result<List<LinkSliderItemResponseBody>>>()
    internal fun getSliderResult(): LiveData<Result<List<LinkSliderItemResponseBody>>> = sliderResult

    private fun getDashboardSliderInfo() {
        viewModelScope.launch {
            val result = asyncLoading {
                userModel.getDashboardSliderInfo()
            }

            if (SUCCESS == result.status)
                changeSliderVisibilityBy(result.result!!.size)

            sliderResult.postAction(result)
        }
    }

    private fun putAllProjects(investorProjects: List<InitiatorProjectDetails>) {
        allProjects.apply {
            clear()
            addAll(investorProjects)
        }
        projectsByType.clear()
        allProjects.run {
            if (find { it.projectStatus == ACTIVE } != null
                && find { project -> finishedStatuses.contains(project.projectStatus) } != null)
                tabLayoutVisibility.set(true)
            else {
                tabLayoutVisibility.set(false)
                projectsByType.addAll(investorProjects.map { it.mapToInitiatorProjectItem() })
            }
        }
    }

    internal fun getProjectsForType(tabPosition: Int) {
        savedTabPosition = tabPosition
        when (tabPosition) {
            ACTIVE_PROJECTS -> putRunningProjects(allProjects)
            FINISHED_PROJECTS -> putFinishedProjects(allProjects)
        }
    }

    private fun putRunningProjects(items: List<InitiatorProjectDetails>) {
        val filteredItems = items.filter { project -> activeStatuses.contains(project.projectStatus) }
            .map { it.mapToInitiatorProjectItem() }
        projectsByType.apply {
            clear()
            addAll(filteredItems)
        }
    }

    private fun putFinishedProjects(items: List<InitiatorProjectDetails>) {
        val initiatorProjects = items.filter { project -> finishedStatuses.contains(project.projectStatus) }
            .map { it.mapToInitiatorProjectItem() }
        projectsByType.apply {
            clear()
            addAll(initiatorProjects)
        }
    }

    private fun changeSliderVisibilityBy(amount: Int) {
        sliderTabsVisibility.set(amount > 0)
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        savedTabPosition = 0
        allProjects.clear()
        projectsByType.clear()
        tabLayoutVisibility.set(false)
        sliderTabsVisibility.set(false)
    }
}