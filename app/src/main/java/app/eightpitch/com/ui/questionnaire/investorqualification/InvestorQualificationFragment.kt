package app.eightpitch.com.ui.questionnaire.investorqualification

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.View
import android.widget.AdapterView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import app.eightpitch.com.BuildConfig
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.questionnaire.UploadedFilesAdapter
import app.eightpitch.com.extensions.*
import app.eightpitch.com.utils.IntentsController
import app.eightpitch.com.utils.SafeLinearLayoutManager
import kotlinx.android.synthetic.main.file_manager_layout.*
import kotlinx.android.synthetic.main.fragment_investor_qualification.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class InvestorQualificationFragment : BaseFragment<InvestorQualificationViewModel>() {

    override fun getLayoutID() = R.layout.fragment_investor_qualification

    override fun getVMClass() = InvestorQualificationViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()
        setupQualificationSpinnerSpinner()
        toolbarBackButton.setImageDrawable(ContextCompat.getDrawable(view.context, R.drawable.icon_cross))

        with(conditionsCheckBox) {
            text = resources.getString(R.string.investment_qualification_condition_text,"${BuildConfig.WEB_ENDPOINT}static/privacy").toHtml()
            movementMethod = LinkMovementMethod.getInstance()
        }
        nextButton.setOnClickListener {
            val description = textInputDescription.getText().toString()
            if (!conditionsCheckBox.isChecked || !viewModel.validateFields(description))
                showDialog(message = getString(R.string.please_fill_all_the_required_fields_to_continue_text))
        }
        cancelButton.setDisablingClickListener { moveBackward() }

        viewModel.run {
            getUploadFileResult().observe(viewLifecycleOwner, { handleResult(it) })
            getDeleteFileResult().observe(viewLifecycleOwner, { handleResult(it) })
            getQuestionnaireFormResult().observe(viewLifecycleOwner, {
                handleResult(it) {
                    navigate(R.id.action_investorQualificationFragment_to_questionnaireFirstFragment)
                }
            })
        }

        addFilesButton.setOnClickListener {
            IntentsController.browseFiles(this, UPLOAD_FILES)
        }

        uploadedFilesRecycler.apply {
            layoutManager = SafeLinearLayoutManager(context, VERTICAL, false)
            adapter = UploadedFilesAdapter().apply {
                deleteFileCallback = { viewModel.deleteFile(it) }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && requestCode == UPLOAD_FILES) {
            data?.data?.also { fileUri ->
                viewModel.uploadFile(fileUri)
            }
        }
    }

    private fun setupQualificationSpinnerSpinner() {
        qualificationSpinner.apply {
            //TODO("will be implemented in the future")
            /*context.run {
                adapter = DefaultSpinnerAdapter(
                    this,
                    resources.getStringArray(R.array.investorQualificationStatuses).toList()
                )
                onItemSelectedListener = SpinnerSelectionListener()
            }*/
        }
    }

    private inner class SpinnerSelectionListener : AdapterView.OnItemSelectedListener {

        override fun onNothingSelected(parent: AdapterView<*>?) {
            //Do nothing.
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            viewModel.setQualification(position)
            activity?.hideKeyboard()
        }
    }
}