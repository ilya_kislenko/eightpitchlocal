package app.eightpitch.com.ui.kyc.summaruinformation

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.webid.WebIdUserRequestBody
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.extensions.injectViewModel
import app.eightpitch.com.extensions.toHtml
import app.eightpitch.com.ui.kyc.editsummary.KycEditSummaryInfoFragment
import app.eightpitch.com.ui.kyc.editsummary.SharableViewModel
import app.eightpitch.com.ui.kyc.summatyconfirmed.SummaryConfirmedFragment
import kotlinx.android.synthetic.main.fragment_kyc_summary_info.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class KycSummaryInfoFragment : BaseFragment<KycSummaryInfoFragmentViewModel>() {

    companion object {

        internal const val KYC_BODY = "KYC_BODY"

        fun buildWithAnArguments(
            webIdUserRequestBody: WebIdUserRequestBody
        ) = Bundle().apply {
            putParcelable(KYC_BODY, webIdUserRequestBody)
        }
    }

    private val webIdUserRequestBody: WebIdUserRequestBody
        get() = arguments?.getParcelable(KYC_BODY) ?: WebIdUserRequestBody()

    override fun getLayoutID() = R.layout.fragment_kyc_summary_info

    override fun getVMClass() = KycSummaryInfoFragmentViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        applySharedState()
        setupUI()

        additionalToolbarRightButton.setOnClickListener {
            navigate(
                R.id.action_kycSummaryInfoFragment_to_kycEditSummaryInfoFragment,
                KycEditSummaryInfoFragment.buildWithAnArguments(webIdUserRequestBody)
            )
        }

        viewModel.getUpdateUserPersonalDataResult().observe(viewLifecycleOwner, Observer { result ->
            handleResult(result) {
                val arguments = SummaryConfirmedFragment.buildWithAnArguments(it.actionId)
                navigate(R.id.action_kycSummaryInfoFragment_to_summaryConfirmedFragment, arguments)
            }
        })

        buttonConfirm.setOnClickListener {
            viewModel.updateUserPersonalData(webIdUserRequestBody)
        }
    }

    private fun setupUI() {
        describeSummaryInfo.text = resources.getString(R.string.kyc_summary_description).toHtml()
        additionalToolbarRightButton.setImageResource(R.drawable.icon_edit)
        viewModel.setKycField(webIdUserRequestBody)
    }

    private fun applySharedState() {
        val sharableViewModel = injectViewModel(SharableViewModel::class.java)
        sharableViewModel.getKycInfo()?.let {
            arguments?.putParcelable(KYC_BODY, it)
            sharableViewModel.webIdUserRequestBody = null
        }
    }
}