package app.eightpitch.com.ui.investment

import android.view.*
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.R
import app.eightpitch.com.extensions.inflateView
import app.eightpitch.com.ui.investment.ClickableFilesAdapter.FilesViewHolder
import app.eightpitch.com.ui.recycleradapters.BindableRecyclerAdapter
import app.eightpitch.com.utils.Constants.FILES_ENDPOINT

class ClickableFilesAdapter :
    BindableRecyclerAdapter<Pair<String, String>, FilesViewHolder>() {

    lateinit var filesUrlCallback: (String) -> Unit

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        FilesViewHolder(
            inflateView(
                parent,
                R.layout.item_view_clickable_files
            )
        )

    override fun onBindViewHolder(holder: FilesViewHolder, position: Int) {
        val (fileName, url) = dataList[position]
        holder.fileName.apply {
            text = fileName
            tag = position
            setOnClickListener {
                if (::filesUrlCallback.isInitialized){
                    val tagPosition = it.tag as Int
                    val (_, tagUrl) = dataList[tagPosition]
                    filesUrlCallback.invoke("$FILES_ENDPOINT$tagUrl")
                }
            }
        }
    }

    class FilesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val fileName: AppCompatTextView = itemView.findViewById(R.id.fileNameText)
    }
}