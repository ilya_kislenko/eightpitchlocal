package app.eightpitch.com.ui.profilepersonal

import android.content.Context
import android.view.View.*
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.*
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.user.Country.Companion.toDisplayValue
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.data.dto.user.User.CREATOR.INSTITUTIONAL_INVESTOR
import app.eightpitch.com.data.dto.user.User.CREATOR.LEVEL_2
import app.eightpitch.com.data.dto.user.User.CREATOR.PRIVATE_INVESTOR
import app.eightpitch.com.data.dto.user.User.CREATOR.PROJECT_INITIATOR
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.data.transmit.ResultStatus.SUCCESS
import app.eightpitch.com.extensions.wrapWithAnEmptyResult
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.utils.Constants.DD_p_MM_p_YYYY
import app.eightpitch.com.utils.Constants.YYYY_MM_DD
import app.eightpitch.com.utils.DateUtils.formatByPattern
import app.eightpitch.com.utils.DateUtils.parseByFormatWithoutMMSS
import app.eightpitch.com.utils.Empty
import app.eightpitch.com.utils.SingleActionLiveData
import app.eightpitch.com.utils.defaultstrokeviews.StrokeViewItem
import kotlinx.coroutines.launch
import javax.inject.Inject

class ProfilePersonalFragmentViewModel @Inject constructor(
    private val appContext: Context,
    private val userModel: UserModel,
    threadsSeparator: ThreadsSeparator,
) : BaseViewModel(threadsSeparator) {

    val title = appContext.getString(R.string.personal_screen_toolbar_title)

    //region binding fields
    val companyNameField = ObservableField("")
    val companyTypeField = ObservableField("")
    val companyNumberField = ObservableField("")

    val genderTypeField = ObservableField("")
    val userFullNameField = ObservableField("")
    val userDateOfBirthField = ObservableField("")
    val userPlaceOfBirthField = ObservableField("")
    val userNationalityField = ObservableField("")

    val userCountryField = ObservableField("")
    val userCityField = ObservableField("")
    val userZipCodeField = ObservableField("")
    val userStreetField = ObservableField("")
    val userStreetNumberField = ObservableField("")

    val userAccountOwnerField = ObservableField("")
    val userIbanField = ObservableField("")

    val emailField = ObservableField("")
    val phoneNumberField = ObservableField("")

    val userPoliticallyField = ObservableField("")
    val userLiabilityField = ObservableField("")
    //endregion fields

    //region binding visibility
    val companyInformationVisibility = ObservableField<Int>(GONE)

    val personalBaseInformationVisibility = ObservableField<Int>(GONE)
    val dateOfBirthVisibility = ObservableField<Int>(GONE)
    val placeOfBirthVisibility = ObservableField<Int>(GONE)
    val nationalityVisibility = ObservableField<Int>(GONE)

    val addressInformationVisibility = ObservableField<Int>(GONE)
    val financialInformationVisibility = ObservableField<Int>(GONE)
    val contactInformationVisibility = ObservableField<Int>(GONE)
    val otherInformationVisibility = ObservableField<Int>(GONE)

    val germanTaxLiabilityItem = StrokeViewItem(
        ObservableField(appContext.getString(R.string.german_church_tax_liability_text)),
        ObservableInt(R.drawable.icon_german_tax_liability)
    )
    val taxIdItem = StrokeViewItem(
        ObservableField(appContext.getString(R.string.tax_id_text)),
        ObservableInt(R.drawable.icon_tax_id)
    )
    val taxOfficeItem = StrokeViewItem(
        ObservableField(appContext.getString(R.string.tax_office_text)),
        ObservableInt(R.drawable.icon_tax_office)
    )
    val churchTaxLiabilityItem = StrokeViewItem(
        ObservableField(appContext.getString(R.string.church_tax_liability_text)),
        ObservableInt(R.drawable.icon_church_tax_liability)
    )
    val churchItem = StrokeViewItem(
        ObservableField(appContext.getString(R.string.church_text)),
        ObservableInt(R.drawable.icon_church)
    )
    //endregion fields visibility

    val upgradeButtonVisibility = ObservableField<Int>(GONE)

    private val userMeResult = SingleActionLiveData<Result<Empty>>()
    internal fun getUserMeResult(): LiveData<Result<Empty>> = userMeResult

    internal fun getUser() {

        viewModelScope.launch {

            val result = asyncLoading {
                userModel.getUserData()
            }

            if (result.status == SUCCESS) {
                setProfileFields(result.result!!)
            }

            userMeResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    private fun setProfileFields(user: User) {

        if (user.companyName.isNotEmpty())
            companyNameField.set(user.companyName)
        if (user.companyType.isNotEmpty())
            companyTypeField.set(user.companyType)
        if (user.commercialRegisterNumber.isNotEmpty())
            companyNumberField.set(user.commercialRegisterNumber)

        if (user.prefix.isNotEmpty())
            genderTypeField.set(User.PrefixType.toDisplayValue(appContext, user.prefix))

        userFullNameField.set("${user.firstName} ${user.lastName}")

        if (user.dateOfBirth.isNotEmpty()) {
            val birthDateMillis = user.dateOfBirth.parseByFormatWithoutMMSS(pattern = YYYY_MM_DD)
            userDateOfBirthField.set(birthDateMillis.formatByPattern(DD_p_MM_p_YYYY))
        }

        if (user.placeOfBirth.isNotEmpty())
            userPlaceOfBirthField.set(user.placeOfBirth)
        if (user.nationality.isNotEmpty())
            userNationalityField.set(toDisplayValue(appContext, user.nationality))

        val userAddress = user.userAddress
        userAddress.run {
            if (user.userAddress.country.isNotEmpty())
                userCountryField.set(toDisplayValue(appContext, userAddress.country))
            if (user.userAddress.city.isNotEmpty())
                userCityField.set(userAddress.city)
            if (user.userAddress.zipCode.isNotEmpty())
                userZipCodeField.set(userAddress.zipCode)
            if (user.userAddress.street.isNotEmpty())
                userStreetField.set(userAddress.street)
            if (user.userAddress.streetNumber.isNotEmpty())
                userStreetNumberField.set(userAddress.streetNumber)
        }

        if (user.accountOwner.isNotEmpty())
            userAccountOwnerField.set(user.accountOwner)
        if (user.iban.isNotEmpty())
            userIbanField.set(user.iban)

        if (user.email.isNotEmpty())
            emailField.set(user.email)
        if (user.phoneNumber.isNotEmpty())
            phoneNumberField.set("+${user.phoneNumber}")

        userPoliticallyField.set(
            if (user.politicallyExposedPerson) appContext.getString(R.string.yes) else appContext.getString(
                R.string.no
            )
        )
        userLiabilityField.set(
            if (user.usTaxLiability) appContext.getString(R.string.yes) else appContext.getString(
                R.string.no
            )
        )
        user.taxInformation?.run {
            val germanTaxLiability =
                if (isTaxResidentInGermany) appContext.getString(R.string.yes) else appContext.getString(R.string.no)
            germanTaxLiabilityItem.value.set(germanTaxLiability)

            taxIdItem.value.set(taxNumber)
            taxOfficeItem.value.set(responsibleTaxOffice)

            val churchTaxLiability =
                if (churchTaxLiability) appContext.getString(R.string.yes) else appContext.getString(R.string.no)
            churchTaxLiabilityItem.value.set(churchTaxLiability)
            churchItem.value.set(churchTaxAttribute)
        }

        val isCompleteUser = user.status == LEVEL_2

        upgradeButtonVisibility.set(if (user.canUpgradeAccount()) VISIBLE else GONE)

        if (isCompleteUser) {
            when (user.role) {
                INSTITUTIONAL_INVESTOR -> {
                    companyInformationVisibility.set(VISIBLE)
                    personalBaseInformationVisibility.set(VISIBLE)
                    dateOfBirthVisibility.set(VISIBLE)
                    placeOfBirthVisibility.set(VISIBLE)
                    nationalityVisibility.set(VISIBLE)
                    addressInformationVisibility.set(VISIBLE)
                    financialInformationVisibility.set(VISIBLE)
                    contactInformationVisibility.set(VISIBLE)
                    otherInformationVisibility.set(VISIBLE)
                }
                PRIVATE_INVESTOR -> {
                    companyInformationVisibility.set(GONE)
                    personalBaseInformationVisibility.set(VISIBLE)
                    dateOfBirthVisibility.set(VISIBLE)
                    placeOfBirthVisibility.set(VISIBLE)
                    nationalityVisibility.set(VISIBLE)
                    addressInformationVisibility.set(VISIBLE)
                    financialInformationVisibility.set(VISIBLE)
                    contactInformationVisibility.set(VISIBLE)
                    otherInformationVisibility.set(VISIBLE)
                }
                else -> {
                    companyInformationVisibility.set(GONE)
                    personalBaseInformationVisibility.set(GONE)
                    dateOfBirthVisibility.set(GONE)
                    placeOfBirthVisibility.set(GONE)
                    nationalityVisibility.set(GONE)
                    addressInformationVisibility.set(GONE)
                    financialInformationVisibility.set(GONE)
                    contactInformationVisibility.set(GONE)
                    otherInformationVisibility.set(GONE)
                }
            }
        } else {
            when (user.role) {
                INSTITUTIONAL_INVESTOR -> {
                    companyInformationVisibility.set(VISIBLE)
                    personalBaseInformationVisibility.set(VISIBLE)
                    dateOfBirthVisibility.set(GONE)
                    placeOfBirthVisibility.set(GONE)
                    nationalityVisibility.set(GONE)
                    addressInformationVisibility.set(GONE)
                    financialInformationVisibility.set(GONE)
                    contactInformationVisibility.set(VISIBLE)
                    otherInformationVisibility.set(GONE)
                }
                PRIVATE_INVESTOR -> {
                    companyInformationVisibility.set(GONE)
                    personalBaseInformationVisibility.set(VISIBLE)
                    dateOfBirthVisibility.set(GONE)
                    placeOfBirthVisibility.set(GONE)
                    nationalityVisibility.set(GONE)
                    addressInformationVisibility.set(GONE)
                    financialInformationVisibility.set(GONE)
                    contactInformationVisibility.set(VISIBLE)
                    otherInformationVisibility.set(GONE)
                }
                PROJECT_INITIATOR -> {
                    companyInformationVisibility.set(VISIBLE)
                    personalBaseInformationVisibility.set(VISIBLE)
                    dateOfBirthVisibility.set(GONE)
                    placeOfBirthVisibility.set(GONE)
                    nationalityVisibility.set(GONE)
                    addressInformationVisibility.set(GONE)
                    financialInformationVisibility.set(GONE)
                    contactInformationVisibility.set(VISIBLE)
                    otherInformationVisibility.set(GONE)
                }
            }
        }

        user.taxInformation?.let {
            otherInformationVisibility.set(VISIBLE)
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        companyNameField.set("")
        companyTypeField.set("")

        genderTypeField.set("")
        userFullNameField.set("")
        userDateOfBirthField.set("")
        userPlaceOfBirthField.set("")
        userNationalityField.set("")

        userCountryField.set("")
        userCityField.set("")
        userZipCodeField.set("")
        userStreetField.set("")
        userStreetNumberField.set("")

        userAccountOwnerField.set("")
        userIbanField.set("")

        emailField.set("")
        phoneNumberField.set("")

        userPoliticallyField.set("")
        userLiabilityField.set("")

        companyInformationVisibility.set(GONE)
        personalBaseInformationVisibility.set(GONE)
        dateOfBirthVisibility.set(GONE)
        placeOfBirthVisibility.set(GONE)
        nationalityVisibility.set(GONE)
        addressInformationVisibility.set(GONE)
        financialInformationVisibility.set(GONE)
        contactInformationVisibility.set(GONE)
        otherInformationVisibility.set(GONE)

        germanTaxLiabilityItem.value.set("")
        taxIdItem.value.set("")
        taxOfficeItem.value.set("")
        churchTaxLiabilityItem.value.set("")
        churchItem.value.set("")

        upgradeButtonVisibility.set(GONE)
    }
}