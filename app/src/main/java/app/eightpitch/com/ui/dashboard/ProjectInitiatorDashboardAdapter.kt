package app.eightpitch.com.ui.dashboard

import android.view.*
import androidx.appcompat.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.R
import app.eightpitch.com.data.dto.projectdetails.ProjectsFile.CREATOR.BACKGROUND_IMAGE
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.dashboard.initiator.InitiatorProjectItem
import app.eightpitch.com.ui.recycleradapters.BindableRecyclerAdapter
import app.eightpitch.com.utils.Constants.FILES_ENDPOINT
import app.eightpitch.com.views.HorizontalProgressBarView
import com.bumptech.glide.load.resource.bitmap.*
import java.util.*

/**
 * Adapter for representation projects on InitiatorStatisticFragment
 */
class ProjectInitiatorDashboardAdapter :
    BindableRecyclerAdapter<InitiatorProjectItem, ProjectInitiatorDashboardAdapter.PagedProjectViewHolder>() {

    /**
     * Listener that represents a click on any project in the list,
     * will pass a [InitiatorProjectItem] when click is performed
     */
    internal lateinit var onProjectClicked: (InitiatorProjectItem) -> (Unit)
    internal lateinit var onDetailsClicked: (InitiatorProjectItem) -> (Unit)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PagedProjectViewHolder(
        inflateView(
            parent,
            R.layout.item_view_initiator_dashboard_project
        )
    )

    override fun getItemId(position: Int): Long {
        return getDataItem(position).id.toLongSafe()
    }

    override fun onBindViewHolder(holder: PagedProjectViewHolder, position: Int) {

        val projectItem = getDataItem(position)

        holder.apply {
            val context = itemView.context
            val imageUrl = projectItem.projectFiles.findImageUrlByTag(BACKGROUND_IMAGE)
            context.loadImage(
                "$FILES_ENDPOINT${imageUrl}",
                targetImageView = videoPreView,
                placeholderRef = R.drawable.icon_app_name_logo,
                transformation = arrayOf(
                    CenterCrop(),
                    GranularRoundedCorners(0F,
                        0F,
                        context.resources.getDimension(R.dimen.margin_10),
                        context.resources.getDimension(R.dimen.margin_10))
                )
            )
            projectItem.run {
                setupProgressBarView(this, holder.fundingProgressBar)
                headerProjectTextView.text = companyName
                describeProjectTextView.text = description
                capitalInvestedTextView.text = String.format(
                    context.getString(R.string.token_placeholder),
                    projectBi.capitalInvested.toLong().asCurrency()
                )
                timeLeftTextView.text = projectItem.tokenParametersDocument.projectFinishDate.getDaysDuration(context)

                averageInvestmentTextView.text = String.format(
                    context.getString(R.string.token_placeholder),
                    projectBi.averageInvestment.toLong().asCurrency()
                )
                numberOfInvestorTextView.text = projectBi.investors.totalCount.toString()
                highestInvestmentTextView.text = String.format(
                    context.getString(R.string.token_placeholder),
                    projectBi.maxInvestment.toLong().asCurrency()
                )
                projectBi.maxInvestment.toString()
                lowestInvestmentTextView.text =
                    String.format(
                        context.getString(R.string.token_placeholder),
                        projectBi.maxInvestment.toLong().asCurrency()
                    )
                projectBi.minInvestment.toString()
            }

            parentView.apply {
                tag = projectItem
                setOnClickListener {
                    (it.tag as? InitiatorProjectItem)?.let(onProjectClicked)
                }
                moreDetailsButton.also {
                    it.setOnClickListener {
                        (tag as? InitiatorProjectItem)?.let(onDetailsClicked)
                    }
                }
            }
        }
    }

    private fun setupProgressBarView(
        projectItem: InitiatorProjectItem,
        fundingProgressBar: HorizontalProgressBarView
    ) {

        val currentFundingSum = projectItem.graph.currentFundingSum
        fundingProgressBar.run {
            setCurrentProgress(currentFundingSum)
            setCurrentTextProgress(
                String.format(
                    Locale.getDefault(),
                    context.getString(R.string.percent_string),
                    currentFundingSum
                )
            )
        }

        val context = fundingProgressBar.context
        val softCap = projectItem.graph.softCap
        val (cap, textProgress) = if (softCap == 0 || softCap == null) {
            projectItem.graph.hardCap to context.getString(R.string.hard_cap)
        } else {
            softCap.orDefaultValue(0) to context.getString(R.string.soft_cap)
        }

        fundingProgressBar.run {
            setCapProgress(cap)
            setCapTextProgress(textProgress)
        }
    }

    class PagedProjectViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val fundingProgressBar: HorizontalProgressBarView =
            itemView.findViewById(R.id.fundingProgressBar)
        val parentView: ConstraintLayout = itemView.findViewById(R.id.parentView)
        val videoPreView: AppCompatImageView = itemView.findViewById(R.id.projectPreviewImage)
        val headerProjectTextView: AppCompatTextView =
            itemView.findViewById(R.id.headerProjectTextView)
        val describeProjectTextView: AppCompatTextView =
            itemView.findViewById(R.id.describeProjectTextView)
        val capitalInvestedTextView: AppCompatTextView =
            itemView.findViewById(R.id.capitalInvestedTextView)
        val timeLeftTextView: AppCompatTextView =
            itemView.findViewById(R.id.timeLeftTextView)
        val averageInvestmentTextView: AppCompatTextView =
            itemView.findViewById(R.id.averageInvestmentTextView)
        val numberOfInvestorTextView: AppCompatTextView =
            itemView.findViewById(R.id.numberOfInvestorTextView)
        val highestInvestmentTextView: AppCompatTextView =
            itemView.findViewById(R.id.highestInvestmentTextView)
        val lowestInvestmentTextView: AppCompatTextView =
            itemView.findViewById(R.id.lowestInvestmentTextView)
        val moreDetailsButton: AppCompatTextView =
            itemView.findViewById(R.id.moreDetailsButton)
    }
}