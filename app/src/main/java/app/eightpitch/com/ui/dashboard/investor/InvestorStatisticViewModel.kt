package app.eightpitch.com.ui.dashboard.investor

import android.content.Context
import androidx.databinding.*
import androidx.lifecycle.*
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.investment.*
import app.eightpitch.com.data.dto.project.InvestorsProject
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.ACTIVE
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.finishedStatuses
import app.eightpitch.com.data.dto.user.LinkSliderItemResponseBody
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.data.transmit.ResultStatus.SUCCESS
import app.eightpitch.com.extensions.*
import app.eightpitch.com.models.ProjectModel
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.ui.projects.adapters.ProjectItem
import app.eightpitch.com.ui.projects.mappers.mapToProjectItem
import app.eightpitch.com.utils.*
import kotlinx.coroutines.launch
import java.math.BigDecimal
import java.math.RoundingMode.HALF_EVEN
import kotlin.random.Random.Default.nextInt

class InvestorStatisticViewModel(
    private val appContext: Context,
    private val projectModel: ProjectModel,
    private val userModel: UserModel,
    threadsSeparator: ThreadsSeparator,
) : BaseViewModel(threadsSeparator) {

    private val ACTIVE_PROJECTS = 0
    private val FINISHED_PROJECTS = 1

    val toolbarTitle = appContext.getString(R.string.investor_dashboard_text)

    internal var savedTabPosition: Int = 0
    val tabLayoutVisibility = ObservableBoolean()
    val totalField = ObservableField("")
    val welcomeText = ObservableField("")
    val sliderTabsVisibility = ObservableBoolean()

    val twoSegments = ObservableArrayList<String>()
    val fourSegments = ObservableArrayList<String>()
    val eightSegments = ObservableArrayList<String>()

    //region Data list
    val typedProjects = ObservableArrayList<ProjectItem>()
    val graphItems = ObservableArrayList<GraphsInvestment>()

    /**
     * @see allProjects is need for finding differences by status
     */
    private val allProjects = ObservableArrayList<InvestorsProject>()
    //endregion

    private val investorGraphResult = SingleActionLiveData<Result<InvestorGraph>>()
    internal fun getInvestorGraphResult(): LiveData<Result<InvestorGraph>> = investorGraphResult

    internal fun retrieveData() {
        viewModelScope.launch {

            val result = asyncLoading {
                val graph = projectModel.getInvestorGraph()
                val projects = projectModel.getInvestorProjects()
                val user = userModel.getUserData()
                getDashboardSliderInfo()
                Triple(graph, projects, user)
            }

            if (SUCCESS == result.status) {
                val (graph, projects, user) = result.result!!
                setGraphItems(graph)
                setupLineSegments(graph.totalMonetaryAmount)
                putAllProjects(projects)
                welcomeText.set(appContext.getString(R.string.welcome_pattern_text, user.getWelcomeAppeal(appContext)))
                investorGraphResult.postAction(result.wrapWithAnotherResult(graph))
            } else {
                investorGraphResult.postAction(result.wrapWithAnotherResult(InvestorGraph()))
            }
        }
    }

    private val sliderResult = UniqueDataLiveData<Result<List<LinkSliderItemResponseBody>>>()
    internal fun getSliderResult(): LiveData<Result<List<LinkSliderItemResponseBody>>> = sliderResult

    private fun getDashboardSliderInfo() {
        viewModelScope.launch {
            val result = asyncLoading {
                userModel.getDashboardSliderInfo()
            }


            if (SUCCESS == result.status)
                changeSliderVisibilityBy(result.result!!.size)

            sliderResult.postAction(result)
        }
    }

    private fun setupLineSegments(total: BigDecimal) {
        clearSegments()
        when {
            total < BigDecimal(100) -> setupTwoLineSegments(total)
            total < BigDecimal(1000) -> setupFourLineSegments(total)
            else -> setupEightLineSegments(total)
        }
    }

    private fun clearSegments() {
        twoSegments.clear()
        fourSegments.clear()
        eightSegments.clear()
    }

    private fun setupTwoLineSegments(total: BigDecimal) {
        val step = total.divideSafe(BigDecimal(2))
        twoSegments.run {
            clear()
            add(0.toString())
            add(step.setScaleSafe(0).formatToCurrencyView())
        }
    }

    private fun setupFourLineSegments(total: BigDecimal) {
        fourSegments.clear()
        val step = total.divideSafe(BigDecimal(4)).setScaleSafe(0, HALF_EVEN)
        val investmentSteps = arrayListOf<String>()
        var value = BigDecimal.ZERO
        for (i in 0 until 4) {
            investmentSteps.add(value.formatToCurrencyView())
            value += step
        }
        if (investmentSteps.size == 4)
            fourSegments.addAll(investmentSteps)
    }

    private fun setupEightLineSegments(total: BigDecimal) {
        eightSegments.clear()
        val step = total.divideSafe(BigDecimal(8)).divideSafe(BigDecimal(10), HALF_EVEN)
            .multiplySafe(BigDecimal(10), 0)
        val investmentSteps = arrayListOf<String>()
        var value = BigDecimal.ZERO
        for (i in 0 until 8) {
            investmentSteps.add(value.formatToCurrencyView())
            value += step
        }
        if (investmentSteps.size == 8)
            eightSegments.addAll(investmentSteps)
    }

    private fun setGraphItems(graph: InvestorGraph) {
        graphItems.apply {
            clear()
            addAll(graph.investments)
        }
        totalField.set("€${graph.totalMonetaryAmount.toInt().formatToCurrencyView()}")
    }

    private fun putAllProjects(investorProjects: List<InvestorsProject>) {
        typedProjects.clear()
        allProjects.apply {
            clear()
            addAll(investorProjects)
            if (hasActiveProjects() && hasFinishedProjects())
                tabLayoutVisibility.set(true)
            else {
                tabLayoutVisibility.set(false)
                typedProjects.addAll(investorProjects.toProjectItems())
            }
        }
    }

    private val tabLayoutVisibilityResult = UniqueDataLiveData<Boolean>()
    internal fun geTabLayoutVisibilityResult(): LiveData<Boolean> = tabLayoutVisibilityResult

    internal fun addTabLayoutListener(lifecycleOwner: LifecycleOwner) {
        tabLayoutVisibility.onChangedSafe(lifecycleOwner) { toVisible ->
            tabLayoutVisibilityResult.postAction(toVisible)
        }
    }

    internal fun getProjectsForType(tabPosition: Int) {
        savedTabPosition = tabPosition
        when (tabPosition) {
            ACTIVE_PROJECTS -> putRunningProjects(allProjects)
            FINISHED_PROJECTS -> putFinishedProjects(allProjects)
        }
    }

    private fun putRunningProjects(items: List<InvestorsProject>) {
        val runningProjects = items.toRunningProjects()
        typedProjects.apply {
            clear()
            addAll(runningProjects)
        }
    }

    private fun putFinishedProjects(items: List<InvestorsProject>) {
        val finishedProjects = items.toFinishingProjects()
        typedProjects.apply {
            clear()
            addAll(finishedProjects)
        }
    }

    internal fun getRandomColorList(size: Int): List<String> {
        val randomColorList = arrayListOf<String>()
        repeat(size) {
            addRandomColor(randomColorList)
        }
        return randomColorList
    }

    private tailrec fun addRandomColor(randomColorList: ArrayList<String>) {
        val colorList = appContext.resources.getStringArray(R.array.progress_bar_colors)
        val randomColor = colorList[nextInt(colorList.size)]
        if (!randomColorList.isPreviousEqual(randomColor))
            randomColorList.add(randomColor)
        else
            addRandomColor(randomColorList)
    }

    private fun List<InvestorsProject>.hasActiveProjects() =
        find { it.projectStatus == ACTIVE } != null

    private fun List<InvestorsProject>.hasFinishedProjects() = find { project ->
        finishedStatuses.contains(project.projectStatus)
    } != null

    private fun List<InvestorsProject>.toRunningProjects() = filter { project ->
        project.projectStatus == ACTIVE
    }.map { it.mapToProjectItem() }

    private fun List<InvestorsProject>.toFinishingProjects() = filter { project ->
        finishedStatuses.contains(project.projectStatus)
    }.map { it.mapToProjectItem() }

    private fun List<InvestorsProject>.toProjectItems() = this.map { it.mapToProjectItem() }

    private fun changeSliderVisibilityBy(amount: Int) {
        sliderTabsVisibility.set(amount > 0)
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        savedTabPosition = 0
        totalField.set("")
        allProjects.clear()
        graphItems.clear()
        typedProjects.clear()
        tabLayoutVisibility.set(false)
        sliderTabsVisibility.set(false)
    }
}