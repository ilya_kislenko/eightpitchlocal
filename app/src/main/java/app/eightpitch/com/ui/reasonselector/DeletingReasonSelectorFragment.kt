package app.eightpitch.com.ui.reasonselector

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.base.baseviewmodels.codeverification.DefaultCodeValidationFragment
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO.CREATOR.DELETE_ACCOUNT
import app.eightpitch.com.data.dto.signup.CommonInvestorRequestBody
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.extensions.hideKeyboard
import kotlinx.android.synthetic.main.fragment_reason_selector.*

class DeletingReasonSelectorFragment : BaseFragment<DeletingReasonSelectorFragmentViewModel>() {


    companion object {

        internal const val USER_STATUS = "USER_STATUS"

        fun buildWithAnArguments(
            userStatus: String
        ) = Bundle().apply {
            putString(USER_STATUS, userStatus)
        }
    }

    private val userStatus: String
        get() = arguments?.getString(USER_STATUS, "") ?: ""

    override fun getLayoutID() = R.layout.fragment_reason_selector

    override fun getVMClass() = DeletingReasonSelectorFragmentViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        setupReasonSpinner()

        deleteButton.setOnClickListener {
            viewModel.validateDeletionReason(userStatus)
        }

        viewModel.getDeleteRequestResult().observe(viewLifecycleOwner, Observer {
            handleResult(it) {
                navigate(R.id.action_deletingReasonSelectorFragment_to_deleteAccountTwoLevelConfirmFragment)
            }
        })

        viewModel.getSMSResult().observe(viewLifecycleOwner, Observer { result ->
            handleResult(result) {
                val (interactionId, phoneNumber) = it
                val arguments =
                    DefaultCodeValidationFragment.buildWithAnArguments(
                        deletingReason = viewModel.getDeletionReasonText(),
                        validationScreenInfo = createValidationScreenInfo(),
                        partialUser = CommonInvestorRequestBody(
                            interactionId = interactionId,
                            phoneNumber = phoneNumber
                        )
                    )
                navigate(
                    R.id.action_deletingReasonSelectorFragment_to_defaultCodeValidationFragment,
                    arguments
                )
            }
        })
    }

    private fun setupReasonSpinner() {
        reasonSpinner.apply {
            val reasonsData = resources.getStringArray(R.array.defaultAccountDeletionReasons).toList()
            val companyAdapter = DeletingReasonSpinnerAdapter(context, reasonsData)
            setAdapter(companyAdapter)
            setSelection(companyAdapter.getHintPosition())
            setSelectedListener({
                viewModel.setReason(it)
                activity?.hideKeyboard()
            }, {})
        }
    }

    private fun createValidationScreenInfo() = ValidationScreenInfoDTO(
        verificationType = DELETE_ACCOUNT,
        validationButtonName = getString(R.string.reason_validation_button_text),
        header = getString(R.string.reason_validation_header),
        toolbarTitle = getString(R.string.reason_validation_title),
        subTitleMessage = getString(R.string.we_have_sent_sms_to_login_text)
    )
}
