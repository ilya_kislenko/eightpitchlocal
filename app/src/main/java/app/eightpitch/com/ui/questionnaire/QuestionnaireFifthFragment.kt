package app.eightpitch.com.ui.questionnaire

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody.CREATOR.FROM_5K_TO_25K
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody.CREATOR.LESS_THAN_5K
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody.CREATOR.MORE_THAN_25K
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody.CREATOR.NULL
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.ui.questionnaire.QuestionnaireFirstFragment.Companion.QUESTIONNAIRE_KEY_CODE
import kotlinx.android.synthetic.main.fragment_questionnaire_four_answer.*

class QuestionnaireFifthFragment : BaseFragment<QuestionnaireViewModel>() {

    companion object {
        fun buildWithAnArguments(
            questionnaireRequestBody: QuestionnaireRequestBody
        ) = Bundle().apply {
            putParcelable(QUESTIONNAIRE_KEY_CODE, questionnaireRequestBody)
        }
    }

    private val questionnaireRequestBody: QuestionnaireRequestBody
        get() = arguments?.getParcelable(QUESTIONNAIRE_KEY_CODE) ?: QuestionnaireRequestBody()

    override fun getLayoutID() = R.layout.fragment_questionnaire_four_answer

    override fun getVMClass() = QuestionnaireViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        viewModel.setupLayout(
            6,
            getString(R.string.how_big_was_your_average_investment_text),
            arrayListOf(
                getString(R.string.i_have_not_invested_yet_text),
                getString(R.string.up_to_5000_eur_text),
                getString(R.string.up_to_25000_eur_text),
                getString(R.string.more_than_25000_eur_text)
            )
        )

        viewModel.getQuestionnaireResult().observe(viewLifecycleOwner, Observer {
            handleResult(it) {
                navigate(R.id.action_questionnaireFifthFragment_to_questionnaireFinishFragment)
            }
        })

        nextButton.setOnClickListener {
            val answer = viewModel.getCheckedAnswer(
                listOf(NULL, LESS_THAN_5K, FROM_5K_TO_25K, MORE_THAN_25K), answersGroup
            )
            if (answer == NULL)
                viewModel.sendQuestionnaire(questionnaireRequestBody.copy(experience = null))
            else
                navigate(
                    R.id.action_questionnaireFifthFragment_to_questionnaireSixthFragment,
                    QuestionnaireSixthFragment.buildWithAnArguments(
                        questionnaireRequestBody.copy(investingAmount = answer)
                    )
                )
        }

        backButton.setOnClickListener {
            popBackStack()
        }
    }
}
