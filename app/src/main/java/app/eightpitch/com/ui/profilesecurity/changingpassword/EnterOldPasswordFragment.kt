package app.eightpitch.com.ui.profilesecurity.changingpassword

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.extensions.handleResult
import kotlinx.android.synthetic.main.fragment_enter_old_password.*

class EnterOldPasswordFragment : BaseFragment<EnterOldPasswordViewModel>() {

    companion object {

        internal const val USER_EXTERNAL_ID = "USER_EXTERNAL_ID"

        fun buildWithAnArguments(
            userExternalId: String
        ) = Bundle().apply {
            putString(USER_EXTERNAL_ID, userExternalId)
        }
    }

    private val userExternalId: String
        get() = arguments?.getString(USER_EXTERNAL_ID) ?: ""

    override fun getLayoutID() = R.layout.fragment_enter_old_password

    override fun getVMClass() = EnterOldPasswordViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindLoadingIndicator()

        buttonNext.setOnClickListener {
            viewModel.validatePassword(userExternalId)
        }

        viewModel.getVerifyPasswordResult().observe(viewLifecycleOwner, Observer { result ->
            handleResult(result) { verifyPasswordResponse ->
                val args =
                    SetNewPasswordFragment.buildWithAnArguments(verifyPasswordResponse.interactionId, userExternalId)
                navigate(R.id.action_enterOldPasswordFragment_to_setNewPasswordFragment, args)
            }
        })
    }
}