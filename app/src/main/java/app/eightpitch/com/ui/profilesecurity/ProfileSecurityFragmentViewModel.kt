package app.eightpitch.com.ui.profilesecurity

import android.content.Context
import androidx.databinding.ObservableField
import androidx.lifecycle.*
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.data.transmit.ResultStatus.SUCCESS
import app.eightpitch.com.extensions.wrapWithAnEmptyResult
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.utils.*
import kotlinx.coroutines.launch
import javax.inject.Inject

class ProfileSecurityFragmentViewModel @Inject constructor(
    private val appContext: Context,
    private val userModel: UserModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val title = appContext.getString(R.string.security_toolbar_title)

    var externalId: String = ""
        private set

    val twoFactorSwitchEnabledField = ObservableField(false)

    private val userMeResult = SingleActionLiveData<Result<Empty>>()
    internal fun getUserMeResult(): LiveData<Result<Empty>> = userMeResult

    internal fun getUser() {
        viewModelScope.launch {

            val result = asyncLoading {
                userModel.getUserData()
            }

            if (result.status == SUCCESS) {
                val user = result.result!!
                externalId = user.externalId
                setSecurityFields(user)
            }

            userMeResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    private fun setSecurityFields(user: User) {
        twoFactorSwitchEnabledField.set(user.isTwoFactorAuthenticationEnabled)
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        twoFactorSwitchEnabledField.set(false)
    }
}
