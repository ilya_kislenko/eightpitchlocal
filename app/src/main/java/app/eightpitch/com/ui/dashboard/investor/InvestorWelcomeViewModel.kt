package app.eightpitch.com.ui.dashboard.investor

import android.content.Context
import androidx.databinding.ObservableField
import androidx.lifecycle.*
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.data.transmit.ResultStatus.SUCCESS
import app.eightpitch.com.models.ProjectModel
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.coroutines.launch

class InvestorWelcomeViewModel(
    private val appContext: Context,
    private val projectModel: ProjectModel,
    private val userModel: UserModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val toolbarTitle = appContext.getString(R.string.investor_dashboard_text)
    val welcomeText = ObservableField("")

    /**
     * Result that is describing the state of user's investments, true if user has some and User
     */
    private val dataResult = SingleActionLiveData<Result<Pair<Boolean, User>>>()
    internal fun getDataResult(): LiveData<Result<Pair<Boolean, User>>> = dataResult

    internal fun retrieveData() {
        viewModelScope.launch {
            val result = asyncLoading {
                projectModel.getInvestorProjects().isNotEmpty() to
                        userModel.getUserData()
            }
            if (SUCCESS == result.status) {
                val welcomeAppeal = result.result!!.second.getWelcomeAppeal(appContext)
                welcomeText.set(appContext.getString(R.string.welcome_pattern_text, welcomeAppeal))
            }

            dataResult.postAction(result)
        }
    }
}