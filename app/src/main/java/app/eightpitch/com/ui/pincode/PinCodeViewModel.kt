package app.eightpitch.com.ui.pincode

import android.content.Context
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.base.preferences.PreferencesReader
import app.eightpitch.com.base.preferences.PreferencesWriter
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.extensions.updateQuickLoginPinCode

class PinCodeViewModel(
    private val appContext: Context,
    private val preferencesReader: PreferencesReader,
    private val preferencesWriter: PreferencesWriter,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val headerText = ObservableField("")

    /**
     * Lambda that will notify the calling side as
     * soon as user has finished an input of the code.
     */
    lateinit var onInputEndedHandler: () -> Unit

    //GradientDrawable with oval shape
    val firstObserveField = ObservableField(false)
    val secondObserveField = ObservableField(false)
    val thirdObserveField = ObservableField(false)
    val fourObserveField = ObservableField(false)

    val errorField = ObservableField("")
    val errorVisibility = ObservableBoolean(false)

    /**
     * @see PinCodeScreenState - ENTER_CODE
     * @sample usePasswordVisibility ability to use login and password
     * @sample useBiometryVisibility show biometry scanner
     */
    val usePasswordVisibility = ObservableBoolean(false)
    val useBiometryVisibility = ObservableBoolean(false)

    var pinCode = PinCodeInput()

    internal fun inputPinCode(digit: String) {
        errorVisibility.set(false)
        val appended = pinCode.appendIfPossible(digit)
        fillProgress(pinCode.getLength())
        if (!appended && ::onInputEndedHandler.isInitialized)
            onInputEndedHandler.invoke()
    }

    private fun fillProgress(length: Int) {
        firstObserveField.set(length >= 1)
        secondObserveField.set(length >= 2)
        thirdObserveField.set(length >= 3)
        fourObserveField.set(length == 4)
    }

    internal fun removeLastPinCodeDigit() {
        pinCode.clearDigit()
        fillProgress(pinCode.getLength())
    }

    internal fun clearPinCode() {
        pinCode.clear()
        fillProgress(pinCode.getLength())
    }

    internal fun setHeader(text: String) {
        headerText.set(text)
    }

    internal fun isCodesMatch(previousCode: String): Boolean {
        val isCodesMatch = previousCode == pinCode.code
        if (!isCodesMatch) {
            errorVisibility.set(true)
            errorField.set(appContext.getString(R.string.entered_codes_do_not_match_text))
        }
        return isCodesMatch
    }

    internal fun savePinCode() {
        preferencesWriter.updateQuickLoginPinCode(
            preferencesReader = preferencesReader,
            pinCode = pinCode.code
        )
    }

    internal fun setupBiometricsVisibility() {
        useBiometryVisibility.set(preferencesReader.getUserBiometricInfo().isBiometricActive)
    }

    internal fun isActualCode(): Boolean {
        val equal = preferencesReader.getUserBiometricInfo().pinCode == pinCode.code
        if (!equal) {
            errorVisibility.set(true)
            errorField.set(appContext.getString(R.string.invalid_pin_code_text))
        }
        return equal
    }

    internal fun deletePinCode() {
        preferencesWriter.clearBiometricInfo(preferencesReader.getCurrentUserExternalId())
    }

    internal fun enableBiometric(save: Boolean) {
        preferencesWriter.updateQuickLoginPinCode(preferencesReader = preferencesReader, isBiometricSetup = save)
    }

    override fun cleanUp() {
        super.cleanUp()
        firstObserveField.set(false)
        secondObserveField.set(false)
        thirdObserveField.set(false)
        fourObserveField.set(false)
        errorVisibility.set(false)
        usePasswordVisibility.set(false)
        useBiometryVisibility.set(false)
        errorField.set("")
        pinCode = PinCodeInput()
    }
}