package app.eightpitch.com.ui.changeemailconfirm

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import app.eightpitch.com.R
import app.eightpitch.com.base.rootbackhandlers.ChildNavGraphBackPressureHandler
import app.eightpitch.com.extensions.popBackStack
import kotlinx.android.synthetic.main.fragment_change_email_success.*

class ChangeEmailSuccessFragment : Fragment(), ChildNavGraphBackPressureHandler {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View =
        inflater.inflate(R.layout.fragment_change_email_success, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        crossButton.setOnClickListener {
            popBackStack(R.id.profilePersonalFragment)
        }

        okButton.setOnClickListener {
            popBackStack(R.id.profilePersonalFragment)
        }
    }

    override fun handleBackPressure(): Boolean = popBackStack(R.id.profilePersonalFragment)
}