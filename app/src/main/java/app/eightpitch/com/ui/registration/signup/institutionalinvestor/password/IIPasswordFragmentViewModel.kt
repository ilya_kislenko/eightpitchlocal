package app.eightpitch.com.ui.registration.signup.institutionalinvestor.password

import android.content.Context
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.*
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.signup.CommonInvestorRequestBody
import app.eightpitch.com.data.dto.signup.InstitutionalInvestorRequestBody
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.extensions.*
import app.eightpitch.com.models.RegistrationModel
import app.eightpitch.com.utils.*
import app.eightpitch.com.views.interfaces.ShortFieldValidator
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

class IIPasswordFragmentViewModel @Inject constructor(
    private val appContext: Context,
    private val registrationModel: RegistrationModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val title = appContext.getString(R.string.creating_account)

    val createButtonAvailability = ObservableBoolean()

    var firstPassword: String = ""
    var firstPasswordErrorHolder = SingleActionObservableString()

    private var secondPassword: String = ""
    var secondPasswordErrorHolder = SingleActionObservableString()

    val firstPasswordWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            firstPassword = sequence.toString()
        }
    }

    val secondPasswordWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            secondPassword = sequence.toString()
        }
    }

    fun validatePasswords(partialUser: CommonInvestorRequestBody) {

        val isPasswordsEquals = firstPassword == secondPassword

        val passwordsMismatchError = appContext.getString(R.string.password_mismatch)

        if (!isPasswordsEquals) {
            firstPasswordErrorHolder.set(passwordsMismatchError)
            secondPasswordErrorHolder.set(passwordsMismatchError)
        }

        if (!isPasswordsEquals)
            return

        val firstPasswordError = when {
            firstPassword.isEmpty() -> getErrorEmptyString(appContext)
            !firstPassword.matchPattern(RegexPatterns.PATTERN_VALID_PASSWORD_SYMBOLS) -> appContext.getString(
                R.string.not_valid_password
            )
            else -> ""
        }.takeIf { it.isNotEmpty() }?.let { firstPasswordErrorHolder.set(it); it }

        val secondPasswordError = when {
            secondPassword.isEmpty() -> getErrorEmptyString(appContext)
            !secondPassword.matchPattern(RegexPatterns.PATTERN_VALID_PASSWORD_SYMBOLS) -> appContext.getString(
                R.string.not_valid_password
            )
            else -> ""
        }.takeIf { it.isNotEmpty() }?.let { secondPasswordErrorHolder.set(it); it }

        val isPasswordsAreValid =
            firstPasswordError.isNullOrEmpty() && secondPasswordError.isNullOrEmpty()

        if (isPasswordsEquals && isPasswordsAreValid)
            createIIAccount(partialUser as InstitutionalInvestorRequestBody)
    }

    private val creatingIIAccountResult = SingleActionLiveData<Result<Empty>>()
    fun getCreatingIIAccountResult(): LiveData<Result<Empty>> = creatingIIAccountResult

    private fun createIIAccount(partialUser: InstitutionalInvestorRequestBody) {
        viewModelScope.launch {
            val result = asyncLoading {
                registrationModel.run {
                    performRegistration(partialUser.copy(password = firstPassword))
                    setUserLanguage(Locale.getDefault())
                }
            }

            creatingIIAccountResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        firstPassword = ""
        secondPassword = ""
        firstPasswordErrorHolder.set("")
        secondPasswordErrorHolder.set("")
        createButtonAvailability.set(false)
    }
}