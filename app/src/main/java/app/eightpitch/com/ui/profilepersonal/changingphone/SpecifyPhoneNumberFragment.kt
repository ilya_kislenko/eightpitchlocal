package app.eightpitch.com.ui.profilepersonal.changingphone

import android.os.Bundle
import android.view.View
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.base.baseviewmodels.codeverification.DefaultCodeValidationFragment
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO
import app.eightpitch.com.data.dto.signup.CommonInvestorRequestBody
import app.eightpitch.com.data.dto.user.CountryCode
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.ui.GenderSpinnerAdapter
import kotlinx.android.synthetic.main.fragment_specify_phone_number.*

class SpecifyPhoneNumberFragment : BaseFragment<SpecifyPhoneNumberViewModel>() {

    override fun getLayoutID() = R.layout.fragment_specify_phone_number

    override fun getVMClass() = SpecifyPhoneNumberViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()
        setupCountryCodeSpinner()

        nextButton.setOnClickListener {
            viewModel.isPhoneNumberValid()
        }

        viewModel.getPhoneValidationResult.observe(viewLifecycleOwner, { result ->
            handleResult(result) {
                viewModel.requestSMSCode()
            }
        })

        viewModel.getSMSResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) { smsResponse ->
                val interactionId = smsResponse.interactionId
                val arguments =
                    DefaultCodeValidationFragment.buildWithAnArguments(
                        validationScreenInfo = createValidationScreenInfo(), partialUser = CommonInvestorRequestBody(
                            interactionId = interactionId, phoneNumber = viewModel.getUserPhoneNumberWithCode()
                        )
                    )
                navigate(R.id.action_specifyPhoneNumberFragment_to_defaultCodeValidationFragment, arguments)
            }
        })
    }

    private fun setupCountryCodeSpinner() {
        codeSpinner.apply {
            val spinnerData = CountryCode.getPhoneCodes()
            val codeAdapter = GenderSpinnerAdapter(context, spinnerData)
            setAdapter(codeAdapter)
            setSelection(0)
            setSelectedListener({
                viewModel.setCountryCode(it)
            }, {})
        }
    }

    private fun createValidationScreenInfo() = ValidationScreenInfoDTO(
        verificationType = ValidationScreenInfoDTO.CHANGE_PHONE_NUMBER,
        validationButtonName = getString(R.string.change_phone_number_text),
        header = getString(R.string.change_confirmation_text),
        toolbarTitle = getString(R.string.change_phone_number_title),
        subTitleMessage = getString(R.string.change_phone_number_info_message)
    )
}