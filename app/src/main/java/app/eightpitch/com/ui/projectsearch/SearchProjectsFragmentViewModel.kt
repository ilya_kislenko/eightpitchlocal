package app.eightpitch.com.ui.projectsearch

import android.content.Context
import android.text.Spanned
import android.view.View
import androidx.core.text.toSpanned
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.data.transmit.ResultStatus.SUCCESS
import app.eightpitch.com.extensions.toHtml
import app.eightpitch.com.extensions.wrapWithAnEmptyResult
import app.eightpitch.com.models.FilterModel
import app.eightpitch.com.ui.projects.adapters.ProjectItem
import app.eightpitch.com.ui.projects.mappers.mapToProjectItem
import app.eightpitch.com.utils.Empty
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

class SearchProjectsFragmentViewModel @Inject constructor(
    private val appContext: Context,
    private val filterModel: FilterModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val rightClearButtonVisibility = ObservableField(View.GONE)
    val resultTextVisibility = ObservableField(View.GONE)
    val emptySearchFieldTextVisibility = ObservableField(View.VISIBLE)
    val noResultsVisibility = ObservableField(View.GONE)
    val textInfoResult = ObservableField<Spanned>()
    val projects = ObservableArrayList<ProjectItem>()

    private var searchText: String = ""

    internal fun setSearchText(text: String) {
        searchText = text.trim()
    }

    internal fun setClearButtonVisibility(visibility: Int) {
        rightClearButtonVisibility.set(visibility)
    }

    private val projectsResult = SingleActionLiveData<Result<Empty>>()
    internal fun getProjectsResult(): LiveData<Result<Empty>> = projectsResult

    internal fun getProjects() {
        projects.clear()
        textInfoResult.set("".toSpanned())
        noResultsVisibility.set(View.GONE)
        resultTextVisibility.set(View.GONE)

        if (searchText.isEmpty()) {
            emptySearchFieldTextVisibility.set(View.VISIBLE)
        } else {
            emptySearchFieldTextVisibility.set(View.GONE)
            viewModelScope.launch {

                val result = asyncLoading {
                    filterModel.getFilteredProject(companyName = searchText)
                }

                if (result.status == SUCCESS) {
                    val resultSet = result.result!!
                    val projects = resultSet.content.map { it.mapToProjectItem() }?: emptyList()
                    consumeFilteredProjects(projects)
                }
                projectsResult.postAction(result.wrapWithAnEmptyResult())
            }
        }
    }

    private fun consumeFilteredProjects(filteredProjects: List<ProjectItem>) {
        val isEmptyResult = searchText.isNotEmpty() && filteredProjects.isEmpty()
        resultTextVisibility.set(if (isEmptyResult) View.GONE else View.VISIBLE)
        noResultsVisibility.set(if (isEmptyResult) View.VISIBLE else View.GONE)
        textInfoResult.set(
            if (filteredProjects.isEmpty()){
                "".toSpanned()
            } else {
                String.format(
                    Locale.getDefault(),
                    appContext.getString(R.string.info_result_placeholder),
                    filteredProjects.size.toString(),
                    appContext.resources.getQuantityString(R.plurals.plural_result_count, filteredProjects.size),
                    searchText.trim()
                ).toHtml()
            }
        )
        updateVisibleFilteredProjects(filteredProjects)
    }

    private fun updateVisibleFilteredProjects(newProjects: List<ProjectItem>) {
        projects.apply {
            clear()
            addAll(newProjects)
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        projects.clear()
        resultTextVisibility.set(View.GONE)
        rightClearButtonVisibility.set(View.GONE)
        emptySearchFieldTextVisibility.set(View.VISIBLE)
        noResultsVisibility.set(View.GONE)
    }
}
