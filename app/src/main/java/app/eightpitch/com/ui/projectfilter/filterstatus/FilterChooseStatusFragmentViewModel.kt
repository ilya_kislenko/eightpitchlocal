package app.eightpitch.com.ui.projectfilter.filterstatus

import android.content.Context
import androidx.databinding.ObservableArrayList
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.ACTIVE
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.COMING_SOON
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.FINISHED
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.MANUAL_RELEASE
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.REFUNDED
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.TOKENS_TRANSFERRED
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.WAIT_BLOCKCHAIN
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.WAIT_RELEASE
import app.eightpitch.com.ui.projectfilter.SharableFilterViewModel
import javax.inject.Inject

class FilterChooseStatusFragmentViewModel @Inject constructor(
    private val appContext: Context,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val title = appContext.getString(R.string.filter_choose_status_title)

    val statuses = ObservableArrayList<String>()
    val selectedStatuses = ObservableArrayList<String>()

    private var sharableFilterViewModel: SharableFilterViewModel? = null

    private val finishedStatusesGroup: List<String> = listOf(
        MANUAL_RELEASE,
        WAIT_RELEASE,
        REFUNDED,
        TOKENS_TRANSFERRED,
        WAIT_BLOCKCHAIN
    )

    private val projectStatuses: List<String> = listOf(
        COMING_SOON,
        ACTIVE,
        FINISHED
    )

    internal fun setFilters(sharableFilterViewModel: SharableFilterViewModel){
        this.sharableFilterViewModel = sharableFilterViewModel
    }

    private fun updateSelectedList(){
        selectedStatuses.apply {
            clear()
            addAll(sharableFilterViewModel?.statuses?: emptyList())
        }
    }

    internal fun updateStateList(){
        updateSelectedList()
        statuses.apply {
            clear()
            addAll(projectStatuses)
        }
    }

    internal fun addToFilter(status: String){
        selectedStatuses.add(status)

        if(status == FINISHED){
            selectedStatuses.addAll(finishedStatusesGroup)
        }
    }

    internal fun removeFromFilter(status: String){
        selectedStatuses.remove(status)

        if(status == FINISHED){
            finishedStatusesGroup.forEach {
                selectedStatuses.remove(it)
            }
        }
    }

    internal fun resetStatuses(){
        selectedStatuses.apply {
            clear()
            addAll(emptyList())
        }
    }

    internal fun applyFilter() {
        sharableFilterViewModel?.setStatuses(selectedStatuses)
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        sharableFilterViewModel = null
        statuses.clear()
        selectedStatuses.clear()
    }
}
