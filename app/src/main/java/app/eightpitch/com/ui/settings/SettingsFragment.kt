package app.eightpitch.com.ui.settings

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.extensions.handleResultWithError
import app.eightpitch.com.extensions.showDialog
import app.eightpitch.com.ui.kyc.KycActivity
import app.eightpitch.com.ui.reasonselector.DeletingReasonSelectorFragment
import kotlinx.android.synthetic.main.fragment_settings.*

class SettingsFragment : BaseFragment<SettingsFragmentViewModel>() {

    override fun getLayoutID() = R.layout.fragment_settings

    override fun getVMClass() = SettingsFragmentViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindLoadingIndicator()

        subscriptionSwitch.setOnClickListener {
            subscriptionSwitch.isChecked = false
            viewModel.updateEmailSubscription(viewModel.subscriptionFields.get() == false)
        }

        accountStatusButton.setOnClickListener {
            val dialogStatusInfo = AccountStatusInfoDialog()
            dialogStatusInfo.arguments =
                AccountStatusInfoDialog.buildWithAnArguments(viewModel.userStatus)
            dialogStatusInfo.show(
                AccountStatusInfoDialog::class.java.name,
                childFragmentManager
            )
        }

        upgradeProfileButton.setOnClickListener {
            context?.let {
                startActivity(Intent(it, KycActivity::class.java))
            }
        }

        viewModel.getEmailSubscriptionResult().observe(viewLifecycleOwner, Observer {
            handleResultWithError(result = it, completeIfError = {
                if(viewModel.subscriptionFields.get() == true){
                    viewModel.setSubscription(true)
                }
            }, completeIfSuccess = {
                if (viewModel.subscriptionFields.get() == false) {
                    showDialog(message = getString(R.string.confirm_email_subscription_text))
                }
            })
        })

        deleteAccountButton.setOnClickListener {
            navigate(
                R.id.action_settingsFragment_to_deletingReasonSelectorFragment,
                DeletingReasonSelectorFragment.buildWithAnArguments(viewModel.userStatus)
            )
        }

        viewModel.getUserMeResult()
            .observe(viewLifecycleOwner, Observer { result -> handleResult(result) {} })
    }

    override fun onResume() {
        super.onResume()
        viewModel.getUser()
    }
}