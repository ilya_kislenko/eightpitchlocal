package app.eightpitch.com.ui.dynamicdetails

interface FragmentScrollable {
    fun scrollTop()
}