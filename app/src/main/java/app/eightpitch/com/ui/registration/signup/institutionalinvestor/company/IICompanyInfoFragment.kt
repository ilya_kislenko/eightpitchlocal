package app.eightpitch.com.ui.registration.signup.institutionalinvestor.company

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.signup.InstitutionalInvestorRequestBody
import app.eightpitch.com.data.dto.user.CompanyDataDTO
import app.eightpitch.com.data.dto.user.CompanyDataDTO.CompanyType.Companion.fromDisplayValue
import app.eightpitch.com.ui.registration.signup.institutionalinvestor.personalInfo.IIPersonalInfoFragment
import app.eightpitch.com.ui.webview.DefaultWebViewFragment
import app.eightpitch.com.utils.Constants
import kotlinx.android.synthetic.main.fragment_ii_company_info.*
import kotlinx.android.synthetic.main.toolbar_layout.*


class IICompanyInfoFragment : BaseFragment<IICompanyInfoFragmentViewModel>() {

    override fun getLayoutID() = R.layout.fragment_ii_company_info

    override fun getVMClass() = IICompanyInfoFragmentViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        additionalToolbarRightButton.setOnClickListener {
            val bundle = DefaultWebViewFragment.buildWithAnArguments(Constants.FAQ_URL)
            navigate(R.id.action_IICompanyInfoFragment_to_defaultWebViewFragment, bundle)
        }

        setupCompanyTypeSpinner()
        textInputLayoutCompanyName.addTextChangedListener(CompanyNameTextWatcher())

        buttonNext.setOnClickListener {
            if (viewModel.validateCompanyName()) {
                val data = viewModel.getCompanyData()
                val (companyType, companyName) = data.first
                val registerNumber = data.second
                val companyTypeBackend = (fromDisplayValue(companyType)).toString()
                navigate(
                    R.id.action_IICompanyInfoFragment_to_IIPersonalInfoFragment,
                    IIPersonalInfoFragment.buildWithAnArguments(
                        InstitutionalInvestorRequestBody(companyDataDTO = CompanyDataDTO(
                            companyTypeBackend, companyName),
                            commercialRegisterNumber = registerNumber
                        )
                    )
                )
            }
        }
    }

    private fun setupCompanyTypeSpinner() {
        companyTypeSpinner.apply {
            val companyTypeSpinnerData = context.resources.getStringArray(R.array.companyType).toList()
            val companyAdapter = CompanyTypeSpinnerAdapter(context, companyTypeSpinnerData)
            setAdapter(companyAdapter)
            setSelection(companyAdapter.getHintPosition())
            setSelectedListener({
                viewModel.setCompanyType(it)
            }, {})
        }
    }

    private inner class CompanyNameTextWatcher : TextWatcher {

        override fun afterTextChanged(s: Editable?) {
            // Do nothing
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            // Do nothing
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            viewModel.setCompanyNameValue(textInputLayoutCompanyName.text.toString())
        }
    }
}