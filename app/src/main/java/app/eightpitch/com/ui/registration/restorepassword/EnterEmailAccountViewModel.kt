package app.eightpitch.com.ui.registration.restorepassword

import android.content.Context
import android.util.Patterns
import androidx.lifecycle.*
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.restorepassword.PasswordRecoverySendInitialSmsCodeResponse
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.extensions.getErrorEmptyString
import app.eightpitch.com.models.SMSModel
import app.eightpitch.com.utils.*
import app.eightpitch.com.views.interfaces.ShortFieldValidator
import kotlinx.coroutines.launch

class EnterEmailAccountViewModel(
    private val appContext: Context,
    private val smsModel: SMSModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val toolbarTitle = appContext.getString(R.string.password_recovery_text)

    private var email: String = ""
    val emailErrorHolder = SingleActionObservableString()

    val emailWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            email = sequence.toString()
        }
    }

    fun validateEmail() {
        val emailError = when {
            email.isEmpty() -> getErrorEmptyString(appContext)
            !Patterns.EMAIL_ADDRESS.matcher(email).matches() -> appContext.getString(R.string.not_valid_email)
            else -> ""
        }.takeIf { it.isNotEmpty() }?.let { emailErrorHolder.set(it); it }

        if (emailError.isNullOrBlank())
            sendInitialSmsCode()
    }

    private val sendInitialSmsCodeResult = SingleActionLiveData<Result<PasswordRecoverySendInitialSmsCodeResponse>>()
    internal fun getSendInitialSmsCodeResult(): LiveData<Result<PasswordRecoverySendInitialSmsCodeResponse>> =
        sendInitialSmsCodeResult

    private fun sendInitialSmsCode() {
        viewModelScope.launch {
            val result = asyncLoading {
                smsModel.sendSmsCodeForPasswordRecovery(email.trim())
            }
            sendInitialSmsCodeResult.postAction(result)
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        email = ""
        emailErrorHolder.set("")
    }
}