package app.eightpitch.com.ui.projectfilter.filteramount

import android.content.Context
import androidx.databinding.ObservableArrayList
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.filter.AmountRange
import app.eightpitch.com.data.dto.filter.AmountRange.*
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.data.transmit.ResultStatus
import app.eightpitch.com.extensions.wrapWithAnEmptyResult
import app.eightpitch.com.models.FilterModel
import app.eightpitch.com.ui.projectfilter.SharableFilterViewModel
import app.eightpitch.com.utils.Empty
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.coroutines.launch
import javax.inject.Inject

class FilterChooseAmountRangeFragmentViewModel @Inject constructor(
    private val appContext: Context,
    private val filterModel: FilterModel,
    threadsSeparator: ThreadsSeparator,
) : BaseViewModel(threadsSeparator) {

    val title = appContext.getString(R.string.filter_choose_amount_range_title)

    val amountRanges = ObservableArrayList<AmountRange>()
    val selectedRanges = ObservableArrayList<AmountRange>()

    private var sharableFilterViewModel: SharableFilterViewModel? = null

    private val allAmountRanges = arrayListOf<AmountRange>()

    internal fun setFilters(sharableFilterViewModel: SharableFilterViewModel){
        this.sharableFilterViewModel = sharableFilterViewModel
    }

    private fun updateSelectedList() {
        selectedRanges.apply {
            clear()
            addAll(sharableFilterViewModel?.amountRanges?: emptyList())
        }
    }

    internal fun addToFilter(ranges: AmountRange) {
        selectedRanges.add(ranges)
    }

    internal fun removeFromFilter(ranges: AmountRange) {
        selectedRanges.remove(ranges)
    }

    internal fun resetStatuses() {
        selectedRanges.apply {
            clear()
            addAll(emptyList())
        }
    }

    private fun updateStateList() {
        amountRanges.apply {
            clear()
            addAll(allAmountRanges)
        }
    }

    internal fun applyFilter() {
        sharableFilterViewModel?.setAmountRanges(selectedRanges)
    }

    private val amountRangesResult = SingleActionLiveData<Result<Empty>>()
    internal fun getAmountRangesResult(): LiveData<Result<Empty>> =
        amountRangesResult

    internal fun getAmountRanges() {

        viewModelScope.launch {

            val result = asyncLoading {
                filterModel.getMinimalInvestment()
            }

            if (result.status == ResultStatus.SUCCESS) {
                allAmountRanges.addAll(result.result?.getActualRange() ?: arrayListOf())
            } else {
                allAmountRanges.addAll(
                    listOf(
                        RANGE_0_1000_EUR,
                        RANGE_1001_5000_EUR,
                        RANGE_5001_10000_EUR,
                        RANGE_10001_25000_EUR,
                        RANGE_25001_50000_EUR,
                        RANGE_50001_100000_EUR,
                        RANGE_MORE_THAN_100000_EUR
                    )
                )
            }

            updateSelectedList()
            updateStateList()
            amountRangesResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        sharableFilterViewModel = null
        selectedRanges.clear()
        allAmountRanges.clear()
        amountRanges.clear()
    }
}
