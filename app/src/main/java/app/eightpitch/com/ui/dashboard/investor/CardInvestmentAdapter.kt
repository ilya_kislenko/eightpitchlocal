package app.eightpitch.com.ui.dashboard.investor

import android.graphics.*
import android.view.*
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.R
import app.eightpitch.com.data.dto.investment.GraphsInvestment
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.dashboard.investor.CardInvestmentAdapter.CardInvestmentViewHolder
import app.eightpitch.com.ui.recycleradapters.BindableRecyclerAdapter
import app.eightpitch.com.views.CircleProgressLayout
import java.math.BigDecimal
import java.math.RoundingMode.HALF_DOWN

class CardInvestmentAdapter :
    BindableRecyclerAdapter<GraphsInvestment, CardInvestmentViewHolder>() {

    private var colorList: List<String> = listOf()
    private var totalMonetaryAmount = BigDecimal.ONE

    internal fun updateAttributes(
        colorList: List<String>,
        totalMonetaryAmount: BigDecimal
    ) {
        this.colorList = colorList
        this.totalMonetaryAmount = totalMonetaryAmount
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardInvestmentViewHolder =
        CardInvestmentViewHolder(inflateView(parent, R.layout.item_investment_statistic_card_view))

    override fun onBindViewHolder(holder: CardInvestmentViewHolder, position: Int) {
        val item = getDataItem(position)
        holder.apply {
            cardLayout.background = ContextCompat.getDrawable(
                itemView.context,
                R.drawable.top_gray_line_background
            ).apply {
                this?.setColorFilter(Color.parseColor(colorList[position]), PorterDuff.Mode.SRC_OUT)
            }
            progress.apply {
                setPercentage(countPercentage(item))
                setProgressColor(parseColorBy(position))
            }
            name.text = item.companyName
            "€${item.monetaryAmount.toInt().formatToCurrencyView()}".also { total.text = it }
        }
    }

    private fun parseColorBy(position: Int) = Color.parseColor(colorList[position])

    private fun countPercentage(graph: GraphsInvestment): Float {
        val percent = graph.monetaryAmount.setScale(3).divideSafe(totalMonetaryAmount.setScale(3))
            .setScale(2, HALF_DOWN).toFloat()
        return if (percent != 0F) percent else 0.01F
    }

    class CardInvestmentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: AppCompatTextView = itemView.findViewById(R.id.cardNameText)
        val total: AppCompatTextView = itemView.findViewById(R.id.totalTextView)
        val progress: CircleProgressLayout = itemView.findViewById(R.id.progressBar)
        val cardLayout: ConstraintLayout = itemView.findViewById(R.id.cardLayout)
    }
}