package app.eightpitch.com.ui.investment.choosedocuments

import android.Manifest
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.base.rootbackhandlers.ChildNavGraphBackPressureHandler
import app.eightpitch.com.data.dto.investment.ProjectInvestmentInfoResponse
import app.eightpitch.com.data.dto.projectdetails.ProjectsFile
import app.eightpitch.com.extensions.REQUEST_WRITING_EXTERNAL_STORAGE
import app.eightpitch.com.extensions.checkPermissions
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.extensions.toArrayList
import app.eightpitch.com.ui.investment.directdownloading.DirectDownloadingFragment
import app.eightpitch.com.ui.investment.sendingemail.SendingByEmailFragment
import app.eightpitch.com.utils.TextViewsRecyclerAdapter
import app.eightpitch.com.utils.SafeLinearLayoutManager
import kotlinx.android.synthetic.main.fragment_project_documents.*
import java.util.*

class ProjectDocumentsFragment : BaseFragment<ProjectDocumentsViewModel>(),
    ChildNavGraphBackPressureHandler {

    companion object {

        const val PROJECT_ID = "PROJECT_ID"
        const val INVESTMENT_ID = "INVESTMENT_ID"
        const val PROJECT_INVESTMENT = "PROJECT_INVESTMENT"
        const val PROJECT_FILES = "PROJECT_FILES"

        fun buildWithAnArguments(
            projectId: String,
            investmentId: String,
            projectInvestmentInfo: ProjectInvestmentInfoResponse,
            projectFiles: ArrayList<ProjectsFile>
        ) = Bundle().apply {
            putString(PROJECT_ID, projectId)
            putString(INVESTMENT_ID, investmentId)
            putParcelable(PROJECT_INVESTMENT, projectInvestmentInfo)
            putParcelableArrayList(PROJECT_FILES, projectFiles)
        }
    }

    private val projectId: String
        get() = arguments?.getString(PROJECT_ID) ?: ""

    private val investmentId: String
        get() = arguments?.getString(INVESTMENT_ID) ?: ""

    private val projectInvestmentInfo: ProjectInvestmentInfoResponse
        get() = arguments?.getParcelable(PROJECT_INVESTMENT) ?: ProjectInvestmentInfoResponse()

    private val projectFiles: List<ProjectsFile>
        get() = arguments?.getParcelableArrayList(PROJECT_FILES) ?: listOf()

    override fun getLayoutID() = R.layout.fragment_project_documents

    override fun getVMClass() = ProjectDocumentsViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        viewModel.setupFiles(projectFiles.map { it.projectsFile.originalFileName })

        confirmCheckBox.setOnCheckedChangeListener { _, isChecked ->
            viewModel.changeAvailability(isChecked)
        }

        filesRecycler.apply {
            adapter = TextViewsRecyclerAdapter()
            layoutManager = SafeLinearLayoutManager(context, VERTICAL, false)
        }

        viewModel.getSendingResult().observe(viewLifecycleOwner, Observer {
            handleResult(it) {
                val investmentId = getInvestmentID()
                val arguments =
                    SendingByEmailFragment.buildWithAnArguments(projectId, investmentId, projectFiles.toArrayList())
                navigate(R.id.action_projectDocumentsFragment_to_sendingByEmailFragment, arguments)
            }
        })

        sendByEmailButton.setOnClickListener {
            viewModel.sendDocumentsByEmail(projectId, investmentId)
        }

        directDownload.setDisablingClickListener {
            grantWriteStoragePermission {
                val investmentId = getInvestmentID()
                val arguments =
                    DirectDownloadingFragment.buildWithAnArguments(projectId, investmentId, projectFiles.toArrayList())
                navigate(R.id.action_projectDocumentsFragment_to_directDownloadingFragment, arguments)
            }
        }
    }

    private fun grantWriteStoragePermission(granted: () -> Unit) {
        activity?.let { fragmentActivity ->
            fragmentActivity.checkPermissions(
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                { granted.invoke() },
                { deniedPermissions ->
                    ActivityCompat.requestPermissions(
                        fragmentActivity,
                        deniedPermissions,
                        REQUEST_WRITING_EXTERNAL_STORAGE
                    )
                })
        }
    }

    private fun getInvestmentID() = if (investmentId.isNotEmpty()) investmentId
    else projectInvestmentInfo.unprocessedInvestment.investmentId

    override fun handleBackPressure(): Boolean {
        return popBackStack(R.id.projectsFragment)
    }
}