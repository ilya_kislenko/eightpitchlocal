package app.eightpitch.com.ui.investment.germanytax

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.user.TaxInformationBody.TaxChurch.Companion.getTaxChurches
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.GenderSpinnerAdapter
import kotlinx.android.synthetic.main.fragment_german_tax.*

class GermanTaxFragment : BaseFragment<GermanTaxViewModel>() {

    override fun getLayoutID() = R.layout.fragment_german_tax

    override fun getVMClass() = GermanTaxViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        taxResidentToggleButton.setOnSelectionChangedListener {
            viewModel.isGermanyTaxResident.set(it)
        }

        churchTaxToggleButton.setOnSelectionChangedListener {
            viewModel.isTaxLiability.set(it)
        }

        viewModel.run {
            getTaxInformationResultResult().observe(viewLifecycleOwner, {
                handleResult(it) {
                    navigate(
                        R.id.action_germanTaxFragment_to_preContractualInfoFragment,
                        arguments.orDefaultValue(Bundle())
                    )
                }
            })
            getValidateFieldsResultResult().observe(viewLifecycleOwner, {
                showDialog(message = getString(R.string.please_fill_all_the_required_fields_to_continue_text))
            })
        }


        confirmButton.setOnClickListener {
            viewModel.validate()
        }

        setupTaxChurchSpinner()
    }

    private fun setupTaxChurchSpinner() {
        churchSpinner.apply {
            val spinnerData = getTaxChurches()
            adapter = GenderSpinnerAdapter(context, spinnerData)
            onItemSelectedListener = SpinnerSelectionListener()
        }
    }

    private inner class SpinnerSelectionListener : AdapterView.OnItemSelectedListener {

        override fun onNothingSelected(parent: AdapterView<*>?) {
            //Do nothing.
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            viewModel.setChurch(position)
        }
    }
}