package app.eightpitch.com.ui.investment.sendingemail

import android.content.Context
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.projectdetails.ProjectsFile
import app.eightpitch.com.data.dto.sms.SendTokenLimitResponse
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.models.SMSModel
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.coroutines.launch

class SendingByEmailViewModel(
    appContext: Context,
    private val smsModel: SMSModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val buttonAvailable = ObservableBoolean()
    internal fun changeAvailability(available: Boolean) {
        buttonAvailable.set(available)
    }

    val files = ObservableArrayList<String>()
    internal fun setupFiles(list: List<ProjectsFile>) {
        val mappedList = list.map { "• ${it.projectsFile.originalFileName}" }
        files.addAll(mappedList)
    }

    private val requestResult = SingleActionLiveData<Result<SendTokenLimitResponse>>()
    internal fun getRequestResult(): LiveData<Result<SendTokenLimitResponse>> = requestResult

    internal fun requestInvestmentConfirmation(
        investmentId: String,
        projectId: String
    ) {
        viewModelScope.launch {
            val result = asyncLoading {
                smsModel.requestInvestmentConfirmation(investmentId, projectId)
            }
            requestResult.postAction(result)
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        files.clear()
        buttonAvailable.set(false)
    }
}