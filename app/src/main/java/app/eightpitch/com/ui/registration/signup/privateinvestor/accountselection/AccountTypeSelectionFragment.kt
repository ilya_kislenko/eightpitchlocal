package app.eightpitch.com.ui.registration.signup.privateinvestor.accountselection

import android.os.Bundle
import android.view.*
import androidx.core.content.ContextCompat
import androidx.databinding.*
import androidx.fragment.app.Fragment
import app.eightpitch.com.BR
import app.eightpitch.com.R
import app.eightpitch.com.data.dto.signup.InstitutionalInvestorRequestBody
import app.eightpitch.com.data.dto.signup.PrivateInvestorRequestBody
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.registration.signup.institutionalinvestor.personalInfo.IIPersonalInfoFragment
import app.eightpitch.com.ui.registration.signup.privateinvestor.personalinfo.PVIPersonalInfoFragment
import kotlinx.android.synthetic.main.fragment_account_type_selection.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class AccountTypeSelectionFragment : Fragment() {

    private lateinit var binding: ViewDataBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_account_type_selection, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.setVariable(BR.title, getString(R.string.choose_account_type_title))

        additionalToolbarRightButton.apply {
            setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_info))
            setOnClickListener {
                AccountTypeInfoDialog().show(
                    AccountTypeInfoDialog::class.java.name,
                    childFragmentManager
                )
            }
        }

        handleBackButton()

        firstItemView.setOnClickListener {
            navigate(
                R.id.action_accountTypeSelectionFragment_to_PVIPersonalInfoFragment,
                PVIPersonalInfoFragment.buildWithAnArguments(PrivateInvestorRequestBody())
            )
        }

        secondItemView.setOnClickListener {
            navigate(R.id.action_accountTypeSelectionFragment_to_IICompanyInfoFragment)
        }
    }
}