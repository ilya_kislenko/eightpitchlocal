package app.eightpitch.com.ui.registration.authorization

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import app.eightpitch.com.R
import app.eightpitch.com.extensions.navigate
import kotlinx.android.synthetic.main.fragment_registration_type_selection.*

class RegistrationTypeSelectionFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_registration_type_selection, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        buttonSignUp.setOnClickListener {
            navigate(R.id.action_registrationTypeSelectionFragment_to_accountTypeSelectionFragment)
        }
        buttonSignIn.setOnClickListener {
           navigate(R.id.action_registrationTypeSelectionFragment_to_loginFragment)
        }
    }
}