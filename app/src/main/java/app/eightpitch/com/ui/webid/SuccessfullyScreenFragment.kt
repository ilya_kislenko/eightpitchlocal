package app.eightpitch.com.ui.webid

import android.os.Bundle
import android.view.*
import app.eightpitch.com.R
import app.eightpitch.com.base.BackPressureFragment
import app.eightpitch.com.extensions.*
import kotlinx.android.synthetic.main.fragment_successfully_identified.*

class SuccessfullyScreenFragment : BackPressureFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_successfully_identified, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        startExploringButton.setOnClickListener {
            navigate(R.id.action_successfullyScreenFragment_to_questionnaireStartFragment)
        }

        crossButton.setOnClickListener { moveBackward() }
        registerOnBackPressedListener { moveBackward() }
    }
}
