package app.eightpitch.com.ui.recycleradapters

/**
 * Should be used from data binding adapter
 */
interface BindableFilterAdapter<T: Any> {
    fun setFilterData(data: MutableList<T>)
}