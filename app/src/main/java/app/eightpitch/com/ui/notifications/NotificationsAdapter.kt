package app.eightpitch.com.ui.notifications

import android.text.method.LinkMovementMethod
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.R
import app.eightpitch.com.data.dto.notifications.Notification
import app.eightpitch.com.data.dto.notifications.Notification.Companion.NEW
import app.eightpitch.com.data.dto.notifications.NotificationsMapper
import app.eightpitch.com.extensions.animateProperty
import app.eightpitch.com.extensions.inflateView
import app.eightpitch.com.extensions.setDisablingClickListener
import app.eightpitch.com.ui.notifications.NotificationsAdapter.NotificationHolder
import app.eightpitch.com.ui.recycleradapters.BindableRecyclerAdapter
import app.eightpitch.com.utils.Constants.DD_DOT_MM_DOT_YY_HH_DOT_MM
import app.eightpitch.com.utils.DateUtils.formatByPattern

class NotificationsAdapter(private val notificationsMapper: NotificationsMapper) :
    BindableRecyclerAdapter<Notification, NotificationHolder>() {

    private val DEFAULT_MAX_LINES = 3
    private val ACTUAL_MAX_LINES = 30

    /**
     * Holds the max lines for particular view by Int - position
     * second Int - max lines for the view
     */
    private val maxLinesHolder = mutableMapOf<Int, Int>()

    /**
     * Callback that represents a notification clicking, Int as a Id of
     * the clicked notification
     */
    internal var onNotificationClicked: ((Int) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationHolder {
        return NotificationHolder(inflateView(parent, R.layout.item_view_notification))
    }

    override fun onBindViewHolder(holder: NotificationHolder, position: Int) {
        val notification = getDataItem(position)
        holder.apply {
            val itsNewNotification = notification.notificationStatus == NEW
            unreadView.isVisible = itsNewNotification
            readNotificationView.apply {
                isVisible = itsNewNotification
                tag = position
                setOnClickListener { view ->
                    onNotificationClicked?.let {
                        val tagPosition = view.tag as Int
                        val selectedNotification = getDataItem(tagPosition)
                        it.invoke(selectedNotification.id)
                    }
                }
            }
            notificationMessageView.apply {
                text = notificationsMapper.mapNotificationMessageBy(
                    notification.message,
                    notification.notificationType
                )
                isSelected = itsNewNotification
                movementMethod = LinkMovementMethod.getInstance()
                setLinkTextColor(ContextCompat.getColor(context, R.color.lightRed))
                post {
                    notificationShevronView.apply {
                        tag = notificationMessageView to position
                        if (maxLinesHolder.contains(position))
                            maxLines = maxLinesHolder[position] ?: maxLines
                        else
                            updateMaxLinesFor(notificationMessageView, position, DEFAULT_MAX_LINES)
                        visibility =
                            if (maxLines >= DEFAULT_MAX_LINES && lineCount > DEFAULT_MAX_LINES) VISIBLE else INVISIBLE
                        setImageDrawable(
                            ContextCompat.getDrawable(
                                context,
                                if (maxLines == ACTUAL_MAX_LINES) R.drawable.icon_shevron_up
                                else if (maxLines >= DEFAULT_MAX_LINES && lineCount > DEFAULT_MAX_LINES) R.drawable.icon_shevron_bottom
                                else R.drawable.icon_shevron_up
                            )
                        )
                        setDisablingClickListener(delay = 200) { shevron ->
                            updateMaxLinesWithAnimation(shevron)
                        }
                    }
                }
            }
            notificationTimeView.text =
                notification.date.formatByPattern(DD_DOT_MM_DOT_YY_HH_DOT_MM)
        }
    }

    private fun updateMaxLinesWithAnimation(shevronView: View) {
        val (messageView, tagPosition) = shevronView.tag as Pair<AppCompatTextView, Int>
        val linesCountOnRelatedView = messageView.maxLines
        val toValue =
            if (linesCountOnRelatedView == ACTUAL_MAX_LINES) DEFAULT_MAX_LINES else ACTUAL_MAX_LINES
        messageView.animateProperty("maxLines", toValue, 90)
        (shevronView as AppCompatImageView).setImageDrawable(
            ContextCompat.getDrawable(
                shevronView.context,
                if (toValue == ACTUAL_MAX_LINES) R.drawable.icon_shevron_up else R.drawable.icon_shevron_bottom
            )
        )
        updateMaxLinesFor(messageView, tagPosition, toValue)
    }

    private fun updateMaxLinesFor(messageView: AppCompatTextView, position: Int, maxLines: Int) {
        maxLinesHolder[position] = maxLines
        messageView.apply {
            this.maxLines = maxLines
            invalidate()
        }
    }

    class NotificationHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val unreadView: View = itemView.findViewById<View>(R.id.unreadView)
        val notificationShevronView: AppCompatImageView =
            itemView.findViewById(R.id.shevronIconView)
        val notificationTimeView: AppCompatTextView =
            itemView.findViewById(R.id.notificationTimeView)
        val notificationMessageView: AppCompatTextView =
            itemView.findViewById(R.id.notificationMessageView)
        val readNotificationView: AppCompatTextView =
            itemView.findViewById(R.id.readNotificationView)
    }
}
