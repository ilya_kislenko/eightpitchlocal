package app.eightpitch.com.ui.dynamicdetails.maintab

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.LinearLayout
import androidx.annotation.StringRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import app.eightpitch.com.R
import app.eightpitch.com.data.dto.dynamicviews.mainfacts.FinancialInformationItem
import app.eightpitch.com.data.dto.dynamicviews.mainfacts.FinancialInformationItem.CREATOR.ISSUING_VOLUME
import app.eightpitch.com.data.dto.dynamicviews.mainfacts.FinancialInformationItem.CREATOR.MINIMUM_INVESTMENT_AMOUNT
import app.eightpitch.com.data.dto.dynamicviews.mainfacts.FinancialInformationItem.CREATOR.NOMINAL_VALUE
import app.eightpitch.com.data.dto.dynamicviews.mainfacts.FinancialInformationItem.CREATOR.SOFT_CAP
import app.eightpitch.com.data.dto.projectdetails.AdditionalFields
import app.eightpitch.com.data.dto.dynamicviews.mainfacts.MainFact
import app.eightpitch.com.data.dto.projectdetails.AdditionalFields.CREATOR.FUTURE_SHARE_NOMINAL_VALUE
import app.eightpitch.com.extensions.getTarget
import app.eightpitch.com.extensions.orDefaultValue
import app.eightpitch.com.ui.dynamicdetails.FragmentScrollable
import app.eightpitch.com.ui.dynamicdetails.OnSwipeUpBackPressuredListener
import app.eightpitch.com.utils.OnSwipeTouchListener
import kotlinx.android.synthetic.main.fargment_main_facts.*

class MainFactsFragment : Fragment(), FragmentScrollable {

    companion object {

        private const val ADDITIONAL_FIELDS_KEY = "ADDITIONAL_FIELDS_KEY"
        internal const val FINANCIAL_INFORMATION_KEY = "FINANCIAL_INFORMATION_KEY"

        fun buildWithArguments(
            additionalFields: AdditionalFields,
            financialInformationItem: FinancialInformationItem,
        ): MainFactsFragment {
            return MainFactsFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ADDITIONAL_FIELDS_KEY, additionalFields)
                    putParcelable(FINANCIAL_INFORMATION_KEY, financialInformationItem)
                }
            }
        }
    }

    private val additionalFields: AdditionalFields
        get() = arguments?.getParcelable(ADDITIONAL_FIELDS_KEY) ?: AdditionalFields()
    private val financialInformation: FinancialInformationItem
        get() = arguments?.getParcelable<FinancialInformationItem>(FINANCIAL_INFORMATION_KEY)
            .orDefaultValue(FinancialInformationItem())

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fargment_main_facts, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        nestedScroll.setOnTouchListener(object : OnSwipeTouchListener(context) {
            override fun onSwipeDown() {
                super.onSwipeDown()
                if (nestedScroll.scrollY == 0)
                    getTarget(OnSwipeUpBackPressuredListener::class.java)?.onOverSwiped()
            }
        })

        val views =
            createAdditionalFields(additionalFields.descriptionToLabel) + createFinancialFields(financialInformation.descriptionToLabel)

        fillContainerBy(views)
    }

    private fun fillContainerBy(views: Array<View>) {
        if (!resources.getBoolean(R.bool.isItTabletDevice)) {
            val container = view?.findViewById<LinearLayout>(R.id.container)
            container?.let {
                views.forEach { view -> it.addView(view) }
            }
        } else {
            val firstContainer = view?.findViewById<LinearLayout>(R.id.leftContainer)
            val secondContainer = view?.findViewById<LinearLayout>(R.id.rightContainer)
            if (firstContainer != null && secondContainer != null) {
                views.forEachIndexed { index, view ->
                    when {
                        index % 2 == 0 -> firstContainer.addView(view)
                        else -> secondContainer.addView(view)
                    }
                }
            }
        }
    }

    private fun createAdditionalFields(items: List<MainFact>): Array<View> {
        return Array(items.size) {
            LayoutInflater.from(context).inflate(R.layout.main_fact_item_layout, null)
                .apply {
                    layoutParams = ConstraintLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
                    findViewById<AppCompatTextView>(R.id.labelTextView).apply {
                        text = getString(items[it].label)
                    }
                    findViewById<AppCompatTextView>(R.id.valueTextView).apply {
                        text = handleDescription(items[it])
                    }
                    findViewById<AppCompatImageView>(R.id.iconView).apply {
                        setImageDrawable(ContextCompat.getDrawable(context, items[it].icon))
                    }
                }
        }
    }

    private fun createFinancialFields(items: List<MainFact>): Array<View> {
        return Array(items.size) {
            LayoutInflater.from(context).inflate(R.layout.main_fact_item_layout, null)
                .apply {
                    layoutParams = ConstraintLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
                    findViewById<AppCompatTextView>(R.id.labelTextView).apply {
                        text = getString(items[it].label)
                    }
                    findViewById<AppCompatTextView>(R.id.valueTextView).apply {
                        text = handleDescription(items[it])
                    }
                    findViewById<AppCompatImageView>(R.id.iconView).apply {
                        setImageDrawable(ContextCompat.getDrawable(context, items[it].icon))
                    }
                }
        }
    }

    //TODO backend
    private fun handleDescription(item: MainFact): String {
        return when (item.label) {
            ISSUING_VOLUME, SOFT_CAP, FUTURE_SHARE_NOMINAL_VALUE,
            MINIMUM_INVESTMENT_AMOUNT, NOMINAL_VALUE,
            -> item.description.addPostfix(R.string.eur_text)
            else -> item.description
        }
    }

    private fun String.addPostfix(@StringRes res: Int): String {
        return this + " " + getString(res)
    }

    override fun scrollTop() {
        nestedScroll.scrollTo(0, 0)
    }
}