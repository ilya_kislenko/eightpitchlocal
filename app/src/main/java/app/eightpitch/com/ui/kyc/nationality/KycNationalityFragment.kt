package app.eightpitch.com.ui.kyc.nationality

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.webid.WebIdUserRequestBody
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.ui.kyc.country.KycCountryInfoFragment
import kotlinx.android.synthetic.main.fragment_kyc_nationality.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class KycNationalityFragment : BaseFragment<KycNationalityViewModel>() {

    companion object {

        internal const val KYC_BODY = "KYC_BODY"

        fun buildWithAnArguments(
            webIdUserRequestBody: WebIdUserRequestBody,
        ) = Bundle().apply {
            putParcelable(KYC_BODY, webIdUserRequestBody)
        }
    }

    private val webIdUserRequestBody: WebIdUserRequestBody
        get() = arguments?.getParcelable(KYC_BODY) ?: WebIdUserRequestBody()

    override fun getLayoutID() = R.layout.fragment_kyc_nationality

    override fun getVMClass() = KycNationalityViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        setupNationalitySpinner()

        additionalToolbarRightButton.apply {
            setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_info))
            setOnClickListener {
                KycPoliticalExposedPersonDialog().show(
                    KycPoliticalExposedPersonDialog::class.java.name,
                    childFragmentManager
                )
            }
        }

        buttonNext.setOnClickListener {
            if(viewModel.validateNationalityField()){
                val (politically, taxLiability) = getToggleButtonState()
                viewModel.setToggleButtonState(politically, taxLiability)
                val updatedInfo = viewModel.appendInformationTo(
                    webIdUserRequestBody,
                    politically,
                    taxLiability
                )
                navigate(
                    R.id.action_kycNationalityFragment_to_kycCountryInfoFragment,
                    KycCountryInfoFragment.buildWithAnArguments(updatedInfo)
                )
            }
        }

        viewModel.getUserMeResult().observe(viewLifecycleOwner, Observer { result ->
            handleResult(result) { user ->
                nationalitySpinner.setSelection(viewModel.getUserNationalityArrayIndex(user))
            }
        })

        viewModel.loadUserOrRestoreState()
    }

    private fun setupNationalitySpinner() {
        context?.let { ctx ->
            nationalitySpinner.apply {
                val nationalityAdapter = NationalitySpinnerAdapter(ctx)
                setAdapter(nationalityAdapter)
                setSelection(nationalityAdapter.getHintPosition())
                setSelectedListener({
                    viewModel.setNationality(it)
                }, {})
            }
        }
    }

    private fun getToggleButtonState(): Pair<Boolean, Boolean> {
        return politicallyToggle.isPositiveSelection() to taxLiabilityToggle.isPositiveSelection()
    }
}