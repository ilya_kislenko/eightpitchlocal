package app.eightpitch.com.ui.profilesecurity.changingpassword

import android.content.Context
import androidx.lifecycle.*
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.changingpassword.VerifyOldPasswordRequestBody
import app.eightpitch.com.data.dto.changingpassword.VerifyOldPasswordResponseBody
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.extensions.*
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.utils.RegexPatterns.PATTERN_VALID_PASSWORD_SYMBOLS
import app.eightpitch.com.utils.*
import app.eightpitch.com.views.interfaces.ShortFieldValidator
import kotlinx.coroutines.launch

class EnterOldPasswordViewModel(
    private val appContext: Context,
    private val userModel: UserModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val toolbarTitle = appContext.getString(R.string.changing_password_text)

    var currentPassword: String = ""
    var currentPasswordErrorHolder = SingleActionObservableString()

    val currentPasswordWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            currentPassword = sequence.toString()
        }
    }

    internal fun validatePassword(userExternalId: String) {
        val passwordError = when {
            currentPassword.isEmpty() -> getErrorEmptyString(appContext)
            else -> ""
        }.takeIf { it.isNotEmpty() }?.let { currentPasswordErrorHolder.set(it); it }

        if (passwordError.isNullOrEmpty())
            verifyOldPassword(currentPassword, userExternalId)
    }

    private val verifyPasswordResult = SingleActionLiveData<Result<VerifyOldPasswordResponseBody>>()
    internal fun getVerifyPasswordResult(): LiveData<Result<VerifyOldPasswordResponseBody>> = verifyPasswordResult

    private fun verifyOldPassword(
        password: String,
        userExternalId: String
    ) {
        viewModelScope.launch {
            val result = asyncLoading {
                userModel.verifyOldPassword(VerifyOldPasswordRequestBody(password, userExternalId))
            }
            verifyPasswordResult.postAction(result)
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        currentPassword = ""
        currentPasswordErrorHolder.set("")
    }
}