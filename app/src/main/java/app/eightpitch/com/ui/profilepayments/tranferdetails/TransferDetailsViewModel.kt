package app.eightpitch.com.ui.profilepayments.tranferdetails

import android.content.Context
import androidx.databinding.ObservableField
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.profiletransfers.TransferDetails
import app.eightpitch.com.extensions.copyToClipboard
import app.eightpitch.com.extensions.formatToCurrencyView
import java.util.*
import javax.inject.Inject

class TransferDetailsViewModel@Inject constructor(
    private val appContext: Context,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val title = appContext.getString(R.string.profile_payments_toolbar_title)

    //region Binding
    val dateTimeField = ObservableField("")
    val companyNameField = ObservableField("")
    val businessAddressField = ObservableField("")
    val isinField = ObservableField("")
    val securityTokenField = ObservableField("")
    val userFromField = ObservableField("")
    val userToField = ObservableField("")
    //endregion

    internal fun setupPaymentDetailsBinding(transferDetails: TransferDetails) {
        val tokenAmountString = getTokenAmount(transferDetails)

        transferDetails.apply {
            dateTimeField.set(formattedDate)
            companyNameField.set(companyName)
            businessAddressField.set(companyAddress)
            isinField.set(isin)
            securityTokenField.set(tokenAmountString)
            userFromField.set(fromUserFullName)
            userToField.set(toUserFullName)
        }
    }

    internal fun getTokenAmount(transferDetails: TransferDetails) = String.format(
        Locale.getDefault(),
        appContext.getString(R.string.token_amount_pattern),
        transferDetails.amount.formatToCurrencyView(),
        transferDetails.shortCut
    )

    internal fun copyToClipboard(text: String) {
        appContext.copyToClipboard(text)
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        companyNameField.set("")
        businessAddressField.set("")
        isinField.set("")
        securityTokenField.set("")
        userFromField.set("")
        userToField.set("")
        dateTimeField.set("")
    }
}
