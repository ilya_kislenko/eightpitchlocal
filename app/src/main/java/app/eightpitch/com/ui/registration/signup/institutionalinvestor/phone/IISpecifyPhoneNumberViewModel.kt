package app.eightpitch.com.ui.registration.signup.institutionalinvestor.phone

import android.content.Context
import androidx.lifecycle.*
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.sms.SendTokenLimitResponse
import app.eightpitch.com.data.dto.user.CountryCode.Companion.displayValueToPhoneCode
import app.eightpitch.com.data.dto.user.CountryCode.Companion.getPhoneCodes
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.extensions.*
import app.eightpitch.com.models.*
import app.eightpitch.com.utils.*
import app.eightpitch.com.views.interfaces.ShortFieldValidator
import kotlinx.coroutines.launch

class IISpecifyPhoneNumberViewModel(
    private val appContext: Context,
    private val smsModel: SMSModel,
    private val supportModel: SupportModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val toolbarTitle = appContext.getString(R.string.creating_account_title_text)

    private var countryCode = ""
    private var userPhoneNumber = ""
    val userPhoneNumberErrorHolder = SingleActionObservableString()

    internal fun setCountryCode(position: Int) {
        countryCode = displayValueToPhoneCode(getPhoneCodes()[position])
    }

    internal fun getUserPhoneNumberWithCode() = userPhoneNumber.appendPrefixToPhone(countryCode)

    val userPhoneNumberWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            userPhoneNumber = sequence.toString()
        }
    }

    internal fun isPhoneNumberValid() {
        if (userPhoneNumber.isEmpty()) {
            userPhoneNumberErrorHolder.set(getErrorEmptyString(appContext))
        } else  {
            checkIfPhoneIsFree(getUserPhoneNumberWithCode())
        }
    }

    private val smsResult = SingleActionLiveData<Result<SendTokenLimitResponse>>()
    val getSMSResult: LiveData<Result<SendTokenLimitResponse>> = smsResult

    internal fun requestSMSCode() {
        viewModelScope.launch {
            val result = asyncLoading {
                smsModel.requireLimitSMSCodeByPhoneNumber(getUserPhoneNumberWithCode())
            }
            smsResult.postAction(result)
        }
    }

    private val phoneValidationResult = SingleActionLiveData<Result<Empty>>()
    val getPhoneValidationResult: LiveData<Result<Empty>> = phoneValidationResult

    private fun checkIfPhoneIsFree(phoneNumber: String) {
        viewModelScope.launch {
            val result = asyncLoading {
                supportModel.checkIfPhoneIsFree(phoneNumber)
            }
            phoneValidationResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        userPhoneNumber = ""
        userPhoneNumberErrorHolder.set("")
        countryCode = displayValueToPhoneCode(getPhoneCodes().first())
    }
}