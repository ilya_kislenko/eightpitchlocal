package app.eightpitch.com.ui.pincode

import android.os.Bundle
import android.view.*
import app.eightpitch.com.R
import app.eightpitch.com.base.BackPressureFragment
import app.eightpitch.com.extensions.finishActivity
import kotlinx.android.synthetic.main.fragment_biometric_set_up.*

class BiometricSetUpFragment : BackPressureFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_biometric_set_up, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        okButton.setDisablingClickListener {
            finishActivity()
        }

        crossButton.setOnClickListener {
            finishActivity()
        }

        registerOnBackPressedListener {
            finishActivity()
        }
    }
}