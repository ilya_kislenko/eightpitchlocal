package app.eightpitch.com.ui.kyc.personalinfo

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.extensions.moveBackward
import app.eightpitch.com.ui.GenderSpinnerAdapter
import app.eightpitch.com.ui.kyc.yourbirth.UserBirthInfoFragment
import kotlinx.android.synthetic.main.fragment_kyc_personal_info.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class KycPersonalInfoFragment : BaseFragment<KycPersonalInfoFragmentViewModel>() {

    override fun getLayoutID() = R.layout.fragment_kyc_personal_info

    override fun getVMClass() = KycPersonalInfoFragmentViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        setupGenderSpinner()

        registerOnBackPressedListener { moveBackward() }

        toolbarBackButton.setOnClickListener { moveBackward() }

        buttonNext.setOnClickListener {
            if (viewModel.validateInput()) {
                navigate(
                    R.id.action_kycPersonalInfoFragment_to_userBirthInfoFragment,
                    UserBirthInfoFragment.buildWithAnArguments(viewModel.getKYCInformationBody())
                )
            }
        }

        viewModel.getUserMeResult().observe(viewLifecycleOwner, Observer { result ->
            handleResult(result) { user ->
                genderSpinner.setSelection(viewModel.getUserSexArrayIndex(user))
            }
        })

        viewModel.loadUserOrRestoreState()
    }

    private fun setupGenderSpinner() {
        genderSpinner.apply {
            val genderSpinnerData = context.resources.getStringArray(R.array.genderType).toList()
            val genderAdapter = GenderSpinnerAdapter(context, genderSpinnerData)
            setAdapter(genderAdapter)
            setSelection(genderAdapter.getHintPosition())
            setSelectedListener({
                viewModel.setGender(it)
            }, {})
        }
    }
}
