package app.eightpitch.com.ui.registration.signup.institutionalinvestor.email

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.signup.CommonInvestorRequestBody
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.ui.registration.signup.institutionalinvestor.phone.IISpecifyPhoneNumberFragment
import app.eightpitch.com.ui.webview.DefaultWebViewFragment
import app.eightpitch.com.utils.Constants
import kotlinx.android.synthetic.main.fragment_pvi_personal_info.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class IIPersonalInfoEmailFragment : BaseFragment<IIPersonalInfoEmailFragmentViewModel>() {

    companion object {

        internal const val PARTIAL_USER = "PARTIAL_USER"

        fun buildWithAnArguments(
            partialUser: CommonInvestorRequestBody
        ) = Bundle().apply {
            putParcelable(PARTIAL_USER, partialUser)
        }
    }

    private val partialUser: CommonInvestorRequestBody
        get() = arguments?.getParcelable(PARTIAL_USER) ?: CommonInvestorRequestBody()

    override fun getLayoutID() = R.layout.fragment_ii_personal_info_email

    override fun getVMClass() = IIPersonalInfoEmailFragmentViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        additionalToolbarRightButton.setOnClickListener {
            val bundle = DefaultWebViewFragment.buildWithAnArguments(Constants.FAQ_URL)
            navigate(R.id.action_IIPersonalInfoEmailFragment_to_defaultWebViewFragment, bundle)
        }

        buttonNext.setOnClickListener {
            viewModel.validateEmail()
        }

        viewModel.getEmailValidationResult().observe(viewLifecycleOwner, Observer { result ->
            handleResult(result) { email ->
                val arguments = IISpecifyPhoneNumberFragment.buildWithAnArguments(partialUser.copy(email = email))
                navigate(R.id.action_IIPersonalInfoEmailFragment_to_IISpecifyPhoneNumberFragment, arguments)
            }
        })
    }
}