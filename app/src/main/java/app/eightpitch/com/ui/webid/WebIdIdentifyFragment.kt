package app.eightpitch.com.ui.webid

import android.Manifest
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import app.eightpitch.com.BuildConfig
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.extensions.*
import de.webid_solutions.mobile_app.sdk.domain.EUserActionIdentState.SUCCESSFUL
import de.webid_solutions.mobile_app.sdk.domain.EUserActionIdentState.UNSUCCESSFUL
import de.webid_solutions.mobile_app.sdk.domain.GetUserActionStatusResult
import kotlinx.android.synthetic.main.fragment_web_id_identify.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class WebIdIdentifyFragment : BaseFragment<WebIdIdentifyViewModel>() {

    companion object {

        private const val ACTION_ID_KEY = "ACTION_ID_KEY"

        fun buildWithAnArguments(
            actionId: String
        ) = Bundle().apply {
            putString(ACTION_ID_KEY, actionId)
        }
    }

    private val actionId: String
        get() = arguments?.getString(ACTION_ID_KEY) ?: ""

    private val locationPermissions = arrayOf(
        Manifest.permission.CAMERA,
        Manifest.permission.RECORD_AUDIO
    )

    override fun getLayoutID() = R.layout.fragment_web_id_identify

    override fun getVMClass() = WebIdIdentifyViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ifNotGrantedFinish()

        setupToolbar()
        bindLoadingIndicator()

        setupWebIdFunctional()

        registerOnBackPressedListener {
            moveBackward()
        }

        viewModel.getVerificationTANResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) { tanResult ->
                tanResult?.let {
                    viewModel.run { performCallFinished() }
                }
            }
        })

        viewModel.getActionResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) { response ->
                viewModel.verifyActionId(response.actionId)
                actionIdTextView.text = getString(R.string.your_action_id_pattern, response.actionId)
                //featureForTestTap()
            }
        })

        viewModel.getVerificationIdResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) { actionIdResult ->
                actionIdResult?.let {
                    activity?.let { viewModel.createCall(it, localVideoView to remoteVideoView) }
                }
            }
        })

        viewModel.getUserActionStatusResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) { userActionResult ->
                userActionResult?.let { navigateNext(it) }
            }
        })

        viewModel.getShowCameraResult().observe(viewLifecycleOwner, {
            additionalToolbarRightButton.visibility = it
        })

        viewModel.getAnyMessageResult().observe(viewLifecycleOwner, { message ->
            showDialog(message = message)
        })

        viewModel.getWebIDErrorsContainer()
            .observe(viewLifecycleOwner, { handleResult(it) {} })

        if (BuildConfig.BUILD_TYPE != "release") {
            if (actionId.isEmpty())
                viewModel.getActionId()
            else
                viewModel.verifyActionId(actionId)
        }
    }

    private fun navigateNext(userActionStatusResult: GetUserActionStatusResult) {
        when (userActionStatusResult.userActionStatus.identState) {
            SUCCESSFUL -> navigate(R.id.action_webIdIdentifyFragment_to_successfullyScreenFragment)
            UNSUCCESSFUL -> navigate(R.id.action_webIdIdentifyFragment_to_failureScreenFragment)
            else -> {
                context?.let {
                    Toast.makeText(it, it.getString(R.string.wait_your_status_please_text), Toast.LENGTH_LONG).show()
                }
                runDelayedInMainThread(8000) {
                    viewModel.getUserActionStatus()
                }
            }
        }
    }

    private fun setupWebIdFunctional() {
        setupClickListeners()
        actionIdTextView.text = getString(R.string.your_action_id_pattern, actionId)
        viewModel.apply {
            setupWebIdArguments()
            setupCallbacks()
        }
    }

    private fun setupClickListeners() {
        startCallButton.setOnClickListener {
            if (actionId.isEmpty())
                viewModel.getActionId()
            else {
                viewModel.verifyActionId(actionId)
            }
        }
        cancelCallButton.setOnClickListener {
            viewModel.stopCall()
        }
        applyButton.setDisablingClickListener {
            viewModel.verifyTan()
        }
    }

    private fun setupToolbar() {
        context?.let {
            toolbarBackButton.apply {
                setImageDrawable(ContextCompat.getDrawable(it, R.drawable.icon_cross))
                setOnClickListener {
                    moveBackward()
                }
            }
            additionalToolbarRightButton.apply {
                setImageDrawable(ContextCompat.getDrawable(it, R.drawable.icon_switch_camera))
                setDisablingClickListener {
                    viewModel.switchCamera()
                }
            }
        }
    }

    private fun ifNotGrantedFinish() {
        if (!isAliveAndAvailable())
            return
        activity?.let {
            it.checkPermissions(locationPermissions,
                performIfAllIsGranted = {},
                performIfSomeDenied = { _ ->
                    it.finish()
                })
        }
    }
}
