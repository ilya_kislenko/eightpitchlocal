package app.eightpitch.com.ui.twofactor.addextrasecurity

import android.os.Bundle
import android.view.*
import app.eightpitch.com.R
import app.eightpitch.com.base.BackPressureFragment
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.twofactor.choosetwoauthmethod.ChooseTwoAuthMethodFragment
import app.eightpitch.com.ui.twofactor.choosetwoauthmethod.ChooseTwoAuthMethodFragment.Companion.NAVIGATE_FROM_KYC
import kotlinx.android.synthetic.main.fragment_kyc_add_extra_security.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class KycAddExtraSecurityFragment : BackPressureFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_kyc_add_extra_security, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        registerOnBackPressedListener { moveBackward() }

        headerTitle.text = context?.getString(R.string.add_extra_security_toolbar_title)
        additionalToolbarRightButton.visibility = View.GONE
        toolbarBackButton.apply {
            setImageResource(R.drawable.icon_cross)
            setOnClickListener { moveBackward() }
        }

        buttonGetStarted.setOnClickListener {
            navigate(R.id.action_kycAddExtraSecurityFragment_to_chooseTwoAuthMethodFragment,
                ChooseTwoAuthMethodFragment.buildWithAnArguments(NAVIGATE_FROM_KYC))
        }

        skipTextView.setOnClickListener {
            navigate(R.id.action_kycAddExtraSecurityFragment_to_kycPersonalInfoFragment)
        }
    }
}
