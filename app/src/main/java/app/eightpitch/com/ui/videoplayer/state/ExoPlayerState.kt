package app.eightpitch.com.ui.videoplayer.state

import com.google.android.exoplayer2.SimpleExoPlayer

/**
 * Class that represents a state of [SimpleExoPlayer]
 */
data class ExoPlayerState(
    val volume: Float = 1f,
    val currentWindow: Int = 0,
    val muted: Boolean = false,
    val fullScreen: Boolean = false,
    val playbackPosition: Long = 0L,
    val playWhenReady: Boolean = true
)