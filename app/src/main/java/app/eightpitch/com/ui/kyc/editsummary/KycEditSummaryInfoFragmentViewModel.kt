package app.eightpitch.com.ui.kyc.editsummary

import android.content.Context
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.user.Country.Companion.fromDisplayValue
import app.eightpitch.com.data.dto.webid.WebIdUserRequestBody
import app.eightpitch.com.extensions.getErrorEmptyString
import app.eightpitch.com.extensions.getSortedCountries
import app.eightpitch.com.extensions.validateByDefaultPattern
import app.eightpitch.com.utils.Constants.DD_p_MM_p_YYYY
import app.eightpitch.com.utils.DateUtils.formatByPattern
import app.eightpitch.com.utils.DateUtils.parseByFormatWithoutMMSS
import app.eightpitch.com.utils.DateUtils.toInstant
import app.eightpitch.com.utils.IBANValidator
import app.eightpitch.com.utils.RegexPatterns.PATTERN_OWNER_PREFIX_CORRECT
import app.eightpitch.com.utils.RegexPatterns.PATTERN_VALID_CITY
import app.eightpitch.com.utils.RegexPatterns.PATTERN_VALID_CODE
import app.eightpitch.com.utils.RegexPatterns.PATTERN_VALID_NAME
import app.eightpitch.com.utils.RegexPatterns.PATTERN_VALID_OWNER
import app.eightpitch.com.utils.SingleActionObservableString
import app.eightpitch.com.utils.YearsValidator.validateYearsOld
import app.eightpitch.com.views.interfaces.ShortFieldValidator
import java.time.LocalDate
import javax.inject.Inject

class KycEditSummaryInfoFragmentViewModel @Inject constructor(
    private val appContext: Context,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val title = appContext.getString(R.string.kyc_edit_summary)

    val firstNameField = ObservableField("")
    val lastNameField = ObservableField("")
    val birthDateField = ObservableField("")
    val cityField = ObservableField("")
    val zipCodeField = ObservableField("")
    val streetField = ObservableField("")
    val streetNoField = ObservableField("")
    val ownerField = ObservableField("")
    val ibanField = ObservableField("")
    val userPoliticallyField = ObservableBoolean()
    val userLiabilityField = ObservableBoolean()

    private var firstName: String = ""
    val firstNameErrorHolder = SingleActionObservableString()

    private var lastName: String = ""
    val lastNameErrorHolder = SingleActionObservableString()

    var birthDateMillis: Long = 0L
    val birthDateErrorHolder = SingleActionObservableString()

    private var city: String = ""
    val cityErrorHolder = SingleActionObservableString()

    private var zipCode: String = ""
    val zipCodeErrorHolder = SingleActionObservableString()

    private var street: String = ""
    val streetErrorHolder = SingleActionObservableString()

    private var number: String = ""
    val numberErrorHolder = SingleActionObservableString()

    private var owner: String = ""
    val ownerErrorHolder = SingleActionObservableString()

    private var IBAN: String = ""
    val IBANErrorHolder = SingleActionObservableString()

    private var country = ""
    val countryErrorHolder = SingleActionObservableString()

    private var nationality = ""
    val nationalityErrorHolder = SingleActionObservableString()

    val firstNameWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            firstName = sequence.toString()
        }
    }

    val lastNameWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            lastName = sequence.toString()
        }
    }

    val cityWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            city = sequence.toString()
        }
    }

    val zipCodeWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            zipCode = sequence.toString()
        }
    }

    val streetWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            street = sequence.toString()
        }
    }

    val numberWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            number = sequence.toString()
        }
    }

    val ownerWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            owner = sequence.toString()
        }
    }

    val ibanWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            IBAN = sequence.toString()
        }
    }

    internal fun setNationality(position: Int) {
        nationality = appContext.getSortedCountries().getOrElse(position) { "" }
    }

    internal fun setCountry(position: Int) {
        country = appContext.getSortedCountries().getOrElse(position) { "" }
    }

    internal fun setupBinding(webIdUserRequestBody: WebIdUserRequestBody) {
        webIdUserRequestBody.apply {
            firstNameField.set(firstName)
            lastNameField.set(lastName)
            birthDateField.set(_dateOfBirth)
            birthDateMillis = _dateOfBirth.parseByFormatWithoutMMSS()
            webIdUserRequestBody.address.apply {
                streetNoField.set(streetNumber)
                zipCodeField.set(zipCode)
                cityField.set(city)
                streetField.set(street)
            }
            webIdUserRequestBody.webIdPersonalInfo.apply {
                ownerField.set(accountOwner)
                ibanField.set(IBAN)
                userPoliticallyField.set(isPoliticallyExposedPerson)
                userLiabilityField.set(isUsTaxLiability)
            }
        }
    }

    fun addFieldDataToKycBody(
        webIdUserRequestBody: WebIdUserRequestBody,
        isUserPolitically: Boolean,
        isUserLiability: Boolean
    ): WebIdUserRequestBody =
        webIdUserRequestBody.copy(
            firstName = firstName,
            lastName = lastName,
            _dateOfBirth = birthDateMillis.formatByPattern(),
            webIdPersonalInfo = webIdUserRequestBody.webIdPersonalInfo.copy(
                nationality = fromDisplayValue(appContext, nationality).toString(),
                accountOwner = owner,
                IBAN = IBAN.replace(" ", ""),
                isPoliticallyExposedPerson = isUserPolitically,
                isUsTaxLiability = isUserLiability
            ),
            address = webIdUserRequestBody.address.copy(
                country = fromDisplayValue(appContext, country).toString(),
                city = city,
                street = street,
                streetNumber = number,
                zipCode = zipCode
            )
        )

    fun validateInput(): Boolean {

        val firstNameError =
            firstName.validateByDefaultPattern(appContext, R.string.first_name, PATTERN_VALID_NAME)
        if (firstNameError.isNotEmpty())
            firstNameErrorHolder.set(firstNameError)

        val secondNameError =
            lastName.validateByDefaultPattern(appContext, R.string.last_name, PATTERN_VALID_NAME)
        if (secondNameError.isNotEmpty())
            lastNameErrorHolder.set(secondNameError)

        val birthDateError = when {
            birthDateMillis == 0L -> getErrorEmptyString(appContext)
            validateYearsOld(birthDateMillis) -> appContext.getString(R.string.not_valid_years_old)
            else -> ""
        }
        if (birthDateError.isNotEmpty())
            birthDateErrorHolder.set(birthDateError)

        val nationalityError = when {
            nationality.isEmpty() -> getErrorEmptyString(appContext)
            else -> ""
        }
        if (nationalityError.isNotEmpty())
            nationalityErrorHolder.set(nationalityError)

        val countryError = when {
            country.isEmpty() -> getErrorEmptyString(appContext)
            else -> ""
        }
        if (countryError.isNotEmpty())
            countryErrorHolder.set(countryError)

        val cityError = city.validateByDefaultPattern(appContext, R.string.city, PATTERN_VALID_CITY)
        if (cityError.isNotEmpty())
            cityErrorHolder.set(cityError)

        val zipCodeError =
            zipCode.validateByDefaultPattern(appContext, R.string.zip_code, PATTERN_VALID_CODE, "")
        if (zipCodeError.isNotEmpty())
            zipCodeErrorHolder.set(zipCodeError)

        val streetError =
            street.validateByDefaultPattern(appContext, R.string.street, PATTERN_VALID_NAME, "")
        if (streetError.isNotEmpty())
            streetErrorHolder.set(streetError)

        val streetNumberError =
            number.validateByDefaultPattern(appContext, R.string.number, PATTERN_VALID_CODE, "")
        if (streetNumberError.isNotEmpty())
            numberErrorHolder.set(streetNumberError)

        val accountOwnerError =
            owner.validateByDefaultPattern(
                appContext,
                R.string.account_owner,
                PATTERN_VALID_OWNER,
                PATTERN_OWNER_PREFIX_CORRECT
            )
        if (accountOwnerError.isNotEmpty())
            ownerErrorHolder.set(accountOwnerError)

        val IBANError = when {
            IBAN.isEmpty() -> getErrorEmptyString(appContext)
            !IBANValidator.validate(IBAN) -> appContext.getString(R.string.invalid_iban)
            else -> ""
        }
        if (IBANError.isNotEmpty())
            IBANErrorHolder.set(IBANError)

        return firstNameError.isEmpty() && secondNameError.isEmpty()
               && birthDateError.isEmpty() && nationalityError.isEmpty()
                && countryError.isEmpty() && cityError.isEmpty()
               && zipCodeError.isEmpty() && streetError.isEmpty()
               && streetNumberError.isEmpty() && accountOwnerError.isEmpty()
               && IBANError.isEmpty()
    }

    internal fun setBirthDate(localDate: LocalDate) {
        birthDateMillis = localDate.toInstant()
        val formattedString = birthDateMillis.formatByPattern(DD_p_MM_p_YYYY, false)
        birthDateField.set(formattedString)
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        firstNameField.set("")
        lastNameField.set("")
        birthDateField.set("")
        cityField.set("")
        zipCodeField.set("")
        streetField.set("")
        streetNoField.set("")
        ownerField.set("")
        ibanField.set("")
    }
}