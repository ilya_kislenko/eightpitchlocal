package app.eightpitch.com.ui.projects

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.projectfilter.SharableFilterViewModel
import app.eightpitch.com.ui.projects.adapters.ProjectAdapter
import app.eightpitch.com.ui.projectsdetail.ProjectDetailsFragment
import app.eightpitch.com.utils.SafeLinearLayoutManager
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_projects.*

class ProjectsFragment : BaseFragment<ProjectsFragmentViewModel>() {

    override fun getLayoutID() = R.layout.fragment_projects

    override fun getVMClass() = ProjectsFragmentViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        viewModel.run {
            getUserMeResult().observe(viewLifecycleOwner, { result -> handleResult(result) })
            getInvestorGraphResult().observe(viewLifecycleOwner, { result -> handleResult(result) })
            getInvestorProjectsResult().observe(viewLifecycleOwner, { result -> handleResult(result) })
            getProjectsResult().observe(viewLifecycleOwner, { result -> handleResult(result) })
        }

        projectsRecycler.apply {
            layoutManager = SafeLinearLayoutManager(context, VERTICAL, false)
            adapter = ProjectAdapter().apply {
                onProjectClicked = { projectItem ->
                    navigate(R.id.action_projectsFragment_to_projectDetailsFragment,
                        ProjectDetailsFragment.buildWithAnArguments(projectItem.id.toString()))
                }
            }
        }

        projectChangeModeTabLayout.apply {
            addOnTabSelectedListener(OnProjectsTabListener())
        }

        iconSearch.apply {
            setOnClickListener {
                navigate(R.id.action_projectsFragment_to_searchProjectsFragment)
            }
        }

        val sharedViewModel = injectViewModel(SharableFilterViewModel::class.java)
        viewModel.setFilters(sharedViewModel)
    }

    override fun onResume() {
        super.onResume()
        viewModel.getUser()
        projectChangeModeTabLayout.selectTabWith(viewModel.getSavedTabPosition())
    }

    private inner class OnProjectsTabListener : TabLayout.OnTabSelectedListener {

        override fun onTabReselected(tab: TabLayout.Tab?) {
            loadProjectsWithType(tab)
        }

        override fun onTabUnselected(tab: TabLayout.Tab?) {
            /*Do nothing*/
        }

        override fun onTabSelected(tab: TabLayout.Tab?) {
            loadProjectsWithType(tab)
        }
    }

    private fun loadProjectsWithType(selectedTab: TabLayout.Tab?) {
        selectedTab?.let { viewModel.loadProjectsForType(it.position) }
    }
}
