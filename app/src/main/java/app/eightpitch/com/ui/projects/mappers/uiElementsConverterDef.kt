package app.eightpitch.com.ui.projects.mappers

import android.view.View
import app.eightpitch.com.data.dto.dynamicviews.*
import app.eightpitch.com.data.dto.dynamicviews.builders.*
import app.eightpitch.com.data.dto.dynamicviews.language.I18nData
import javax.inject.Inject

/**
 * This converter is designed to map a [UIElement]
 * to a corresponded [View]
 */
interface IUIElementsConverter {

    /**
     * Method that is converting a [UIElement]
     * to a corresponded [View]
     */
    fun convertUIElements(items: List<UIElement>, i18nData: I18nData): List<View>
    fun convertTabletUIElements(items: List<UIElement>, i18nData: I18nData): List<View>
}

class UIElementsConverter
@Inject constructor(
    private val uiElementsBuilder: UIElementsBuilder,
    private val uiElementsTabletBuilder: UIElementsTabletBuilder
) : IUIElementsConverter {

    override fun convertUIElements(items: List<UIElement>, i18nData: I18nData): List<View> {
        return items.map { uiElement ->
            uiElementsBuilder.run {
                when (uiElement) {
                    is ImageUIElement -> buildImageView(uiElement, i18nData)
                    is TextUIElement -> buildTextView(uiElement, i18nData)
                    is DividerUIElement -> buildDividerView(uiElement)
                    is FAQUIElement -> buildFAQView(uiElement)
                    is IconsUIElement -> buildIconsView(uiElement)
                    is ImageAndTextUIElement -> buildImageAndTextView(uiElement, i18nData)
                    is ListUIElement -> buildListView(uiElement, i18nData)
                    is TableUIElement -> buildTableView(uiElement, i18nData)
                    is VimeoUIElement -> buildVimeoView(uiElement, i18nData)
                    is ChartItems -> buildCharts(uiElement, i18nData)
                    is DualColumnImagesUIElement -> buildDualColumnImages(uiElement, i18nData)
                    is FactsUIElement -> buildFacts(uiElement, i18nData)
                    is DualColumnTextUIElement -> buildDualColumnText(uiElement, i18nData)
                    is TiledImagesUIElement -> buildTiledImage(uiElement, i18nData)
                    is ButtonUIElement -> buildButton(uiElement, i18nData)
                    is DisclaimerUiElement -> buildDisclaimer(uiElement, i18nData)
                    is LinkedTextUIElement -> buildLinkedText(uiElement)
                    is HeaderUIElement -> buildHeader(uiElement, i18nData)
                    is DepartmentUIElement -> buildDepartment(uiElement)
                    is ImageLinkUIElement -> buildImageLink(uiElement, i18nData)
                    is Group -> buildGroupUIElementView(uiElement)
                }
            }
        }
    }

    override fun convertTabletUIElements(items: List<UIElement>, i18nData: I18nData): List<View> {
        return items.map { uiElement ->
            uiElementsTabletBuilder.run {
                when (uiElement) {
                    is ImageUIElement -> buildImageView(uiElement, i18nData)
                    is TextUIElement -> buildTextView(uiElement, i18nData)
                    is DividerUIElement -> buildDividerView(uiElement)
                    is FAQUIElement -> buildFAQView(uiElement)
                    is IconsUIElement -> buildIconsView(uiElement)
                    is ImageAndTextUIElement -> buildImageAndTextView(uiElement, i18nData)
                    is ListUIElement -> buildListView(uiElement, i18nData)
                    is TableUIElement -> buildTableView(uiElement,i18nData)
                    is VimeoUIElement -> buildVimeoView(uiElement,i18nData)
                    is ChartItems -> buildCharts(uiElement, i18nData)
                    is DualColumnImagesUIElement -> buildDualColumnImages(uiElement, i18nData)
                    is FactsUIElement -> buildFacts(uiElement, i18nData)
                    is DualColumnTextUIElement -> buildDualColumnText(uiElement, i18nData)
                    is TiledImagesUIElement -> buildTiledImage(uiElement, i18nData)
                    is ButtonUIElement -> buildButton(uiElement, i18nData)
                    is DisclaimerUiElement -> buildDisclaimer(uiElement, i18nData)
                    is LinkedTextUIElement -> buildLinkedText(uiElement)
                    is HeaderUIElement -> buildHeader(uiElement, i18nData)
                    is DepartmentUIElement -> buildDepartment(uiElement)
                    is ImageLinkUIElement -> buildImageLink(uiElement, i18nData)
                    is Group -> buildGroupUIElementView(uiElement)
                }
            }
        }
    }
}
 
 