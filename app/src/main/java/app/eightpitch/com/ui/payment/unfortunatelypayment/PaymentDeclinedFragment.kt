package app.eightpitch.com.ui.payment.unfortunatelypayment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import app.eightpitch.com.R
import app.eightpitch.com.base.rootbackhandlers.ChildNavGraphBackPressureHandler
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.payment.hardcappaymentselection.HardCapPaymentSelectionFragment
import app.eightpitch.com.ui.payment.softcappaymentselection.SoftCapPaymentSelectionFragment
import kotlinx.android.synthetic.main.fragment_payment_declined.*

class PaymentDeclinedFragment : Fragment(), ChildNavGraphBackPressureHandler {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View =
        inflater.inflate(R.layout.fragment_payment_declined, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        crossButton.setOnClickListener {
            handleBackWard()
        }

        okButton.setOnClickListener {
            handleBackWard()
        }
    }

    override fun handleBackPressure(): Boolean {
        handleBackWard()
        return true
    }

    @SuppressLint("RestrictedApi")
    private fun handleBackWard() {
        view?.let {
            val backStack = Navigation.findNavController(it).backStack
            when {
                backStack.foundFragmentByName(HardCapPaymentSelectionFragment::class) -> popBackStack(R.id.hardCapPaymentSelectionFragment)
                backStack.foundFragmentByName(SoftCapPaymentSelectionFragment::class) -> popBackStack(R.id.softCapPaymentSelectionFragment)
                else -> popBackStack(R.id.projectsFragment)
            }
        }
    }
}
