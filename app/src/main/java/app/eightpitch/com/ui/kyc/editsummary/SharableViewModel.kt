package app.eightpitch.com.ui.kyc.editsummary

import androidx.lifecycle.ViewModel
import app.eightpitch.com.data.dto.webid.WebIdUserRequestBody

class SharableViewModel(var webIdUserRequestBody: WebIdUserRequestBody?) : ViewModel() {

    fun getKycInfo(): WebIdUserRequestBody? = webIdUserRequestBody
}