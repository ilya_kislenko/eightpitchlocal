package app.eightpitch.com.ui.dynamicdetails

import android.view.*
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.R
import app.eightpitch.com.data.dto.dynamicviews.FactItem
import app.eightpitch.com.data.dto.dynamicviews.ImageUIElementArguments
import app.eightpitch.com.extensions.*

class FactsAdapter(private val items: List<FactItem>) :
    RecyclerView.Adapter<FactsAdapter.FactsHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FactsHolder {
        return FactsHolder(inflateView(parent, R.layout.item_fact))
    }

    override fun onBindViewHolder(holder: FactsHolder, position: Int) {
        val fact = items[position]
        holder.apply {
            val imageId = fact.imageId
            imageView.tag = ImageUIElementArguments("", imageId)
            description.text = fact.text
        }
    }

    override fun getItemCount() = items.size

    class FactsHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageView: AppCompatImageView = itemView.findViewById(R.id.imageView)
        val description: AppCompatTextView = itemView.findViewById(R.id.descriptionTextView)
    }
}