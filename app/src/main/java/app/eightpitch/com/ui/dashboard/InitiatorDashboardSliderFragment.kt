package app.eightpitch.com.ui.dashboard

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import app.eightpitch.com.R
import app.eightpitch.com.data.dto.user.LinkSliderItemResponseBody
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.webview.DefaultWebViewFragment
import app.eightpitch.com.utils.Constants.FILES_ENDPOINT
import kotlinx.android.synthetic.main.initiator_dashboard_slider_layout.*

class InitiatorDashboardSliderFragment : Fragment() {

    companion object {

        const val SLIDE_INFO = "SLIDE_INFO"

        fun createFragmentWithArguments(
            linkSliderItemResponseBody: LinkSliderItemResponseBody,
        ) = InitiatorDashboardSliderFragment().apply {
            arguments = Bundle().apply {
                putParcelable(SLIDE_INFO, linkSliderItemResponseBody)
            }
        }
    }

    private val linkSliderItemResponseBody: LinkSliderItemResponseBody
        get() = arguments?.getParcelable(SLIDE_INFO) ?: LinkSliderItemResponseBody()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.initiator_dashboard_slider_layout, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        linkSliderItemResponseBody.run {
            headerText.text = header
            context?.loadImage(url = "$FILES_ENDPOINT$backgroundImageFileId", targetImageView = slideBackground)

            if (link.isNotEmpty()) {
                slideBackground.setOnClickListener {
                    val arguments = DefaultWebViewFragment.buildWithAnArguments(link)
                    navigate(R.id.action_initiatorStatisticFragment_to_defaultWebViewFragment, arguments)
                }
            }
        }
    }
}