package app.eightpitch.com.ui.changeemail

import android.content.Context
import android.util.Patterns.EMAIL_ADDRESS
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.data.transmit.ResultStatus.SUCCESS
import app.eightpitch.com.extensions.getErrorEmptyString
import app.eightpitch.com.extensions.wrapWithAnEmptyResult
import app.eightpitch.com.models.SMSModel
import app.eightpitch.com.models.SupportModel
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.utils.Empty
import app.eightpitch.com.utils.SingleActionLiveData
import app.eightpitch.com.utils.SingleActionObservableString
import app.eightpitch.com.views.interfaces.ShortFieldValidator
import kotlinx.coroutines.launch
import javax.inject.Inject

class ChangeEmailFragmentViewModel @Inject constructor(
    private val appContext: Context,
    private val userModel: UserModel,
    private val smsModel: SMSModel,
    private val supportModel: SupportModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val title = appContext.getString(R.string.change_email_title)
    val emailField = ObservableField("")

    private var email: String = ""
    val emailErrorHolder = SingleActionObservableString()

    val emailWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            email = sequence.toString()
        }
    }

    internal fun getEmail() = email

    private val userMeResult = SingleActionLiveData<Result<Empty>>()
    internal fun getUserMeResult(): LiveData<Result<Empty>> = userMeResult

    internal fun getUser() {
        viewModelScope.launch {

            val result = asyncLoading {
                userModel.getUserData()
            }

            if (result.status == SUCCESS) {
                emailField.set(result.result?.email)
            }

            userMeResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    fun validateEmail() {
        val emailError = when {
            email.isEmpty() -> getErrorEmptyString(appContext)
            !EMAIL_ADDRESS.matcher(email)
                .matches() -> appContext.getString(R.string.not_valid_email)
            else -> ""
        }.takeIf { it.isNotEmpty() }?.let { emailErrorHolder.set(it); it }

        if (emailError.isNullOrBlank())
            requireSMSToken()
    }

    private val requireEmailTokenResult = SingleActionLiveData<Result<Empty>>()
    fun getRequireEmailTokenResult(): LiveData<Result<Empty>> = requireEmailTokenResult

    private fun requireSMSToken() {
        viewModelScope.launch {
            val trimmedEmail = email.trim()
            val result = asyncLoading {
                supportModel.checkIfEmailIsFree(trimmedEmail)
                smsModel.requireCodeByNewEmail(trimmedEmail)
            }
            requireEmailTokenResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        email = ""
        emailErrorHolder.set("")
    }
}