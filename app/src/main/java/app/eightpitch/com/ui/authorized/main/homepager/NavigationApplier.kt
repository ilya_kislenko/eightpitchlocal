package app.eightpitch.com.ui.authorized.main.homepager

import app.eightpitch.com.base.rootbackhandlers.RootNavGraphHandleAbleFragment

/**
 * It's an interface that extends behavior of [RootNavGraphHandleAbleFragment] to
 * be able to apply any navigation id within a related nav graph.
 */
interface NavigationApplier {

    /**
     * Method that should apply [navigationId] within a related nav graph.
     */
    fun applyNavigationState(navigationId: Int)

    fun popUpToStartDestination()
}