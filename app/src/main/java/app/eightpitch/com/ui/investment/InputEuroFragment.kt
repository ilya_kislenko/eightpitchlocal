package app.eightpitch.com.ui.investment

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.investment.ProjectInvestmentInfoResponse
import app.eightpitch.com.extensions.*
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.android.synthetic.main.fragment_input_euro.*
import java.math.BigDecimal

class InputEuroFragment : BaseFragment<InputEuroViewModel>() {

    companion object {

        const val PROJECT_INVESTMENT = "PROJECT_INVESTMENT"

        fun newInstance(
            arguments: Bundle,
            currencyInputValidatorObserver: SingleActionLiveData<Boolean>
        ) =
            InputEuroFragment().apply {
                this.arguments = arguments
                this.currencyInputValidatorObserver = currencyInputValidatorObserver
            }

        fun buildWithAnArguments(
            projectInvestmentInfo: ProjectInvestmentInfoResponse
        ) = Bundle().apply {
            putParcelable(PROJECT_INVESTMENT, projectInvestmentInfo)
        }
    }

    private var currencyInputValidatorObserver: SingleActionLiveData<Boolean>? = null
    private val projectInvestmentInfo: ProjectInvestmentInfoResponse
        get() = arguments?.getParcelable(PROJECT_INVESTMENT) ?: ProjectInvestmentInfoResponse()

    override fun getLayoutID() = R.layout.fragment_input_euro

    override fun getVMClass() = InputEuroViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()

        projectInvestmentInfo.run {
            viewModel.setVariables(this)

            val investmentStepEuro = nominalValue.multiply(investmentStepSize.toBigDecimal())
            plusButton.setOnClickListener {
                increaseInput(investmentStepEuro)
            }

            minusButton.setOnClickListener {
                decreaseInput(investmentStepEuro)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        actualizeInputCurrency()
        viewModel.getInputResult().observe(viewLifecycleOwner, Observer {
            viewModel.euro?.let { euro ->
                val tokens = convertToBgh(euro.parseToBigDecimalSafe())
                getSharableViewModel().setValue(tokens)
            }
        })
    }

    private fun actualizeInputCurrency() {
        setCurrencyValue(convertToEuro(getSharableViewModel().getTokens()))
    }

    private fun getSharableViewModel() = injectViewModel(ViewPagerSharableViewModel::class.java)

    private fun convertToEuro(tokens: BigDecimal) =
        tokens.multiply(projectInvestmentInfo.nominalValue)

    private fun convertToBgh(euro: BigDecimal) = euro.divide(projectInvestmentInfo.nominalValue)

    private fun setupUI() {
        projectInvestmentInfo.run {
            val eur = getString(R.string.eur_text)

            val investmentAmountEuro = nominalValue.multiplySafe(minimumInvestmentAmount)
            "${investmentAmountEuro.formatToCurrencyView()} $eur".also { valueMinimalAmountEuro.text = it }

            val investmentStepEuro = nominalValue.multiplySafe(investmentStepSize)
            "${investmentStepEuro.formatToCurrencyView()} $eur".also { valueStepEuro.text = it }

            investorClassLimit?.let { limit ->
                "${limit.formatToCurrencyView()} $eur".also { valueMaximumAmountForAccountEuro.text = it }
            } ?: maximumAmountForAccountContainer.hide()
        }

        setupHelpButtons()
        viewModel.run {
            addMessageChangeListener(viewLifecycleOwner)
            messageVisibilityObserver.observe(viewLifecycleOwner, { block ->
                currencyInputValidatorObserver?.sendAction(block)
            })
        }
    }

    private fun increaseInput(step: BigDecimal) {
        euroInput.run {
            val currentValue = inputToBigDecimal()
            setCurrencyValue(currentValue + step)
        }
    }

    private fun decreaseInput(step: BigDecimal) {
        euroInput.run {
            val currentValue = inputToBigDecimal()
            setCurrencyValue(currentValue - step)
        }
    }

    private fun setCurrencyValue(value: BigDecimal) {
        euroInput.setText(value.setScaleSafe(0).formatToCurrencyView())
    }

    private fun setupHelpButtons() {
        lessValue.setOnClickListener {
            val value = viewModel.getLessValue() ?: BigDecimal.ZERO
            setInputValue(value)
        }
        moreValue.setOnClickListener {
            val value = viewModel.getMoreValue() ?: BigDecimal.ZERO
            setInputValue(value)
        }
    }

    private fun setInputValue(value: BigDecimal) {
        viewModel.hideMessage()
        setCurrencyValue(value)
    }
}