package app.eightpitch.com.ui.investment

import android.content.Context
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.investment.ProjectInvestmentInfoResponse
import app.eightpitch.com.extensions.*
import app.eightpitch.com.utils.SingleActionLiveData
import app.eightpitch.com.views.interfaces.ShortFieldValidator
import java.math.BigDecimal
import java.util.*

class InputEuroViewModel(
    private val appContext: Context,
    threadsSeparator: ThreadsSeparator,
) : BaseViewModel(threadsSeparator) {

    var tokenName: String = ""
    private var userLimit = BigDecimal.ZERO
    private var minValue = BigDecimal.ZERO
    private var maxValue = BigDecimal.ZERO
    private var investmentStep = BigDecimal.ONE
    private var investmentFee = BigDecimal.ONE
    private var nomValue = BigDecimal.ONE

    internal fun setVariables(info: ProjectInvestmentInfoResponse) {
        info.run {
            tokenName = shortCut
            maxValue = maximumInvestmentAmount * nominalValue
            userLimit = investorClassLimit ?: maxValue
            minValue = nominalValue.multiplySafe(minimumInvestmentAmount)
            investmentStep = nominalValue.multiplySafe(investmentStepSize)
            investmentFee = assetBasedFees.setScale(2)
            nomValue = nominalValue
        }
    }

    /**
     * Message about incorrect user input.
     */
    val messageVisibility = ObservableBoolean()
    val currencyInputMessage =
        ObservableField(appContext.getString(
            R.string.investment_currency_input_rules_text,
            investmentStep.asCurrency(),
            appContext.getString(R.string.eur_text))
        )

    private fun showMessage() = run { messageVisibility.set(true) }
    internal fun hideMessage() = run { messageVisibility.set(false) }

    /**
     * Adding new info messages for cases when user input less then minimal and more them maximum
     */
    private fun setMessageText(message: String) = currencyInputMessage.set(message)

    internal val messageVisibilityObserver = SingleActionLiveData<Boolean>()

    /**
     * If message is visible - hide convertText(F.e. = 100BGH), show help buttons with the closest less and more values.
     * If message is invisible then on the contrary
     */
    internal fun addMessageChangeListener(lifecycleOwner: LifecycleOwner) {
        messageVisibility.onChangedSafe(lifecycleOwner) { toVisible ->
            changeVisibilityBgh(!toVisible)
            messageVisibilityObserver.value = !toVisible
            if (!toVisible)
                hideHelpButtons()
        }
    }

    /**
     * Button with less value (left button)
     * @see lessValue
     */
    val lessValue = ObservableField(BigDecimal.ZERO)
    val lessButtonVisibility = ObservableBoolean()

    private fun setLessValue(value: BigDecimal) = run { lessValue.set(value.setScale(2)) }
    internal fun getLessValue() = lessValue.get()

    /**
     * Button with more value (right button)
     * @see moreValue
     */
    val moreValue = ObservableField<BigDecimal>()
    val moreButtonVisibility = ObservableBoolean()

    private fun setMoreValue(value: BigDecimal) = run { moreValue.set(value.setScale(2)) }
    internal fun getMoreValue() = moreValue.get()

    /**
     * Converting user input from euro to bgh tokens.
     * @see token
     */

    val token = ObservableField<BigDecimal>()
    val tokenVisibility = ObservableBoolean(true)

    private fun changeVisibilityBgh(visibility: Boolean) {
        tokenVisibility.set(visibility)
    }

    private fun convertToBGH(value: BigDecimal) = value.divideSafe(investmentStep)

    /**
     * Observable user input
     */
    private val inputResult = SingleActionLiveData<BigDecimal>()
    internal fun getInputResult(): LiveData<BigDecimal> = inputResult

    var euro: String? = null
        private set(value) {
            field = value
            value?.let {
                validateInput(value)
            }
        }

    val euroTextWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            //Skipping initial state
            if (euro == null && sequence.isEmpty())
                return
            euro = sequence.toString()
        }
    }

    val fee = ObservableField<String>()
    val valueFee = ObservableField<String>()

    private fun setInvestmentFee(countedFee: BigDecimal) {
        val eurText = appContext.getString(R.string.eur_text)
        fee.set(String.format(Locale.getDefault(),
            appContext.getString(R.string.investment_fee_text),
            investmentFee.formatToCurrencyView()))
        valueFee.set("${countedFee.setScale(2).formatToCurrencyView()} $eurText")
    }

    /**
     * Buttons for increasing and decreasing input value.
     * Block the button if entered less minValue or more maxValue
     */
    val plusButtonAvailability = ObservableBoolean(true)
    val minusButtonAvailability = ObservableBoolean(false)

    private fun blockPlusButton() {
        plusButtonAvailability.set(false)
    }

    private fun blockMinusButton() {
        minusButtonAvailability.set(false)
    }

    private fun unblockPlusMinusButtons() {
        plusButtonAvailability.set(true)
        minusButtonAvailability.set(true)
    }

    val total = ObservableField(0.toBigDecimal())

    private fun setTotal(value: BigDecimal) {
        total.set(value.setScale(2))
    }

    val totalShares = ObservableField<String>()

    private fun setTotalShares(value: BigDecimal) {
        totalShares.set(appContext.getString(
            R.string.total_shares_pattern_text,
            value.setScale(2).formatToCurrencyView(),
            tokenName)
        )
    }

    private fun validateInput(input: String) {
        val value = input.replace(".", "").parseToBigDecimalSafe()
        inputResult.postAction(value)
        handlePlusMinusButtonsState(value)
        validateValue(value)
    }

    private fun handlePlusMinusButtonsState(value: BigDecimal) {
        when {
            value >= maxValue || value >= userLimit -> {
                blockPlusButton()
                minusButtonAvailability.set(true)
            }
            value <= minValue -> {
                blockMinusButton()
                plusButtonAvailability.set(true)
            }
            else -> unblockPlusMinusButtons()
        }
    }

    private fun validateValue(value: BigDecimal) {
        if (value in minValue..userLimit) {
            when (value.rem(investmentStep).compareTo(BigDecimal.ZERO)) {
                0 -> {
                    hideMessage()
                    countProcess(value)
                }
                else -> notMultipleInput(value)
            }
        } else {
            goingBeyond(value)
        }
    }

    private fun handleHelpButtons(value: BigDecimal) {
        handleLessButton(value)
        handleMoreButton(value)
    }

    private fun handleLessButton(value: BigDecimal) {
        val lessValue = value.roundDownToMultipleOf(investmentStep)
        lessButtonVisibility.set(true)
        setLessValue(lessValue)
    }

    private fun handleMoreButton(value: BigDecimal) {
        val moreValue = investmentStep.plus(value.minus(value.rem(investmentStep)))
        moreButtonVisibility.set(true)
        setMoreValue(moreValue)
    }

    private fun countProcess(value: BigDecimal) {
        val countedFee = value.multiplySafe(investmentFee.divideSafe(BigDecimal(100)))
        setInvestmentFee(countedFee)
        val bhgValue = convertToBGH(value)
        this.token.set(bhgValue.toPlainString().parseToBigDecimalSafe())
        setTotalShares(bhgValue)
        setTotal(value.plus(countedFee))
    }

    private fun hideHelpButtons() {
        lessButtonVisibility.set(false)
        moreButtonVisibility.set(false)
    }

    /**
     * If entered value more maxValue then moreButton set maximum investment amount
     * @see maxValue
     * @sample applyMoreThanSideEffect
     * If entered value les minValue then moreButton set minimum investment amount
     * @see minValue
     * @sample applyLessThanSideEffect
     */
    private fun goingBeyond(value: BigDecimal) {
        when {
            value > userLimit -> applyMoreThanSideEffect()
            value < minValue -> applyLessThanSideEffect()
        }
        showMessage()
    }

    /**
     * Show message if entered value not multiply of the investment step
     */
    private fun notMultipleInput(value: BigDecimal) {
        showMessage()
        setMessageText(appContext.getString(R.string.investment_currency_input_rules_text,
            investmentStep,
            appContext.getString(R.string.eur_text)
        ))
        handleHelpButtons(value)
    }

    private fun applyMoreThanSideEffect() {
        setMessageText(when {
            userLimit >= maxValue -> {
                setLessValue(maxValue)
                appContext.getString(R.string.investment_currency_max_input_rules_text)
            }
            userLimit < maxValue -> {
                setLessValue(userLimit.roundDownToMultipleOf(investmentStep))
                appContext.getString(R.string.maximum_investment_currency_limit_for_the_user)
            }
            else -> appContext.getString(R.string.investment_currency_max_input_rules_text)
        })
        blockPlusButton()
        minusButtonAvailability.set(true)
        lessButtonVisibility.set(true)
        moreButtonVisibility.set(false)
    }

    private fun applyLessThanSideEffect() {
        setMessageText(appContext.getString(R.string.investment_currency_min_input_rules_text))
        blockMinusButton()
        plusButtonAvailability.set(true)
        moreButtonVisibility.set(true)
        lessButtonVisibility.set(false)
        setMoreValue(minValue)
    }

    override fun cleanUp() {
        super.cleanUp()
        euro = null
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        fee.set("")
        hideHelpButtons()
        tokenVisibility.set(true)
        messageVisibility.set(false)
        plusButtonAvailability.set(true)
        minusButtonAvailability.set(false)
    }
}