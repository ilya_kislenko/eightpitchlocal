package app.eightpitch.com.ui.projectfilter.filtersecurity.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.R
import app.eightpitch.com.extensions.inflateView
import app.eightpitch.com.ui.projectfilter.filtersecurity.adapter.FilterButtonSecurityAdapter.PagedItemViewHolder
import app.eightpitch.com.ui.recycleradapters.BindableFilterRecyclerAdapter
import app.eightpitch.com.views.ToggleTextView

class FilterButtonSecurityAdapter : BindableFilterRecyclerAdapter<String, String, PagedItemViewHolder>() {

    /**
     * Listener that represents a click on any project in the list,
     * will pass a [String] when click is performed
     */
    internal lateinit var onItemClicked: (String, Boolean, Int) -> (Unit)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PagedItemViewHolder(
        inflateView(
            parent,
            R.layout.item_view_filter_btn_text
        )
    )

    override fun onBindViewHolder(holder: PagedItemViewHolder, position: Int) {
        val item = getDataItem(position)
        holder.apply {
            customCheckButton.apply {
                tag = item
                textHeader = item.replace("_", " ")
                isActivated(filterDataList.contains(item))
                setDisablingClickListener {
                    onItemClicked.invoke(item, customCheckButton.isActive, position)
                }
            }
        }
    }

    class PagedItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val customCheckButton: ToggleTextView = itemView.findViewById(R.id.customCheckButton)
    }
}