package app.eightpitch.com.ui.dialogs

import android.app.Dialog
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.DialogFragment
import app.eightpitch.com.R
import app.eightpitch.com.extensions.hide
import app.eightpitch.com.views.RoundedCornersButton

class GenericDialogFragment : DialogFragment() {

    private var title: String = ""
    private var message: String = ""
    private var positiveButtonMessage: String = ""
    private var positiveListener: (() -> Unit)? = null
    private var negativeButtonMessage: String = ""
    private var negativeListener: (() -> Unit)? = null
    private var neutralButtonMessage: String = ""
    private var neutralListener: (() -> Unit)? = null

    companion object {

        fun createAnInstance(
            title: String = "",
            message: String = "",
            positiveButtonMessage: String = "",
            positiveListener: (() -> Unit)? = null,
            negativeButtonMessage: String = "",
            negativeListener: (() -> Unit)? = null,
            neutralButtonMessage: String = "",
            neutralListener: (() -> Unit)? = null,
            cancelable: Boolean = true

        ) = GenericDialogFragment().apply {
            this.title = title
            this.message = message
            this.positiveButtonMessage = positiveButtonMessage
            this.positiveListener = positiveListener
            this.negativeButtonMessage = negativeButtonMessage
            this.negativeListener = negativeListener
            this.neutralButtonMessage = neutralButtonMessage
            this.neutralListener = neutralListener
            isCancelable = cancelable
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val alertDialogBuilder = AlertDialog.Builder(requireContext(), R.style.GenericAlertDialog)
        val view = View.inflate(context, R.layout.dialog_generic_layout, null)
        alertDialogBuilder.setView(view)
        val dialog = alertDialogBuilder.create()

        val titleView = view.findViewById<AppCompatTextView>(R.id.titleView)
        val messageView = view.findViewById<AppCompatTextView>(R.id.messageView)
        val okButton = view.findViewById<RoundedCornersButton>(R.id.okButton)
        val cancelButton = view.findViewById<RoundedCornersButton>(R.id.cancelButton)
        val crossButton = view.findViewById<AppCompatImageView>(R.id.crossButton)

        fulfillTheTextView(title, titleView)
        fulfillTheTextView(message, messageView)
        fulfillTheButton(positiveButtonMessage, dialog, true, okButton)
        fulfillTheButton(negativeButtonMessage, dialog, false, cancelButton)
        fulfillCrossButton(dialog, crossButton)

        return dialog
    }

    private fun fulfillTheTextView(
        text: String,
        view: AppCompatTextView
    ) {
        view.apply {
            if (message.isEmpty())
                hide()
            else
                this.text = text
        }
    }

    private fun fulfillTheButton(
        text: String,
        dialog: Dialog,
        isPositiveButton: Boolean,
        button: RoundedCornersButton
    ) {

        button.apply {
            if (text.isEmpty())
                hide()
            else
                this.text = text

            setOnClickListener {
                dialog.dismiss()
                if (isPositiveButton)
                    positiveListener?.invoke()
                else
                    negativeListener?.invoke()
            }
        }
    }

    private fun fulfillCrossButton(
        dialog: Dialog,
        button: AppCompatImageView
    ) {

        button.apply {
            setOnClickListener {
                dialog.dismiss()
                neutralListener?.invoke()
            }
        }
    }
}
