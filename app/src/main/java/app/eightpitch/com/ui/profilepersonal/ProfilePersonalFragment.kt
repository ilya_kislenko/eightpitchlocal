package app.eightpitch.com.ui.profilepersonal

import android.content.Intent
import android.os.Bundle
import android.view.View
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.ui.kyc.KycActivity
import kotlinx.android.synthetic.main.fragment_profile_personal.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class ProfilePersonalFragment : BaseFragment<ProfilePersonalFragmentViewModel>() {

    override fun getLayoutID() = R.layout.fragment_profile_personal

    override fun getVMClass() = ProfilePersonalFragmentViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        additionalToolbarRightButton.apply {
            setImageResource(R.drawable.icon_edit)
            setOnClickListener {
                navigate(R.id.action_profilePersonalFragment_to_editProfileFragment)
            }
        }

        upgradeProfileButton.setOnClickListener {
            context?.let {
                startActivity(Intent(it, KycActivity::class.java))
            }
        }

        emailItemView.setOnClickListener {
            navigate(R.id.action_profilePersonalFragment_to_changeEmailFragment)
        }

        phoneItemView.setOnClickListener {
            navigate(R.id.action_profilePersonalFragment_to_specifyPhoneNumberFragment)
        }

        viewModel.getUserMeResult()
            .observe(viewLifecycleOwner, { result -> handleResult(result) {} })
        viewModel.getUser()
    }
}