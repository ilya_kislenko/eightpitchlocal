package app.eightpitch.com.ui.profilesecurity.changingpassword

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.extensions.handleResult
import kotlinx.android.synthetic.main.fragment_set_new_password.*

class SetNewPasswordFragment : BaseFragment<SetNewPasswordViewModel>() {

    companion object {

        internal const val INTERACTION_ID = "INTERACTION_ID"
        internal const val USER_EXTERNAL_ID = "USER_EXTERNAL_ID"

        fun buildWithAnArguments(
            interactionId: String,
            userExternalId: String
        ) = Bundle().apply {
            putString(INTERACTION_ID, interactionId)
            putString(USER_EXTERNAL_ID, userExternalId)
        }
    }

    private val interactionId: String
        get() = arguments?.getString(INTERACTION_ID) ?: ""

    private val userExternalId: String
        get() = arguments?.getString(USER_EXTERNAL_ID) ?: ""

    override fun getLayoutID() = R.layout.fragment_set_new_password

    override fun getVMClass() = SetNewPasswordViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindLoadingIndicator()

        changePasswordButton.setOnClickListener {
            viewModel.validatePasswords(interactionId, userExternalId)
        }

        viewModel.getChangingPasswordResult().observe(viewLifecycleOwner, Observer { result ->
            handleResult(result) {
                navigate(R.id.action_setNewPasswordFragment_to_successPasswordChangedScreen)
            }
        })
    }
}