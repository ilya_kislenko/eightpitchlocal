package app.eightpitch.com.ui.webid

import android.os.Bundle
import android.view.*
import app.eightpitch.com.R
import app.eightpitch.com.base.BackPressureFragment
import app.eightpitch.com.dagger.Injector
import app.eightpitch.com.data.dto.user.User.CREATOR.INSTITUTIONAL_INVESTOR
import app.eightpitch.com.data.dto.user.User.CREATOR.PRIVATE_INVESTOR
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.webview.DefaultWebViewFragment
import app.eightpitch.com.utils.Constants.HELP_INITIATOR_URL
import app.eightpitch.com.utils.Constants.HELP_INVESTORS_URL
import kotlinx.android.synthetic.main.fragment_failure_identified.*

class FailureScreenFragment : BackPressureFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_failure_identified, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        needHelpButton.setOnClickListener {
            val helpLink = when (Injector.getAppComponent().providePreferencesReader().getUserRole()) {
                INSTITUTIONAL_INVESTOR, PRIVATE_INVESTOR -> HELP_INVESTORS_URL
                else -> HELP_INITIATOR_URL
            }
            navigate(
                R.id.action_failureScreenFragment_to_defaultWebViewFragment,
                DefaultWebViewFragment.buildWithAnArguments(stringLinkUrl = helpLink, withBackPressure = true)
            )
        }

        crossButton.setOnClickListener { moveBackward() }
        registerOnBackPressedListener { moveBackward() }
    }
}
