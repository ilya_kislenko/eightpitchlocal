package app.eightpitch.com.ui.investment

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import app.eightpitch.com.data.dto.investment.ProjectInvestmentInfoResponse
import app.eightpitch.com.utils.SingleActionLiveData

class CurrencyInputAdapter(
    fragment: Fragment,
    projectInvestmentInfo: ProjectInvestmentInfoResponse,
    currencyInputValidatorObserver: SingleActionLiveData<Boolean>
) : FragmentStateAdapter(fragment) {

    private val fragments = arrayOf(
        InputEuroFragment.newInstance(InputEuroFragment.buildWithAnArguments(projectInvestmentInfo), currencyInputValidatorObserver),
        InputBghFragment.newInstance(InputBghFragment.buildWithAnArguments(projectInvestmentInfo), currencyInputValidatorObserver)
    )

    override fun getItemCount() = fragments.size

    override fun createFragment(position: Int) = fragments[position]
}