package app.eightpitch.com.ui.kyc.financial

import android.os.Bundle
import android.view.*
import app.eightpitch.com.R
import app.eightpitch.com.ui.dialogs.DefaultBottomDialog

class KycIbanInfoDialog : DefaultBottomDialog() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.dialog_iban_info, container, false)
}