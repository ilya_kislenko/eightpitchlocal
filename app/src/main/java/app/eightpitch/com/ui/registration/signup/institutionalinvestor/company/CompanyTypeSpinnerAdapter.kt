package app.eightpitch.com.ui.registration.signup.institutionalinvestor.company

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import app.eightpitch.com.R
import app.eightpitch.com.views.interfaces.HintAbleSpinnerAdapter
import kotlinx.android.synthetic.main.item_view_company_type.view.*
import kotlinx.android.synthetic.main.item_view_dropdown_company_type.view.*

class CompanyTypeSpinnerAdapter(
    context: Context,
    companyTypes: List<String>,
) : HintAbleSpinnerAdapter(context, companyTypes) {

    override fun getView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, recycledView, parent)
    }

    override fun getDropDownView(position: Int, recycledView: View?, parent: ViewGroup): View {
        val companyType = getItem(position)
        val view = getView(parent, recycledView, R.layout.item_view_dropdown_company_type)
        view?.apply { genderDropDownTitle.text = companyType }
        return view
    }

    private fun createView(position: Int, recycledView: View?, parent: ViewGroup): View {
        val view = getView(parent, recycledView, R.layout.item_view_company_type)
        view?.apply {
            if (position == getHintPosition()) {
                genderTitle.text = ""
            } else {
                val companyType = getItem(position)
                genderTitle.text = companyType
                genderTitle.hint = ""
            }
        }
        return view
    }

    private fun getView(
        parent: ViewGroup,
        recycledView: View?,
        @LayoutRes idRes: Int,
    ) = recycledView ?: LayoutInflater.from(context).inflate(
        idRes,
        parent,
        false
    )

    override fun getHintPosition(): Int {
        val count = count
        return if (count > 0) count + 1 else count
    }

    override fun getHint(): String = try {
        getItem(getHintPosition()).orEmpty()
    } catch (throwable: Throwable) {
        ""
    }
}