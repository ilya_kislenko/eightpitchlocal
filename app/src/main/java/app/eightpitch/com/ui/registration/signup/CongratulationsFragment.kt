package app.eightpitch.com.ui.registration.signup

import android.os.Bundle
import android.view.*
import app.eightpitch.com.R
import app.eightpitch.com.base.BackPressureFragment
import app.eightpitch.com.data.dto.user.User.CREATOR.INSTITUTIONAL_INVESTOR
import app.eightpitch.com.data.dto.user.User.CREATOR.PRIVATE_INVESTOR
import app.eightpitch.com.extensions.setDisablingClickListener
import app.eightpitch.com.ui.authorized.AuthorizedActivity
import app.eightpitch.com.ui.kyc.KycActivity
import app.eightpitch.com.utils.IntentsController
import kotlinx.android.synthetic.main.fragment_congratulations_screen.*

class CongratulationsFragment : BackPressureFragment() {

    companion object {
        internal const val USER_KEY_CODE = "USER_KEY_CODE"

        fun buildWithAnArguments(
            userGroup: String
        ) = Bundle().apply {
            putString(USER_KEY_CODE, userGroup)
        }
    }

    private val userGroup: String
        get() = arguments?.getString(USER_KEY_CODE) ?: ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View =
        inflater.inflate(R.layout.fragment_congratulations_screen, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setTextField()

        registerOnBackPressedListener {
            startKycActivity()
        }

        crossButton.setDisablingClickListener {
            startKycActivity()
        }

        getApprovalButton.setDisablingClickListener {
            startKycActivity()
        }

        browseProjectButton.setOnClickListener {
            context?.let {
                IntentsController.startActivityWithANewStack(it, AuthorizedActivity::class.java)
            }
        }
    }

    private fun startKycActivity() {
        context?.let {
            IntentsController.startActivityWithANewStack(it, KycActivity::class.java)
        }
    }

    private fun setTextField() {
        val additionalText: String = when (userGroup) {
            PRIVATE_INVESTOR -> getString(R.string.now_you_have_an_private_investor_account_text)
            INSTITUTIONAL_INVESTOR -> getString(R.string.now_you_have_an_institutional_investor_account_text)
            else -> ""
        }
        subHeaderTextView.text = additionalText
    }
}
