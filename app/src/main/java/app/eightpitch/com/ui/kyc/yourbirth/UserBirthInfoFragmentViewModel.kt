package app.eightpitch.com.ui.kyc.yourbirth

import android.content.Context
import androidx.databinding.ObservableField
import androidx.lifecycle.*
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.webid.WebIdPersonalInfo
import app.eightpitch.com.data.dto.webid.WebIdUserRequestBody
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.data.transmit.ResultStatus
import app.eightpitch.com.extensions.*
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.utils.DateUtils.formatByPattern
import app.eightpitch.com.utils.RegexPatterns.PATTERN_VALID_BIRTH_PLACES
import app.eightpitch.com.utils.*
import app.eightpitch.com.utils.Constants.DD_p_MM_p_YYYY
import app.eightpitch.com.utils.Constants.YYYY_MM_DD
import app.eightpitch.com.utils.DateUtils.parseByFormatWithoutMMSS
import app.eightpitch.com.views.interfaces.ShortFieldValidator
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import javax.inject.Inject

class UserBirthInfoFragmentViewModel @Inject constructor(
    private val appContext: Context,
    private val userModel: UserModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val title = appContext.getString(R.string.kyc_two_seven)

    var birthDateMillis: Long = 0

    val dateOfBirthField = ObservableField("")
    val placeOfBirthField = ObservableField("")

    val birthDateErrorHolder = SingleActionObservableString()

    private var birthPlace: String = ""
    val birthPlaceErrorHolder = SingleActionObservableString()

    val birthPlaceWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            birthPlace = sequence.toString()
        }
    }

    internal fun addBirthInfoToBody(webIdUserRequestBody: WebIdUserRequestBody): WebIdUserRequestBody =
        webIdUserRequestBody.copy(
            _dateOfBirth = birthDateMillis.formatByPattern(),
            webIdPersonalInfo = WebIdPersonalInfo(placeOfBirth = birthPlace)
        )

    fun validateInput(): Boolean {

        val birthDateError = when {
            birthDateMillis == 0L -> getErrorEmptyString(appContext)
            YearsValidator.validateYearsOld(birthDateMillis) -> appContext.getString(R.string.not_valid_years_old)
            else -> ""
        }
        if (birthDateError.isNotEmpty())
            birthDateErrorHolder.set(birthDateError)

        val birthPlaceError = when {
            birthPlace.isEmpty() -> getErrorEmptyString(appContext)
            !birthPlace.matchPattern(PATTERN_VALID_BIRTH_PLACES) -> appContext.getString(
                R.string.not_valid_birth_place_letter
            )
            else -> ""
        }
        if (birthPlaceError.isNotEmpty())
            birthPlaceErrorHolder.set(birthPlaceError)

        return birthDateError.isEmpty() && birthPlaceError.isEmpty()
    }

    //TODO Refactor this class by using a java 8 API (java.time.*) and move to DateUtils
    internal fun setBirthDate(date: LocalDate) {
        birthDateMillis = try {
            date.atStartOfDay(ZoneId.of("UTC")).toInstant().toEpochMilli()
        } catch (parseException: Exception) {
            System.currentTimeMillis()
        }
        val formatter = DateTimeFormatter.ofPattern(DD_p_MM_p_YYYY)
        val formattedString: String = date.format(formatter)
        dateOfBirthField.set(formattedString)
    }

    private val userMeResult = SingleActionLiveData<Result<Empty>>()
    internal fun getUserMeResult(): LiveData<Result<Empty>> = userMeResult

    private fun getUser() {
        viewModelScope.launch {
            val result = asyncLoading {
                userModel.getUserData()
            }

            if (result.status == ResultStatus.SUCCESS) {
                birthPlace = result.result?.placeOfBirth.orEmpty()

                val userBirthDate = result.result?.dateOfBirth.orEmpty()

                if (userBirthDate.isNotEmpty()) {
                    birthDateMillis = userBirthDate.parseByFormatWithoutMMSS(pattern = YYYY_MM_DD)
                    dateOfBirthField.set(birthDateMillis.formatByPattern(DD_p_MM_p_YYYY))
                }

                if (birthPlace.isNotEmpty()) {
                    placeOfBirthField.set(birthPlace)
                }
            }
            userMeResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    internal fun loadUserOrRestoreState() {
        if (birthDateMillis == 0L || birthPlace.isEmpty())
            getUser()
        else {
            dateOfBirthField.set(birthDateMillis.formatByPattern(pattern = DD_p_MM_p_YYYY))
            placeOfBirthField.set(birthPlace)
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        birthPlace = ""
        birthDateMillis = 0L
        dateOfBirthField.set("")
        placeOfBirthField.set("")
    }
}