package app.eightpitch.com.ui.registration.login

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.base.BaseModel.AbsentTokensException
import app.eightpitch.com.base.baseviewmodels.codeverification.DefaultCodeValidationFragment
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO.CREATOR.LOGIN
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO.CREATOR.TWO_AUTH_CODE_VERIFICATION
import app.eightpitch.com.data.dto.signup.CommonInvestorRequestBody
import app.eightpitch.com.data.dto.twofactor.TwoFactorVerifyCodeResponse.Companion.PHONE_VERIFICATION
import app.eightpitch.com.data.dto.twofactor.TwoFactorVerifyCodeResponse.Companion.TWO_FACTOR_AUTH_VERIFICATION
import app.eightpitch.com.data.dto.user.CountryCode
import app.eightpitch.com.extensions.appendPrefixToPhone
import app.eightpitch.com.extensions.handleResultWithError
import app.eightpitch.com.ui.GenderSpinnerAdapter
import app.eightpitch.com.ui.authorized.AuthorizedActivity
import app.eightpitch.com.utils.IntentsController
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : BaseFragment<LoginViewModel>() {

    override fun getLayoutID() = R.layout.fragment_login

    override fun getVMClass() = LoginViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindLoadingIndicator()
        setupCountryCodeSpinner()

        nextButton.setOnClickListener {
            viewModel.validateInput()
        }

        forgotPasswordButton.setOnClickListener {
            navigate(R.id.action_loginFragment_to_enterEmailAccountFragment)
        }

        viewModel.getLoginResult().observe(viewLifecycleOwner, {
            handleResultWithError(it, AbsentTokensException::class.java, { error ->
                if (error is AbsentTokensException) {
                    when {
                        error.arrayStatusTwoAuth.contains(PHONE_VERIFICATION) -> {
                            moveForwardToSMSConfirmation(error.interactionId)
                        }
                        error.arrayStatusTwoAuth.contains(TWO_FACTOR_AUTH_VERIFICATION) -> {
                            moveForwardToGAConfirmation(error.interactionId)
                        }
                        else -> {
                            moveForwardToSMSConfirmation(error.interactionId)
                        }
                    }
                }
            }, { loginResponseBody ->
                when {
                    loginResponseBody.verificationTypes.contains(PHONE_VERIFICATION) -> {
                        moveForwardToSMSConfirmation(loginResponseBody.interactionId)
                    }
                    loginResponseBody.verificationTypes.contains(TWO_FACTOR_AUTH_VERIFICATION) -> {
                        moveForwardToGAConfirmation(loginResponseBody.interactionId)
                    }
                    else -> {
                        context?.let { appContext ->
                            IntentsController.startActivityWithANewStack(
                                appContext,
                                AuthorizedActivity::class.java
                            )
                        }
                    }
                }
            })
        })
    }

    private fun moveForwardToSMSConfirmation(interactionId: String) {
        val arguments = DefaultCodeValidationFragment.buildWithAnArguments(
            validationScreenInfo = createValidationScreenInfo(),
            partialUser = CommonInvestorRequestBody(
                interactionId = interactionId,
                phoneNumber = viewModel.userPhoneNumber.appendPrefixToPhone(viewModel.countryCode)
            )
        )
        navigate(R.id.action_loginFragment_to_digitCodeValidationFragment, arguments)
    }

    private fun moveForwardToGAConfirmation(interactionId: String) {
        val arguments = DefaultCodeValidationFragment.buildWithAnArguments(
            validationScreenInfo = createGAValidationScreenInfo(),
            partialUser = CommonInvestorRequestBody(
                interactionId = interactionId,
                phoneNumber = viewModel.userPhoneNumber.appendPrefixToPhone(viewModel.countryCode)
            )
        )
        navigate(R.id.action_loginFragment_to_defaultCodeValidationFragment, arguments)
    }

    private fun createValidationScreenInfo() = ValidationScreenInfoDTO(
        verificationType = LOGIN,
        validationButtonName = getString(R.string.log_in),
        header = getString(R.string.account_confirmation_text),
        toolbarTitle = getString(R.string.log_in_title_text),
        subTitleMessage = getString(R.string.we_have_sent_sms_to_login_text)
    )

    private fun createGAValidationScreenInfo() = ValidationScreenInfoDTO(
        verificationType = TWO_AUTH_CODE_VERIFICATION,
        validationButtonName = getString(R.string.log_in),
        header = getString(R.string.enter_code_from_ga),
        toolbarTitle = getString(R.string.log_in_title_text),
        subTitleMessage = getString(R.string.enter_code_from_ga_description)
    )

    private fun setupCountryCodeSpinner() {
        codeSpinner.apply {
            val spinnerData = CountryCode.getPhoneCodes()
            val codeAdapter = GenderSpinnerAdapter(context, spinnerData)
            setAdapter(codeAdapter)
            setSelection(0)
            setSelectedListener({
                viewModel.setCountryCode(it)
            }, {})

        }
    }

    private inner class SpinnerSelectionListener : AdapterView.OnItemSelectedListener {

        override fun onNothingSelected(parent: AdapterView<*>?) {
            //Do nothing.
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            viewModel.setCountryCode(position)
        }
    }
}