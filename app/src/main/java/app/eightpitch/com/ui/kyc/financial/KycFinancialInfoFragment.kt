package app.eightpitch.com.ui.kyc.financial

import android.os.Bundle
import android.view.View
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.webid.WebIdUserRequestBody
import app.eightpitch.com.ui.kyc.summaruinformation.KycSummaryInfoFragment
import kotlinx.android.synthetic.main.fragment_kyc_finantial_info.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class KycFinancialInfoFragment : BaseFragment<KycFinancialInfoFragmentViewModel>() {

    companion object {

        internal const val KYC_BODY = "KYC_BODY"

        fun buildWithAnArguments(
            webIdUserRequestBody: WebIdUserRequestBody
        ) = Bundle().apply {
            putParcelable(KYC_BODY, webIdUserRequestBody)
        }
    }

    override fun getLayoutID() = R.layout.fragment_kyc_finantial_info

    override fun getVMClass() = KycFinancialInfoFragmentViewModel::class.java

    private val webIdUserRequestBody: WebIdUserRequestBody
        get() = arguments?.getParcelable(KYC_BODY) ?: WebIdUserRequestBody()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        setupUI()

        checkboxSameLike.run {
            setOnCheckedChangeListener { _, isChecked ->
                viewModel.setAccountOwner(if (isChecked) webIdUserRequestBody.fullName else "")
            }
        }

        buttonNext.setOnClickListener {
            if (viewModel.validateInput()) {
                val kycArguments = viewModel.addFinancialInfoToKycBody(webIdUserRequestBody)
                navigate(
                    R.id.action_kycFinancialInfoFragment_to_kycSummaryInfoFragment,
                    KycSummaryInfoFragment.buildWithAnArguments(kycArguments)
                )
            }
        }
    }

    private fun setupUI() {

        viewModel.setAccountOwner(webIdUserRequestBody.fullName)

        additionalToolbarRightButton.apply {
            setImageDrawable(context.getDrawable(R.drawable.icon_info))
            setOnClickListener {
                KycIbanInfoDialog().show(
                    KycIbanInfoDialog::class.java.name,
                    childFragmentManager
                )
            }
        }
    }
}