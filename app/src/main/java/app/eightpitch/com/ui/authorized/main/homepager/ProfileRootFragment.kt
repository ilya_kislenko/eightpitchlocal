package app.eightpitch.com.ui.authorized.main.homepager

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment
import app.eightpitch.com.R
import app.eightpitch.com.base.rootbackhandlers.RootNavGraphHandleAbleFragment
import app.eightpitch.com.extensions.isAliveAndAvailable
import app.eightpitch.com.extensions.moveToStartDestination

class ProfileRootFragment : RootNavGraphHandleAbleFragment(), NavigationApplier {

    override fun getNavigationHostFragmentId() = R.id.navigation_profile_fragment_host

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        return inflater.inflate(R.layout.fragment_root_profile_layout, container, false)
    }

    override fun applyNavigationState(navigationId: Int) {
        if (this.isAliveAndAvailable()) {
            val navHostFragment =
                childFragmentManager.findFragmentById(getNavigationHostFragmentId()) as NavHostFragment
            if (navHostFragment.navController.currentDestination?.id != navigationId) {
                navHostFragment.navController.navigate(navigationId)
            }
        }
    }

    override fun popUpToStartDestination() {
        moveToStartDestination(getNavigationHostFragmentId())
    }
}