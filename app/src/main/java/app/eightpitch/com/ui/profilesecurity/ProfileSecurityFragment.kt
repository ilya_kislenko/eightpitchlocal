package app.eightpitch.com.ui.profilesecurity

import android.os.Bundle
import android.view.View
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.profilesecurity.changingpassword.EnterOldPasswordFragment
import app.eightpitch.com.ui.twofactor.choosetwoauthmethod.ChooseTwoAuthMethodFragment
import app.eightpitch.com.ui.twofactor.choosetwoauthmethod.ChooseTwoAuthMethodFragment.Companion.NAVIGATE_FROM_SECURITY
import kotlinx.android.synthetic.main.fragment_security.*

class ProfileSecurityFragment : BaseFragment<ProfileSecurityFragmentViewModel>() {

    override fun getLayoutID() = R.layout.fragment_security

    override fun getVMClass() = ProfileSecurityFragmentViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindLoadingIndicator()

        twoFactorSwitch.setOnClickListener {
            navigate(R.id.action_profileSecurityFragment_to_chooseTwoAuthMethodFragment,
                ChooseTwoAuthMethodFragment.buildWithAnArguments(NAVIGATE_FROM_SECURITY))
        }

        changePasswordButton.setDisablingClickListener {
            val externalId = viewModel.externalId
            if (externalId.isNotEmpty()) {
                val args = EnterOldPasswordFragment.buildWithAnArguments(externalId)
                navigate(R.id.action_profileSecurityFragment_to_enterOldPasswordFragment, args)
            }
        }

        loginSettingsButton.setOnClickListener {
            navigate(R.id.action_profileSecurityFragment_to_profileQuickLoginFragment)
        }

        viewModel.run {
            getUserMeResult().observe(viewLifecycleOwner, { result -> handleResult(result) {} })
            getUser()
        }
    }
}
