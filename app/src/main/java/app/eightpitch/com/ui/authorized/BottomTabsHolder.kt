package app.eightpitch.com.ui.authorized

import androidx.viewpager2.widget.ViewPager2
import app.eightpitch.com.ui.authorized.main.MainScreenTab
import com.google.android.material.tabs.TabLayout

/**
 * An interface for the root UI element that has
 * a [TabLayout] and [ViewPager2] within, and gives
 * an ability to operate with the tabs.
 */
interface BottomTabsHolder {
    /**
     * @param position it's a tab's position to switch on.
     */
    fun switchTabTo(position: Int)


    /**
     * @param position it's a tab's position to switch on.
     * @param navigationId it's Id with which the navigation will be
     */
    fun switchTabWith(position: Int, navigationId:Int)

    /**
     * Method that returns a recent selected tab.
     */
    fun getRecentSelectedTab(): MainScreenTab

    /**
     * Method that should be called whenever we need to sync
     * bottom tabs `unread` indicators.
     */
    fun syncBottomTabsIndicators()
}