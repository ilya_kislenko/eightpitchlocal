package app.eightpitch.com.ui.questionnaire

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody.CREATOR.NOT_PROVIDED
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody.CREATOR.NULL
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody.CREATOR.TRUE
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.questionnaire.knowledge.QuestionnaireSecondFragment
import kotlinx.android.synthetic.main.fragment_questionnaire_three_answer.*

class QuestionnaireFirstFragment : BaseFragment<QuestionnaireViewModel>() {

    companion object {
        const val QUESTIONNAIRE_KEY_CODE = "QUESTIONNAIRE_KEY_CODE"
    }

    override fun getVMClass() = QuestionnaireViewModel::class.java

    override fun getLayoutID() = R.layout.fragment_questionnaire_three_answer

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        answersGroup.check(R.id.secondOptionTitle)

        backButton.apply {
            text = getString(R.string.cancel_text)
            setCompoundDrawablesRelativeWithIntrinsicBounds(resources.getDrawable(R.drawable.icon_cross, null),
                null,
                null,
                null)
            setDisablingClickListener {
                moveBackward()
            }
        }

        viewModel.setupLayout(
            2,
            getString(R.string.do_you_already_have_any_experience_with_any_asset_classes_text),
            arrayListOf(
                getString(R.string.yes_text),
                getString(R.string.no_text),
                getString(R.string.i_would_like_to_not_provide_details_text)
            )
        )

        viewModel.getQuestionnaireResult().observe(viewLifecycleOwner, Observer {
            handleResult(it) {
                navigate(R.id.action_questionnaireFirstFragment_to_questionnaireFinishFragment)
            }
        })

        nextButton.setDisablingClickListener {
            val answer = viewModel.getCheckedAnswer(
                listOf(TRUE, NULL, NOT_PROVIDED), answersGroup
            )
            when (answer) {
                TRUE -> navigate(R.id.action_questionnaireFirstFragment_to_questionnaireSecondFragment,
                    QuestionnaireSecondFragment.buildWithAnArguments(
                        QuestionnaireRequestBody(experience = TRUE))
                )
                NULL -> viewModel.sendQuestionnaire(QuestionnaireRequestBody())
                NOT_PROVIDED -> viewModel.sendQuestionnaire(
                    QuestionnaireRequestBody(experience = NOT_PROVIDED, knowledge = listOf(NOT_PROVIDED))
                )
            }
        }
    }
}