package app.eightpitch.com.ui.webid

import android.app.Activity
import android.content.Context
import android.view.View
import androidx.databinding.*
import androidx.lifecycle.*
import app.eightpitch.com.BuildConfig
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.webid.*
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.extensions.*
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.utils.*
import app.eightpitch.com.utils.webid.WebIdController
import app.eightpitch.com.views.interfaces.ShortFieldValidator
import de.webid_solutions.mobile_app.sdk.domain.*
import kotlinx.coroutines.launch
import org.webrtc.SurfaceViewRenderer
import kotlin.properties.Delegates

class WebIdIdentifyViewModel(
    private val appContext: Context,
    private val webIdController: WebIdController,
    private val userModel: UserModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val toolbarTitle = appContext.getString(R.string.web_id_title)

    /**
     * Binding for buttons availability. (startCall, stopCall, apply TAN code)
     * @see  revertValue
     */
    val stopCallAvailability = ObservableBoolean()
    val startCallAvailability = ObservableBoolean(true)
    val applyButtonAvailability = ObservableBoolean()

    /**
     * Binding for the TAN code and  waiting time message visibility. (if one VISIBLE then other GONE)
     * @see revertValue
     */
    val tanVisibility = ObservableBoolean()
    val userActionIdVisibility = ObservableBoolean()
    val userPositionVisibility = ObservableBoolean(true)

    /**
     * Observable binding field for correct enter 6-digit TAN code.
     * @see inputChanged
     */
    var tanCode by Delegates.observable("") { _, _, newValue ->
        inputChanged(newValue)
    }

    val tanCodeErrorHolder = SingleActionObservableString()

    val tanCodeTextWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            tanCode = sequence.toString()
        }
    }

    val userPositionInQueueMessage =
        ObservableField(appContext.getString(R.string.please_wait_for_a_while_you_are_connecting_to_our_agent_text))

    private fun inputChanged(input: String) {
        applyButtonAvailability.set(input.length == 6)
    }

    /**
     * @sample ActionIdResponseBody is backend response with actionId which mandatory for WebId.
     * Nullable because of mandatory try/catch in the WebIdController.
     * This concerns all objects below getting by asyncLoading.
     */
    private val actionResult = SingleActionLiveData<Result<ActionIdResponseBody>>()
    fun getActionResult(): LiveData<Result<ActionIdResponseBody>> = actionResult

    /**
     * Requesting actionId from backend to send and verify to WebId.
     */
    internal fun getActionId() {
        asyncWith(actionResult) {
            val user = userModel.getUserData()
            userModel.getActionId(
                WebIdUserRequestBody(
                    address = user.userAddress,
                    _dateOfBirth = user.dateOfBirth,
                    firstName = user.firstName,
                    lastName = user.lastName,
                    prefix = user.prefix,
                    webIdPersonalInfo = WebIdPersonalInfo(
                        accountOwner = user.accountOwner,
                        IBAN = user.iban,
                        nationality = user.nationality,
                        placeOfBirth = user.placeOfBirth,
                        isPoliticallyExposedPerson = user.politicallyExposedPerson,
                        isUsTaxLiability = user.usTaxLiability
                    )
                )
            )
        }
    }

    /**
     * @sample VerifyActionIdResult WebId response with verification action id.
     */
    private val verificationIdResult = SingleActionLiveData<Result<VerifyActionIdResult?>>()
    fun getVerificationIdResult(): LiveData<Result<VerifyActionIdResult?>> = verificationIdResult

    /**
     * Verify action id in WebId. If isVerified then we can createCall.
     */
    internal fun verifyActionId(actionId: String) {
        asyncWith(verificationIdResult) {
            webIdController.verifyActionId(actionId)
        }
    }

    /**
     * @sample VerifyActionIdResult WebId response with entered TAN code by user.
     */
    private val verificationTANResult = SingleActionLiveData<Result<VerifyTanResult?>>()
    fun getVerificationTANResult(): LiveData<Result<VerifyTanResult?>> = verificationTANResult

    internal fun verifyTan() {
        asyncWith(verificationTANResult) {
            webIdController.verifyTan(tanCode)
        }
    }

    /**
     * @sample GetUserActionStatusResult WebId response which return status of interview.
     */
    private val userActionStatusResult = SingleActionLiveData<Result<GetUserActionStatusResult?>>()
    fun getUserActionStatusResult(): LiveData<Result<GetUserActionStatusResult?>> = userActionStatusResult

    /**
     * WebId service might not notify backend that call is finished for any reason,
     * so we report to backend about finishing call.
     *
     * Might be called until we get the status. (one per 8 sec)
     * Return result of interview. We need handle success and unsuccessful status.
     */
    internal fun getUserActionStatus() {
        asyncWith(userActionStatusResult) {
            userModel.reportFinishedCall()
            webIdController.getUserActionStatus()
        }
    }

    //WebId methods
    /**
     * Creating the WebId main factory
     */
    internal fun setupWebIdArguments() {
        webIdController.setupWebIdArguments()
    }

    /**
     * Creating call
     * @param pair Pair of localeVideoView and remoteVideoView. (localeVideoView for user camera)
     */
    internal fun createCall(
        appContext: Activity,
        pair: Pair<SurfaceViewRenderer, SurfaceViewRenderer>
    ) {
        pair.apply {
            first.release()
            second.release()
        }
        if (webIdController.createCall(appContext, pair) != null) {
            (stopCallAvailability to startCallAvailability).revertValues()
            userActionIdVisibility.revertValue()
        }
    }

    /**
     * Blocking stopCall button, unblocking startCall button and stopping call.
     */
    internal fun stopCall() {
        (startCallAvailability to stopCallAvailability).revertValues()
        userActionIdVisibility.revertValue()
        webIdController.stopCall()
        setDefaultUIState()
    }

    internal fun switchCamera() {
        webIdController.switchCamera()
    }
    //endregion

    /**
     * Live data  for handling unexpected situations in WebId
     * For example, has no one agent on the call line
     */
    private val anyMessageResult = SingleActionLiveData<String>()
    fun getAnyMessageResult(): LiveData<String> = anyMessageResult

    private val showCameraResult = MutableLiveData<Int>()
    fun getShowCameraResult(): MutableLiveData<Int> = showCameraResult

    internal fun getWebIDErrorsContainer(): LiveData<Result<String>> = webIdController.getWebIDErrorsContainer()

    internal fun setupCallbacks() {
        webIdController.apply {
            callWithAgentStartedCallback = {
                if (BuildConfig.BUILD_TYPE != "release")
                    (tanVisibility to userPositionVisibility).revertValues()
                showCameraResult.postValue(View.VISIBLE)
            }
            userPositionInQueueCallback = {
                if (BuildConfig.BUILD_TYPE != "release") {
                    (tanVisibility to userPositionVisibility).revertValues()
                    userPositionInQueueMessage.set(appContext.getString(R.string.user_position_message_pattern,
                        it.positionInQueue, it.averageWaitingTimeOfLastHourInSeconds))
                }
            }
            callEndCallback = { stopCall() }
            webIdEdgeCasesHandler = { message ->
                anyMessageResult.postAction(message)
            }
            //remove
            if (BuildConfig.BUILD_TYPE == "release")
                showTANCallback = {
                    tanVisibility.set(true)
                    userPositionVisibility.set(false)
                }
        }
    }

    internal fun performCallFinished() {
        stopCall()
        getUserActionStatus()
    }

    private fun <T> asyncWith(
        liveData: SingleActionLiveData<Result<T>>,
        operation: () -> T
    ) {
        viewModelScope.launch {
            val result = asyncLoading {
                operation()
            }
            liveData.postAction(result)
        }
    }

    private fun setDefaultUIState() {
        //tanVisibility.set(false)
        //userPositionVisibility.set(true)
        showCameraResult.postValue(View.GONE)
        userPositionInQueueMessage.set(appContext.getString(R.string.call_is_stopped_you_can_call_again_text))
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        stopCall()
        webIdController.releaseSDK()
        tanCode = ""
        tanCodeErrorHolder.set("")
        tanVisibility.set(false)
        applyButtonAvailability.set(false)
        stopCallAvailability.set(false)
        startCallAvailability.set(true)
        userPositionVisibility.set(true)
        userPositionInQueueMessage.set(appContext.getString(R.string.please_wait_for_a_while_you_are_connecting_to_our_agent_text))
    }
}