package app.eightpitch.com.ui.projectfilter.filter.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.R
import app.eightpitch.com.extensions.inflateView
import app.eightpitch.com.ui.projectfilter.filter.adapters.FilterStatusesAdapter.PagedItemViewHolder
import app.eightpitch.com.ui.recycleradapters.BindableRecyclerAdapter
import app.eightpitch.com.views.ToggleTextView

class FilterStatusesAdapter : BindableRecyclerAdapter<String, PagedItemViewHolder>() {

    /**
     * Listener that represents a click on any project in the list,
     * will pass a [String] when click is performed
     */
    internal lateinit var onItemClicked: (String, Boolean, Int) -> (Unit)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PagedItemViewHolder(
        inflateView(
            parent,
            R.layout.item_view_filter_btn
        )
    )

    override fun onBindViewHolder(holder: PagedItemViewHolder, position: Int) {
        val item = getDataItem(position)
        holder.apply {
            customCheckButton.apply {
                tag = item
                setText(item.replace("_", " "))
                setIconDisablingClickListener {
                    onItemClicked.invoke(item, customCheckButton.isActive, position)
                }
            }
        }
    }

    class PagedItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val customCheckButton: ToggleTextView = itemView.findViewById(R.id.customCheckButton)
    }
}