package app.eightpitch.com.ui.twofactor.choosetwoauthmethod

import android.content.Context
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.twofactor.TwoFactorQrCodeResponse
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.data.dto.user.User.CREATOR.SMS
import app.eightpitch.com.data.dto.user.User.CREATOR.TOTP
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.data.transmit.ResultStatus.SUCCESS
import app.eightpitch.com.extensions.wrapWithAnEmptyResult
import app.eightpitch.com.models.AuthModel
import app.eightpitch.com.models.SMSModel
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.ui.reasonselector.InteractionIdToPhoneNumber
import app.eightpitch.com.utils.Empty
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.coroutines.launch
import javax.inject.Inject

class ChooseTwoAuthMethodFragmentViewModel @Inject constructor(
    private val appContext: Context,
    private val userModel: UserModel,
    private val authModel: AuthModel,
    private val smsModel: SMSModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val title = appContext.getString(R.string.choose_two_auth_title)

    val twoFactorSmsSwitchEnabledField = ObservableField(false)
    val twoFactorAppSwitchEnabledField = ObservableField(false)

    private val userMeResult = SingleActionLiveData<Result<Empty>>()
    internal fun getUserMeResult(): LiveData<Result<Empty>> = userMeResult

    internal fun getUser() {
        viewModelScope.launch {

            val result = asyncLoading {
                userModel.getUserData()
            }

            if (result.status == SUCCESS) {
                setTwoFactorFields(result.result!!)
            }

            userMeResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    private val qrCodeResult = SingleActionLiveData<Result<TwoFactorQrCodeResponse>>()
    internal fun getQrCodeResult(): LiveData<Result<TwoFactorQrCodeResponse>> = qrCodeResult

    internal fun getQrCode() {
        viewModelScope.launch {

            val result = asyncLoading {
                authModel.twoFactorGetQrCode()
            }

            qrCodeResult.postAction(result)
        }
    }

    private val smsResult = SingleActionLiveData<Result<InteractionIdToPhoneNumber>>()
    internal fun getSMSResult(): LiveData<Result<InteractionIdToPhoneNumber>> = smsResult

    internal fun requestSMSCode() {
        viewModelScope.launch {

            val result = asyncLoading {
                val user = userModel.getUserData()
                val response = smsModel.requireSMSCodeByPhoneNumber(user.phoneNumber)
                response.interactionId to user.phoneNumber
            }

            smsResult.postAction(result)
        }
    }

    private fun setTwoFactorFields(user: User) {
        if (user.isTwoFactorAuthenticationEnabled) {
            twoFactorSmsSwitchEnabledField.set(user.twoFactorAuthenticationType == SMS)
            twoFactorAppSwitchEnabledField.set(user.twoFactorAuthenticationType == TOTP)
        } else {
            twoFactorSmsSwitchEnabledField.set(false)
            twoFactorAppSwitchEnabledField.set(false)
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        twoFactorSmsSwitchEnabledField.set(false)
        twoFactorAppSwitchEnabledField.set(false)
    }
}
