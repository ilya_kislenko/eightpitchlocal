package app.eightpitch.com.ui.dynamicdetails

import android.content.res.ColorStateList
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.*
import androidx.appcompat.widget.*
import androidx.core.content.ContextCompat
import androidx.core.view.*
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.dynamicviews.*
import app.eightpitch.com.data.dto.dynamicviews.language.I18nData
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.videoplayer.activity.VideoPlayerActivity
import app.eightpitch.com.ui.webview.DefaultWebViewFragment
import app.eightpitch.com.utils.Constants.FILES_ENDPOINT
import app.eightpitch.com.utils.OnSwipeTouchListener
import kotlinx.android.synthetic.main.fragment_dynamic_project_details_child.*

/**
 * Fragment that is representing a single `tab` of a
 * [DynamicProjectDetailsParentFragment] tabs.
 *
 * This fragment is responsible for drawing (adding/deleting)
 * the views dynamically by passed args.
 */
class DynamicProjectDetailsChildFragment : BaseFragment<DynamicProjectDetailsChildViewModel>(), FragmentScrollable {

    companion object {

        private const val UI_ELEMENTS_KEY = "UI_ELEMENTS_KEY"
        private const val I18NDATA_ID_MAP = "IDS_MAPPING_KEY"
        private const val LAYOUT_ID_LIST = "LAYOUT_ID_LIST"

        fun buildWithArguments(
            uiElements: List<UIElement>,
            i18nDataToContentIdMap: I18nDataToContentIdMap,
            rootViewsContainer: RootViewsContainer
        ): DynamicProjectDetailsChildFragment {
            return DynamicProjectDetailsChildFragment().apply {
                arguments = Bundle().apply {
                    putParcelableArrayList(UI_ELEMENTS_KEY, uiElements.toArrayList())
                    putSerializable(I18NDATA_ID_MAP, i18nDataToContentIdMap)
                    putParcelable(LAYOUT_ID_LIST, rootViewsContainer)
                }
            }
        }
    }

    private val uiElements: ArrayList<UIElement>
        get() = arguments?.getParcelableArrayList<UIElement>(UI_ELEMENTS_KEY)
            .orDefaultValue(arrayListOf())

    private val i18nDataToContentIdMap: I18nDataToContentIdMap
        get() = (arguments?.getSerializable(I18NDATA_ID_MAP) as? I18nDataToContentIdMap
                 ?: I18nData() to LocalFileIdToBackendFileId())

    private val rootViewsContainer: RootViewsContainer
        get() = arguments?.getParcelable<RootViewsContainer>(LAYOUT_ID_LIST)
            .orDefaultValue(RootViewsContainer())

    override fun getLayoutID() = R.layout.fragment_dynamic_project_details_child

    override fun getVMClass() = DynamicProjectDetailsChildViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val (i18nData, localFileIdToBackendField) = i18nDataToContentIdMap
        val views = if (resources.getBoolean(R.bool.isItTabletDevice)) viewModel.convertTabletUiElements(uiElements,
            i18nData) else
            viewModel.convertUiElements(uiElements, i18nData)

        if (views.isEmpty()) {
            emptyTextView.isVisible = true
            return
        }

        handleViewsArguments(if (rootViewsContainer.layoutRootIdList.isEmpty()) views else makeOrder(views))
        nestedScroll.setOnTouchListener(object : OnSwipeTouchListener(context) {
            override fun onSwipeDown() {
                super.onSwipeDown()
                if (nestedScroll.scrollY == 0)
                    getTarget(OnSwipeUpBackPressuredListener::class.java)?.onOverSwiped()
            }
        })

        viewModel.getVideoUrlLinkResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) { vimeoUrl ->
                context?.let {
                    startActivity(VideoPlayerActivity.generateIntent(it, vimeoUrl))
                }
            }
        })
    }

    private fun makeOrder(views: List<View>): List<View> {
        handleGroups(views.filterIsInstance<ViewGroup>())
        val sortedViews = arrayListOf<View>()
        rootViewsContainer.layoutRootIdList.forEach { id ->
            views.find {
                (it.tag as? UIElementArguments)?.id == id
            }?.let {
                sortedViews.add(it)
            }
        }
        return sortedViews
    }

    private fun handleGroups(views: List<ViewGroup>) {
        views.filter { it.tag is GroupUIElementArguments }
            .forEach { view -> fulfillViewGroup(views, view, view.tag as GroupUIElementArguments) }
    }

    /**
     * Method that is handling a view arguments type of [UIElementArguments],
     * which were passed by the [View]'s tag.
     */
    private fun handleViewsArguments(views: List<View>) {
        views.forEach { view ->
            if (view is ViewGroup) {
                checkAllChildren(view)
            } else {
                parseArguments(view)
            }
            dynamicViewsContainer.addViewSafe(view)
            //TODO please consider to decrease a delay, or just using post without delay
            //Delay is required for providing an ability for RV to create a ViewHolders.
            (view as? RecyclerView)?.postDelayed({
                checkAllChildren(view)
            }, 1000L)
        }
    }

    private fun fulfillViewGroup(
        views: List<View>,
        container: ViewGroup,
        arguments: GroupUIElementArguments
    ) {
        val viewsGroup = arrayListOf<View>()
        arguments.idList.forEach { id ->
            viewsGroup.addAll(views.filterByTag(id))
        }
        viewsGroup.forEach {
            container.addView(it)
        }
    }

    private fun parseArguments(view: View) {
        val context = view.context
        when (val tag = (view.tag)) {
            is IconsUIElementArguments -> {
                context.loadImagePlaceholderNot(
                    "$FILES_ENDPOINT${i18nDataToContentIdMap.second.getOrElse(tag.imageId) { "" }}",
                    withOriginalSize = true,
                    targetImageView = (view as AppCompatImageView)
                )
                view.setDisablingClickListener {
                    navigate(
                        R.id.action_dynamicProjectDetailsParentFragment_to_defaultWebViewFragment,
                        DefaultWebViewFragment.buildWithAnArguments(tag.iconLink)
                    )
                }
            }
            is ImageUIElementArguments -> {
                context.loadImagePlaceholderNot(
                    "$FILES_ENDPOINT${i18nDataToContentIdMap.second.getOrElse(tag.imageId) { "" }}",
                    withOriginalSize = tag.withOriginalSize,
                    targetImageView = (view as AppCompatImageView)
                )
            }
            is VimeoUIElementArguments -> view.setDisablingClickListener {
                viewModel.retrieveVideoLink(tag.vimeoId)
            }
            is ClickableUIElementArguments -> view.setDisablingClickListener {
                navigate(
                    R.id.action_dynamicProjectDetailsParentFragment_to_defaultWebViewFragment,
                    DefaultWebViewFragment.buildWithAnArguments(tag.url)
                )
            }
            is SpannableUIElementArguments -> if (view is AppCompatTextView)
                view.apply {
                    movementMethod = LinkMovementMethod.getInstance()
                    setLinkTextColor(ColorStateList(
                        arrayOf(
                            intArrayOf(android.R.attr.state_pressed),
                            intArrayOf()
                        ), intArrayOf(
                            ContextCompat.getColor(context, R.color.colorPrimaryDark),
                            ContextCompat.getColor(context, R.color.linkBlueColor)
                        )
                    ))
                    text = tag.words.createSpannableString {
                        navigate(
                            R.id.action_dynamicProjectDetailsParentFragment_to_defaultWebViewFragment,
                            DefaultWebViewFragment.buildWithAnArguments(it)
                        )
                    }
                }

            else -> return
        }
    }

    private fun checkAllChildren(view: ViewGroup) {
        view.forEach { childView ->
            when (childView) {
                is ViewPager2 -> handleViewPagerAdapters(childView)
                is ViewGroup -> checkAllChildren(childView)
                else -> parseArguments(childView)
            }
        }
    }

    private fun handleViewPagerAdapters(viewPager2: ViewPager2) {
        viewPager2.adapter.run {
            when (this) {
                is ImageLinkAdapter -> getResult().observe(viewLifecycleOwner, {
                    getParentLayout()?.let { checkAllChildren(it) }
                })
                is TiledImageAdapter -> getResult().observe(viewLifecycleOwner, {
                    getParentLayout()?.let { checkAllChildren(it) }
                })
                is TabletImageLinkAdapter -> getResult().observe(viewLifecycleOwner, {
                    getParentLayout()?.let { checkAllChildren(it) }
                })
                is TabletTiledImageAdapter -> getResult().observe(viewLifecycleOwner, {
                    getParentLayout()?.let { checkAllChildren(it) }
                })
            }
        }
    }

    override fun scrollTop() {
        nestedScroll.scrollTo(0, 0)
    }
}

