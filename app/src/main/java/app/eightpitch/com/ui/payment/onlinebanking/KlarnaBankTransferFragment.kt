package app.eightpitch.com.ui.payment.onlinebanking

import android.graphics.drawable.Animatable
import android.os.Bundle
import android.view.*
import androidx.core.view.isVisible
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import app.eightpitch.com.R
import app.eightpitch.com.base.BackPressureFragment
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.webview.DefaultWebViewFragment
import app.eightpitch.com.utils.OnSwipeTouchListener
import app.eightpitch.com.utils.animation.AnimationCallback
import kotlinx.android.synthetic.main.fragment_klarna_bank_transfer.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class KlarnaBankTransferFragment : BackPressureFragment() {

    companion object {

        const val KLARNA_URL = "KLARNA_URL"
        const val PROJECT_NAME = "PROJECT_NAME"

        fun buildWithAnArguments(
            url: String,
            projectName: String
        ) = Bundle().apply {
            putString(KLARNA_URL, url)
            putString(PROJECT_NAME, projectName)
        }
    }

    private val url: String
        get() = arguments?.getString(KLARNA_URL, "") ?: ""

    private val projectName: String
        get() = arguments?.getString(PROJECT_NAME, "").orDefaultValue("")

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View =
        inflater.inflate(R.layout.fragment_klarna_bank_transfer, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        additionalToolbarRightButton.isVisible = false
        headerTitle.text = getString(R.string.investment_payment_toolbar_title)

        setupAnimation()

        view.setOnTouchListener(object : OnSwipeTouchListener(context) {
            override fun onSwipeDown() {
                super.onSwipeDown()
                val bundle = DefaultWebViewFragment.buildWithAnArguments(url, projectName)
                navigate(R.id.action_klarnaBankTransferFragment_to_defaultWebViewFragment, bundle)
            }
        })
    }

    private fun setupAnimation() {
        val drawableImage = klarnaItemView.drawable
        (drawableImage as? Animatable)?.let { animatableDrawable ->
            animatableDrawable.start()
            AnimatedVectorDrawableCompat.registerAnimationCallback(
                drawableImage,
                AnimationCallback(animatableDrawable)
            )
        }
    }
}