package app.eightpitch.com.ui.kyc.summatyconfirmed

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.*
import app.eightpitch.com.R
import app.eightpitch.com.base.BackPressureFragment
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.webid.WebIdIdentifyFragment
import kotlinx.android.synthetic.main.fragment_email_confirmed.*

class SummaryConfirmedFragment : BackPressureFragment() {

    companion object {

        private const val ACTION_ID_KEY = "ACTION_ID_KEY"

        fun buildWithAnArguments(
            actionId: String
        ) = Bundle().apply {
            putString(ACTION_ID_KEY, actionId)
        }
    }

    private val actionId: String
        get() = arguments?.getString(ACTION_ID_KEY) ?: ""

    private val locationPermissions = arrayOf(
        Manifest.permission.CAMERA,
        Manifest.permission.RECORD_AUDIO
    )

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_summary_confirmed, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        registerOnBackPressedListener { moveBackward() }

        crossButton.setOnClickListener { moveBackward() }

        nextToKYCButton.setOnClickListener {
            startWebId(actionId)
        }
    }

    private fun startWebId(actionId: String) {
        if (!isAliveAndAvailable())
            return
        context?.let {
            it.checkPermissions(locationPermissions,
                {
                    moveToWebId()
                },
                { deniedPermissions ->
                    requestPermissions(
                        deniedPermissions,
                        REQUEST_CAMERA_AND_MICROPHONE
                    )
                })
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CAMERA_AND_MICROPHONE) {
            if (grantResults.any { it != PackageManager.PERMISSION_GRANTED }) {
                if (shouldShowRequest()) {
                    showInfoDialog(
                        getString(R.string.system_settings_text)
                    ) { moveToSettings() }
                } else
                    showInfoDialog(
                        getString(R.string.activate_text)
                    ) { startWebId("") }
            } else
                moveToWebId()
        }
    }

    private fun moveToWebId() {
        navigate(
            R.id.action_summaryConfirmedFragment_to_webIdIdentifyFragment,
            WebIdIdentifyFragment.buildWithAnArguments(actionId)
        )
    }

    private fun showInfoDialog(positiveButtonMessage: String, positiveListener: () -> Unit) {
        showDialog(
            title = getString(R.string.edit_settings_text),
            message = getString(R.string.you_can_only_proceed_text),
            negativeButtonMessage = getString(R.string.cancel_text),
            positiveButtonMessage = positiveButtonMessage,
            positiveListener = { positiveListener() }
        )
    }

    private fun moveToSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        intent.data = Uri.fromParts("package", context?.packageName, null)
        startActivity(intent)
    }

    private fun shouldShowRequest() = !shouldShowRequestPermissionRationale(Manifest.permission.CAMERA) &&
                                      !shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO)
}
