package app.eightpitch.com.ui.authorized.main.homepager

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.eightpitch.com.R
import app.eightpitch.com.base.rootbackhandlers.RootNavGraphHandleAbleFragment
import app.eightpitch.com.extensions.moveToStartDestination

class NotificationRootFragment : RootNavGraphHandleAbleFragment(), NavigationApplier {

    override fun getNavigationHostFragmentId() = R.id.navigation_notification_fragment_host

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_root_notification_layout, container, false)
    }

    override fun applyNavigationState(navigationId: Int) {
        //TODO do nothing for now.
    }

    override fun popUpToStartDestination() {
        moveToStartDestination(getNavigationHostFragmentId())
    }
}