package app.eightpitch.com.ui.kyc.country

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import app.eightpitch.com.R
import app.eightpitch.com.extensions.getSortedCountries
import app.eightpitch.com.views.interfaces.HintAbleSpinnerAdapter
import kotlinx.android.synthetic.main.item_view_dropdown_gender.view.*
import kotlinx.android.synthetic.main.item_view_gender.view.*

class CountrySpinnerAdapter(
    context: Context
) : HintAbleSpinnerAdapter(context, context.getSortedCountries()) {

    override fun getView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, recycledView, parent)
    }

    override fun getDropDownView(position: Int, recycledView: View?, parent: ViewGroup): View {
        val country = getItem(position)
        val view = getView(parent, recycledView, R.layout.item_view_dropdown_gender)
        view?.apply { genderDropDownTitle.text = country }
        return view
    }

    private fun createView(position: Int, recycledView: View?, parent: ViewGroup): View {
        val view = getView(parent, recycledView, R.layout.item_view_gender)
        view?.apply {
            if (position == getHintPosition()){
                genderTitle.text = ""
            }else{
                val country = getItem(position)
                genderTitle.text = country
                genderTitle.hint = ""
            }
        }
        return view
    }

    private fun getView(
        parent: ViewGroup,
        recycledView: View?,
        @LayoutRes idRes: Int
    ) = recycledView ?: LayoutInflater.from(context).inflate(
        idRes,
        parent,
        false
    )

    override fun getHintPosition(): Int {
        val count = count
        return if (count > 0) count + 1 else count
    }

    override fun getHint(): String = try {
        getItem(getHintPosition()).orEmpty()
    } catch (throwable: Throwable) {
        ""
    }
}