package app.eightpitch.com.ui.kyc

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseActivity
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.authorized.AuthorizedActivity
import app.eightpitch.com.ui.kyc.KycNavigationId.*
import app.eightpitch.com.utils.IntentsController

class KycActivity : BaseActivity() {

    lateinit var viewModel: KycActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kyc)

        val factory = (this as BaseActivity).viewModelFactory
        viewModel = ViewModelProvider(this, factory)[KycActivityViewModel::class.java]

        if (!::viewModel.isInitialized)
            return

        viewModel.getUserMeResult().observe(this, {
            handleResult(it) { kycNavigationId ->
                val navigationId = when (kycNavigationId) {
                    NAVIGATE_TO_EMAIL -> R.id.emailCodeValidationFragment
                    NAVIGATE_TO_2FA -> R.id.kycAddExtraSecurityFragment
                    NAVIGATE_TO_KYC -> R.id.kycPersonalInfoFragment
                    NAVIGATE_TO_WEB_ID -> R.id.summaryConfirmedFragment
                    NAVIGATE_TO_INV_QUALIFICATION -> R.id.questionnaireStartFragment
                    NAVIGATE_TO_QUESTIONNAIRE -> R.id.questionnaireFirstFragment
                    CLOSE_QUESTIONNAIRE -> -1
                }
                if (navigationId == -1) startActivityOrFinish()
                val relatedNavController =
                    Navigation.findNavController(this, R.id.nav_host_fragment)
                relatedNavController.navigateSafe(navigationId)
            }
        })

        viewModel.getUser()
    }

    private fun startActivityOrFinish() {
        if (isTaskRoot)
            IntentsController.startActivityWithANewStack(applicationContext, AuthorizedActivity::class.java)
        else
            finish()
    }
}
