package app.eightpitch.com.ui.kyc.summaruinformation

import android.content.Context
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.user.Country.Companion.toDisplayValue
import app.eightpitch.com.data.dto.webid.ActionIdResponseBody
import app.eightpitch.com.data.dto.webid.WebIdUserRequestBody
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.utils.Constants.DD_MM_YYYY_HH_MM_SS_ZONE
import app.eightpitch.com.utils.DateUtils.formatByPattern
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.coroutines.launch
import javax.inject.Inject

class KycSummaryInfoFragmentViewModel @Inject constructor(
    private val appContext: Context,
    private val userModel: UserModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val title = appContext.getString(R.string.kyc_summary_title)

    val fullNameField = ObservableField("")
    val dateOfBirthField = ObservableField("")
    val nationalityField = ObservableField("")
    val countryField = ObservableField("")
    val cityField = ObservableField("")
    val zipCodeField = ObservableField("")
    val streetField = ObservableField("")
    val numberField = ObservableField("")
    val accountOwnerField = ObservableField("")
    val ibanField = ObservableField("")
    val userPoliticallyField = ObservableField(appContext.getString(R.string.no))
    val userLiabilityField = ObservableField(appContext.getString(R.string.no))

    private val updateUserPersonalDataResult = SingleActionLiveData<Result<ActionIdResponseBody>>()
    internal fun getUpdateUserPersonalDataResult(): LiveData<Result<ActionIdResponseBody>> =
        updateUserPersonalDataResult

    internal fun updateUserPersonalData(webIdUserRequestBody: WebIdUserRequestBody) {
        viewModelScope.launch {
            val result = asyncLoading {
                webIdUserRequestBody.run {
                    userModel.getActionId(
                        copy(_dateOfBirth = dateOfBirth.formatByPattern(DD_MM_YYYY_HH_MM_SS_ZONE))
                    )
                }
            }
            updateUserPersonalDataResult.postAction(result)
        }
    }

    internal fun setKycField(webIdUserRequestBody: WebIdUserRequestBody) {
        webIdUserRequestBody.run {
            fullNameField.set("$firstName $lastName")
            dateOfBirthField.set(dateOfBirth.formatByPattern())
            webIdPersonalInfo.apply {
                nationalityField.set(toDisplayValue(appContext, nationality))
                accountOwnerField.set(accountOwner)
                ibanField.set(IBAN)
                userPoliticallyField.set(appContext.getString(if(isPoliticallyExposedPerson) R.string.yes else R.string.no))
                userLiabilityField.set(appContext.getString(if(isUsTaxLiability) R.string.yes else R.string.no))
            }
            address.apply {
                countryField.set(toDisplayValue(appContext, country))
                cityField.set(city)
                zipCodeField.set(zipCode)
                streetField.set(street)
                numberField.set(streetNumber)
            }
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        fullNameField.set("")
        dateOfBirthField.set("")
        nationalityField.set("")
        countryField.set("")
        cityField.set("")
        zipCodeField.set("")
        streetField.set("")
        numberField.set("")
        accountOwnerField.set("")
        ibanField.set("")
    }
}