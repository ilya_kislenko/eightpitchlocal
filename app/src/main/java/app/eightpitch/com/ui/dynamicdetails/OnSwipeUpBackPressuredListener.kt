package app.eightpitch.com.ui.dynamicdetails

/**
 * An interface that helps to inform parent fragment that
 * swipe event on a child fragment has been performed, so
 * parent can do something with it.
 */
interface OnSwipeUpBackPressuredListener {

    fun onOverSwiped()
}