package app.eightpitch.com.ui.dashboard.initiator

import android.os.Parcel
import android.os.Parcelable
import app.eightpitch.com.data.dto.project.AmountOfProjectInvestments
import app.eightpitch.com.data.dto.project.InvestorsProject
import app.eightpitch.com.data.dto.project.ProjectBi
import app.eightpitch.com.data.dto.project.TokenParametersDocument
import app.eightpitch.com.data.dto.projectdetails.ProjectsFile
import app.eightpitch.com.extensions.NO_TYPE
import app.eightpitch.com.extensions.readBigDecimal
import app.eightpitch.com.extensions.readStringSafe
import java.math.BigDecimal

/**
 * Initiator project UI representation DTO. Supposed to be passed by arguments.
 * @param projectBi information for UI presentation (digits about investments)
 */
class InitiatorProjectItem(
    var id: String = "0",
    var companyName: String = "",
    var description: String = "",
    var graph: AmountOfProjectInvestments = AmountOfProjectInvestments(softCap = null),
    var projectBi: ProjectBi = ProjectBi(),
    var tokenParametersDocument: TokenParametersDocument = TokenParametersDocument(),
    var monetaryAmount: BigDecimal = BigDecimal.ZERO,
    @InvestorsProject.Companion.ProjectStatus var projectStatus: String = NO_TYPE,
    var projectFiles: List<ProjectsFile> = arrayListOf()
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readParcelable(AmountOfProjectInvestments::class.java.classLoader) ?: AmountOfProjectInvestments(),
        parcel.readParcelable(ProjectBi::class.java.classLoader) ?: ProjectBi(),
        parcel.readParcelable(TokenParametersDocument::class.java.classLoader) ?: TokenParametersDocument(),
        parcel.readBigDecimal(),
        parcel.readStringSafe(),
        parcel.readParcelableList(mutableListOf<ProjectsFile>(), ProjectsFile::class.java.classLoader)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(companyName)
        parcel.writeString(description)
        parcel.writeParcelable(graph, flags)
        parcel.writeParcelable(projectBi, flags)
        parcel.writeParcelable(tokenParametersDocument, flags)
        parcel.writeSerializable(monetaryAmount)
        parcel.writeString(projectStatus)
        parcel.writeParcelableList(projectFiles, flags)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<InitiatorProjectItem> {
        override fun createFromParcel(parcel: Parcel): InitiatorProjectItem {
            return InitiatorProjectItem(parcel)
        }

        override fun newArray(size: Int): Array<InitiatorProjectItem?> {
            return arrayOfNulls(size)
        }
    }
}