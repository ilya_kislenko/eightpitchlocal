package app.eightpitch.com.ui.pincode

class PinCodeInput(
    private val size: Int = 4
) {

    var code: String = ""
        private set

    /**
     * Method that will try to append a [char] to a [code].
     * @return true if appended successfully false if size if reached and last possible char is appended.
     */
    internal fun appendIfPossible(char: String): Boolean {
        addDigit(char)
        return code.length != size
    }

    private fun addDigit(digit: String) {
        if (size > code.length) {
            code += digit
        }
    }

    internal fun getLength() = code.length

    internal fun clearDigit() {
        if (code.isEmpty())
            return
        code = code.substring(0, code.length - 1)
    }

    internal fun clear() {
        code = ""
    }
}