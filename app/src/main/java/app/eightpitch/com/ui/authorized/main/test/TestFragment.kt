package app.eightpitch.com.ui.authorized.main.test

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.ui.registration.RegistrationActivity
import app.eightpitch.com.utils.IntentsController
import app.eightpitch.com.utils.Logger
import kotlinx.android.synthetic.main.fragment_test_profile.*

class TestFragment : BaseFragment<TestFragmentViewModel>() {

    override fun getLayoutID() = R.layout.fragment_test_profile

    override fun getVMClass() = TestFragmentViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        logoutButton.setOnClickListener {
            viewModel.logout()
        }

        refreshToken.setOnClickListener {
            viewModel.refreshToken()
        }

        viewModel.getRefreshTokenResult.observe(viewLifecycleOwner, Observer {
            handleResult(it) { tokenResponse ->
                Logger.logInfo(Logger.TAG_INFO, tokenResponse.toString())
            }
        })

        viewModel.getMeResult.observe(viewLifecycleOwner, Observer {
            handleResult(it) { me ->
                Logger.logInfo(Logger.TAG_INFO, me.toString())
            }
        })

        viewModel.getLogoutResult.observe(viewLifecycleOwner, Observer {
            handleResult(it) { applicationContext ->
                IntentsController.startActivityWithANewStack(
                    applicationContext,
                    RegistrationActivity::class.java
                )
            }
        })
    }
}