package app.eightpitch.com.ui.payment.softcappaymentselection

import android.os.Bundle
import android.view.View
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.base.rootbackhandlers.ChildNavGraphBackPressureHandler
import app.eightpitch.com.data.dto.investment.ProjectInvestmentInfoResponse
import app.eightpitch.com.data.dto.payment.TransactionInfoResponse.Companion.OK
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.ui.payment.compleatepayment.ClassicBankPaymentFragment
import app.eightpitch.com.ui.payment.compleatepayment.ClassicBankPaymentFragment.Companion.FROM_SOFT_CAP
import app.eightpitch.com.ui.payment.directdebitcompleate.DirectDebitPaymentFragment
import app.eightpitch.com.ui.payment.hardcappaymentselection.HardCapPaymentSelectionFragment
import app.eightpitch.com.ui.payment.onlinebanking.FintBankTransferFragment
import kotlinx.android.synthetic.main.fragment_soft_cap_payment_selection.*

class SoftCapPaymentSelectionFragment : BaseFragment<SoftCapPaymentSelectionViewModel>(),
    ChildNavGraphBackPressureHandler {

    companion object {

        const val INVESTMENT_ID = "INVESTMENT_ID"
        const val PROJECT_ID = "PROJECT_ID"
        const val SOFT_CAP_PAYMENTS_DATA = "SOFT_CAP_PAYMENTS_DATA"

        fun buildWithAnArguments(
            investmentId: String,
            projectId: String,
            projectInvestmentInfo: ProjectInvestmentInfoResponse
        ) = Bundle().apply {
            putString(INVESTMENT_ID, investmentId)
            putString(PROJECT_ID, projectId)
            putParcelable(SOFT_CAP_PAYMENTS_DATA, projectInvestmentInfo)
        }
    }

    private val investmentId: String
        get() = arguments?.getString(INVESTMENT_ID) ?: ""

    private val projectId: String
        get() = arguments?.getString(HardCapPaymentSelectionFragment.PROJECT_ID, "") ?: ""

    private val projectInvestmentInfo: ProjectInvestmentInfoResponse
        get() = arguments?.getParcelable(SOFT_CAP_PAYMENTS_DATA) ?: ProjectInvestmentInfoResponse()

    override fun getLayoutID() = R.layout.fragment_soft_cap_payment_selection

    override fun getVMClass() = SoftCapPaymentSelectionViewModel::class.java

    override fun onResume() {
        super.onResume()
        viewModel.getProjectInvestmentInfo(projectId)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        fintItemView.setOnClickListener {
            viewModel.submitFintPayment(investmentId)
        }

        classicItemView.setOnClickListener {
            navigate(
                R.id.action_softCapPaymentSelectionFragment_to_classicBankPaymentFragment,
                ClassicBankPaymentFragment.buildWithAnArguments(
                    projectInvestmentInfo, FROM_SOFT_CAP
                )
            )
        }

        directItemView.setOnClickListener {
            navigate(
                R.id.action_softCapPaymentSelectionFragment_to_directDebitPaymentFragment,
                DirectDebitPaymentFragment.buildWithAnArguments(projectInvestmentInfo)
            )
        }

        viewModel.getTransactionResult().observe(viewLifecycleOwner, {
            handleResult(it) { transaction ->
                when (transaction.responseStatus) {
                    OK -> navigate(
                        R.id.action_softCapPaymentSelectionFragment_to_fintBankTransferFragment,
                        FintBankTransferFragment.buildWithAnArguments(
                            transaction.paymentUrl, projectInvestmentInfo.getProjectName()
                        )
                    )
                    else -> {
                        navigate(R.id.action_softCapPaymentSelectionFragment_to_paymentDeclinedFragment)
                    }
                }

            }
        })

        viewModel.getProjectInvestmentInfoResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) {}
        })
    }

    override fun handleBackPressure(): Boolean {
        return popBackStack(R.id.projectDetailsFragment)
    }
}