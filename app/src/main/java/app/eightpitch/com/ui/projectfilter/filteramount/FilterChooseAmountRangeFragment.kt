package app.eightpitch.com.ui.projectfilter.filteramount

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.extensions.injectViewModel
import app.eightpitch.com.ui.projectfilter.SharableFilterViewModel
import app.eightpitch.com.ui.projectfilter.filteramount.adapter.FilterButtonAmountRangeAdapter
import app.eightpitch.com.utils.SafeLinearLayoutManager
import kotlinx.android.synthetic.main.fragment_filter_choose_amount.*
import kotlinx.android.synthetic.main.fragment_filter_choose_security.applyFilterButton
import kotlinx.android.synthetic.main.toolbar_layout.*

class FilterChooseAmountRangeFragment : BaseFragment<FilterChooseAmountRangeFragmentViewModel>() {

    override fun getLayoutID() = R.layout.fragment_filter_choose_amount

    override fun getVMClass() = FilterChooseAmountRangeFragmentViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindLoadingIndicator()

        additionalToolbarRightButton.apply {
            setImageResource(R.drawable.icon_refresh)
            setOnClickListener {
                viewModel.resetStatuses()
            }
        }

        rangeRecycler.apply {
            layoutManager = SafeLinearLayoutManager(context, VERTICAL, false)
            adapter = FilterButtonAmountRangeAdapter().apply {
                onItemClicked = { item, isActive, _ ->
                    if (isActive) viewModel.addToFilter(item)
                    else viewModel.removeFromFilter(item)
                }
            }
        }

        applyFilterButton.setOnClickListener {
            viewModel.applyFilter()
            popBackStack(R.id.filterProjectFragment)
        }

        viewModel.getAmountRangesResult().observe(viewLifecycleOwner, Observer {
            handleResult(it) { }
        })

        val sharedViewModel = injectViewModel(SharableFilterViewModel::class.java)
        viewModel.setFilters(sharedViewModel)
        viewModel.getAmountRanges()
    }
}
