package app.eightpitch.com.ui.investment

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleObserver
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.investment.ProjectInvestmentInfoResponse
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.extensions.filterDraftDocuments
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.extensions.hideKeyboard
import app.eightpitch.com.extensions.injectViewModel
import app.eightpitch.com.ui.PreContractualInfoFragment
import app.eightpitch.com.ui.authorized.BottomTabsHolder
import app.eightpitch.com.ui.dialogs.AmountLimitInfoDialog
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.fragment_currency_chooser.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class CurrencyChooserFragment : BaseFragment<CurrencyChooserViewModel>() {

    companion object {

        private const val PROJECT_ID = "PROJECT_ID"
        private const val PROJECT_INVESTMENT = "PROJECT_INVESTMENT"
        private const val USER_KEY = "USER_KEY"

        fun buildWithAnArguments(
            projectId: String,
            projectInvestmentInfo: ProjectInvestmentInfoResponse,
            user: User,
        ) = Bundle().apply {
            putString(PROJECT_ID, projectId)
            putParcelable(PROJECT_INVESTMENT, projectInvestmentInfo)
            putParcelable(USER_KEY, user)
        }
    }

    private val projectId: String
        get() = arguments?.getString(PROJECT_ID, "") ?: ""

    private val projectInvestmentInfo: ProjectInvestmentInfoResponse
        get() = arguments?.getParcelable(PROJECT_INVESTMENT) ?: ProjectInvestmentInfoResponse()

    private val user: User
        get() = arguments?.getParcelable(USER_KEY) ?: User()

    override fun getLayoutID() = R.layout.fragment_currency_chooser

    override fun getVMClass() = CurrencyChooserViewModel::class.java

    private var tabLayoutMediator: TabLayoutMediator? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        viewModel.getCurrencyInputValidatorObserver().observe(viewLifecycleOwner, { block ->
            continueButton.isEnabled = block
        })

        initTabLayoutWithPager()

        headerTextView.text = getString(R.string.welcome_invest_pattern_text, user.getWelcomeAppeal(context))

        viewModel.getProjectResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) { projectResponse ->
                val viewPagerSharableViewModel =
                    injectViewModel(ViewPagerSharableViewModel::class.java)
                val amount = viewPagerSharableViewModel.getTokens()
                val arguments = PreContractualInfoFragment.buildWithAnArguments(
                    projectId,
                    projectInvestmentInfo,
                    projectResponse.projectFiles.filterDraftDocuments(),
                    amount
                )
                when {
                    user.isShowTaxUpdating -> navigate(
                        R.id.action_currencyChooserFragment_to_germanTaxFragment,
                        arguments)
                    else -> navigate(
                        R.id.action_currencyChooserFragment_to_preContractualInfoFragment,
                        arguments)
                }
            }
        })

        continueButton.setOnClickListener {
            viewModel.getProjectInfo(projectId)
        }
    }

    private fun initTabLayoutWithPager() {

        val tabNamesToDrawable = listOf(
            getString(R.string.euro_tab_text) to R.drawable.icon_euro,
            "${projectInvestmentInfo.shortCut}," to R.drawable.icon_amount_of_tokens_in_white_circle
        )
        tabLayoutMediator = TabLayoutMediator(currencyTab, inputPager) { tab, position ->
            val (tabName, tabDrawable) = tabNamesToDrawable[position]
            tab.customView = context?.let {
                (layoutInflater.inflate(R.layout.tab_item_custom_view, null, false) as ConstraintLayout)
                    .apply {
                        (getViewById(R.id.appCompatTextView) as AppCompatTextView).text = tabName
                        (getViewById(R.id.appCompatImageView) as AppCompatImageView).setImageDrawable(
                            ContextCompat.getDrawable(it, tabDrawable))
                    }
            }
        }
        inputPager.run {
            adapter = CurrencyInputAdapter(this@CurrencyChooserFragment,
                projectInvestmentInfo,
                viewModel.getCurrencyInputValidatorObserver())
            setCurrentItem(0, true)
        }
        tabLayoutMediator?.attach()

        val hasInvestorLimit = projectInvestmentInfo.investorClassLimit != null
        if (hasInvestorLimit) {
            additionalToolbarRightButton.apply {
                setImageResource(R.drawable.icon_info)
                setOnClickListener {
                    showLimitInfoDialog()
                }
            }
        }
        viewModel.updateVisibilityRightButton(hasInvestorLimit)
    }


    private fun showLimitInfoDialog() {
        val dialogLimitInfo = AmountLimitInfoDialog().apply {
            onTextClicked = {
                if (activity is BottomTabsHolder) {
                    val rootActivity = (activity as BottomTabsHolder)
                    rootActivity.switchTabWith(3, R.id.profilePersonalFragment)
                }
            }
        }
        dialogLimitInfo.show(
            AmountLimitInfoDialog::class.java.name,
            childFragmentManager
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        activity?.hideKeyboard()
        tabLayoutMediator?.detach()
        tabLayoutMediator = null
    }
}