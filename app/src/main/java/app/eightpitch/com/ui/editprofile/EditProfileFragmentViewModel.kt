package app.eightpitch.com.ui.editprofile

import android.content.Context
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.user.*
import app.eightpitch.com.data.dto.user.TaxInformationBody.TaxChurch.Companion.fromDisplayValue
import app.eightpitch.com.data.dto.user.User.CREATOR.INSTITUTIONAL_INVESTOR
import app.eightpitch.com.data.dto.user.User.CREATOR.LEVEL_1
import app.eightpitch.com.data.dto.user.User.CREATOR.LEVEL_2
import app.eightpitch.com.data.dto.user.User.CREATOR.PRIVATE_INVESTOR
import app.eightpitch.com.data.dto.user.User.CREATOR.PROJECT_INITIATOR
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.data.transmit.ResultStatus.SUCCESS
import app.eightpitch.com.extensions.*
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.utils.*
import app.eightpitch.com.utils.Constants.DD_p_MM_p_YYYY
import app.eightpitch.com.utils.Constants.YYYY_MM_DD
import app.eightpitch.com.utils.DateUtils.formatByPattern
import app.eightpitch.com.utils.DateUtils.parseByFormatWithoutMMSS
import app.eightpitch.com.utils.RegexPatterns.PATTERN_OWNER_PREFIX_CORRECT
import app.eightpitch.com.utils.RegexPatterns.PATTERN_VALID_CITY
import app.eightpitch.com.utils.RegexPatterns.PATTERN_VALID_CODE
import app.eightpitch.com.utils.RegexPatterns.PATTERN_VALID_NAME
import app.eightpitch.com.utils.RegexPatterns.PATTERN_VALID_OWNER
import app.eightpitch.com.views.interfaces.ShortFieldValidator
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*
import javax.inject.Inject

class EditProfileFragmentViewModel @Inject constructor(
    private val appContext: Context,
    private val userModel: UserModel,
    threadsSeparator: ThreadsSeparator,
) : BaseViewModel(threadsSeparator) {

    val title = appContext.getString(R.string.edit_personal_toolbar_title)

    private var currentUser: User? = null

    var birthDateMillis: Long = 0
        private set

    private var liability = false
    private var politically = false

    //region visibility observable fields
    val otherButtonsVisibility = ObservableField<Int>(GONE)
    val addressFieldsVisibility = ObservableField<Int>(GONE)
    val companyFieldsVisibility = ObservableField<Int>(GONE)
    val financialFieldsVisibility = ObservableField<Int>(GONE)
    val dateOfBirthFieldVisibility = ObservableField<Int>(GONE)
    val nationalityFieldVisibility = ObservableField<Int>(GONE)
    val placeOfBirthFieldVisibility = ObservableField<Int>(GONE)
    val personalBaseFieldsVisibility = ObservableField<Int>(GONE)
    //endregion

    //region observable fields
    val confirmButtonNameField = ObservableField("")

    val companyNameField = ObservableField("")
    val companyNumberField = ObservableField("")

    val firstNameField = ObservableField("")
    val lastNameField = ObservableField("")
    val birthDateField = ObservableField("")
    val placeOfBirthField = ObservableField("")

    val cityField = ObservableField("")
    val zipCodeField = ObservableField("")
    val streetNameField = ObservableField("")
    val streetNumberField = ObservableField("")

    val accountOwnerField = ObservableField("")
    val ibanField = ObservableField("")

    val politicalExposedFieldsValue = ObservableBoolean(false)
    val usTaxFieldsValue = ObservableBoolean(false)
    //endregion

    //region Binding fields
    private var companyName: String = ""
    val companyNameErrorHolder = SingleActionObservableString()

    private var companyNumber: String = ""
    val companyNumberErrorHolder = SingleActionObservableString()

    private var companyType = ""
    val companyTypeErrorHolder = SingleActionObservableString()

    private var genderType = ""
    val genderErrorHolder = SingleActionObservableString()

    private var firstName: String = ""
    val firstNameErrorHolder = SingleActionObservableString()

    private var lastName: String = ""
    val lastNameErrorHolder = SingleActionObservableString()

    val birthDateErrorHolder = SingleActionObservableString()

    private var birthPlace: String = ""
    val birthPlaceErrorHolder = SingleActionObservableString()

    private var nationality = ""
    val nationalityErrorHolder = SingleActionObservableString()

    private var country = ""
    val countryErrorHolder = SingleActionObservableString()

    private var city: String = ""
    val cityErrorHolder = SingleActionObservableString()

    private var zipCode: String = ""
    val zipCodeErrorHolder = SingleActionObservableString()

    private var street: String = ""
    val streetErrorHolder = SingleActionObservableString()

    private var streetNumber: String = ""
    val numberErrorHolder = SingleActionObservableString()

    private var owner: String = ""
    val ownerErrorHolder = SingleActionObservableString()

    private var IBAN: String = ""
    val IBANErrorHolder = SingleActionObservableString()

    val firstNameWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            firstName = sequence.toString()
        }
    }

    val lastNameWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            lastName = sequence.toString()
        }
    }

    val companyNameWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            companyName = sequence.toString()
        }
    }

    val companyNumberWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            companyNumber = sequence.toString()
        }
    }

    val cityWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            city = sequence.toString()
        }
    }

    val birthPlaceWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            birthPlace = sequence.toString()
        }
    }

    val zipCodeWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            zipCode = sequence.toString()
        }
    }

    val streetWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            street = sequence.toString()
        }
    }

    val numberWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            streetNumber = sequence.toString()
        }
    }

    val ownerWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            owner = sequence.toString()
        }
    }

    val ibanWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            IBAN = sequence.toString()
        }
    }
    //endregion

    //GermanTax
    val germanTaxInfoVisibility = ObservableField(GONE)
    val isGermanyTaxResident = ObservableBoolean()
    val isTaxLiability = ObservableBoolean()

    private var taxId = ""
    val taxIdErrorHolder = SingleActionObservableString()
    val taxIdWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            taxId = sequence.toString()
        }
    }
    val taxIdField = ObservableField("")

    private var taxOffice = ""
    val taxOfficeErrorHolder = SingleActionObservableString()
    val taxOfficeWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            taxOffice = sequence.toString()
        }
    }
    val taxOfficeField = ObservableField("")

    var taxChurch = ""
    internal fun setChurch(position: Int) {
        taxChurch = TaxInformationBody.TaxChurch.getTaxChurches()[position]
    }
    //endregion

    internal fun validateInput(): Boolean {

        val companyError = when {
            companyName.isEmpty() -> getErrorEmptyString(appContext)
            else -> ""
        }
        if (companyError.isNotEmpty())
            companyNameErrorHolder.set(companyError)

        val companyTypeError = when {
            companyType.trim().isEmpty() -> getErrorEmptyString(appContext)
            else -> ""
        }
        if (companyTypeError.isNotEmpty())
            companyTypeErrorHolder.set(companyError)

        val companyNumberError = when {
            companyNumber.isEmpty() -> getErrorEmptyString(appContext)
            else -> ""
        }

        if (companyNumberError.isNotEmpty())
            companyNumberErrorHolder.set(companyNumberError)


        val genderTypeError = when {
            genderType.isEmpty() -> getErrorEmptyString(appContext)
            else -> ""
        }
        if (genderTypeError.isNotEmpty())
            genderErrorHolder.set(companyError)

        val firstNameError = firstName.validateByDefaultPattern(
            appContext,
            R.string.first_name,
            PATTERN_VALID_NAME
        )
        if (firstNameError.isNotEmpty())
            firstNameErrorHolder.set(firstNameError)

        val secondNameError = lastName.validateByDefaultPattern(
            appContext,
            R.string.last_name,
            PATTERN_VALID_NAME
        )
        if (secondNameError.isNotEmpty())
            lastNameErrorHolder.set(secondNameError)

        val birthDateError = when {
            birthDateMillis == 0L -> getErrorEmptyString(appContext)
            YearsValidator.validateYearsOld(birthDateMillis) -> appContext.getString(R.string.not_valid_years_old)
            else -> ""
        }
        if (birthDateError.isNotEmpty())
            birthDateErrorHolder.set(birthDateError)

        val birthPlaceError = when {
            birthPlace.isEmpty() -> getErrorEmptyString(appContext)
            !birthPlace.matchPattern(RegexPatterns.PATTERN_VALID_BIRTH_PLACES) -> appContext.getString(
                R.string.not_valid_birth_place_letter
            )
            else -> ""
        }
        if (birthPlaceError.isNotEmpty())
            birthPlaceErrorHolder.set(birthPlaceError)

        val nationalityError = when {
            nationality.trim().isEmpty() -> getErrorEmptyString(appContext)
            else -> ""
        }
        if (nationalityError.isNotEmpty())
            nationalityErrorHolder.set(companyError)

        val countryError = when {
            country.isEmpty() -> getErrorEmptyString(appContext)
            else -> ""
        }
        if (countryError.isNotEmpty())
            countryErrorHolder.set(companyError)

        val cityError = city.validateByDefaultPattern(
            appContext,
            R.string.city,
            PATTERN_VALID_CITY
        )
        if (cityError.isNotEmpty())
            cityErrorHolder.set(cityError)

        val zipCodeError = zipCode.validateByDefaultPattern(
            appContext,
            R.string.zip_code,
            PATTERN_VALID_CODE,
            ""
        )
        if (zipCodeError.isNotEmpty())
            zipCodeErrorHolder.set(zipCodeError)

        val streetError = street.validateByDefaultPattern(
            appContext,
            R.string.street,
            PATTERN_VALID_NAME,
            ""
        )
        if (streetError.isNotEmpty())
            streetErrorHolder.set(streetError)

        val streetNumberError = streetNumber.validateByDefaultPattern(
            appContext,
            R.string.number,
            PATTERN_VALID_CODE,
            ""
        )
        if (streetNumberError.isNotEmpty())
            numberErrorHolder.set(streetNumberError)

        val accountOwnerError = owner.validateByDefaultPattern(
            appContext,
            R.string.account_owner,
            PATTERN_VALID_OWNER,
            PATTERN_OWNER_PREFIX_CORRECT
        )
        if (accountOwnerError.isNotEmpty())
            ownerErrorHolder.set(accountOwnerError)

        val IBANError = when {
            IBAN.isEmpty() -> getErrorEmptyString(appContext)
            !IBANValidator.validate(IBAN) -> appContext.getString(R.string.invalid_iban)
            else -> ""
        }
        if (IBANError.isNotEmpty())
            IBANErrorHolder.set(IBANError)

        if (taxId.isEmpty())
            taxIdErrorHolder.set(getErrorEmptyString(appContext))
        if (taxOffice.isEmpty())
            taxOfficeErrorHolder.set(getErrorEmptyString(appContext))

        val isCompleteUser = currentUser?.status == LEVEL_2
        if (isCompleteUser) {
            when (currentUser?.role) {
                INSTITUTIONAL_INVESTOR -> {
                    return companyError.isEmpty() && companyTypeError.isEmpty() && companyNumberError.isEmpty()
                            && genderTypeError.isEmpty() && firstNameError.isEmpty() &&
                           secondNameError.isEmpty() && birthDateError.isEmpty() &&
                           birthPlaceError.isEmpty() && nationalityError.isEmpty()
                            && countryError.isEmpty() && cityError.isEmpty() &&
                           zipCodeError.isEmpty() && streetError.isEmpty() &&
                           streetNumberError.isEmpty() && accountOwnerError.isEmpty() &&
                           IBANError.isEmpty() && (if(isGermanyTaxResident.get()) taxId.isNotEmpty() && taxOffice.isNotEmpty() else true)
                }
                PRIVATE_INVESTOR -> {
                    return genderTypeError.isEmpty() && firstNameError.isEmpty() &&
                           secondNameError.isEmpty() && birthDateError.isEmpty() &&
                           birthPlaceError.isEmpty() && nationalityError.isEmpty()
                            && countryError.isEmpty() && cityError.isEmpty() &&
                           zipCodeError.isEmpty() && streetError.isEmpty() &&
                           streetNumberError.isEmpty() && accountOwnerError.isEmpty() &&
                           IBANError.isEmpty() && (if(isGermanyTaxResident.get()) taxId.isNotEmpty() && taxOffice.isNotEmpty() else true)
                }
                else -> {
                    return companyError.isEmpty() && companyTypeError.isEmpty() && companyNumberError.isEmpty()
                            && genderTypeError.isEmpty() && firstNameError.isEmpty() &&
                           secondNameError.isEmpty() && birthDateError.isEmpty() &&
                           birthPlaceError.isEmpty() && nationalityError.isEmpty()
                            && countryError.isEmpty() && cityError.isEmpty() &&
                           zipCodeError.isEmpty() && streetError.isEmpty() &&
                           streetNumberError.isEmpty() && accountOwnerError.isEmpty() &&
                           IBANError.isEmpty() && (if(isGermanyTaxResident.get()) taxId.isNotEmpty() && taxOffice.isNotEmpty() else true)
                }
            }
        } else {
            when (currentUser?.role) {
                INSTITUTIONAL_INVESTOR -> {
                    return companyError.isEmpty() && companyTypeError.isEmpty() && companyNumberError.isEmpty()
                            && genderTypeError.isEmpty() && firstNameError.isEmpty() &&
                           secondNameError.isEmpty()
                }
                PRIVATE_INVESTOR -> {
                    return  genderTypeError.isEmpty() && firstNameError.isEmpty() && secondNameError.isEmpty()
                }
                PROJECT_INITIATOR -> {
                    return companyError.isEmpty() && companyTypeError.isEmpty() && companyNumberError.isEmpty()
                            && genderTypeError.isEmpty() && firstNameError.isEmpty() && secondNameError.isEmpty()
                }
                else -> {
                    return companyError.isEmpty() && companyTypeError.isEmpty() && companyNumberError.isEmpty()
                            && genderTypeError.isEmpty() && firstNameError.isEmpty() &&
                           secondNameError.isEmpty() && birthDateError.isEmpty() &&
                           birthPlaceError.isEmpty() && nationalityError.isEmpty()
                            && countryError.isEmpty() && cityError.isEmpty() &&
                           zipCodeError.isEmpty() && streetError.isEmpty() &&
                           streetNumberError.isEmpty() && accountOwnerError.isEmpty() &&
                           IBANError.isEmpty()
                }
            }
        }
    }

    internal fun setPoliticallyState(state: Boolean) {
        politically = state
    }

    internal fun setLiabilityState(state: Boolean) {
        liability = state
    }

    internal fun setGender(position: Int) {
        genderType = appContext.resources.getStringArray(R.array.genderType)[position]
    }

    //TODO Refactor this class by using a java 8 API (java.time.*) and move to DateUtils
    internal fun setBirthDate(date: LocalDate) {
        birthDateMillis = try {
            date.atStartOfDay(ZoneId.of("UTC")).toInstant().toEpochMilli()
        } catch (parseException: Exception) {
            System.currentTimeMillis()
        }
        val formatter = DateTimeFormatter.ofPattern(DD_p_MM_p_YYYY)
        val formattedString: String = date.format(formatter)
        birthDateField.set(formattedString)
    }

    internal fun setNationality(position: Int) {
        nationality = appContext.getSortedCountries().getOrElse(position) { "" }
    }

    internal fun setCountry(position: Int) {
        country = appContext.getSortedCountries().getOrElse(position) { "" }
    }

    fun setCompanyType(position: Int) {
        companyType =
            appContext.resources.getStringArray(R.array.companyType).getOrElse(position) { "" }
    }

    internal fun getUserSexArrayIndex(user: User): Int {
        val prefixesArray = appContext.resources.getStringArray(R.array.genderType)
        val index = prefixesArray.indexOf(User.PrefixType.toDisplayValue(appContext, user.prefix))
        return if (index < 0) 0 else index
    }

    internal fun getUserNationalityArrayIndex(user: User): Int {
        val lowerCasedArray = appContext.resources.getStringArray(R.array.countries)
            .map { it.toLowerCase(Locale.getDefault()) }
        val index = lowerCasedArray.indexOf(user.prefix.toLowerCase(Locale.getDefault()))
        return if (index < 0) 0 else index
    }

    internal fun getUserCompanyTypeArrayIndex(user: User): Int {
        val lowerCasedArray = appContext.resources.getStringArray(R.array.companyType)
            .map { it.toLowerCase(Locale.getDefault()) }
        val index = lowerCasedArray.indexOf(user.companyType.toLowerCase(Locale.getDefault()))
        return if (index < 0) 0 else index
    }

    internal fun getChurchAttrArrayIndex(user: User): Int {
        val array = TaxInformationBody.TaxChurch.getTaxChurches()
        val index = array.indexOf(user.taxInformation?.churchTaxAttribute?.let {
            TaxInformationBody.TaxChurch.toDisplayValue(it)
        })
        return if (index < 0) 0 else index
    }

    private fun setUserDataField(user: User) {

        if (user.companyName.isNotEmpty())
            companyNameField.set(user.companyName)

        if (user.commercialRegisterNumber.isNotEmpty())
            companyNumberField.set(user.commercialRegisterNumber)

        //name
        if (user.firstName.isNotEmpty())
            firstNameField.set(user.firstName)
        if (user.lastName.isNotEmpty())
            lastNameField.set(user.lastName)

        //address
        user.userAddress.let {
            if (it.city.isNotEmpty())
                cityField.set(user.userAddress.city)
            if (it.zipCode.isNotEmpty())
                zipCodeField.set(user.userAddress.zipCode)
            if (it.street.isNotEmpty())
                streetNameField.set(user.userAddress.street)
            if (it.streetNumber.isNotEmpty())
                streetNumberField.set(user.userAddress.streetNumber)
        }


        val userBirthDate = user.dateOfBirth

        if (userBirthDate.isNotEmpty()) {
            birthDateMillis = userBirthDate.parseByFormatWithoutMMSS(pattern = YYYY_MM_DD)
            birthDateField.set(birthDateMillis.formatByPattern(DD_p_MM_p_YYYY))
        }

        if (user.placeOfBirth.isNotEmpty())
            placeOfBirthField.set(user.placeOfBirth)
        if (user.accountOwner.isNotEmpty())
            accountOwnerField.set(user.accountOwner)
        if (user.iban.isNotEmpty())
            ibanField.set(user.iban)

        politicalExposedFieldsValue.set(user.politicallyExposedPerson)
        usTaxFieldsValue.set(user.usTaxLiability)

        user.taxInformation?.let {
            germanTaxInfoVisibility.set(VISIBLE)
            taxIdField.set(it.taxNumber)
            taxOfficeField.set(it.responsibleTaxOffice)
            isGermanyTaxResident.set(it.isTaxResidentInGermany)
            isTaxLiability.set(it.churchTaxLiability)
        }

        val isCompleteUser = currentUser?.status == LEVEL_2
        if (isCompleteUser) {
            when (currentUser?.role) {
                INSTITUTIONAL_INVESTOR -> {
                    companyFieldsVisibility.set(VISIBLE)
                    personalBaseFieldsVisibility.set(VISIBLE)
                    dateOfBirthFieldVisibility.set(VISIBLE)
                    placeOfBirthFieldVisibility.set(VISIBLE)
                    nationalityFieldVisibility.set(VISIBLE)
                    addressFieldsVisibility.set(VISIBLE)
                    financialFieldsVisibility.set(VISIBLE)
                    otherButtonsVisibility.set(VISIBLE)
                }
                PRIVATE_INVESTOR -> {
                    companyFieldsVisibility.set(GONE)
                    personalBaseFieldsVisibility.set(VISIBLE)
                    dateOfBirthFieldVisibility.set(VISIBLE)
                    placeOfBirthFieldVisibility.set(VISIBLE)
                    nationalityFieldVisibility.set(VISIBLE)
                    addressFieldsVisibility.set(VISIBLE)
                    financialFieldsVisibility.set(VISIBLE)
                    otherButtonsVisibility.set(VISIBLE)
                }
                else -> {
                    companyFieldsVisibility.set(GONE)
                    personalBaseFieldsVisibility.set(GONE)
                    dateOfBirthFieldVisibility.set(GONE)
                    placeOfBirthFieldVisibility.set(GONE)
                    nationalityFieldVisibility.set(GONE)
                    addressFieldsVisibility.set(GONE)
                    financialFieldsVisibility.set(GONE)
                    otherButtonsVisibility.set(GONE)
                }
            }
        } else {
            when (currentUser?.role) {
                INSTITUTIONAL_INVESTOR -> {
                    companyFieldsVisibility.set(VISIBLE)
                    personalBaseFieldsVisibility.set(VISIBLE)
                    dateOfBirthFieldVisibility.set(GONE)
                    placeOfBirthFieldVisibility.set(GONE)
                    nationalityFieldVisibility.set(GONE)
                    addressFieldsVisibility.set(GONE)
                    financialFieldsVisibility.set(GONE)
                    otherButtonsVisibility.set(GONE)
                }
                PRIVATE_INVESTOR -> {
                    companyFieldsVisibility.set(GONE)
                    personalBaseFieldsVisibility.set(VISIBLE)
                    dateOfBirthFieldVisibility.set(GONE)
                    placeOfBirthFieldVisibility.set(GONE)
                    nationalityFieldVisibility.set(GONE)
                    addressFieldsVisibility.set(GONE)
                    financialFieldsVisibility.set(GONE)
                    otherButtonsVisibility.set(GONE)
                }
                PROJECT_INITIATOR -> {
                    companyFieldsVisibility.set(VISIBLE)
                    personalBaseFieldsVisibility.set(VISIBLE)
                    dateOfBirthFieldVisibility.set(GONE)
                    placeOfBirthFieldVisibility.set(GONE)
                    nationalityFieldVisibility.set(GONE)
                    addressFieldsVisibility.set(GONE)
                    financialFieldsVisibility.set(GONE)
                    otherButtonsVisibility.set(GONE)
                }
                else -> {
                    companyFieldsVisibility.set(GONE)
                    personalBaseFieldsVisibility.set(GONE)
                    dateOfBirthFieldVisibility.set(GONE)
                    placeOfBirthFieldVisibility.set(GONE)
                    nationalityFieldVisibility.set(GONE)
                    addressFieldsVisibility.set(GONE)
                    financialFieldsVisibility.set(GONE)
                    otherButtonsVisibility.set(GONE)
                }
            }
        }

        confirmButtonNameField.set(
            if (user.status == LEVEL_2)
                appContext.getString(R.string.send_request)
            else
                appContext.getString(R.string.apply_changes)
        )
    }

    private val userMeResult = SingleActionLiveData<Result<User>>()
    internal fun getUserMeResult(): LiveData<Result<User>> = userMeResult

    internal fun getUser() {
        viewModelScope.launch {

            val result = asyncLoading {
                userModel.getUserData()
            }

            if (result.status == SUCCESS) {
                result.result?.let { user ->
                    currentUser = user
                    setUserDataField(user)
                }
            }

            userMeResult.postAction(result)
        }
    }

    private fun getUpdateRequestBody(user: User): UpdateUserPersonalDataRequestBody {
        val isCompleteUser = user.status == LEVEL_2
        if (isCompleteUser) {
            when (currentUser?.role) {
                INSTITUTIONAL_INVESTOR -> {
                    return UpdateUserPersonalDataRequestBody(
                        companyName = companyName,
                        companyType = CompanyDataDTO.CompanyType.fromDisplayValue(companyType)
                            .toString(),
                        commercialRegisterNumber = companyNumber,
                        prefix = User.PrefixType.fromDisplayValue(appContext, genderType).toString(),
                        firstName = firstName,
                        lastName = lastName,
                        dateOfBirth = birthDateMillis.formatByPattern(Constants.DD_MM_YYYY_HH_MM_SS_ZONE),
                        placeOfBirth = birthPlace,
                        nationality = Country.fromDisplayValue(appContext, nationality).toString(),
                        address = UserAddress(
                            country = Country.fromDisplayValue(appContext, country).toString(),
                            city = city,
                            street = street,
                            streetNumber = streetNumber,
                            zipCode = zipCode
                        ),
                        accountOwner = owner,
                        iban = IBAN,
                        politicallyExposedPerson = politically,
                        usTaxLiability = liability,
                        taxInformation = createGermanTaxInfo()
                    )
                }
                PRIVATE_INVESTOR -> {
                    return UpdateUserPersonalDataRequestBody(
                        prefix = User.PrefixType.fromDisplayValue(appContext, genderType).toString(),
                        firstName = firstName,
                        lastName = lastName,
                        dateOfBirth = birthDateMillis.formatByPattern(Constants.DD_MM_YYYY_HH_MM_SS_ZONE),
                        placeOfBirth = birthPlace,
                        nationality = Country.fromDisplayValue(appContext, nationality).toString(),
                        address = UserAddress(
                            country = Country.fromDisplayValue(appContext, country).toString(),
                            city = city,
                            street = street,
                            streetNumber = streetNumber,
                            zipCode = zipCode
                        ),
                        accountOwner = owner,
                        iban = IBAN,
                        politicallyExposedPerson = politically,
                        usTaxLiability = liability,
                        taxInformation = createGermanTaxInfo()
                    )
                }
                else -> {
                    return UpdateUserPersonalDataRequestBody()
                }
            }
        } else {
            when (currentUser?.role) {
                INSTITUTIONAL_INVESTOR -> {
                    return UpdateUserPersonalDataRequestBody(
                        companyName = companyName,
                        commercialRegisterNumber = companyNumber,
                        companyType = CompanyDataDTO.CompanyType.fromDisplayValue(companyType)
                            .toString(),
                        prefix = User.PrefixType.fromDisplayValue(appContext, genderType).toString(),
                        firstName = firstName,
                        lastName = lastName
                    )
                }
                PRIVATE_INVESTOR -> {
                    return UpdateUserPersonalDataRequestBody(
                        prefix = User.PrefixType.fromDisplayValue(appContext, genderType).toString(),
                        firstName = firstName,
                        lastName = lastName
                    )
                }
                PROJECT_INITIATOR -> {
                    return UpdateUserPersonalDataRequestBody(
                        companyName = companyName,
                        commercialRegisterNumber = companyNumber,
                        companyType = CompanyDataDTO.CompanyType.fromDisplayValue(companyType)
                            .toString(),
                        prefix = User.PrefixType.fromDisplayValue(appContext, genderType).toString(),
                        firstName = firstName,
                        lastName = lastName
                    )
                }
                else -> {
                    return UpdateUserPersonalDataRequestBody()
                }
            }
        }
    }

    internal fun updateProfileData() {
        currentUser?.let {
            val body = getUpdateRequestBody(it)
            updateUserMe(body = body)
        }
    }

    /**
     * Action that returning the status of the user that is
     * a mark to determine should we show an additional info popup or not
     */
    private val updateUserMeResult = SingleActionLiveData<Result<Boolean>>()
    internal fun getUpdateUserMeResult(): LiveData<Result<Boolean>> = updateUserMeResult

    private fun updateUserMe(body: UpdateUserPersonalDataRequestBody) {
        viewModelScope.launch {

            val result = asyncLoading {
                userModel.updateUserProfileData(body)
            }

            val isNeedWait = currentUser?.let {
                it.status != LEVEL_1
            }.orDefaultValue(false)

            updateUserMeResult.postAction(result.wrapWithAnotherResult(isNeedWait))
        }
    }

    private fun createGermanTaxInfo() = if (isGermanyTaxResident.get()) TaxInformationBody(
        _churchTaxAttribute = fromDisplayValue(taxChurch).toString(),
        _churchTaxLiability = isTaxLiability.get(),
        _isTaxResidentInGermany = isGermanyTaxResident.get(),
        _responsibleTaxOffice = taxOffice,
        _taxNumber = taxId
    ) else
        TaxInformationBody()

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        currentUser = null

        liability = false
        politically = false

        genderType = ""

        ibanField.set("")
        cityField.set("")
        zipCodeField.set("")
        lastNameField.set("")
        firstNameField.set("")
        birthDateField.set("")
        streetNameField.set("")
        companyNameField.set("")
        companyNumberField.set("")
        streetNumberField.set("")
        accountOwnerField.set("")
        placeOfBirthField.set("")
        confirmButtonNameField.set("")

        usTaxFieldsValue.set(false)
        politicalExposedFieldsValue.set(false)

        otherButtonsVisibility.set(GONE)
        companyFieldsVisibility.set(GONE)
        addressFieldsVisibility.set(GONE)
        financialFieldsVisibility.set(GONE)
        nationalityFieldVisibility.set(GONE)
        dateOfBirthFieldVisibility.set(GONE)
        placeOfBirthFieldVisibility.set(GONE)
        personalBaseFieldsVisibility.set(GONE)

        //German tax
        taxId = ""
        taxChurch = ""
        taxOffice = ""
        isGermanyTaxResident.set(false)
        isTaxLiability.set(false)
        germanTaxInfoVisibility.set(GONE)
        //endregion
    }
}