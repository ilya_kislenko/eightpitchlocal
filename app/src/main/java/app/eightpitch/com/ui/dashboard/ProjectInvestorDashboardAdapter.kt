package app.eightpitch.com.ui.dashboard

import android.content.Context
import android.graphics.PorterDuff
import android.view.*
import androidx.annotation.ColorRes
import androidx.appcompat.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.graphics.ColorUtils
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.R
import app.eightpitch.com.data.dto.project.ProjectInvestment.Companion.CONFIRMED
import app.eightpitch.com.data.dto.project.ProjectInvestment.Companion.PAID
import app.eightpitch.com.data.dto.project.ProjectInvestment.Companion.PENDING
import app.eightpitch.com.data.dto.project.ProjectInvestment.Companion.RECEIVED
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.projects.adapters.ProjectItem
import app.eightpitch.com.ui.recycleradapters.BindableRecyclerAdapter
import app.eightpitch.com.utils.Constants.FILES_ENDPOINT
import app.eightpitch.com.views.HorizontalProgressBarView
import com.bumptech.glide.load.resource.bitmap.*
import java.math.BigDecimal
import java.util.*

class ProjectInvestorDashboardAdapter :
    BindableRecyclerAdapter<ProjectItem, ProjectInvestorDashboardAdapter.PagedProjectViewHolder>() {

    /**
     * Listener that represents a click on any project in the list,
     * will pass a [ProjectItem] when click is performed
     */
    internal lateinit var onProjectClicked: (ProjectItem) -> (Unit)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PagedProjectViewHolder(
        inflateView(
            parent,
            R.layout.item_view_investor_dashboard_project
        )
    )

    override fun getItemId(position: Int): Long {
        return getDataItem(position).id.toLong()
    }

    override fun onBindViewHolder(holder: PagedProjectViewHolder, position: Int) {

        val projectItem = getDataItem(position)

        holder.apply {
            val context = itemView.context
            context.loadImage(
                "$FILES_ENDPOINT${projectItem.thumbNailImageUrl}",
                targetImageView = videoPreView,
                placeholderRef = R.drawable.icon_app_name_logo,
                transformation = arrayOf(
                    CenterCrop(),
                    GranularRoundedCorners(0F,
                        0F,
                        context.resources.getDimension(R.dimen.margin_10),
                        context.resources.getDimension(R.dimen.margin_10))
                )
            )
            val nominalValue: BigDecimal = projectItem.run {
                tokenParametersDocument.run {
                    if (nominalValue != BigDecimal.ZERO) nominalValue
                    else financialInformation.nominalValue.parseToBigDecimalSafe()
                }

            }
            projectItem.run {
                setupProgressBarView(this, holder.fundingProgressBar)
                typeOfSecurityTextView.text = financialInformation.typeOfSecurity
                nominalValueTextView.text = String.format(
                    context.getString(R.string.token_placeholder),
                    nominalValue.toLong().asCurrency()
                )

                investmentAmountTextView.text = String.format(
                    context.getString(R.string.token_placeholder),
                    monetaryAmount.multiplySafe(nominalValue).toLong().asCurrency()
                )
                tokensAmountTextView.text = String.format(
                    context.getString(R.string.token_placeholder),
                    monetaryAmount.toLong().asCurrency()
                )
                describeProjectTextView.text = description
                headerProjectTextView.text = companyName
                investmentStatusTextView.setupStatus(context, investmentStatus)
            }

            parentView.apply {
                tag = projectItem
                setOnClickListener {
                    (it.tag as? ProjectItem)?.let(onProjectClicked)
                }
            }
        }
    }

    private fun AppCompatTextView.setupStatus(context: Context, investmentStatus: String) {
        when (investmentStatus) {
            CONFIRMED, PENDING -> {
                text = context.getString(R.string.awaiting_payment_text)
                setColorFilter(R.color.lightRed)
                setDrawableLeft(R.drawable.icon_red_bulb)
            }
            PAID -> {
                text = context.getString(R.string.processing_payment_text)
                setColorFilter(R.color.gray20)
                setDrawableLeft(R.drawable.icon_checkbox_unchecked_background)
            }
            RECEIVED -> {
                text = context.getString(R.string.payment_received_text)
                setColorFilter(R.color.green)
                setDrawableLeft(R.drawable.icon_green_bulb)
            }
            else -> {
            }
        }
    }

    private fun AppCompatTextView.setColorFilter(@ColorRes color: Int) {
        background?.setColorFilter(
            ColorUtils.setAlphaComponent(
                context.resources.getColor(color),
                10
            ),
            PorterDuff.Mode.SRC
        )
    }

    private fun setupProgressBarView(
        projectItem: ProjectItem,
        fundingProgressBar: HorizontalProgressBarView
    ) {

        val currentFundingSum = projectItem.graph.currentFundingSum
        fundingProgressBar.run {
            setCurrentProgress(currentFundingSum)
            setCurrentTextProgress(
                String.format(
                    Locale.getDefault(),
                    context.getString(R.string.percent_string),
                    currentFundingSum
                )
            )
        }

        val context = fundingProgressBar.context
        val softCap = projectItem.graph.softCap
        val (cap, textProgress) = if (softCap == null || softCap == 0) {
            projectItem.graph.hardCap to context.getString(R.string.hard_cap)
        } else {
            softCap.orDefaultValue(0) to context.getString(R.string.soft_cap)
        }

        fundingProgressBar.run {
            setCapProgress(cap)
            setCapTextProgress(textProgress)
        }
    }

    class PagedProjectViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val fundingProgressBar: HorizontalProgressBarView =
            itemView.findViewById(R.id.fundingProgressBar)
        val parentView: ConstraintLayout = itemView.findViewById(R.id.parentView)
        val videoPreView: AppCompatImageView = itemView.findViewById(R.id.projectPreviewImage)
        val typeOfSecurityTextView: AppCompatTextView =
            itemView.findViewById(R.id.typeOfSecurityTextView)
        val nominalValueTextView: AppCompatTextView =
            itemView.findViewById(R.id.nominalValueTextView)
        val investmentAmountTextView: AppCompatTextView =
            itemView.findViewById(R.id.investmentAmountTextView)
        val tokensAmountTextView: AppCompatTextView =
            itemView.findViewById(R.id.tokensAmountTextView)
        val describeProjectTextView: AppCompatTextView =
            itemView.findViewById(R.id.describeProjectTextView)
        val headerProjectTextView: AppCompatTextView =
            itemView.findViewById(R.id.headerProjectTextView)
        val investmentStatusTextView: AppCompatTextView =
            itemView.findViewById(R.id.investmentStatus)
    }
}