package app.eightpitch.com.ui.questionnaire.investorqualification

import android.content.Context
import android.net.Uri
import androidx.databinding.*
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.files.*
import app.eightpitch.com.data.dto.questionnaire.InvestorQualificationRequestBody
import app.eightpitch.com.data.dto.questionnaire.InvestorQualificationRequestBody.InvestorQualification.Companion.fromDisplayValue
import app.eightpitch.com.data.dto.questionnaire.InvestorQualificationRequestBody.InvestorQualification.Companion.isUnQualified
import app.eightpitch.com.data.transmit.*
import app.eightpitch.com.extensions.wrapWithAnEmptyResult
import app.eightpitch.com.models.*
import app.eightpitch.com.utils.*
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class InvestorQualificationViewModel(
    private val appContext: Context,
    private val filesModel: FilesModel,
    private val userModel: UserModel,
    threadsSeparator: ThreadsSeparator,
) : BaseViewModel(threadsSeparator) {

    var toolbarTitle = appContext.getString(R.string.questionnaire_title_pattern, 1)

    val uploadedFiles = ObservableArrayList<FileMetaData>()

    val fileUploaderVisibility = ObservableBoolean()
    val descriptionVisibility = ObservableBoolean()

    private val fileIds = arrayListOf<String>()
    private var investorQualification =
        appContext.resources.getStringArray(R.array.investorQualificationStatuses).firstOrNull().orEmpty()

    internal fun setQualification(position: Int) {
        val investorQualifications = appContext.resources.getStringArray(R.array.investorQualificationStatuses)
        investorQualification = investorQualifications.getOrElse(position) { "" }
        when (position) {
            0 -> {
                fileUploaderVisibility.set(false)
                descriptionVisibility.set(false)
            }
            else -> {
                fileUploaderVisibility.set(true)
                descriptionVisibility.set(true)
            }
        }
    }

    internal fun validateFields(description: String): Boolean {
        return when {
            isUnQualified(appContext, investorQualification) -> {
                sendQuestionnaireForm(InvestorQualificationRequestBody(
                    investorClass = fromDisplayValue(appContext, investorQualification).toString()
                ))
                true
            }

            description.isNotEmpty() && fileIds.isNotEmpty() -> {
                sendQuestionnaireForm(InvestorQualificationRequestBody(
                    investorClass = fromDisplayValue(appContext, investorQualification).toString(),
                    description = description,
                    fileIds = fileIds
                ))
                true
            }
            else -> false
        }
    }

    private val questionnaireFormResult = SingleActionLiveData<Result<Empty>>()
    internal fun getQuestionnaireFormResult(): LiveData<Result<Empty>> = questionnaireFormResult

    private fun sendQuestionnaireForm(investorQualificationRequestBody: InvestorQualificationRequestBody) {
        viewModelScope.launch {
            val result = asyncLoading {
                userModel.sendQualificationForm(investorQualificationRequestBody)
            }
            questionnaireFormResult.postAction(result)
        }
    }

    private val uploadFileResult = SingleActionLiveData<Result<Empty>>()
    internal fun getUploadFileResult(): LiveData<Result<Empty>> = uploadFileResult

    //TODO refactor
    internal fun uploadFile(uri: Uri) {
        viewModelScope.launch {
            val result = asyncLoading {
                val handler = CoroutineExceptionHandler { _, exception ->
                    throw exception
                }
                withContext(handler) {
                    try {
                        val metaData = filesModel.getFileMetaDataByUri(uri)
                        val fileCreationResponse = filesModel.uploadFile(uri, metaData)
                        uploadedFiles.add(0, metaData)
                        fileIds.add(fileCreationResponse.fileId)
                    } catch (e: Exception) {
                        throw e
                    }
                }
            }

            uploadFileResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    private val deleteFileResult = SingleActionLiveData<Result<Empty>>()
    internal fun getDeleteFileResult(): LiveData<Result<Empty>> = deleteFileResult

    internal fun deleteFile(position: Int) {
        viewModelScope.launch {
            val result = asyncLoading {
                val id = fileIds[position]
                fileIds.removeAt(position)
                filesModel.deleteFile(id)
            }

            //TODO does not work.Has not time
            //deleteFileResult.postAction(result)
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        fileIds.clear()
        uploadedFiles.clear()
    }
}