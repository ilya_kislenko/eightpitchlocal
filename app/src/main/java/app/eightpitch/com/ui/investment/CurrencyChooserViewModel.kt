package app.eightpitch.com.ui.investment

import android.content.Context
import android.view.View
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.projectdetails.GetProjectResponse
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.models.ProjectModel
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.coroutines.launch

class CurrencyChooserViewModel(
    appContext: Context,
    private val projectModel: ProjectModel,
    threadsSeparator: ThreadsSeparator,
) : BaseViewModel(threadsSeparator) {

    val toolbarTitle = appContext.getString(R.string.invest_title_text)

    val toolbarRightButtonVisibility = ObservableField(View.GONE)

    internal fun updateVisibilityRightButton(isVisible: Boolean) {
        toolbarRightButtonVisibility.set(
            if (isVisible)
                View.VISIBLE
            else
                View.GONE
        )
    }

    /**
     * LiveData to inform [CurrencyChooserFragment] about the
     * currency in [InputBghFragment] or [InputEuroFragment] was
     * changed to a correct/un-correct values to affect the UI
     */
    private val currencyInputValidatorObserver = SingleActionLiveData<Boolean>()
    internal fun getCurrencyInputValidatorObserver() = currencyInputValidatorObserver

    private val projectResult = SingleActionLiveData<Result<GetProjectResponse>>()
    internal fun getProjectResult(): LiveData<Result<GetProjectResponse>> = projectResult

    internal fun getProjectInfo(projectId: String) {
        viewModelScope.launch {
            val result = asyncLoading {
                projectModel.getProjectInfo(projectId)
            }
            projectResult.postAction(result)
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        currencyInputValidatorObserver.sendAction(true)
    }
}