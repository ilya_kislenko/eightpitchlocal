package app.eightpitch.com.ui.authorized.main.homepager

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import app.eightpitch.com.R
import app.eightpitch.com.base.rootbackhandlers.RootNavGraphHandleAbleFragment
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.extensions.hide
import app.eightpitch.com.extensions.injectViewModel
import app.eightpitch.com.extensions.moveToStartDestination
import app.eightpitch.com.extensions.navigate
import app.eightpitch.com.extensions.popBackStack
import app.eightpitch.com.extensions.show

class DashboardRootFragment : RootNavGraphHandleAbleFragment(), NavigationApplier {

    private lateinit var viewModel: DashboardRootViewModel

    override fun getNavigationHostFragmentId() = R.id.navigation_dashboard_fragment_host

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = injectViewModel(DashboardRootViewModel::class.java)
        return inflater.inflate(R.layout.fragment_root_dashboard_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindLoadingIndicator()
        viewModel.getDataResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) { (user, hasExperience) ->
                navigateForward(user, hasExperience)
            }
        })
        viewModel.retrieveData()
    }

    private fun navigateForward(user: User, hasExperience: Boolean) {
        val navHostFragment = getNavHostFragment() ?: return
        val graph = navHostFragment.navController.navInflater.inflate(R.navigation.dashboard_navigation_graph).apply {
            startDestination = when {
                user.isInvestor -> if (hasExperience) R.id.investmentStatisticFragment else R.id.investorWelcomeFragment
                else -> if (hasExperience) R.id.initiatorStatisticFragment else R.id.initiatorWelcomeFragment
            }
        }
        navHostFragment.navController.graph = graph
    }

    private fun getNavHostFragment() =
        childFragmentManager.findFragmentById(getNavigationHostFragmentId()) as? NavHostFragment

    //region UI blocking
    private fun bindLoadingIndicator() {
        viewModel.getLoadingState().observe(viewLifecycleOwner, { isLoading ->
            activity?.runOnUiThread {
                if (isLoading)
                    blockUI()
                else
                    unblockUI()
            }
        })
    }

    private fun blockUI() {
        view?.findViewById<FrameLayout>(R.id.curtainView)?.show()
    }

    private fun unblockUI() {
        view?.findViewById<FrameLayout>(R.id.curtainView)?.hide()
    }

    override fun applyNavigationState(navigationId: Int) {
        //TODO do nothing for now.
    }

    override fun popUpToStartDestination() {
        moveToStartDestination(getNavigationHostFragmentId())
    }
    //endregion
}