package app.eightpitch.com.ui.kyc.emailconfirmed

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.eightpitch.com.R
import app.eightpitch.com.base.BackPressureFragment
import app.eightpitch.com.extensions.finishActivity
import app.eightpitch.com.extensions.navigate
import kotlinx.android.synthetic.main.fragment_email_confirmed.*

class EmailConfirmedFragment : BackPressureFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_email_confirmed, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        registerOnBackPressedListener { finishActivity() }

        crossButton.setOnClickListener {
            finishActivity()
        }

        nextToKYCButton.setOnClickListener {
            navigate(R.id.action_emailConfirmedFragment_to_kycAddExtraSecurityFragment)
        }
    }
}