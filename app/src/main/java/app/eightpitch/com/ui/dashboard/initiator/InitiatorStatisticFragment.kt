package app.eightpitch.com.ui.dashboard.initiator

import android.graphics.drawable.*
import android.graphics.drawable.GradientDrawable.RECTANGLE
import android.os.*
import android.view.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.user.LinkSliderItemResponseBody
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.dashboard.*
import app.eightpitch.com.ui.dashboard.initiator.details.InitiatorDetailsFragment
import app.eightpitch.com.ui.projectsdetail.ProjectDetailsFragment
import app.eightpitch.com.utils.SafeLinearLayoutManager
import app.eightpitch.com.utils.Timer
import com.google.android.material.tabs.*
import kotlinx.android.synthetic.main.fragment_initiator_statistic.*

class InitiatorStatisticFragment : BaseFragment<InitiatorStatisticViewModel>() {

    private val timer = Timer()

    override fun getLayoutID() = R.layout.fragment_initiator_statistic

    override fun getVMClass() = InitiatorStatisticViewModel::class.java

    override fun onResume() {
        super.onResume()
        viewModel.retrieveData()
        timer.startTimer(8)
    }

    override fun onPause() {
        super.onPause()
        timer.stopTimer()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        timer.timerListener = TimerListener()

        viewModel.tabLayoutVisibility.onChangedSafe(viewLifecycleOwner, { visible ->
            if (visible) {
                projectChangeModeTabLayout.addOnTabSelectedListener(OnProjectsTabListener())
            } else {
                projectChangeModeTabLayout.clearOnTabSelectedListeners()
            }
        })

        viewModel.getInitiatorResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) {
                projectChangeModeTabLayout.selectTabWith(viewModel.savedTabPosition)
            }
        })

        projectsRecycler.apply {
            layoutManager = SafeLinearLayoutManager(context, VERTICAL, false)
            adapter = ProjectInitiatorDashboardAdapter().apply {
                onProjectClicked = { projectItem ->
                    val arguments = ProjectDetailsFragment.buildWithAnArguments(projectItem.id)
                    navigate(R.id.action_initiatorStatisticFragment_to_projectDetailsFragment, arguments)
                }
                onDetailsClicked = { projectItem ->
                    val arguments = InitiatorDetailsFragment.buildWithAnArguments(projectItem)
                    navigate(R.id.action_initiatorStatisticFragment_to_initiatorDetailsFragment, arguments)
                }
                setHasStableIds(true)
                setHasFixedSize(true)
            }
        }

        viewModel.getSliderResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) { listSliders ->
                setupSlider(listSliders)
                startSwap()
            }
        })

        projectChangeModeTabLayout.addOnTabSelectedListener(OnProjectsTabListener())
    }

    private fun setupSlider(listSliders: List<LinkSliderItemResponseBody>) {
        val list = arrayListOf(LinkSliderItemResponseBody(header = viewModel.welcomeText.get() ?: "")).apply {
            addAll(listSliders)
        }.toList()

        val fragments = arrayListOf<Fragment>()

        for (i in list.indices) {
            tabLayout.apply { addTab(newTab()) }
            fragments.add(InitiatorDashboardSliderFragment.createFragmentWithArguments(list[i]))
        }

        sliderView.adapter = DashboardSliderAdapter(this@InitiatorStatisticFragment, fragments)

        if (list.size == 1)
            return

        TabLayoutMediator(tabLayout, sliderView) { tab, _ ->
            tab.customView = createSliderView()
        }.attach()
    }

    private fun createSliderView() = View(context).apply {
        val (width, height) = resources.run {
            getDimensionPixelOffset(R.dimen.tabLineWidth) to getDimensionPixelOffset(R.dimen.tabLineHeight)
        }
        layoutParams = ViewGroup.LayoutParams(width, height)
        background = StateListDrawable().apply {
            context.also {
                addState(intArrayOf(android.R.attr.state_selected),
                    it.createGradientDrawable(RECTANGLE, R.color.lightRed))
                addState(intArrayOf(),
                    it.createGradientDrawable(RECTANGLE, R.color.white))
            }
        }
    }

    private inner class OnProjectsTabListener : TabLayout.OnTabSelectedListener {

        override fun onTabReselected(tab: TabLayout.Tab?) {
            loadProjectsWithType(tab)
        }

        override fun onTabUnselected(tab: TabLayout.Tab?) {
            /*Do nothing*/
        }

        override fun onTabSelected(tab: TabLayout.Tab?) {
            loadProjectsWithType(tab)
        }
    }

    private fun loadProjectsWithType(selectedTab: TabLayout.Tab?) {
        selectedTab?.let { viewModel.getProjectsForType(it.position) }
    }


    private inner class TimerListener : Timer.TimerListener {

        override fun countProcessing(value: Int) {
            //Nothing.
        }

        override fun countFinished() {
            if (isAliveAndAvailable()) {
                sliderView.selectNextItem()
                timer.startTimer(8)
            }
        }
    }

    private fun startSwap() {
        if (sliderView.adapter != null)
            timer.startTimer(8)
    }
}