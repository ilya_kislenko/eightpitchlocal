package app.eightpitch.com.ui

import android.content.Intent
import android.content.Intent.ACTION_VIEW
import android.net.Uri
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.investment.CurrencyInputRequestBody
import app.eightpitch.com.data.dto.investment.ProjectInvestmentInfoResponse
import app.eightpitch.com.data.dto.projectdetails.ProjectsFile
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.extensions.toArrayList
import app.eightpitch.com.extensions.toHtml
import app.eightpitch.com.ui.investment.ClickableFilesAdapter
import app.eightpitch.com.ui.investment.choosedocuments.ProjectDocumentsFragment
import app.eightpitch.com.utils.SafeLinearLayoutManager
import kotlinx.android.synthetic.main.fragment_pre_contractual_info.*
import java.math.BigDecimal
import java.util.ArrayList

class PreContractualInfoFragment : BaseFragment<PreContractualViewModel>() {

    companion object {

        const val PROJECT_ID = "PROJECT_ID"
        const val PROJECT_INVESTMENT = "PROJECT_INVESTMENT"
        const val PROJECT_FILES = "PROJECT_FILES"
        const val AMOUNT_KEY = "AMOUNT_KEY"

        fun buildWithAnArguments(
            projectId: String,
            projectInvestmentInfo: ProjectInvestmentInfoResponse,
            projectFiles: ArrayList<ProjectsFile>,
            amount: BigDecimal
        ) = Bundle().apply {
            putString(PROJECT_ID, projectId)
            putParcelable(PROJECT_INVESTMENT, projectInvestmentInfo)
            putParcelableArrayList(PROJECT_FILES, projectFiles)
            putSerializable(AMOUNT_KEY, amount)
        }
    }

    private val projectId: String
        get() = arguments?.getString(PROJECT_ID, "") ?: ""

    private val projectInvestmentInfo: ProjectInvestmentInfoResponse
        get() = arguments?.getParcelable(PROJECT_INVESTMENT) ?: ProjectInvestmentInfoResponse()

    private val projectFiles: List<ProjectsFile>
        get() = arguments?.getParcelableArrayList(PROJECT_FILES) ?: listOf()

    private val amount: BigDecimal
        get() = arguments?.getSerializable(AMOUNT_KEY) as BigDecimal? ?: BigDecimal.ZERO

    override fun getLayoutID() = R.layout.fragment_pre_contractual_info

    override fun getVMClass() = PreContractualViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        viewModel.run {
            setupFields(projectInvestmentInfo)
            setupFiles(projectFiles.map {
                it.projectsFile.run { originalFileName to uniqueFileName }
            })
        }

        filesRecycler.apply {
            adapter = ClickableFilesAdapter().apply {
                filesUrlCallback = { startActivity(Intent(ACTION_VIEW, Uri.parse(it))) }
            }
            layoutManager = SafeLinearLayoutManager(context, VERTICAL, false)
        }

        firstCheckBox.setOnCheckedChangeListener { _, isChecked ->
            viewModel.changeAvailability(isChecked && secondCheckBox.isChecked)
        }
        secondCheckBox.setOnCheckedChangeListener { _, isChecked ->
            viewModel.changeAvailability(isChecked && firstCheckBox.isChecked)
        }

        viewModel.getBookInvestmentResult().observe(viewLifecycleOwner, Observer {
            handleResult(it) { bookInvestment ->
                val arguments = ProjectDocumentsFragment.buildWithAnArguments(
                    projectId,
                    bookInvestment.investmentId,
                    projectInvestmentInfo,
                    projectFiles.toArrayList())
                navigate(R.id.action_preContractualInfoFragment_to_projectDocumentsFragment, arguments)
            }
        })

        with(firstCheckBox) {
            text = resources.getString(R.string.i_hereby_agree_to_the_data_protection_declaration_text).toHtml()
            movementMethod = LinkMovementMethod.getInstance()
        }

        confirmButton.setDisablingClickListener {
            viewModel.bookInvestment(projectId, CurrencyInputRequestBody(amount))
        }
    }
}