package app.eightpitch.com.ui.settings

import android.content.Context
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.data.dto.user.User.CREATOR.LEVEL_2
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.data.transmit.ResultStatus.SUCCESS
import app.eightpitch.com.extensions.wrapWithAnEmptyResult
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.utils.Empty
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.coroutines.launch
import javax.inject.Inject

class SettingsFragmentViewModel @Inject constructor(
    private val appContext: Context,
    private val userModel: UserModel,

    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {


    val title = appContext.getString(R.string.settings_title)

    val upgradeButtonVisibility = ObservableField<Int>(GONE)
    val subscriptionFields = ObservableField<Boolean>(false)
    val accountStatus = ObservableField<String>("")

    var userStatus = ""

    internal fun setSubscription(isSubscribe: Boolean){
        subscriptionFields.set(isSubscribe)
    }

    private val userMeResult = SingleActionLiveData<Result<Empty>>()
    internal fun getUserMeResult(): LiveData<Result<Empty>> = userMeResult

    internal fun getUser() {
        viewModelScope.launch {

            val result = asyncLoading {
                userModel.getUserData()
            }

            if (result.status == SUCCESS) {
                setProfileFields(result.result!!)
            }

            userMeResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    private fun setProfileFields(user: User) {
        userStatus = user.status

        val isCompleteUser = user.status == LEVEL_2
        upgradeButtonVisibility.set(if (user.canUpgradeAccount()) VISIBLE else GONE)

        subscriptionFields.set(user.isSubscribedToEmails)

        accountStatus.set(
            if (isCompleteUser)
                appContext.getString(R.string.two_level)
            else
                appContext.getString(R.string.one_level)
        )
    }

    private val emailSubscriptionResult = SingleActionLiveData<Result<Empty>>()
    internal fun getEmailSubscriptionResult(): LiveData<Result<Empty>> = emailSubscriptionResult

    internal fun updateEmailSubscription(isSubscribe: Boolean) {
        viewModelScope.launch {

            val result = asyncLoading {
                userModel.updateEmailSubscription(isSubscribe)
            }
            emailSubscriptionResult.postAction(result)
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        userStatus = ""
        accountStatus.set("")
        subscriptionFields.set(false)
        upgradeButtonVisibility.set(GONE)
    }
}