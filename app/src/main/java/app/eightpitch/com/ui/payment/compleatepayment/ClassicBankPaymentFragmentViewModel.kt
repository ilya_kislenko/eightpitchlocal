package app.eightpitch.com.ui.payment.compleatepayment

import android.content.Context
import android.text.Spanned
import android.view.View
import androidx.core.text.toSpanned
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.investment.ProjectInvestmentInfoResponse
import app.eightpitch.com.data.dto.payment.BankPaymentsDataResponse
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.extensions.*
import app.eightpitch.com.models.DownloadModel
import app.eightpitch.com.models.PaymentModel
import app.eightpitch.com.utils.Constants.INVOICE_PDF_FILE_NAME
import app.eightpitch.com.utils.Empty
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.coroutines.launch
import java.math.BigDecimal
import java.util.*
import javax.inject.Inject

class ClassicBankPaymentFragmentViewModel @Inject constructor(
    private val appContext: Context,
    private val paymentModel: PaymentModel,
    private val downloadModel: DownloadModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val title = ObservableField(appContext.getString(R.string.investment_payment_toolbar_title))

    val bicField = ObservableField("")
    val IBANField = ObservableField("")

    val totalSharesField = ObservableField("")
    val paymentReferenceNumberField = ObservableField("")

    val investedFeeField = ObservableField<Spanned>()
    val nominalTokenValueField = ObservableField<Spanned>()
    val investedAmountField = ObservableField("")


    val stepValueField = ObservableField("")
    val minimalAmountValueField = ObservableField("")
    val valueUserLimitField = ObservableField("")
    val valueUserLimitVisibility = ObservableField(View.GONE)
    val feeField = ObservableField<String>()
    val valueFeeField = ObservableField<String>()

    var tokenName: String = ""
    private var investmentStep = BigDecimal.ONE

    private val paymentResult = SingleActionLiveData<Result<BankPaymentsDataResponse>>()
    internal fun getPaymentDataResult(): LiveData<Result<BankPaymentsDataResponse>> = paymentResult

    internal fun getPaymentFieldsData(investmentId: String, isSoftCap: Boolean) {

        viewModelScope.launch {

            val result = asyncLoading {
                if (isSoftCap) {
                    paymentModel.getClassicSoftCapPaymentInfoBy(investmentId)
                } else {
                    paymentModel.getClassicHardCapPaymentInfoBy(investmentId)
                }
            }

            paymentResult.postAction(result)
        }
    }

    internal fun downloadInvoice(url: String) {
        downloadModel.downloadDocuments(url, INVOICE_PDF_FILE_NAME)
    }

    //region Binding setup
    internal fun setFields(bankPaymentsDataResponse: BankPaymentsDataResponse) = bankPaymentsDataResponse.run {
        bicField.set(bic)
        IBANField.set(IBAN)
        paymentReferenceNumberField.set(transactionInfo.referenceNumber)
    }

    internal fun setPaymentData(projectInvestmentInfo: ProjectInvestmentInfoResponse) = projectInvestmentInfo.run {
        tokenName = shortCut
        investmentStep = nominalValue.multiplySafe(investmentStepSize)
        setInvestmentStep(investmentStep)
        setMinimalInvestment(nominalValue.multiplySafe(minimumInvestmentAmount))
        setUserLimit(projectInvestmentInfo)
        setNominalTokenValue(investmentStep)
        val scaledFee = assetBasedFees.setScale(2)
        val tokens = unprocessedInvestment.amount
        val euroValue = tokens.multiplySafe(nominalValue)
        val countedFee = euroValue.multiplySafe(scaledFee.divideSafe(BigDecimal(100)))
        setInvestmentFee(scaledFee, countedFee)
        setInvestmentAmount(euroValue + countedFee)
        setTotalShares(tokens)
    }

    private fun setInvestmentStep(step: BigDecimal){
        val eurText = appContext.getString(R.string.eur_text)
        "${step.formatToCurrencyView()} $eurText".also { stepValueField.set(it) }
    }

    private fun setMinimalInvestment(minimalInvestment: BigDecimal) {
        val eurText = appContext.getString(R.string.eur_text)
        "${minimalInvestment.formatToCurrencyView()} $eurText".also {
            minimalAmountValueField.set(it)
        }
    }

    private fun setUserLimit(projectInvestmentInfo: ProjectInvestmentInfoResponse){
        val eurText = appContext.getString(R.string.eur_text)
        projectInvestmentInfo.investorClassLimit?.let { limit ->
            "${limit.formatToCurrencyView()} $eurText".also {
                valueUserLimitField.set(it)
                valueUserLimitVisibility.set(View.VISIBLE)
            }
        } ?: valueUserLimitVisibility.set(View.GONE)
    }

    private fun setNominalTokenValue(nominalTokenValue: BigDecimal) {
        val eurText = appContext.getString(R.string.eur_text)
        nominalTokenValueField.set(
            appContext.getString(
                R.string.nominal_token_pattern_text,
                nominalTokenValue.formatToCurrencyView(),
                eurText
            ).toHtml()
        )
    }

    private fun setInvestmentFee(assetBasedFees: BigDecimal, countedFee: BigDecimal) {
        val eurText = appContext.getString(R.string.eur_text)

        feeField.set(String.format(Locale.getDefault(),
            appContext.getString(R.string.investment_fee_text),
            assetBasedFees.formatToCurrencyView()))
        valueFeeField.set("${countedFee.setScale(2).formatToCurrencyView()} $eurText")
    }

    private fun setTotalShares(value: BigDecimal) {
        totalSharesField.set(
            appContext.getString(
                R.string.total_shares_pattern_text,
                value.setScale(2).formatToCurrencyView(),
                tokenName
            )
        )
    }

    private fun setInvestmentAmount(amountValue: BigDecimal) {
        investedAmountField.set(
            appContext.getString(
                R.string.total_amount,
                amountValue.formatToCurrencyView()
            )
        )
    }
    //endregion

    private val checkOutResult = SingleActionLiveData<Result<Empty>>()
    internal fun getCheckOutResult(): LiveData<Result<Empty>> = checkOutResult

    internal fun checkOut(investmentId: String) {
        viewModelScope.launch {
            val result = asyncLoading {
                paymentModel.confirmBankTransfer(investmentId)
            }
            checkOutResult.postAction(result)
        }
    }

    internal fun copyIban(text: String) {
        copyToClipboard(text)
    }

    internal fun copyBic(text: String) {
        copyToClipboard(text)
    }

    internal fun copyPaymentReference(text: String) {
        copyToClipboard(text)
    }

    private fun copyToClipboard(text: String) {
        appContext.copyToClipboard(text)
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        bicField.set("")
        IBANField.set("")
        paymentReferenceNumberField.set("")
        investedFeeField.set("".toSpanned())
        nominalTokenValueField.set("".toSpanned())
    }
}