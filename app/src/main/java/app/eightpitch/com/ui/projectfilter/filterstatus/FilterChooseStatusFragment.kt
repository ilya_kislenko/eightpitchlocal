package app.eightpitch.com.ui.projectfilter.filterstatus

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.extensions.injectViewModel
import app.eightpitch.com.ui.projectfilter.SharableFilterViewModel
import app.eightpitch.com.ui.projectfilter.filterstatus.adapter.FilterButtonAdapter
import app.eightpitch.com.utils.SafeLinearLayoutManager
import kotlinx.android.synthetic.main.fragment_filter_choose_status.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class FilterChooseStatusFragment : BaseFragment<FilterChooseStatusFragmentViewModel>() {

    override fun getLayoutID() = R.layout.fragment_filter_choose_status

    override fun getVMClass() = FilterChooseStatusFragmentViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindLoadingIndicator()

        additionalToolbarRightButton.apply {
            setImageResource(R.drawable.icon_refresh)
            setOnClickListener {
                viewModel.resetStatuses()
            }
        }

        statesRecycler.apply {
            layoutManager = SafeLinearLayoutManager(context, VERTICAL, false)
            adapter = FilterButtonAdapter().apply {
                onItemClicked = { item, isActive, _ ->
                    if (isActive) viewModel.addToFilter(item)
                    else viewModel.removeFromFilter(item)
                }
            }
        }

        applyFilterButton.setOnClickListener {
            viewModel.applyFilter()
            popBackStack(R.id.filterProjectFragment)
        }

        val sharedViewModel = injectViewModel(SharableFilterViewModel::class.java)
        viewModel.setFilters(sharedViewModel)
        viewModel.updateStateList()
    }

}
