package app.eightpitch.com.ui.authorized.main

import android.content.Context
import androidx.annotation.DrawableRes
import app.eightpitch.com.R
import app.eightpitch.com.extensions.orDefaultValue

enum class MainScreenTab(
    private val tabQueuePosition: Int,
    @DrawableRes private val backgroundSelector: Int
) {

    PROJECTS(
        0,
        R.drawable.projects_tab_background_selector
    ),
    DASHBOARD(
        1,
        R.drawable.dashboard_tab_background_selector
    ),
    NOTIFICATIONS(
        2,
        R.drawable.notifications_tab_background_selector
    ),
    PROFILE(
        3,
        R.drawable.profile_tab_background_selector
    );

    companion object {

        internal fun getMaxTabsCount() = values().size

        /**
         * Function that will return selected tab by a position
         */
        internal fun getTabByPosition(selectedPosition: Int) =
            values().find { it.tabQueuePosition == selectedPosition }
                .orDefaultValue(PROJECTS)

        /**
         * Function that will return all the tab names
         * for bottom toolbar layout sorted by a queue position
         */
        internal fun getBottomTabNames(context: Context): List<String> {
            val tabTitles = context.resources.getStringArray(R.array.root_tab_names)
            val values = values().toMutableList()
            return values.run {
                sortBy { it.tabQueuePosition }
                map { tabTitles[it.tabQueuePosition] }
            }
        }

        /**
         * Function that will return all the tab background
         * selectors for bottom toolbar layout sorted by a queue position
         */
        internal fun getBottomTabSelectors(): List<Int> {
            val values = values().toMutableList()
            return values.run {
                sortBy { it.tabQueuePosition }
                map { it.backgroundSelector }
            }
        }
    }
}
