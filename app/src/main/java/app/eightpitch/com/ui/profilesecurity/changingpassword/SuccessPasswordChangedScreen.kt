package app.eightpitch.com.ui.profilesecurity.changingpassword

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import app.eightpitch.com.R
import app.eightpitch.com.extensions.popBackStack
import kotlinx.android.synthetic.main.fragment_success_password_changed.*

class SuccessPasswordChangedScreen : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_success_password_changed, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        crossButton.setOnClickListener {
            popBackStack(R.id.profileSecurityFragment)
        }

        okButton.setOnClickListener {
            popBackStack(R.id.profileSecurityFragment)
        }
    }
}