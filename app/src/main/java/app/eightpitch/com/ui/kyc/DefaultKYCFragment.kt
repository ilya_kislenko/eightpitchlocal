package app.eightpitch.com.ui.kyc

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.eightpitch.com.R
import app.eightpitch.com.base.BackPressureFragment
import app.eightpitch.com.extensions.moveBackward
import kotlinx.android.synthetic.main.loading_progress_indicator.*

class DefaultKYCFragment : BackPressureFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_kyc_default, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        curtainView.visibility = View.VISIBLE

        registerOnBackPressedListener { moveBackward() }
    }
}
