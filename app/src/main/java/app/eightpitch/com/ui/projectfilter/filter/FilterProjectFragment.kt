package app.eightpitch.com.ui.projectfilter.filter

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.extensions.injectViewModel
import app.eightpitch.com.ui.projectfilter.SharableFilterViewModel
import app.eightpitch.com.ui.projectfilter.filter.adapters.FilterAmountRangeAdapter
import app.eightpitch.com.ui.projectfilter.filter.adapters.FilterSecurityAdapter
import app.eightpitch.com.ui.projectfilter.filter.adapters.FilterStatusesAdapter
import app.eightpitch.com.utils.SafeLinearLayoutManager
import kotlinx.android.synthetic.main.fragment_filter_projects.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class FilterProjectFragment : BaseFragment<FilterProjectFragmentViewModel>() {

    override fun getLayoutID() = R.layout.fragment_filter_projects

    override fun getVMClass() = FilterProjectFragmentViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindLoadingIndicator()

        additionalToolbarRightButton.apply {
            setImageResource(R.drawable.icon_refresh)
            setOnClickListener {
                viewModel.resetAllFilter()
            }
        }
        viewModel.getFilteredProjectResult().observe(viewLifecycleOwner, Observer { handleResult(it) {} })
        statusRecycler.apply {
            layoutManager = SafeLinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = FilterStatusesAdapter().apply {
                onItemClicked = { item, _, _ ->
                    viewModel.removeStatus(item)
                }
            }
        }
        amountRangeRecycler.apply {
            layoutManager = SafeLinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = FilterAmountRangeAdapter().apply {
                onItemClicked = { item, _, _ ->
                    viewModel.removeAmountRange(item)
                }
            }
        }
        securityTypesRecycler.apply {
            layoutManager = SafeLinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = FilterSecurityAdapter().apply {
                onItemClicked = { item, _, _ ->
                    viewModel.removeSecurity(item)
                }
            }
        }
        containerStatus.setOnClickListener {
            navigate(R.id.action_filterProjectFragment_to_filterChooseStatusFragment)
        }
        containerAmountRange.setOnClickListener {
            navigate(R.id.action_filterProjectFragment_to_filterChooseAmountRangeFragment)
        }
        containerSecurity.setOnClickListener {
            navigate(R.id.action_filterProjectFragment_to_filterChooseSecurityFragment)
        }
        applyFilterButton.setOnClickListener {
            popBackStack(R.id.projectsFragment)
        }

        val sharableViewModel =
            injectViewModel(SharableFilterViewModel::class.java)

        viewModel.setFilters(sharableViewModel)
        viewModel.setupFilterParameters()
    }

    override fun onResume() {
        super.onResume()

        viewModel.setupFilterParameters()
    }
}
