package app.eightpitch.com.ui.editprofile

import android.os.Bundle
import android.view.View
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.user.Country.Companion.toDisplayValue
import app.eightpitch.com.data.dto.user.TaxInformationBody
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.extensions.getSortedCountries
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.extensions.showDatePickerDialog
import app.eightpitch.com.extensions.showDialog
import app.eightpitch.com.ui.GenderSpinnerAdapter
import app.eightpitch.com.ui.kyc.country.CountrySpinnerAdapter
import app.eightpitch.com.ui.kyc.nationality.NationalitySpinnerAdapter
import app.eightpitch.com.ui.registration.signup.institutionalinvestor.company.CompanyTypeSpinnerAdapter
import kotlinx.android.synthetic.main.fragment_edit_profile.*
import kotlinx.android.synthetic.main.german_tax_edit_profile_layout.*

class EditProfileFragment : BaseFragment<EditProfileFragmentViewModel>() {

    override fun getLayoutID() = R.layout.fragment_edit_profile

    override fun getVMClass() = EditProfileFragmentViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindLoadingIndicator()
        setupSpinners()

        buttonUpdateAccount.setOnClickListener {
            if (viewModel.validateInput()) {
                viewModel.updateProfileData()
            }
        }

        politicallyToggle.setOnSelectionChangedListener {
            viewModel.setPoliticallyState(it)
        }

        taxLiabilityToggle.setOnSelectionChangedListener {
            viewModel.setLiabilityState(it)
        }


        taxResidentToggleButton.setOnSelectionChangedListener {
            viewModel.isGermanyTaxResident.set(it)
        }

        churchTaxToggleButton.setOnSelectionChangedListener {
            viewModel.isTaxLiability.set(it)
        }

        textInputLayoutBirthDate.setEndIconClickListener {
            showDatePickerDialog(
                selectedDateInMillis = if (viewModel.birthDateMillis == 0L)
                    System.currentTimeMillis() else viewModel.birthDateMillis
            ) { localDate ->
                viewModel.setBirthDate(localDate)
            }
        }

        viewModel.getUserMeResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) { user ->
                companyTypeSpinner.setSelection(viewModel.getUserCompanyTypeArrayIndex(user))
                genderSpinner.setSelection(viewModel.getUserSexArrayIndex(user))
                nationalitySpinner.setSelection(viewModel.getUserNationalityArrayIndex(user))
                countrySpinner.setSelection(viewModel.getUserNationalityArrayIndex(user))
                churchSpinner.setSelection(viewModel.getChurchAttrArrayIndex(user))
                setSpinnersSelection(user)
            }
        })

        viewModel.getUpdateUserMeResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) { isNeedWait ->
                if (isNeedWait) {
                    showDialog(
                        getString(R.string.your_information_was_updated),
                        getString(R.string.thank_you_for_updating_your_information_as),
                        getString(R.string.ok_text),
                        positiveListener = { popBackStack(R.id.profilePersonalFragment) },
                        negativeListener = { popBackStack(R.id.profilePersonalFragment) },
                        cancelable = false
                    )
                } else {
                    popBackStack(R.id.profilePersonalFragment)
                }
            }
        })

        viewModel.getUser()
    }

    private fun setupSpinners() {
        setupCompanyTypeSpinner()
        setupGenderSpinner()
        setupNationalitySpinner()
        setupCountrySpinner()
        setupTaxChurchSpinner()
    }

    private fun setSpinnersSelection(user: User) {
        context?.let { context ->

            val countrySpinnerData = context.getSortedCountries()
            val (country, nationality) = user.run {
                toDisplayValue(context, userAddress.country) to toDisplayValue(context, nationality)
            }

            val countryIndex = countrySpinnerData.indexOf(country)
            val nationalityIndex = countrySpinnerData.indexOf(nationality)

            countrySpinner.setSelection(countryIndex)
            nationalitySpinner.setSelection(nationalityIndex)
        }
    }

    private fun setupCompanyTypeSpinner() {
        companyTypeSpinner.apply {
            val companyTypeSpinnerData = context.resources.getStringArray(R.array.companyType).toList()
            val companyAdapter = CompanyTypeSpinnerAdapter(context, companyTypeSpinnerData)
            setAdapter(companyAdapter)
            setSelection(companyAdapter.getHintPosition())
            setSelectedListener({
                viewModel.setCompanyType(it)
            }, {})
        }
    }

    private fun setupGenderSpinner() {
        genderSpinner.apply {
            val genderSpinnerData = context.resources.getStringArray(R.array.genderType).toList()
            val genderTypeAdapter = GenderSpinnerAdapter(context, genderSpinnerData)
            setAdapter(genderTypeAdapter)
            setSelection(genderTypeAdapter.getHintPosition())
            setSelectedListener({
                viewModel.setGender(it)
            }, {})
        }
    }

    private fun setupNationalitySpinner() {
        nationalitySpinner.apply {
            val nationalityAdapter = NationalitySpinnerAdapter(context)
            setAdapter(nationalityAdapter)
            setSelection(nationalityAdapter.getHintPosition())
            setSelectedListener({
                viewModel.setNationality(it)
            }, {})
        }
    }

    private fun setupCountrySpinner() {
        countrySpinner.apply {
            val countryAdapter = CountrySpinnerAdapter(context)
            setAdapter(countryAdapter)
            setSelection(countryAdapter.getHintPosition())
            setSelectedListener({
                viewModel.setCountry(it)
            }, {})
        }
    }

    private fun setupTaxChurchSpinner() {
        churchSpinner.apply {
            val spinnerData = TaxInformationBody.TaxChurch.getTaxChurches()
            val churchAdapter = CompanyTypeSpinnerAdapter(context, spinnerData)
            setAdapter(churchAdapter)
            setSelection(churchAdapter.getHintPosition())
            setSelectedListener({
                viewModel.setChurch(it)
            }, {})
        }
    }
}
