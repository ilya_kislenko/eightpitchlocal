package app.eightpitch.com.ui.registration.signup.privateinvestor.accountselection

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.eightpitch.com.R
import app.eightpitch.com.ui.dialogs.DefaultBottomDialog

class AccountTypeInfoDialog : DefaultBottomDialog() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.dialog_account_type_info, container, false)
}