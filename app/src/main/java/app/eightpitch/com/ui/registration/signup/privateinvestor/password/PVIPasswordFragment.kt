package app.eightpitch.com.ui.registration.signup.privateinvestor.password

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.View
import androidx.lifecycle.Observer
import app.eightpitch.com.BuildConfig
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.signup.CommonInvestorRequestBody
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.extensions.toHtml
import app.eightpitch.com.ui.registration.signup.CongratulationsFragment
import app.eightpitch.com.ui.webview.DefaultWebViewFragment
import app.eightpitch.com.utils.Constants
import kotlinx.android.synthetic.main.fragment_pvi_password.*
import kotlinx.android.synthetic.main.toolbar_layout.*


class PVIPasswordFragment : BaseFragment<PVIPasswordFragmentViewModel>() {

    companion object {

        internal const val PARTIAL_USER = "PARTIAL_USER"

        fun buildWithAnArguments(
            partialUser: CommonInvestorRequestBody
        ) = Bundle().apply {
            putParcelable(PARTIAL_USER, partialUser)
        }
    }

    private val partialUser: CommonInvestorRequestBody
        get() = arguments?.getParcelable(PARTIAL_USER) ?: CommonInvestorRequestBody()

    override fun getLayoutID() = R.layout.fragment_pvi_password

    override fun getVMClass() = PVIPasswordFragmentViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        additionalToolbarRightButton.setOnClickListener {
            val bundle = DefaultWebViewFragment.buildWithAnArguments(Constants.FAQ_URL)
            navigate(R.id.action_PVIPasswordFragment_to_defaultWebViewFragment, bundle)
        }

        createAccountButton.setOnClickListener {
            viewModel.validatePasswords(partialUser.copy(isSubscribedToEmail = checkboxSendMe.isChecked))
        }

        checkboxAccept.setOnCheckedChangeListener { _, isChecked ->
            viewModel.createButtonAvailability.set(isChecked)
        }

        with(describeCheckboxAccept) {
            text = resources.getString(R.string.agreement_and_privacy_policy).toHtml()
            movementMethod = LinkMovementMethod.getInstance()
        }

        viewModel.getCreatingPviAccountResult().observe(viewLifecycleOwner, Observer { result ->
            handleResult(result) {
                navigate(
                    R.id.action_PVIPasswordFragment_to_congratulationsFragment,
                    CongratulationsFragment.buildWithAnArguments(partialUser.group)
                )
            }
        })
    }
}