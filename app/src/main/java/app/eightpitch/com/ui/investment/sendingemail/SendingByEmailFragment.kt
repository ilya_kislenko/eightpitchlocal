package app.eightpitch.com.ui.investment.sendingemail

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.base.baseviewmodels.codeverification.DefaultCodeValidationFragment
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO.CREATOR.INVEST
import app.eightpitch.com.data.dto.projectdetails.ProjectsFile
import app.eightpitch.com.data.dto.signup.CommonInvestorRequestBody
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.utils.*
import kotlinx.android.synthetic.main.fragment_sending_documents_by_email.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import java.util.ArrayList

class SendingByEmailFragment : BaseFragment<SendingByEmailViewModel>() {

    companion object {

        const val PROJECT_ID = "PROJECT_ID"
        const val INVESTMENT_ID = "INVESTMENT_ID"
        const val PROJECT_FILES = "PROJECT_FILES"

        fun buildWithAnArguments(
            projectId: String,
            investmentId: String,
            projectFiles: ArrayList<ProjectsFile>
        ) = Bundle().apply {
            putString(PROJECT_ID, projectId)
            putString(INVESTMENT_ID, investmentId)
            putParcelableArrayList(PROJECT_FILES, projectFiles)
        }
    }

    private val projectId: String
        get() = arguments?.getString(PROJECT_ID) ?: ""

    private val investmentId: String
        get() = arguments?.getString(INVESTMENT_ID) ?: ""

    private val projectFiles: List<ProjectsFile>
        get() = arguments?.getParcelableArrayList(PROJECT_FILES) ?: listOf()

    override fun getLayoutID() = R.layout.fragment_sending_documents_by_email

    override fun getVMClass() = SendingByEmailViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        setupUI()
        viewModel.setupFiles(projectFiles)

        filesRecycler.apply {
            adapter = TextViewsRecyclerAdapter(true)
            layoutManager = SafeLinearLayoutManager(context, VERTICAL, false)
        }

        firstCheckBox.setOnCheckedChangeListener { _, isChecked ->
            viewModel.changeAvailability(isChecked && secondCheckBox.isChecked)
        }
        secondCheckBox.setOnCheckedChangeListener { _, isChecked ->
            viewModel.changeAvailability(isChecked && firstCheckBox.isChecked)
        }

        viewModel.getRequestResult().observe(viewLifecycleOwner, Observer { result ->
            handleResult(result) { response ->
                val arguments =
                    DefaultCodeValidationFragment.buildWithAnArguments(
                        projectId = projectId,
                        investmentId = investmentId,
                        validationScreenInfo = createValidationScreenInfo(),
                        partialUser = CommonInvestorRequestBody(interactionId = response.interactionId)
                    )
                navigate(R.id.action_sendingByEmailFragment_to_digitCodeValidationFragment, arguments)
            }
        })

        nextButton.setOnClickListener {
            viewModel.requestInvestmentConfirmation(investmentId, projectId)
        }
    }

    private fun setupUI() {
        context?.let {
            downDivider.isVisible = false
            headerTitle.setTextColor(ResourcesCompat.getColor(resources, R.color.white, it.theme))
            toolbarBackButton.setImageDrawable(ContextCompat.getDrawable(it, R.drawable.icon_white_cross))
        }
    }

    private fun createValidationScreenInfo() = ValidationScreenInfoDTO(
        verificationType = INVEST,
        validationButtonName = getString(R.string.confirm_text),
        header = getString(R.string.investment_confirmation_text),
        toolbarTitle = getString(R.string.investment_payment_title_text),
        subTitleMessage = getString(R.string.investment_confirmation_message_text)
    )
}