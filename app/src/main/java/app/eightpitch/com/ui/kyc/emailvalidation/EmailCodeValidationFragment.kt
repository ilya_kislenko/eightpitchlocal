package app.eightpitch.com.ui.kyc.emailvalidation

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.extensions.REQUEST_SMS_CONSENT_REQUEST
import app.eightpitch.com.extensions.*
import app.eightpitch.com.utils.Timer
import app.eightpitch.com.utils.autoinsertsms.DefaultSMSInterceptor
import app.eightpitch.com.utils.autoinsertsms.SMSBroadcastReceiver
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.gms.auth.api.phone.SmsRetriever.EXTRA_SMS_MESSAGE
import kotlinx.android.synthetic.main.fragment_email_validation_code.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import java.lang.ref.WeakReference

class EmailCodeValidationFragment : BaseFragment<EmailCodeValidationViewModel>() {

    private val timer = Timer()
    private val smsVerificationReceiver = SMSBroadcastReceiver()

    override fun getLayoutID() = R.layout.fragment_email_validation_code

    override fun getVMClass() = EmailCodeValidationViewModel::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupReceiver()
    }

    private fun setupReceiver() {
        context?.registerSMSBroadcastReceiver(smsVerificationReceiver)
        smsVerificationReceiver.registerNewMessageListener(DefaultSMSInterceptor(WeakReference(this)))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()
        timer.timerListener = TimerListener()
        setupInputFieldsListener()
        context?.let { SmsRetriever.getClient(it).startSmsUserConsent(null) }

        viewModel.sendSMSCodeAndRetrieveUserEmail()

        viewModel.getVerifyWithEmailCodeResult().observe(viewLifecycleOwner, Observer {
            handleResult(it) {
                navigate(R.id.action_emailCodeValidationFragment_to_emailConfirmedFragment)
            }
        })

        viewModel.getResendingCodeResult().observe(viewLifecycleOwner, Observer {
            handleResult(it) {
                restartTimer()
                focusableView.clear()
            }
        })

        registerOnBackPressedListener { moveBackward() }

        setupView()
        viewModel.setupBinding()
    }

    private fun setupView() {
        toolbarBackButton.apply {
            setImageResource(R.drawable.icon_cross)
            setOnClickListener {
                moveBackward()
            }
        }

        validateButton.setOnClickListener {
            viewModel.confirmEmailVerificationCode()
        }
        resendSMSTextView.setOnClickListener {
            viewModel.resendSMSCodeByEmail()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_SMS_CONSENT_REQUEST ->
                if (resultCode == Activity.RESULT_OK && data != null) {
                    val message = data.getStringExtra(EXTRA_SMS_MESSAGE).orDefaultValue("")
                    context?.copyToClipboard(message.findActivationCode())
                }
        }
    }

    override fun onResume() {
        super.onResume()
        showKeyboard()
    }

    override fun onDestroy() {
        timer.stopTimer()
        context?.unregisterSMSBroadcastReceiver(smsVerificationReceiver)
        activity?.hideKeyboard()
        super.onDestroy()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        timer.timerListener = null
    }

    private inner class TimerListener : Timer.TimerListener {

        override fun countProcessing(value: Int) {
            if (isAliveAndAvailable()) {
                updateControlsState()
                viewModel.updateSMSMessage(value)
            }
        }

        override fun countFinished() {
            updateControlsState(true)
        }
    }

    private fun restartTimer() {
        timer.startTimer(30)
    }

    private fun updateControlsState(toEnabled: Boolean = false) {
        if (isAliveAndAvailable()) {
            resendSMSTextView.isEnabled = toEnabled
            messageTextView.changeVisibility(!toEnabled)
        }
    }

    private inner class OnClickListener : View.OnClickListener {
        override fun onClick(v: View?) {
            showKeyboard()
        }
    }

    private fun showKeyboard() {
        focusableView.focus()
    }

    private fun setupInputFieldsListener(listener: OnClickListener = OnClickListener()) {
        firstSMSInputField.setOnClickListener(listener)
        secondSMSInputField.setOnClickListener(listener)
        thirdSMSInputField.setOnClickListener(listener)
        fourthSMSInputField.setOnClickListener(listener)
        fifthSMSInputField.setOnClickListener(listener)
        sixthSMSInputField.setOnClickListener(listener)
    }
}