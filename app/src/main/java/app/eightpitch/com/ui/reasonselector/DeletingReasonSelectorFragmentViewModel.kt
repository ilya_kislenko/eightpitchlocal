package app.eightpitch.com.ui.reasonselector

import android.content.Context
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.user.User.CREATOR.LEVEL_2
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.extensions.getErrorEmptyString
import app.eightpitch.com.models.SMSModel
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.utils.Empty
import app.eightpitch.com.utils.SingleActionLiveData
import app.eightpitch.com.utils.SingleActionObservableString
import app.eightpitch.com.views.interfaces.ShortFieldValidator
import kotlinx.coroutines.launch
import javax.inject.Inject

internal typealias InteractionIdToPhoneNumber = Pair<String, String>

class DeletingReasonSelectorFragmentViewModel @Inject constructor(
    private val appContext: Context,
    private val userModel: UserModel,
    private val smsModel: SMSModel,
    threadsSeparator: ThreadsSeparator,
) : BaseViewModel(threadsSeparator) {

    val title = appContext.getString(R.string.reason_toolbar_title)

    val reasonFieldVisibility = ObservableField<Int>(GONE)

    private var reasonSpinnerText = ""
    val reasonSpinnerErrorHolder = SingleActionObservableString()

    private var reason = ""
    val reasonErrorHolder = SingleActionObservableString()

    val reasonWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            reason = sequence.toString()
        }
    }

    internal fun validateDeletionReason(userStatus: String) {
        val reasonSpinnerTextError = when {
            reasonSpinnerText.isEmpty() -> getErrorEmptyString(appContext)
            else -> ""
        }
        if (reasonSpinnerTextError.isNotEmpty())
            reasonSpinnerErrorHolder.set(reasonSpinnerTextError)

        val reasonTextError = when {
            reason.isEmpty() -> getErrorEmptyString(appContext)
            else -> ""
        }
        if (reasonTextError.isNotEmpty())
            reasonErrorHolder.set(reasonTextError)

        val defaultAccountDeletionReasons =
            appContext.resources.getStringArray(R.array.defaultAccountDeletionReasons)
        val isSelectedOtherReason =
            defaultSelectionDeletionReason == defaultAccountDeletionReasons.lastOrNull()

        if (isSelectedOtherReason && reasonSpinnerTextError.isEmpty() && reasonTextError.isEmpty()) {
            resolveNextAction(userStatus)
        } else if (!isSelectedOtherReason && reasonSpinnerTextError.isEmpty()) {
            resolveNextAction(userStatus)
        }
    }

    private fun resolveNextAction(userStatus: String) {
        if (userStatus == LEVEL_2) {
            deleteAccountRequest()
        } else {
            requestSMSCode()
        }
    }

    /**
     * Reason selected by the user from the spinner
     */
    private var defaultSelectionDeletionReason =
        appContext.resources.getStringArray(R.array.defaultAccountDeletionReasons).firstOrNull()
            .orEmpty()

    internal fun getDeletionReasonText() =
        if (reasonFieldVisibility.get() == VISIBLE) reason else defaultSelectionDeletionReason

    internal fun setReason(position: Int) {
        reasonSpinnerText =
            appContext.resources.getStringArray(R.array.defaultAccountDeletionReasons)
                .getOrElse(position) { "" }
        val defaultAccountDeletionReasons =
            appContext.resources.getStringArray(R.array.defaultAccountDeletionReasons)
        defaultSelectionDeletionReason = defaultAccountDeletionReasons.getOrElse(position) { "" }

        val isSelectedOtherReason =
            defaultSelectionDeletionReason == defaultAccountDeletionReasons.lastOrNull()
        reasonFieldVisibility.set(if (isSelectedOtherReason) VISIBLE else GONE)
    }

    private val smsResult = SingleActionLiveData<Result<InteractionIdToPhoneNumber>>()
    internal fun getSMSResult(): LiveData<Result<InteractionIdToPhoneNumber>> = smsResult

    private fun requestSMSCode() {
        viewModelScope.launch {

            val result = asyncLoading {
                val user = userModel.getUserData()
                val response = smsModel.requireSMSCodeByPhoneNumber(user.phoneNumber)
                response.interactionId to user.phoneNumber
            }

            smsResult.postAction(result)
        }
    }

    private val deleteRequestResult = SingleActionLiveData<Result<Empty>>()
    internal fun getDeleteRequestResult(): LiveData<Result<Empty>> = deleteRequestResult

    private fun deleteAccountRequest() {
        viewModelScope.launch {

            val result = asyncLoading {
                userModel.deleteAccount(getDeletionReasonText())
            }

            deleteRequestResult.postAction(result)
        }
    }


    override fun cleanUpFinally() {
        super.cleanUpFinally()
        reason = ""
        reasonErrorHolder.set("")
        reasonFieldVisibility.set(GONE)
        defaultSelectionDeletionReason =
            appContext.resources.getStringArray(R.array.defaultAccountDeletionReasons).firstOrNull()
                .orEmpty()
    }
}
