package app.eightpitch.com.ui.projectsdetail

import android.os.Bundle
import android.view.View
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.dynamicviews.mainfacts.FinancialInformationItem
import app.eightpitch.com.data.dto.investment.ProjectInvestmentInfoResponse
import app.eightpitch.com.data.dto.project.AmountOfProjectInvestments
import app.eightpitch.com.data.dto.project.ProjectInvestment.Companion.BOOKED
import app.eightpitch.com.data.dto.project.ProjectInvestment.Companion.CANCELED_IN_PROGRESS
import app.eightpitch.com.data.dto.project.ProjectInvestment.Companion.CONFIRMED
import app.eightpitch.com.data.dto.project.ProjectInvestment.Companion.NEW
import app.eightpitch.com.data.dto.project.ProjectInvestment.Companion.PAID
import app.eightpitch.com.data.dto.project.ProjectInvestment.Companion.PENDING
import app.eightpitch.com.data.dto.projectdetails.AdditionalFields
import app.eightpitch.com.data.dto.projectdetails.GetProjectResponse
import app.eightpitch.com.data.dto.projectdetails.ProjectsFile
import app.eightpitch.com.data.dto.projectdetails.ProjectsFile.CREATOR.BACKGROUND_IMAGE
import app.eightpitch.com.data.dto.projectdetails.ProjectsFile.CREATOR.COMPANY_LOGO_COLORED
import app.eightpitch.com.data.dto.user.Country
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.dynamicdetails.DynamicProjectDetailsParentFragment
import app.eightpitch.com.ui.investment.CurrencyChooserFragment
import app.eightpitch.com.ui.investment.ViewPagerSharableViewModel
import app.eightpitch.com.ui.investment.processing.InvestmentProcessingFragment
import app.eightpitch.com.ui.investment.unprocessed.UnfinishedInvestmentFragment
import app.eightpitch.com.ui.payment.hardcappaymentselection.HardCapPaymentSelectionFragment
import app.eightpitch.com.ui.payment.softcappaymentselection.SoftCapPaymentSelectionFragment
import app.eightpitch.com.ui.videoplayer.activity.VideoPlayerActivity
import app.eightpitch.com.utils.Constants.FILES_ENDPOINT
import app.eightpitch.com.utils.Logger
import app.eightpitch.com.utils.OnSwipeTouchListener
import kotlinx.android.synthetic.main.fragment_project_details.*
import java.math.BigDecimal
import java.util.*

class ProjectDetailsFragment : BaseFragment<ProjectDetailsViewModel>() {

    companion object {

        internal const val PROJECT_ID = "PROJECT_ID"

        fun buildWithAnArguments(
            projectId: String,
        ) = Bundle().apply {
            putString(PROJECT_ID, projectId)
        }
    }

    private val projectId: String
        get() = arguments?.getString(PROJECT_ID, "") ?: ""

    override fun getLayoutID() = R.layout.fragment_project_details

    override fun getVMClass() = ProjectDetailsViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        viewModel.getProjectResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) {
                setupFundingProgressBar(it.graph)
                setUpProjectBackground(it.projectPage.projectPageFiles)
            }
        })

        viewModel.getProjectInvestmentInfoResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) { projectInvestmentInfo ->
                navigateForwardWith(projectInvestmentInfo)
            }
        })

        viewModel.getVideoUrlLinkResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) { vimeoUrl ->
                context?.let {
                    startActivity(VideoPlayerActivity.generateIntent(it, vimeoUrl))
                }
            }
        })

        toolbarBackButton.setOnClickListener {
            popBackStack()
        }

        playImageButton.setOnClickListener {
            viewModel.retrieveVideoLink()
        }

        investNowButton.setOnClickListener {
            viewModel.getProjectInvestmentInfo(projectId)
        }

        scrollView.setOnTouchListener(object : OnSwipeTouchListener(context) {
            override fun onSwipeUp() {
                super.onSwipeUp()
                navigateToAdditionalDetails()
            }
        })
        viewModel.retrieveData(projectId)
    }

    private fun navigateToAdditionalDetails() {
        viewModel.getProjectInfo()?.let {

            val blockConstructor = it.projectPage.blockConstructor
            val additionalFields = it.additionalFields
            val financialInfoItem = createFinancialInformationItem(it)

            Logger.logInfo(Logger.TAG_INFO, blockConstructor)
            if (hasContent(blockConstructor, additionalFields, financialInfoItem)) {
                val arguments = DynamicProjectDetailsParentFragment.buildWithAnArguments(
                    projectId, blockConstructor, additionalFields, financialInfoItem
                )
                navigate(
                    R.id.action_projectDetailsFragment_to_dynamicProjectDetailsParentFragment,
                    arguments
                )
            }
        }
    }

    private fun createFinancialInformationItem(response: GetProjectResponse) = response.run {
        FinancialInformationItem(
            country = context?.let { ctx ->Country.toDisplayValue(ctx, financialInformation.country)}.orDefaultValue(""),
            financingPurpose = financialInformation.financingPurpose,
            goal = financialInformation.goal.formatToCurrencyView(),
            guarantee = financialInformation.guarantee,
            investmentSeries = financialInformation.investmentSeries,
            isin = financialInformation.isin,
            nominalValue = tokenParametersDocument.nominalValue.toLong().formatToCurrencyView(),
            typeOfSecurity = financialInformation.typeOfSecurity,
            dsoProjectFinishDate = tokenParametersDocument.dsoProjectFinishDate,
            dsoProjectStartDate = tokenParametersDocument.dsoProjectStartDate,
            softCap = tokenParametersDocument.softCap?.formatToCurrencyView().orDefaultValue(""),
            minimumInvestmentAmount = tokenParametersDocument.minimumInvestmentAmount.formatToCurrencyView()
        )
    }

    private fun setupFundingProgressBar(amountOfInvestment: AmountOfProjectInvestments) {
        fundingProgressBar.run {
            setCapProgress(
                if (viewModel.isSoftCapProject) amountOfInvestment.softCap.orDefaultValue(
                    0
                ) else amountOfInvestment.hardCap
            )
            setCurrentProgress(amountOfInvestment.currentFundingSum)
            setCurrentTextProgress(
                String.format(
                    Locale.getDefault(),
                    context.getString(R.string.percent_string),
                    amountOfInvestment.currentFundingSum
                )
            )
        }
    }

    private fun setUpProjectBackground(projectPageFiles: List<ProjectsFile>) {
        projectPageFiles.find { it.fileType == BACKGROUND_IMAGE }?.let {
            context?.loadImage(
                FILES_ENDPOINT + it.projectsFile.uniqueFileName,
                targetImageView = imageBackground
            )
        }
        projectPageFiles.find { it.fileType == COMPANY_LOGO_COLORED }?.let {
            context?.loadImage(
                FILES_ENDPOINT + it.projectsFile.uniqueFileName,
                targetImageView = toolbarLogoImage
            )
        }
    }

    private fun navigateForwardWith(projectInvestmentInfo: ProjectInvestmentInfoResponse) {
        projectInvestmentInfo.setProjectName(viewModel.getProjectInfo()?.projectPage?.companyName)
        when (projectInvestmentInfo.unprocessedInvestment.investmentStatus) {
            PAID -> {
                navigate(R.id.action_projectDetailsFragment_to_investmentProcessingFragment)
            }
            CANCELED_IN_PROGRESS, NEW, PENDING -> {
                navigate(R.id.action_projectDetailsFragment_to_investmentProcessingFragment,
                    InvestmentProcessingFragment.buildWithAnArguments(projectId))
            }
            BOOKED -> {
                val arguments =
                    UnfinishedInvestmentFragment.buildWithAnArguments(
                        projectId,
                        projectInvestmentInfo
                    )
                navigate(
                    R.id.action_projectDetailsFragment_to_unFinishedInvestmentFragment,
                    arguments
                )
            }
            CONFIRMED -> {

                val investmentId = projectInvestmentInfo.unprocessedInvestment.investmentId
                val fee =
                    viewModel.getProjectInfo()?.tokenParametersDocument?.assetBasedFees.orDefaultValue(
                        BigDecimal(0)
                    )
                if (viewModel.isSoftCapProject) {
                    navigate(
                        R.id.action_projectDetailsFragment_to_softCapPaymentSelectionFragment,
                        SoftCapPaymentSelectionFragment.buildWithAnArguments(
                            investmentId,
                            projectId,
                            projectInvestmentInfo.copy(assetBasedFees = fee).also {
                                it.setProjectName(viewModel.getProjectInfo()?.projectPage?.companyName)
                            }
                        )
                    )
                } else {
                    navigate(
                        R.id.action_projectDetailsFragment_to_hardCapPaymentSelectionFragment,
                        HardCapPaymentSelectionFragment.buildWithAnArguments(
                            investmentId,
                            projectId,
                            projectInvestmentInfo.copy(assetBasedFees = fee).also {
                                it.setProjectName(viewModel.getProjectInfo()?.projectPage?.companyName)
                            }
                        )
                    )
                }
            }
            else -> {
                val viewPagerSharableViewModel =
                    injectViewModel(ViewPagerSharableViewModel::class.java)
                viewPagerSharableViewModel.setValue(projectInvestmentInfo.minimumInvestmentAmount.toBigDecimal())
                val arguments = CurrencyChooserFragment.buildWithAnArguments(
                    projectId, projectInvestmentInfo, viewModel.getRecentUser().orDefaultValue(User())
                )
                navigate(R.id.action_projectDetailsFragment_to_currencyChooserFragment, arguments)
            }
        }
    }

    private fun hasContent(
        blockConstructor: String,
        additionalFields: AdditionalFields,
        financialInfoItem: FinancialInformationItem,
    ): Boolean {
        val hasAdditional = additionalFields.run {
            listOf(
                agio, conversionRight, dividend, futureShareNominalValue,
                highOfDividend, initialSalesCharge, interest, interestInterval,
                issuedShares, typeOfRepayment, wkin
            ).any { item -> item.isNotEmpty() }
        }

        val hasFinancial = financialInfoItem.run {
            listOf(
                country, financingPurpose, goal, guarantee, investmentSeries,
                isin, nominalValue, typeOfSecurity, dsoProjectFinishDate,
                dsoProjectStartDate, softCap, minimumInvestmentAmount
            ).any { item -> item.isNotEmpty() }
        }

        return blockConstructor.isNotEmpty() || hasAdditional || hasFinancial
    }
}
