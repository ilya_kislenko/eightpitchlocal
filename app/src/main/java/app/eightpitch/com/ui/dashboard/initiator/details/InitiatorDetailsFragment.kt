package app.eightpitch.com.ui.dashboard.initiator.details

import android.content.Context
import android.graphics.PorterDuff.Mode.SRC
import android.os.Bundle
import android.view.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import app.eightpitch.com.R
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.ACTIVE
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.COMING_SOON
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.NOT_STARTED
import app.eightpitch.com.data.dto.projectdetails.ProjectsFile.CREATOR.COMPANY_LOGO_COLORED
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.dashboard.initiator.InitiatorProjectItem
import app.eightpitch.com.utils.Constants.FILES_ENDPOINT
import app.eightpitch.com.views.HorizontalProgressBarView
import kotlinx.android.synthetic.main.fragment_initiator_details.*
import java.util.*

class InitiatorDetailsFragment : Fragment() {

    companion object {

        internal const val INITIATOR_PROJECT_ITEM = "INITIATOR_PROJECT_ITEM"

        fun buildWithAnArguments(
            projectItem: InitiatorProjectItem
        ) = Bundle().apply {
            putParcelable(INITIATOR_PROJECT_ITEM, projectItem)
        }
    }

    private val projectItem: InitiatorProjectItem
        get() = arguments?.getParcelable(INITIATOR_PROJECT_ITEM) ?: InitiatorProjectItem()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_initiator_details, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI(view.context)
    }

    private fun setupUI(context: Context) {
        projectItem.run {
            val companyLogo = projectFiles.findImageUrlByTag(COMPANY_LOGO_COLORED)
            context.loadImage(
                FILES_ENDPOINT + companyLogo,
                targetImageView = toolbarLogoImage
            )
            statusStatusTextView.apply {
                text = projectStatus.replace('_', ' ')
                background?.setColorFilter(
                    ContextCompat.getColor(
                        context,
                        getColorFilter(projectStatus)
                    ), SRC
                )
            }
            setupProgressBarView(this, fundingProgressBar)
            capitalInvestedTextView.text = String.format(
                getString(R.string.token_placeholder),
                projectBi.capitalInvested.toLong().asCurrency()
            )
            timeLeftTextView.text = projectItem.tokenParametersDocument.projectFinishDate.getDaysDuration(context)

            averageInvestmentTextView.text = String.format(
                getString(R.string.token_placeholder),
                projectBi.averageInvestment.toLong().asCurrency()
            )
            numberOfInvestorTextView.text = projectBi.investors.totalCount.toString()
            highestInvestmentTextView.text = String.format(
                getString(R.string.token_placeholder),
                projectBi.maxInvestment.toLong().asCurrency()
            )
            projectBi.maxInvestment.toString()
            lowestInvestmentTextView.text =
                String.format(
                    getString(R.string.token_placeholder),
                    projectBi.minInvestment.toLong().asCurrency()
                )
        }

        toolbarBackButton.setOnClickListener {
            popBackStack()
        }
    }

    private fun setupProgressBarView(
        projectItem: InitiatorProjectItem,
        fundingProgressBar: HorizontalProgressBarView
    ) {
        val currentFundingSum = projectItem.graph.currentFundingSum
        fundingProgressBar.run {
            setCurrentProgress(currentFundingSum)
            setCurrentTextProgress(
                String.format(
                    Locale.getDefault(),
                    context.getString(R.string.percent_string),
                    currentFundingSum
                )
            )
        }
        val context = fundingProgressBar.context
        val softCap = projectItem.graph.softCap
        val (cap, textProgress) = if (softCap == 0 || softCap == null) {
            projectItem.graph.hardCap to context.getString(R.string.hard_cap)
        } else {
            softCap.orDefaultValue(0) to context.getString(R.string.soft_cap)
        }
        fundingProgressBar.run {
            setCapProgress(cap)
            setCapTextProgress(textProgress)
        }
    }

    private fun getColorFilter(projectStatus: String) = when (projectStatus) {
        NOT_STARTED, COMING_SOON -> R.color.gray20
        ACTIVE -> R.color.green
        else -> R.color.lightRed
    }
}