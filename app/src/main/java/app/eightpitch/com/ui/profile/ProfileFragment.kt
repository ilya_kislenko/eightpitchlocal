package app.eightpitch.com.ui.profile

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.ui.kyc.KycActivity
import app.eightpitch.com.ui.registration.RegistrationActivity
import app.eightpitch.com.ui.webview.DefaultWebViewFragment
import app.eightpitch.com.utils.IntentsController
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment : BaseFragment<ProfileFragmentViewModel>() {

    override fun getLayoutID() = R.layout.fragment_profile

    override fun getVMClass() = ProfileFragmentViewModel::class.java

    private var profileOptionsClickListener: ProfileOptionsClickListener? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        profileOptionsClickListener = ProfileOptionsClickListener()
        bindLoadingIndicator()

        setupListeners()

        viewModel.getLogoutResult().observe(viewLifecycleOwner, Observer { result ->
            handleResult(result) {
                context?.let {
                    IntentsController.startActivityWithANewStack(
                        it.applicationContext,
                        RegistrationActivity::class.java
                    )
                }
            }
        })

        viewModel.getUserMeResult()
            .observe(viewLifecycleOwner, Observer { result -> handleResult(result) {} })
        viewModel.getUser()
    }

    private fun setupListeners() {
        investorNameTextView.setOnClickListener(profileOptionsClickListener)
        upgradeProfileButton.setDisablingClickListener {
            context?.let {
                startActivity(Intent(it, KycActivity::class.java))
            }
        }

        paymentsButton.setOnClickListener(profileOptionsClickListener)
        securityButton.setOnClickListener(profileOptionsClickListener)
        settingsButton.setOnClickListener(profileOptionsClickListener)

        helpButton.setOnClickListener(profileOptionsClickListener)
        legalButton.setOnClickListener(profileOptionsClickListener)
        logoutButton.setOnClickListener(profileOptionsClickListener)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        profileOptionsClickListener = null
    }

    private inner class ProfileOptionsClickListener : View.OnClickListener {

        override fun onClick(view: View) {
            when (view.id) {
                R.id.paymentsButton -> {
                    navigate(R.id.action_profileFragment_to_profilePaymentsFragment)
                }
                R.id.securityButton -> {
                    navigate(R.id.action_profileFragment_to_profileSecurityFragment)
                }
                R.id.settingsButton -> {
                    navigate(R.id.action_profileFragment_to_settingsFragment)
                }
                R.id.helpButton -> {
                    navigate(
                        R.id.action_profileFragment_to_defaultWebViewFragment,
                        DefaultWebViewFragment.buildWithAnArguments(viewModel.helpLink)
                    )
                }
                R.id.legalButton -> {
                    navigate(
                        R.id.action_profileFragment_to_defaultWebViewFragment,
                        DefaultWebViewFragment.buildWithAnArguments(getString(R.string.legal_url))
                    )
                }
                R.id.logoutButton -> {
                    viewModel.logout()
                }
                R.id.investorNameTextView -> {
                    navigate(R.id.action_profileFragment_to_profilePersonalFragment)
                }
            }
        }
    }
}