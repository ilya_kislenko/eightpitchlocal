package app.eightpitch.com.ui.projects.mappers

import app.eightpitch.com.data.dto.project.*
import app.eightpitch.com.data.dto.projectdetails.ProjectsFile.CREATOR.BACKGROUND_IMAGE
import app.eightpitch.com.data.dto.projectdetails.ProjectsFile.CREATOR.PROJECT_THUMBNAIL
import app.eightpitch.com.extensions.findImageUrlByTag
import app.eightpitch.com.ui.dashboard.initiator.InitiatorProjectItem
import app.eightpitch.com.ui.projects.adapters.ProjectItem

fun ProjectDetails.mapToProjectItem(): ProjectItem = ProjectItem(
    id = id,
    videoUrl = videoUrl,
    projectPageUrl = projectPageUrl,
    companyName = companyName,
    description = description,
    graph = amountOfProjectInvestments,
    financialInformation = financialInformation,
    tokenParametersDocument = tokenParametersDocument,
    backgroundImageUrl = projectPageFiles.findImageUrlByTag(BACKGROUND_IMAGE),
    thumbNailImageUrl = projectPageFiles.findImageUrlByTag(PROJECT_THUMBNAIL)
)

fun InvestorsProject.mapToProjectItem(): ProjectItem = projectInfo.run {
    ProjectItem(
        id = id,
        videoUrl = videoUrl,
        projectPageUrl = projectPageUrl,
        companyName = companyName,
        description = description,
        graph = amountOfProjectInvestments,
        financialInformation = financialInformation,
        tokenParametersDocument = tokenParametersDocument,
        monetaryAmount = investmentsInfo.totalAmountOfTokens,
        investmentStatus = investmentsInfo.lastInvestmentStatus,
        backgroundImageUrl = projectPageFiles.findImageUrlByTag(BACKGROUND_IMAGE),
        thumbNailImageUrl = projectPageFiles.findImageUrlByTag(PROJECT_THUMBNAIL)
    )
}

fun InitiatorProjectDetails.mapToInitiatorProjectItem(): InitiatorProjectItem =
    InitiatorProjectItem(
        id = id,
        companyName = companyName,
        description = projectDescription,
        graph = graph,
        tokenParametersDocument = tokenParametersDocument,
        projectStatus = projectStatus,
        projectFiles = projectFiles,
        projectBi = projectBi
    )
