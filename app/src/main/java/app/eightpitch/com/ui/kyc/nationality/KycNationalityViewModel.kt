package app.eightpitch.com.ui.kyc.nationality

import android.content.Context
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.user.Country.Companion.fromDisplayValue
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.data.dto.webid.WebIdUserRequestBody
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.extensions.getErrorEmptyString
import app.eightpitch.com.extensions.getSortedCountries
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.utils.SingleActionLiveData
import app.eightpitch.com.utils.SingleActionObservableString
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

class KycNationalityViewModel @Inject constructor(
    private val appContext: Context,
    private val userModel: UserModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val title = appContext.getString(R.string.kyc_nationality_title)

    val politicalExposed = ObservableBoolean()
    val usTax = ObservableBoolean()

    private var nationality = ""
    val nationalityErrorHolder = SingleActionObservableString()

    internal fun getUserNationalityArrayIndex(user: User): Int {
        val lowerCasedArray = appContext.resources.getStringArray(R.array.countries)
            .map { it.toLowerCase(Locale.getDefault()) }
        val index = lowerCasedArray.indexOf(user.prefix.toLowerCase(Locale.getDefault()))
        return if (index < 0) 0 else index
    }

    internal fun setNationality(position: Int) {
        nationality = appContext.getSortedCountries().getOrElse(position) { "" }
    }

    internal fun validateNationalityField(): Boolean{
        val nationalityError = when {
            nationality.isEmpty() -> getErrorEmptyString(appContext)
            else -> ""
        }
        if (nationalityError.isNotEmpty())
            nationalityErrorHolder.set(nationalityError)

        return nationalityError.isEmpty()
    }

    private val userMeResult = SingleActionLiveData<Result<User>>()
    internal fun getUserMeResult(): LiveData<Result<User>> = userMeResult

    private fun getUser() {
        viewModelScope.launch {
            val result = asyncLoading {
                userModel.getUserData()
            }
            userMeResult.postAction(result)
        }
    }

    internal fun loadUserOrRestoreState() {
        if (nationality.isEmpty())
            getUser()
    }

    internal fun appendInformationTo(
        webIdUserRequestBody: WebIdUserRequestBody,
        politicallyExposedPerson: Boolean,
        uSTaxLiability: Boolean
    ): WebIdUserRequestBody {
        return webIdUserRequestBody.copy(
            webIdPersonalInfo = webIdUserRequestBody.webIdPersonalInfo.copy(
                nationality = fromDisplayValue(appContext, nationality).toString(),
                isPoliticallyExposedPerson = politicallyExposedPerson,
                isUsTaxLiability = uSTaxLiability
            )
        )
    }

    internal fun setToggleButtonState(politically: Boolean, taxLiability: Boolean) {
        politicalExposed.set(politically)
        usTax.set(taxLiability)
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        usTax.set(false)
        politicalExposed.set(false)
    }
}