package app.eightpitch.com.ui.payment.directdebitcompleate

import android.os.Bundle
import android.view.View
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.base.rootbackhandlers.ChildNavGraphBackPressureHandler
import app.eightpitch.com.data.dto.investment.ProjectInvestmentInfoResponse
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.payment.paymentconfirmation.PaymentConfirmedFragment
import kotlinx.android.synthetic.main.fragment_direct_debit_payment.*

class DirectDebitPaymentFragment : BaseFragment<DirectDebitPaymentFragmentViewModel>(),
    ChildNavGraphBackPressureHandler {

    companion object {

        internal const val DEBIT_PAYMENTS_DATA = "DEBIT_PAYMENTS_DATA"

        fun buildWithAnArguments(
            projectInvestmentInfo: ProjectInvestmentInfoResponse
        ) = Bundle().apply {
            putParcelable(DEBIT_PAYMENTS_DATA, projectInvestmentInfo)
        }
    }

    private val projectInvestmentInfo: ProjectInvestmentInfoResponse
        get() = arguments?.getParcelable(DEBIT_PAYMENTS_DATA) ?: ProjectInvestmentInfoResponse()

    override fun getLayoutID() = R.layout.fragment_direct_debit_payment

    override fun getVMClass() = DirectDebitPaymentFragmentViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        checkOutButton.setOnClickListener {
            if (viewModel.validateInput()) {
                viewModel.saveSecupayData(projectInvestmentInfo.unprocessedInvestment.investmentId)
            }
        }

        viewModel.getUserMeResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) { user ->
                viewModel.apply {
                    setFields(user, projectInvestmentInfo)
                }
            }
        })

        viewModel.getSaveSecupayDataResult().observe(viewLifecycleOwner, { result ->
            handleResultWithError(result, completeIfSuccess = {
                navigate(
                    R.id.action_directDebitPaymentFragment_to_paymentConfirmedFragment,
                    PaymentConfirmedFragment.buildWithAnArguments(projectInvestmentInfo.getProjectName())
                )
            }, completeIfError = {
                navigate(R.id.action_directDebitPaymentFragment_to_paymentDeclinedFragment)
            })
        })

        viewModel.getUser()
    }

    override fun handleBackPressure(): Boolean {
        return popBackStack()
    }
}