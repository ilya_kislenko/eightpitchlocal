package app.eightpitch.com.ui.pincode

import android.app.Activity.RESULT_CANCELED
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.GradientDrawable.OVAL
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.view.isVisible
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.authorized.AuthorizedActivity
import app.eightpitch.com.ui.pincode.PinCodeScreenState.CREATE_CODE
import app.eightpitch.com.ui.pincode.PinCodeScreenState.ENTER_ACTUALLY_CODE
import app.eightpitch.com.ui.pincode.PinCodeScreenState.ENTER_CODE
import app.eightpitch.com.ui.pincode.PinCodeScreenState.REPEAT_CODE
import app.eightpitch.com.ui.registration.RegistrationActivity
import app.eightpitch.com.utils.IntentsController
import kotlinx.android.synthetic.main.fragment_pin_code_screen.*

class PinCodeFragment : BaseFragment<PinCodeViewModel>() {

    companion object {

        const val NAVIGATION_KEY = "NAVIGATION_KEY"
        const val PIN_CODE_KEY = "PIN_CODE_KEY"
        const val REASON_KEY = "REASON_KEY"

        const val ACTIVATE_BIOMETRIC = "ACTIVATE_BIOMETRIC"
        const val DELETE_CODE = "DELETE_CODE"

        fun buildWithArguments(
            navigation: String,
            pinCode: String = "",
            reason: String = "",
        ) = Bundle().apply {
            putString(NAVIGATION_KEY, navigation)
            putString(PIN_CODE_KEY, pinCode)
            putString(REASON_KEY, reason)
        }
    }

    private val navigationKey: String
        get() = arguments?.getString(NAVIGATION_KEY).orDefaultValue(ENTER_CODE.toString())

    private val pinCode: String
        get() = arguments?.getString(PIN_CODE_KEY).orDefaultValue("")

    private val reason: String
        get() = arguments?.getString(REASON_KEY).orDefaultValue("")

    override fun getLayoutID() = R.layout.fragment_pin_code_screen

    override fun getVMClass() = PinCodeViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindLoadingIndicator()

        setupKeyBoardButtons()
        setupNavigation()
        setProgressDrawables()
        setupHeader()

        backButton.setOnClickListener {
            if (!popBackStack())
                finishActivity()
        }
    }

    private fun setupNavigation() {
        viewModel.run {
            when (navigationKey) {
                CREATE_CODE.toString() -> {
                    onInputEndedHandler = when (reason) {
                        ACTIVATE_BIOMETRIC -> {
                            {
                                navigate(R.id.action_fingerPrintWithEnterPinCodeFragment_to_enterPinCodeFragment,
                                    buildWithArguments(REPEAT_CODE.toString(), pinCode.code, ACTIVATE_BIOMETRIC))
                            }
                        }
                        else -> {
                            {
                                navigate(R.id.action_createPinCodeFragment_to_enterPinCodeFragment,
                                    buildWithArguments(REPEAT_CODE.toString(), pinCode.code))
                            }
                        }
                    }
                }
                REPEAT_CODE.toString() -> {
                    onInputEndedHandler = when (reason) {
                        ACTIVATE_BIOMETRIC -> {
                            {
                                if (isCodesMatch(this@PinCodeFragment.pinCode))
                                    startBiometrics(
                                        onSuccess = {
                                            savePinCode()
                                            enableBiometric(true)
                                            activity?.setResult(RESULT_OK)
                                            navigate(R.id.action_enterPinCodeFragment_to_biometricSetUpFragment)
                                        },
                                        onFailure = {
                                            activity?.setResult(RESULT_CANCELED)
                                            finishActivity()
                                        },
                                        onBiometricDismiss = {
                                            viewModel.clearPinCode()
                                        })
                            }
                        }
                        else -> {
                            {
                                if (isCodesMatch(this@PinCodeFragment.pinCode)) {
                                    savePinCode()
                                    activity?.setResult(RESULT_OK)
                                    navigate(R.id.action_enterPinCodeFragment_to_pinCodeSetUpFragment)
                                }
                            }
                        }
                    }
                }
                ENTER_CODE.toString() -> enterCodeFlow()
                ENTER_ACTUALLY_CODE.toString() -> enterActuallyCodeFlow()
            }
        }
    }

    private fun enterCodeFlow() {
        viewModel.run {
            usePasswordVisibility.set(true)
            useLoginAndPassword.setDisablingClickListener {
                context?.let {
                    IntentsController.startActivityWithANewStack(it, RegistrationActivity::class.java)
                }
            }

            onInputEndedHandler = {
                if (isActualCode())
                    goToAuthZone()
            }
            buttonFingerPrint.setDisablingClickListener {
                startBiometrics(onSuccess = {
                    goToAuthZone()
                })
            }
            setupBiometricsVisibility()
            if (useBiometryVisibility.get())
                startBiometrics(onSuccess = {
                    goToAuthZone()
                })
            backButton.isVisible = false
        }
    }

    private fun enterActuallyCodeFlow() {
        viewModel.run {
            onInputEndedHandler = {
                if (isActualCode()) {
                    when (reason) {
                        ACTIVATE_BIOMETRIC -> {
                            startBiometrics(
                                onSuccess = {
                                    enableBiometric(true)
                                    activity?.setResult(RESULT_OK)
                                    navigate(R.id.action_activateFingerPrint_to_fingerPrintSetUpFragment)
                                },
                                onFailure = {
                                    finishActivity()
                                },
                                onBiometricDismiss = {
                                    viewModel.clearPinCode()
                                })
                        }
                        DELETE_CODE -> {
                            deletePinCode()
                            activity?.setResult(RESULT_OK)
                            finishActivity()
                        }
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_BIOMETRIC_SUCCESS ->
                if (resultCode == RESULT_OK && data != null) {
                    goToAuthZone()
                }
            REQUEST_SETTINGS_FINGERPRINT -> startBiometrics(onSuccess = {
                viewModel.enableBiometric(true)
                activity?.setResult(RESULT_OK)
                navigate(R.id.action_activateFingerPrint_to_fingerPrintSetUpFragment)
            },
                onFailure = {
                    finishActivity()
                })
        }
    }

    private fun setProgressDrawables() {
        val imageArray = arrayListOf(firstField, secondField, thirdField, fourthField)
        for (i in 0 until 4) {
            imageArray[i].setImageDrawable(GradientDrawable().apply {
                shape = OVAL
                val size = resources.getDimension(R.dimen.margin_24).toInt()
                setSize(size, size)
            })
        }
    }

    private fun setupHeader() {
        viewModel.setHeader(when (navigationKey) {
            CREATE_CODE.toString() -> getString(R.string.create_your_pin_code_text)
            REPEAT_CODE.toString() -> getString(R.string.repeat_pin_code_text)
            ENTER_CODE.toString() -> getString(R.string.enter_your_pin_code_text)
            ENTER_ACTUALLY_CODE.toString() -> getString(R.string.enter_your_actual_pin_code_text)
            else -> getString(R.string.enter_your_pin_code_text)
        })
    }

    private val digitListener = View.OnClickListener { view ->
        (view as? AppCompatTextView)?.text?.let {
            viewModel.inputPinCode(it.toString())
        }
    }

    private fun setupKeyBoardButtons() {
        buttonOne.setOnClickListener(digitListener)
        buttonTwo.setOnClickListener(digitListener)
        buttonThree.setOnClickListener(digitListener)
        buttonFour.setOnClickListener(digitListener)
        buttonFive.setOnClickListener(digitListener)
        buttonSix.setOnClickListener(digitListener)
        buttonSeven.setOnClickListener(digitListener)
        buttonEight.setOnClickListener(digitListener)
        buttonNine.setOnClickListener(digitListener)
        buttonZero.setOnClickListener(digitListener)

        buttonDeleteSymbol.setOnClickListener {
            viewModel.removeLastPinCodeDigit()
        }

        buttonFingerPrint.setOnClickListener {
            startBiometrics(onSuccess = {
                goToAuthZone()
            })
        }
    }

    private fun goToAuthZone() {
        context?.let {
            IntentsController.startActivityWithANewStack(it,
                AuthorizedActivity::class.java)
        }
    }
}