package app.eightpitch.com.ui.investment.choosedocuments

import android.content.Context
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.models.DocumentsModel
import app.eightpitch.com.utils.Empty
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.coroutines.launch

class ProjectDocumentsViewModel(
    appContext: Context,
    private val documentsModel: DocumentsModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val toolbarTitle = appContext.getString(R.string.get_project_documents_text_title)
    val buttonAvailable = ObservableBoolean()
    internal fun changeAvailability(available: Boolean) {
        buttonAvailable.set(available)
    }

    val files = ObservableArrayList<String>()
    internal fun setupFiles(list: List<String>) {
        files.apply {
            clear()
            addAll(list)
        }
    }

    private val sendingResult = SingleActionLiveData<Result<Empty>>()
    internal fun getSendingResult(): LiveData<Result<Empty>> = sendingResult

    internal fun sendDocumentsByEmail(
        projectId: String,
        investmentId: String
    ) {
        viewModelScope.launch {
            val result = asyncLoading {
                documentsModel.sendDocumentsByEmail(projectId, investmentId)
            }
            sendingResult.postAction(result)
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        files.clear()
        buttonAvailable.set(false)
    }
}