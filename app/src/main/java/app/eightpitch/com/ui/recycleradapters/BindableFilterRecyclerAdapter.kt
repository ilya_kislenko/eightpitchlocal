package app.eightpitch.com.ui.recycleradapters

import androidx.recyclerview.widget.RecyclerView

abstract class BindableFilterRecyclerAdapter<FilterData: Any ,DataType : Any, ViewHolderType : RecyclerView.ViewHolder> : RecyclerView.Adapter<ViewHolderType>(),
    BindableAdapter<DataType>, BindableFilterAdapter<FilterData> {

    protected lateinit var dataList: MutableList<DataType>

    protected lateinit var filterDataList: MutableList<FilterData>

    override fun setData(data: MutableList<DataType>) {
        dataList = data
        applyState()
        notifyDataSetChanged()
    }

    override fun setFilterData(data: MutableList<FilterData>) {
        filterDataList = data
        applyState()
        notifyDataSetChanged()
    }

    /**
     * Callback to modify received data right before adapter starting to bind the views
     */
    protected open fun applyState(){}

    protected fun getDataItem(adapterPosition : Int) = dataList[adapterPosition]

    override fun getItemCount() = if(::dataList.isInitialized) dataList.size else 0
}