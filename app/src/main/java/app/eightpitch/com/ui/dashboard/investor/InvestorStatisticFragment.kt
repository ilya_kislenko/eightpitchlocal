package app.eightpitch.com.ui.dashboard.investor

import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.GradientDrawable.RECTANGLE
import android.graphics.drawable.StateListDrawable
import android.os.*
import android.view.*
import android.widget.ProgressBar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.MATCH_CONSTRAINT
import androidx.constraintlayout.widget.ConstraintSet
import androidx.constraintlayout.widget.ConstraintSet.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView.HORIZONTAL
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.investment.GraphsInvestment
import app.eightpitch.com.data.dto.user.LinkSliderItemResponseBody
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.dashboard.*
import app.eightpitch.com.ui.projectsdetail.ProjectDetailsFragment
import app.eightpitch.com.utils.SafeLinearLayoutManager
import app.eightpitch.com.utils.Timer
import com.google.android.material.tabs.*
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import kotlinx.android.synthetic.main.fragment_investor_statistic.*
import kotlinx.android.synthetic.main.statistic_graph_layout.*
import java.math.BigDecimal
import java.math.RoundingMode.HALF_DOWN
import java.util.*

class InvestorStatisticFragment : BaseFragment<InvestorStatisticViewModel>() {

    private val timer = Timer()
    private var tabListener: OnTabSelectedListener? = OnProjectsTabListener()

    override fun getLayoutID() = R.layout.fragment_investor_statistic

    override fun getVMClass() = InvestorStatisticViewModel::class.java

    override fun onResume() {
        super.onResume()
        viewModel.retrieveData()
        timer.startTimer(8)
        projectChangeModeTabLayout.selectTabWith(viewModel.savedTabPosition)
    }

    override fun onPause() {
        super.onPause()
        timer.stopTimer()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindUI()
        bindLoadingIndicator()

        viewModel.getInvestorGraphResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) { investorGraph ->
                val colorList =
                    viewModel.getRandomColorList(investorGraph.investments.size)
                (statisticRecycler.adapter as? CardInvestmentAdapter)?.updateAttributes(
                    colorList, investorGraph.totalMonetaryAmount)
                setupHorizontalProgressBarsGraphic(investorGraph.investments,
                    investorGraph.totalMonetaryAmount, colorList)
                projectChangeModeTabLayout.selectTabWith(viewModel.savedTabPosition)
            }
        })

        viewModel.getSliderResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) { listSliders ->
                setupSlider(listSliders)
                startSwap()
            }
        })

        viewModel.geTabLayoutVisibilityResult().observe(viewLifecycleOwner, {
            when (it) {
                true -> addTabListener()
                else -> removeTabListener()
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        tabListener = null
    }

    private fun bindUI() {
        setupRecyclers()
        timer.timerListener = TimerListener()
        viewModel.addTabLayoutListener(viewLifecycleOwner)
    }

    private fun addTabListener() = tabListener?.let { projectChangeModeTabLayout.addOnTabSelectedListener(it) }
    private fun removeTabListener() = tabListener?.let { projectChangeModeTabLayout.removeOnTabSelectedListener(it) }

    private fun setupSlider(listSliders: List<LinkSliderItemResponseBody>) {
        val list = arrayListOf(LinkSliderItemResponseBody(header = viewModel.welcomeText.get() ?: "")).apply {
            addAll(listSliders)
        }.toList()

        val fragments = arrayListOf<Fragment>()

        for (i in list.indices) {
            tabLayout.apply { addTab(newTab()) }
            fragments.add(InvestorDashboardSliderFragment.createFragmentWithArguments(list[i]))
        }

        sliderView.adapter = DashboardSliderAdapter(this@InvestorStatisticFragment, fragments)

        if (list.size == 1)
            return

        TabLayoutMediator(tabLayout, sliderView) { tab, _ ->
            tab.customView = createSliderView()
        }.attach()
    }

    private fun createSliderView() = View(context).apply {
        val (width, height) = resources.run {
            getDimensionPixelOffset(R.dimen.tabLineWidth) to getDimensionPixelOffset(R.dimen.tabLineHeight)
        }
        layoutParams = ViewGroup.LayoutParams(width, height)
        background = StateListDrawable().apply {
            context.also {
                addState(intArrayOf(android.R.attr.state_selected),
                    it.createGradientDrawable(RECTANGLE, R.color.lightRed))
                addState(intArrayOf(),
                    it.createGradientDrawable(RECTANGLE, R.color.white))
            }
        }
    }

    private fun setupRecyclers() {
        statisticRecycler.apply {
            adapter = CardInvestmentAdapter()
            layoutManager = SafeLinearLayoutManager(context, HORIZONTAL, false)
        }

        projectsRecycler.apply {
            layoutManager = SafeLinearLayoutManager(context, VERTICAL, false)
            adapter = ProjectInvestorDashboardAdapter().apply {
                onProjectClicked = { projectItem ->
                    navigate(R.id.action_investmentStatisticFragment_to_projectDetailsFragment,
                        ProjectDetailsFragment.buildWithAnArguments(projectItem.id.toString()))
                }
                setHasStableIds(true)
                setHasFixedSize(true)
            }
        }
    }

    //region Dynamic investment progress bars drawing
    private fun setupHorizontalProgressBarsGraphic(
        listGraphs: List<GraphsInvestment>,
        totalMonetaryAmount: BigDecimal,
        colorList: List<String>,
    ) {
        (statisticView as? ConstraintLayout)?.let { parentLayout ->
            val bars = buildInvestmentsProgressBars(colorList, totalMonetaryAmount, listGraphs)
            bars.reversed().forEach {
                parentLayout.addView(it)
                it.setInvestmentProgressBarConstraints(parentLayout)
            }
        }
    }

    private fun buildInvestmentsProgressBars(
        colorList: List<String>,
        totalMonetaryAmount: BigDecimal,
        listGraphs: List<GraphsInvestment>,
    ): List<ProgressBar> {
        var previousPercentage = 0
        val investmentProgressBars = mutableListOf<ProgressBar>()
        for (i in listGraphs.indices) {
            val progressBar = ProgressBar(
                context, null, R.style.Widget_AppCompat_ProgressBar_Horizontal).apply {
                id = View.generateViewId()
                max = 10000
                progress = previousPercentage + countPercentage(totalMonetaryAmount, listGraphs[i].monetaryAmount)
                previousPercentage = progress
                progressDrawable =
                    ContextCompat.getDrawable(context, R.drawable.progress_digital_farm_background).apply {
                        this?.setColorFilter(Color.parseColor(colorList[i]), PorterDuff.Mode.SRC_OVER)
                    }
            }
            investmentProgressBars.add(progressBar)
        }
        return investmentProgressBars
    }

    private fun countPercentage(sum: BigDecimal, part: BigDecimal): Int {
        return part.multiplySafe(10000).divideSafe(sum).setScale(0, HALF_DOWN).toInt()
    }

    private fun View.setInvestmentProgressBarConstraints(constraintLayout: ConstraintLayout) {
        ConstraintSet().apply {
            this@setInvestmentProgressBarConstraints.id.also {
                clone(constraintLayout)
                connect(it, END, headerText.id, END)
                connect(it, START, headerText.id, START)
                connect(it, TOP, totalInvestmentText.id, BOTTOM, resources.getDimensionPixelSize(R.dimen.margin_12))
                constrainWidth(it, MATCH_CONSTRAINT)
                constrainHeight(it, resources.getDimensionPixelSize(R.dimen.progress_bar_height))
                applyTo(constraintLayout)
            }
        }
    }
    //endregion

    private inner class OnProjectsTabListener : OnTabSelectedListener {

        override fun onTabReselected(tab: TabLayout.Tab?) {
            loadProjectsWithType(tab)
        }

        override fun onTabUnselected(tab: TabLayout.Tab?) {
            /*Do nothing*/
        }

        override fun onTabSelected(tab: TabLayout.Tab?) {
            loadProjectsWithType(tab)
        }
    }

    private fun loadProjectsWithType(selectedTab: TabLayout.Tab?) {
        selectedTab?.let { viewModel.getProjectsForType(it.position) }
    }

    private inner class TimerListener : Timer.TimerListener {

        override fun countProcessing(value: Int) {
            //Nothing.
        }

        override fun countFinished() {
            if (isAliveAndAvailable()) {
                sliderView.selectNextItem()
                timer.startTimer(8)
            }
        }
    }

    private fun startSwap() {
        if (sliderView.adapter != null)
            timer.startTimer(8)
    }
}