package app.eightpitch.com.ui.kyc.street

import android.os.Bundle
import android.view.View
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.kyc.KycInformationRequestBody
import app.eightpitch.com.data.dto.webid.WebIdUserRequestBody
import app.eightpitch.com.ui.kyc.financial.KycFinancialInfoFragment
import kotlinx.android.synthetic.main.fragment_pvi_personal_info.*


class KycStreetInfoFragment : BaseFragment<KycStreetInfoFragmentViewModel>() {

    companion object {

        internal const val KYC_BODY = "KYC_BODY"

        fun buildWithAnArguments(
            webIdUserRequestBody: WebIdUserRequestBody
        ) = Bundle().apply {
            putParcelable(KYC_BODY, webIdUserRequestBody)
        }
    }

    override fun getLayoutID() = R.layout.fragment_kyc_street_info

    override fun getVMClass() = KycStreetInfoFragmentViewModel::class.java

    private val webIdUserRequestBody: WebIdUserRequestBody
        get() = arguments?.getParcelable(KYC_BODY) ?: WebIdUserRequestBody()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        buttonNext.setOnClickListener {
            if (viewModel.validateInput()) {
                val kycArguments = viewModel.addStreetInfoToKycBody(webIdUserRequestBody)
                navigate(R.id.action_kycStreetInfoFragment_to_kycFinancialInfoFragment,
                    KycFinancialInfoFragment.buildWithAnArguments(kycArguments)
                )
            }
        }
    }
}