package app.eightpitch.com.ui.profilepayments.tranferdetails

import android.os.Bundle
import android.view.View
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.profiletransfers.TransferDetails
import app.eightpitch.com.extensions.showDialog
import kotlinx.android.synthetic.main.fragment_transfer_details.*

class TransferDetailsFragment : BaseFragment<TransferDetailsViewModel>() {

    companion object {

        internal const val TRANSFER_DETAILS_KEY = "TRANSFER_DETAILS_KEY"

        fun buildWithAnArguments(
            transferDetails: TransferDetails,
        ) = Bundle().apply {
            putParcelable(TRANSFER_DETAILS_KEY, transferDetails)
        }
    }

    private var paymentDetailClickListener: PaymentDetailClickListener? = null

    private val transferDetails: TransferDetails
        get() = arguments?.getParcelable(TRANSFER_DETAILS_KEY) ?: TransferDetails()

    override fun getLayoutID() = R.layout.fragment_transfer_details

    override fun getVMClass() = TransferDetailsViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()
        paymentDetailClickListener = PaymentDetailClickListener()

        setupListeners()

        viewModel.setupPaymentDetailsBinding(transferDetails)
    }

    private fun setupListeners() {
        dateTimeLabel.setOnClickListener(paymentDetailClickListener)
        companyNameLabel.setOnClickListener(paymentDetailClickListener)
        businessAddressLabel.setOnClickListener(paymentDetailClickListener)
        isinLabel.setOnClickListener(paymentDetailClickListener)
        tokenAmountLabel.setOnClickListener(paymentDetailClickListener)
        userFromLabel.setOnClickListener(paymentDetailClickListener)
        userToLabel.setOnClickListener(paymentDetailClickListener)
    }

    private inner class PaymentDetailClickListener : View.OnClickListener {
        override fun onClick(view: View) {
            val (title, message, negativeListener) = when (view.id) {
                R.id.dateTimeLabel -> {
                    val dateTime = viewModel.dateTimeField.get() ?: ""
                    Triple(
                        getString(R.string.date_time),
                        dateTime
                    ) {
                        viewModel.copyToClipboard(dateTime)
                    }
                }
                R.id.companyNameLabel -> {
                    val companyName = transferDetails.companyName
                    Triple(
                        getString(R.string.company_name),
                        companyName
                    ) {
                        viewModel.copyToClipboard(companyName)
                    }
                }
                R.id.businessAddressLabel -> {
                    val address = transferDetails.companyAddress
                    Triple(
                        getString(R.string.business_address_text),
                        address
                    ) {
                        viewModel.copyToClipboard(address)
                    }
                }
                R.id.isinLabel -> {
                    val isin = transferDetails.isin
                    Triple(
                        getString(R.string.isin_text),
                        isin
                    ) {
                        viewModel.copyToClipboard(isin)
                    }
                }
                R.id.tokenAmountLabel -> {
                    val amount = viewModel.getTokenAmount(transferDetails)
                    Triple(
                        getString(R.string.security_token_amount),
                        amount
                    ) {
                        viewModel.copyToClipboard(amount)
                    }
                }
                R.id.userFromLabel -> {
                    val userFromFullName = transferDetails.fromUserFullName
                    Triple(
                        getString(R.string.from_text),
                        userFromFullName
                    ) {
                        viewModel.copyToClipboard(userFromFullName)
                    }
                }
                R.id.userToLabel -> {
                    val toUserFullName = transferDetails.toUserFullName
                    Triple(
                        getString(R.string.to_text),
                        toUserFullName
                    ) {
                        viewModel.copyToClipboard(toUserFullName)
                    }
                }
                else -> Triple("", "") {}
            }
            showInfoDialog(title, message, negativeListener)
        }
    }

    private fun showInfoDialog(
        title: String,
        message: String,
        negativeListener: () -> Unit,
    ) {
        val dialogPositiveButtonText = getString(R.string.ok_text)
        val dialogNegativeButtonText = getString(R.string.copy_text)
        showDialog(
            title = title,
            message = message,
            positiveButtonMessage = dialogPositiveButtonText,
            cancelable = false,
            negativeButtonMessage = dialogNegativeButtonText,
            negativeListener = { negativeListener.invoke() }
        )
    }
}