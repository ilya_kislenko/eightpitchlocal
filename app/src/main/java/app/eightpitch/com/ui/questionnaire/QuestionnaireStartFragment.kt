package app.eightpitch.com.ui.questionnaire

import android.os.Bundle
import android.view.*
import app.eightpitch.com.R
import app.eightpitch.com.base.BackPressureFragment
import app.eightpitch.com.extensions.*
import kotlinx.android.synthetic.main.fragment_questionnaire_start_screen.*

class QuestionnaireStartFragment : BackPressureFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_questionnaire_start_screen, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        crossButton.setOnClickListener { moveBackward() }
        registerOnBackPressedListener { moveBackward() }

        nextButton.setOnClickListener {
            navigate(R.id.action_questionnaireStartFragment_to_investorQualificationFragment)
        }
    }
}
