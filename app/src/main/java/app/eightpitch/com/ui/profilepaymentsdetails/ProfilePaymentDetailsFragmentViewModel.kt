package app.eightpitch.com.ui.profilepaymentsdetails

import android.content.Context
import androidx.databinding.ObservableField
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.profilepayments.adapters.PaymentItem
import java.util.*
import javax.inject.Inject

class ProfilePaymentDetailsFragmentViewModel @Inject constructor(
    private val appContext: Context,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val title = appContext.getString(R.string.profile_payments_toolbar_title)

    //region Binding
    val companyNameField = ObservableField("")
    val amountInvestedField = ObservableField("")
    val paymentProviderField = ObservableField("")
    val tokenAmountField = ObservableField("")
    val isinField = ObservableField("")
    val transactionIdField = ObservableField("")
    val dateTimeField = ObservableField("")
    //endregion

    internal fun setupPaymentDetailsBinding(paymentItem: PaymentItem) {
        val monetaryAmountString = getMonetaryAmount(paymentItem)
        val tokenAmountString = getTokenAmount(paymentItem)

        paymentItem.apply {
            companyNameField.set(companyName)
            amountInvestedField.set(monetaryAmountString)
            paymentProviderField.set(paymentSystem)
            tokenAmountField.set(tokenAmountString)
            isinField.set(isin)
            transactionIdField.set(transactionId)
            dateTimeField.set(formattedDate)
        }
    }

    internal fun getMonetaryAmount(paymentItem: PaymentItem) = String.format(
        Locale.getDefault(),
        appContext.getString(R.string.euro_monetary_amount),
        paymentItem.monetaryAmount.formatToCurrencyView()
    )

    internal fun getTokenAmount(paymentItem: PaymentItem) = String.format(
        Locale.getDefault(),
        appContext.getString(R.string.token_amount_pattern),
        paymentItem.amount.formatToCurrencyView(),
        paymentItem.shortCut
    )

    internal fun copyToClipboard(text: String) {
        appContext.copyToClipboard(text)
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        companyNameField.set("")
        amountInvestedField.set("")
        paymentProviderField.set("")
        tokenAmountField.set("")
        isinField.set("")
        transactionIdField.set("")
        dateTimeField.set("")
    }
}
