package app.eightpitch.com.ui.payment.compleatepayment

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import app.eightpitch.com.BuildConfig
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.base.rootbackhandlers.ChildNavGraphBackPressureHandler
import app.eightpitch.com.data.dto.investment.ProjectInvestmentInfoResponse
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.payment.paymentconfirmation.PaymentConfirmedFragment
import app.eightpitch.com.utils.Constants.INVOICE_ENDPOINT
import kotlinx.android.synthetic.main.fragment_classic_bank_payment.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class ClassicBankPaymentFragment : BaseFragment<ClassicBankPaymentFragmentViewModel>(),
    ChildNavGraphBackPressureHandler {

    companion object {

        internal const val FROM_SOFT_CAP = "FROM_SOFT_CAP"
        internal const val FROM_SCREEN_DATA = "FROM_SCREEN_DATA"
        internal const val CLASSIC_PAYMENTS_DATA = "CLASSIC_PAYMENTS_DATA"

        fun buildWithAnArguments(
            projectInvestmentInfo: ProjectInvestmentInfoResponse,
            fromFragment: String = ""
        ) = Bundle().apply {
            putString(FROM_SCREEN_DATA, fromFragment)
            putParcelable(CLASSIC_PAYMENTS_DATA, projectInvestmentInfo)
        }
    }

    private val projectInvestmentInfo: ProjectInvestmentInfoResponse
        get() = arguments?.getParcelable(CLASSIC_PAYMENTS_DATA) ?: ProjectInvestmentInfoResponse()

    private val isSoftCap
        get() = (arguments?.getString(FROM_SCREEN_DATA) ?: "") == FROM_SOFT_CAP

    private val investmentId
        get() = projectInvestmentInfo.unprocessedInvestment.investmentId

    private var isCheckOutPerformed : Boolean = false

    override fun getLayoutID() = R.layout.fragment_classic_bank_payment

    override fun getVMClass() = ClassicBankPaymentFragmentViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        setListeners()

        viewModel.getPaymentDataResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) { paymentResponse ->
                viewModel.setFields(paymentResponse)
            }
        })

        viewModel.getCheckOutResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) {
                isCheckOutPerformed = true
                exitButton.isVisible = true
                checkOutButton.isVisible = false
            }
        })

        viewModel.apply {
            getPaymentFieldsData(investmentId, isSoftCap)
            setPaymentData(projectInvestmentInfo)
        }
    }

    override fun handleBackPressure() = if (isCheckOutPerformed) {
        popBackStack(R.id.projectDetailsFragment)
    } else {
        popBackStack()
    }

    private fun setListeners() {
        additionalToolbarRightButton.apply {
            setImageResource(R.drawable.icon_download)
            setOnClickListener {
                grantWriteStoragePermission {
                    viewModel.downloadInvoice("$INVOICE_ENDPOINT$investmentId")
                }
            }
        }

        checkOutButton.setOnClickListener {
            isCheckOutPerformed = false
            viewModel.checkOut(investmentId)
        }

        textInputLayoutIban.setEndIconClickListener {
            viewModel.copyIban(textInputLayoutIban.getText().toString())
        }

        textInputLayoutBic.setEndIconClickListener {
            viewModel.copyBic(textInputLayoutBic.getText().toString())
        }

        textInputLayoutPaymentReferenceNumber.setEndIconClickListener {
            viewModel.copyPaymentReference(
                textInputLayoutPaymentReferenceNumber.getText().toString()
            )
        }

        exitButton.setOnClickListener {
            navigate(
                R.id.action_classicBankPaymentFragment_to_paymentConfirmedFragment,
                PaymentConfirmedFragment.buildWithAnArguments(projectInvestmentInfo.getProjectName())
            )
        }
    }

    private fun grantWriteStoragePermission(granted: () -> Unit) {
        activity?.let { fragmentActivity ->
            fragmentActivity.checkPermissions(
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                { granted.invoke() },
                { deniedPermissions ->
                    requestPermissions(
                        deniedPermissions,
                        REQUEST_WRITING_EXTERNAL_STORAGE
                    )
                })
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_WRITING_EXTERNAL_STORAGE) {
            if (grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
                viewModel.downloadInvoice("$INVOICE_ENDPOINT$investmentId")
            }
        }
    }
}