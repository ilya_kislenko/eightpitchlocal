package app.eightpitch.com.ui.registration

import android.os.Bundle
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseActivity

class RegistrationActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
    }
}