package app.eightpitch.com.ui.questionnaire

import android.os.Bundle
import android.view.View
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody.CREATOR.LESS_THAN_50
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody.CREATOR.MORE_THAN_50
import app.eightpitch.com.ui.questionnaire.QuestionnaireFirstFragment.Companion.QUESTIONNAIRE_KEY_CODE
import kotlinx.android.synthetic.main.fragment_questionnaire_two_answer.*

class QuestionnaireEighthFragment : BaseFragment<QuestionnaireViewModel>() {

    companion object {
        fun buildWithAnArguments(
            questionnaireRequestBody: QuestionnaireRequestBody
        ) = Bundle().apply {
            putParcelable(QUESTIONNAIRE_KEY_CODE, questionnaireRequestBody)
        }
    }

    private val questionnaireRequestBody: QuestionnaireRequestBody
        get() = arguments?.getParcelable(QUESTIONNAIRE_KEY_CODE) ?: QuestionnaireRequestBody()

    override fun getLayoutID() = R.layout.fragment_questionnaire_two_answer

    override fun getVMClass() = QuestionnaireViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        viewModel.setupLayout(
            9,
            getString(R.string.how_many_of_your_transactions_are_those_with_virtual_currencies_text),
            arrayListOf(
                getString(R.string.less_than_fifty_percent_text),
                getString(R.string.more_than_fifty_percent_text)
            )
        )

        backButton.setOnClickListener {
            popBackStack()
        }

        nextButton.setOnClickListener {
            navigate(
                R.id.action_questionnaireEighthFragment_to_questionnaireNinthFragment,
                QuestionnaireNinthFragment.buildWithAnArguments(
                    questionnaireRequestBody.copy(
                        virtualCurrenciesUsed = viewModel.getCheckedAnswer(
                            listOf(LESS_THAN_50, MORE_THAN_50), answersGroup
                        )
                    )
                )
            )
        }
    }
}