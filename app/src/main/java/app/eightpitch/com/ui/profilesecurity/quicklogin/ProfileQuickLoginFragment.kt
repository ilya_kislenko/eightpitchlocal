package app.eightpitch.com.ui.profilesecurity.quicklogin

import android.annotation.SuppressLint
import android.app.Activity.RESULT_CANCELED
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.View
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.profilesecurity.quicklogin.QuickLoginSettingActivity.Companion.QUICK_LOGIN_NAVIGATION_ID
import kotlinx.android.synthetic.main.fragment_profile_quick_login.*

class ProfileQuickLoginFragment : BaseFragment<ProfileQuickLoginViewModel>() {

    override fun getLayoutID() = R.layout.fragment_profile_quick_login

    override fun getVMClass() = ProfileQuickLoginViewModel::class.java

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.setupSwitchers()

        pinCodeSwitch.setOnClickListener {
            val isChecked = pinCodeSwitch.isChecked
            val (navigationID, requestCode) = if (isChecked) {
                R.id.createPinCodeFragment to PIN_CODE_ENABLE_KEY
            } else {
                R.id.deletePinCodeFragment to PIN_CODE_DISABLE_KEY
            }
            startActivityForResult(Intent(it.context, QuickLoginSettingActivity::class.java).apply {
                putExtra(QUICK_LOGIN_NAVIGATION_ID, navigationID)
            }, requestCode)
        }
        fingerPrintSwitch.setOnClickListener {
            if (viewModel.isQuickLoginActive()) {
                if (fingerPrintSwitch.isChecked)
                    startActivityForResult(Intent(it.context, QuickLoginSettingActivity::class.java).apply {
                        putExtra(QUICK_LOGIN_NAVIGATION_ID, R.id.activateFingerPrint)
                    }, BIOMETRIC_SET_UP_START)
                else {
                    viewModel.enableFingerPrint(false)
                }
            } else {
                startActivityForResult(Intent(it.context, QuickLoginSettingActivity::class.java).apply {
                    putExtra(QUICK_LOGIN_NAVIGATION_ID, R.id.fingerPrintWithEnterPinCodeFragment)
                }, BIOMETRIC_SET_UP_START)
                viewModel.enableFingerPrint(false)
                fingerPrintSwitch.isChecked = false
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            when (requestCode) {
                REQUEST_BIOMETRIC_SUCCESS -> viewModel.enableFingerPrint(fingerPrintSwitch.isChecked)
                REQUEST_START_BIOMETRIC_ENROLL, BIOMETRIC_SET_UP_START -> {
                    fingerPrintSwitch.isChecked = true
                    pinCodeSwitch.isChecked = true
                }
                PIN_CODE_ENABLE_KEY -> pinCodeSwitch.isChecked = true
                PIN_CODE_DISABLE_KEY -> {
                    pinCodeSwitch.isChecked = false
                    fingerPrintSwitch.isChecked = false
                }
            }
        } else if (resultCode == RESULT_CANCELED) {
            when (requestCode) {
                BIOMETRIC_SET_UP_START -> fingerPrintSwitch.isChecked = false
                else -> pinCodeSwitch.isChecked = false
            }
        }
    }
}