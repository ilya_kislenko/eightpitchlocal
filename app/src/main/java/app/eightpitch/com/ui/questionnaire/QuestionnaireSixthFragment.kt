package app.eightpitch.com.ui.questionnaire

import android.os.Bundle
import android.view.View
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody.CREATOR.LESS_THAN_5
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody.CREATOR.MORE_THAN_5
import app.eightpitch.com.ui.questionnaire.QuestionnaireFirstFragment.Companion.QUESTIONNAIRE_KEY_CODE
import kotlinx.android.synthetic.main.fragment_questionnaire_two_answer.*

class QuestionnaireSixthFragment : BaseFragment<QuestionnaireViewModel>() {

    companion object {
        fun buildWithAnArguments(
            questionnaireRequestBody: QuestionnaireRequestBody
        ) = Bundle().apply {
            putParcelable(
                QUESTIONNAIRE_KEY_CODE,
                questionnaireRequestBody
            )
        }
    }

    private val questionnaireRequestBody: QuestionnaireRequestBody
        get() = arguments?.getParcelable(QUESTIONNAIRE_KEY_CODE) ?: QuestionnaireRequestBody()

    override fun getLayoutID() = R.layout.fragment_questionnaire_two_answer

    override fun getVMClass() = QuestionnaireViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        viewModel.setupLayout(
            7,
            getString(R.string.how_many_times_per_year_do_you_invest_in_these_asset_classes_text),
            arrayListOf(
                getString(R.string.less_than_five_transactions_text),
                getString(R.string.more_than_five_transactions_text)
            )
        )

        backButton.setOnClickListener {
            popBackStack()
        }

        nextButton.setOnClickListener {
            navigate(
                R.id.action_questionnaireSixthFragment_to_questionnaireSeventhFragment,
                QuestionnaireSeventhFragment.buildWithAnArguments(
                    questionnaireRequestBody.copy(
                        investingFrequency = viewModel.getCheckedAnswer(
                            listOf(LESS_THAN_5, MORE_THAN_5),
                            answersGroup
                        )
                    )
                )
            )
        }
    }
}