package app.eightpitch.com.ui.investment

import androidx.lifecycle.ViewModel
import app.eightpitch.com.extensions.parseToBigDecimalSafe
import java.math.BigDecimal
import java.math.BigDecimal.ZERO

class ViewPagerSharableViewModel : ViewModel() {

    private var tokens: BigDecimal = ZERO

    internal fun setValue(amount: BigDecimal) {
        tokens = amount
    }

    internal fun getTokens(): BigDecimal = tokens.toPlainString().parseToBigDecimalSafe()
}