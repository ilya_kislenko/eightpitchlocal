package app.eightpitch.com.ui.deleteaccountconfirm.onelevel

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import app.eightpitch.com.R
import app.eightpitch.com.base.rootbackhandlers.ChildNavGraphBackPressureHandler
import app.eightpitch.com.ui.registration.RegistrationActivity
import app.eightpitch.com.utils.Constants.DELETE_REDIRECT_DURATION
import app.eightpitch.com.utils.IntentsController
import kotlinx.android.synthetic.main.fragment_delete_account_one_level_confirm.*

class DeleteAccountOneLevelConfirmFragment : Fragment(), ChildNavGraphBackPressureHandler {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View =
        inflater.inflate(R.layout.fragment_delete_account_one_level_confirm, container, false)

    private val handler = Handler(Looper.getMainLooper())
    private val loginScreenRunnable = Runnable {
        navigateToLoginScreen()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        crossButton.setOnClickListener {
            navigateToLoginScreen()
        }

        scheduleAccountDeletion()

        okButton.setOnClickListener {
            navigateToLoginScreen()
        }
    }

    private fun scheduleAccountDeletion() {
        handler.postDelayed(loginScreenRunnable, DELETE_REDIRECT_DURATION)
    }

    override fun handleBackPressure(): Boolean {
        navigateToLoginScreen()
        return true
    }

    private fun navigateToLoginScreen() {
        handler.removeCallbacks(loginScreenRunnable)
        context?.let {
            IntentsController.startActivityWithANewStack(
                it,
                RegistrationActivity::class.java
            )
        }
    }
}