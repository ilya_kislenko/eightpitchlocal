package app.eightpitch.com.ui.registration.signup.privateinvestor.phone

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.base.baseviewmodels.codeverification.DefaultCodeValidationFragment
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO.CREATOR.PHONE
import app.eightpitch.com.data.dto.signup.CommonInvestorRequestBody
import app.eightpitch.com.data.dto.user.CountryCode
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.ui.GenderSpinnerAdapter
import app.eightpitch.com.ui.webview.DefaultWebViewFragment
import app.eightpitch.com.utils.Constants
import kotlinx.android.synthetic.main.fragment_pvi_specify_phone_number.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class PVISpecifyPhoneNumberFragment : BaseFragment<PVISpecifyPhoneNumberViewModel>() {

    companion object {

        internal const val PARTIAL_USER = "PARTIAL_USER"

        fun buildWithAnArguments(
            partialUser: CommonInvestorRequestBody
        ) = Bundle().apply {
            putParcelable(PARTIAL_USER, partialUser)
        }
    }

    private val partialUser: CommonInvestorRequestBody
        get() = arguments?.getParcelable(PARTIAL_USER) ?: CommonInvestorRequestBody()

    override fun getLayoutID() = R.layout.fragment_pvi_specify_phone_number

    override fun getVMClass() = PVISpecifyPhoneNumberViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindLoadingIndicator()
        setupCountryCodeSpinner()

        additionalToolbarRightButton.setOnClickListener {
            val arguments = DefaultWebViewFragment.buildWithAnArguments(Constants.FAQ_URL)
            navigate(R.id.action_specifyPhoneNumberFragment_to_defaultWebViewFragment, arguments)
        }

        nextButton.setOnClickListener {
            viewModel.isPhoneNumberValid()
        }

        viewModel.getPhoneValidationResult.observe(viewLifecycleOwner, Observer { result ->
            handleResult(result) {
                viewModel.requestSMSCode()
            }
        })

        viewModel.getSMSResult().observe(viewLifecycleOwner, Observer { result ->
            handleResult(result) { smsResponse ->
                val interactionId = smsResponse.interactionId
                val arguments =
                    DefaultCodeValidationFragment.buildWithAnArguments(
                        validationScreenInfo = createValidationScreenInfo(), partialUser = partialUser.copy(
                            interactionId = interactionId, phoneNumber = viewModel.getUserPhoneNumberWithCode()
                        )
                    )
                navigate(R.id.action_specifyPhoneNumberFragment_to_digitCodeValidationFragment, arguments)
            }
        })
    }

    private fun setupCountryCodeSpinner() {
        codeSpinner.apply {
            val spinnerData = CountryCode.getPhoneCodes()
            val codeAdapter = GenderSpinnerAdapter(context, spinnerData)
            setAdapter(codeAdapter)
            setSelection(0)
            setSelectedListener({
                viewModel.setCountryCode(it)
            }, {})
        }
    }

    private fun createValidationScreenInfo() = ValidationScreenInfoDTO(
        verificationType = PHONE,
        validationButtonName = getString(R.string.confirm_text),
        header = getString(R.string.confirm_your_phone_number_text),
        toolbarTitle = getString(R.string.creating_account_title_text),
        subTitleMessage = getString(R.string.we_have_sent_sms_to_confirm_text)
    )
}