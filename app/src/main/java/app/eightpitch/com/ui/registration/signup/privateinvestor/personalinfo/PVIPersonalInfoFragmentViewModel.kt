package app.eightpitch.com.ui.registration.signup.privateinvestor.personalinfo

import android.content.Context
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.extensions.getErrorEmptyString
import app.eightpitch.com.extensions.validateByDefaultPattern
import app.eightpitch.com.utils.RegexPatterns.PATTERN_VALID_NAME
import app.eightpitch.com.utils.SingleActionObservableString
import app.eightpitch.com.views.interfaces.ShortFieldValidator
import javax.inject.Inject

class PVIPersonalInfoFragmentViewModel @Inject constructor(
    private val appContext: Context,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val title = appContext.getString(R.string.creating_account)

    private var firstName: String = ""
    val firstNameErrorHolder = SingleActionObservableString()

    val firstNameWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            firstName = sequence.toString()
        }
    }

    private var lastName: String = ""
    val lastNameErrorHolder = SingleActionObservableString()

    val lastNameWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            lastName = sequence.toString()
        }
    }

    private var genderType = ""
    val genderTypeErrorHolder = SingleActionObservableString()

    /**
     * Method that returns a First Name, Last Name, and Gender Type selected
     * by user in a one bunch
     */
    internal fun getPersonalInfo() = Triple(firstName, lastName, genderType)

    fun validateInput(): Boolean {

        val genderTypeError = when{
            genderType.isEmpty() -> getErrorEmptyString(appContext)
            else -> ""
        }
        if (genderTypeError.isNotEmpty())
            genderTypeErrorHolder.set(genderTypeError)

        val firstNameError =
            firstName.validateByDefaultPattern(appContext, R.string.first_name, PATTERN_VALID_NAME)
        if (firstNameError.isNotEmpty())
            firstNameErrorHolder.set(firstNameError)

        val secondNameError =
            lastName.validateByDefaultPattern(appContext, R.string.last_name, PATTERN_VALID_NAME)
        if (secondNameError.isNotEmpty())
            lastNameErrorHolder.set(secondNameError)

        return genderTypeError.isEmpty() && firstNameError.isEmpty() && secondNameError.isEmpty()
    }

    fun setGender(position: Int) {
        genderType = appContext.resources.getStringArray(R.array.genderType).getOrElse(position) { "" }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        lastName = ""
        firstName = ""
        genderType = ""
        genderTypeErrorHolder.set("")
        lastNameErrorHolder.set("")
        firstNameErrorHolder.set("")
    }
}