package app.eightpitch.com.ui.projects.adapters

import android.view.*
import androidx.appcompat.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.R
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.recycleradapters.BindableRecyclerAdapter
import app.eightpitch.com.utils.Constants.FILES_ENDPOINT
import app.eightpitch.com.views.HorizontalProgressBarView
import com.bumptech.glide.load.resource.bitmap.*
import java.util.*

class ProjectAdapter :
    BindableRecyclerAdapter<ProjectItem, ProjectAdapter.PagedProjectViewHolder>() {

    /**
     * Listener that represents a click on any project in the list,
     * will pass a [ProjectItem] when click is performed
     */
    internal lateinit var onProjectClicked: (ProjectItem) -> (Unit)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PagedProjectViewHolder(
        inflateView(
            parent,
            R.layout.item_view_project
        )
    )

    override fun onBindViewHolder(holder: PagedProjectViewHolder, position: Int) {

        val projectItem = getDataItem(position)

        holder.apply {
            val context = itemView.context
            context.loadImage("$FILES_ENDPOINT${projectItem.thumbNailImageUrl}",
                targetImageView = videoPreView,
                placeholderRef = R.drawable.icon_app_name_logo,
                transformation = arrayOf(
                    CenterCrop(),
                    GranularRoundedCorners(0F,
                        0F,
                        context.resources.getDimension(R.dimen.margin_10),
                        context.resources.getDimension(R.dimen.margin_10))
                )
            )
            setupProgressBarView(projectItem, holder.fundingProgressBar)
            typeOfSecurityTextView.text = projectItem.financialInformation.typeOfSecurity
            minimumAmountTextView.text = String.format(
                context.getString(R.string.token_placeholder),
                projectItem.tokenParametersDocument.run {
                    nominalValue.multiplySafe(minimumInvestmentAmount).toLong()
                }.asCurrency()
            )

            runningTimeTextView.text = projectItem.tokenParametersDocument.projectFinishDate.getDaysDuration(context)

            hardCapTextView.text = String.format(
                context.getString(R.string.token_placeholder),
                projectItem.tokenParametersDocument.hardCap.toLong().asCurrency()
            )
            describeProjectTextView.text = projectItem.description
            headerProjectTextView.text = projectItem.companyName

            parentView.apply {
                tag = projectItem
                setOnClickListener {
                    (it.tag as? ProjectItem)?.let(onProjectClicked)
                }
            }
        }
    }

    private fun setupProgressBarView(
        projectItem: ProjectItem,
        fundingProgressBar: HorizontalProgressBarView
    ) {

        val currentFundingSum = projectItem.graph.currentFundingSum
        fundingProgressBar.run {
            setCurrentProgress(currentFundingSum)
            setCurrentTextProgress(
                String.format(
                    Locale.getDefault(),
                    context.getString(R.string.percent_string),
                    currentFundingSum
                )
            )
        }

        val context = fundingProgressBar.context
        val (cap, textProgress) = if (projectItem.graph.softCap == 0 || projectItem.graph.softCap == null) {
            projectItem.graph.hardCap to context.getString(R.string.hard_cap)
        } else {
            projectItem.graph.softCap.orDefaultValue(0) to context.getString(R.string.soft_cap)
        }

        fundingProgressBar.run {
            setCapProgress(cap)
            setCapTextProgress(textProgress)
        }
    }

    class PagedProjectViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val fundingProgressBar: HorizontalProgressBarView =
            itemView.findViewById(R.id.fundingProgressBar)
        val parentView: ConstraintLayout = itemView.findViewById(R.id.parentView)
        val videoPreView: AppCompatImageView = itemView.findViewById(R.id.projectPreviewImage)
        val typeOfSecurityTextView: AppCompatTextView =
            itemView.findViewById(R.id.typeOfSecurityTextView)
        val minimumAmountTextView: AppCompatTextView =
            itemView.findViewById(R.id.minimumAmountTextView)
        val runningTimeTextView: AppCompatTextView =
            itemView.findViewById(R.id.runningTimeTextView)
        val hardCapTextView: AppCompatTextView =
            itemView.findViewById(R.id.hardCapTextView)
        val describeProjectTextView: AppCompatTextView =
            itemView.findViewById(R.id.describeProjectTextView)
        val headerProjectTextView: AppCompatTextView =
            itemView.findViewById(R.id.headerProjectTextView)
    }
}