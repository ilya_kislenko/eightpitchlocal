package app.eightpitch.com.ui.authorized

import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.base.preferences.PreferencesReader
import app.eightpitch.com.base.preferences.PreferencesWriter
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.notifications.Notification.Companion.NEW
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.models.NotificationsModel
import app.eightpitch.com.ui.authorized.main.MainScreenTab
import app.eightpitch.com.ui.authorized.main.MainScreenTab.PROJECTS
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.coroutines.launch

class AuthorizedActivityViewModel(
    private val preferencesReader: PreferencesReader,
    private val preferencesWriter: PreferencesWriter,
    private val notificationsModel: NotificationsModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    var selectedTab: MainScreenTab = PROJECTS
        private set

    internal fun onPageChanged(position: Int) {
        selectedTab = MainScreenTab.getTabByPosition(position)
    }

    private val notificationsStatus = SingleActionLiveData<Result<Boolean>>()
    internal fun getNotificationsStatusResult(): LiveData<Result<Boolean>> = notificationsStatus

    internal fun getNotificationsStatus() {

        viewModelScope.launch {

            val result = asyncLoading {
                val notifications = notificationsModel.retrieveNotifications()
                notifications.find { it.notificationStatus == NEW } != null
            }

            notificationsStatus.postAction(result)
        }
    }

    internal fun shouldShowBiometricDialog() = preferencesReader.run {
        !getUserBiometricInfo().hasPinCode && shouldShowQuickLoginDialog()
    }

    internal fun postponeQuickLogin() = preferencesWriter.cacheQuickLoginDialogState(false)
}