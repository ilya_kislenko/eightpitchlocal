package app.eightpitch.com.ui.payment.softcappaymentselection

import android.content.Context
import androidx.lifecycle.*
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.payment.TransactionInfoResponse
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.extensions.wrapWithAnEmptyResult
import app.eightpitch.com.models.InvestmentModel
import app.eightpitch.com.models.PaymentModel
import app.eightpitch.com.utils.Empty
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.coroutines.launch

class SoftCapPaymentSelectionViewModel(
    appContext: Context,
    private val paymentModel: PaymentModel,
    private val investmentModel: InvestmentModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val toolbarTitle = appContext.getString(R.string.payment_methods_text)

    private val transactionResult = SingleActionLiveData<Result<TransactionInfoResponse>>()
    fun getTransactionResult(): LiveData<Result<TransactionInfoResponse>> = transactionResult

    internal fun submitFintPayment(
        investmentId: String
    ) {
        viewModelScope.launch {
            val result = asyncLoading {
                paymentModel.submitFintPayment(investmentId)
            }
            transactionResult.postAction(result)
        }
    }

    private val projectInvestmentInfoResult = SingleActionLiveData<Result<Empty>>()
    internal fun getProjectInvestmentInfoResult(): LiveData<Result<Empty>> = projectInvestmentInfoResult

    internal fun getProjectInvestmentInfo(projectId: String) {
        viewModelScope.launch {
            val result = asyncLoading {
                investmentModel.getProjectInvestmentInfo(projectId)
            }
            projectInvestmentInfoResult.postAction(result.wrapWithAnEmptyResult())
        }
    }
}