package app.eightpitch.com.ui.projectfilter.filter

import android.content.Context
import android.view.View
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.filter.AmountRange
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.ACTIVE
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.COMING_SOON
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.FINISHED
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.MANUAL_RELEASE
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.REFUNDED
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.TOKENS_TRANSFERRED
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.WAIT_BLOCKCHAIN
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.WAIT_RELEASE
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.data.transmit.ResultStatus
import app.eightpitch.com.extensions.wrapWithAnEmptyResult
import app.eightpitch.com.models.FilterModel
import app.eightpitch.com.ui.projectfilter.SharableFilterViewModel
import app.eightpitch.com.utils.Empty
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

class FilterProjectFragmentViewModel @Inject constructor(
    private val appContext: Context,
    private val filterModel: FilterModel,
    threadsSeparator: ThreadsSeparator,
) : BaseViewModel(threadsSeparator) {

    val title = appContext.getString(R.string.projects_overview)

    var sharableFilterViewModel: SharableFilterViewModel? = null

    val statuses = ObservableArrayList<String>()
    val securities = ObservableArrayList<String>()
    val amountRanges = ObservableArrayList<AmountRange>()

    val statusVisibility = ObservableField(View.GONE)
    val rangeVisibility = ObservableField(View.GONE)
    val securityVisibility = ObservableField(View.GONE)
    val buttonTextField = ObservableField(appContext.getString(R.string.show_all_projects))

    private val approvedToShowStatuses: List<String> = listOf(
        COMING_SOON,
        ACTIVE,
        FINISHED
    )

    private val finishedStatusesGroup: List<String> = listOf(
        MANUAL_RELEASE,
        WAIT_RELEASE,
        REFUNDED,
        TOKENS_TRANSFERRED,
        WAIT_BLOCKCHAIN
    )

    internal fun setFilters(sharableFilterViewModel: SharableFilterViewModel){
        this.sharableFilterViewModel = sharableFilterViewModel
    }

    internal fun setupFilterParameters() {
        setupStateLists()
        setupRangeLists()
        setupSecurityLists()
        getFilteredProjects()
    }

    internal fun removeStatus(status: String) {
        sharableFilterViewModel?.statuses?.remove(status)

        if(status == FINISHED){
            finishedStatusesGroup.forEach {
                sharableFilterViewModel?.statuses?.remove(it)
            }
        }

        statusVisibility.set(if (sharableFilterViewModel?.statuses?.isEmpty() == true) View.GONE else View.VISIBLE)
        setupStateLists()
        getFilteredProjects()
    }

    internal fun removeAmountRange(range: AmountRange) {
        sharableFilterViewModel?.removeAmountRange(range)
        rangeVisibility.set(if (sharableFilterViewModel?.amountRanges?.isEmpty() == true) View.GONE else View.VISIBLE)
        setupRangeLists()
        getFilteredProjects()
    }

    internal fun removeSecurity(security: String) {
        sharableFilterViewModel?.removeSecurityTypes(security)
        securityVisibility.set(if (sharableFilterViewModel?.securityTypes?.isEmpty() == true) View.GONE else View.VISIBLE)
        setupSecurityLists()
        getFilteredProjects()
    }

    private fun setupRangeLists() {
        rangeVisibility.set(if (sharableFilterViewModel?.amountRanges?.isEmpty() == true) View.GONE else View.VISIBLE)
        amountRanges.apply {
            clear()
            addAll(sharableFilterViewModel?.amountRanges?: emptyList())
        }
    }

    private fun setupStateLists() {
        statusVisibility.set(if (sharableFilterViewModel?.statuses?.isEmpty() == true) View.GONE else View.VISIBLE)
        statuses.apply {
            clear()
            addAll(sharableFilterViewModel?.statuses?.filter {
                approvedToShowStatuses.contains(it)
            }?: emptyList())
        }
    }

    private fun setupSecurityLists() {
        securityVisibility.set(if (sharableFilterViewModel?.securityTypes?.isEmpty() == true) View.GONE else View.VISIBLE)
        securities.apply {
            clear()
            addAll(sharableFilterViewModel?.securityTypes?: emptyList())
        }
    }

    internal fun resetAllFilter() {
        sharableFilterViewModel?.apply {
            clearStatuses()
            clearRanges()
            clearSecurities()
        }
        setupFilterParameters()
    }


    private val filteredProjectResult = SingleActionLiveData<Result<Empty>>()
    internal fun getFilteredProjectResult(): LiveData<Result<Empty>> =
        filteredProjectResult

    private fun getFilteredProjects() {

        viewModelScope.launch {

            val result = asyncLoading {
                filterModel.getFilteredProject(
                    minimumAmounts = sharableFilterViewModel?.amountRanges?: emptyList(),
                    projectStatuses = sharableFilterViewModel?.statuses?: emptyList(),
                    typesOfSecurity = sharableFilterViewModel?.securityTypes?: emptyList()
                )
            }

            if (result.status == ResultStatus.SUCCESS) {
                if (result.result?.content?.isEmpty() == true || sharableFilterViewModel?.isEmptyFilter == true) {
                    buttonTextField.set(appContext.getString(R.string.show_all_projects))
                } else {
                    buttonTextField.set(
                        String.format(
                            Locale.getDefault(), appContext.getString(R.string.show_n_projects),
                            result.result?.content?.size ?: 0
                        )
                    )
                }
            }
            filteredProjectResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        sharableFilterViewModel = null
        amountRanges.clear()
        securities.clear()
        statuses.clear()
    }
}
