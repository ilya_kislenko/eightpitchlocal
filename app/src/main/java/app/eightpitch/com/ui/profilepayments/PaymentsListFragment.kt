package app.eightpitch.com.ui.profilepayments

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.ui.authorized.BottomTabsHolder
import app.eightpitch.com.ui.profilepayments.adapters.PaymentAdapter
import app.eightpitch.com.ui.profilepaymentsdetails.ProfilePaymentDetailsFragment
import app.eightpitch.com.utils.SafeLinearLayoutManager
import kotlinx.android.synthetic.main.fragment_payments_list_layout.*
import kotlinx.android.synthetic.main.no_payments_layout.*

class PaymentsListFragment : BaseFragment<PaymentsListViewModel>() {

    override fun getLayoutID() = R.layout.fragment_payments_list_layout

    override fun getVMClass() = PaymentsListViewModel::class.java

    override fun onResume() {
        super.onResume()
        viewModel.getPayments()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        setupPaymentRecycler(view.context)

        investNowButton.setDisablingClickListener {
            getTarget(BottomTabsHolder::class.java)?.switchTabTo(0)
        }

        viewModel.getPaymentsResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) {}
        })
        viewModel.getPayments()
    }

    private fun setupPaymentRecycler(context: Context) {
        val horizontalDecoration = DividerItemDecoration(
            context,
            DividerItemDecoration.VERTICAL
        ).apply {
            val horizontalDivider =
                ContextCompat.getDrawable(context, R.drawable.horizontal_divider)
            horizontalDivider?.let { setDrawable(it) }
        }
        paymentsRecycler.apply {
            layoutManager = SafeLinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = PaymentAdapter().apply {
                onPaymentClicked = { paymentItem ->
                    navigate(
                        R.id.action_profilePaymentsFragment_to_profilePaymentDetailsFragment,
                        ProfilePaymentDetailsFragment.buildWithAnArguments(paymentItem)
                    )
                }
            }
            addItemDecoration(horizontalDecoration)
        }
    }
}