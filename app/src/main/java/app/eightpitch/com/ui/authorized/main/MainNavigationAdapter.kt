package app.eightpitch.com.ui.authorized.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import app.eightpitch.com.extensions.orDefaultValue
import app.eightpitch.com.ui.authorized.main.MainScreenTab.*
import app.eightpitch.com.ui.authorized.main.homepager.DashboardRootFragment
import app.eightpitch.com.ui.authorized.main.homepager.NotificationRootFragment
import app.eightpitch.com.ui.authorized.main.homepager.ProfileRootFragment
import app.eightpitch.com.ui.authorized.main.homepager.ProjectsRootFragment

class MainNavigationAdapter(activity: FragmentActivity) : FragmentStateAdapter(activity) {

    private val fragmentsMapping = listOf<Pair<MainScreenTab, Fragment>>(
        PROJECTS to ProjectsRootFragment(),
        DASHBOARD to DashboardRootFragment(),
        NOTIFICATIONS to NotificationRootFragment(),
        PROFILE to ProfileRootFragment()
    )

    override fun getItemCount() = MainScreenTab.getMaxTabsCount()

    override fun createFragment(position: Int) = findFragmentBy(position)

    fun getCurrentFragment(position: Int) = findFragmentBy(position)

    private fun findFragmentBy(position: Int): Fragment{
        val selectedTab = MainScreenTab.getTabByPosition(position)
        val (_, fragment) = fragmentsMapping.find { it.first == selectedTab }.orDefaultValue(fragmentsMapping.first())
        return fragment
    }
}
