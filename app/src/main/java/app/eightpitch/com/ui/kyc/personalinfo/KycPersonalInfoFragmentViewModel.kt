package app.eightpitch.com.ui.kyc.personalinfo

import android.content.Context
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.data.dto.user.User.PrefixType.Companion.toDisplayValue
import app.eightpitch.com.data.dto.webid.WebIdUserRequestBody
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.data.transmit.ResultStatus
import app.eightpitch.com.extensions.getErrorEmptyString
import app.eightpitch.com.extensions.validateByDefaultPattern
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.utils.RegexPatterns.PATTERN_VALID_NAME
import app.eightpitch.com.utils.SingleActionLiveData
import app.eightpitch.com.utils.SingleActionObservableString
import app.eightpitch.com.views.interfaces.ShortFieldValidator
import kotlinx.coroutines.launch
import javax.inject.Inject

class KycPersonalInfoFragmentViewModel @Inject constructor(
    private val appContext: Context,
    private val userModel: UserModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val title = appContext.getString(R.string.kyc_personal_info_title)

    val firstNameField = ObservableField("")
    val lastNameField = ObservableField("")

    private var firstName: String = ""
    val firstNameErrorHolder = SingleActionObservableString()

    private var lastName: String = ""
    val lastNameErrorHolder = SingleActionObservableString()

    private var genderType = ""
    val genderTypeErrorHolder = SingleActionObservableString()

    val firstNameWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            firstName = sequence.toString()
        }
    }

    val lastNameWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            lastName = sequence.toString()
        }
    }

    fun validateInput(): Boolean {

        val genderTypeError = when{
            genderType.isEmpty() -> getErrorEmptyString(appContext)
            else -> ""
        }
        if (genderTypeError.isNotEmpty())
            genderTypeErrorHolder.set(genderTypeError)

        val firstNameError =
            firstName.validateByDefaultPattern(appContext, R.string.first_name, PATTERN_VALID_NAME)
        if (firstNameError.isNotEmpty())
            firstNameErrorHolder.set(firstNameError)

        val secondNameError =
            lastName.validateByDefaultPattern(appContext, R.string.last_name, PATTERN_VALID_NAME)
        if (secondNameError.isNotEmpty())
            lastNameErrorHolder.set(secondNameError)

        return genderTypeError.isEmpty() && firstNameError.isEmpty() && secondNameError.isEmpty()
    }

    internal fun getKYCInformationBody() = WebIdUserRequestBody(
        firstName = firstName,
        lastName = lastName,
        prefix = User.PrefixType.fromDisplayValue(appContext, genderType).toString()
    )

    internal fun setGender(position: Int) {
        genderType =
            appContext.resources.getStringArray(R.array.genderType).getOrElse(position) { "" }
    }

    internal fun getUserSexArrayIndex(user: User): Int {
        val prefixesArray = appContext.resources.getStringArray(R.array.genderType)
        val index = prefixesArray.indexOf(toDisplayValue(appContext, user.prefix))
        return if (index < 0) 0 else index
    }

    private val userMeResult = SingleActionLiveData<Result<User>>()
    internal fun getUserMeResult(): LiveData<Result<User>> = userMeResult

    private fun getUser() {
        viewModelScope.launch {
            val result = asyncLoading {
                userModel.getUserData()
            }

            if (result.status == ResultStatus.SUCCESS) {
                result.result?.apply {
                    firstNameField.set(firstName)
                    lastNameField.set(lastName)
                }
            }
            userMeResult.postAction(result)
        }
    }

    internal fun loadUserOrRestoreState() {
        if (firstName.isEmpty() || lastName.isEmpty())
            getUser()
        else {
            firstNameField.set(firstName)
            lastNameField.set(lastName)
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        lastName = ""
        firstName = ""
        lastNameField.set("")
        firstNameField.set("")
        genderType = appContext.resources.getStringArray(R.array.genderType).firstOrNull() ?: ""
    }
}