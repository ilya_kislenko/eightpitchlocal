package app.eightpitch.com.ui.registration.signup.privateinvestor.personalinfo

import android.os.Bundle
import android.view.View
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.signup.CommonInvestorRequestBody
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.ui.GenderSpinnerAdapter
import app.eightpitch.com.ui.registration.signup.privateinvestor.email.PVIPersonalInfoEmailFragment
import app.eightpitch.com.ui.webview.DefaultWebViewFragment
import app.eightpitch.com.utils.Constants
import kotlinx.android.synthetic.main.fragment_pvi_personal_info.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class PVIPersonalInfoFragment : BaseFragment<PVIPersonalInfoFragmentViewModel>() {

    companion object {

        internal const val PARTIAL_USER = "PARTIAL_USER"

        fun buildWithAnArguments(
            partialUser: CommonInvestorRequestBody
        ) = Bundle().apply {
            putParcelable(PARTIAL_USER, partialUser)
        }
    }

    private val partialUser: CommonInvestorRequestBody
        get() = arguments?.getParcelable(PARTIAL_USER) ?: CommonInvestorRequestBody()

    override fun getLayoutID() = R.layout.fragment_pvi_personal_info

    override fun getVMClass() = PVIPersonalInfoFragmentViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        additionalToolbarRightButton.setOnClickListener {
            val bundle = DefaultWebViewFragment.buildWithAnArguments(Constants.FAQ_URL)
            navigate(R.id.action_PVIPersonalInfoFragment_to_defaultWebViewFragment, bundle)
        }
        setupGenderSpinner()

        buttonNext.setOnClickListener {
            if (viewModel.validateInput()) {
                val (firstName, lastName, genderType) = viewModel.getPersonalInfo()
                val genderTypeBackend = User.PrefixType.fromDisplayValue(context, genderType).toString()
                val arguments = PVIPersonalInfoEmailFragment.buildWithAnArguments(partialUser.copy(
                    firstName = firstName,
                    lastName = lastName,
                    prefix = genderTypeBackend
                ))
                navigate(R.id.action_PVIPersonalInfoFragment_to_PVIPersonalInfoEmailFragment, arguments)
            }
        }
    }

    private fun setupGenderSpinner() {
        genderSpinner.apply {
            val genderSpinnerData = context.resources.getStringArray(R.array.genderType).toList()
            val genderAdapter = GenderSpinnerAdapter(context, genderSpinnerData)
            setAdapter(genderAdapter)
            setSelection(genderAdapter.getHintPosition())
            setSelectedListener({
                viewModel.setGender(it)
            }, {})
        }
    }
}