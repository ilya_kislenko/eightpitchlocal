package app.eightpitch.com.ui.dialogs

import android.os.Bundle
import androidx.fragment.app.FragmentManager
import app.eightpitch.com.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

open class DefaultBottomDialog : BottomSheetDialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.GenericBottomSheetDialogTheme)
    }

    fun show(tag: String, fragmentManager: FragmentManager) {
        val transaction = fragmentManager.beginTransaction()
        val previousDialog = fragmentManager.findFragmentByTag(tag)
        if (null != previousDialog)
            transaction.remove(previousDialog)
        transaction.addToBackStack(null)
        show(transaction, tag)
    }
}