package app.eightpitch.com.ui.profilesecurity.quicklogin

import android.content.Context
import androidx.databinding.ObservableBoolean
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.base.preferences.PreferencesReader
import app.eightpitch.com.base.preferences.PreferencesWriter
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.extensions.isQuickLoginActive
import app.eightpitch.com.extensions.updateQuickLoginPinCode

class ProfileQuickLoginViewModel(
    appContext: Context,
    private val preferencesReader: PreferencesReader,
    private val preferencesWriter: PreferencesWriter,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val toolbarTitle = appContext.getString(R.string.quick_login_text)

    val pinCodeSwitchEnabledField = ObservableBoolean(false)
    val biometricsSwitchEnabledField = ObservableBoolean(false)

    internal fun setupSwitchers() {
        pinCodeSwitchEnabledField.set(preferencesReader.isQuickLoginActive())
        biometricsSwitchEnabledField.set(preferencesReader.getUserBiometricInfo().isBiometricActive)
    }

    internal fun isQuickLoginActive() = preferencesReader.isQuickLoginActive()

    internal fun enableFingerPrint(save: Boolean) {
        preferencesWriter.updateQuickLoginPinCode(preferencesReader = preferencesReader, isBiometricSetup = save)
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        pinCodeSwitchEnabledField.set(false)
        biometricsSwitchEnabledField.set(false)
    }
}