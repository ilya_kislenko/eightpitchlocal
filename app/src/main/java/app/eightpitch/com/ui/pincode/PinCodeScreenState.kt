package app.eightpitch.com.ui.pincode

/**
 * @see PinCodeFragment navigation
 * @see CREATE_CODE - creating pinCode flow
 * @see REPEAT_CODE - a part of creating pinCode flow
 * @see ENTER_CODE - entering to app
 * @see ENTER_ACTUALLY_CODE - removing pinCode
 */
enum class PinCodeScreenState {
    CREATE_CODE,
    REPEAT_CODE,
    ENTER_CODE,
    ENTER_ACTUALLY_CODE;
}