package app.eightpitch.com.ui.questionnaire.knowledge

import android.os.Bundle
import android.view.*
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody
import app.eightpitch.com.ui.questionnaire.QuestionnaireFirstFragment.Companion.QUESTIONNAIRE_KEY_CODE
import app.eightpitch.com.ui.questionnaire.QuestionnaireThirdFragment
import app.eightpitch.com.utils.SafeLinearLayoutManager
import kotlinx.android.synthetic.main.fragment_questionnaire_second.*

class QuestionnaireSecondFragment : BaseFragment<KnowledgeViewModel>() {

    companion object {
        fun buildWithAnArguments(
            questionnaireRequestBody: QuestionnaireRequestBody
        ) = Bundle().apply {
            putParcelable(QUESTIONNAIRE_KEY_CODE, questionnaireRequestBody)
        }
    }

    private val questionnaireRequestBody: QuestionnaireRequestBody
        get() = arguments?.getParcelable(QUESTIONNAIRE_KEY_CODE) ?: QuestionnaireRequestBody()

    override fun getLayoutID() = R.layout.fragment_questionnaire_second

    override fun getVMClass() = KnowledgeViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.setToolbarIndex(3)

        nextButton.setOnClickListener {
            navigate(R.id.action_questionnaireSecondFragment_to_questionnaireThirdFragment,
                QuestionnaireThirdFragment.buildWithAnArguments(questionnaireRequestBody.copy(
                    knowledge = viewModel.getSelectedKnowledge()
                ))
            )
        }

        answersRecycler.apply {
            layoutManager = SafeLinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = KnowledgeAdapter(viewModel.getSelectedKnowledge()).apply {
                answersCallback = { name, isChecked ->
                    viewModel.setAnswers(name, isChecked)
                }
            }
        }

        backButton.setOnClickListener {
            popBackStack()
        }
    }
}