package app.eightpitch.com.ui.dynamicdetails

import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.GradientDrawable.RECTANGLE
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.R
import app.eightpitch.com.data.dto.dynamicviews.FaqItem
import app.eightpitch.com.extensions.changeVisibility
import app.eightpitch.com.extensions.inflateView
import app.eightpitch.com.extensions.setDisablingClickListener
import app.eightpitch.com.ui.dynamicdetails.FAQAdapter.DescriptionHolder

class FAQAdapter(private val items: List<FaqItem>) :
    RecyclerView.Adapter<DescriptionHolder>() {

    /**
     * State of a selection inside this list, Int as Position and Int as View visibility.
     */
    private val itemsStateHolder = mutableMapOf<Int, Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DescriptionHolder {
        return DescriptionHolder(inflateView(parent, R.layout.item_faq))
    }

    override fun onBindViewHolder(holder: DescriptionHolder, position: Int) {
        val faq = items[position]
        holder.apply {
            descriptionText.also {
                it.text = faq.answer
                it.post {
                    faqShevronView.apply {
                        tag = descriptionText to position
                        setImageDrawable(
                            ContextCompat.getDrawable(
                                context,
                                R.drawable.icon_shevron_bottom
                            )
                        )

                        val haveCacheForPosition = itemsStateHolder.contains(position)
                        updateMaxLinesWithAnimation(
                            this,
                            if (haveCacheForPosition) itemsStateHolder[position] == VISIBLE else false
                        )
                        descriptionText.changeVisibility(if (haveCacheForPosition) itemsStateHolder[position] == VISIBLE else false)

                        container.setDisablingClickListener(delay = 200) {
                            val shevron = this
                            val tag = shevron.tag as? Pair<AppCompatTextView, Int>
                            val state = descriptionText.isVisible
                            tag?.let {
                                val (textView, tagPosition) = it
                                textView.run {

                                    updateMaxLinesWithAnimation(shevron, !state)
                                    descriptionText.changeVisibility(!state)
                                    itemsStateHolder[tagPosition] =
                                        if (!state) View.GONE else VISIBLE
                                }
                            }
                            val drawable = GradientDrawable().apply {
                                shape = RECTANGLE
                            }
                            container.background = if (!state) drawable.apply {
                                setColor(ContextCompat.getColor(context, R.color.gray15))
                                cornerRadius = 10f
                            } else drawable.apply {
                                cornerRadius = 0f
                                setColor(ContextCompat.getColor(context, R.color.white))
                            }
                        }
                    }
                    header.text = faq.question
                }

            }
        }
    }

    override fun getItemCount() = items.size

    private fun updateMaxLinesWithAnimation(shevronView: View, state: Boolean) {
        (shevronView as AppCompatImageView).setImageDrawable(
            ContextCompat.getDrawable(
                shevronView.context,
                if (state) R.drawable.icon_shevron_up else R.drawable.icon_shevron_bottom
            )
        )
    }

    class DescriptionHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val header: AppCompatTextView = itemView.findViewById(R.id.headerText)
        val container: ConstraintLayout = itemView.findViewById(R.id.container)
        val faqShevronView: AppCompatImageView = itemView.findViewById(R.id.shevronIconView)
        val descriptionText: AppCompatTextView = itemView.findViewById(R.id.descriptionText)
    }
}