package app.eightpitch.com.ui.projects

import android.content.Context
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.investment.InvestorGraph
import app.eightpitch.com.data.dto.project.GetProjectPageResponse
import app.eightpitch.com.data.dto.project.InvestorsProject
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.data.dto.user.User.CREATOR.LEVEL_2
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.data.transmit.ResultStatus.SUCCESS
import app.eightpitch.com.extensions.wrapWithAnEmptyResult
import app.eightpitch.com.models.FilterModel
import app.eightpitch.com.models.ProjectModel
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.ui.projectfilter.SharableFilterViewModel
import app.eightpitch.com.ui.projects.adapters.ProjectItem
import app.eightpitch.com.ui.projects.mappers.mapToProjectItem
import app.eightpitch.com.utils.Empty
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.coroutines.launch

class ProjectsFragmentViewModel(
    appContext: Context,
    private val projectModel: ProjectModel,
    private val filterModel: FilterModel,
    private val userModel: UserModel,
    threadsSeparator: ThreadsSeparator,
) : BaseViewModel(threadsSeparator) {

    companion object {
        const val ALL_PROJECT_TAB_POSITION = 0
        const val INVESTORS_PROJECT_TAB_POSITION = 1
    }

    val emptyMessage = ObservableBoolean(false)
    val projects = ObservableArrayList<ProjectItem>()

    val tabLayoutVisibility = ObservableField(GONE)

    private var selectTabPosition = ALL_PROJECT_TAB_POSITION
    internal fun getSavedTabPosition() = selectTabPosition

    private var sharableFilterViewModel: SharableFilterViewModel? = null

    internal fun setFilters(sharableFilterViewModel: SharableFilterViewModel){
        this.sharableFilterViewModel = sharableFilterViewModel
    }

    private val investorProjectsResult = SingleActionLiveData<Result<Empty>>()
    internal fun getInvestorProjectsResult(): LiveData<Result<Empty>> =
        investorProjectsResult

    private fun getInvestorProjects() {
        viewModelScope.launch {

            val result = asyncLoading {
                projectModel.getInvestorProjects()
            }

            if (result.status == SUCCESS) {
                val resultSet = result.result!!
                updateVisibleProjects(resultSet)
                emptyMessage.set(resultSet.isEmpty())
            }

            investorProjectsResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    private val projectsResult = SingleActionLiveData<Result<Empty>>()
    internal fun getProjectsResult(): LiveData<Result<Empty>> = projectsResult

    private fun getProjects() {
        viewModelScope.launch {

            val result = asyncLoading {
                if (sharableFilterViewModel?.isEmptyFilter != true) {
                    filterModel.getFilteredProject(
                        minimumAmounts = sharableFilterViewModel?.amountRanges?: emptyList(),
                        projectStatuses = sharableFilterViewModel?.statuses?: emptyList(),
                        typesOfSecurity = sharableFilterViewModel?.securityTypes?: emptyList()
                    )
                } else {
                    projectModel.getProjectInfo()
                }
            }

            if (result.status == SUCCESS) {
                val resultSet = result.result!!
                updateVisibleProjects(resultSet)
                emptyMessage.set(resultSet.content.isEmpty())
            }

            projectsResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    private val userMeResult = SingleActionLiveData<Result<Empty>>()
    internal fun getUserMeResult(): LiveData<Result<Empty>> = userMeResult

    internal fun getUser() {
        viewModelScope.launch {

            val result = asyncLoading {
                userModel.getUserData()
            }

            if (result.status == SUCCESS) {
                loadProjectsOrGraph(result.result!!)
            }

            userMeResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    private val investorGraphResult = SingleActionLiveData<Result<Empty>>()
    internal fun getInvestorGraphResult(): LiveData<Result<Empty>> = investorGraphResult

    private fun getInvestorGraph() {
        viewModelScope.launch {

            val result = asyncLoading {
                projectModel.getInvestorGraph()
            }

            if (result.status == SUCCESS) {
                setTabLayoutVisibility(result.result)
            }

            investorGraphResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    /**
     * Method that will load whether a projects
     * or a investor graph depending on a user's level
     * [LEVEL_2] -> Projects, graph otherwise
     */
    private fun loadProjectsOrGraph(user: User) {
        if (LEVEL_2 != user.status) {
            tabLayoutVisibility.set(GONE)
        } else {
            getInvestorGraph()
        }
    }

    /**
     * Method that is changing the visibility of a tab layout
     * depending of a investor graph state, is state is empty
     * then graph is gone
     */
    private fun setTabLayoutVisibility(investorGraph: InvestorGraph?) {
        val isGraphEmpty = investorGraph?.investments.isNullOrEmpty()
        tabLayoutVisibility.set(if (isGraphEmpty) GONE else VISIBLE)
    }

    private fun updateVisibleProjects(investorProjects: List<InvestorsProject>) {
        projects.apply {
            clear()
            addAll(investorProjects.map { it.mapToProjectItem() })
        }
    }

    private fun updateVisibleProjects(projectResponse: GetProjectPageResponse) {
        projects.apply {
            clear()
            addAll(projectResponse.content.reversed().map { it.mapToProjectItem() })
        }
    }


    internal fun loadProjectsForType(tabPosition: Int) {
        selectTabPosition = tabPosition
        when (tabPosition) {
            ALL_PROJECT_TAB_POSITION -> getProjects()
            INVESTORS_PROJECT_TAB_POSITION -> getInvestorProjects()
            else -> getProjects()
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        sharableFilterViewModel = null
        projects.clear()
        emptyMessage.set(false)
        tabLayoutVisibility.set(GONE)
        selectTabPosition = ALL_PROJECT_TAB_POSITION
    }
}
