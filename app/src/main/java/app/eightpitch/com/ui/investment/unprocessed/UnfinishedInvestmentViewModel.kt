package app.eightpitch.com.ui.investment.unprocessed

import android.content.Context
import androidx.lifecycle.*
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.projectdetails.GetProjectResponse
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.models.InvestmentModel
import app.eightpitch.com.models.ProjectModel
import app.eightpitch.com.utils.*
import kotlinx.coroutines.launch

class UnfinishedInvestmentViewModel(
    appContext: Context,
    private val investmentModel: InvestmentModel,
    private val projectModel: ProjectModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    private val cancelResult = SingleActionLiveData<Result<Empty>>()
    internal fun getCancelResult(): LiveData<Result<Empty>> = cancelResult

    internal fun cancelProjectInvestment(projectId: String, investmentId: String) {
        viewModelScope.launch {
            val result = asyncLoading {
                investmentModel.cancelProjectInvestment(projectId, investmentId)
            }
            cancelResult.postAction(result)
        }
    }

    private val projectResult = SingleActionLiveData<Result<GetProjectResponse>>()
    internal fun getProjectResult(): LiveData<Result<GetProjectResponse>> = projectResult

    internal fun getProjectInfo(projectId: String) {
        viewModelScope.launch {
            val result = asyncLoading {
                projectModel.getProjectInfo(projectId)
            }
            projectResult.postAction(result)
        }
    }
}