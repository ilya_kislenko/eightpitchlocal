package app.eightpitch.com.ui.notifications

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.ui.authorized.BottomTabsHolder
import app.eightpitch.com.utils.SafeLinearLayoutManager
import kotlinx.android.synthetic.main.fragment_notifications.*

class NotificationsFragment : BaseFragment<NotificationsFragmentViewModel>() {

    override fun getLayoutID() = R.layout.fragment_notifications

    override fun getVMClass() = NotificationsFragmentViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        viewModel.getNotificationsResult().observe(viewLifecycleOwner, Observer { result -> handleResult(result) {} })
        viewModel.getMarkingNotificationResult().observe(viewLifecycleOwner, Observer { result ->
            handleResult(result) {
                getTarget(BottomTabsHolder::class.java)?.syncBottomTabsIndicators()
                retrieveNotifications()
            }
        })
        viewModel.getNotificationsStatusResult().observe(viewLifecycleOwner, Observer { result ->
            handleResult(result) { hasUnreadNotifications ->
                if (hasUnreadNotifications)
                    getTarget(BottomTabsHolder::class.java)?.syncBottomTabsIndicators()
            }
        })
    }

    private fun initNotificationsRecycler() {
        notificationsRecyclerView.apply {
            layoutManager = SafeLinearLayoutManager(context, VERTICAL, false)
            adapter = NotificationsAdapter(viewModel.getNotificationsMapper()).apply {
                onNotificationClicked = { notificationId ->
                    viewModel.markNotificationAsReadBy(notificationId)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        initNotificationsRecycler()
        retrieveNotifications()
    }

    private fun retrieveNotifications() {
        viewModel.getNotifications()
    }
}
