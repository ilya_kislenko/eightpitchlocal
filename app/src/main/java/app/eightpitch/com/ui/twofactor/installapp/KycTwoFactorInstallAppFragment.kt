package app.eightpitch.com.ui.twofactor.installapp

import android.os.Bundle
import android.view.*
import android.view.View.GONE
import android.view.View.VISIBLE
import app.eightpitch.com.R
import app.eightpitch.com.base.BackPressureFragment
import app.eightpitch.com.extensions.navigate
import app.eightpitch.com.ui.twofactor.choosetwoauthmethod.ChooseTwoAuthMethodFragment.Companion.NAVIGATE_FROM_KEY
import app.eightpitch.com.ui.twofactor.choosetwoauthmethod.ChooseTwoAuthMethodFragment.Companion.NAVIGATE_FROM_KYC
import app.eightpitch.com.ui.twofactor.choosetwoauthmethod.ChooseTwoAuthMethodFragment.Companion.NAVIGATE_FROM_SECURITY
import app.eightpitch.com.ui.twofactor.gettwoauthcode.KycTwoFactorGetCodeFragment
import app.eightpitch.com.utils.*
import kotlinx.android.synthetic.main.fragment_two_factor_install_app.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class KycTwoFactorInstallAppFragment : BackPressureFragment() {

    companion object {

        internal const val GA_ACCOUNT_URI = "GA_ACCOUNT_URI"
        internal const val TWO_AUTH_STATUS = "GA_ACCOUNT_STATUS"

        fun buildWithAnArguments(
            navigateFrom: String,
            accountUri: String,
            twoAuthStatus: String
        ) = Bundle().apply {
            putString(GA_ACCOUNT_URI, accountUri)
            putString(NAVIGATE_FROM_KEY, navigateFrom)
            putString(TWO_AUTH_STATUS, twoAuthStatus)
        }
    }

    private var isPushInstallApp = false

    private val accountUri: String
        get() = arguments?.getString(GA_ACCOUNT_URI) ?: ""
    private val twoAuthStatus: String
        get() = arguments?.getString(TWO_AUTH_STATUS) ?: ""

    private val navigateFrom: String
        get() = arguments?.getString(NAVIGATE_FROM_KEY) ?: ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View =
        inflater.inflate(R.layout.fragment_two_factor_install_app, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        registerOnBackPressedListener { popBackStack() }

        when (navigateFrom) {
            NAVIGATE_FROM_KYC -> {
                skipTextView.visibility = VISIBLE
                stepView.visibility = VISIBLE
            }
            else -> {
                skipTextView.visibility = GONE
                stepView.visibility = GONE
            }
        }

        headerTitle.text = context?.getString(R.string.add_extra_security_toolbar_title)
        additionalToolbarRightButton.visibility = GONE
        toolbarBackButton.apply {
            setImageResource(R.drawable.icon_cross)
            setOnClickListener { popBackStack() }
        }

        buttonInstallApp.setOnClickListener {
            context?.let {
                isPushInstallApp = true
                IntentsController.redirectToPlayMarket(it, Constants.GA_PACKAGE_NAME)
            }
        }

        skipTextView.setOnClickListener {
            navigate(R.id.action_kycTwoFactorInstallAppFragment_to_kycPersonalInfoFragment)
        }
    }

    private fun getNavigationId() = when (navigateFrom) {
        NAVIGATE_FROM_KYC -> {
            R.id.action_kycTwoFactorInstallAppFragment_to_kycTwoFactorGetCodeFragment
        }
        NAVIGATE_FROM_SECURITY -> {
            R.id.action_kycTwoFactorInstallAppFragment_to_kycTwoFactorGetCodeFragment
        }
        else -> {
            R.id.action_kycTwoFactorInstallAppFragment_to_kycTwoFactorGetCodeFragment
        }
    }

    override fun onResume() {
        super.onResume()
        context?.let {
            if (ExternalApplicationsManager.isGoogleAuthenticatorInstalled(it) && isPushInstallApp) {
                navigate(
                    getNavigationId(),
                    KycTwoFactorGetCodeFragment.buildWithAnArguments(
                        navigateFrom,
                        accountUri,
                        twoAuthStatus
                    )
                )
            }
        }
    }
}