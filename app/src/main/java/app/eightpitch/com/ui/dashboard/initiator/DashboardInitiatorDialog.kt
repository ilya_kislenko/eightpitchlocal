package app.eightpitch.com.ui.dashboard.initiator

import android.os.Bundle
import android.view.*
import app.eightpitch.com.R
import app.eightpitch.com.ui.dialogs.DefaultBottomDialog

class DashboardInitiatorDialog : DefaultBottomDialog() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.dialog_initiator_info_dashboard, container, false)
}