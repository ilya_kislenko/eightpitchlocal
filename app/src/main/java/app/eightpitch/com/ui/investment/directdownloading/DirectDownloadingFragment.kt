package app.eightpitch.com.ui.investment.directdownloading

import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.base.baseviewmodels.codeverification.DefaultCodeValidationFragment
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO.CREATOR.INVEST
import app.eightpitch.com.data.dto.projectdetails.ProjectsFile
import app.eightpitch.com.data.dto.signup.CommonInvestorRequestBody
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.utils.Logger
import app.eightpitch.com.utils.Logger.TAG_GENERIC_ERROR
import app.eightpitch.com.utils.SafeLinearLayoutManager
import app.eightpitch.com.utils.TextViewsRecyclerAdapter
import kotlinx.android.synthetic.main.fragment_direct_downloading_documents.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import java.util.*

class DirectDownloadingFragment : BaseFragment<DirectDownloadingViewModel>() {

    companion object {

        const val PROJECT_ID = "PROJECT_ID"
        const val INVESTMENT_ID = "INVESTMENT_ID"
        const val PROJECT_FILES = "PROJECT_FILES"

        fun buildWithAnArguments(
            projectId: String,
            investmentId: String,
            projectFiles: ArrayList<ProjectsFile>
        ) = Bundle().apply {
            putString(PROJECT_ID, projectId)
            putString(INVESTMENT_ID, investmentId)
            putParcelableArrayList(PROJECT_FILES, projectFiles)
        }
    }

    private val projectId: String
        get() = arguments?.getString(PROJECT_ID, "") ?: ""

    private val investmentId: String
        get() = arguments?.getString(INVESTMENT_ID, "") ?: ""

    private val projectFiles: List<ProjectsFile>
        get() = arguments?.getParcelableArrayList(PROJECT_FILES) ?: listOf()

    override fun getLayoutID() = R.layout.fragment_direct_downloading_documents

    override fun getVMClass() = DirectDownloadingViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
        setupDownloadLink()
        bindLoadingIndicator()

        viewModel.setupFiles(projectFiles)

        filesRecycler.apply {
            adapter = TextViewsRecyclerAdapter(true)
            layoutManager = SafeLinearLayoutManager(context, VERTICAL, false)
        }

        firstCheckBox.setOnCheckedChangeListener { _, isChecked ->
            viewModel.changeAvailability(isChecked && secondCheckBox.isChecked)
        }
        secondCheckBox.setOnCheckedChangeListener { _, isChecked ->
            viewModel.changeAvailability(isChecked && firstCheckBox.isChecked)
        }

        viewModel.getRequestResult().observe(viewLifecycleOwner, Observer {
            handleResult(it) { response ->
                val arguments =
                    DefaultCodeValidationFragment.buildWithAnArguments(
                        projectId = projectId,
                        investmentId = investmentId,
                        validationScreenInfo = createValidationScreenInfo(),
                        partialUser = CommonInvestorRequestBody(interactionId = response.interactionId)
                    )
                navigate(R.id.action_directDownloadingFragment_to_digitCodeValidationFragment, arguments)
            }
        })
        viewModel.getProjectsSubscription().observe(viewLifecycleOwner, Observer { result -> handleResult(result) {} })

        nextButton.setOnClickListener {
            viewModel.requestInvestmentConfirmation(investmentId, projectId)
        }
        viewModel.getProject(projectId)
    }

    private fun setupUI() {
        context?.let {
            downDivider.isVisible = false
            headerTitle.setTextColor(ResourcesCompat.getColor(resources, R.color.white, it.theme))
            toolbarBackButton.setImageDrawable(ContextCompat.getDrawable(it, R.drawable.icon_white_cross))
        }
    }

    private fun setupDownloadLink() {
        val clickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                viewModel.getProject(projectId)
            }
        }

        val downloadText = SpannableString(getString(R.string.if_the_files_were_not_downloaded_text))
        val indexStart = downloadText.indexOf('—')
        if (indexStart != -1)
            try {
                downloadText.setSpan(clickableSpan,
                    indexStart + 2,
                    downloadText.length,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            } catch (exception: IndexOutOfBoundsException) {
                Logger.logError(TAG_GENERIC_ERROR, "Incorrect indexes", exception)
            }
        subHeaderText.apply {
            text = downloadText
            movementMethod = LinkMovementMethod.getInstance()
        }
    }

    private fun createValidationScreenInfo() = ValidationScreenInfoDTO(
        verificationType = INVEST,
        validationButtonName = getString(R.string.confirm_text),
        header = getString(R.string.investment_confirmation_text),
        toolbarTitle = getString(R.string.investment_payment_title_text),
        subTitleMessage = getString(R.string.investment_confirmation_message_text)
    )
}