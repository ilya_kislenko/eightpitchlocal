package app.eightpitch.com.ui.projectfilter

import androidx.lifecycle.ViewModel
import app.eightpitch.com.data.dto.filter.AmountRange
import javax.inject.Inject

class SharableFilterViewModel @Inject constructor() : ViewModel() {

    private val _statuses = arrayListOf<String>()

    private val _amountRanges = arrayListOf<AmountRange>()

    private val _securityTypes = arrayListOf<String>()

    val isEmptyFilter: Boolean
        get() = statuses.isEmpty()&&amountRanges.isEmpty()&&securityTypes.isEmpty()

    val statuses: ArrayList<String>
        get() = _statuses

    val amountRanges: ArrayList<AmountRange>
        get() = _amountRanges

    val securityTypes: ArrayList<String>
        get() = _securityTypes

    fun setStatuses(statusList: ArrayList<String>){
        statuses.apply {
            clear()
            addAll(statusList)
        }
    }

    fun setSecurities(securityList: ArrayList<String>){
        securityTypes.apply {
            clear()
            addAll(securityList)
        }
    }

    fun setAmountRanges(rangesList: ArrayList<AmountRange>){
        amountRanges.apply {
            clear()
            addAll(rangesList)
        }
    }

    fun removeState(state: String){
        _statuses.remove(state)
    }

    fun removeAmountRange(range: AmountRange){
        _amountRanges.remove(range)
    }

    fun removeSecurityTypes(securityType: String){
        _securityTypes.remove(securityType)
    }

    fun clearStatuses(){
        _statuses.clear()
    }

    fun clearRanges(){
        _amountRanges.clear()
    }

    fun clearSecurities(){
        _securityTypes.clear()
    }
}
