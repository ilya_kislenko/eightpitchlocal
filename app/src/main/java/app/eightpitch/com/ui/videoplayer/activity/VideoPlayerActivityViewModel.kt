package app.eightpitch.com.ui.videoplayer.activity

import android.content.Context
import android.graphics.drawable.Drawable
import android.net.Uri
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.extensions.orDefaultValue
import app.eightpitch.com.ui.videoplayer.state.ExoPlayerState
import app.eightpitch.com.utils.SingleActionLiveData
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout.RESIZE_MODE_FIT
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout.RESIZE_MODE_ZOOM
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory

class VideoPlayerActivityViewModel(
    val appContext: Context,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val muteIconRecourseDrawable =
        ObservableField<Drawable>(appContext.getDrawable(R.drawable.icon_volume_off))
    val fullScreenIconRecourseDrawable =
        ObservableField<Drawable>(appContext.getDrawable(R.drawable.icon_full_screen))

    private var player: SimpleExoPlayer? = null
    private var exoPlayerState = ExoPlayerState()

    /**
     * Method that will invert icons on the full screen button
     * and will return a new [AspectRatioFrameLayout] resize mode
     */
    internal fun computeResizeMode(): Int {
        exoPlayerState = exoPlayerState.copy(fullScreen = exoPlayerState.fullScreen.not())
        val isFullScreen = exoPlayerState.fullScreen
        fullScreenIconRecourseDrawable.set(appContext.getDrawable(if (isFullScreen) R.drawable.icon_small_screen else R.drawable.icon_full_screen))
        return if (isFullScreen) RESIZE_MODE_ZOOM else RESIZE_MODE_FIT
    }

    /**
     * Function that will mute/un-mute an exo player
     */
    internal fun changeMuteOptionState() {
        exoPlayerState = exoPlayerState.copy(
            volume = player?.audioComponent?.volume.orDefaultValue(1f),
            muted = exoPlayerState.muted.not()
        )

        val isMuted = exoPlayerState.muted
        muteIconRecourseDrawable.set(appContext.getDrawable(if (isMuted) R.drawable.icon_volume_on else R.drawable.icon_volume_off))
        player?.audioComponent?.volume = if (isMuted) 0f else 1f
    }

    /**
     * LiveData that represents a state of exo player,
     * it will return a [SimpleExoPlayer] when it's initialized successfully
     */
    private val initPlayerResult = SingleActionLiveData<ExoPlayer>()
    internal fun getInitPlayerResult(): LiveData<ExoPlayer> = initPlayerResult

    internal fun initializePlayer(context: Context, videoLink: String) {
        player = SimpleExoPlayer.Builder(context).build()
        val mediaSource = buildMediaSource(context, Uri.parse(videoLink))
        player?.let { exoPlayer ->
            exoPlayer.playWhenReady = exoPlayerState.playWhenReady
            exoPlayer.seekTo(exoPlayerState.currentWindow, exoPlayerState.playbackPosition)
            mediaSource?.let { mediaSource ->
                exoPlayer.prepare(mediaSource, false, false);
            }
            initPlayerResult.value = exoPlayer
        }
    }

    private fun buildMediaSource(
        context: Context,
        uri: Uri
    ): MediaSource? =
        ProgressiveMediaSource.Factory(DefaultDataSourceFactory(context, "ua"))
            .createMediaSource(uri)

    /**
     * Function that will cache [SimpleExoPlayer] state
     * and then will release it
     */
    internal fun releasePlayer() {
        player?.let {
            exoPlayerState = exoPlayerState.copy(
                playWhenReady = it.playWhenReady,
                playbackPosition = it.currentPosition,
                currentWindow = it.currentWindowIndex
            )
            it.release()
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        player = null
        exoPlayerState = ExoPlayerState()
        muteIconRecourseDrawable.set(appContext.getDrawable(R.drawable.icon_volume_off))
        fullScreenIconRecourseDrawable.set(appContext.getDrawable(R.drawable.icon_full_screen))
    }
}