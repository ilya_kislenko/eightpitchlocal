package app.eightpitch.com.ui.profile

import android.content.Context
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.data.dto.user.User.CREATOR.INSTITUTIONAL_INVESTOR
import app.eightpitch.com.data.dto.user.User.CREATOR.PRIVATE_INVESTOR
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.data.transmit.ResultStatus.SUCCESS
import app.eightpitch.com.extensions.wrapWithAnEmptyResult
import app.eightpitch.com.models.AuthModel
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.utils.Constants.HELP_INITIATOR_URL
import app.eightpitch.com.utils.Constants.HELP_INVESTORS_URL
import app.eightpitch.com.utils.Empty
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.coroutines.launch
import javax.inject.Inject

class ProfileFragmentViewModel @Inject constructor(
    val appContext: Context,
    private val userModel: UserModel,
    private val authModel: AuthModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val title = appContext.getString(R.string.your_account_text)

    var helpLink = ""
        private set
    val investorName = ObservableField<String>("")
    val upgradeButtonVisibility = ObservableField<Int>(GONE)
    val paymentButtonVisibility = ObservableField<Int>(GONE)

    private val userMeResult = SingleActionLiveData<Result<Empty>>()
    internal fun getUserMeResult(): LiveData<Result<Empty>> = userMeResult

    internal fun getUser() {
        viewModelScope.launch {

            val result = asyncLoading {
                userModel.getUserData()
            }

            if (result.status == SUCCESS) {
                setProfileFields(result.result!!)
            }

            userMeResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    private fun setProfileFields(user: User) {

        investorName.set("${user.firstName} ${user.lastName}")

        helpLink = when (user.role) {
            INSTITUTIONAL_INVESTOR, PRIVATE_INVESTOR -> HELP_INVESTORS_URL
            else -> HELP_INITIATOR_URL
        }

        val isCompleteUser = user.status == User.LEVEL_2 && (user.role == PRIVATE_INVESTOR || user.role == INSTITUTIONAL_INVESTOR)
        upgradeButtonVisibility.set(if (user.canUpgradeAccount()) VISIBLE else GONE)
        paymentButtonVisibility.set(if (isCompleteUser) VISIBLE else GONE)
    }

    private val logoutResult = SingleActionLiveData<Result<Empty>>()
    internal fun getLogoutResult(): LiveData<Result<Empty>> = logoutResult

    internal fun logout() {
        viewModelScope.launch {
            val result = asyncLoading {
                authModel.logout()
            }
            logoutResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        helpLink = ""
        investorName.set("")
        upgradeButtonVisibility.set(GONE)
        paymentButtonVisibility.set(GONE)
    }
}