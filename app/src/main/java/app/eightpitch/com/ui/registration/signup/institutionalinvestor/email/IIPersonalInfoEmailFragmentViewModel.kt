package app.eightpitch.com.ui.registration.signup.institutionalinvestor.email

import android.content.Context
import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.extensions.getErrorEmptyString
import app.eightpitch.com.extensions.wrapWithAnotherResult
import app.eightpitch.com.models.SupportModel
import app.eightpitch.com.utils.SingleActionLiveData
import app.eightpitch.com.utils.SingleActionObservableString
import app.eightpitch.com.views.interfaces.ShortFieldValidator
import kotlinx.coroutines.launch
import javax.inject.Inject

class IIPersonalInfoEmailFragmentViewModel @Inject constructor(
    private val appContext: Context,
    private val supportModel: SupportModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val title = appContext.getString(R.string.creating_account)

    private var email: String = ""
    val emailErrorHolder = SingleActionObservableString()

    val emailWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            email = sequence.toString()
        }
    }

    fun validateEmail() {
        val emailError = when {
            email.isEmpty() -> getErrorEmptyString(appContext)
            !Patterns.EMAIL_ADDRESS.matcher(email).matches() -> appContext.getString(R.string.not_valid_email)
            else -> ""
        }.takeIf { it.isNotEmpty() }?.let { emailErrorHolder.set(it); it }

        if (emailError.isNullOrBlank())
            checkIsEmailValid()
    }

    private val emailValidationResult = SingleActionLiveData<Result<String>>()
    fun getEmailValidationResult(): LiveData<Result<String>> = emailValidationResult

    private fun checkIsEmailValid() {
        viewModelScope.launch {
            val trimmedEmail = email.trim()
            val result = asyncLoading {
                supportModel.checkIfEmailIsFree(trimmedEmail)
            }
            emailValidationResult.postAction(result.wrapWithAnotherResult(email))
        }
    }


    override fun cleanUpFinally() {
        super.cleanUpFinally()
        email = ""
        emailErrorHolder.set("")
    }
}