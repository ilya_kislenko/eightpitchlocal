package app.eightpitch.com.ui.profilepayments.mappers

import app.eightpitch.com.data.dto.profilepayments.ProfilePayment
import app.eightpitch.com.ui.profilepayments.adapters.PaymentItem

fun ProfilePayment.mapToPaymentsItem(): PaymentItem = PaymentItem(
    amount = amount,
    companyAddress = companyAddress,
    companyName = companyName,
    isin = isin,
    monetaryAmount = monetaryAmount,
    paymentSystem = paymentSystem,
    shortCut = shortCut,
    transactionDate = transactionDate,
    transactionId = transactionId
)
