package app.eightpitch.com.ui.settings

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.*
import app.eightpitch.com.R
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.ui.dialogs.DefaultBottomDialog

class AccountStatusInfoDialog : DefaultBottomDialog() {

    companion object {
        val ACCOUNT_STATUS_KEY = "ACCOUNT_STATUS_KEY"

        fun buildWithAnArguments(
            accountLevel: String
        ) = Bundle().apply {
            putString(ACCOUNT_STATUS_KEY, accountLevel)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.dialog_account_status_info, container, false)

        val accountLevel = arguments?.getString(ACCOUNT_STATUS_KEY) ?: ""

        val (imageRes, headerRes) = if (accountLevel == User.LEVEL_2) {
            Pair(R.drawable.icon_two_level, R.string.two_level_limitations)
        } else {
            Pair(R.drawable.icon_one_level, R.string.one_level_limitations)
        }

        view.findViewById<AppCompatTextView>(R.id.description).text = getString(
            if (accountLevel == User.LEVEL_2) R.string.user_level_2_description
            else R.string.user_level_1_description
        )

        view.findViewById<AppCompatImageView>(R.id.levelIcon).setImageResource(imageRes)
        view.findViewById<AppCompatTextView>(R.id.accountLevelTextView).text = getString(headerRes)

        return view
    }
}