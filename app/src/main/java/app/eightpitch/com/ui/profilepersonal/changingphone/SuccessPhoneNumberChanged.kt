package app.eightpitch.com.ui.profilepersonal.changingphone

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import app.eightpitch.com.R
import app.eightpitch.com.extensions.popBackStack
import kotlinx.android.synthetic.main.fragment_success_password_changed.*

class SuccessPhoneNumberChanged : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_success_phone_number_changed, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        crossButton.setOnClickListener {
            popBackStack(R.id.profilePersonalFragment)
        }

        okButton.setOnClickListener {
            popBackStack(R.id.profilePersonalFragment)
        }
    }
}