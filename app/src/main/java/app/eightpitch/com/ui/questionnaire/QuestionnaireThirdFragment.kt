package app.eightpitch.com.ui.questionnaire

import android.os.Bundle
import android.view.View
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody.CREATOR.NULL
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody.CREATOR.TRUE
import app.eightpitch.com.ui.questionnaire.QuestionnaireFirstFragment.Companion.QUESTIONNAIRE_KEY_CODE
import kotlinx.android.synthetic.main.fragment_questionnaire_two_answer.*

class QuestionnaireThirdFragment : BaseFragment<QuestionnaireViewModel>() {

    companion object {
        fun buildWithAnArguments(
            questionnaireRequestBody: QuestionnaireRequestBody
        ) = Bundle().apply {
            putParcelable(QUESTIONNAIRE_KEY_CODE, questionnaireRequestBody)
        }
    }

    private val questionnaireRequestBody: QuestionnaireRequestBody
        get() = arguments?.getParcelable(QUESTIONNAIRE_KEY_CODE) ?: QuestionnaireRequestBody()

    override fun getLayoutID() = R.layout.fragment_questionnaire_two_answer

    override fun getVMClass() = QuestionnaireViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        answersGroup.check(R.id.secondOptionTitle)

        viewModel.setupLayout(
            4,
            getString(R.string.do_you_have_any_experience_in_investing_in_one_of_these_asset_classes_text),
            arrayListOf(
                getString(R.string.yes_text),
                getString(R.string.no_text)
            )
        )

        nextButton.setOnClickListener {
            if (viewModel.getCheckedAnswer(listOf(TRUE, NULL), answersGroup) == TRUE)
                navigate(
                    R.id.action_questionnaireThirdFragment_to_questionnaireFourthFragment,
                    QuestionnaireFourthFragment.buildWithAnArguments(questionnaireRequestBody)
                )
            else
                navigate(
                    R.id.action_questionnaireThirdFragment_to_questionnaireSeventhFragment,
                    QuestionnaireFourthFragment.buildWithAnArguments(
                        questionnaireRequestBody.copy(
                            experience = null
                        )
                    )
                )
        }

        backButton.setOnClickListener {
            popBackStack()
        }
    }
}