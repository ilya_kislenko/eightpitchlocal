package app.eightpitch.com.ui.projectfilter.filtersecurity

import android.content.Context
import androidx.databinding.ObservableArrayList
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.ACTIVE
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.COMING_SOON
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.FINISHED
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.MANUAL_RELEASE
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.REFUNDED
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.TOKENS_TRANSFERRED
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.WAIT_BLOCKCHAIN
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.WAIT_RELEASE
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.data.transmit.ResultStatus
import app.eightpitch.com.extensions.wrapWithAnEmptyResult
import app.eightpitch.com.models.FilterModel
import app.eightpitch.com.ui.projectfilter.SharableFilterViewModel
import app.eightpitch.com.utils.Empty
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.coroutines.launch
import javax.inject.Inject

class FilterChooseSecurityFragmentViewModel @Inject constructor(
    private val appContext: Context,
    private val filterModel: FilterModel,
    threadsSeparator: ThreadsSeparator,
) : BaseViewModel(threadsSeparator) {

    val title = appContext.getString(R.string.filter_choose_security_title)

    val securities = ObservableArrayList<String>()
    val selectedSecurities = ObservableArrayList<String>()

    private var sharableFilterViewModel: SharableFilterViewModel? = null

    private val allTypeOfSecurities = arrayListOf<String>()

    internal fun setFilters(sharableFilterViewModel: SharableFilterViewModel){
        this.sharableFilterViewModel = sharableFilterViewModel
    }

    internal fun updateFilterData(){
        selectedSecurities.apply {
            clear()
            addAll(sharableFilterViewModel?.securityTypes?: emptyList())
        }
    }

    internal fun updateStateList() {
        securities.apply {
            clear()
            addAll(allTypeOfSecurities)
        }
    }

    internal fun addToFilter(status: String) {
        selectedSecurities.add(status)
    }

    internal fun removeFromFilter(status: String) {
        selectedSecurities.remove(status)
    }

    internal fun resetStatuses() {
        selectedSecurities.apply {
            clear()
            addAll(emptyList())
        }
    }

    internal fun applyFilter() {
        sharableFilterViewModel?.setSecurities(selectedSecurities)
    }

    private val typeOfSecurityResult = SingleActionLiveData<Result<Empty>>()
    internal fun getTypeOfSecurityResult(): LiveData<Result<Empty>> =
        typeOfSecurityResult

    internal fun getTypeOfSecurities() {
        viewModelScope.launch {

            val result = asyncLoading {
                filterModel.getTypeOfSecurity()
            }

            if (result.status == ResultStatus.SUCCESS) {
                allTypeOfSecurities.addAll(result.result ?: arrayListOf())
            } else {
                allTypeOfSecurities.addAll(listOf(
                    COMING_SOON,
                    ACTIVE,
                    MANUAL_RELEASE,
                    FINISHED,
                    WAIT_RELEASE,
                    REFUNDED,
                    TOKENS_TRANSFERRED,
                    WAIT_BLOCKCHAIN
                ))
            }
            updateFilterData()
            updateStateList()
            typeOfSecurityResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        sharableFilterViewModel = null
        selectedSecurities.clear()
        allTypeOfSecurities.clear()
        securities.clear()
    }
}
