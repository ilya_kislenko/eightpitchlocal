package app.eightpitch.com.ui.kyc

import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.data.dto.user.User.CREATOR.KYC_INTERNAL
import app.eightpitch.com.data.dto.user.User.CREATOR.KYC_WEB_ID
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.ui.kyc.KycNavigationId.*
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.coroutines.launch

class KycActivityViewModel(
    private val userModel: UserModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    private val userMeResult = SingleActionLiveData<Result<KycNavigationId>>()
    internal fun getUserMeResult(): LiveData<Result<KycNavigationId>> = userMeResult

    internal fun getUser() {
        viewModelScope.launch {

            val result = asyncLoading {
                val user = userModel.getUserData()
                getNavigationPath(user)
            }

            userMeResult.postAction(result)
        }
    }

    /**
     * Method that resolves next navigation step
     * by user's state
     */
    private fun getNavigationPath(user: User): KycNavigationId {
        val userStatus = user.status
        return when {
            userStatus == KYC_INTERNAL && user.investorQualificationForm == null -> NAVIGATE_TO_INV_QUALIFICATION
            userStatus == KYC_INTERNAL && user.investmentExperienceQuestionnaire == null -> NAVIGATE_TO_QUESTIONNAIRE
            userStatus == KYC_WEB_ID -> NAVIGATE_TO_WEB_ID
            !user.isEmailConfirmed && userStatus != KYC_INTERNAL && userStatus != KYC_WEB_ID-> NAVIGATE_TO_EMAIL
            user.isEmailConfirmed && userStatus != KYC_INTERNAL && userStatus != KYC_WEB_ID && !user.isTwoFactorAuthenticationEnabled -> NAVIGATE_TO_2FA
            user.isEmailConfirmed && userStatus != KYC_INTERNAL && userStatus != KYC_WEB_ID && user.isTwoFactorAuthenticationEnabled -> NAVIGATE_TO_KYC
            else -> CLOSE_QUESTIONNAIRE
        }
    }
}