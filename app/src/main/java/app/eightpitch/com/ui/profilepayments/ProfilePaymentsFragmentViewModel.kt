package app.eightpitch.com.ui.profilepayments

import android.content.Context
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.*
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.extensions.*
import app.eightpitch.com.models.PaymentModel
import app.eightpitch.com.utils.Empty
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.coroutines.launch
import javax.inject.Inject

class ProfilePaymentsFragmentViewModel @Inject constructor(
    appContext: Context,
    private val paymentModel: PaymentModel,
    threadsSeparator: ThreadsSeparator,
) : BaseViewModel(threadsSeparator) {

    val toolbarTitle = appContext.getString(R.string.profile_payments_toolbar_title)

    val hasPayments = ObservableBoolean()
    val hasTransfers = ObservableBoolean()
    val isShowingEmptyMessage = ObservableBoolean()

    private val paymentsResult = SingleActionLiveData<Result<Empty>>()
    internal fun getPaymentsResult(): LiveData<Result<Empty>> = paymentsResult

    internal fun getPayments() {

        viewModelScope.launch {
            val result = asyncLoading {
                val (payments, transfers) = paymentModel.run {
                    getProfilePayments() to getProfileTransfers()
                }

                val hasPaymentsContent = payments.hasContent.orDefaultValue(false)
                val hasTransfersContent = transfers.hasContent.orDefaultValue(false)
                hasPayments.set(hasPaymentsContent)
                isShowingEmptyMessage.set(!hasPaymentsContent && !hasTransfersContent)
                hasTransfers.set(hasTransfersContent)
            }

            paymentsResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        hasTransfers.set(false)
        hasPayments.set(false)
        isShowingEmptyMessage.set(false)
    }
}
