package app.eightpitch.com.ui.profilepaymentsdetails

import android.os.Bundle
import android.view.View
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.extensions.showDialog
import app.eightpitch.com.ui.profilepayments.adapters.PaymentItem
import kotlinx.android.synthetic.main.fragment_payment_details.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class ProfilePaymentDetailsFragment : BaseFragment<ProfilePaymentDetailsFragmentViewModel>() {

    companion object {

        internal const val PAYMENT_ITEM_KEY = "PAYMENT_ITEM_KEY"

        fun buildWithAnArguments(
            paymentItem: PaymentItem
        ) = Bundle().apply {
            putParcelable(PAYMENT_ITEM_KEY, paymentItem)
        }
    }

    private var paymentDetailClickListener: PaymentDetailClickListener? = null

    private val paymentItem: PaymentItem
        get() = arguments?.getParcelable(PAYMENT_ITEM_KEY) ?: PaymentItem()

    override fun getLayoutID() = R.layout.fragment_payment_details

    override fun getVMClass() = ProfilePaymentDetailsFragmentViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindLoadingIndicator()
        paymentDetailClickListener = PaymentDetailClickListener()

        setupListeners()

        additionalToolbarRightButton.apply {
            setImageResource(R.drawable.icon_download)
            setOnClickListener {
                navigate(R.id.action_profilePersonalFragment_to_editProfileFragment)
            }
        }

        viewModel.setupPaymentDetailsBinding(paymentItem)
    }

    private fun setupListeners() {
        companyNameLabel.setOnClickListener(paymentDetailClickListener)
        amountInvestedLabel.setOnClickListener(paymentDetailClickListener)
        paymentProviderLabel.setOnClickListener(paymentDetailClickListener)
        tokenAmountLabel.setOnClickListener(paymentDetailClickListener)
        isinLabel.setOnClickListener(paymentDetailClickListener)
        transactionIdLabel.setOnClickListener(paymentDetailClickListener)
        dateTimeLabel.setOnClickListener(paymentDetailClickListener)
    }

    private inner class PaymentDetailClickListener : View.OnClickListener {
        override fun onClick(view: View) {
            val (title, message, negativeListener) = when (view.id) {
                R.id.companyNameLabel -> {
                    val companyName = paymentItem.companyName
                    Triple(
                        getString(R.string.company_name),
                        companyName
                    ) {
                        viewModel.copyToClipboard(companyName)
                    }
                }
                R.id.amountInvestedLabel -> {
                    val monetaryAmount = viewModel.getMonetaryAmount(paymentItem)
                    Triple(
                        getString(R.string.amount_invested),
                        monetaryAmount
                    ) {
                        viewModel.copyToClipboard(monetaryAmount)
                    }
                }
                R.id.paymentProviderLabel -> {
                    val paymentSystem = paymentItem.paymentSystem
                    Triple(
                        getString(R.string.payment_provider),
                        paymentSystem
                    ) {
                        viewModel.copyToClipboard(paymentSystem)
                    }
                }
                R.id.tokenAmountLabel -> {
                    val amount = viewModel.getTokenAmount(paymentItem)
                    Triple(
                        getString(R.string.security_token_amount),
                        amount
                    ) {
                        viewModel.copyToClipboard(amount)
                    }
                }
                R.id.isinLabel -> {
                    val isin = paymentItem.isin
                    Triple(
                        getString(R.string.isin_text),
                        isin
                    ) {
                        viewModel.copyToClipboard(isin)
                    }
                }
                R.id.transactionIdLabel -> {
                    val transactionId = paymentItem.transactionId
                    Triple(
                        getString(R.string.transaction_id),
                        transactionId
                    ) {
                        viewModel.copyToClipboard(transactionId)
                    }
                }
                R.id.dateTimeLabel -> {
                    val dateTime = viewModel.dateTimeField.get() ?: ""
                    Triple(
                        getString(R.string.date_time),
                        dateTime
                    ) {
                        viewModel.copyToClipboard(dateTime)
                    }
                }
                else -> Triple("", "") {}
            }
            showInfoDialog(title, message, negativeListener)
        }
    }

    private fun showInfoDialog(
        title: String,
        message: String,
        negativeListener: () -> Unit
    ) {
        val dialogPositiveButtonText = getString(R.string.ok_text)
        val dialogNegativeButtonText = getString(R.string.copy_text)
        showDialog(
            title = title,
            message = message,
            positiveButtonMessage = dialogPositiveButtonText,
            cancelable = false,
            negativeButtonMessage = dialogNegativeButtonText,
            negativeListener = { negativeListener.invoke() }
        )
    }
}
