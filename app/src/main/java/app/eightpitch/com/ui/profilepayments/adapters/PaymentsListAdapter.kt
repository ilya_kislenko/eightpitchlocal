package app.eightpitch.com.ui.profilepayments.adapters

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import app.eightpitch.com.ui.profilepayments.PaymentsListFragment
import app.eightpitch.com.ui.profilepayments.TransfersListFragment

const val DEFAULT = 0
const val ONLY_PAYMENTS = 1

class PaymentsListAdapter(fragment: Fragment, mode: Int = DEFAULT) : FragmentStateAdapter(fragment) {

    private val fragments = if (mode == DEFAULT) arrayOf(
        PaymentsListFragment(),
        TransfersListFragment()
    ) else arrayOf(PaymentsListFragment())

    override fun getItemCount() = fragments.size

    override fun createFragment(position: Int) = fragments[position]
}