package app.eightpitch.com.ui.projectsearch

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.extensions.clear
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.extensions.showKeyboard
import app.eightpitch.com.ui.projects.adapters.ProjectAdapter
import app.eightpitch.com.ui.projectsdetail.ProjectDetailsFragment
import app.eightpitch.com.utils.SafeLinearLayoutManager
import kotlinx.android.synthetic.main.fragment_projects.projectsRecycler
import kotlinx.android.synthetic.main.fragment_search_projects.*
import kotlinx.android.synthetic.main.loading_progress_indicator.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import java.util.*

class SearchProjectsFragment : BaseFragment<SearchProjectsFragmentViewModel>() {

    companion object {
        private const val SEARCH_THRESHOLD_MILLIS: Long = 300L
    }

    override fun getLayoutID() = R.layout.fragment_search_projects

    override fun getVMClass() = SearchProjectsFragmentViewModel::class.java

    private var timer = Timer()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindLoadingIndicator()
        curtainView.setBackgroundColor(ContextCompat.getColor(view.context, android.R.color.transparent))

        viewModel.getProjectsResult()
            .observe(viewLifecycleOwner, { result -> handleResult(result) {} })

        projectsRecycler.apply {
            layoutManager = SafeLinearLayoutManager(context, VERTICAL, false)
            adapter = ProjectAdapter().apply {
                onProjectClicked = { projectItem ->
                    navigate(
                        R.id.action_searchProjectsFragment_to_projectDetailsFragment,
                        ProjectDetailsFragment.buildWithAnArguments(projectItem.id.toString())
                    )
                }
            }
        }

        additionalToolbarRightButton.apply {
            setImageResource(R.drawable.icon_cross)
            setOnClickListener {
                searchEditText.clear()
                curtainView.visibility = View.GONE
            }
        }

        searchEditText.apply {
            addTextChangedListener(SearchTextWatcher())
            requestFocus()
        }
        activity?.showKeyboard()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        timer.cancel()
    }

    private inner class SearchTextWatcher : TextWatcher {

        override fun beforeTextChanged(text: CharSequence?, start: Int, count: Int, after: Int) {
            //do nothing
        }

        override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {
            timer.cancel()
            viewModel.apply {
                setSearchText(text.toString())
                setClearButtonVisibility(if (text.toString().isEmpty()) View.GONE else View.VISIBLE)
            }
        }

        override fun afterTextChanged(text: Editable?) {
            timer = Timer()
            if (text.toString().isNotEmpty()) {
                curtainView.visibility = View.GONE
            }
            timer.schedule(RetrieveProjectsTimerTask(), SEARCH_THRESHOLD_MILLIS)
        }
    }

    private inner class RetrieveProjectsTimerTask : TimerTask() {
        override fun run() {
            viewModel.getProjects()
        }
    }
}
