package app.eightpitch.com.ui.twofactor.gettwoauthcode

import android.os.Bundle
import android.view.*
import app.eightpitch.com.R
import app.eightpitch.com.base.BackPressureFragment
import app.eightpitch.com.base.baseviewmodels.codeverification.DefaultCodeValidationFragment
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO.CREATOR.TWO_AUTH_VALIDATION_GA_DISABLE
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO.CREATOR.TWO_AUTH_VALIDATION_GA_ENABLE
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO.CREATOR.TWO_AUTH_VALIDATION_SMS_DISABLE
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO.CREATOR.TWO_AUTH_VALIDATION_SMS_ENABLE
import app.eightpitch.com.data.dto.signup.CommonInvestorRequestBody
import app.eightpitch.com.extensions.navigate
import app.eightpitch.com.ui.twofactor.choosetwoauthmethod.ChooseTwoAuthMethodFragment.Companion.NAVIGATE_FROM_KEY
import app.eightpitch.com.ui.twofactor.choosetwoauthmethod.ChooseTwoAuthMethodFragment.Companion.NAVIGATE_FROM_KYC
import app.eightpitch.com.ui.twofactor.choosetwoauthmethod.ChooseTwoAuthMethodFragment.Companion.NAVIGATE_FROM_SECURITY
import app.eightpitch.com.utils.ExternalApplicationsManager
import kotlinx.android.synthetic.main.fragment_two_factor_get_code.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class KycTwoFactorGetCodeFragment : BackPressureFragment() {

    companion object {

        internal const val GA_ACCOUNT_URI = "GA_ACCOUNT_URI"
        internal const val TWO_AUTH_STATUS = "GA_ACCOUNT_STATUS"
        internal const val SMS_ENABLE = "SMS_ENABLE"
        internal const val SMS_DISABLE = "SMS_DISABLE"
        internal const val GA_ENABLE = "GA_ENABLE"
        internal const val GA_DISABLE = "GA_DISABLE"

        fun buildWithAnArguments(
            navigateFrom: String,
            accountUri: String,
            twoAuthStatus: String
        ) = Bundle().apply {
            putString(GA_ACCOUNT_URI, accountUri)
            putString(NAVIGATE_FROM_KEY, navigateFrom)
            putString(TWO_AUTH_STATUS, twoAuthStatus)
        }
    }

    private val accountUri: String
        get() = arguments?.getString(GA_ACCOUNT_URI) ?: ""
    private val isEnableGA: String
        get() = arguments?.getString(TWO_AUTH_STATUS) ?: ""
    private val navigateFrom: String
        get() = arguments?.getString(NAVIGATE_FROM_KEY) ?: ""
    private var navigateToCodeValidationId = R.id.action_kycTwoFactorGetCodeFragment_to_defaultCodeValidationFragment

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_two_factor_get_code, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        when (navigateFrom) {
            NAVIGATE_FROM_KYC -> {
                stepView.visibility = View.VISIBLE
                navigateToCodeValidationId = R.id.action_kycTwoFactorGetCodeFragment_to_defaultCodeValidationFragment
            }
            NAVIGATE_FROM_SECURITY -> {
                stepView.visibility = View.GONE
                navigateToCodeValidationId = R.id.action_kycTwoFactorGetCodeFragment_to_defaultCodeValidationFragment
            }
        }

        registerOnBackPressedListener { popBackStack() }

        headerTitle.text = context?.getString(R.string.add_extra_security_toolbar_title)
        additionalToolbarRightButton.visibility = View.GONE
        toolbarBackButton.apply {
            setOnClickListener { popBackStack() }
        }

        buttonNext.setOnClickListener {
            context?.let {
                val arguments =
                    DefaultCodeValidationFragment.buildWithAnArguments(
                        twoAuthNavigateFrom = navigateFrom,
                        validationScreenInfo = createValidationScreenInfo(isEnableGA),
                        partialUser = CommonInvestorRequestBody(
                            interactionId = "",
                            phoneNumber = ""
                        )
                    )
                navigate(
                    navigateToCodeValidationId,
                    arguments
                )
                ExternalApplicationsManager.addGoogleAuthenticatorAccount(it, accountUri)
            }
        }
    }


    private fun createValidationScreenInfo(twoAuthStatus: String) = ValidationScreenInfoDTO(
        verificationType = when (twoAuthStatus) {
            SMS_ENABLE -> TWO_AUTH_VALIDATION_SMS_ENABLE
            SMS_DISABLE -> TWO_AUTH_VALIDATION_SMS_DISABLE
            GA_ENABLE -> TWO_AUTH_VALIDATION_GA_ENABLE
            GA_DISABLE -> TWO_AUTH_VALIDATION_GA_DISABLE
            else -> ""
        },
        validationButtonName = getString(R.string.confirm_text),
        header = getString(R.string.enter_code_from_ga),
        toolbarTitle = getString(R.string.two_factor_authentication),
        subTitleMessage = getString(R.string.enter_code_from_ga_description)
    )
}