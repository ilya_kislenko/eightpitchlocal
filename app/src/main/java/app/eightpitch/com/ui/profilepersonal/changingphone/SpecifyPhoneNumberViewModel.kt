package app.eightpitch.com.ui.profilepersonal.changingphone

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.sms.RequireSMSResponse
import app.eightpitch.com.data.dto.sms.SendTokenLimitResponse
import app.eightpitch.com.data.dto.user.CountryCode
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.extensions.appendPrefixToPhone
import app.eightpitch.com.extensions.getErrorEmptyString
import app.eightpitch.com.extensions.wrapWithAnEmptyResult
import app.eightpitch.com.models.SMSModel
import app.eightpitch.com.models.SupportModel
import app.eightpitch.com.utils.Empty
import app.eightpitch.com.utils.SingleActionLiveData
import app.eightpitch.com.utils.SingleActionObservableString
import app.eightpitch.com.views.interfaces.ShortFieldValidator
import kotlinx.coroutines.launch

class SpecifyPhoneNumberViewModel(
    private val appContext: Context,
    val smsModel: SMSModel,
    val supportModel: SupportModel,
    val threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val toolbarTitle = appContext.getString(R.string.change_phone_number_title)

    private var countryCode = ""
    private var userPhoneNumber = ""
    val userPhoneNumberErrorHolder = SingleActionObservableString()

    internal fun setCountryCode(position: Int) {
        countryCode = CountryCode.displayValueToPhoneCode(CountryCode.getPhoneCodes()[position])
    }

    internal fun getUserPhoneNumberWithCode() = userPhoneNumber.appendPrefixToPhone(countryCode)

    val userPhoneNumberWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            userPhoneNumber = sequence.toString()
        }
    }

    internal fun isPhoneNumberValid() {
        if (userPhoneNumber.isEmpty()) {
            userPhoneNumberErrorHolder.set(getErrorEmptyString(appContext))
        } else {
            checkIfPhoneIsFree(getUserPhoneNumberWithCode())
        }
    }

    private val smsResult = SingleActionLiveData<Result<RequireSMSResponse>>()
    fun getSMSResult(): LiveData<Result<RequireSMSResponse>> = smsResult

    internal fun requestSMSCode() {
        viewModelScope.launch {
            val result = asyncLoading {
                smsModel.requireSMSCodeByPhoneNumber(getUserPhoneNumberWithCode())
            }
            smsResult.postAction(result)
        }
    }

    private val phoneValidationResult = SingleActionLiveData<Result<Empty>>()
    val getPhoneValidationResult: LiveData<Result<Empty>> = phoneValidationResult

    private fun checkIfPhoneIsFree(phoneNumber: String) {
        viewModelScope.launch {
            val result = asyncLoading {
                supportModel.checkIfPhoneIsFree(phoneNumber)
            }
            phoneValidationResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        userPhoneNumber = ""
        userPhoneNumberErrorHolder.set("")
        countryCode = CountryCode.displayValueToPhoneCode(CountryCode.getPhoneCodes().first())
    }
}