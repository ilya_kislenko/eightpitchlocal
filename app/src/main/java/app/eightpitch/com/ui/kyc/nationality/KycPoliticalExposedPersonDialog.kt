package app.eightpitch.com.ui.kyc.nationality

import android.os.Bundle
import android.view.*
import app.eightpitch.com.R
import app.eightpitch.com.ui.dialogs.DefaultBottomDialog

class KycPoliticalExposedPersonDialog : DefaultBottomDialog() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.dialog_kyc_political_exposed_person, container, false)
}