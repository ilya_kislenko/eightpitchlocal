package app.eightpitch.com.ui.kyc.editsummary

import android.os.Bundle
import android.view.View
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.user.Country.Companion.toDisplayValue
import app.eightpitch.com.data.dto.webid.WebIdUserRequestBody
import app.eightpitch.com.extensions.getSortedCountries
import app.eightpitch.com.extensions.injectViewModel
import app.eightpitch.com.extensions.showDatePickerDialog
import app.eightpitch.com.ui.kyc.country.CountrySpinnerAdapter
import app.eightpitch.com.ui.kyc.nationality.NationalitySpinnerAdapter
import kotlinx.android.synthetic.main.fragment_kyc_edit_summary_info.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class KycEditSummaryInfoFragment : BaseFragment<KycEditSummaryInfoFragmentViewModel>() {

    companion object {

        internal const val KYC_BODY = "KYC_BODY"

        fun buildWithAnArguments(
            webIdUserRequestBody: WebIdUserRequestBody
        ) = Bundle().apply {
            putParcelable(KYC_BODY, webIdUserRequestBody)
        }
    }

    private val webIdUserRequestBody: WebIdUserRequestBody
        get() = arguments?.getParcelable(KYC_BODY) ?: WebIdUserRequestBody()

    override fun getLayoutID() = R.layout.fragment_kyc_edit_summary_info

    override fun getVMClass() = KycEditSummaryInfoFragmentViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        setupUI()

        textInputLayoutBirthDate.setOnFieldClickListener {
            showDatePickerDialog(
                selectedDateInMillis = if (viewModel.birthDateMillis == 0L)
                    System.currentTimeMillis() else viewModel.birthDateMillis
            ) { localDate ->
                viewModel.setBirthDate(localDate)
            }
        }

        buttonApplyChanges.setOnClickListener {
            if (viewModel.validateInput()) {
                val sharedModel = injectViewModel(SharableViewModel::class.java)
                sharedModel.webIdUserRequestBody =
                    viewModel.addFieldDataToKycBody(webIdUserRequestBody,
                        politicallyToggle.isPositiveSelection(),
                        taxLiabilityToggle.isPositiveSelection())
                popBackStack()
            }
        }
    }

    private fun setupUI() {
        additionalToolbarRightButton.setImageResource(R.drawable.icon_edit)
        setupNationalitySpinner()
        setupCountrySpinner()
        setSpinnersSelection(webIdUserRequestBody)
        viewModel.setupBinding(webIdUserRequestBody)
    }

    private fun setSpinnersSelection(webIdUserRequestBody: WebIdUserRequestBody) {
        context?.let { context ->

            val countrySpinnerData = context.getSortedCountries()
            val (country, nationality) = webIdUserRequestBody.run {
                toDisplayValue(context, address.country) to toDisplayValue(context, webIdPersonalInfo.nationality)
            }

            val countryIndex = countrySpinnerData.indexOf(country)
            val nationalityIndex = countrySpinnerData.indexOf(nationality)

            countrySpinner.setSelection(countryIndex)
            nationalitySpinner.setSelection(nationalityIndex)
        }
    }

    private fun setupNationalitySpinner() {
        context?.let {ctx->
            nationalitySpinner.apply {
                val nationalityAdapter = NationalitySpinnerAdapter(ctx)
                setAdapter(nationalityAdapter)
                setSelection(nationalityAdapter.getHintPosition())
                setSelectedListener({
                    viewModel.setNationality(it)
                }, {})
            }
        }
    }

    private fun setupCountrySpinner() {
        countrySpinner.apply {
            val countryAdapter = CountrySpinnerAdapter(context)
            setAdapter(countryAdapter)
            setSelection(countryAdapter.getHintPosition())
            setSelectedListener({
                viewModel.setCountry(it)
            }, {})
        }
    }
}