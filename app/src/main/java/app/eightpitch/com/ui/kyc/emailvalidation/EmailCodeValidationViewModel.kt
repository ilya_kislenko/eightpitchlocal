package app.eightpitch.com.ui.kyc.emailvalidation

import android.content.Context
import android.text.Spanned
import androidx.core.text.toSpanned
import androidx.databinding.*
import androidx.lifecycle.*
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.sms.EmailPhoneValidationResponse
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.data.transmit.ResultStatus.SUCCESS
import app.eightpitch.com.extensions.toHtml
import app.eightpitch.com.extensions.wrapWithAnEmptyResult
import app.eightpitch.com.models.SMSModel
import app.eightpitch.com.models.SupportModel
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.utils.*
import kotlinx.coroutines.launch
import kotlin.properties.Delegates

/**
 * This ViewModel is designed to validate a code which was send to a user email
 */
class EmailCodeValidationViewModel(
    private val appContext: Context,
    private val smsModel: SMSModel,
    private val userModel: UserModel,
    private val supportModel: SupportModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    companion object {
        internal const val MAX_CODE_LENGTH = 6
    }

    val buttonAvailability = ObservableBoolean()

    val userSMSMessage = ObservableField("")

    /**
     * Variables for binding all view on screens with code verification (not related to input)
     */
    val header = ObservableField("")
    val toolbarTitle = ObservableField("")
    val subTitleMessage = ObservableField<Spanned>()
    val validateButtonName = ObservableField("")

    val firstObservableField = ObservableField("")
    val secondObservableField = ObservableField("")
    val thirdObservableField = ObservableField("")
    val fourthObservableField = ObservableField("")
    val fifthObservableField = ObservableField("")
    val sixthObservableField = ObservableField("")

    var smsInput by Delegates.observable("") { _, _, newValue ->
        inputChanged(newValue)
    }

    private val observableFields = arrayListOf(
        firstObservableField, secondObservableField,
        thirdObservableField, fourthObservableField,
        fifthObservableField, sixthObservableField
    )

    private fun inputChanged(input: String) {

        if (input.isEmpty()) {
            observableFields.forEach { it.set("") }
        } else {

            for (i in 0 until MAX_CODE_LENGTH) {

                val observableField = observableFields[i]

                if (input.length <= i) {
                    observableField.set("")
                } else
                    observableField.set(input[i].toString())
            }
        }
        buttonAvailability.set(input.length == MAX_CODE_LENGTH)
    }

    internal fun setupBinding() {
        toolbarTitle.set(appContext.getString(R.string.email_code_toolbar_title))
        validateButtonName.set(appContext.getString(R.string.email_code_send_button_text))
    }

    private val resendingCodeResult = SingleActionLiveData<Result<Empty>>()
    fun getResendingCodeResult(): LiveData<Result<Empty>> = resendingCodeResult

    internal fun sendSMSCodeAndRetrieveUserEmail (){

        viewModelScope.launch {
            val result = asyncLoading {
                smsModel.requireCodeByEmail()
                userModel.getUserData()
            }

            if(result.status == SUCCESS){
                result.result?.let {
                    subTitleMessage.set(
                        appContext.getString(R.string.verification_code_subtitle_text, it.email).toHtml()
                    )
                }
            }

            resendingCodeResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    internal fun resendSMSCodeByEmail() {
        viewModelScope.launch {
            val result = asyncLoading {
                smsModel.resendCodeByEmail()
            }
            resendingCodeResult.postAction(result)
        }
    }

    internal fun updateSMSMessage(waitingTimeInSeconds: Int) {
        userSMSMessage.set(appContext.getString(R.string.asking_for_code_message_pattern, waitingTimeInSeconds))
    }

    private val verifyEmailCodeResult = SingleActionLiveData<Result<EmailPhoneValidationResponse>>()
    fun getVerifyWithEmailCodeResult(): LiveData<Result<EmailPhoneValidationResponse>> = verifyEmailCodeResult

    internal fun confirmEmailVerificationCode() {
        viewModelScope.launch {
            val result = asyncLoading {
                supportModel.verifyEmailCode(smsInput)
            }
            verifyEmailCodeResult.postAction(result)
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        smsInput = ""
        header.set("")
        toolbarTitle.set("")
        userSMSMessage.set("")
        subTitleMessage.set("".toSpanned())
        validateButtonName.set("")
        buttonAvailability.set(false)
        observableFields.forEach { it.set("") }
    }
}