package app.eightpitch.com.ui

import android.animation.ObjectAnimator
import android.animation.ObjectAnimator.ofPropertyValuesHolder
import android.animation.PropertyValuesHolder
import android.animation.ValueAnimator.REVERSE
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import app.eightpitch.com.R
import app.eightpitch.com.base.preferences.PreferencesReader
import app.eightpitch.com.dagger.Injector
import app.eightpitch.com.extensions.showDialog
import app.eightpitch.com.ui.authorized.AuthorizedActivity
import app.eightpitch.com.ui.profilesecurity.quicklogin.QuickLoginSettingActivity
import app.eightpitch.com.ui.registration.RegistrationActivity
import app.eightpitch.com.utils.IntentsController
import app.eightpitch.com.utils.RootDetection
import kotlinx.android.synthetic.main.activity_splash.*
import kotlinx.coroutines.*
import javax.inject.Inject

class SplashActivity : AppCompatActivity() {

    @Inject
    lateinit var reader: PreferencesReader
    lateinit var rootDetection: RootDetection

    override fun onCreate(savedInstanceState: Bundle?) {
        resolveDependencies()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        setupUI()
    }

    private fun resolveDependencies() {
        Injector.getAppComponent().injectIntoActivity(this)
        rootDetection = Injector.buildModelsComponent().provideRootDetection()
    }

    private fun setupUI() {
        setupCounter()
        startAnimation()
    }

    private fun setupCounter() {
        CoroutineScope(Dispatchers.Main).launch {
            val array = resources.obtainTypedArray(R.array.splash_counter_drawable_array)
            for (index in 0..7) {
                counterImageView.setImageResource(array.getResourceId(index, 0))
                delay(200)
            }
            array.recycle()
            navigateForward()
        }
    }

    private fun startAnimation() {

        val bigCircleAnimator = setupAnimator(bigCircleImageView, 2f)
        val middleCircleAnimator = setupAnimator(middleCircleImageView, 1.5f)

        CoroutineScope(Dispatchers.Main).launch {
            for (i in 0..3) {
                middleCircleAnimator.start()
                bigCircleAnimator.start()
                delay(400)
            }
        }
    }

    private fun setupAnimator(
        view: View,
        toAxisValue: Float
    ): ObjectAnimator = ofPropertyValuesHolder(
        view,
        PropertyValuesHolder.ofFloat(View.SCALE_X, toAxisValue),
        PropertyValuesHolder.ofFloat(View.SCALE_Y, toAxisValue)
    ).apply {
        repeatCount = 1
        repeatMode = REVERSE
    }

    private fun navigateForward() {
        if (rootDetection.isItRootedDevice()) {
            showDialog(message = getString(R.string.root_device_detected_error_message),
                positiveButtonMessage = getString(R.string.ok_text),
                positiveListener = {
                    finish()
                }, cancelable = false)
        } else {
            IntentsController.startActivityWithANewStack(
                applicationContext,
                when {
                    reader.getUserBiometricInfo().hasPinCode -> QuickLoginSettingActivity::class.java
                    reader.isLoggedIn() -> AuthorizedActivity::class.java
                    else -> RegistrationActivity::class.java
                }
            )
        }
    }
}
