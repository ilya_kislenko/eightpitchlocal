package app.eightpitch.com.ui.dashboard.initiator

import android.annotation.SuppressLint
import android.content.Intent
import android.content.Intent.ACTION_VIEW
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.core.content.ContextCompat
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.extensions.setDisablingClickListener
import app.eightpitch.com.utils.Constants.FRONT_APPLICATION_URL
import kotlinx.android.synthetic.main.fragment_initiator_welcome.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class InitiatorWelcomeFragment : BaseFragment<InitiatorWelcomeViewModel>() {

    override fun getLayoutID() = R.layout.fragment_initiator_welcome

    override fun getVMClass() = InitiatorWelcomeViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindLoadingIndicator()
        setupUI()

        viewModel.getInitiatorResult().observe(viewLifecycleOwner, Observer {
            handleResult(it) { hasProjects ->
                if (hasProjects) {
                    navigate(R.id.action_initiatorWelcomeFragment_to_initiatorStatisticFragment)
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.retrieveData()
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun setupUI() {
        context?.let {
            headerTitle.setTextColor(ContextCompat.getColor(it, R.color.white))
            createProject.setDisablingClickListener {
                startActivity(Intent(ACTION_VIEW).apply {
                    data = Uri.parse(FRONT_APPLICATION_URL)
                })
            }
            additionalToolbarRightButton.apply {
                setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_info_white))
                setDisablingClickListener {
                    DashboardInitiatorDialog().show(
                        DashboardInitiatorDialog::class.java.name,
                        childFragmentManager
                    )
                }
            }
        }
    }
}
