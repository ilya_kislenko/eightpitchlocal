package app.eightpitch.com.ui.profilepayments.adapters

import android.view.*
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.R
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.recycleradapters.BindableRecyclerAdapter
import java.util.*

class PaymentAdapter :
    BindableRecyclerAdapter<PaymentItem, PaymentAdapter.PagedPaymentViewHolder>() {

    /**
     * Listener that represents a click on any payment in the list,
     * will pass a [PaymentItem] when click is performed
     */
    internal lateinit var onPaymentClicked: (PaymentItem) -> (Unit)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PagedPaymentViewHolder(
        inflateView(
            parent,
            R.layout.item_view_payment
        )
    )

    override fun onBindViewHolder(holder: PagedPaymentViewHolder, position: Int) {
        val paymentItem = getDataItem(position)
        holder.apply {
            paymentsDateTextView.text = paymentItem.formattedDate
            paymentAmountInTokensTextView.text = String.format(
                Locale.getDefault(),
                paymentAmountInTokensTextView.context.getString(R.string.token_amount_pattern),
                paymentItem.amount.formatToCurrencyView(),
                paymentItem.shortCut
            )
            parentView.apply {
                tag = position
                setOnClickListener {
                    val tagPos = it.tag as Int
                    onPaymentClicked.invoke(getDataItem(tagPos))
                }
            }
        }
    }

    class PagedPaymentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val paymentsDateTextView: AppCompatTextView =
            itemView.findViewById(R.id.paymentsDate)
        val paymentAmountInTokensTextView: AppCompatTextView =
            itemView.findViewById(R.id.paymentAmountInTokensTextView)
        val parentView: ConstraintLayout = itemView.findViewById(R.id.parentView)
    }
}
