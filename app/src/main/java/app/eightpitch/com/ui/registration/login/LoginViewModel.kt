package app.eightpitch.com.ui.registration.login

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.auth.LoginResponseBody
import app.eightpitch.com.data.dto.user.CountryCode.Companion.displayValueToPhoneCode
import app.eightpitch.com.data.dto.user.CountryCode.Companion.getPhoneCodes
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.extensions.appendPrefixToPhone
import app.eightpitch.com.models.AuthModel
import app.eightpitch.com.utils.SingleActionLiveData
import app.eightpitch.com.utils.SingleActionObservableString
import app.eightpitch.com.views.interfaces.ShortFieldValidator
import kotlinx.coroutines.launch

class LoginViewModel(
    private val appContext: Context,
    private val authModel: AuthModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val toolbarTitle = appContext.getString(R.string.log_in_title_text)

    var countryCode = ""
    var userPhoneNumber = ""
    val userPhoneNumberErrorHolder = SingleActionObservableString()

    val userPhoneNumberWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            userPhoneNumber = sequence.toString()
        }
    }

    internal fun setCountryCode(position: Int) {
        countryCode = displayValueToPhoneCode(getPhoneCodes()[position])
    }

    private var userPassword = ""
    val userPasswordErrorHolder = SingleActionObservableString()

    val userPasswordWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            userPassword = sequence.toString()
        }
    }

    internal fun validateInput() {
        if (userPhoneNumber.isEmpty())
            userPhoneNumberErrorHolder.set(appContext.getString(R.string.vld2_error_text))
        if (userPassword.isEmpty())
            userPasswordErrorHolder.set(appContext.getString(R.string.vld2_error_text))

        if (userPhoneNumber.isEmpty() || userPassword.isEmpty())
            return

        performLogin()
    }

    private val loginResult = SingleActionLiveData<Result<LoginResponseBody>>()
    fun getLoginResult(): LiveData<Result<LoginResponseBody>> = loginResult

    private fun performLogin() {
        viewModelScope.launch {
            val result = asyncLoading {
                authModel.login(userPassword, userPhoneNumber.appendPrefixToPhone(countryCode))
            }
            loginResult.postAction(result)
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        userPassword = ""
        userPhoneNumber = ""
        userPasswordErrorHolder.set("")
        userPhoneNumberErrorHolder.set("")
        countryCode = displayValueToPhoneCode(getPhoneCodes().first())
    }
}