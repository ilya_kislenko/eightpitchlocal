package app.eightpitch.com.ui.profilepayments

import android.content.Context
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.*
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.profilepayments.ProfilePayment
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.data.transmit.ResultStatus
import app.eightpitch.com.extensions.orDefaultValue
import app.eightpitch.com.extensions.wrapWithAnEmptyResult
import app.eightpitch.com.models.PaymentModel
import app.eightpitch.com.ui.profilepayments.adapters.PaymentItem
import app.eightpitch.com.ui.profilepayments.mappers.mapToPaymentsItem
import app.eightpitch.com.utils.Empty
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.coroutines.launch

class PaymentsListViewModel(
    appContext: Context,
    private val paymentModel: PaymentModel,
    threadsSeparator: ThreadsSeparator,
) : BaseViewModel(threadsSeparator) {

    //region binding fields
    val payments = ObservableArrayList<PaymentItem>()
    val isShowingEmptyMessage = ObservableBoolean()
    //endregion fields

    private val paymentsResult = SingleActionLiveData<Result<Empty>>()
    internal fun getPaymentsResult(): LiveData<Result<Empty>> = paymentsResult

    internal fun getPayments() {

        viewModelScope.launch {

            val result = asyncLoading {
                paymentModel.getProfilePayments()
            }

            if (result.status == ResultStatus.SUCCESS) {
                val response = result.result!!
                isShowingEmptyMessage.set(!response.hasContent.orDefaultValue(true))
                updateVisiblePayments(response.content.reversed() ?: arrayListOf())
            }

            paymentsResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    private fun updateVisiblePayments(investorPayments: List<ProfilePayment>) {
        payments.apply {
            clear()
            addAll(investorPayments.map { it.mapToPaymentsItem() })
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        payments.clear()
    }
}