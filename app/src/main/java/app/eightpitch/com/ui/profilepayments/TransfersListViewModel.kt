package app.eightpitch.com.ui.profilepayments

import android.content.Context
import androidx.databinding.ObservableArrayList
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.profiletransfers.TransferDetails
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.data.transmit.ResultStatus
import app.eightpitch.com.extensions.wrapWithAnEmptyResult
import app.eightpitch.com.models.PaymentModel
import app.eightpitch.com.utils.*
import kotlinx.coroutines.launch

class TransfersListViewModel(
    appContext: Context,
    private val paymentModel: PaymentModel,
    threadsSeparator: ThreadsSeparator,
) : BaseViewModel(threadsSeparator) {

    val transfers = ObservableArrayList<TransferDetails>()

    private val transfersResult = SingleActionLiveData<Result<Empty>>()
    internal fun getTransfersResult(): LiveData<Result<Empty>> = transfersResult

    internal fun getTransfers() {

        viewModelScope.launch {

            val result = asyncLoading {
                paymentModel.getProfileTransfers()
            }

            if (result.status == ResultStatus.SUCCESS) {
                updateVisiblePayments(result.result?.content?.reversed() ?: arrayListOf())
            }

            transfersResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    private fun updateVisiblePayments(investorTransfers: List<TransferDetails>) {
        transfers.apply {
            clear()
            addAll(investorTransfers)
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        transfers.clear()
    }
}