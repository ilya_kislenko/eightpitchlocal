package app.eightpitch.com.ui.profilepayments

import android.content.Context
import android.os.Bundle
import android.view.View
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.ui.authorized.BottomTabsHolder
import app.eightpitch.com.ui.profilepayments.adapters.ONLY_PAYMENTS
import app.eightpitch.com.ui.profilepayments.adapters.PaymentsListAdapter
import app.eightpitch.com.utils.TabLayoutObservableMediator
import kotlinx.android.synthetic.main.fragment_profile_payments.*
import kotlinx.android.synthetic.main.no_payments_layout.*

class ProfilePaymentsFragment : BaseFragment<ProfilePaymentsFragmentViewModel>() {

    override fun getLayoutID() = R.layout.fragment_profile_payments

    override fun getVMClass() = ProfilePaymentsFragmentViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        investNowButton.setDisablingClickListener {
            getTarget(BottomTabsHolder::class.java)?.switchTabTo(0)
        }

        viewModel.getPaymentsResult().observe(viewLifecycleOwner, {
            handleResult(it) {
                setupUI(view.context)
            }
        })

        viewModel.getPayments()
    }

    private fun setupUI(context: Context) = viewModel.run {
        when {
            hasTransfers.get() -> bindTabLayout(context)
            else -> showOnlyPayments()
        }
    }

    private fun bindTabLayout(context: Context) {
        val tabTitles = arrayOf(
            context.getString(R.string.payments_list_text),
            context.getString(R.string.transfers_list_text),
        )

        TabLayoutObservableMediator(
            unsubscribeType = ProfilePaymentsFragment::class.java,
            parentFragmentManager,
            projectChangeModeTabLayout,
            viewPager) { tab, position ->
            tab.text = tabTitles[position]
        }.also {
            viewPager.apply {
                adapter = PaymentsListAdapter(this@ProfilePaymentsFragment)
                isUserInputEnabled = false
            }
        }.attach()
    }

    private fun showOnlyPayments() {
        viewPager.adapter = PaymentsListAdapter(this@ProfilePaymentsFragment, ONLY_PAYMENTS)
    }
}
