package app.eightpitch.com.ui.kyc

/**
 * An enum that describes all possible ways to
 * continue KYC flow if user has completed some
 * parts of the flow
 */
enum class KycNavigationId {
    NAVIGATE_TO_EMAIL,
    NAVIGATE_TO_2FA,
    NAVIGATE_TO_KYC,
    NAVIGATE_TO_WEB_ID,
    NAVIGATE_TO_INV_QUALIFICATION,
    NAVIGATE_TO_QUESTIONNAIRE,
    CLOSE_QUESTIONNAIRE
}
