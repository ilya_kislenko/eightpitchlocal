package app.eightpitch.com.ui.dashboard.investor

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.data.dto.user.User.CREATOR.LEVEL_2
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.extensions.setDisablingClickListener
import app.eightpitch.com.ui.kyc.KycActivity
import app.eightpitch.com.ui.authorized.BottomTabsHolder
import kotlinx.android.synthetic.main.fragment_investor_welcome.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class InvestorWelcomeFragment : BaseFragment<InvestorWelcomeViewModel>() {

    override fun getLayoutID() = R.layout.fragment_investor_welcome

    override fun getVMClass() = InvestorWelcomeViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        setupUI(view.context)

        viewModel.getDataResult().observe(viewLifecycleOwner, Observer {
            handleResult(it) { (hasInvestments, user) ->
                if (hasInvestments) {
                    navigate(R.id.action_investorWelcomeFragment_to_investmentStatisticFragment)
                } else
                    setupUserFlow(user)
            }
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.retrieveData()
    }

    private fun setupUI(context: Context) {
        headerTitle.setTextColor(ContextCompat.getColor(context, R.color.white))
        additionalToolbarRightButton.apply {
            setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_info_white))
            setDisablingClickListener {
                DashBoardInvestorDialog().show(
                    DashBoardInvestorDialog::class.java.name,
                    childFragmentManager
                )
            }
        }
    }

    private fun setupUserFlow(user: User) {
        visibilityButton(user.isWaitingForApproval())
        opportunitiesButton.apply {
            val isUserLvl2 = user.status == LEVEL_2
            text = getString(
                if (isUserLvl2) R.string.investment_opportunities_text
                else R.string.upgrade_your_profile
            )
            setDisablingClickListener {
                if (isUserLvl2) {
                    getTarget(BottomTabsHolder::class.java)?.switchTabTo(0)
                } else {
                    startActivity(Intent(context, KycActivity::class.java))
                }
            }
        }
    }

    private fun visibilityButton(visible: Boolean) {
        opportunitiesButton.isVisible = !visible
    }
}