package app.eightpitch.com.ui.projectfilter.filtersecurity

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.extensions.injectViewModel
import app.eightpitch.com.ui.projectfilter.SharableFilterViewModel
import app.eightpitch.com.ui.projectfilter.filtersecurity.adapter.FilterButtonSecurityAdapter
import app.eightpitch.com.utils.SafeLinearLayoutManager
import kotlinx.android.synthetic.main.fragment_filter_choose_security.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class FilterChooseSecurityFragment : BaseFragment<FilterChooseSecurityFragmentViewModel>() {

    override fun getLayoutID() = R.layout.fragment_filter_choose_security

    override fun getVMClass() = FilterChooseSecurityFragmentViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindLoadingIndicator()

        additionalToolbarRightButton.apply {
            setImageResource(R.drawable.icon_refresh)
            setOnClickListener {
                viewModel.resetStatuses()
            }
        }

        securityRecycler.apply {
            layoutManager = SafeLinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = FilterButtonSecurityAdapter().apply {
                onItemClicked = { item, isActive, _ ->
                    if (isActive)
                        viewModel.addToFilter(item)
                    else viewModel.removeFromFilter(item)
                }
            }
        }

        applyFilterButton.setOnClickListener {
            viewModel.applyFilter()
            popBackStack(R.id.filterProjectFragment)
        }

        viewModel.getTypeOfSecurityResult().observe(viewLifecycleOwner, Observer {
            handleResult(it){}
        })

        val sharedViewModel = injectViewModel(SharableFilterViewModel::class.java)
        viewModel.setFilters(sharedViewModel)
        viewModel.getTypeOfSecurities()
    }
}
