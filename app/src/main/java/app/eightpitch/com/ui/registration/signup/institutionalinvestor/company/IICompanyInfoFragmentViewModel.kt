package app.eightpitch.com.ui.registration.signup.institutionalinvestor.company

import android.content.Context
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.extensions.getErrorEmptyString
import app.eightpitch.com.utils.SingleActionObservableString
import app.eightpitch.com.views.interfaces.ShortFieldValidator
import javax.inject.Inject

class IICompanyInfoFragmentViewModel @Inject constructor(
    private val appContext: Context,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val title = appContext.getString(R.string.creating_account)

    var companyErrorHolder = ObservableField<String>()
    var companyTypeErrorHolder = ObservableField<String>()
    var companyErrorVisibility = ObservableBoolean(false)

    var companyName = ""
    var companyTypePosition = appContext.resources.getStringArray(R.array.companyType).size + 1

    private var companyType = appContext.resources.getStringArray(R.array.companyType).first()

    private var registerNumber: String = ""

    val registerNumberErrorHolder = SingleActionObservableString()

    val registerNumberWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            registerNumber = sequence.toString()
        }
    }

    internal fun getCompanyData() = Pair(companyType to companyName, registerNumber)

    fun validateCompanyName(): Boolean {

        val companyError = when {
            companyName.isEmpty() -> getErrorEmptyString(appContext)
            else -> ""
        }

        val companyTypeError = when {
            companyTypePosition == appContext.resources.getStringArray(R.array.companyType).size + 1 -> getErrorEmptyString(appContext)
            else -> ""
        }

        val registerNumberError = when {
            registerNumber.isEmpty() -> getErrorEmptyString(appContext)
            else ->""
        }

        if (companyError.isNotEmpty())
            companyErrorHolder.set(companyError)

        if (companyTypeError.isNotEmpty())
            companyTypeErrorHolder.set(companyTypeError)

        if (registerNumberError.isNotEmpty())
            registerNumberErrorHolder.set(registerNumberError)

        companyErrorVisibility.set(companyError.isNotEmpty())

        return companyError.isEmpty() && registerNumberError.isEmpty() && companyTypeError.isEmpty()
    }

    fun setCompanyType(position: Int) {
        companyTypePosition = position
        companyType = appContext.resources.getStringArray(R.array.companyType).getOrElse(position) { " " }
    }

    fun setCompanyNameValue(text: String) {
        companyName = text
        companyErrorVisibility.set(companyName.isEmpty())
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        companyName = ""
        companyErrorHolder.set("")
        companyErrorVisibility.set(false)
        companyType = ""
    }
}