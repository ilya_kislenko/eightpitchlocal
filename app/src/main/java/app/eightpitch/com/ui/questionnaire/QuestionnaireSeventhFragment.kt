package app.eightpitch.com.ui.questionnaire

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody.CREATOR.NULL
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody.CREATOR.TRUE
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.ui.questionnaire.QuestionnaireFirstFragment.Companion.QUESTIONNAIRE_KEY_CODE
import kotlinx.android.synthetic.main.fragment_questionnaire_two_answer.*

class QuestionnaireSeventhFragment : BaseFragment<QuestionnaireViewModel>() {

    companion object {
        fun buildWithAnArguments(
            questionnaireRequestBody: QuestionnaireRequestBody
        ) = Bundle().apply {
            putParcelable(QUESTIONNAIRE_KEY_CODE, questionnaireRequestBody)
        }
    }

    private val questionnaireRequestBody: QuestionnaireRequestBody
        get() = arguments?.getParcelable(QUESTIONNAIRE_KEY_CODE) ?: QuestionnaireRequestBody()

    override fun getLayoutID() = R.layout.fragment_questionnaire_two_answer

    override fun getVMClass() = QuestionnaireViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        answersGroup.check(R.id.secondOptionTitle)

        viewModel.setupLayout(
            8,
            getString(R.string.do_you_have_any_experience_with_virtual_currency_transactions_text),
            arrayListOf(
                getString(R.string.yes_text),
                getString(R.string.no_text)
            )
        )

        backButton.setOnClickListener {
            popBackStack()
        }

        viewModel.getQuestionnaireResult().observe(viewLifecycleOwner, Observer {
            handleResult(it) {
                navigate(R.id.action_questionnaireSeventhFragment_to_questionnaireFinishFragment)
            }
        })

        nextButton.setOnClickListener {
            if (viewModel.getCheckedAnswer(listOf(TRUE, NULL), answersGroup) == TRUE)
                navigate(
                    R.id.action_questionnaireSeventhFragment_to_questionnaireEighthFragment,
                    QuestionnaireEighthFragment.buildWithAnArguments(questionnaireRequestBody)
                )
            else
                viewModel.sendQuestionnaire(questionnaireRequestBody.copy(virtualCurrenciesUsed = null))
        }
    }
}