package app.eightpitch.com.ui.videoplayer.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import app.eightpitch.com.BR
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseActivity
import com.google.android.exoplayer2.util.Util.SDK_INT
import kotlinx.android.synthetic.main.activity_video_player.*

class VideoPlayerActivity : BaseActivity() {

    companion object {

        internal const val VIMEO_URL = "VIMEO_URL"

        fun generateIntent(
            context: Context,
            projectVideoLink: String
        ) = Intent(context, VideoPlayerActivity::class.java).apply {
            putExtra(VIMEO_URL, projectVideoLink)
        }
    }

    private lateinit var binding: ViewDataBinding
    private lateinit var viewModel: VideoPlayerActivityViewModel

    private val vimeoUrl: String
        get() = intent?.getStringExtra(VIMEO_URL) ?: ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupBinding()

        closeFrameButton.setOnClickListener {
            onBackPressed()
        }

        volumeFrameButton.setOnClickListener {
            viewModel.changeMuteOptionState()
        }

        fullScreenFrameButton.setOnClickListener {
            playerView.resizeMode = viewModel.computeResizeMode()
        }

        viewModel.getInitPlayerResult().observe(this, Observer { exoPlayer ->
            playerView.player = exoPlayer
        })
    }

    override fun onStart() {
        super.onStart()
        initPlayer { SDK_INT >= 24 }
    }

    override fun onResume() {
        super.onResume()
        hideSystemUi()
        initPlayer { SDK_INT < 24 }
    }

    override fun onPause() {
        super.onPause()
        releasePlayer { SDK_INT < 24 }
    }

    override fun onStop() {
        super.onStop()
        releasePlayer { SDK_INT >= 24 }
    }

    private fun setupBinding() {
        val factory = (this as BaseActivity).viewModelFactory
        viewModel = ViewModelProvider(this, factory)[VideoPlayerActivityViewModel::class.java]
        binding = DataBindingUtil.setContentView(this, R.layout.activity_video_player)
        binding.setVariable(BR.viewModel, viewModel)
    }

    @SuppressLint("InlinedApi")
    private fun hideSystemUi() {
        playerView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LOW_PROFILE
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)
    }

    private fun initPlayer(condition: () -> Boolean) {
        if (condition.invoke())
            viewModel.initializePlayer(applicationContext, vimeoUrl)
    }

    private fun releasePlayer(condition: () -> Boolean) {
        if (condition.invoke())
            viewModel.releasePlayer()
    }
}