package app.eightpitch.com.ui.payment.directdebitcompleate

import android.content.Context
import android.text.Spanned
import android.view.View
import androidx.core.text.toSpannable
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.BuildConfig
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.investment.ProjectInvestmentInfoResponse
import app.eightpitch.com.data.dto.payment.BankAccount
import app.eightpitch.com.data.dto.payment.SaveSequpayDataRequest
import app.eightpitch.com.data.dto.payment.SaveSequpayDataResponse
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.extensions.*
import app.eightpitch.com.models.PaymentModel
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.utils.IBANValidator
import app.eightpitch.com.utils.RegexPatterns.PATTERN_OWNER_PREFIX_CORRECT
import app.eightpitch.com.utils.RegexPatterns.PATTERN_VALID_OWNER
import app.eightpitch.com.utils.SingleActionLiveData
import app.eightpitch.com.utils.SingleActionObservableString
import app.eightpitch.com.views.interfaces.ShortFieldValidator
import kotlinx.coroutines.launch
import java.math.BigDecimal
import java.util.*
import javax.inject.Inject

class DirectDebitPaymentFragmentViewModel @Inject constructor(
    private val appContext: Context,
    private val userModel: UserModel,
    private val paymentModel: PaymentModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val title = ObservableField(appContext.getString(R.string.investment_payment_toolbar_title))

    val ownerField = ObservableField("")
    val ibanField = ObservableField("")
    val bicField = ObservableField("")

    val nominalTokenValueField = ObservableField<Spanned>()
    val totalSharesField = ObservableField("")
    val investedAmountField = ObservableField("")

    val stepValueField = ObservableField("")
    val minimalAmountValueField = ObservableField("")
    val valueUserLimitField = ObservableField("")
    val valueUserLimitVisibility = ObservableField(View.GONE)
    val feeField = ObservableField<String>()
    val valueFeeField = ObservableField<String>()

    var tokenName: String = ""
    private var investmentStep = BigDecimal.ONE

    private var owner: String = ""
    val ownerErrorHolder = SingleActionObservableString()

    private var IBAN: String = ""
    val IBANErrorHolder = SingleActionObservableString()

    private var bic: String = ""
    val bicErrorHolder = SingleActionObservableString()

    val ownerWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            owner = sequence.toString()
        }
    }

    val ibanWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            IBAN = sequence.toString()
        }
    }

    val bicWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            bic = sequence.toString()
        }
    }

    private val userMeResult = SingleActionLiveData<Result<User>>()
    internal fun getUserMeResult(): LiveData<Result<User>> = userMeResult

    internal fun getUser() {
        viewModelScope.launch {
            val result = asyncLoading {
                userModel.getUserData()
            }
            userMeResult.postAction(result)
        }
    }

    private val saveSecupayDataResult = SingleActionLiveData<Result<SaveSequpayDataResponse>>()
    internal fun getSaveSecupayDataResult(): LiveData<Result<SaveSequpayDataResponse>> =
        saveSecupayDataResult

    internal fun saveSecupayData(investmentId: String) {

        viewModelScope.launch {

            val result = asyncLoading {
                paymentModel.confirmDebitPayment(
                    investmentId, SaveSequpayDataRequest(BankAccount(bic, IBAN, owner))
                )
            }

            saveSecupayDataResult.postAction(result)
        }
    }

    internal fun validateInput(): Boolean {

        val accountOwnerError =
            owner.validateByDefaultPattern(
                appContext,
                R.string.account_owner,
                PATTERN_VALID_OWNER,
                PATTERN_OWNER_PREFIX_CORRECT
            )
        if (accountOwnerError.isNotEmpty())
            ownerErrorHolder.set(accountOwnerError)

        val IBANError = when {
            IBAN.isEmpty() -> getErrorEmptyString(appContext)
            !IBANValidator.validate(IBAN) -> appContext.getString(R.string.invalid_iban)
            else -> ""
        }
        if (IBANError.isNotEmpty())
            IBANErrorHolder.set(IBANError)

        val bicError = when {
            bic.isEmpty() -> getErrorEmptyString(appContext)
            else -> ""
        }
        if (bicError.isNotEmpty())
            bicErrorHolder.set(IBANError)

        return accountOwnerError.isEmpty() && IBANError.isEmpty() && bicError.isEmpty()
    }

    //region Setup binding
    internal fun setFields(
        user: User,
        projectInvestmentInfo: ProjectInvestmentInfoResponse
    ) {

        if (BuildConfig.BUILD_TYPE == "debug") {
            ibanField.set("NL93INGB3031628489")
            bicField.set("LOYDCHGGZCH")
        }

        ownerField.set(user.accountOwner)
        setPaymentData(projectInvestmentInfo)
    }

    private fun setPaymentData(projectInvestmentInfo: ProjectInvestmentInfoResponse) = projectInvestmentInfo.run {
        investmentStep = nominalValue.multiplySafe(investmentStepSize)
        setInvestmentStep(investmentStep)
        setMinimalInvestment(nominalValue.multiplySafe(minimumInvestmentAmount))
        setUserLimit(projectInvestmentInfo)
        setNominalTokenValue(investmentStep)

        val scaledFee = assetBasedFees.setScale(2)
        val tokens = unprocessedInvestment.amount
        val euroValue = tokens.multiplySafe(nominalValue)
        val countedFee = euroValue.multiplySafe(scaledFee.divideSafe(BigDecimal(100)))
        setInvestmentFee(scaledFee, countedFee)
        setInvestmentAmount(euroValue + countedFee)
        setTotalShares(tokens)
    }

    private fun setInvestmentStep(step: BigDecimal){
        val eurText = appContext.getString(R.string.eur_text)
        "${step.formatToCurrencyView()} $eurText".also { stepValueField.set(it) }
    }

    private fun setMinimalInvestment(minimalInvestment: BigDecimal) {
        val eurText = appContext.getString(R.string.eur_text)
        "${minimalInvestment.formatToCurrencyView()} $eurText".also {
            minimalAmountValueField.set(it)
        }
    }

    private fun setUserLimit(projectInvestmentInfo: ProjectInvestmentInfoResponse){
        val eurText = appContext.getString(R.string.eur_text)
        projectInvestmentInfo.investorClassLimit?.let { limit ->
            "${limit.formatToCurrencyView()} $eurText".also {
                valueUserLimitField.set(it)
                valueUserLimitVisibility.set(View.VISIBLE)
            }
        } ?: valueUserLimitVisibility.set(View.GONE)
    }

    private fun setNominalTokenValue(nominalTokenValue: BigDecimal) {
        nominalTokenValueField.set(
            appContext.getString(
                R.string.nominal_token_pattern_text,
                nominalTokenValue.formatToCurrencyView(),
                tokenName
            ).toHtml()
        )
    }

    private fun setInvestmentFee(assetBasedFees: BigDecimal, countedFee: BigDecimal) {
        val eurText = appContext.getString(R.string.eur_text)

        feeField.set(String.format(Locale.getDefault(),
            appContext.getString(R.string.investment_fee_text),
            assetBasedFees.formatToCurrencyView()))
        valueFeeField.set("${countedFee.setScale(2).formatToCurrencyView()} $eurText")
    }

    private fun setTotalShares(value: BigDecimal) {
        totalSharesField.set(
            appContext.getString(
                R.string.total_shares_pattern_text,
                value.setScale(2).formatToCurrencyView(),
                tokenName
            )
        )
    }

    private fun setInvestmentAmount(amountValue: BigDecimal) {
        investedAmountField.set(
            appContext.getString(
                R.string.total_amount,
                amountValue.formatToCurrencyView()
            )
        )
    }
    //endregion

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        bicField.set("")
        ibanField.set("")
        ownerField.set("")
        nominalTokenValueField.set("".toSpannable())
    }
}