package app.eightpitch.com.ui.investment.germanytax

import android.content.Context
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.user.TaxInformationBody
import app.eightpitch.com.data.dto.user.TaxInformationBody.TaxChurch
import app.eightpitch.com.data.dto.user.TaxInformationBody.TaxChurch.Companion.fromDisplayValue
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.utils.Empty
import app.eightpitch.com.utils.SingleActionLiveData
import app.eightpitch.com.views.interfaces.ShortFieldValidator
import kotlinx.coroutines.launch

class GermanTaxViewModel(
    appContext: Context,
    private val userModel: UserModel,
    threadsSeparator: ThreadsSeparator,
) : BaseViewModel(threadsSeparator) {

    var toolbarTitle = appContext.getString(R.string.german_tax_title)

    val isGermanyTaxResident = ObservableBoolean()
    val isTaxLiability = ObservableBoolean()

    private var taxId = ""
    val taxIdWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            taxId = sequence.toString()
        }
    }

    private var taxOffice = ""
    val taxOfficeWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            taxOffice = sequence.toString()
        }
    }

    var taxChurch = ""
    internal fun setChurch(position: Int) {
        taxChurch = TaxChurch.getTaxChurches()[position]
    }

    internal fun validate() {
        when (isGermanyTaxResident.get()) {
            false -> updateTaxInformation(TaxInformationBody(_isTaxResidentInGermany = false))
            else -> validateFields()
        }
    }

    private val validateFieldsResult = SingleActionLiveData<Empty>()
    internal fun getValidateFieldsResultResult(): LiveData<Empty> = validateFieldsResult

    private fun validateFields() {
        when {
            taxId.isEmpty() || taxOffice.isEmpty() -> validateFieldsResult.postAction(Empty.instance)
            else -> updateTaxInformation(
                TaxInformationBody(
                    _isTaxResidentInGermany = true,
                    _churchTaxAttribute = fromDisplayValue(taxChurch).toString(),
                    _churchTaxLiability = isTaxLiability.get(),
                    _responsibleTaxOffice = taxOffice,
                    _taxNumber = taxId
                )
            )
        }
    }

    private val taxInformationResult = SingleActionLiveData<Result<Empty>>()
    internal fun getTaxInformationResultResult(): LiveData<Result<Empty>> = taxInformationResult

    private fun updateTaxInformation(taxInformationRequestBody: TaxInformationBody) {
        viewModelScope.launch {
            val result = asyncLoading {
                userModel.updateTaxInformation(taxInformationRequestBody)
            }
            taxInformationResult.postAction(result)
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        taxId = ""
        taxChurch = ""
        taxOffice = ""
        isGermanyTaxResident.set(false)
        isTaxLiability.set(false)
    }
}