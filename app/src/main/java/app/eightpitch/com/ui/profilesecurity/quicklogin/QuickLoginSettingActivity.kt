package app.eightpitch.com.ui.profilesecurity.quicklogin

import android.os.Bundle
import androidx.navigation.Navigation
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseActivity
import app.eightpitch.com.extensions.showDialog
import app.eightpitch.com.ui.pincode.PinCodeFragment
import app.eightpitch.com.ui.pincode.PinCodeFragment.Companion.ACTIVATE_BIOMETRIC
import app.eightpitch.com.ui.pincode.PinCodeFragment.Companion.DELETE_CODE
import app.eightpitch.com.ui.pincode.PinCodeScreenState.CREATE_CODE
import app.eightpitch.com.ui.pincode.PinCodeScreenState.ENTER_ACTUALLY_CODE
import app.eightpitch.com.ui.pincode.PinCodeScreenState.ENTER_CODE
import app.eightpitch.com.utils.Logger
import app.eightpitch.com.utils.Logger.TAG_GENERIC_ERROR

class QuickLoginSettingActivity : BaseActivity() {

    companion object {

        const val QUICK_LOGIN_NAVIGATION_ID = "QUICK_LOGIN_NAVIGATION_ID"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quick_login)
        val (destination, arguments) = parseArguments()
        setupGraph(destination, arguments)
    }

    private fun parseArguments(): Pair<Int?, Bundle> {
        val destination = intent.extras?.get(QUICK_LOGIN_NAVIGATION_ID) as? Int
        val args = when (destination) {
            R.id.createPinCodeFragment -> buildArguments(CREATE_CODE.toString())
            R.id.deletePinCodeFragment -> buildArguments(ENTER_ACTUALLY_CODE.toString(), DELETE_CODE)
            R.id.enterPinCodeFragment -> buildArguments(ENTER_CODE.toString())
            R.id.activateFingerPrint -> buildArguments(ENTER_ACTUALLY_CODE.toString(), ACTIVATE_BIOMETRIC)
            R.id.fingerPrintWithEnterPinCodeFragment -> buildArguments(CREATE_CODE.toString(), ACTIVATE_BIOMETRIC)
            else -> buildArguments(ENTER_CODE.toString())
        }
        return destination to args
    }

    private fun buildArguments(navigation: String, reason: String = "") =
        PinCodeFragment.buildWithArguments(navigation = navigation, reason = reason)

    private fun setupGraph(destination: Int?, arguments: Bundle) {
        try {
            Navigation.findNavController(this, R.id.nav_host_fragment).apply {
                setGraph(navInflater.inflate(R.navigation.quick_login_settings_navigation_graph).also { graph ->
                    destination?.let {
                        graph.startDestination = it
                    }
                }, arguments)
            }
        } catch (error: Throwable) {
            Logger.logError(TAG_GENERIC_ERROR, "Cannot setup navigation graph for ${
                QuickLoginSettingActivity::class.simpleName
            }, reason: ", error)
            showDialog(message = getString(R.string.something_go_wrong_exception_text))
        }
    }
}