package app.eightpitch.com.ui.dynamicdetails

import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.dynamicviews.LocalFileIdToBackendFileId
import app.eightpitch.com.data.dto.dynamicviews.language.I18nData
import app.eightpitch.com.data.dto.investment.ProjectInvestmentInfoResponse
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.ACTIVE
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.COMING_SOON
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.FINISHED
import app.eightpitch.com.data.dto.projectdetails.GetProjectResponse
import app.eightpitch.com.data.dto.projectdetails.ProjectPage.Companion.PUBLISHED
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.models.*
import app.eightpitch.com.utils.SingleActionLiveData
import app.eightpitch.com.utils.constructor.ProjectDetailsTab
import app.eightpitch.com.utils.constructor.ProjectDetailsTabsContentParser
import kotlinx.coroutines.launch

typealias I18nDataToContentIdMap = Pair<I18nData, LocalFileIdToBackendFileId>

class DynamicProjectDetailsViewModel(
    private val projectDetailsTabsContentParser: ProjectDetailsTabsContentParser,
    private val userModel: UserModel,
    private val projectModel: ProjectModel,
    private val investmentModel: InvestmentModel,
    threadsSeparator: ThreadsSeparator,
) : BaseViewModel(threadsSeparator) {

    val investButtonVisibility = ObservableField(GONE)
    val placeholderVisibility = ObservableBoolean(false)

    private var recentUser: User? = null
    private var projectInfo: GetProjectResponse? = null
    var isSoftCapProject: Boolean = false
        private set

    internal fun getRecentUser() = recentUser
    internal fun getProjectInfo() = projectInfo

    private val dynamicProjectDetailsResult =
        SingleActionLiveData<Result<Pair<ArrayList<ProjectDetailsTab>, I18nDataToContentIdMap>>>()

    internal fun getDynamicProjectDetailsResult(): LiveData<Result<Pair<ArrayList<ProjectDetailsTab>, I18nDataToContentIdMap>>> =
        dynamicProjectDetailsResult

    internal fun parseTabDetailsBy(projectDetailsJson: String) {
        viewModelScope.launch {
            val result = asyncLoading {
                val objectConstructor =
                    projectDetailsTabsContentParser.parseProjectDetailsTabsBlock(projectDetailsJson)
                projectDetailsTabsContentParser.parseUIItems(objectConstructor) to (objectConstructor.run { i18nData to localIdToBackendIdMap })
            }
            dynamicProjectDetailsResult.postAction(result)
        }
    }

    private val projectResult = SingleActionLiveData<Result<GetProjectResponse>>()
    internal fun getProjectResult(): LiveData<Result<GetProjectResponse>> = projectResult

    /**
     * Method that will retrieve a user and the project
     * to handle visibility of `invest now` button and
     * UI state
     */
    internal fun retrieveData(projectId: String) {

        viewModelScope.launch {
            val result = asyncLoading {
                val user = userModel.getUserData()
                val project = projectModel.getProjectInfo(projectId)
                recentUser = user
                setProjectsFields(project)
                project
            }

            projectResult.postAction(result)
        }
    }

    private fun setProjectsFields(getProjectResponse: GetProjectResponse) {
        projectInfo = getProjectResponse
        val softCap = getProjectResponse.tokenParametersDocument.softCap
        isSoftCapProject = softCap != null && softCap != 0L
        investButtonVisibility.set(checkValidateInvestButton(getProjectResponse))
    }

    /**
     * Validates and returns a visibility of an `Invest now` button
     */
    private fun checkValidateInvestButton(project: GetProjectResponse): Int {
        val recentUserLevel = recentUser?.status
        val projectStatus = project.projectStatus
        val projectPageStatus = project.projectPage.projectPageStatus

        return when {
            projectStatus == ACTIVE && projectPageStatus == PUBLISHED -> {
                if (User.LEVEL_2 == recentUserLevel) VISIBLE else GONE
            }
            projectStatus == FINISHED && projectPageStatus == PUBLISHED -> GONE
            projectStatus == COMING_SOON && projectPageStatus == PUBLISHED -> VISIBLE
            else -> GONE
        }
    }

    private val projectInvestmentInfoResult =
        SingleActionLiveData<Result<ProjectInvestmentInfoResponse>>()

    internal fun getProjectInvestmentInfoResult(): LiveData<Result<ProjectInvestmentInfoResponse>> =
        projectInvestmentInfoResult

    internal fun getProjectInvestmentInfo(projectId: String) {
        viewModelScope.launch {
            val result = asyncLoading {
                investmentModel.run {
                    isInvestmentAllowed(projectId)
                    getProjectInvestmentInfo(projectId)
                }
            }
            projectInvestmentInfoResult.postAction(result)
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        projectInfo = null
        recentUser = null
        placeholderVisibility.set(false)
        investButtonVisibility.set(GONE)
        isSoftCapProject = false
    }
}