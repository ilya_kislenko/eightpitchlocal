package app.eightpitch.com.ui.questionnaire

import android.os.Bundle
import android.view.View
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody.CREATOR.FROM_1_TO_3
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody.CREATOR.LESS_THAN_1
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody.CREATOR.MORE_THAN_3
import app.eightpitch.com.ui.questionnaire.QuestionnaireFirstFragment.Companion.QUESTIONNAIRE_KEY_CODE
import kotlinx.android.synthetic.main.fragment_questionnaire_three_answer.*

class QuestionnaireFourthFragment : BaseFragment<QuestionnaireViewModel>() {

    companion object {
        fun buildWithAnArguments(
            questionnaireRequestBody: QuestionnaireRequestBody
        ) = Bundle().apply {
            putParcelable(QUESTIONNAIRE_KEY_CODE, questionnaireRequestBody)
        }
    }

    private val questionnaireRequestBody: QuestionnaireRequestBody
        get() = arguments?.getParcelable(QUESTIONNAIRE_KEY_CODE) ?: QuestionnaireRequestBody()

    override fun getLayoutID() = R.layout.fragment_questionnaire_three_answer

    override fun getVMClass() = QuestionnaireViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        viewModel.setupLayout(
            5,
            getString(R.string.how_big_are_your_prior_knowledge_and_experience_with_these_asset_classes_text),
            arrayListOf(
                getString(R.string.less_than_one_year_text),
                getString(R.string.one_three_years_text),
                getString(R.string.more_than_three_years_text)
            )
        )

        nextButton.setOnClickListener {
            navigate(
                R.id.action_questionnaireFourthFragment_to_questionnaireFifthFragment,
                QuestionnaireFifthFragment.buildWithAnArguments(
                    questionnaireRequestBody.copy(
                        experienceDuration =
                        viewModel.getCheckedAnswer(
                            listOf(
                                LESS_THAN_1,
                                FROM_1_TO_3,
                                MORE_THAN_3
                            ), answersGroup
                        )
                    )
                )
            )
        }

        backButton.setOnClickListener {
            popBackStack()
        }
    }
}