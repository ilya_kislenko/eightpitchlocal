package app.eightpitch.com.ui.projects.adapters

import app.eightpitch.com.data.dto.project.*
import app.eightpitch.com.extensions.NO_TYPE
import java.math.BigDecimal

data class ProjectItem(
    var id: Int = 0,
    var videoUrl: String = "",
    var projectPageUrl: String = "",
    var companyName: String = "",
    var description: String = "",
    var graph: AmountOfProjectInvestments = AmountOfProjectInvestments(softCap = null),
    var financialInformation: FinancialInformation = FinancialInformation(),
    var tokenParametersDocument: TokenParametersDocument = TokenParametersDocument(),
    var monetaryAmount: BigDecimal = BigDecimal.ZERO,
    @ProjectInvestment.Companion.InvestmentStatus var investmentStatus: String = NO_TYPE,
    var backgroundImageUrl: String = "",
    var thumbNailImageUrl: String = ""
)
