package app.eightpitch.com.ui.payment.hardcappaymentselection

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.base.BaseModel.ServerException
import app.eightpitch.com.base.rootbackhandlers.ChildNavGraphBackPressureHandler
import app.eightpitch.com.data.dto.payment.TransactionInfoResponse.Companion.OK
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.data.dto.investment.ProjectInvestmentInfoResponse
import app.eightpitch.com.extensions.handleResultWithError
import app.eightpitch.com.ui.payment.compleatepayment.ClassicBankPaymentFragment
import app.eightpitch.com.ui.payment.onlinebanking.FintBankTransferFragment
import app.eightpitch.com.ui.payment.onlinebanking.KlarnaBankTransferFragment
import kotlinx.android.synthetic.main.fragment_hard_cap_payment_selection.*

class HardCapPaymentSelectionFragment : BaseFragment<HardCapPaymentSelectionViewModel>(),
    ChildNavGraphBackPressureHandler {

    companion object {

        const val INVESTMENT_ID = "INVESTMENT_ID"
        const val PROJECT_ID = "PROJECT_ID"
        const val HARD_CAP_PAYMENTS_DATA = "HARD_CAP_PAYMENTS_DATA"

        fun buildWithAnArguments(
            investmentId: String,
            projectId: String,
            projectInvestmentInfo: ProjectInvestmentInfoResponse
        ) = Bundle().apply {
            putString(INVESTMENT_ID, investmentId)
            putString(PROJECT_ID, projectId)
            putParcelable(HARD_CAP_PAYMENTS_DATA, projectInvestmentInfo)
        }
    }

    private val investmentId: String
        get() = arguments?.getString(INVESTMENT_ID, "") ?: ""

    private val projectId: String
        get() = arguments?.getString(PROJECT_ID, "") ?: ""

    private val projectInvestmentInfo: ProjectInvestmentInfoResponse
        get() = arguments?.getParcelable(HARD_CAP_PAYMENTS_DATA) ?: ProjectInvestmentInfoResponse()

    override fun getLayoutID() = R.layout.fragment_hard_cap_payment_selection

    override fun getVMClass() = HardCapPaymentSelectionViewModel::class.java

    override fun onResume() {
        super.onResume()
        viewModel.getProjectInvestmentInfo(projectId)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        fintItemView.setOnClickListener {
            viewModel.submitFint(investmentId)
        }

        klarnaItemView.setOnClickListener {
            viewModel.submitKlarna(investmentId)
        }

        classicItemView.setOnClickListener {
            navigate(
                R.id.action_hardCapPaymentSelectionFragment_to_classicBankPaymentFragment,
                ClassicBankPaymentFragment.buildWithAnArguments(projectInvestmentInfo)
            )
        }

        viewModel.getFintTransactionResult().observe(viewLifecycleOwner, Observer { result ->
            handleResult(result) { transaction ->
                when (transaction.responseStatus) {
                    OK -> navigate(
                        R.id.action_hardCapPaymentSelectionFragment_to_fintBankTransferFragment,
                        FintBankTransferFragment.buildWithAnArguments(
                            transaction.paymentUrl, projectInvestmentInfo.getProjectName()
                        )
                    )
                    else -> {
                        navigate(R.id.action_hardCapPaymentSelectionFragment_to_paymentDeclinedFragment)
                    }
                }
            }
        })

        viewModel.getKlarnaTransactionResult().observe(viewLifecycleOwner, Observer {
            handleResult(it) { transaction ->
                when (transaction.responseStatus) {
                    OK -> navigate(R.id.action_hardCapPaymentSelectionFragment_to_klarnaBankTransferFragment,
                        KlarnaBankTransferFragment.buildWithAnArguments(
                            transaction.paymentUrl, projectInvestmentInfo.getProjectName()
                        )
                    )
                    else -> {
                        navigate(R.id.action_hardCapPaymentSelectionFragment_to_paymentDeclinedFragment)
                    }
                }
            }
        })

        viewModel.getProjectInvestmentInfoResult().observe(viewLifecycleOwner, { result ->
            handleResultWithError(result, ServerException::class.java,
                completeIfError = {
                    popBackStack(R.id.projectDetailsFragment)
                }, completeIfSuccess = {}
            )
        })
    }

    override fun handleBackPressure(): Boolean {
        return popBackStack(R.id.projectDetailsFragment)
    }
}