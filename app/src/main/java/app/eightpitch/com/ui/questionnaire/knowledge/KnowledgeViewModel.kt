package app.eightpitch.com.ui.questionnaire.knowledge

import android.content.Context
import androidx.databinding.ObservableArrayList
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody

class KnowledgeViewModel(
    private val appContext: Context,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    var toolbarTitle = ""

    var answersData = ObservableArrayList<String>().apply {
        addAll(QuestionnaireRequestBody.Knowledge.values().map { appContext.getString(it.displayValue) })
    }

    internal fun setToolbarIndex(index: Int) {
        toolbarTitle = appContext.getString(R.string.questionnaire_title_pattern, index)
    }

    private var knowledge: MutableList<String> = mutableListOf()
    internal fun getSelectedKnowledge(): List<String> = knowledge

    internal fun setAnswers(
        name: String,
        isChecked: Boolean
    ) {
        if (isChecked)
            knowledge.add(name)
        else
            knowledge.remove(name)
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        knowledge.clear()
    }
}