package app.eightpitch.com.ui.dynamicdetails

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat.getColor
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.dynamicviews.language.I18nData
import app.eightpitch.com.data.dto.dynamicviews.mainfacts.FinancialInformationItem
import app.eightpitch.com.data.dto.investment.ProjectInvestmentInfoResponse
import app.eightpitch.com.data.dto.project.ProjectInvestment
import app.eightpitch.com.data.dto.project.ProjectInvestment.Companion.BOOKED
import app.eightpitch.com.data.dto.project.ProjectInvestment.Companion.CONFIRMED
import app.eightpitch.com.data.dto.project.ProjectInvestment.Companion.PAID
import app.eightpitch.com.data.dto.projectdetails.AdditionalFields
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.dynamicdetails.maintab.MainFactsFragment
import app.eightpitch.com.ui.investment.CurrencyChooserFragment
import app.eightpitch.com.ui.investment.ViewPagerSharableViewModel
import app.eightpitch.com.ui.investment.processing.InvestmentProcessingFragment
import app.eightpitch.com.ui.investment.unprocessed.UnfinishedInvestmentFragment
import app.eightpitch.com.ui.payment.hardcappaymentselection.HardCapPaymentSelectionFragment
import app.eightpitch.com.ui.payment.softcappaymentselection.SoftCapPaymentSelectionFragment
import app.eightpitch.com.ui.projectsdetail.ProjectDetailsFragment
import app.eightpitch.com.utils.OnSwipeTouchListener
import app.eightpitch.com.utils.constructor.ProjectDetailsTab
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.GRAVITY_FILL
import com.google.android.material.tabs.TabLayout.MODE_SCROLLABLE
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.fragment_dynamic_progect_details_parent.*
import java.math.BigDecimal

/**
 * Fragment that is responsible for programmatically creation of UI for the project details
 * according to the json object which was passed via arguments here.
 */
class DynamicProjectDetailsParentFragment
    : BaseFragment<DynamicProjectDetailsViewModel>(), OnSwipeUpBackPressuredListener {

    companion object {

        internal const val PROJECT_DETAILS_JSON_KEY = "PROJECT_DETAILS_JSON_KEY"
        internal const val ADDITIONAL_FIELDS_KEY = "ADDITIONAL_FIELDS_KEY"
        internal const val FINANCIAL_INFORMATION_KEY = "FINANCIAL_INFORMATION_KEY"

        fun buildWithAnArguments(
            projectId: String,
            projectDetailsJson: String,
            additionalFields: AdditionalFields,
            financialInformationItem: FinancialInformationItem,
        ) = Bundle().apply {
            putString(ProjectDetailsFragment.PROJECT_ID, projectId)
            putString(PROJECT_DETAILS_JSON_KEY, projectDetailsJson)
            putParcelable(ADDITIONAL_FIELDS_KEY, additionalFields)
            putParcelable(FINANCIAL_INFORMATION_KEY, financialInformationItem)
        }
    }

    private var tabLayoutMediator: TabLayoutMediator? = null
    private val projectId: String
        get() = arguments?.getString(ProjectDetailsFragment.PROJECT_ID, "") ?: ""
    private val projectDetailsJson: String
        get() = arguments?.getString(PROJECT_DETAILS_JSON_KEY).orDefaultValue("")
    private val additionalFields: AdditionalFields
        get() = arguments?.getParcelable<AdditionalFields>(ADDITIONAL_FIELDS_KEY).orDefaultValue(AdditionalFields())
    private val financialInformation: FinancialInformationItem
        get() = arguments?.getParcelable<FinancialInformationItem>(FINANCIAL_INFORMATION_KEY)
            .orDefaultValue(FinancialInformationItem())

    override fun getLayoutID() = R.layout.fragment_dynamic_progect_details_parent

    override fun getVMClass() = DynamicProjectDetailsViewModel::class.java

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()
        mainLayout.setOnTouchListener(object : OnSwipeTouchListener(context) {
            override fun onSwipeDown() {
                super.onSwipeDown()
                popBackStack()
            }
        })
        //Disabling the touch on the main layout
        mainLayout.setOnTouchListener { _, _ -> true }

        fabEuro.setDisablingClickListener {
            viewModel.getProjectInvestmentInfo(projectId)
        }

        viewModel.run {
            getProjectResult().observe(viewLifecycleOwner, { handleResult(it) {} })
            getProjectInvestmentInfoResult().observe(viewLifecycleOwner, { result ->
                handleResult(result) {
                    navigateForwardWith(it)
                }
            })
        }

        viewModel.getDynamicProjectDetailsResult().observe(viewLifecycleOwner, { result ->
            handleResultWithError(result = result,
                doNotShowDialogFor = NoSuchElementException::class.java,
                completeIfSuccess = { pair ->
                    val (items, i18nDataToContentIdMap) = pair
                    buildParentUIElements(items.filter { it.second.isNotEmpty() }, i18nDataToContentIdMap)
                },
                completeIfError = { buildParentUIElements() })
        })

        viewModel.run {
            parseTabDetailsBy(projectDetailsJson)
            retrieveData(projectId)
        }
    }

    private fun buildParentUIElements(
        items: List<ProjectDetailsTab> = listOf(),
        i18nDataToContentIdMap: I18nDataToContentIdMap = I18nDataToContentIdMap(I18nData(), hashMapOf()),
    ) {
        val fragments = createChildFragments(items, i18nDataToContentIdMap)
        val tabLayout = createTabLayout(fragments.size)
        val viewPager2 = createViewPager(fragments)
        mainLayout.apply {
            addView(tabLayout)
            addView(viewPager2.apply {
                (layoutParams as? RelativeLayout.LayoutParams)?.addRule(RelativeLayout.BELOW, tabLayout.id)
            })
        }
        val tabTitles = arrayListOf<String>().apply {
            add(getString(R.string.main_facts_text))
            addAll(items.map { it.first.tabName })
        }
        bindViewPagerWithTabLayout(tabLayout, viewPager2, tabTitles)
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                //Nothing.
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                //Nothing.
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                tab?.position?.let { ((viewPager2.adapter as? ProjectDetailsViewPagerAdapter)?.getFragmentByPosition(it) as? FragmentScrollable)?.scrollTop() }
            }
        })
    }

    /**
     * Method that is dynamically creating a [TabLayout]
     * with that amount of tabs that is required for particular project
     */
    private fun createTabLayout(itemsSize: Int): TabLayout {
        val tabLayout = TabLayout(requireContext()).apply {
            id = View.generateViewId()
            layoutParams = RelativeLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
            tabMode = MODE_SCROLLABLE
            setTabTextColors(
                getColor(context, R.color.gray15),
                getColor(context, R.color.lightRed)
            )
            setSelectedTabIndicatorColor(getColor(context, R.color.lightRed))
            tabRippleColor = ResourcesCompat.getColorStateList(resources, R.color.lightRed, context.theme)

            if (resources.getBoolean(R.bool.isItTabletDevice)) {
                tabGravity = GRAVITY_FILL
            }
        }
        repeat(itemsSize) {
            tabLayout.apply { addTab(newTab()) }
        }
        return tabLayout
    }

    /**
     * Method that is dynamically creating a [ViewPager2]
     * according to the state of project details
     */
    private fun createViewPager(fragments: List<Fragment>): ViewPager2 {
        return ViewPager2(requireContext()).apply {
            layoutParams = RelativeLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT)
            isUserInputEnabled = false
            orientation = ViewPager2.ORIENTATION_VERTICAL
            adapter = ProjectDetailsViewPagerAdapter(this@DynamicProjectDetailsParentFragment, fragments)
        }
    }

    private fun createChildFragments(
        items: List<ProjectDetailsTab>,
        i18nDataToContentIdMap: I18nDataToContentIdMap,
    ): List<Fragment> {
        val fragments = arrayListOf<Fragment>()
        fragments.add(MainFactsFragment.buildWithArguments(additionalFields, financialInformation))
        items.forEach {
            val (tabViewOrder, uiElements) = it
            fragments.add(
                DynamicProjectDetailsChildFragment.buildWithArguments(
                    uiElements,
                    i18nDataToContentIdMap,
                    tabViewOrder.orderId
                )
            )
        }
        return fragments
    }

    /**
     * Method that is binds [ViewPager2] and [TabLayout]
     */
    private fun bindViewPagerWithTabLayout(
        tabLayout: TabLayout,
        viewPager: ViewPager2,
        tabTitles: List<String>,
    ) {
        //TODO Restore selected position
        tabLayoutMediator = TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.text = tabTitles[position]
        }
        tabLayoutMediator?.attach()
        viewPager.setCurrentItem(0, true)
    }

    private fun navigateForwardWith(projectInvestmentInfo: ProjectInvestmentInfoResponse) {
        projectInvestmentInfo.setProjectName(viewModel.getProjectInfo()?.projectPage?.companyName)
        when (projectInvestmentInfo.unprocessedInvestment.investmentStatus) {
            PAID -> {
                navigate(R.id.action_dynamicProjectDetailsParentFragment_to_investmentProcessingFragment)
            }
            ProjectInvestment.CANCELED_IN_PROGRESS, ProjectInvestment.NEW, ProjectInvestment.PENDING -> {
                navigate(R.id.action_dynamicProjectDetailsParentFragment_to_investmentProcessingFragment,
                    InvestmentProcessingFragment.buildWithAnArguments(projectId))
            }
            BOOKED -> {
                val arguments =
                    UnfinishedInvestmentFragment.buildWithAnArguments(
                        projectId,
                        projectInvestmentInfo
                    )
                navigate(
                    R.id.action_dynamicProjectDetailsParentFragment_to_unFinishedInvestmentFragment,
                    arguments
                )
            }
            CONFIRMED -> {

                val investmentId = projectInvestmentInfo.unprocessedInvestment.investmentId
                val fee =
                    viewModel.getProjectInfo()?.tokenParametersDocument?.assetBasedFees.orDefaultValue(
                        BigDecimal(0)
                    )
                if (viewModel.isSoftCapProject) {
                    navigate(
                        R.id.action_dynamicProjectDetailsParentFragment_to_softCapPaymentSelectionFragment,
                        SoftCapPaymentSelectionFragment.buildWithAnArguments(
                            investmentId,
                            projectId,
                            projectInvestmentInfo.copy(assetBasedFees = fee).also {
                                it.setProjectName(viewModel.getProjectInfo()?.projectPage?.companyName)
                            }
                        )
                    )
                } else {
                    navigate(
                        R.id.action_dynamicProjectDetailsParentFragment_to_hardCapPaymentSelectionFragment,
                        HardCapPaymentSelectionFragment.buildWithAnArguments(
                            investmentId,
                            projectId,
                            projectInvestmentInfo.copy(assetBasedFees = fee).also {
                                it.setProjectName(viewModel.getProjectInfo()?.projectPage?.companyName)
                            }
                        )
                    )
                }
            }
            else -> {
                val viewPagerSharableViewModel =
                    injectViewModel(ViewPagerSharableViewModel::class.java)
                viewPagerSharableViewModel.setValue(projectInvestmentInfo.minimumInvestmentAmount.toBigDecimal())
                val arguments = CurrencyChooserFragment.buildWithAnArguments(
                    projectId, projectInvestmentInfo, viewModel.getRecentUser().orDefaultValue(User())
                )
                navigate(R.id.action_dynamicProjectDetailsParentFragment_to_currencyChooserFragment, arguments)
            }
        }
    }

    override fun onOverSwiped() {
        popBackStack()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        tabLayoutMediator?.detach()
        tabLayoutMediator = null
    }
}
