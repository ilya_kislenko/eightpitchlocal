package app.eightpitch.com.ui.profilesecurity.changingpassword

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.changingpassword.ChangePasswordRequestBody
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.extensions.*
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.utils.Empty
import app.eightpitch.com.utils.RegexPatterns.PATTERN_VALID_PASSWORD_SYMBOLS
import app.eightpitch.com.utils.SingleActionLiveData
import app.eightpitch.com.utils.SingleActionObservableString
import app.eightpitch.com.views.interfaces.ShortFieldValidator
import kotlinx.coroutines.launch
import javax.inject.Inject

class SetNewPasswordViewModel
@Inject constructor(
    private val appContext: Context,
    private val userModel: UserModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val toolbarTitle = appContext.getString(R.string.set_your_new_password_text)

    var firstPassword: String = ""
    var firstPasswordErrorHolder = SingleActionObservableString()

    private var secondPassword: String = ""
    var secondPasswordErrorHolder = SingleActionObservableString()

    val firstPasswordWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            firstPassword = sequence.toString()
        }
    }

    val secondPasswordWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            secondPassword = sequence.toString()
        }
    }

    internal fun validatePasswords(
        interactionId: String,
        userExternalId: String
    ) {

        val isPasswordsEquals = firstPassword == secondPassword
        if (!isPasswordsEquals) {
            val passwordsMismatchError = appContext.getString(R.string.password_mismatch)
            firstPasswordErrorHolder.set(passwordsMismatchError)
            secondPasswordErrorHolder.set(passwordsMismatchError)
            return
        }

        val firstPasswordError = when {
            firstPassword.isEmpty() -> getErrorEmptyString(appContext)
            !firstPassword.matchPattern(PATTERN_VALID_PASSWORD_SYMBOLS) -> appContext.getString(
                R.string.not_valid_password
            )
            else -> ""
        }.takeIf { it.isNotEmpty() }?.let { firstPasswordErrorHolder.set(it); it }

        val secondPasswordError = when {
            secondPassword.isEmpty() -> getErrorEmptyString(appContext)
            !secondPassword.matchPattern(PATTERN_VALID_PASSWORD_SYMBOLS) -> appContext.getString(
                R.string.not_valid_password
            )
            else -> ""
        }.takeIf { it.isNotEmpty() }?.let { secondPasswordErrorHolder.set(it); it }

        val arePasswordsValid =
            firstPasswordError.isNullOrEmpty() && secondPasswordError.isNullOrEmpty()

        if (arePasswordsValid)
            changePassword(interactionId, userExternalId)
    }

    private val changingPasswordResult = SingleActionLiveData<Result<Empty>>()
    internal fun getChangingPasswordResult(): LiveData<Result<Empty>> = changingPasswordResult

    private fun changePassword(
        interactionId: String,
        userExternalId: String
    ) {
        viewModelScope.launch {
            val result = asyncLoading {
                userModel.changePassword(ChangePasswordRequestBody(
                    password = firstPassword,
                    confirmPassword = secondPassword,
                    interactionId = interactionId,
                    userExternalId = userExternalId
                ))
            }
            changingPasswordResult.postAction(result)
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        firstPassword = ""
        secondPassword = ""
        firstPasswordErrorHolder.set("")
        secondPasswordErrorHolder.set("")
    }
}