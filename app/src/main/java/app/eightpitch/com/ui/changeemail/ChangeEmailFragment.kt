package app.eightpitch.com.ui.changeemail

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.base.baseviewmodels.codeverification.DefaultCodeValidationFragment
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO
import app.eightpitch.com.base.baseviewmodels.codeverification.dto.ValidationScreenInfoDTO.CREATOR.CHANGE_EMAIL
import app.eightpitch.com.data.dto.signup.CommonInvestorRequestBody
import app.eightpitch.com.extensions.handleResult
import kotlinx.android.synthetic.main.fragment_change_email.*

class ChangeEmailFragment : BaseFragment<ChangeEmailFragmentViewModel>() {

    override fun getLayoutID() = R.layout.fragment_change_email

    override fun getVMClass() = ChangeEmailFragmentViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindLoadingIndicator()

        buttonNext.setOnClickListener {
            viewModel.validateEmail()
        }

        viewModel.getUserMeResult().observe(viewLifecycleOwner, { handleResult(it) {} })
        viewModel.getRequireEmailTokenResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) {
                val arguments =
                    DefaultCodeValidationFragment.buildWithAnArguments(
                        validationScreenInfo = createValidationScreenInfo(),
                        partialUser = CommonInvestorRequestBody(
                            email = viewModel.getEmail()
                        )
                    )
                navigate(
                    R.id.action_changeEmailFragment_to_defaultCodeValidationFragment,
                    arguments
                )
            }
        })

        viewModel.getUser()
    }

    private fun createValidationScreenInfo() = ValidationScreenInfoDTO(
        verificationType = CHANGE_EMAIL,
        validationButtonName = getString(R.string.change_email),
        header = getString(R.string.change_email_header),
        toolbarTitle = getString(R.string.changing_email),
        subTitleMessage = getString(R.string.verification_code_we_sent_you_to_your_email),
        resendButtonName = getString(R.string.resend_confirmation_email)
    )
}