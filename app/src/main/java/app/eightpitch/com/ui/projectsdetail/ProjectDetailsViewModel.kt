package app.eightpitch.com.ui.projectsdetail

import android.content.Context
import android.view.View
import androidx.databinding.*
import androidx.lifecycle.*
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.investment.ProjectInvestmentInfoResponse
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.ACTIVE
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.COMING_SOON
import app.eightpitch.com.data.dto.project.InvestorsProject.Companion.FINISHED
import app.eightpitch.com.data.dto.projectdetails.GetProjectResponse
import app.eightpitch.com.data.dto.projectdetails.ProjectPage.Companion.PUBLISHED
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.models.InvestmentModel
import app.eightpitch.com.models.ProjectModel
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.ui.projects.adapters.ProjectItem
import app.eightpitch.com.utils.SingleActionLiveData
import app.eightpitch.com.utils.models.VimeoModel
import kotlinx.coroutines.launch
import javax.inject.Inject

class ProjectDetailsViewModel @Inject constructor(
    val appContext: Context,
    private val projectModel: ProjectModel,
    private val investmentModel: InvestmentModel,
    private val userModel: UserModel,
    private val vimeoModel: VimeoModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val title = appContext.getString(R.string.projects)

    val project = ObservableArrayList<ProjectItem>()

    //region Binding
    val investButtonVisibility = ObservableField(View.GONE)
    val companyNameField = ObservableField("")
    val firstInvestmentArgumentsField = ObservableField("")
    val secondInvestmentArgumentsField = ObservableField("")
    val thirdInvestmentArgumentsField = ObservableField("")
    //endregion

    private var recentUser: User? = null
    private var projectPromoVideoLink: String? = null
    private var projectInfo: GetProjectResponse? = null
    var isSoftCapProject: Boolean = false
        private set

    internal fun getRecentUser() = recentUser
    internal fun getProjectInfo() = projectInfo

    private val projectResult = SingleActionLiveData<Result<GetProjectResponse>>()
    internal fun getProjectResult(): LiveData<Result<GetProjectResponse>> = projectResult

    /**
     * Method that will retrieve a user and the project
     * to handle visibility of `invest now` button and
     * UI state
     */
    internal fun retrieveData(projectId: String) {

        viewModelScope.launch {
            val result = asyncLoading {
                val user = userModel.getUserData()
                val project = projectModel.getProjectInfo(projectId)
                recentUser = user
                setProjectsFields(project)
                project
            }

            projectResult.postAction(result)
        }
    }

    private fun setProjectsFields(getProjectResponse: GetProjectResponse) {
        projectInfo = getProjectResponse
        val softCap = getProjectResponse.tokenParametersDocument.softCap
        isSoftCapProject = softCap != null && softCap != 0L
        val projectPage = getProjectResponse.projectPage
        projectPromoVideoLink = projectPage.promoVideo
        companyNameField.set(getProjectResponse.companyName)
        investButtonVisibility.set(checkValidateInvestButton(getProjectResponse))

        firstInvestmentArgumentsField.set(projectPage.firstInvestmentArguments)
        secondInvestmentArgumentsField.set(projectPage.secondInvestmentArguments)
        thirdInvestmentArgumentsField.set(projectPage.thirdInvestmentArguments)
    }

    /**
     * Validates and returns a visibility of an `Invest now` button
     */
    private fun checkValidateInvestButton(project: GetProjectResponse): Int {
        val recentUserLevel = recentUser?.status
        val projectStatus = project.projectStatus
        val projectPageStatus = project.projectPage.projectPageStatus

        return when {
            projectStatus == ACTIVE && projectPageStatus == PUBLISHED -> {
                if (User.LEVEL_2 == recentUserLevel) View.VISIBLE else View.GONE
            }
            projectStatus == FINISHED && projectPageStatus == PUBLISHED -> View.GONE
            projectStatus == COMING_SOON && projectPageStatus == PUBLISHED -> View.VISIBLE
            else -> View.GONE
        }
    }

    private val projectInvestmentInfoResult =
        SingleActionLiveData<Result<ProjectInvestmentInfoResponse>>()

    internal fun getProjectInvestmentInfoResult(): LiveData<Result<ProjectInvestmentInfoResponse>> =
        projectInvestmentInfoResult

    internal fun getProjectInvestmentInfo(projectId: String) {
        viewModelScope.launch {
            val result = asyncLoading {
                investmentModel.run {
                    isInvestmentAllowed(projectId)
                    getProjectInvestmentInfo(projectId)
                }
            }
            projectInvestmentInfoResult.postAction(result)
        }
    }

    private val videoUrlLinkResult = SingleActionLiveData<Result<String>>()
    internal fun getVideoUrlLinkResult(): LiveData<Result<String>> = videoUrlLinkResult

    /**
     *  This method will retrieve a real vimeo url
     *  according to the temporary url received from a backend
     */
    internal fun retrieveVideoLink() {

        viewModelScope.launch {

            val result = asyncLoading {
                vimeoModel.getVimeoLinkOfVideo(720, projectPromoVideoLink)
            }

            videoUrlLinkResult.postAction(result)
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        recentUser = null
        projectInfo = null
        isSoftCapProject = false
        companyNameField.set("")
        projectPromoVideoLink = null
        projectInfo = null
        investButtonVisibility.set(View.GONE)
    }
}
