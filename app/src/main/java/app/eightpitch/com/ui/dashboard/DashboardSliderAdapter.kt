package app.eightpitch.com.ui.dashboard

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class DashboardSliderAdapter(
    anchorFragment: Fragment,
    private val fragments: List<Fragment>
) : FragmentStateAdapter(anchorFragment) {

    override fun createFragment(position: Int) = fragments[position]

    override fun getItemCount() = fragments.size
}