package app.eightpitch.com.ui.authorized.main.test

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.base.preferences.PreferencesReader
import app.eightpitch.com.base.preferences.PreferencesWriter
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.auth.TokenResponse
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.extensions.wrapWithAnotherResult
import app.eightpitch.com.models.AuthModel
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.utils.SingleActionLiveData
import app.eightpitch.com.utils.models.RefreshTokenModel
import kotlinx.coroutines.launch

class TestFragmentViewModel(
    private val appContext: Context,
    private val userModel: UserModel,
    private val authModel: AuthModel,
    private val refreshTokenModel: RefreshTokenModel,
    private val preferencesReader: PreferencesReader,
    private val preferencesWriter: PreferencesWriter,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    private val logoutResult = SingleActionLiveData<Result<Context>>()
    val getLogoutResult: LiveData<Result<Context>> = logoutResult

    internal fun logout() {
        viewModelScope.launch {
            val result = asyncLoading {
                authModel.logout()
            }
            logoutResult.postAction(result.wrapWithAnotherResult(appContext))
        }
    }

    //TODO remove
    private val refreshTokenResult = SingleActionLiveData<Result<TokenResponse>>()
    val getRefreshTokenResult: LiveData<Result<TokenResponse>> = refreshTokenResult

    internal fun refreshToken() {
        viewModelScope.launch {
            val result = asyncLoading {
                refreshTokenModel.refreshToken(preferencesReader.getCachedRefreshToken())
            }
            result.result?.let {
                preferencesWriter.apply {
                    cacheAccessToken(it.accessToken)
                    cacheRefreshToken(it.refreshToken)
                    cacheTokenExpirationTime(1)
                }
            }
            refreshTokenResult.postAction(result)
        }
    }

    //TODO remove
    private val meResult = SingleActionLiveData<Result<User>>()
    val getMeResult: LiveData<Result<User>> = meResult

    internal fun me() {
        viewModelScope.launch {
            val result = asyncLoading {
                userModel.getUserData()
            }
            meResult.postAction(result)
        }
    }
}