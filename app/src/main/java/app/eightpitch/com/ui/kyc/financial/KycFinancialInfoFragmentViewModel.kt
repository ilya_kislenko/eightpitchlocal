package app.eightpitch.com.ui.kyc.financial

import android.content.Context
import androidx.databinding.*
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.webid.WebIdUserRequestBody
import app.eightpitch.com.extensions.*
import app.eightpitch.com.utils.IBANValidator
import app.eightpitch.com.utils.RegexPatterns.PATTERN_OWNER_PREFIX_CORRECT
import app.eightpitch.com.utils.RegexPatterns.PATTERN_VALID_OWNER
import app.eightpitch.com.utils.SingleActionObservableString
import app.eightpitch.com.views.interfaces.ShortFieldValidator
import javax.inject.Inject

class KycFinancialInfoFragmentViewModel @Inject constructor(
    private val appContext: Context,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val title = appContext.getString(R.string.kyc_finantial_title)

    val accountOwnerAvailability = ObservableBoolean(true)
    val accountOwner = ObservableField<String>("")

    private var owner: String = ""
    val ownerErrorHolder = SingleActionObservableString()

    private var IBAN: String = ""
    val IBANErrorHolder = SingleActionObservableString()

    val ownerWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            owner = sequence.toString()
        }
    }

    val ibanWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            IBAN = sequence.toString()
        }
    }

    internal fun addFinancialInfoToKycBody(webIdUserRequestBody: WebIdUserRequestBody): WebIdUserRequestBody =
        webIdUserRequestBody.copy(
            webIdPersonalInfo = webIdUserRequestBody.webIdPersonalInfo.copy(
                accountOwner = owner, IBAN = IBAN
            )
        )

    internal fun validateInput(): Boolean {

        val accountOwnerError =
            owner.validateByDefaultPattern(
                appContext,
                R.string.account_owner,
                PATTERN_VALID_OWNER,
                PATTERN_OWNER_PREFIX_CORRECT
            )
        if (accountOwnerError.isNotEmpty())
            ownerErrorHolder.set(accountOwnerError)

        val IBANError = when {
            IBAN.isEmpty() -> getErrorEmptyString(appContext)
            !IBANValidator.validate(IBAN) -> appContext.getString(R.string.invalid_iban)
            else -> ""
        }
        if (IBANError.isNotEmpty())
            IBANErrorHolder.set(IBANError)

        return accountOwnerError.isEmpty() && IBANError.isEmpty()
    }

    /**
     * Pass an empty name to erase account owner
     */
    internal fun setAccountOwner(fullName: String) {
        accountOwner.set(fullName)
        accountOwnerAvailability.revertValue()
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        owner = ""
        IBAN = ""
        accountOwner.set("")
        accountOwnerAvailability.set(true)
    }
}