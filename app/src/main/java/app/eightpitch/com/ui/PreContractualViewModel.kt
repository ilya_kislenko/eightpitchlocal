package app.eightpitch.com.ui

import android.content.Context
import androidx.databinding.*
import androidx.lifecycle.*
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.investment.*
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.extensions.formatToCurrencyView
import app.eightpitch.com.models.InvestmentModel
import app.eightpitch.com.utils.Constants.DD_p_MM_p_YYYY
import app.eightpitch.com.utils.DateUtils.formatByPattern
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.coroutines.launch

typealias NameToUrl = Pair<String, String>

class PreContractualViewModel(
    appContext: Context,
    private val investmentModel: InvestmentModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val toolbarTitle = appContext.getString(R.string.pre_contractual_info_title)

    val companyNameField = ObservableField("")
    val isinField = ObservableField("")
    val addressField = ObservableField("")
    val selectionTypeField = ObservableField("")
    val financingPurposeField = ObservableField("")
    val nominalValueField = ObservableField("")
    val startDateField = ObservableField("")
    val endDateField = ObservableField("")

    val buttonAvailable = ObservableBoolean()

    internal fun changeAvailability(available: Boolean) {
        buttonAvailable.set(available)
    }

    val files = ObservableArrayList<NameToUrl>()
    internal fun setupFiles(pair: List<NameToUrl>) = files.addAll(pair)

    private val bookInvestmentResult = SingleActionLiveData<Result<BookInvestmentResponse>>()
    internal fun getBookInvestmentResult(): LiveData<Result<BookInvestmentResponse>> =
        bookInvestmentResult

    internal fun bookInvestment(
        projectId: String,
        currencyInputRequestBody: CurrencyInputRequestBody
    ) {
        viewModelScope.launch {
            val result = asyncLoading {
                investmentModel.bookInvestment(projectId, currencyInputRequestBody)
            }
            bookInvestmentResult.postAction(result)
        }
    }

    internal fun setupFields(projectInvestmentInfo: ProjectInvestmentInfoResponse) {
        projectInvestmentInfo.run {
            companyNameField.set(companyName)
            isinField.set(isin)
            addressField.set(companyAddress)
            selectionTypeField.set(typeOfSecurity)
            financingPurposeField.set(financingPurpose)
            nominalValueField.set(nominalValue.formatToCurrencyView())
            startDateField.set(startDate.formatByPattern(pattern = DD_p_MM_p_YYYY))
            endDateField.set(endDate.formatByPattern(pattern = DD_p_MM_p_YYYY))
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        files.clear()
        companyNameField.set("")
        isinField.set("")
        addressField.set("")
        selectionTypeField.set("")
        financingPurposeField.set("")
        nominalValueField.set("")
        startDateField.set("")
        endDateField.set("")
        buttonAvailable.set(false)
    }
}