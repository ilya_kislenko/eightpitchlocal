package app.eightpitch.com.ui.pincode

import android.os.Bundle
import android.view.*
import app.eightpitch.com.R
import app.eightpitch.com.ui.dialogs.DefaultBottomDialog
import kotlinx.android.synthetic.main.dialog_set_up_biometrics.*

class ChoiceBiometricDialog : DefaultBottomDialog() {

    companion object {

        const val USE_PIN_CODE = "USE_PIN_CODE"
        const val USE_FINGER_PRINT = "USE_FINGER_PRINT"
        const val ACTIVATE_LATER = "ACTIVATE_LATER"
    }

    /**
     * Function that will return a String as selected navigation code,
     * it will be one of the [USE_FINGER_PRINT], [USE_PIN_CODE], [ACTIVATE_LATER] codes.
     */
    lateinit var onNavigationCodeSelected: (String) -> Unit

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.dialog_set_up_biometrics, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fingerPrintButton.setDisablingClickListener {
            onButtonClicked(USE_FINGER_PRINT)
        }

        pinCodeButton.setDisablingClickListener {
            onButtonClicked(USE_PIN_CODE)
        }

        activateLaterText.setOnClickListener {
            onButtonClicked(ACTIVATE_LATER)
        }
    }

    private fun onButtonClicked(code: String) {
        if (::onNavigationCodeSelected.isInitialized) {
            onNavigationCodeSelected.invoke(code)
        }
    }
}