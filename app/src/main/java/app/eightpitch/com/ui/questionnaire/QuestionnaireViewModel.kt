package app.eightpitch.com.ui.questionnaire

import android.content.Context
import android.widget.RadioGroup
import androidx.core.view.get
import androidx.core.view.size
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.utils.Empty
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.coroutines.launch

class QuestionnaireViewModel(
    private val appContext: Context,
    private val userModel: UserModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    var toolbarTitle = ObservableField<String>()

    val headerText = ObservableField<String>()

    val firstOptionTitle = ObservableField<String>()
    val secondOptionTitle = ObservableField<String>()
    val thirdOptionTitle = ObservableField<String>()
    val fourthOptionTitle = ObservableField<String>()

    private val observableFields = arrayListOf(
        firstOptionTitle, secondOptionTitle,
        thirdOptionTitle, fourthOptionTitle
    )

    internal fun updateOptionsAvailability(
        firstOptionChecked: Boolean,
        secondOptionChecked: Boolean
    ) {
        nextButtonAvailability.set(firstOptionChecked || secondOptionChecked)
    }

    val nextButtonAvailability = ObservableBoolean()

    private val questionnaireResult = SingleActionLiveData<Result<Empty>>()
    internal fun getQuestionnaireResult(): LiveData<Result<Empty>> = questionnaireResult

    internal fun sendQuestionnaire(questionnaireRequestBody: QuestionnaireRequestBody) {
        viewModelScope.launch {
            val result = asyncLoading {
                userModel.sendQuestionnaire(questionnaireRequestBody)
            }
            questionnaireResult.postAction(result)
        }
    }

    internal fun setupLayout(
        number: Int,
        question: String,
        answerData: List<String>
    ) {
        setToolbar(number)
        setQuestion(question)
        setAnswersData(answerData)
    }

    private fun setToolbar(number: Int) =
        toolbarTitle.set(appContext.getString(R.string.questionnaire_title_pattern, number))

    private fun setQuestion(question: String) = headerText.set(question)

    /**
     * Set variants of answers (until 4 answers) It depends on observableFields and layout
     */
    private fun setAnswersData(answerData: List<String>) {
        for (i in answerData.indices) {
            val observableField = observableFields[i]
            observableField.set(answerData[i])
        }
    }

    /**
     * @param answers backend variants of answers
     * @param answersGroup radioGroup with checked answer
     * Get backend string value of checked answer
     */
    internal fun getCheckedAnswer(
        answers: List<String>,
        answersGroup: RadioGroup
    ): String {
        if (answers.size != answersGroup.size)
            return ""
        for (i in answers.indices) {
            if (answersGroup.checkedRadioButtonId == answersGroup[i].id)
                return answers[i]
        }
        return answers[0]
    }
}