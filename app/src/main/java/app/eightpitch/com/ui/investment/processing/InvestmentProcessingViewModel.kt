package app.eightpitch.com.ui.investment.processing

import androidx.lifecycle.*
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.investment.ProjectInvestmentInfoResponse
import app.eightpitch.com.data.dto.projectdetails.GetProjectResponse
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.data.transmit.ResultStatus.SUCCESS
import app.eightpitch.com.models.InvestmentModel
import app.eightpitch.com.models.ProjectModel
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.coroutines.launch

class InvestmentProcessingViewModel(
    private val investmentModel: InvestmentModel,
    private val projectModel: ProjectModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    private var projectInfo: GetProjectResponse? = null
    var isSoftCapProject: Boolean = false
        private set

    internal fun getProjectInfo() = projectInfo

    private val projectResult = SingleActionLiveData<Result<GetProjectResponse>>()
    internal fun getProjectResult(): LiveData<Result<GetProjectResponse>> =
        projectResult

    /**
     * Method that will retrieve a user and the project
     * to handle visibility of `invest now` button and
     * UI state
     */
    internal fun retrieveData(projectId: String) {

        viewModelScope.launch {

            val result = asyncLoading {
                projectModel.getProjectInfo(projectId)
            }

            if (result.status == SUCCESS) {
                result.result?.let {
                    projectInfo = it
                    setProjectType(it)
                    getProjectInvestmentInfo(projectId)
                }
            }
            projectResult.postAction(result)
        }
    }

    private fun setProjectType(getProjectResponse: GetProjectResponse) {
        val softCap = getProjectResponse.tokenParametersDocument.softCap
        isSoftCapProject = softCap != null && softCap != 0L
    }

    private val projectInvestmentInfoResult =
        SingleActionLiveData<Result<ProjectInvestmentInfoResponse>>()

    internal fun getProjectInvestmentInfoResult(): LiveData<Result<ProjectInvestmentInfoResponse>> =
        projectInvestmentInfoResult

    internal fun getProjectInvestmentInfo(projectId: String) {
        viewModelScope.launch {
            val result = asyncLoading {
                investmentModel.getProjectInvestmentInfo(projectId)
            }
            projectInvestmentInfoResult.postAction(result)
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        projectInfo = null
        isSoftCapProject = false
        projectInfo = null
    }
}