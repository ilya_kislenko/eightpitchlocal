package app.eightpitch.com.ui.dialogs

import android.os.Bundle
import android.text.Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
import android.text.Spannable.SPAN_EXCLUSIVE_INCLUSIVE
import android.text.SpannableStringBuilder
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import app.eightpitch.com.R

class AmountLimitInfoDialog : DefaultBottomDialog() {

    internal var onTextClicked: (() -> Unit)? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        val view = inflater.inflate(R.layout.dialog_account_limit_info, container, false)
        val descriptionTextView = view.findViewById<AppCompatTextView>(R.id.description)
        view.findViewById<AppCompatImageView>(R.id.iconClose).setOnClickListener {
            dismiss()
        }
        val descriptionTextBefore = getString(R.string.dialog_info_limit_text_before_default_color)
        val descriptionText = getString(R.string.dialog_info_limit_text_other_color)
        val descriptionTextAfter = getString(R.string.dialog_info_limit_text_after_default_color)

        val startSpan = descriptionTextBefore.length
        val endSpan = descriptionTextBefore.length + descriptionText.length

        val spannable = SpannableStringBuilder("$descriptionTextBefore$descriptionText$descriptionTextAfter")
        spannable.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(descriptionTextView.context, R.color.lightRed)),
            startSpan, endSpan,
            SPAN_EXCLUSIVE_INCLUSIVE
        )
        spannable.setSpan(AmountLimitClickableSpan(), startSpan, endSpan, SPAN_EXCLUSIVE_EXCLUSIVE)
        descriptionTextView.apply {
            text = spannable
            movementMethod = LinkMovementMethod.getInstance()
        }
        return view
    }

    private inner class AmountLimitClickableSpan : ClickableSpan() {

        override fun onClick(widget: View) {
            onTextClicked?.invoke()
            dismiss()
        }

        override fun updateDrawState(ds: TextPaint) {
            ds.isUnderlineText = false
        }
    }
}