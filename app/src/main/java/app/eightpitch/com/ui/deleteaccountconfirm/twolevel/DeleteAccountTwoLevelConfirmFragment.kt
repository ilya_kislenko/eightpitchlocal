package app.eightpitch.com.ui.deleteaccountconfirm.twolevel

import android.os.*
import android.view.*
import androidx.fragment.app.Fragment
import app.eightpitch.com.R
import app.eightpitch.com.base.rootbackhandlers.ChildNavGraphBackPressureHandler
import app.eightpitch.com.extensions.popBackStack
import app.eightpitch.com.utils.Constants.DELETE_REDIRECT_DURATION
import kotlinx.android.synthetic.main.fragment_delete_account_two_level_confirm.*

class DeleteAccountTwoLevelConfirmFragment : Fragment(), ChildNavGraphBackPressureHandler {

    private val handler = Handler(Looper.getMainLooper())
    private val moveBackRunnable = Runnable {
        moveBack()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View =
        inflater.inflate(R.layout.fragment_delete_account_two_level_confirm, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        crossButton.setOnClickListener {
            moveBack()
        }

        okButton.setOnClickListener {
            moveBack()
        }

        scheduleMoveBack()
    }

    private fun scheduleMoveBack() {
        handler.postDelayed(moveBackRunnable, DELETE_REDIRECT_DURATION)
    }

    private fun moveBack(): Boolean {
        handler.removeCallbacks(moveBackRunnable)
        return popBackStack(R.id.settingsFragment)
    }

    override fun handleBackPressure(): Boolean {
        return moveBack()
    }
}