package app.eightpitch.com.ui.recycleradapters

/**
 * Should be used from data binding adapter
 */
interface BindableAdapter<T : Any> {
    fun setData(data: MutableList<T>)
}