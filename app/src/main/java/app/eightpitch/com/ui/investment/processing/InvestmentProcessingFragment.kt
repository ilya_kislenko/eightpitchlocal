package app.eightpitch.com.ui.investment.processing

import android.os.*
import android.view.*
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.investment.ProjectInvestmentInfoResponse
import app.eightpitch.com.data.dto.project.ProjectInvestment.Companion.BOOKED
import app.eightpitch.com.data.dto.project.ProjectInvestment.Companion.CONFIRMED
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.investment.unprocessed.UnfinishedInvestmentFragment
import app.eightpitch.com.ui.payment.hardcappaymentselection.HardCapPaymentSelectionFragment
import app.eightpitch.com.ui.payment.softcappaymentselection.SoftCapPaymentSelectionFragment
import kotlinx.android.synthetic.main.fragment_investment_processing.*
import java.math.BigDecimal

class InvestmentProcessingFragment : BaseFragment<InvestmentProcessingViewModel>() {

    companion object {

        internal const val PROJECT_ID = "PROJECT_ID"
        private const val UPDATE_TICK_MILLIS = 3000L

        fun buildWithAnArguments(
            projectId: String
        ) = Bundle().apply {
            putString(PROJECT_ID, projectId)
        }
    }

    private val projectId: String
        get() = arguments?.getString(PROJECT_ID, "") ?: ""

    override fun getLayoutID() = R.layout.fragment_investment_processing

    override fun getVMClass() = InvestmentProcessingViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupFlow()

        crossButton.setOnClickListener {
            popBackStack()
        }

        okButton.setOnClickListener {
            popBackStack()
        }
    }

    private fun setupFlow() {
        if (projectId.isNotEmpty()) {
            waitAndRequestStatus()
        }
    }

    private fun waitAndRequestStatus() {
        blockUI()
        viewModel.getProjectResult().observe(viewLifecycleOwner, {
            handleResult(it) {}
        })
        viewModel.getProjectInvestmentInfoResult().observe(viewLifecycleOwner, { result ->
            handleResult(result) { projectInvestmentInfo ->
                when (projectInvestmentInfo.unprocessedInvestment.investmentStatus) {
                    CONFIRMED -> {
                        goToPayment(projectInvestmentInfo)
                    }
                    BOOKED -> {
                        val arguments =
                            UnfinishedInvestmentFragment.buildWithAnArguments(projectId, projectInvestmentInfo)
                        navigate(R.id.action_investmentProcessingFragment_to_unFinishedInvestmentFragment, arguments)
                    }
                    else -> {
                        scheduleUpdate()
                    }
                }
            }
        })
        viewModel.retrieveData(projectId)
    }

    private fun goToPayment(projectInvestmentInfo: ProjectInvestmentInfoResponse) {
        unblockUI()
        val investmentId = projectInvestmentInfo.unprocessedInvestment.investmentId
        val fee =
            viewModel.getProjectInfo()?.tokenParametersDocument?.assetBasedFees.orDefaultValue(
                BigDecimal(0)
            )
        if (viewModel.isSoftCapProject) {
            navigate(
                R.id.action_investmentProcessingFragment_to_softCapPaymentSelectionFragment,
                SoftCapPaymentSelectionFragment.buildWithAnArguments(
                    investmentId,
                    projectId,
                    projectInvestmentInfo.copy(assetBasedFees = fee).also {
                        it.setProjectName(viewModel.getProjectInfo()?.projectPage?.companyName)
                    }
                )
            )
        } else {
            navigate(
                R.id.action_investmentProcessingFragment_to_hardCapPaymentSelectionFragment,
                HardCapPaymentSelectionFragment.buildWithAnArguments(
                    investmentId,
                    projectId,
                    projectInvestmentInfo.copy(assetBasedFees = fee).also {
                        it.setProjectName(viewModel.getProjectInfo()?.projectPage?.companyName)
                    }
                )
            )
        }
    }

    /**
     * Delayed request is needed for trying to get a new status for
     * investment. Because the timing of status changing isn't constant.
     */
    private fun scheduleUpdate() {
        Handler(Looper.getMainLooper()).postDelayed({
            viewModel.getProjectInvestmentInfo(projectId)
        }, UPDATE_TICK_MILLIS)
    }
}