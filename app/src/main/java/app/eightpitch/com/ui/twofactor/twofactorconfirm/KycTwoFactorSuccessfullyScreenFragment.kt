package app.eightpitch.com.ui.twofactor.twofactorconfirm

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import app.eightpitch.com.R
import app.eightpitch.com.base.BackPressureFragment
import app.eightpitch.com.extensions.finishActivity
import app.eightpitch.com.extensions.navigate
import app.eightpitch.com.ui.twofactor.choosetwoauthmethod.ChooseTwoAuthMethodFragment.Companion.NAVIGATE_FROM_KEY
import app.eightpitch.com.ui.twofactor.choosetwoauthmethod.ChooseTwoAuthMethodFragment.Companion.NAVIGATE_FROM_KYC
import app.eightpitch.com.ui.twofactor.choosetwoauthmethod.ChooseTwoAuthMethodFragment.Companion.NAVIGATE_FROM_SECURITY
import kotlinx.android.synthetic.main.fragment_kyc_two_factor_successfully.*
import java.util.*
import java.util.Locale.GERMAN
import java.util.Locale.GERMANY

class KycTwoFactorSuccessfullyScreenFragment : BackPressureFragment() {

    companion object {
        const val STATUS_ENABLED = "enabled"
        const val STATUS_DISABLED = "disabled"
        const val GA_STATUS_KEY = "GA_STATUS_KEY"

        fun buildWithAnArguments(
            twoAuthNavigateFrom: String = "",
            gaStatusKey: String
        ) = Bundle().apply {
            putString(GA_STATUS_KEY, gaStatusKey)
            putString(NAVIGATE_FROM_KEY, twoAuthNavigateFrom)
        }
    }

    private val navigateFrom: String
        get() = arguments?.getString(NAVIGATE_FROM_KEY) ?: ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_kyc_two_factor_successfully, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val defaultLocale = Locale.getDefault()
        val isLocaleGerman = defaultLocale == GERMANY || defaultLocale == GERMAN
        val statusText = when (val status = arguments?.getString(GA_STATUS_KEY) ?: "") {
            STATUS_ENABLED -> {
                if (isLocaleGerman) getString(R.string.enabled) else status
            }
            else -> {
                if (isLocaleGerman) getString(R.string.disabled) else status
            }
        }

        headerTextView.text = String.format(
            defaultLocale,
            getString(R.string.two_factor_authentication_nis_now_enabled), statusText)

        when (navigateFrom) {
            NAVIGATE_FROM_KYC -> {
                nextToKycButton.visibility = VISIBLE
                subHeaderTextView.visibility = VISIBLE
            }
            NAVIGATE_FROM_SECURITY -> {
                nextToKycButton.visibility = GONE
                subHeaderTextView.visibility = GONE
            }
        }

        crossButton.setOnClickListener {
            when (navigateFrom) {
                NAVIGATE_FROM_KYC -> {
                    finishActivity()
                    nextToKycButton.visibility = VISIBLE
                    subHeaderTextView.visibility = VISIBLE
                }
                NAVIGATE_FROM_SECURITY -> {
                    nextToKycButton.visibility = GONE
                    subHeaderTextView.visibility = GONE
                    popBackStack(R.id.chooseTwoAuthMethodFragment)
                }
            }
        }

        if (NAVIGATE_FROM_KYC == navigateFrom) {
            registerOnBackPressedListener {
                finishActivity()
            }
        }

        nextToKycButton.setOnClickListener {
            navigate(R.id.action_kycTwoFactorSuccessfullyScreenFragment_to_kycPersonalInfoFragment)
        }
    }
}