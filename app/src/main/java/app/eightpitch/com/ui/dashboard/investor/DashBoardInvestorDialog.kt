package app.eightpitch.com.ui.dashboard.investor

import android.os.Bundle
import android.view.*
import app.eightpitch.com.R
import app.eightpitch.com.ui.dialogs.DefaultBottomDialog

class DashBoardInvestorDialog : DefaultBottomDialog() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.dialog_investor_info_dashboard, container, false)
}