package app.eightpitch.com.ui.questionnaire

import android.os.Bundle
import android.view.*
import app.eightpitch.com.R
import app.eightpitch.com.base.BackPressureFragment
import app.eightpitch.com.extensions.moveBackward
import kotlinx.android.synthetic.main.fragment_questionnaire_finish_screen.*

class QuestionnaireFinishFragment : BackPressureFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_questionnaire_finish_screen, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        registerOnBackPressedListener { moveBackward() }
        crossButton.setOnClickListener { moveBackward() }
        nextButton.setOnClickListener { moveBackward() }
    }
}