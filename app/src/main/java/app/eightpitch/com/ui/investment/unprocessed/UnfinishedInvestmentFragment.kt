package app.eightpitch.com.ui.investment.unprocessed

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.investment.ProjectInvestmentInfoResponse
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.investment.choosedocuments.ProjectDocumentsFragment
import app.eightpitch.com.ui.projectsdetail.ProjectDetailsFragment
import kotlinx.android.synthetic.main.fragment_unfinished_investment.*

class UnfinishedInvestmentFragment : BaseFragment<UnfinishedInvestmentViewModel>() {

    companion object {

        private const val PROJECT_ID = "PROJECT_ID"
        private const val TOKEN_INFO_KEY_CODE = "TOKEN_INFO_KEY_CODE"

        fun buildWithAnArguments(
            projectId: String,
            projectInvestmentInfo: ProjectInvestmentInfoResponse
        ) = Bundle().apply {
            putString(PROJECT_ID, projectId)
            putParcelable(TOKEN_INFO_KEY_CODE, projectInvestmentInfo)
        }
    }

    private val projectId: String
        get() = arguments?.getString(ProjectDetailsFragment.PROJECT_ID, "") ?: ""

    private val projectInvestmentInfo: ProjectInvestmentInfoResponse
        get() = arguments?.getParcelable(TOKEN_INFO_KEY_CODE) ?: ProjectInvestmentInfoResponse()
    private val unprocessedInvestmentId
        get() = projectInvestmentInfo.unprocessedInvestment.investmentId

    override fun getLayoutID() = R.layout.fragment_unfinished_investment

    override fun getVMClass() = UnfinishedInvestmentViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        crossButton.setOnClickListener {
            popBackStack()
        }

        setupMessage()

        viewModel.getCancelResult().observe(viewLifecycleOwner, Observer { result ->
            handleResult(result) {
                popBackStack()
            }
        })

        viewModel.getProjectResult().observe(viewLifecycleOwner, Observer {
            handleResult(it) { projectResponse ->
                val arguments = ProjectDocumentsFragment.buildWithAnArguments(
                    projectId,
                    unprocessedInvestmentId,
                    projectInvestmentInfo,
                    projectResponse.projectFiles.filterDraftDocuments()
                )
                navigate(
                    R.id.action_unFinishedInvestmentFragment_to_projectDocumentsFragment,
                    arguments
                )
            }
        })

        finaliseButton.setOnClickListener {
            viewModel.getProjectInfo(projectId)

        }

        cancelButton.setOnClickListener {
            viewModel.cancelProjectInvestment(
                projectId,
                unprocessedInvestmentId
            )
        }
    }

    private fun setupMessage() {
        val amount = projectInvestmentInfo.unprocessedInvestment.amount
        val (tokens, euro) =
            amount.toString() to amount.multiplySafe(projectInvestmentInfo.nominalValue).toString()
        val tokenName = projectInvestmentInfo.shortCut
        subHeaderTextView.text =
            getString(R.string.you_did_not_finish_your_previous_attempt_to_invest_pattern, tokens, tokenName, euro)
    }
}