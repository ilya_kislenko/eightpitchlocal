package app.eightpitch.com.ui.kyc.country

import android.content.Context
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.user.Country
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.data.dto.webid.WebIdUserRequestBody
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.extensions.getErrorEmptyString
import app.eightpitch.com.extensions.getSortedCountries
import app.eightpitch.com.extensions.validateByDefaultPattern
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.utils.RegexPatterns.PATTERN_VALID_CITY
import app.eightpitch.com.utils.RegexPatterns.PATTERN_VALID_CODE
import app.eightpitch.com.utils.SingleActionLiveData
import app.eightpitch.com.utils.SingleActionObservableString
import app.eightpitch.com.views.interfaces.ShortFieldValidator
import kotlinx.coroutines.launch
import javax.inject.Inject

class KycCountryInfoFragmentViewModel @Inject constructor(
    private val appContext: Context,
    private val userModel: UserModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val title = appContext.getString(R.string.kyc_country_title)

    val cityField = ObservableField("")
    val zipCodeField = ObservableField("")

    private var city: String = ""
    val cityErrorHolder = SingleActionObservableString()

    private var zipCode: String = ""
    val zipCodeErrorHolder = SingleActionObservableString()

    private var country = ""
    val countryErrorHolder = SingleActionObservableString()

    val cityWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            city = sequence.toString()
        }
    }

    val zipCodeWatcher = object : ShortFieldValidator() {
        override fun onTextChanged(sequence: CharSequence) {
            zipCode = sequence.toString()
        }
    }

    fun addCountryInfoToKycBody(webIdUserRequestBody: WebIdUserRequestBody): WebIdUserRequestBody =
        webIdUserRequestBody.copy(
            address = webIdUserRequestBody.address.copy(
                country = Country.fromDisplayValue(appContext, country).toString(),
                city = city,
                zipCode = zipCode
            )
        )

    fun validateInput(): Boolean {

        val cityError =
            city.validateByDefaultPattern(appContext, R.string.city, PATTERN_VALID_CITY)
        if (cityError.isNotEmpty())
            cityErrorHolder.set(cityError)

        val zipCodeError =
            zipCode.validateByDefaultPattern(appContext, R.string.zip_code, PATTERN_VALID_CODE, "")
        if (zipCodeError.isNotEmpty())
            zipCodeErrorHolder.set(zipCodeError)

        val countryError = when {
            country.isEmpty() -> getErrorEmptyString(appContext)
            else -> ""
        }
        if (countryError.isNotEmpty())
            countryErrorHolder.set(countryError)

        return countryError.isEmpty() && cityError.isEmpty() && zipCodeError.isEmpty()
    }

    internal fun setCountry(position: Int) {
        country = appContext.getSortedCountries().getOrElse(position) { "" }
    }

    //TODO may be remove
    internal fun getUserCountryArrayIndex(user: User): Int {
        val index = appContext.getSortedCountries().indexOf(user.userAddress.country)
        return if (index < 0) 0 else index
    }

    private val userMeResult = SingleActionLiveData<Result<User>>()
    internal fun getUserMeResult(): LiveData<Result<User>> = userMeResult

    private fun getUser() {
        viewModelScope.launch {
            val result = asyncLoading {
                userModel.getUserData()
            }
            userMeResult.postAction(result)
        }
    }

    internal fun loadUserOrRestoreState() {
        if (city.isEmpty())
            getUser()
        else {
            cityField.set(city)
            zipCodeField.set(zipCode)
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        city = ""
        zipCode = ""
        cityField.set("")
        zipCodeField.set("")
    }
}