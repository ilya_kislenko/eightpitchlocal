package app.eightpitch.com.ui.profilepayments.adapters

import android.view.*
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.R
import app.eightpitch.com.data.dto.profiletransfers.TransferDetails
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.recycleradapters.BindableRecyclerAdapter
import java.util.*

class TransferAdapter :
    BindableRecyclerAdapter<TransferDetails, TransferAdapter.PagedTransferViewHolder>() {

    /**
     * Listener that represents a click on any payment in the list,
     * will pass a [PaymentItem] when click is performed
     */
    internal lateinit var onTransferClicked: (TransferDetails) -> (Unit)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PagedTransferViewHolder(
        inflateView(
            parent,
            R.layout.item_view_transfer
        )
    )

    override fun onBindViewHolder(holder: PagedTransferViewHolder, position: Int) {
        val transferItem = getDataItem(position)
        holder.apply {
            transferDateTextView.text = transferItem.formattedDate
            transferAmountInTokensTextView.text = String.format(
                Locale.getDefault(),
                transferAmountInTokensTextView.context.getString(R.string.token_amount_pattern),
                transferItem.amount.formatToCurrencyView(),
                transferItem.shortCut
            )
            parentView.apply {
                tag = position
                setOnClickListener {
                    val tagPos = it.tag as Int
                    onTransferClicked.invoke(getDataItem(tagPos))
                }
            }
        }
    }

    class PagedTransferViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val transferDateTextView: AppCompatTextView =
            itemView.findViewById(R.id.transferDate)
        val transferAmountInTokensTextView: AppCompatTextView =
            itemView.findViewById(R.id.transferAmountInTokensTextView)
        val parentView: ConstraintLayout = itemView.findViewById(R.id.parentView)
    }
}
