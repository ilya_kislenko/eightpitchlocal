package app.eightpitch.com.ui.payment.paymentconfirmation

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import app.eightpitch.com.R
import app.eightpitch.com.base.rootbackhandlers.ChildNavGraphBackPressureHandler
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.authorized.BottomTabsHolder
import kotlinx.android.synthetic.main.fragment_payment_confirmed.*
import java.util.*

class PaymentConfirmedFragment : Fragment(), ChildNavGraphBackPressureHandler {

    companion object {

        private const val PROJECT_NAME = "PROJECT_NAME"

        fun buildWithAnArguments(
            projectName: String
        ) = Bundle().apply {
            putString(PROJECT_NAME, projectName)
        }
    }

    private val projectName: String
        get() = arguments?.getString(PROJECT_NAME) ?: ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View =
        inflater.inflate(R.layout.fragment_payment_confirmed, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        subHeaderTextView.text = String.format(
            Locale.getDefault(),
            requireActivity().getString(R.string.you_successfully_invested),
            projectName
        )

        crossButton.setOnClickListener {
            closeAndForward()
        }

        goDashboardButton.setOnClickListener {
            closeAndForward()
        }
    }

    override fun handleBackPressure(): Boolean {
        return closeAndForward()
    }

    private fun closeAndForward(): Boolean {
        val popResult = popBackStack(R.id.projectsFragment)
        goToDashboard()
        return popResult
    }

    private fun goToDashboard() {
        getTarget(BottomTabsHolder::class.java)?.switchTabTo(1)
    }
}