package app.eightpitch.com.ui.questionnaire

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseFragment
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody.CREATOR.PROFESSION
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody.CREATOR.PROFESSION_STUDIES
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody.CREATOR.STUDIES
import app.eightpitch.com.extensions.handleResult
import app.eightpitch.com.ui.questionnaire.QuestionnaireFirstFragment.Companion.QUESTIONNAIRE_KEY_CODE
import kotlinx.android.synthetic.main.fragment_questionnaire_prof_experience.*

class QuestionnaireTenthFragment : BaseFragment<QuestionnaireViewModel>() {

    companion object {
        fun buildWithAnArguments(
            questionnaireRequestBody: QuestionnaireRequestBody
        ) = Bundle().apply {
            putParcelable(QUESTIONNAIRE_KEY_CODE, questionnaireRequestBody)
        }
    }

    private val questionnaireRequestBody: QuestionnaireRequestBody
        get() = arguments?.getParcelable(QUESTIONNAIRE_KEY_CODE) ?: QuestionnaireRequestBody()

    override fun getLayoutID() = R.layout.fragment_questionnaire_prof_experience

    override fun getVMClass() = QuestionnaireViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindLoadingIndicator()

        viewModel.setupLayout(
            11,
            getString(R.string.how_did_you_get_this_experience_text),
            arrayListOf(
                getString(R.string.relevant_profession_in_banking_or_insurance_text),
                getString(R.string.relevant_studies_or_professional_training_text)
            )
        )

        backButton.setOnClickListener {
            popBackStack()
        }

        viewModel.getQuestionnaireResult().observe(viewLifecycleOwner, Observer {
            handleResult(it) {
                navigate(R.id.action_questionnaireTenthFragment_to_questionnaireFinishFragment)
            }
        })

        firstOptionTitle.setOnCheckedChangeListener { _, isChecked ->
            viewModel.updateOptionsAvailability(isChecked, secondOptionTitle.isChecked)
        }

        secondOptionTitle.setOnCheckedChangeListener { _, isChecked ->
            viewModel.updateOptionsAvailability(firstOptionTitle.isChecked, isChecked)
        }

        nextButton.setOnClickListener {
            val answer = getSelectedAnswer()
            viewModel.sendQuestionnaire(questionnaireRequestBody.copy(professionalExperienceSource = answer))
        }
    }

    private fun getSelectedAnswer(): String {
        val firstOptionSelected = firstOptionTitle.isChecked
        val secondOptionSelected = secondOptionTitle.isChecked
        return when {
            firstOptionSelected && secondOptionSelected -> PROFESSION_STUDIES
            firstOptionSelected -> PROFESSION
            else -> STUDIES
        }
    }
}