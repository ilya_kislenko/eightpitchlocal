package app.eightpitch.com.ui.authorized.main.homepager

import androidx.lifecycle.*
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.user.User
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.models.ProjectModel
import app.eightpitch.com.models.UserModel
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.coroutines.launch

class DashboardRootViewModel(
    private val userModel: UserModel,
    private val projectModel: ProjectModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    /**
     * Result that represent a User and if he has an project-related experience state.
     */
    private val dataResult = SingleActionLiveData<Result<Pair<User, Boolean>>>()
    internal fun getDataResult(): LiveData<Result<Pair<User, Boolean>>> = dataResult

    internal fun retrieveData() {
        viewModelScope.launch {
            val result = asyncLoading {
                val user = userModel.getUserData()
                val hasExperience = if (user.isInvestor)
                    projectModel.getInvestorGraph().investments.isNotEmpty()
                else
                    projectModel.getInitiatorProjects().projects.isNotEmpty()
                user to hasExperience
            }
            dataResult.postAction(result)
        }
    }
}