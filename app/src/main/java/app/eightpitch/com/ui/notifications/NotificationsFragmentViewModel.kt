package app.eightpitch.com.ui.notifications

import android.content.Context
import android.view.inspector.PropertyReader
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.notifications.Notification
import app.eightpitch.com.data.dto.notifications.Notification.Companion.NEW
import app.eightpitch.com.data.dto.notifications.Notification.Companion.NotificationType
import app.eightpitch.com.data.dto.notifications.Notification.Companion.notificationsList
import app.eightpitch.com.data.dto.notifications.NotificationsMapper
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.data.transmit.ResultStatus.SUCCESS
import app.eightpitch.com.extensions.wrapWithAnEmptyResult
import app.eightpitch.com.models.NotificationsModel
import app.eightpitch.com.utils.Empty
import app.eightpitch.com.utils.SingleActionLiveData
import kotlinx.coroutines.launch
import kotlin.reflect.typeOf

class NotificationsFragmentViewModel(
    private val appContext: Context,
    private val notificationsModel: NotificationsModel,
    private val notificationsMapper: NotificationsMapper,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val title = appContext.getString(R.string.be_up_to_date_text)

    val emptyMessage = ObservableBoolean(false)
    val notifications = ObservableArrayList<Notification>()

    internal fun getNotificationsMapper() = notificationsMapper

    private val notificationsResult = SingleActionLiveData<Result<Empty>>()
    internal fun getNotificationsResult(): LiveData<Result<Empty>> = notificationsResult

    private val notificationsStatusResult = SingleActionLiveData<Result<Boolean>>()
    internal fun getNotificationsStatusResult(): LiveData<Result<Boolean>> = notificationsStatusResult

    internal fun getNotifications() {

        viewModelScope.launch {

            val result = asyncLoading {
                notificationsModel.retrieveNotifications()
            }

            if (result.status == SUCCESS) {
                val resultSet = result.result!!.toMutableList()
                val filteredSet =  resultSet.filter { notificationsList.contains(it.notificationType) }.toMutableList()
                filteredSet.sort()
                notifications.apply {
                    clear()
                    addAll(filteredSet)
                }
                emptyMessage.set(filteredSet.isEmpty())
                notificationsStatusResult.postAction(Result(SUCCESS,
                    notifications.find { it.notificationStatus == NEW } != null))
            }

            notificationsResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    private val markingNotificationResult = SingleActionLiveData<Result<Empty>>()
    internal fun getMarkingNotificationResult(): LiveData<Result<Empty>> = markingNotificationResult

    internal fun markNotificationAsReadBy(notificationId: Int) {

        viewModelScope.launch {

            val result = asyncLoading {
                notificationsModel.markNotificationRead(notificationId)
            }

            markingNotificationResult.postAction(result.wrapWithAnEmptyResult())
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        notifications.clear()
        emptyMessage.set(false)
    }
}