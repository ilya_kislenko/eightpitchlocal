package app.eightpitch.com.ui.dynamicdetails

import android.view.*
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.R
import app.eightpitch.com.data.dto.dynamicviews.*
import app.eightpitch.com.extensions.inflateView

class DepartmentAdapter(private val items: List<DepartmentItem>) :
    RecyclerView.Adapter<DepartmentAdapter.DepartmentHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DepartmentHolder {
        return DepartmentHolder(inflateView(parent, R.layout.item_view_department))
    }

    override fun onBindViewHolder(holder: DepartmentHolder, position: Int) {
        val department = items[position]
        holder.apply {
            profileImage.tag = ImageUIElementArguments("", department.imageUrl)
            personPosition.text = department.position
            header.text = department.fullName
            linkedIn.tag = ClickableUIElementArguments("", department.linkedinUrl)
        }
    }

    override fun getItemCount() = items.size

    class DepartmentHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val profileImage: AppCompatImageView = itemView.findViewById(R.id.profileImage)
        val personPosition: AppCompatTextView = itemView.findViewById(R.id.positionTextView)
        val header: AppCompatTextView = itemView.findViewById(R.id.headerTextView)
        val linkedIn: AppCompatTextView = itemView.findViewById(R.id.linkedInView)
    }
}