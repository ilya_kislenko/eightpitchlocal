package app.eightpitch.com.ui.investment.directdownloading

import android.content.Context
import androidx.databinding.*
import androidx.lifecycle.*
import app.eightpitch.com.BuildConfig
import app.eightpitch.com.R
import app.eightpitch.com.base.BaseViewModel
import app.eightpitch.com.data.ThreadsSeparator
import app.eightpitch.com.data.dto.projectdetails.ProjectsFile
import app.eightpitch.com.data.dto.sms.SendTokenLimitResponse
import app.eightpitch.com.data.transmit.Result
import app.eightpitch.com.data.transmit.ResultStatus
import app.eightpitch.com.extensions.wrapWithAnEmptyResult
import app.eightpitch.com.models.*
import app.eightpitch.com.utils.*
import kotlinx.coroutines.launch

class DirectDownloadingViewModel(
    appContext: Context,
    private val projectModel: ProjectModel,
    private val downloadModel: DownloadModel,
    private val smsModel: SMSModel,
    threadsSeparator: ThreadsSeparator
) : BaseViewModel(threadsSeparator) {

    val toolbarTitle = appContext.getString(R.string.investment_documents_title_text)

    val buttonAvailable = ObservableBoolean()
    internal fun changeAvailability(available: Boolean) {
        buttonAvailable.set(available)
    }

    val files = ObservableArrayList<String>()
    internal fun setupFiles(list: List<ProjectsFile>) {
        val mappedList = list.map { "• ${it.projectsFile.originalFileName}" }
        files.addAll(mappedList)
    }

    private fun downloadDocument(url: String, originalName: String) {
        downloadModel.downloadDocuments(
            "${BuildConfig.ENDPOINT}project-service/files/$url",
            originalName
        )
    }

    private val requestResult = SingleActionLiveData<Result<SendTokenLimitResponse>>()
    internal fun getRequestResult(): LiveData<Result<SendTokenLimitResponse>> = requestResult

    internal fun requestInvestmentConfirmation(
        investmentId: String,
        projectId: String
    ) {
        viewModelScope.launch {
            val result = asyncLoading {
                smsModel.requestInvestmentConfirmation(investmentId, projectId)
            }
            requestResult.postAction(result)
        }
    }

    private val projectsSubscription = SingleActionLiveData<Result<Empty>>()
    internal fun getProjectsSubscription(): LiveData<Result<Empty>> = projectsSubscription

    internal fun getProject(projectId: String) {
        viewModelScope.launch {
            val result = asyncLoading {
                projectModel.getProjectInfo(projectId)
            }
            if (ResultStatus.SUCCESS == result.status) {
                val listFiles = result.result!!.projectFiles
                listFiles.map { it.projectsFile }.forEach { projectFileDetails ->
                    downloadDocument(
                        projectFileDetails.uniqueFileName,
                        projectFileDetails.originalFileName
                    )
                }
            }
            projectsSubscription.postAction(result.wrapWithAnEmptyResult())
        }
    }

    override fun cleanUpFinally() {
        super.cleanUpFinally()
        files.clear()
        buttonAvailable.set(false)
    }
}