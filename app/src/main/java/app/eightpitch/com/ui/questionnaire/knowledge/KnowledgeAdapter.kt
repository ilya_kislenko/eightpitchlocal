package app.eightpitch.com.ui.questionnaire.knowledge

import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import app.eightpitch.com.R
import app.eightpitch.com.data.dto.questionnaire.QuestionnaireRequestBody.Knowledge
import app.eightpitch.com.extensions.inflateView
import app.eightpitch.com.ui.recycleradapters.BindableRecyclerAdapter
import app.eightpitch.com.views.DefaultToggleButton

class KnowledgeAdapter(private val checkedKnowledges: List<String>) :
    BindableRecyclerAdapter<String, KnowledgeAdapter.KnowledgeViewHolder>() {

    /**
     * Callback that represents a String as backend knowledge name
     * and Boolean as state (add or delete)
     */
    internal lateinit var answersCallback: (String, Boolean) -> Unit

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        KnowledgeViewHolder(
            inflateView(
                parent,
                R.layout.item_view_questionnaire_knowledge
            )
        )

    override fun onBindViewHolder(holder: KnowledgeViewHolder, position: Int) {
        val knowledgeName = dataList[position]
        holder.knowledgeOptionTitle.text = knowledgeName
        holder.knowledgeToggleButton.apply {
            if(checkedKnowledges.isNotEmpty() &&
               checkedKnowledges.find { Knowledge.toDisplayValue(context, it) == knowledgeName } != null)
                changeSelectionState(true)
            setOnSelectionChangedListener {
                if (::answersCallback.isInitialized) {
                    answersCallback.invoke(
                        Knowledge.fromDisplayValue(context, knowledgeName).toString(), it
                    )
                }
            }
        }
    }

    class KnowledgeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val knowledgeToggleButton: DefaultToggleButton = itemView.findViewById(R.id.toggleButton)
        val knowledgeOptionTitle: AppCompatTextView = itemView.findViewById(R.id.knowledgeTextView)
    }
}