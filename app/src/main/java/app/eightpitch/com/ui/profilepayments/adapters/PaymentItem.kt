package app.eightpitch.com.ui.profilepayments.adapters

import android.os.*
import app.eightpitch.com.extensions.readStringSafe
import app.eightpitch.com.utils.Constants.DD_DOT_MM_DOT_YYYY_HH_MM_SS
import app.eightpitch.com.utils.Constants.DEFAULT_DATA_PATTERN
import app.eightpitch.com.utils.DateUtils.formatByPattern
import app.eightpitch.com.utils.DateUtils.parseByFormat
import java.math.BigDecimal

class PaymentItem(
    var amount: BigDecimal = BigDecimal(-1),
    var companyAddress: String = "",
    var companyName: String = "",
    var isin: String = "",
    var monetaryAmount: BigDecimal = BigDecimal(-1),
    var paymentSystem: String = "",
    var shortCut: String = "",
    var transactionDate: String = "",
    var transactionId: String = ""
) : Parcelable {

    val formattedDate
        get() = transactionDate.parseByFormat(pattern = DEFAULT_DATA_PATTERN)
            .formatByPattern(pattern = DD_DOT_MM_DOT_YYYY_HH_MM_SS)

    constructor(parcel: Parcel) : this(
        BigDecimal(parcel.readLong()),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        BigDecimal(parcel.readLong()),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe(),
        parcel.readStringSafe()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(amount.longValueExact())
        parcel.writeString(companyAddress)
        parcel.writeString(companyName)
        parcel.writeString(isin)
        parcel.writeLong(monetaryAmount.longValueExact())
        parcel.writeString(paymentSystem)
        parcel.writeString(shortCut)
        parcel.writeString(transactionDate)
        parcel.writeString(transactionId)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<PaymentItem> {
        override fun createFromParcel(parcel: Parcel): PaymentItem {
            return PaymentItem(parcel)
        }

        override fun newArray(size: Int): Array<PaymentItem?> {
            return arrayOfNulls(size)
        }
    }
}
