package app.eightpitch.com.ui.webview

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.*
import android.webkit.*
import androidx.fragment.app.Fragment
import app.eightpitch.com.R
import app.eightpitch.com.extensions.*
import app.eightpitch.com.ui.authorized.AuthorizedActivity
import app.eightpitch.com.ui.payment.paymentconfirmation.PaymentConfirmedFragment
import app.eightpitch.com.utils.IntentsController
import kotlinx.android.synthetic.main.fragment_web_view_default.*
import kotlinx.android.synthetic.main.loading_progress_indicator.*

class DefaultWebViewFragment : Fragment() {

    companion object {

        const val STRING_URL_ARG = "STRING_URL_ARG"
        const val PROJECT_NAME = "PROJECT_NAME"
        const val FINISH_ACTIVITY = "FINISH_ACTIVITY"

        fun buildWithAnArguments(
            stringLinkUrl: String,
            projectName: String = "",
            withBackPressure: Boolean = false
        ): Bundle = Bundle().apply {
            putString(STRING_URL_ARG, stringLinkUrl)
            putString(PROJECT_NAME, projectName)
            putBoolean(FINISH_ACTIVITY, withBackPressure)
        }
    }

    private val withBackPressure: Boolean
        get() = arguments?.getBoolean(FINISH_ACTIVITY) ?: false

    private val projectName: String
        get() = arguments?.getString(PROJECT_NAME, "").orDefaultValue("")

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_web_view_default, container, false)

    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        backButton.setOnClickListener {
            if (withBackPressure)
                IntentsController.startActivityWithANewStack(view.context, AuthorizedActivity::class.java)
            else
                popBackStack()
        }

        webView.apply {
            settings.apply {
                javaScriptEnabled = true
                domStorageEnabled = true
            }
            webViewClient = DefaultWebViewClient()

            val linkUrl = arguments?.getString(STRING_URL_ARG) ?: ""

            loadUrl(linkUrl)
        }
    }

    private inner class DefaultWebViewClient : WebViewClient() {
        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            curtainView?.hide()
        }

        //TODO set this listener only in the case of Payments flow
        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            val string = request?.url?.lastPathSegment.orDefaultValue("")
            return when {
                string.contains("confirm") -> {
                    navigate(R.id.action_defaultWebViewFragment_to_paymentConfirmedFragment,
                        PaymentConfirmedFragment.buildWithAnArguments(projectName))
                    true
                }
                string.contains("abort") -> {
                    navigate(R.id.action_defaultWebViewFragment_to_paymentDeclinedFragment)
                    true
                }
                else -> {
                    false
                }
            }
        }
    }
}