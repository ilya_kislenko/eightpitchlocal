package app.eightpitch.com.views.interfaces

import androidx.annotation.StringRes

interface TextField {

    fun setText(text: CharSequence)
    fun setText(@StringRes stringResID: Int)
    fun getText(): CharSequence
}