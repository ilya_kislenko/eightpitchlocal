package app.eightpitch.com.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.widget.FrameLayout
import androidx.annotation.ColorInt
import app.eightpitch.com.R

class CircleProgressLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) :
    FrameLayout(context, attrs, defStyleAttr) {
    private var percentage = 0f
    private var circleLineWidth = 0f
    private var progressLineWidth = 0f
    private var minViewSize = 0
    private var circleColor = 0
    private var progressColor = 0
    private val startAngle = 90
    private fun parseAttributes(context: Context, attrs: AttributeSet?) {
        val array = context.obtainStyledAttributes(attrs, R.styleable.CircleProgressLayout)
        circleLineWidth = array.getDimension(R.styleable.CircleProgressLayout_circleWidth, 0f)
        progressLineWidth = array.getDimension(R.styleable.CircleProgressLayout_progressWidth, 0f)
        circleColor = array.getColor(R.styleable.CircleProgressLayout_circleColor, 0)
        progressColor = array.getColor(R.styleable.CircleProgressLayout_progressColor, 0)
        array.recycle()
    }

    override fun onSizeChanged(width: Int, height: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(width, height, oldw, oldh)
        setViewSize(width, height)
    }

    private fun setViewSize(width: Int, height: Int) {
        minViewSize = if (width < height) width else height
    }

    override fun dispatchDraw(canvas: Canvas) {
        super.dispatchDraw(canvas)
        canvas.drawArc(provideRectF(),
            startAngle.toFloat(),
            360f,
            false,
            makePaint(circleLineWidth, circleColor, false))
        canvas.drawArc(provideRectF(),
            startAngle.toFloat(),
            360 * percentage,
            false,
            makePaint(progressLineWidth, progressColor, true))
        val textPaint = makeTextPaint()
        canvas.drawText("${(percentage * 100).toInt()}%",
            canvas.width / 2F,
            (canvas.height / 2) - ((textPaint.descent() + textPaint.ascent()) / 2F),
            textPaint)
    }

    private fun provideRectF(): RectF {
        val left: Float
        val right: Float
        val top: Float
        val bottom: Float
        val padding: Float
        val height = measuredHeight.toFloat()
        val width = measuredWidth.toFloat()
        padding = circleLineWidth / 2f
        if (minViewSize == measuredWidth) {
            left = padding
            right = minViewSize - padding
            top = height / 2f - minViewSize / 2f + padding
            bottom = height / 2f + minViewSize / 2f - padding
        } else {
            left = width / 2f - minViewSize / 2f + padding
            right = width / 2f + minViewSize / 2f - padding
            top = padding
            bottom = minViewSize - padding
        }
        return RectF(left, top, right, bottom)
    }

    private fun makePaint(paintWidth: Float, color: Int, progressView: Boolean): Paint {
        val paint = Paint()
        paint.color = color
        paint.isAntiAlias = true
        paint.strokeCap = if (progressView) Paint.Cap.BUTT else Paint.Cap.ROUND
        paint.strokeWidth = paintWidth
        paint.style = Paint.Style.STROKE
        return paint
    }

    private fun makeTextPaint() = Paint().apply {
        setARGB(200, 0, 0, 0)
        textAlign = Paint.Align.CENTER
        textSize = resources.getDimensionPixelOffset(R.dimen.margin_12).toFloat()
    }

    /**
     * Expected passing percentage from 0 to 1, to show progress;
     *
     * @param percentage
     */
    fun setPercentage(percentage: Float) {
        var percentage = percentage
        if (percentage > 1.0f) {
            percentage = 1f
        }
        this.percentage = percentage
        invalidate()
    }

    fun setProgressColor(@ColorInt color: Int) {
        progressColor = color
    }

    init {
        setWillNotDraw(false)
        parseAttributes(context, attrs)
    }
}