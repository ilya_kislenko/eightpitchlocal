package app.eightpitch.com.views

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView

/**
 * Heir of [AppCompatTextView] which is blocked for inner scrolling of the text
 * because it supposed to animate it's own sizes in runtime by `maxLines` param.
 *
 * And if we are allowed to scroll then UI will be mixed.
 */
class NotScrollableTextView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : AppCompatTextView(context, attrs, defStyleAttr) {

    override fun scrollTo(x: Int, y: Int) {}
}
