package app.eightpitch.com.views

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.core.view.postDelayed
import app.eightpitch.com.R
import kotlinx.android.synthetic.main.view_custom_check.view.*

/**
 * It is a simple toggle with text and an interactive icon for use on filter screens.
 * Has two states, selected and not selected
 */
class ToggleTextView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    var enableBackground: Drawable?
    var enableTextColor = 0
    var disableBackground: Drawable?
    var disableTextColor = 0
    var enableDrawable: Drawable?
    var disableDrawable: Drawable?
    var textHeader = ""
    var isActive = false
    var isEnableClick = true


    init {
        View.inflate(context, R.layout.view_custom_check, this)

        val typedArray =
            getContext().obtainStyledAttributes(
                attrs,
                R.styleable.ToggleTextView,
                0,
                0
            )

        enableBackground =
            typedArray.getDrawable(R.styleable.ToggleTextView_enabledBackgroundDrawable)
        disableBackground =
            typedArray.getDrawable(R.styleable.ToggleTextView_disabledBackgroundDrawable)
        enableTextColor =
            typedArray.getColor(R.styleable.ToggleTextView_enabledTextColor, 0)
        disableTextColor =
            typedArray.getColor(R.styleable.ToggleTextView_disabledTextColor, 0)
        textHeader =
            typedArray.getString(R.styleable.ToggleTextView_text).toString()
        enableDrawable = typedArray.getDrawable(R.styleable.ToggleTextView_enableIcon)
        disableDrawable = typedArray.getDrawable(R.styleable.ToggleTextView_disableIcon)
        isActive = typedArray.getBoolean(R.styleable.ToggleTextView_isActive, false)
        isEnableClick = typedArray.getBoolean(R.styleable.ToggleTextView_isEnableClick, true)
        initViewParams()
        typedArray.recycle()
    }

    private fun initViewParams() {

        initStatus()

        text.text = textHeader

        if(isEnableClick){
            parentView.setOnClickListener {
                isActive = !isActive
                initStatus()
            }
        }
    }

    fun isActivated(isActive: Boolean) {
        this.isActive = isActive
        initStatus()
    }

    private fun initStatus() {
        if (isActive) {

            enableBackground?.let {
                parentView.background = it
            }

            if (enableTextColor != 0)
                text.setTextColor(enableTextColor)

            iconImageView.setImageDrawable(enableDrawable)
        } else {

            disableBackground?.let {
                parentView.background = it
            }

            if (disableTextColor != 0)
                text.setTextColor(disableTextColor)

            iconImageView.setImageDrawable(disableDrawable)
        }
        text.text = textHeader
    }

    /**
     * Listener which blocks a button on a 500 ms,
     * before allowing next click
     */
    fun setDisablingClickListener(perform: () -> Unit) {
        parentView.setOnClickListener {
            isEnabled = false
            isActive = !isActive
            initStatus()
            perform()
            postDelayed(500) {
                isEnabled = true
            }
        }
    }

    /**
     * Listener which blocks a button on a 500 ms,
     * before allowing next click
     */
    fun setIconDisablingClickListener(perform: () -> Unit) {
        iconImageView.setOnClickListener {
            isEnabled = false
            perform()
            postDelayed(500) {
                isEnabled = true
            }
        }
    }

    fun setText(string: String){
        textHeader = string
        text.text = string
    }
}