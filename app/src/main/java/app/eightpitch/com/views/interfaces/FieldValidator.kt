package app.eightpitch.com.views.interfaces

interface FieldValidator {
    fun validate(sequence: CharSequence, start: Int, count: Int, after: Int)
}