package app.eightpitch.com.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import app.eightpitch.com.R
import app.eightpitch.com.extensions.hide
import app.eightpitch.com.extensions.show
import app.eightpitch.com.views.interfaces.HintAbleSpinnerAdapter
import app.eightpitch.com.views.interfaces.HintProvider
import kotlinx.android.synthetic.main.view_spinner_field_layout.view.*

/**
 * This is a normal spinner with an error field and a default empty value.
 * There is also a dynamic tooltip above the spinner.
 */
class HintSpinnerView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : FrameLayout(context, attrs, defStyleAttr) {

    private var hintSpinner = ""

    /**
     * The parameter is responsible for the visibility of the tooltip above the field.
     * (whether it will take up screen space or not)
     */
    var defaultSmallHintVisibility = View.GONE

    init {
        View.inflate(context, R.layout.view_spinner_field_layout, this)
        val typedArray =
            getContext().obtainStyledAttributes(attrs,
                R.styleable.HintSpinnerView, 0, 0)
        hintSpinner = typedArray.getString(R.styleable.HintSpinnerView_hintSpinner).toString()
        defaultSmallHintVisibility =
            typedArray.getInt(R.styleable.HintSpinnerView_defaultSmallHintVisibility, GONE)
        //initSpinner()
        typedArray.recycle()
    }

    private fun initSpinner() {
        val adapterBase = fieldSpinner.adapter
        if (adapterBase != null) {
            val position = fieldSpinner.selectedItemPosition
            val adapter = fieldSpinner.adapter as HintProvider

            if (position == adapter.getHintPosition()) {
                spinnerSmallTextHint.apply {
                    text = hintSpinner
                    visibility = defaultSmallHintVisibility
                }
                spinnerTextHint.apply {
                    text = hintSpinner
                    spinnerTextHint.show()
                }
                inputFieldSeparator.setBackgroundColor(ContextCompat.getColor(context, R.color.gray20))
            } else {
                spinnerSmallTextHint.apply {
                    text = hintSpinner
                    isVisible = hintSpinner.isNotEmpty()
                }
                spinnerTextHint.apply {
                    text = hintSpinner
                    hide()
                }
                inputFieldSeparator.setBackgroundColor(ContextCompat.getColor(context, R.color.darkBlue))
            }
        }
        setError("")
    }

    fun setAdapter(adapter: HintAbleSpinnerAdapter) {
        fieldSpinner.adapter = adapter
    }

    fun setHint(hintText: String) {
        spinnerTextHint.text = hintText
        initSpinner()
    }

    fun setSelection(select: Int) {
        fieldSpinner.setSelection(select)
    }

    fun setSelectedListener(onSelected: (Int) -> Unit, nothingSelected: () -> Unit) {
        fieldSpinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long,
            ) {
                initSpinner()
                onSelected.invoke(position)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                initSpinner()
                nothingSelected.invoke()
            }
        }
    }

    fun setError(errorText: String) {
        if (errorText.isEmpty()) {
            spinnerFieldErrorLayout.apply {
                text = ""
                hide()
            }
        } else {
            inputFieldSeparator.setBackgroundColor(ContextCompat.getColor(context, R.color.lightRed))
            spinnerFieldErrorLayout.apply {
                text = errorText
                show()
            }
        }
    }
}