package app.eightpitch.com.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import app.eightpitch.com.R
import kotlinx.android.synthetic.main.default_toggle_view.view.*

typealias OnSelectionChangedListener = (Boolean) -> Unit

/**
 * This is a common toggle button that is supposed
 * to be used in a whole project, it has two options,
 * whether is was checked or not
 */
class DefaultToggleButton @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    init {
        View.inflate(context, R.layout.default_toggle_view, this)
        val commonAttributes =
            context.obtainStyledAttributes(attrs, R.styleable.DefaultToggleButton, 0, 0)
        val checkByDefault =
            commonAttributes.getBoolean(R.styleable.DefaultToggleButton_isChecked, false)

        changeSelectionState(checkByDefault)
        commonAttributes.recycle()
    }

    override fun setVisibility(visibility: Int) {
        toggleRadioGroup.visibility = visibility
    }

    fun changeSelectionState(toSelected: Boolean) {
        toggleRadioGroup.check(if (toSelected) R.id.radioButtonPositive else R.id.radioButtonNegative)
    }

    fun isPositiveSelection() = toggleRadioGroup.checkedRadioButtonId == R.id.radioButtonPositive

    fun setOnSelectionChangedListener(onSelectionChangedListener: OnSelectionChangedListener) {
        toggleRadioGroup.setOnCheckedChangeListener { _, checkedId ->
            onSelectionChangedListener(checkedId == R.id.radioButtonPositive)
        }
    }
}