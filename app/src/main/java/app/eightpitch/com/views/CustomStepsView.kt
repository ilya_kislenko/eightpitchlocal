package app.eightpitch.com.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import app.eightpitch.com.R
import kotlinx.android.synthetic.main.view_custom_step.view.*

class CustomStepsView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private val minNumber = 1
    private var maxNumber = 5
    private val defaultMaximum = 5
    private var selectedNumber = minNumber


    private var lineSeparatorsArray = arrayListOf<View>()
    private var imageDigitsArray = arrayListOf<AppCompatImageView>()

    private val grayNumbersResources = arrayListOf(
        R.drawable.icon_gray_digit_one,
        R.drawable.icon_gray_digit_two,
        R.drawable.icon_gray_digit_three,
        R.drawable.icon_gray_digit_four,
        R.drawable.icon_gray_digit_five
    )
    private val redNumbersResources = arrayListOf(
        R.drawable.icon_red_digit_one,
        R.drawable.icon_red_digit_two,
        R.drawable.icon_red_digit_three,
        R.drawable.icon_red_digit_four,
        R.drawable.icon_red_digit_five
    )

    init {
        View.inflate(context, R.layout.view_custom_step, this)

        val typedArray =
            getContext().obtainStyledAttributes(attrs, R.styleable.CustomStepsView, 0, 0)
        selectedNumber = typedArray.getInt(R.styleable.CustomStepsView_setSelectedNumber, 0)
        maxNumber = typedArray.getInt(R.styleable.CustomStepsView_setMaxNumber, defaultMaximum)
        imageDigitsArray =
            arrayListOf(iconDigitOne, iconDigitTwo, iconDigitThree, iconDigitFour, iconDigitFive)
        lineSeparatorsArray =
            arrayListOf(lineSeparatorOne, lineSeparatorTwo, lineSeparatorThree, lineSeparatorFour)
        initVisibilityNumber()
        setSelectedStep()
    }

    fun setSelectNumber(number: Int) {
        if (number < 0 || number > maxNumber)
            return
        selectedNumber = number
        setSelectedStep()
    }

    fun setMaxNumber(number: Int) {
        if (number < 1 || number > maxNumber)
            return
        maxNumber = number
        initVisibilityNumber()
    }

    private fun initVisibilityNumber() {
        imageDigitsArray.forEachIndexed { index, appCompatImageView ->
            appCompatImageView.isVisible = index <= maxNumber - 1
        }
        lineSeparatorsArray.forEachIndexed { index, view ->
            view.isVisible = index <= maxNumber - 2
        }
    }

    private fun setSelectedStep() {
        imageDigitsArray.forEachIndexed { index, appCompatImageView ->
            if (index < redNumbersResources.size) {
                val imageResource = when {
                    index <= selectedNumber - 1 -> redNumbersResources.getOrNull(index)
                    else -> grayNumbersResources.getOrNull(index)
                }
                imageResource?.let { appCompatImageView.setImageResource(it) }
            }
        }
        lineSeparatorsArray.forEachIndexed { index, view ->
            view.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    if (index <= selectedNumber - 2) R.color.lightRed else R.color.middleGray
                )
            )
        }
    }
}