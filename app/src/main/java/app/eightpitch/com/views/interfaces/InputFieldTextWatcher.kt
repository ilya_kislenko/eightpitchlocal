package app.eightpitch.com.views.interfaces

import android.text.Editable
import android.text.TextWatcher

internal abstract class InputFieldTextWatcher : TextWatcher {

    override fun afterTextChanged(s: Editable?) {}

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
}
