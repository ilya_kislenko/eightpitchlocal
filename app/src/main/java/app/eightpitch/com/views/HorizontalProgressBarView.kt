package app.eightpitch.com.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.core.view.isVisible
import app.eightpitch.com.R
import kotlinx.android.synthetic.main.view_horisontal_progress_bar.view.*

class HorizontalProgressBarView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    var currentProgress = 0
    private var capProgress = 0
    private var capProgressColor = 0
    var colorBackground = 0

    var currentText = ""
    var capText = ""

    companion object {

        private const val CAP_PROGRESS_ROTATION_PERCENTAGE_CAP = 20
        private const val CURRENT_PROGRESS_ROTATION_PERCENTAGE_CAP = 10
    }


    init {
        View.inflate(context, R.layout.view_horisontal_progress_bar, this)

        val typedArray =
            getContext().obtainStyledAttributes(
                attrs,
                R.styleable.HorizontalProgressBarView,
                0,
                0
            )
        currentProgress =
            typedArray.getInt(R.styleable.HorizontalProgressBarView_setCurrentProgress, 0)
        capProgress = typedArray.getInt(R.styleable.HorizontalProgressBarView_setCapProgress, 0)
        capProgressColor = typedArray.getColor(R.styleable.HorizontalProgressBarView_setCapColor, 0)
        colorBackground =
            typedArray.getColor(R.styleable.HorizontalProgressBarView_progressBackgroundColor, 0)
        currentText =
            typedArray.getString(R.styleable.HorizontalProgressBarView_setCurrentText).toString()
        capText =
            typedArray.getString(R.styleable.HorizontalProgressBarView_setCapText).toString()
        initProgressBar()
        typedArray.recycle()
    }

    private fun initProgressBar() {

        val firstProgressPercent: Float
        val secondProgressPercent: Float

        if (currentProgress >= capProgress) {
            firstProgressPercent = capProgress.toFloat() / 100
            secondProgressPercent = currentProgress.toFloat() / 100
            firstProgress.setBackgroundResource(R.drawable.red_oval_background)
            secondProgress.setBackgroundResource(R.drawable.light_red_oval_background)
        } else {
            firstProgressPercent = currentProgress.toFloat() / 100
            secondProgressPercent = capProgress.toFloat() / 100

            firstProgress.setBackgroundResource(R.drawable.red_oval_background)

            if (capProgressColor != 0) {
                secondProgress.setBackgroundColor(capProgressColor)
            } else {
                secondProgress.setBackgroundResource(R.drawable.blue_oval_background)
            }
        }

        currentlyTextGuideline.setGuidelinePercent(firstProgressPercent)
        currentlyGuideline.setGuidelinePercent(firstProgressPercent)
        softCapGuideline.setGuidelinePercent(secondProgressPercent)
        capTextGuideline.setGuidelinePercent(secondProgressPercent)

        setupCurrentProgress()
        setupCapProgress()

        if (colorBackground != 0)
            progressbarContainer.setCardBackgroundColor(colorBackground)
    }

    private fun setupCurrentProgress() {
        if (currentProgress < CURRENT_PROGRESS_ROTATION_PERCENTAGE_CAP) {
            currentFirstProgressTextView.visibility = INVISIBLE
            currentSecondProgressTextView.visibility = INVISIBLE
            zeroFundingSumTextView.run {
                isVisible = true
                text = currentText
            }
        } else {
            zeroFundingSumTextView.isVisible = false

            if (currentProgress >= capProgress) {
                currentSecondProgressTextView.run {
                    text = currentText
                    visibility = View.VISIBLE
                }
                currentFirstProgressTextView.visibility = INVISIBLE
            } else {
                currentSecondProgressTextView.visibility = INVISIBLE
                currentFirstProgressTextView.run {
                    text = currentText
                    visibility = View.VISIBLE
                }
            }

        }
    }

    private fun setupCapProgress() {
        if (capProgress < CAP_PROGRESS_ROTATION_PERCENTAGE_CAP) {
            capFirstProgressTextView.visibility = View.INVISIBLE
            capSecondProgressTextView.visibility = INVISIBLE
            lowСapTextView.run {
                text = capText
                visibility = VISIBLE
            }
        } else {
            lowСapTextView.visibility = View.INVISIBLE

            if (currentProgress >= capProgress) {
                capFirstProgressTextView.run {
                    text = capText
                    visibility = VISIBLE
                }
                capSecondProgressTextView.visibility = INVISIBLE
            } else {
                capFirstProgressTextView.visibility = INVISIBLE
                capSecondProgressTextView.run {
                    text = capText
                    visibility = VISIBLE
                }
            }
        }
    }

    internal fun setCurrentProgress(progress: Int) {
        currentProgress = progress
        initProgressBar()
    }

    internal fun setCapProgress(progress: Int) {
        capProgress = progress
        initProgressBar()
    }

    internal fun setCapTextProgress(text: String) {
        capText = text
        initProgressBar()
    }

    internal fun setCurrentTextProgress(text: String) {
        currentText = text
        initProgressBar()
    }
}