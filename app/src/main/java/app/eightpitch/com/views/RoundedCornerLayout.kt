package app.eightpitch.com.views

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.widget.RelativeLayout
import app.eightpitch.com.R

class RoundedCornerLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {

    private var leftBottomCorner = 0f
    private var leftTopCorner = 0f
    private var rightBottomCorner = 0f
    private var rightTopCorner = 0f
    private var path: Path? = null

    init {
        val typedArray =
            context.obtainStyledAttributes(attrs, R.styleable.RoundedCornerLayout, 0, 0)
        leftBottomCorner = typedArray.getDimension(R.styleable.RoundedCornerLayout_leftBottomCorner, 0f)
        leftTopCorner = typedArray.getDimension(R.styleable.RoundedCornerLayout_leftTopCorner, 0f)
        rightBottomCorner = typedArray.getDimension(R.styleable.RoundedCornerLayout_rightBottomCorner, 0f)
        rightTopCorner = typedArray.getDimension(R.styleable.RoundedCornerLayout_rightTopCorner, 0f)
        setWillNotDraw(false)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh);
        val r = RectF(0f, 0f, w.toFloat(), h.toFloat())

        path = Path()
        path?.addRoundRect(r,
            floatArrayOf(leftTopCorner,
                leftTopCorner,
                rightTopCorner,
                rightTopCorner,
                rightBottomCorner,
                rightBottomCorner,
                leftBottomCorner,
                leftBottomCorner),
            Path.Direction.CW)
        path?.close()
    }

    override fun draw(canvas: Canvas) {
        canvas.save()
        path?.let { canvas.clipPath(it) }
        super.draw(canvas)
        canvas.restore()
    }
}