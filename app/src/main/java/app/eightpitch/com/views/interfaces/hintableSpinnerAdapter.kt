package app.eightpitch.com.views.interfaces

import android.content.Context
import android.widget.ArrayAdapter
import app.eightpitch.com.views.HintSpinnerView

/**
 * This is a spinner adapter that designed to be used in combine with [HintSpinnerView]
 */
abstract class HintAbleSpinnerAdapter(context: Context, dataSet: List<String>)
    : ArrayAdapter<String>(context, 0, dataSet), HintProvider

/**
 * This interface is required to define the default position when no counter is selected.
 */
interface HintProvider {

    /**
     * Function that returns a position of the hint in data set.
     */
    fun getHintPosition(): Int

    /**
     * Function that is returning a hint.
     */
    fun getHint(): String
}
