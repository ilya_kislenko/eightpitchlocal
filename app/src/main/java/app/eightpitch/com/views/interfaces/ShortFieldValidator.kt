package app.eightpitch.com.views.interfaces

abstract class ShortFieldValidator : FieldValidator {

    override fun validate(sequence: CharSequence, start: Int, count: Int, after: Int) {
        onTextChanged(sequence)
    }

    abstract fun onTextChanged(sequence: CharSequence)
}