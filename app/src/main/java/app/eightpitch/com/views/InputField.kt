package app.eightpitch.com.views

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.text.Editable
import android.text.InputFilter
import android.text.InputType
import android.text.TextWatcher
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import app.eightpitch.com.R
import app.eightpitch.com.extensions.*
import app.eightpitch.com.views.interfaces.FieldValidator
import app.eightpitch.com.views.interfaces.InputFieldTextWatcher
import app.eightpitch.com.views.interfaces.TextField

open class InputField : ParcelableFrameLayout, TextField {

    companion object {

        private val ATTRIBUTES_SET = arrayOf(
            android.R.attr.hint,
            android.R.attr.inputType,
            android.R.attr.imeOptions,
            android.R.attr.textSize,
            android.R.attr.maxLength,
            android.R.attr.drawableLeft
        ).sortedArray()
    }

    protected open fun getLayoutId(): Int = R.layout.input_field_layout

    /**
     *  Seems like there is only one suitable way to retrieve a last char
     *  sequence just after initialization of this field
     */
    var fieldValidator: FieldValidator? = null
        set(value) {
            field = value
            if (::editText.isInitialized)
                editText.text = editText.text
        }

    protected var endIconSrc: Drawable? = null
    protected var isItPasswordField: Boolean = false
    protected lateinit var editText: AppCompatEditText
    protected lateinit var customViewHintTextView: AppCompatTextView
    protected lateinit var eraseButton: AppCompatImageView
    protected lateinit var eyeButton: AppCompatImageView
    protected lateinit var errorsView: AppCompatTextView
    protected lateinit var drawableLeft: AppCompatImageView
    protected lateinit var highlighter: View

    /**
     * Property that indicates whether InputField_customViewHintVisibility
     * option was enabled for this view or not.
     */
    private var shouldShowCustomViewHint: Boolean = false

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(attrs, defStyleAttr)
    }

    private fun init(
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) {

        isSaveEnabled = true
        inflate(context, getLayoutId(), this)

        eyeButton = findViewById(R.id.inputFieldEyeButton)
        editText = findViewById(R.id.inputFieldEditText)
        customViewHintTextView = findViewById(R.id.editTextHint)
        errorsView = findViewById(R.id.inputFieldErrorLayout)
        eraseButton = findViewById(R.id.inputFieldEraseImageView)
        eraseButton.setOnClickListener {
            editText.clear()
        }
        highlighter = findViewById(R.id.inputFieldSeparator)
        drawableLeft = findViewById(R.id.inputFieldDrawableLeft)

        parseCustomAttributes(attrs)
        parseCommonAttributes(attrs, defStyleAttr)
    }

    private fun parseCommonAttributes(
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) {

        val array = context.theme.obtainStyledAttributes(
            attrs,
            ATTRIBUTES_SET.toIntArray(),
            defStyleAttr,
            0
        )

        val hintText = array.getText(ATTRIBUTES_SET.indexOf(android.R.attr.hint)) ?: ""
        editText.hint = hintText
        customViewHintTextView.text = editText.hint.toString()

        var imeOptions = editText.imeOptions
        imeOptions = array.getInt(ATTRIBUTES_SET.indexOf(android.R.attr.imeOptions), imeOptions)
        editText.imeOptions = imeOptions

        var inputType = editText.inputType
        inputType = array.getInt(ATTRIBUTES_SET.indexOf(android.R.attr.inputType), inputType)
        editText.inputType = inputType

        var textSize = editText.textSize
        textSize =
            array.getDimensionPixelSize(
                ATTRIBUTES_SET.indexOf(android.R.attr.textSize),
                textSize.toInt()
            ).toFloat()
        editText.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize)

        val drawable = array.getDrawable(ATTRIBUTES_SET.indexOf(android.R.attr.drawableLeft))

        if (null != drawable) {
            drawableLeft.apply {
                show()
                setImageDrawable(drawable)
            }
        }

        val maxCharLength = array.getInt(ATTRIBUTES_SET.indexOf(android.R.attr.maxLength), 255)
        setMaxCharLength(maxCharLength)

        editText.addTextChangedListener(inputFieldTextWatcher)

        array.recycle()
    }

    private val inputFieldTextWatcher = object : InputFieldTextWatcher() {

        override fun onTextChanged(
            sequence: CharSequence?,
            start: Int,
            before: Int,
            count: Int
        ) {

            updateHighlighterBackground(sequence)

            if (eraseButton.visibility != View.GONE)
                sequence?.let { if (it.isNotEmpty()) eraseButton.show() else eraseButton.hideHoldingAPlace() }

            if (before != count)
                clearErrors()

            if (before == count)
                sequence?.let { editText.setSelection(it.length) }

            fieldValidator?.let { validator ->
                if (null != sequence)
                    validator.validate(sequence, start, before, count)
            }
        }
    }


    private fun parseCustomAttributes(attrs: AttributeSet?) {

        val customAttributes = context.obtainStyledAttributes(attrs, R.styleable.InputField)

        val viewHintVisibility =
            customAttributes.getBoolean(R.styleable.InputField_customViewHintVisibility, false)

        shouldShowCustomViewHint = viewHintVisibility
        customViewHintTextView.isVisible = viewHintVisibility

        editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                /*do nothing*/
            }

            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {
                /*do nothing*/
            }

            override fun onTextChanged(
                sequence: CharSequence?,
                start: Int,
                before: Int,
                count: Int
            ) {
                if (!shouldShowCustomViewHint)
                    return
                //If user enabled custom hint feature we should replace default hint only when view has some text
                //otherwise default EditText's hint will be showed
                customViewHintTextView.visibility =
                    if (sequence.isNullOrBlank()) View.INVISIBLE else View.VISIBLE

                updateHighlighterBackground(sequence)
            }
        })

        isItPasswordField =
            customAttributes.getBoolean(R.styleable.InputField_isItPasswordField, false)

        //TODO use move convenient way to set a drawableRight
        endIconSrc = customAttributes.getDrawable(R.styleable.InputField_endIconSrc)

        if (endIconSrc != null) {
            eyeButton.apply {
                show()
                setImageDrawable(endIconSrc)
            }
        }

        val focusableEditText =
            customAttributes.getBoolean(R.styleable.InputField_focusableField, true)

        editText.isFocusable = focusableEditText
        editText.isFocusableInTouchMode = focusableEditText

        if (isItPasswordField) {
            handlePasswordVisibility()
            editText.apply {
                filters = arrayOf(passwordInputFilter)
                setTextAppearance(R.style.Body16RobotoLight)
            }
        }

        val canInput = customAttributes.getBoolean(R.styleable.InputField_canInput, true)
        setInputAvailability(canInput)

        val hideClearButton =
            customAttributes.getBoolean(R.styleable.InputField_hideClearButton, false)

        if (hideClearButton)
            hideClearButton()

        customAttributes.recycle()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun handlePasswordVisibility() {

        eyeButton.show()
        eyeButton.setOnClickListener {
            it.isSelected = !it.isSelected
            editText.inputType = if (it.isSelected) InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS else 129
        }
    }

    private val passwordInputFilter = InputFilter { source, start, end, dest, dstart, dend ->

        for (i in start until end)
            if (Character.isWhitespace(source[i])) return@InputFilter ""

        return@InputFilter null
    }

    override fun setText(text: CharSequence) {
        editText.post {
            editText.setText(text)
        }

        updateHighlighterBackground(text)
    }

    override fun setText(stringResID: Int) {
        editText.setText(stringResID)
        updateHighlighterBackground(editText.text)
    }

    override fun getText(): CharSequence {
        return editText.text.toString()
    }

    fun setInputAvailability(available: Boolean) {
        editText.isEnabled = available
    }

    fun setHint(hintText: String) {
        editText.hint = hintText
    }

    fun setSelection(index: Int) {

        editText.text?.let {
            if (it.length > index)
                editText.setSelection(index)
        }
    }

    fun hideClearButton() {
        eraseButton.hide()
    }

    fun showClearButton() {
        eraseButton.show()
    }

    fun setMaxCharLength(length: Int) {

        val filters = editText.filters

        if (filters.isNotEmpty()) {

            val newFilters = filters.copyOf(filters.size + 1)
            newFilters[filters.size] = InputFilter.LengthFilter(length)
            editText.filters = newFilters
        } else
            editText.filters = arrayOf(InputFilter.LengthFilter(length))
    }

    fun setPasswordInputFilter() {
        editText.filters = arrayOf(passwordInputFilter)
    }

    fun setInputType(inputType: Int) {
        editText.inputType = inputType
    }

    fun showError(text: String) {

        highlighter.setBackgroundColor(ContextCompat.getColor(context, R.color.lightRed))
        errorsView.apply {
            show()
            this.text = text
        }
    }

    protected open fun clearErrors() {
        updateHighlighterBackground(editText.text)
        errorsView.apply {
            clear()
            hide()
        }
    }

    fun setEndIconClickListener(clickOnEndIcon: (View) -> Unit) {
        endIconSrc?.let {
            eyeButton.setOnClickListener {
                clickOnEndIcon.invoke(it)
            }
        }
    }

    fun setOnFieldClickListener(click: () -> Unit) {
        editText.setDisablingClickListener {
            click.invoke()
        }
        endIconSrc?.let {
            eyeButton.setOnClickListener {
                click.invoke()
            }
        }
    }

    private fun updateHighlighterBackground(sequence: CharSequence?) {
        highlighter.setBackgroundColor(ContextCompat.getColor(context,
            if (!sequence.isNullOrBlank()) R.color.blackLight else R.color.gray20))
    }
}