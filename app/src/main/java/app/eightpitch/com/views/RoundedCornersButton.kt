package app.eightpitch.com.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import androidx.annotation.ColorRes
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.view.postDelayed
import app.eightpitch.com.R

class RoundedCornersButton : AppCompatTextView {

    private val absenceOfColor = -404

    private var paintColor = 0
    private var enabledColor = 0
    private var disabledColor = 0
    private var strokeColor = absenceOfColor
    private var _strokeWidth = 2f

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs, defStyleAttr)
    }

    private fun init(
        attrs: AttributeSet? = null,
        defStyleAttr: Int = -1
    ) {

        isSaveEnabled = true

        setWillNotDraw(false)
        setLayerType(LAYER_TYPE_SOFTWARE, null)
        paintColor = ContextCompat.getColor(context, R.color.white)
        parseAttributes(attrs)
        invalidate()
    }

    private fun parseAttributes(attrs: AttributeSet?) {

        if (null == attrs)
            return

        val customAttributes = context.obtainStyledAttributes(attrs, R.styleable.RoundedCornersButton)
        val fillColor = customAttributes.getColor(R.styleable.RoundedCornersButton_fillColor, -1)
        paintColor = fillColor
        enabledColor = paintColor

        val disabledColor = customAttributes.getColor(R.styleable.RoundedCornersButton_disabledColor, absenceOfColor)
        this.disabledColor = disabledColor

        val strokeColor = customAttributes.getColor(R.styleable.RoundedCornersButton_dashColor, absenceOfColor)
        this.strokeColor = strokeColor

        val strokeWidth = customAttributes.getInteger(R.styleable.RoundedCornersButton_dashWidth, 2)
        this._strokeWidth = (strokeWidth * resources.displayMetrics.density)

        customAttributes.recycle()
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        if (absenceOfColor != disabledColor) {
            paintColor = if (enabled) enabledColor else disabledColor
            invalidate()
        }
    }

    internal fun setColorRes(@ColorRes colorRes: Int) {
        paintColor = ContextCompat.getColor(context, colorRes)
        invalidate()
    }

    internal fun setColor(color: Int) {
        paintColor = color
        invalidate()
    }

    override fun onDraw(canvas: Canvas?) {

        super.onDraw(canvas?.apply {

            val cornerRadius = resources.getDimension(R.dimen.cornerRadius)
            val hasStroke = strokeColor != absenceOfColor

            if (hasStroke)
                drawRoundRect(
                    getFullRectF(),
                    cornerRadius,
                    cornerRadius,
                    getPaint(strokeColor)
                )

            drawRoundRect(
                if (hasStroke) getStrokedRectF() else getFullRectF(),
                cornerRadius,
                cornerRadius,
                getPaint(paintColor)
            )
        })
    }

    private fun getFullRectF() = RectF(0f, 0f, getFloatedWidth(), getFloatedHeight())
    private fun getStrokedRectF(): RectF {

        val halfStrokeWidth = _strokeWidth / 2f
        return RectF(
            halfStrokeWidth,
            halfStrokeWidth,
            getFloatedWidth() - halfStrokeWidth,
            getFloatedHeight() - halfStrokeWidth
        )
    }

    private fun getFloatedWidth() = width.toFloat()
    private fun getFloatedHeight() = height.toFloat()

    private fun getPaint(withColor: Int) = Paint(Paint.ANTI_ALIAS_FLAG or Paint.FILTER_BITMAP_FLAG).apply {
        style = Paint.Style.FILL_AND_STROKE
        color = withColor
    }

    /**
     * Listener which blocks a button on a 500 ms,
     * before allowing next click
     */
    fun setDisablingClickListener(perform: () -> Unit) {
        setOnClickListener {
            isEnabled = false
            perform()
            postDelayed(500) {
                isEnabled = true
            }
        }
    }
}