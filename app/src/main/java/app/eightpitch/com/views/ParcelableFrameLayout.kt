package app.eightpitch.com.views

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import android.util.AttributeSet
import android.util.SparseArray
import android.view.View
import android.widget.FrameLayout

/**
 * Implementation of FrameLayout which support save and restore state of it's children views
 */
open class ParcelableFrameLayout : FrameLayout {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun dispatchSaveInstanceState(container: SparseArray<Parcelable>?) {

        if (null == container || id == View.NO_ID)
            super.dispatchSaveInstanceState(container)
        else {

            val thisViewState = cacheSuperStateAndKeepContainerForChildState()
            container.put(id, thisViewState)
            super.dispatchSaveInstanceState(thisViewState.childStateContainer)
        }
    }

    private fun cacheSuperStateAndKeepContainerForChildState(): ParcelableFrameLayoutSavedState<Parcelable> {

        val superState = super.onSaveInstanceState()
        val inputFieldSavedState = ParcelableFrameLayoutSavedState<Parcelable>(superState)
        inputFieldSavedState.childStateContainer = SparseArray()
        return inputFieldSavedState
    }

    override fun dispatchRestoreInstanceState(container: SparseArray<Parcelable>?) {

        if (null == container || id == View.NO_ID)
            super.dispatchRestoreInstanceState(container)
        else {
            val thisViewState: ParcelableFrameLayoutSavedState<Parcelable> = container.get(id) as ParcelableFrameLayoutSavedState<Parcelable>
            super.dispatchRestoreInstanceState(thisViewState.childStateContainer)
        }
    }

    private class ParcelableFrameLayoutSavedState<T : Parcelable> : BaseSavedState {

        internal var childStateContainer: SparseArray<T>? = SparseArray()

        internal constructor(superState: Parcelable?) : super(superState)

        private constructor(input: Parcel) : super(input) {
            childStateContainer = input.readSparseArray<ClassLoader>(javaClass.classLoader) as SparseArray<T>
        }

        override fun writeToParcel(out: Parcel, flags: Int) {

            super.writeToParcel(out, flags)
            out.writeSparseArray(childStateContainer as SparseArray<Any>)
        }

        companion object {

            @JvmField
            val CREATOR: Parcelable.Creator<ParcelableFrameLayoutSavedState<Parcelable>> =
                object : Parcelable.Creator<ParcelableFrameLayoutSavedState<Parcelable>> {

                    override fun createFromParcel(source: Parcel): ParcelableFrameLayoutSavedState<Parcelable> {
                        return ParcelableFrameLayoutSavedState(source)
                    }

                    override fun newArray(size: Int): Array<ParcelableFrameLayoutSavedState<Parcelable>> {
                        return arrayOf()
                    }
                }
        }
    }
}